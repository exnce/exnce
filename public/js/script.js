$('.slide').on('click', function(){
  $('#fade-in').toggleClass('show');
  $('.sub-hidden').toggleClass('show');
  $('.nav-btns').toggleClass('show');
});


function getTheme(){
    let retVal = "DARK";
    if((localStorage.getItem("DarkExchange") !== null) && ($("body").hasClass("dark"))){
        retVal = localStorage.getItem("DarkExchange");
    }else{
        retVal = "DARK";
    }
    return retVal;
};

function switchTheme(isDark){
    if (isDark != "LIGHT"){
        $("body").addClass("dark");
        localStorage.removeItem("DarkExchange");
        localStorage.setItem("DarkExchange","DARK");
    }else{
        $("body").removeClass("dark");
        localStorage.removeItem("DarkExchange");
        localStorage.setItem("DarkExchange","LIGHT");
    }
    return true;
};

$(".darkTheme").click(function (e) {
    e.preventDefault();
    switchTheme("DARK");
});

$(".lightTheme").click(function (e) {
    e.preventDefault();
    switchTheme("LIGHT");
});

function showNotify(szMsgType, szTitle,szDescription){

    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-center",
        "preventDuplicates": true,
        "onclick": null,
        "showDuration": "1500",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
    if(szMsgType === "success"){
        toastr.success(szDescription, szTitle);
    }else if(szMsgType === "info"){
        toastr.info(szDescription, szTitle);
    }else if(szMsgType === "warning"){
        toastr.warning(szDescription, szTitle);
    }else{
        toastr.error(szDescription, szTitle);
    }
}
function showMessage(szMsgType,szTitle,szDescription,hasConfirm,szConfirmText="Evet",hasCancel,szCancelText="İptal"){
    var defer = $.Deferred();

    var szConfirmBtnClass = "btn-danger";
    if(szMsgType == "info"){
        szConfirmBtnClass = "btn-primary";
    }else if(szMsgType =="warning"){
        szConfirmBtnClass = "btn-warning";
    }else if(szMsgType == "success"){
        szConfirmBtnClass = "btn-success";
    }

    let wrapper = document.createElement('div');
    wrapper.innerHTML = szDescription;

    swal({
        allowOutsideClick: false,
        title: szTitle,
        text: szDescription,
        //content: wrapper,
        icon: szMsgType,
        buttons: {
            cancel: {
                text: szCancelText,
                value: false,
                visible: hasCancel,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: szConfirmText,
                value: true,
                visible: hasConfirm,
                className: szConfirmBtnClass,
                closeModal: true
            }
        }
    }).then((isConfirm) => {
            defer.resolve(isConfirm);
    });

    return defer.promise();
}





function toggleFullScreen() {
    var doc = window.document;
    var docEl = doc.documentElement;

    var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
    var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

    if(!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
        requestFullScreen.call(docEl);
    }
    else {
        cancelFullScreen.call(doc);
    }
}

$(document).ready(function() {

    switchTheme(getTheme());






    AOS.init({
        easing: 'ease-in-out-sine',
        duration: 600,
        disable: 'mobile'
    });


    var sidebarOpen = true;
    var sidePanelLeft = "-50%";

    $( ".exnce_pairs" ).click(function() {

        let exnceChartPanelWidth = $(".exnce_chart").width();

        if(sidebarOpen) {
            $( ".outer-most .side-panel").css('margin-left',0+'px');
        }else{
            $( ".outer-most .side-panel").css('margin-left',(-1 * exnceChartPanelWidth-35)+'px');
        }
        sidebarOpen = sidebarOpen === false ? true : false;
        sidePanelLeft = sidebarOpen === true ? '0' : (-1 * (exnceChartPanelWidth-35))+"px";

         $( ".outer-most .side-panel").width(exnceChartPanelWidth).animate({ marginLeft: sidePanelLeft }, "fast" );
        $("html, body, *").scrollLeft(0);

    });

    $('.back').click(function(){
            $("html, body, *").animate({ scrollLeft: 0 }, 1000);
            return false;
    });

        $('.animsition').animsition({
        inClass: 'fade-in',
        outClass: 'fade-out',
        inDuration: 1000,
        outDuration: 5000,
        loading: true,
        loadingParentElement: 'body', //animsition wrapper element
        loadingClass: 'page-loader-wrapper',
        overlay : false,
        onLoadEvent: true,
        timeout: true,
        timeoutCountdown: 10,
        browser: [ 'animation-duration', '-webkit-animation-duration'],
        overlayClass : 'animsition-overlay-slide',
        overlayParentElement : 'body',
        transition: function(url){ window.location.href = url; }
    }) ;






  $(".currencies-item").click(function () {
      $(".currencies-item").removeClass("currencies-item-active");
      $(this).addClass("currencies-item-active");
  });


    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });





  $(".currency-nav-item").click(function () {
      $(".currency-nav-item").removeClass("currency-nav-item-active");
      $(this).addClass("currency-nav-item-active");
      });

    $('.toggle-accountDetails').click(function() {
        $accountDetails = $(this).parent().parent().siblings('.accountDetails');
        if ($accountDetails.hasClass('show')) {
            $accountDetails.collapse('hide');
        } else {
            $accountDetails.collapse('show');
        }
    });

    $('.toggle-accountQRCode').click(function() {
        $accountQRCode = $(this).parent().parent().siblings('.qrCode');
        if ($accountQRCode.hasClass('show')) {
            $accountQRCode.collapse('hide');
        } else {
            $accountQRCode.collapse('show');
        }
    });


    var verticalBarsAnimation = () => {
        var tl = new TimelineMax({repeat:-1, repeatDelay:2.5});

        tl.to('.v-bar-a', 0.2, {height: '-=10px', ease: Power0.easeIn})
            .to('.v-bar-b', 0.2, {height: '-=10px', ease: Power0.easeIn})
            .to('.v-bar-a', 0.2, {height: '+=10px', ease: Power0.easeIn})
            .to('.v-bar-b', 0.2, {height: '+=10px', ease: Power0.easeIn});

    };
    var blinkingPrompt = function(){
        var tl = new TimelineMax({repeat: -1});
        tl.to('.blink', 0.8, {opacity: 0})
            .to('.blink', 1, {opacity: 1})
    };


    verticalBarsAnimation();
    blinkingPrompt();


//Vertical Scrolling
//     $(".outer-most").mousewheel(function(event, delta) {
//
//         this.scrollLeft -= (delta * 2);
//         event.preventDefault();
//
//
//
//     });

/*    $('#calling_code').select2({
        allowClear: false
    });*/
});



