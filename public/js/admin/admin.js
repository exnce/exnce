$(document).ready(function () {
    TAdmin = function () {
        this.depositOrdersEndpointURI = "/admin/orders/deposit/requests/get";
        this.depositOrdersDetailEndpointURI = "/admin/orders/deposit/requests/get/";
        this.depositOrdersDetailCancelEndpointURI = "/admin/orders/deposit/requests/cancel";
        this.depositOrdersDetailVerifyEndpointURI = "/admin/orders/deposit/requests/approve";

        this.withdrawOrdersEndpointURI = "/admin/orders/withdraw/requests/get";
        this.withdrawOrdersDetailEndpointURI = "/admin/orders/withdraw/requests/get/";
        this.withdrawOrdersDetailCancelEndpointURI = "/admin/orders/withdraw/requests/cancel";
        this.withdrawOrdersDetailVerifyEndpointURI = "/admin/orders/withdraw/requests/approve";

        this.definationsCryptoCurrencyEndpointURI = "/admin/definations/cryptocurrency/get";
        this.definationsCryptoCurrencyDetailEndpointURI = "/admin/definations/cryptocurrency/get/";
        this.definationsCryptoCurrencyUpdateEndpointURI = "/admin/definations/cryptocurrency/update";
        this.definationsCryptoCurrencyAddEndpointURI = "/admin/definations/cryptocurrency/add";

        this.definationsFiatCurrencyEndpointURI = "/admin/definations/fiatcurrency/get";
        this.definationsFiatCurrencyDetailEndpointURI = "/admin/definations/fiatcurrency/get/";
        this.definationsFiatCurrencyUpdateEndpointURI = "/admin/definations/fiatcurrency/update";
        this.definationsFiatCurrencyAddEndpointURI = "/admin/definations/fiatcurrency/add";

        this.definationsBankEndpointURI = "/admin/definations/bank/get";
        this.definationsBankDetailEndpointURI = "/admin/definations/bank/get/";
        this.definationsBankUpdateEndpointURI = "/admin/definations/bank/update";
        this.definationsBankAddEndpointURI = "/admin/definations/bank/add";

        this.definationsBankAccountEndpointURI = "/admin/definations/bank-account/get";
        this.definationsBankAccountDetailEndpointURI = "/admin/definations/bank-account/get/";
        this.definationsBankAccountUpdateEndpointURI = "/admin/definations/bank-account/update";
        this.definationsBankAccountAddEndpointURI = "/admin/definations/bank-account/add";

        this.identityApprovalEndpointURI = "/admin/approval/requests/get";
        this.identityApprovalDetailEndpointURI = "/admin/approval/requests/get/";
        this.identityApprovalDetailUpdateEndpointURI = "/admin/approval/requests/update";
        this.identityApprovalShowDocumentEndpointURI = "/admin/approval/showDocument";


        this.loadSupportRequestsEndPointURI = "/admin/support/get";
        this.createSupportRequestEndPointURI = "/admin/support/save";

        this.userListUsersEndpointURI = "/admin/users/get";

        this.contactListEndpointURI = "/admin/contacts/get";



        this.loadSupportRequests = function () {
            var supportTable = $('#lstTickets').DataTable({
                "processing": true,	language: { search: "", "decimal":",",
                    "thousands": "."
                },
                "processing": true,
                "serverSide": true,
                "language": {
                    url: '/localization/'+localeCode+'.json'
                },
                "paging": true,
                "info": false,
                "bSort" : true,
                "stateSave": true,
                responsive: {
                    details: {
                        type: 'column',
                        target: -1
                    }
                },
                "lengthMenu": [[10, 15, 30, 50,70,100, -1], [10, 15, 30, 50,70,100, translate["ALL"]]],
                fixedHeader: true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.loadSupportRequestsEndPointURI,
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": false,
                    "headers": {
                        'X-CSRF-TOKEN': Admin.CSRF_TOKEN
                    },
                    "data" : function(json){
                        json.is_completed=ListCompletedSupportRequests;
                        return JSON.stringify(json);
                    }
                },
                "rowCallback": function( row, data, index ) {
                    $(row).find('td:eq(2)').css('color', data.color);

                    $(row).find('td:eq(0)').html("<a href='"+data.detail+"'>"+data.subject+"</a>");
                    $(row).find('td:eq(1)').html("<a href='"+data.userdetail+"'>"+data.user+"</a>");


                },
                columnDefs: [ {
                    "width" : "5%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                    { "width": "25%", "targets": 0 },
                    { "width": "10%", "targets": 1 },
                    { "width": "10%", "targets": 2 },
                    { "width": "10%", "targets": 3 },
                    { "width": "25%", "targets": 4},
                ],
                "columns": [
                    {"data": "subject",responsivePriority: 1},
                    {"data": "user",responsivePriority: 2},
                    {"data": "status",responsivePriority: 3},
                    {"data": "date",responsivePriority: 4},
                    {"data": "agent",className:'text-right'
                    },
                    {render: function () {
                            return "";
                        }}
                ]

            });
        };


        this.loadIdentityApprovals = function () {
            otable = $('#identityApprovals').DataTable({
                "processing": true,
                "serverSide": false,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.identityApprovalEndpointURI,
                    "data": function (json) {
                        return JSON.stringify({"Sql": 12});
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "user_name"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<a class='' data-id='" + data + "' href='" + Admin.identityApprovalShowDocumentEndpointURI + "/" + data + "/idcard' target='_blank'>Göster</a>";
                        }
                    },
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<a class='' data-id='" + data + "' href='" + Admin.identityApprovalShowDocumentEndpointURI + "/" + data + "/selfie' target='_blank'>Göster</a>";
                        }
                    },
                    {"data": "status"},
                    {"data": "updated_at"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-approval-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };


        this.loadIdentityApprovalDetail = function (approvalRequestId) {
            var json = {_token: this.CSRF_TOKEN, requestId: approvalRequestId};
            $.ajax({
                type: "POST",
                url: this.identityApprovalDetailEndpointURI + approvalRequestId,
                data: json,
                success: function (data) {
                    $(".approval-details").html(data);
                    $("#frmApprovalDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };


        $(document).on('click', ".btn-approval-details", function (event) {
            event.preventDefault();
            var $self = $(this);
            var id = $self.data('id');
            Admin.loadIdentityApprovalDetail(id);
        });


        $(document).on('click', ".btn-identity-approve", function (event) {
            event.preventDefault();
            var $self = $(this);

            var serializedData = $("#frmApprovalRequestDetailForm").serialize();
            Admin.updateApprovalRequest(serializedData, "frmDepositRequestDetailForm");
        });

        this.updateApprovalRequest = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.identityApprovalDetailUpdateEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };


        this.loadContacts = function () {
            otable = $('#contactList').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.contactListEndpointURI,
                    "data": function (d) {
                        return JSON.stringify(d);
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columnDefs": [
                    { "width": "5%", "targets": 0 },
                    { "width": "5%", "targets": 0 },
                    { "width": "5%", "targets": 1 },
                    { "width": "5%", "targets": 2,"className": "text-left"},
                    { "width": "30%", "targets": 3,"className": "text-left"},
                    { "width": "10%", "targets": 2,"className": "text-left"},
                    { "width": "10%", "targets": 2,"className": "text-left"},

                ],
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "lastname"},
                    {"data": "email"},
                    {"data": "message"},
                    {"data": "status"},
                    {"data": "date"}
                ]
            });
        };


        this.loadUsers = function () {
            otable = $('#userList').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.userListUsersEndpointURI,
                    "data": function (d) {
                        return JSON.stringify(d);
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columnDefs": [
                    { "width": "4%", "targets": 0 },
                    { "width": "20%", "targets": 0 },
                    { "width": "20%", "targets": 1 },
                    { "width": "15%", "targets": 2,"className": "text-left"},
                    { "width": "15%", "targets": 3,"className": "text-left"},
                    { "width": "15%", "targets": 2,"className": "text-left"},
                    { "width": "5%", "targets": 2,"className": "text-left"},
                    { "width": "5%", "targets": 2,"className": "text-left"},
                    { "width": "15%", "targets": 2,"className": "text-left"},

                ],
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "email"},
                    {"data": "phone"},
                    {"data": "user_type"},
                    {"data": "verified"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<a class='btn btn-user-details btn-block btn-success btn-sm' href='/admin/users/detail/" + data + "' href='#'>Detay</a>";
                        }
                    }
                ]
            });
        };

        this.loadFiatCurrencyDefinations = function () {
            otable = $('#fiatcurrencyList').DataTable({
                "processing": true,
                "serverSide": false,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.definationsFiatCurrencyEndpointURI,
                    "data": function (json) {
                        return JSON.stringify({"Sql": 12});
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "symbol"},
                    {"data": "slug"},
                    {"data": "decimals"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-fiatcurrency-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };

        this.loadBankDefinationDetails = function (currencyId) {
            var json = {_token: this.CSRF_TOKEN, requestId: currencyId};
            $.ajax({
                type: "POST",
                url: this.definationsBankDetailEndpointURI + currencyId,
                data: json,
                success: function (data) {
                    $(".bank-details").html(data);
                    $("#frmBankDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.loadFiatCurrencyDefinationDetails = function (currencyId) {
            var json = {_token: this.CSRF_TOKEN, requestId: currencyId};
            $.ajax({
                type: "POST",
                url: this.definationsFiatCurrencyDetailEndpointURI + currencyId,
                data: json,
                success: function (data) {
                    $(".currency-details").html(data);
                    $("#frmCurrencyDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.loadBankAccountDefinationDetails = function (bankAccountId) {
            var json = {_token: this.CSRF_TOKEN, requestId: bankAccountId};
            $.ajax({
                type: "POST",
                url: this.definationsBankAccountDetailEndpointURI + bankAccountId,
                data: json,
                success: function (data) {
                    $(".bank-account-details").html(data);
                    $("#frmBankAccountDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.addNewFiatCurrency = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsFiatCurrencyAddEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.updateFiatCurrencyDetails = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsFiatCurrencyUpdateEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.updateBankDetails = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsBankUpdateEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.addNewBank = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsBankAddEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.addNewBankAccount = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsBankAccountAddEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.updateBankAccount = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsBankAccountUpdateEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };


        this.loadCryptoCurrencyDefinations = function () {
            otable = $('#cryptocurrencyList').DataTable({
                "processing": true,
                "serverSide": false,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.definationsCryptoCurrencyEndpointURI,
                    "data": function (json) {
                        return JSON.stringify({"Sql": 12});
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "name"},
                    {"data": "symbol"},
                    {"data": "slug"},
                    {"data": "decimals"},
                    {"data": "min_deposit"},
                    {"data": "min_withdraw"},
                    {"data": "fee"},
                    {"data": "processing_fee"},
                    {"data": "display_index"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-cryptocurrency-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };

        this.loadCryptoCurrencyDefinationDetails = function (currencyId) {
            var json = {_token: this.CSRF_TOKEN, requestId: currencyId};
            $.ajax({
                type: "POST",
                url: this.definationsCryptoCurrencyDetailEndpointURI + currencyId,
                data: json,
                success: function (data) {
                    $(".coin-details").html(data);
                    $("#frmCurrencyDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };


        this.addNewCryptoCurrency = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsCryptoCurrencyAddEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.updateCryptoCurrencyDetails = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.definationsCryptoCurrencyUpdateEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };



        this.loadWithdrawRequestDetails = function (withdrawRequestId) {
            var json = {_token: this.CSRF_TOKEN, requestId: withdrawRequestId};
            $.ajax({
                type: "POST",
                url: this.withdrawOrdersDetailEndpointURI + withdrawRequestId,
                data: json,
                success: function (data) {
                    $(".withdraw-details").html(data);
                    $("#frmWithdrawDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.cancelWithdrawRequest = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.withdrawOrdersDetailCancelEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.verifyWithdrawRequest = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.withdrawOrdersDetailVerifyEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });

        };

        this.loadDepositRequestDetails = function (depositRequestId) {
            var json = {_token: this.CSRF_TOKEN, requestId: depositRequestId};
            $.ajax({
                type: "POST",
                url: this.depositOrdersDetailEndpointURI + depositRequestId,
                data: json,
                success: function (data) {
                    $(".deposit-details").html(data);
                    $("#frmDepositDetails").modal().show();
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.cancelDepositRequest = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.depositOrdersDetailCancelEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };

        this.verifyDepositRequest = function (serializedData, activeForm) {
            $.ajax({
                type: "POST",
                url: this.depositOrdersDetailVerifyEndpointURI,
                data: serializedData,
                success: function (data) {
                    showMessage(data.type, data.title, data.message, true, "Tamam", false, "");
                },
                failure: function (errMsg) {
                    alert(errMsg);
                }
            });
        };


        this.loadDepositOrders = function () {
            otable = $('#depositOrders').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.depositOrdersEndpointURI,
                    "data": function (d) {
                        return JSON.stringify(d);
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "coin"},
                    {"data": "txid"},
                    {"data": "user"},
                    {"data": "amount"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                           if(!full.can_confirm){
                              return "GAS";
                           }else{
                            return "<button class='btn btn-deposit-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                           }
                        }
                    }
                ]
            });
        };

        this.loadWithdrawOrders = function () {
            otable = $('#withdrawOrders').DataTable({
                "processing": true,
                "serverSide": true,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.withdrawOrdersEndpointURI,
                    "data": function (d) {
                        return JSON.stringify(d);
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "rowCallback": function( row, data, index ) {
                    $(row).find('td:eq(3)').html(data.name);

                },
                "columns": [
                    {"data": "id"},
                    {"data": "bank_name"},
                    {"data": "transfer_code"},
                    {"data": "name"},
                    {"data": "amount"},
                    {"data": "operator"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-withdraw-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };

        this.loadBanks = function () {
            otable = $('#bankList').DataTable({
                "processing": true,
                "serverSide": false,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.definationsBankEndpointURI,
                    "data": function (json) {
                        return JSON.stringify({"Sql": 12});
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "bank_name"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-bank-defination-bank-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };

        this.loadBankAccounts = function () {
            otable = $('#bankAccountList').DataTable({
                "processing": true,
                "serverSide": false,
                "stateSave": true,
                "ajax": {
                    "type": "POST",
                    "url": Admin.definationsBankAccountEndpointURI,
                    "data": function (json) {
                        return JSON.stringify({"Sql": 12});
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": true,
                    beforeSend: function (xhr) {
                        var access_token = "test";
                        xhr.setRequestHeader('Authorization', 'Bearer ' + access_token);
                    }
                },
                "columns": [
                    {"data": "id"},
                    {"data": "bank_name"},
                    {"data": "currency_type"},
                    {"data": "account_owner"},
                    {"data": "iban"},
                    {"data": "status"},
                    {
                        "data": "id", render: function (data, type, full, meta) {
                            return "<button class='btn btn-bankaccount-defination-bankaccount-details btn-block btn-success btn-sm' data-id='" + data + "' href='#'>Detay</button>";
                        }
                    }
                ]
            });
        };

    };

    var Admin = new TAdmin;




    $(document).on('click', ".btn-withdraw-details", function (event) {
        event.preventDefault();
        var $self = $(this);
        var id = $self.data('id');
        Admin.loadWithdrawRequestDetails(id);
    });

    $(document).on('click', ".btn-deposit-details", function (event) {
        event.preventDefault();
        var $self = $(this);
        var id = $self.data('id');
        Admin.loadDepositRequestDetails(id);
    });

    $(document).on('click', ".btn-approve-deposit", function (event) {
        event.preventDefault();
        var $self = $(this);

        showMessage("warning", "Uyari", "Yatırım Talebini Onaylamak Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmDepositRequestDetailForm").serialize();
                Admin.verifyDepositRequest(serializedData, "frmDepositRequestDetailForm");
            } else {
                showMessage("info", "Bilgi", "Yatırım İşlemi Onaylanmadi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-cancel-deposit", function (event) {
        event.preventDefault();
        var $self = $(this);
        showMessage("warning", "Uyari", "Yatırım Talebini İptal Etmek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmDepositRequestDetailForm").serialize();
                Admin.cancelDepositRequest(serializedData, "frmDepositRequestDetailForm");
            } else {
                showMessage("info", "Bilgi", "Yatırım İptal İşlemi Edilmedi!", true, "Tamam", false, "Hayir");
            }
        });
    });
    $(document).on('click', ".btn-approve-withdraw", function (event) {
        event.preventDefault();
        var $self = $(this);

        showMessage("warning", "Uyari", "Çekim Talebini Onaylamak Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmWithdrawRequestDetailForm").serialize();
                Admin.verifyWithdrawRequest(serializedData, "frmWithdrawRequestDetailForm");
            } else {
                showMessage("info", "Bilgi", "Çekim İşlemi Onaylanmadi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-cancel-withdraw", function (event) {
        event.preventDefault();
        var $self = $(this);
        showMessage("warning", "Uyari", "Çekim Talebini İptal Etmek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmWithdrawRequestDetailForm").serialize();
                Admin.cancelWithdrawRequest(serializedData, "frmWithdrawRequestDetailForm");
            } else {
                showMessage("info", "Bilgi", "Çekim İptal İşlemi Edilmedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-add-cryptocurrency", function (event) {
        event.preventDefault();
        var $self = $(this);
        $("#frmCurrencyAdd").modal().show();
    });


    $(document).on('click', ".btn-cryptocurrency-details", function (event) {
        event.preventDefault();
        var $self = $(this);
        var id = $self.data('id');
        Admin.loadCryptoCurrencyDefinationDetails(id);
    });

    $(document).on('click', ".btn-add-new-coin", function (event) {
        event.preventDefault();
        var $self = $(this);

        showMessage("warning", "Uyari", "Yeni Kripto Para Birimi Eklemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmAddCryptoCurrencyForm").serialize();
                Admin.addNewCryptoCurrency(serializedData, "#frmCurrencyAdd");
            } else {
                showMessage("info", "Bilgi", "Kripto Para Eklenemedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-update-coin-details", function (event) {
        event.preventDefault();
        var $self = $(this);

        showMessage("warning", "Uyari", "Kripto Para Detaylarini Guncellemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmCryptoCurrencyDetailForm").serialize();
                Admin.updateCryptoCurrencyDetails(serializedData, "#frmCurrencyDetails");
            } else {
                showMessage("info", "Bilgi", "Kripto Para Detaylari Guncellenmedi!", true, "Tamam", false, "Hayir");
            }
        });
    });


    $(document).on('click', ".btn-add-fiatcurrency", function (event) {
        event.preventDefault();
        var $self = $(this);
        $("#frmCurrencyAdd").modal().show();
    });

    $(document).on('click', ".btn-add-new-fiat-currency", function (event) {
        event.preventDefault();
        let $self = $(this);
        showMessage("warning", "Uyari", "Yeni Para Birimi Eklemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmAddFiatCurrencyForm").serialize();
                Admin.addNewFiatCurrency(serializedData, "#frmCurrencyAdd");
            } else {
                showMessage("info", "Bilgi", "Para Birimi Eklenemedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-update-currency-details", function (event) {
        event.preventDefault();
        let $self = $(this);

        showMessage("warning", "Uyari", "Para Birimi Detaylarini Guncellemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                let serializedData = $("#frmFiatCurrencyDetailForm").serialize();
                Admin.updateFiatCurrencyDetails(serializedData, "#frmCurrencyDetails");
            } else {
                showMessage("info", "Bilgi", "Para Birimi Detaylari Guncellenmedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-fiatcurrency-details", function (event) {
        event.preventDefault();
        let $self = $(this);
        let id = $self.data('id');
        Admin.loadFiatCurrencyDefinationDetails(id);
    });

    $(document).on('click', ".btn-bank-defination-add-bank", function (event) {
        event.preventDefault();
        var $self = $(this);
        $("#frmAddBank").modal().show();
    });
    $(document).on('click', ".btn-bank-defination-bank-details", function (event) {
        event.preventDefault();
        let $self = $(this);
        let id = $self.data('id');
        Admin.loadBankDefinationDetails(id);
    });

    $(document).on('click', ".btn-bank-definations-update-bank-details", function (event) {
        event.preventDefault();
        let $self = $(this);

        showMessage("warning", "Uyari", "Banka Bilgilerini Guncellemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                let serializedData = $("#frmUpdateBankForm").serialize();
                Admin.updateBankDetails(serializedData, "#frmBankDetails");
            } else {
                showMessage("info", "Bilgi", "Banka Guncellenmedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-bank-definations-add-new-bank", function (event) {
        event.preventDefault();
        let $self = $(this);
        showMessage("warning", "Uyari", "Yeni Banka Eklemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmAddBankForm").serialize();
                Admin.addNewBank(serializedData, "#frmAddBank");
            } else {
                showMessage("info", "Bilgi", "Banka Eklenemedi!", true, "Tamam", false, "Hayir");
            }
        });
    });


    $(document).on('click', ".btn-bankaccount-defination-bankaccount-details", function (event) {
        event.preventDefault();
        var $self = $(this);
        var id = $self.data('id');
        Admin.loadBankAccountDefinationDetails(id);
    });

    $(document).on('click', ".btn-bankaccount-defination-add-bankaccount", function (event) {
        event.preventDefault();
        var $self = $(this);
        $("#frmAddBankAccount").modal().show();
    });

    $(document).on('click', ".btn-bankaccount-definations-add-new-bankaccount", function (event) {
        event.preventDefault();
        let $self = $(this);
        showMessage("warning", "Uyari", "Yeni Banka Hesabi Eklemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmAddBankAccountForm").serialize();
                Admin.addNewBankAccount(serializedData, "#frmAddBankAccount");
            } else {
                showMessage("info", "Bilgi", "Banka Hesabi Eklenemedi!", true, "Tamam", false, "Hayir");
            }
        });
    });

    $(document).on('click', ".btn-bankaccount-definations-update-bankaccount-details", function (event) {
        event.preventDefault();
        let $self = $(this);
        showMessage("warning", "Uyari", "Banka Hesabini Guncellemek Istediginizden Eminmisiniz?", true, "Evet", true, "Hayir").then(function (bResult) {
            if (bResult) {
                var serializedData = $("#frmAddBankAccountDetailsForm").serialize();
                Admin.updateBankAccount(serializedData, "#frmBankAccountDetails");
            } else {
                showMessage("info", "Bilgi", "Banka Hesabi Guncellenemedi!", true, "Tamam", false, "Hayir");
            }
        });
    });



    Admin.loadDepositOrders();
    Admin.loadWithdrawOrders();
    Admin.loadCryptoCurrencyDefinations();
    Admin.loadFiatCurrencyDefinations();
    Admin.loadBanks();
    Admin.loadBankAccounts();
    Admin.loadUsers();

    Admin.loadIdentityApprovals();
    Admin.loadSupportRequests();
    Admin.loadContacts();

});