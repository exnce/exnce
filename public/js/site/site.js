var TSite = new function () {
    this.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    this.MarketEndPointURI = "/market/load";
    this.pairEndPointURI = "/market/";
    this.airdropDetailsEndpointURI = "/airdrops/details/";

    this.coinBalanceFloat = 0;
    this.currencyBalanceFloat = 0;
    this.coinID = -1;
    this.fiatBalance = 0.00;
    this.cryptoBalance = 0.00;

    this.activeCurrency = null;
    this.activeCoin = null;


    this.orderBook = null;
    this.depthChart = null;
    
    this.selectedTabID = 0;

    this.fixZeroValues = function(Amount) {

        var szDecimalData = "";
        var szDecimalPlaces = "";
        var szIntPlaces = "";
        var szAmountVal = Amount + "";
        var checkDecimalPlaces = szAmountVal.match(/\./);
        var getZeroNumbers = szAmountVal.match(/(0+)$/g);
        var szData =  `<span class='fade-price'>${szAmountVal}</span>`;
        if (checkDecimalPlaces && getZeroNumbers)
        {
            [szIntPlaces, szDecimalPlaces] = szAmountVal.split('.');
            szDecimalData = szDecimalPlaces.replace(/(0+)$/g, '');
            szData = `<span class='fade-price'>${szIntPlaces}.${szDecimalData}`;
            if (getZeroNumbers)
            {
                szData += `<span class='fade-number'>${getZeroNumbers[0]}</span>`;
            }
            szData += `</span>`;
        }
        return szData;
    };

    this.setCoinID = function (coinID) {
        this.coinID = coinID;
    };

    this.setCurrencyID = function (currencyID) {
        this.currencyID = currencyID;
    };

    this.loadAirdropDetails = function(airdropId){
        var json = {_token: this.CSRF_TOKEN, airdropId: airdropId};
        $.ajax({
            type: "GET",
            url: this.airdropDetailsEndpointURI + airdropId,
            data: json,
            success: function (data) {
                $(".airdrop-details").html(data);
                $("#frmAirdropDetails").modal().show();
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };

    this.updateHeaderStatistics = function (data) {

        jQuery.each(data, function (i, val) {
            $("#"+i+"-info").html(val.data);
            $("#"+i+"-symbol").html(val.symbol);
            $("#"+i+"-info").removeClass();
            $("#"+i+"-info").addClass(val.color);
            $("#"+i+"-symbol").removeClass();
            $("#"+i+"-symbol").addClass(val.color);
        });

    };

    $.fn.dataTable.ext.errMode = 'throw';

    this.initAirdrops = function(){
        $('#listAirdrops').DataTable({
            "stateSave": true,
            "fixedColumns": true,
            "paging": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": false,
            "ordering": false,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "columnDefs": [
                {
                    "width" : "5%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "10%", "targets": 0, "className": "text-left"},
                { "width": "15%", "targets": 1, "className": "text-left"},
                { "width": "15%", "targets": 2, "className": "text-left"},
                { "width": "15%", "targets": 3, "className": "text-left"},
                { "width": "15%", "targets": 4, "className": "text-left"},
                { "width": "20%", "targets": 5, "className": "text-left"},
                { "width": "20%", "targets": 6, "className": "text-left"}
                ],
        });
    };

    this.chartColours = function(colorName){
        var colorList = {}
        if(this.getTheme() == "LIGHT"){
            colorList["BACKGROUND"] = "#ffffff";
            colorList["GRID"] = "#3a4c59";
            colorList["TEXT"] = "#D99E08";
        }else{
            colorList["BACKGROUND"] = "#293742";
            colorList["GRID"] = "#49555e";
            colorList["TEXT"] = "#D99E08";
        }
        return colorList[colorName];
    };


    this.fillDepthChart = function (sellData,buyData,actualPrice) {
        TSite.depthChart = $('#depthchartcontainer').highcharts();

        var bidList = [];
        var askList = [];

        var askDepthTotal = 0;
        var bidDepthTotal = 0;

        var askListEx = [];
        var bidListEx = [];
        var i = 0;



        jQuery.each(buyData, function (i, val) {
            bidDepthTotal += parseFloat(val.amount);
            var singleBid = {x:parseFloat(val.price), y:bidDepthTotal};
            bidList.push(singleBid);
        });

        jQuery.each(sellData, function (i, val) {
            var singleAsk = {price:val.price, amount: val.amount};
            askListEx.push(singleAsk);
        });
        askListEx = askListEx.reverse();

        jQuery.each(askListEx, function (i, val) {
            askDepthTotal += parseFloat(val.amount);
            var singleAsk = {x:parseFloat(val.price), y:askDepthTotal};
            askList.push(singleAsk);
        });

        this.orderBook={bids: bidList,asks:askList,bid:bidDepthTotal, ask:askDepthTotal};
        
    };

    this.closeSidebar = function () {
        $(".exnce_pairs").click();
    };

    this.initDepthChart = function (chartTitle, bids,asks) {
        let totalHeight = $(".exnce_pairs").height();
        let headerHeight = 50;
        $('#depthchartcontainer').highcharts({
            chart: {
                marginTop:25,
                marginLeft: 25,
                marginRight: 25,
                marginBottom: 25,
                backgroundColor: "#0D131E",
                type: 'area',
                zoomType: 'xy',
                height: totalHeight-headerHeight,
                styledMode: true,
                reflow: true,
                events: {
                    load: function() {


                        refreshInterval = setInterval(function () {
                            if ((null != TSite.orderBook) && (TSite.orderBook.asks.length > 0) && (TSite.orderBook.bids.length > 0)) {
                                TSite.depthChart.series[0].setData(TSite.orderBook.bids);
                                TSite.depthChart.series[1].setData(TSite.orderBook.asks);
                            }
                        }, 1000)
                    }
                },

            },
            credits: {
                enabled: false
            },
            title: {
                text: chartTitle
            },



            xAxis: {
                className: 'mh-buy',
                minPadding: 0,
                maxPadding: 0,
                type: "linear",
                labels: {
                    align: 'left',
                    x: 8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    }
                }
            },
            yAxis: [{
                className: 'mh-sell',
                lineWidth: 0,
                gridLineWidth: 0,
                title: null,
                tickWidth: 1,
                tickLength: 5,
                tickPosition: "inside",
                type: "linear",
                labels: {
                    align: 'left',
                    x: 8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    },
                }
            }, {
                opposite: !0,
                linkedTo: 0,
                lineWidth: 1,
                gridLineWidth: 0,
                title: null,
                tickWidth: 1,
                tickLength: 5,
                tickPosition: "inside",
                labels: {
                    align: "right",
                    x: -8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    }
                }
            }

            ],
            legend: {
                enabled: !1
            },
            plotOptions: {
                area: {
                    fillOpacity: .5,
                    lineWidth: 1,
                    step: "center",
                    marker: {
                        enabled: !1
                    }
                }
            },
            tooltip: {
                headerFormat: "<span style='font-size=10px;'>" + translate["DEPTH_CHART_PRICE"]+ ": {point.key}</span><br/>",

                crosshairs: !0,
                valueDecimals: 8,
            },
            series: [ {
                type: "area",
                name: translate["DEPTH_CHART_SELL"],
                data: [{
                    x: 0,
                    y: 0
                }],
                color: "#ea0070"
            },{
                type: "area",
                name: translate["DEPTH_CHART_BUY"],
                data: [{
                    x: 0,
                    y: 0
                }],
                color: "#70a800"
            }]
        });
    };
    this.loadWebCamera = function () {
        var webcamVideoPanel = $("#webcamVideoPanel");
        if (webcamVideoPanel.length > 0) {
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');
            var mediaConfig =  { video: true };
            var errBack = function(e) {
                console.log('An error has occurred!', e);
            };
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia(mediaConfig).then(function(stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    webcamVideoPanel.srcObject = stream;
                    webcamVideoPanel.play();
                });
            }
        }};

    this.getTheme = function(){
        let retVal = "LIGHT";
        if((localStorage.getItem("DarkExchange") !== null) || ($("body").hasClass("dark"))){
            retVal = localStorage.getItem("DarkExchange");
        }else{
            retVal = "LIGHT";
        }
        retVal = "LIGHT";
        return retVal;
    };

    this.switchTheme = function(isDark){
        if (isDark != "LIGHT"){
            $("body").addClass("dark");
            localStorage.removeItem("DarkExchange");
            localStorage.setItem("DarkExchange","DARK");

        }else{
            $("body").removeClass("dark");
            localStorage.removeItem("DarkExchange");
            localStorage.setItem("DarkExchange","LIGHT");
        }
        if($('IFRAME#tradeChart').length > 0){
            $('IFRAME#tradeChart').get(0).contentDocument.location.reload();
        }
        return true;
    };

    this.fillOrderBookTable = function (data, type, table) {
        let color = "";
        if (type === "sell") {
            color = "229, 85, 65, 0.2";
        } else if (type === "buy") {
            color = "0, 206, 125, 0.2";
        } else {
            return false;
        }
        let tableData = "";
        jQuery.each(data, function (i, val) {
            tableData += "<tr data-total='"+val.totalPrice+"' data-amount='"+val.totalAmount+"' data-price='"+val.price+"'>" +
                "<td class='text-left'>" + TSite.fixZeroValues(val.price) + "</td>" +
                "<td class='text-right'>" + val.totalAmount + "</td>" +
                "<td class='text-right'>" + TSite.fixZeroValues(val.calculatedTotalAmount) + "</td>" +
                "<td class='bar'><div class='"+(type === "sell" ? "ask" : "bid")+" percent' style='width: "+val.depth+"%;'></div></td></tr>";
        });
        $("." + table).html(tableData);
    };

    this.fillOrderHistoryTable = function (data) {
        let tableData = "";
        jQuery.each(data, function (i, val) {

            if (val.color == 2) {
                style = "mh-sell";
            } else if (val.color == 1) {
                style="mh-buy";
            } else {
                style="mh-sameprice";
            }
            tableData += "<tr><td>" + val.date + "</td>" +
                "<td class='text-left "+style+"'>" + val.amount + "</td>" +
                "<td class='text-right "+style+"'>" + TSite.fixZeroValues(val.price) + "</td></tr>";
        });
        $(".marketOrderHistory").html(tableData);

    };

    this.fillCoinsTable = function (data) {
        let tableData = "";
        jQuery.each(data, function (i, val) {
            let pair = val.currency + "-" + val.coin;
            let row = $('[data-pair="'+pair+'"]');
            row.find("#lastprice").html(TSite.fixZeroValues(val.lastprice));
            row.find("#volume").html(val.volume);
            row.find("#change").html(val.change);
        });
    };

    this.updateVolumes = function (data) {
        jQuery.each(data, function (i, val) {
            $('[data-pair="'+i+'"] output').text(val);
        });
    };

    this.loadMarketData = function () {
        $("#edtAmountBuyInt").val("");
        $("#edtPriceBuy").val("");
        $("#edtSummaryBuy").val("");
        $("#edtFeeBuy").val("");
        $("#edtAmountSellInt").val("");
        $("#edtPriceSell").val("");
        $("#edtSummarySell").val("");
        $("#edtFeeSell").val("");
        $("#divFeeSell").text('-');
        $("#divFeeBuy").text('-');

        var json = {coinId: this.coinID, currencyId: this.currencyID};
        var object = this;
        $.ajax({
            type: "POST",
            url: this.MarketEndPointURI,
            data: json,
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': TSite.CSRF_TOKEN
            },
            success: function (data) {

                if(data.currency == null){
                    return;
                }

                $("#edtCoinWalletBalance").val(data.balance);
                $("#edtCurrencyWalletBalance").val(data.currencybalance);
                $(".marketBuyCurrencySymbol").html(data.currency.symbol);

                TSite.activeCoin = data.coin;
                TSite.activeCurrency = data.currency;

                TSite.fiatBalance = data.fiatbalance;
                TSite.cryptoBalance = data.balance;

                $(".userCurrentBalance").html(data.balance + " " + data.coin.symbol);
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

    };

    this.selectPair = function (pair) {
            window.location = this.pairEndPointURI + pair;
        }};

$(document).ready(function () {


    $(document).on('click touch', '.currency-tabs .tab-item', function (e) {
        var selectedCoin = $(this).data("coinid");
        $(".currency-tabs .tab-item").removeClass("active");
        $(this).addClass("active");
        TSite.selectedTabID = $(this).data("coinid");

        $(".coinsTable .pair").each(function () {
            if($(this).data("coinid") != selectedCoin) {
                $(this).hide();
            } else {
                $(this).show();
            }
        });
        e.preventDefault();
    });

    $(document).on('click', '.fav-btn.filter', function (e) {
        $(this).toggleClass("checked");
        let isChecked = $(this).hasClass("checked");
        $(".coinsTable .pair").each(function () {
            let favBtn = $(this).find(".fav-btn");
            if(!favBtn.hasClass("checked")) {
                favBtn.parent().parent().toggleClass("hidden");
            }
        });
    });

    $(document).on('click', '.coinsTable .pair .fav-btn', function (e) {
        e.stopPropagation();
        var selectedPair = $(this).parent().parent().data("pair");
        var obj = $(this);
        let defer = $.Deferred();
        $.ajax({
            type: "POST",
            url: TSite.updateFavoritePairsEndPointURI,
            data: {pair: selectedPair},
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
                if(data.action == "add") {
                    obj.addClass("checked");
                } else {
                    obj.removeClass("checked");
                }
            },
            error: function (req, status, err) {
                defer.reject(err);
                window.location.href = document.location.origin + '/login';
            },
        });
        return defer.promise();
        });


    $(document).on('click', '.coinsTable .pair', function (e) {
        let pair = $(this).data('pair');
        TSite.selectPair(pair);
        e.preventDefault();
    });

    $('.orderbook tbody').on('click', 'tr', function () {
        let total = $(this).data("total");
        let amount = $(this).data("amount");
        let price = $(this).data("price");

        let objDetect = this.parentElement.getAttribute("class").toString();

        if(objDetect==="orderbookBuyTable"){
            let fee = currency(total) * $("#coinFee").val();
            fee = Math.round(fee*Math.pow(10,4))/Math.pow(10,4)

            setVal("#edtSummarySell", total);
            setVal("#edtPriceSell", price);
            setVal("#edtPriceBuy", price);
            setVal("#edtAmountSellInt", amount);
            setVal("#edtFeeSell", fee.toFixed(4));
            if(fee>0) $("#divFeeSell").text(fee.toFixed(4));

        }else{
            let fee = currency(total) * $("#currencyFee").val();
            fee = Math.round(fee*Math.pow(10,4))/Math.pow(10,4)

            setVal("#edtSummaryBuy", total);
            setVal("#edtPriceBuy", price);
            setVal("#edtPriceSell", price);
            setVal("#edtAmountBuyInt", amount);
            setVal("#edtFeeBuy", fee.toFixed(4));
            if(fee>0) $("#divFeeBuy").text(fee.toFixed(4));
        }
    } );

    $(document).on('click', ".btnExpandTable", function (event) {
        event.preventDefault();
        let buttonObject = $(this);
        let tableObjectStr = $(this).data("object");

        let wrapper =  "#"+tableObjectStr+"_wrapper";
        let tableObject = "#"+tableObjectStr;

        $(wrapper).toggle();

        var isVisible = $( wrapper ).is( ":visible" );
        if(isVisible){
            $(buttonObject).html('<i class="fa fa-angle-double-down"></i>');
        }else{
            $(buttonObject).html('<i class="fa fa-angle-double-up"></i>');
        }




    });

    $(".orderbook-short").on('click', function (e){
        let originHeight = $(".exnce_pairs").innerHeight();
        let maxHeight =(originHeight - 81.4)+"px";
        let itemHeight =((originHeight/2) - 54)+"px";
        let bidHeight = ((originHeight - 81.4) - 54) + "px";
        let filter = $(this).data("filter");
        switch(filter){
            case 0:
                $(".orderbook-panels").css( "height", itemHeight ).removeClass("open").slideDown("slow").show();
                break;
            case 1:
                $(".orderbook-panels").first().slideUp("slow").removeClass("open");
                $(".orderbook-panels").last().css( "height", maxHeight).addClass("open").slideDown("slow").show();
                break;
            case 2:
                $(".orderbook-panels").first().css( "height", bidHeight ).addClass("open").slideDown("slow").show();
                $(".orderbook-panels").last().slideUp("slow").removeClass("open");
                break;
            default:
                $(".orderbook-panels").css( "height", itemHeight ).removeClass("open").slideDown("slow").show();
                break;
        }
    });


    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });

    var activeSlide = $('.value-list .active').index();
    $('.value-list').slick({
        infinite: true,
        centerMode: false,
        centerPadding: '40px',
        slidesToShow: 4,
        rows: 0,
        //initialSlide: activeSlide,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 360,
                settings: {
                    slidesToShow: 2
                }
            },
        ]
    });


    $('#listAirdrops tbody').on( 'click', '.btn-airdrop-details', function (event) {
        event.preventDefault();
        let airdropId = $(this).data("id");
        TSite.loadAirdropDetails(airdropId);
    });

    $('.slick-slide').on('click', function (e){
        let slideClicked = $(e.currentTarget).attr("data-slick-index");
    });

    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });


    $('#id_card_front, #id_card_back, #selfie').bind('change', function() {
        if(this.files[0].size >= 5*1024*1024) {
            this.value = this.defaultValue;
            showMessage("error", "Hata", translate["UPLOAD_ERROR"], true, translate["OK"], false, "");
        }
    });

    $(".darkTheme").click(function (e) {
        e.preventDefault();
        TSite.switchTheme("DARK");
    });

    $(".lightTheme").click(function (e) {
        e.preventDefault();
        TSite.switchTheme("LIGHT");
    });

    TSite.switchTheme(TSite.getTheme());
    TSite.initDepthChart();

    TSite.setCoinID(selectedCoin);
    TSite.setCurrencyID(selectedCurrency);
    TSite.selectedTabID = selectedCoin;

    TSite.initAirdrops();
    TSite.closeSidebar();

});


function requestCameraAccess(){
    TSite.loadWebCamera();
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    str=str.toString();
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function enforceFloat(object) {
    var valid = /^\-?\d+\.\d*$|^\-?[\d]*$/;
    var number = /\-\d+\.\d*|\-[\d]*|[\d]+\.[\d]*|[\d]+/;
    if (object.value.charAt(0) != "." && !valid.test(object.value)) {
        var n = object.value.match(number);
        object.value = n ? n[0] : '';
    }
}

function setVal(identifier, value) {
    $(identifier).val(value);
    $(identifier).attr("value",value);
}

function calculateBuy(event,control,object,istotal,isbuy){
    var decAmount = 0;// (parseInt($("#edtAmountBuyInt").val()) || 0 )+ "." + (parseInt($("#edtAmountBuyDec").val()) || 0);
    var priceBuy = 0;// parseFloat($("#edtPriceBuy").val()) || 0;
    var summary = 0;// decAmount * priceBuy;
    var feeTotal = 0;//
    var feeAmount = 0;

    if(isbuy === true){
        feeAmount = parseFloat($("#currencyFee").val()).toFixed(8);

        szAmountInt = "#edtAmountBuyInt";
        szPrice = "#edtPriceBuy";
        szSummary = "#edtSummaryBuy";
        szFee = "#edtFeeBuy";
        szDivFee = "#divFeeBuy";
    }else{
        feeAmount = parseFloat($("#coinFee").val()).toFixed(8);

        szAmountInt = "#edtAmountSellInt";
        szPrice = "#edtPriceSell";
        szSummary = "#edtSummarySell";
        szFee = "#edtFeeSell";
        szDivFee = "#divFeeSell";
    }

    if(istotal==false) {
        decAmount = (parseFloat($(szAmountInt).val()).toFixed(8) || 0);
        priceBuy = parseFloat($(szPrice).val()).toFixed(8) || 0;
        summary = parseFloat(decAmount).toFixed(8) * priceBuy;
        if(isbuy === true){ feeTotal = decAmount * feeAmount; }
        else { feeTotal = summary * feeAmount; }
        setVal(szSummary, parseFloat(summary).toFixed(8));
        console.log("DETECTED");

    } else {
        priceBuy = parseFloat($(szPrice).val()).toFixed(8) || 0;
        summary =  parseFloat($(szSummary).val()).toFixed(8);
        if(priceBuy>0.00){
            if(summary > 0){
                totalAmount = summary / priceBuy;
                let n = totalAmount;
                let str = n.toString();
                let decimalOnly = 0;
                if( str.indexOf('.') != -1 ){ //check if has decimal
                    decimalOnly = str.split('.')[1];
                }
                setVal(szAmountInt, parseFloat(Math.abs(n)).toFixed(8));
            }
        }

        decAmount = (parseFloat($(szAmountInt).val()).toFixed(8) || 0);
        if(isbuy === true){ feeTotal = decAmount * feeAmount; }
        else { feeTotal = summary * feeAmount; }
    }

    feeTotal = Math.round(feeTotal*Math.pow(10,8))/Math.pow(10,8);
    if(feeTotal>0) {
        setVal(szFee, parseFloat(feeTotal).toFixed(8));
        $(szDivFee).text(parseFloat(feeTotal).toFixed(8));
    }
}

var socket = io.connect(EXCHANGE_SOCKET_ADDRESS, {secure: true});
socket.on('connect', function () {
    TSite.setCoinID(selectedCoin);
    TSite.setCurrencyID(selectedCurrency);
    TSite.loadMarketData();
});

socket.on('message', function (message) {
    message = JSON.parse(message);

    if(message == null){
        return;
    }

    if (typeof message == 'undefined') {
        return;
    }

    if (typeof message.stats != 'undefined') {
        if (TSite.coinID == message.coin_id) {
            TSite.updateHeaderStatistics(message.stats.html);
            setTimeout(function () {
                setVal("#edtPriceSell", message.stats.price);
                setVal("#edtPriceBuy", message.stats.price);
            }, 10);
        }
    }

    if (typeof message.volumes != 'undefined') {
        TSite.updateVolumes(message.volumes);
    }

    if((TSite.coinID == message.coin.id) && (TSite.currencyID == message.currency.id)){
        dLastPrice = 0.00;
        if (typeof message.market_history != 'undefined') {
            if(message.market_history.length>0){
                dLastPrice = message.market_history[0].price;
            }
        }

        TSite.fillOrderBookTable(message.buy, "buy", "orderbookBuyTable");
        TSite.fillOrderBookTable(message.sell, "sell", "orderbookSellTable");
        TSite.fillDepthChart(message.sell,message.buy, dLastPrice);
        TSite.fillCoinsTable(message.pairsData);
        document.title = SELECTED_PAIR_INFO + " " + message.pairsData[SELECTED_PAIR_INFO].lastprice + " | " + EXCHANGE_NAME;

        $(".lastPrice .price").text(0);
        $(".lastPrice").removeClass("up down");
        if (typeof message.market_history != 'undefined') {
            TSite.fillOrderHistoryTable(message.market_history);
            if (typeof message.market_history[0] != 'undefined') {
                $(".lastPrice .price").text(message.market_history[0].price);
                if (message.market_history[0].color == 1) {
                    $(".lastPrice").addClass("up");
                } else if (message.market_history[0].color == 2) {
                    $(".lastPrice").addClass("down");
                }
            }
        }
    }

    if (typeof message.balance != 'undefined' && message.balance.token == CURRENT_USER_HASH)
    {
        if (typeof message.coin_id != 'undefined') {
            if (TSite.coinID == message.coin_id) {
                TSite.coinBalanceFloat = message.balance.coinBalanceFloat;
                TSite.currencyBalanceFloat = message.balance.currencyBalanceFloat;
                let coinSlug = message.currency.slug;
                $(".marketCoinName").html(message.coin.name);
                $(".marketCoinSymbol").html(message.currency.symbol);//coin
                $(".marketSellCoinSymbol").html(message.currency.symbol);//coin
                $(".orderbook-buy-amount-symbol").html(message.coin.symbol);
                $(".orderbook-sell-amount-symbol").html(message.coin.symbol);
                $("#edtBuyCoin").attr("value", message.coin.symbol);
                $("#edtBuyCurrency").attr("value", message.currency.symbol);
                $("#edtSellCoin").attr("value", message.coin.symbol);
                $("#edtSellCurrency").attr("value", message.currency.symbol);
                $("#coinSymbol").text(message.coin.symbol);
                $("#currencySymbol").text(message.currency.symbol);
                $(".img-coin-logo").attr("src", "/images/coins/" + coinSlug + ".svg");
                $("#coinBalance").text(message.balance.coinBalance);
                $("#currencyBalance").text(message.balance.currencyBalance);
                $(".userCurrentBalance").text(message.balance.coinBalance);
                $('#lstOpenOrders').DataTable().ajax.reload();
                $('#lstOrderHistory').DataTable().ajax.reload();
            }
        }
    }
});