 var TPersonal = new function () {
    this.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    this.createWalletEndPointURI = "/personal/wallet/create";
    this.getNewAddressEndPointURI = "/personal/wallet/get-new-address";
    this.walletListEndPointURI = "/personal/wallet/load-wallets";

    // this.MarketEndPointURI = "/market/load";
    // this.pairEndPointURI = "/market/";

    this.MarketEndPointURI = "/personal/market/load";
    this.IEOEndPointURI = "/personal/ieo/load";
    this.pairEndPointURI = "/personal/market/";
    this.pairIEOEndPointURI = "/personal/ieo/";

    this.loadDepositHistoryEndPointURI = "/personal/deposit/load-withdraw-requests";
    this.createDepositEndPointURI = "/personal/deposit/create-deposit-request";
    this.deleteDepositEndPointURI = "/personal/deposit/delete-deposit-request";
    this.verifyWithdrawEndPointURI = "/personal/withdraw/verification";
    this.createWithdrawEndPointURI = "/personal/withdraw/create";
    this.cancelWithdrawEndPointURI = "/personal/withdraw/cancel";
    this.deleteBankAccountEndPointURI = "/personal/delete-bank-account";
    this.loadSupportRequestsEndPointURI = "/personal/support/get";
    this.createSupportRequestEndPointURI = "/personal/support/save";
    this.withdrawURL = "/personal/personal/withdraw";
    this.airdropDetailsEndpointURI = "/personal/airdrops/details/";
    this.airdropDetailsRatingEndpointURI = "/personal/airdrops/update/ratings";
    this.airdropDetailsFollowEndpointURI = "/personal/airdrops/update/follow";


    this.COIN_DECIMAL = 8;
    this.CURRENCY_DECIMAL = 8;

    this.coinBalanceFloat = 0;
    this.currencyBalanceFloat = 0;
    this.coinID = -1;
    this.fiatBalance = 0.00;
    this.cryptoBalance = 0.00;

    this.activeCurrency = null;
    this.activeCoin = null;

    this.orderBook = null;
    this.depthChart = null;

    this.selectedTabID = 0;

    this.createBuyTransactionEndPointURI = "/personal/market/buy";
    this.createSellTransactionEndPointURI = "/personal/market/sell";
    this.cancelTransactionEndPointURI = "/personal/market/cancel";

     this.createIEOBuyTransactionEndPointURI = "/personal/ieo/buy";
     this.createIEOSellTransactionEndPointURI = "/personal/ieo/sell";

    this.openOrdersEndPointURI  ="/personal/market/open";
    this.orderHistoryEndPointURI  ="/personal/market/completed";
    this.marketHistoryEndPointURI = "/personal/market/history";

    this.updateFavoritePairsEndPointURI = "/personal/market/fav";

    this.settingsLogsEndPointURI = "/personal/account/logs/get";
    this.userOrderHistoryEndPointURI = "/personal/account/orders/get";
    this.settingsAPIKeysEndPointURI = "/personal/account/apis/get";
    this.createAPIKeyEndPointURI = "/personal/account/apis/create";
    this.storeAPIKeyEndPointURI = "/personal/account/apis/store";

    this.settingsReferralsEndpointURI = "/personal/account/referrals/get"


    this.enableAuthenticatorEndPointURI = "/authenticator/enable";
    this.disableAuthenticatorEndPointURI = "/authenticator/disable";
    this.enableSMS2FAEndPointURI = "/sms2fa/enable";

    this.listWithdrawRequestsEndPointURI = "/personal/withdraw/list-withdraw-requests";
    this.deleteWithdrawRequestEndPointURI = "/personal/withdraw/delete-withdraw-request";


     this.closeSidebar = function () {
         $(".exnce_pairs").click();
     };


     this.fixZeroValues = function(Amount) {

        if(Amount===null){
            return "";
        }

        var szDecimalData = "";
        var szDecimalPlaces = "";
        var szIntPlaces = "";
        var szAmountVal = Amount + "";
        var checkDecimalPlaces = szAmountVal.match(/\./);
        var getZeroNumbers = szAmountVal.match(/(0+)$/g);
        var szData =  `<span class='fade-price'>${szAmountVal}</span>`;
        if (checkDecimalPlaces && getZeroNumbers) {
            [szIntPlaces, szDecimalPlaces] = szAmountVal.split('.');
            szDecimalData = szDecimalPlaces.replace(/(0+)$/g, '');
            szData = `<span class='fade-price'>${szIntPlaces}.${szDecimalData}`;
            if (getZeroNumbers) {
                szData += `<span class='fade-number'>${getZeroNumbers[0]}</span>`;
            }
            szData += `</span>`;
        }
        return szData;
    };

    this.setCoinID = function (coinID) {
        this.coinID = coinID;
    };

    this.setCurrencyID = function (currencyID) {
        this.currencyID = currencyID;
    };



     this.initAirdrops = function(){
         $('#listAirdrops').DataTable({
             "stateSave": true,
             "fixedColumns": true,
             "paging": true,
             "bLengthChange": true,
             "bFilter": true,
             "bInfo": false,
             "ordering": false,
             "autoWidth": false,
             responsive: {
                 details: {
                     type: 'column',
                     target: -1
                 }
             },
             "columnDefs": [
                 {
                     "width" : "5%",
                     className: 'control',
                     orderable: false,
                     targets:   -1
                 },
                 { "width": "10%", "targets": 0, "className": "text-left"},
                 { "width": "15%", "targets": 1, "className": "text-left"},
                 { "width": "15%", "targets": 2, "className": "text-left"},
                 { "width": "15%", "targets": 3, "className": "text-left"},
                 { "width": "15%", "targets": 4, "className": "text-left"},
                 { "width": "20%", "targets": 5, "className": "text-left"},
                 { "width": "20%", "targets": 6, "className": "text-left"}
             ],
         });
     };


     this.updateRatingValue= function(rating,airdropId){
        var json = {_token: this.CSRF_TOKEN, rating:rating, airdropId: airdropId};

        $.ajax({
            type: "POST",
            url: TPersonal.airdropDetailsRatingEndpointURI,
            data: json,
            success: function (data) {
                if(data.success == true){
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                    $('#apiList').DataTable().ajax.reload();
                } else {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };

    this.updateFollow= function(follow,airdropId){
        var json = {_token: this.CSRF_TOKEN,follow:follow, airdropId: airdropId};

        $.ajax({
            type: "POST",
            url: TPersonal.airdropDetailsFollowEndpointURI,
            data: json,
            success: function (data) {
                if(data.success == true){
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                    $('#apiList').DataTable().ajax.reload();
                } else {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };



    this.loadAirdropDetails = function(airdropId){
        var json = {_token: this.CSRF_TOKEN, airdropId: airdropId};
        $.ajax({
            type: "GET",
            url: this.airdropDetailsEndpointURI + airdropId,
            data: json,
            success: function (data) {
                $(".airdrop-details").html(data);
                $("#frmAirdropDetails").modal().show();
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };


    this.updateHeaderStatistics = function (data) {
        jQuery.each(data, function (i, val) {
            $("#"+i+"-info").html(val.data);
            $("#"+i+"-symbol").html(val.symbol);
            $("#"+i+"-info").removeClass();
            $("#"+i+"-info").addClass(val.color);
            $("#"+i+"-symbol").removeClass();
            $("#"+i+"-symbol").addClass(val.color);
        });
    };

    $.fn.dataTable.ext.errMode = 'throw';

    this.chartColours = function(colorName){
       var colorList = {}
        if(this.getTheme() == "LIGHT"){
            colorList["BACKGROUND"] = "#ffffff";
            colorList["GRID"] = "#3a4c59";
            colorList["TEXT"] = "#848e9c";
        }else{
            colorList["BACKGROUND"] = "#293742";
            colorList["GRID"] = "#49555e";
            colorList["TEXT"] = "#848e9c";
        }
        return colorList[colorName];
    };


    this.fillDepthChart = function (sellData, buyData,actualPrice) {
       // return false;

        this.depthChart = $('#depthchartcontainer').highcharts();

        var bidList = [];
        var askList = [];

        var askDepthTotal = 0;
        var bidDepthTotal = 0;

        var askListEx = [];
        var bidListEx = [];
        var i = 0;



        jQuery.each(buyData, function (i, val) {
            bidDepthTotal += parseFloat(val.amount);
             var singleBid = {x:parseFloat(val.price), y:bidDepthTotal};
            bidList.push(singleBid);
        });

        jQuery.each(sellData, function (i, val) {
            var singleAsk = {price:val.price, amount: val.amount};
            askListEx.push(singleAsk);
        });
        askListEx = askListEx.reverse();

        jQuery.each(askListEx, function (i, val) {
            askDepthTotal += parseFloat(val.amount);
            var singleAsk = {x:parseFloat(val.price), y:askDepthTotal};
            askList.push(singleAsk);
        });

        this.orderBook={bids: bidList,asks:askList,bid:bidDepthTotal, ask:askDepthTotal};

    };
    this.initDepthChart = function (chartTitle, bids,asks) {
        let tradeFormHeight = $(".tradeForms").height();
        let totalHeight = $(".exnce_pairs").height();
        let headerHeight = 40;

        $('#depthchartcontainer').highcharts({
            chart: {
                marginTop:25,
                marginLeft: 25,
                marginRight: 25,
                marginBottom: 25,
                backgroundColor: "#0D131E",
                type: 'area',
                zoomType: 'xy',
                height: totalHeight-tradeFormHeight-headerHeight,
                styledMode: true,
                reflow: true,
                events: {
                    load: function() {


                        refreshInterval = setInterval(function () {
                            if ((null != TPersonal.orderBook) && (TPersonal.orderBook.asks.length > 0) && (TPersonal.orderBook.bids.length > 0)) {
                                 TPersonal.depthChart.series[0].setData(TPersonal.orderBook.bids);
                                 TPersonal.depthChart.series[1].setData(TPersonal.orderBook.asks);
                            }
                        }, 1000)
                    }
                },

            },
            credits: {
                enabled: false
            },
            title: {
                text: chartTitle
            },



            xAxis: {
                className: 'mh-buy',
                minPadding: 0,
                maxPadding: 0,
                type: "linear",
                labels: {
                    align: 'left',
                    x: 8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    }
                }
            },
            yAxis: [{
                className: 'mh-sell',
                lineWidth: 0,
                gridLineWidth: 0,
                title: null,
                tickWidth: 1,
                tickLength: 5,
                tickPosition: "inside",
                type: "linear",
                labels: {
                    align: 'left',
                    x: 8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    },
                }
            }, {
                opposite: !0,
                linkedTo: 0,
                lineWidth: 1,
                gridLineWidth: 0,
                title: null,
                tickWidth: 1,
                tickLength: 5,
                tickPosition: "inside",
                labels: {
                    align: "right",
                    x: -8,
                    formatter: function () {
                        return parseFloat(this.value).toFixed(8);
                    }
                }
            }

            ],
            legend: {
                enabled: !1
            },
            plotOptions: {
                area: {
                    fillOpacity: .5,
                    lineWidth: 1,
                    step: "center",
                    marker: {
                        enabled: !1
                    }
                }
            },
            tooltip: {
                headerFormat: "<span style='font-size=10px;'>" + translate["DEPTH_CHART_PRICE"]+ ": {point.key}</span><br/>",

                crosshairs: !0,
                valueDecimals: 8,
            },
            series: [ {
                type: "area",
                name: translate["DEPTH_CHART_SELL"],
                data: [{
                    x: 0,
                    y: 0
                }],
                color: "#ea0070"
            },{
                type: "area",
                name: translate["DEPTH_CHART_BUY"],
                data: [{
                    x: 0,
                    y: 0
                }],
                color: "#70a800"
            }]
        });
    };

    this.loadWebCamera = function () {
        var webcamVideoPanel = $("#webcamVideoPanel");
        if (webcamVideoPanel.length > 0) {
            var canvas = document.getElementById('canvas');
            var context = canvas.getContext('2d');
             var mediaConfig =  { video: true };
            var errBack = function(e) {
                console.log('An error has occurred!', e);
            };
            if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                navigator.mediaDevices.getUserMedia(mediaConfig).then(function(stream) {
                    //video.src = window.URL.createObjectURL(stream);
                    webcamVideoPanel.srcObject = stream;
                    webcamVideoPanel.play();
                });
            }
        }
    };

    this.getTheme = function(){
        let retVal = "LIGHT";
        if(localStorage.getItem("DarkExchange") !== null){
            retVal = localStorage.getItem("DarkExchange");
        }else{
            retVal = "LIGHT";
        }
        return retVal;
    };

    this.switchTheme = function(isDark){
        if (isDark != "LIGHT"){
            $("body").addClass("dark");
            localStorage.removeItem("DarkExchange");
            localStorage.setItem("DarkExchange","DARK");
        }else{
            $("body").removeClass("dark");
            localStorage.removeItem("DarkExchange");
            localStorage.setItem("DarkExchange","LIGHT");
        }
        if($('IFRAME#tradeChart').length > 0){
            $('IFRAME#tradeChart').get(0).contentDocument.location.reload();
        }
        return true;
    };

    this.loadSupportRequests = function () {
        var supportTable = $('#lstTickets').DataTable({
            "processing": true,	language: { search: "", "decimal":",",
                "thousands": "."
            },
            "serverSide": true,
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "paging": true,
            "info": false,
            "bSort" : true,
            "stateSave": true,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "lengthMenu": [[10, 15, 30, 50,70,100, -1], [10, 15, 30, 50,70,100, translate["ALL"]]],
            fixedHeader: true,
            "ajax": {
                "type": "POST",
                "url": TPersonal.loadSupportRequestsEndPointURI,
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": false,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                },
                "data" : function(){
                    return JSON.stringify({"is_completed" : ListCompletedSupportRequests});
                }
            },
            "rowCallback": function( row, data, index ) {
                    $(row).find('td:eq(1)').css('color', data.color);
                    $(row).find('td:eq(0)').html("<a href='"+data.detail+"'>"+data.subject+"</a>");
            },
            columnDefs: [ {
                "width" : "5%",
                className: 'control',
                orderable: false,
                targets:   -1
            },
                { "width": "25%", "targets": 0 },
                { "width": "10%", "targets": 1 },
                { "width": "10%", "targets": 2 },
                { "width": "25%", "targets": 3},
            ],
            "columns": [
                {"data": "subject",responsivePriority: 1},
                {"data": "status",responsivePriority: 2},
                {"data": "date",responsivePriority: 3},
                {"data": "agent",className:'text-right'
                },
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.loadAPIKeys = function () {
        otable = $('#apiList').DataTable({
            "processing": true,
            "serverSide": false,
            "stateSave": true,
            "fixedColumns": true,
            "paging": true,
            "bFilter": true,
            "bInfo": true,
            "ordering": false,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },

            "ajax": {
                "type": "POST",
                "url": TPersonal.settingsAPIKeysEndPointURI,
                "data": function (json) {
                    return JSON.stringify({"coin_id": TPersonal.coinID});
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": false,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "columnDefs": [

                { "width": "75%", "targets": 0,"className": "text-left"},
                { "width": "15%", "targets": 1 },
                {  "width": "20%","targets": 2,"display":"none" },
                {  "width": "20%","targets": 3,"display": "none"},
                {  "width": "20%","targets": 4,"display": "none"},
                {  "width": "20%","targets": 5,"display": "none"},
                {
                    "width":"10%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                }
            ],
            "columns": [
                {"data": "secret_key",responsivePriority: 0},
                {
                    "data": "id",responsivePriority: 0, render: function (data, type, full, meta) {

                        let buttonGroup = '<div class="btn-group btn-group-sm btn-block" role="group">'+
                        '<button data-action="view" class="btn btn-light">'+
                        '<i class="fa fa-eye text-success"></i></button>'+
                        '<button data-action="update" class="btn btn-light">'+
                        '<i class="fa fa-edit text-primary"></i></button>'+
                            '<button data-action="stats" class="btn btn-light hidden">'+
                            '<i class="fa fa-pie-chart text-info"></i></button>'+
                        '<button data-action="delete" class="btn btn-light">'+
                        '<i class="fa fa-trash text-danger"></i></button>'+
                        '</div>';
                        return buttonGroup;
                    }
                },
                {"data": "can_access_user_info",responsivePriority:5,render:function (data,type,full,meta) {
                    let szData = "";
                    if(data==false){
                        szData = '<span class="pull-right text-danger pr-2"><i class="fa fa-times"></i> </span>';
                    }else{
                        szData = '<span class="pull-right text-success pr-2"><i class="fa fa-check"></i> </span>';
                    }
                    return szData;
                    }},
                {"data": "can_access_balance_info",responsivePriority:5,render:function (data,type,full,meta) {
                        let szData = "";
                        if(data==false){
                            szData = '<span class="pull-right text-danger pr-2"><i class="fa fa-times"></i> </span>';
                        }else{
                            szData = '<span class="pull-right text-success pr-2"><i class="fa fa-check"></i> </span>';
                        }
                        return szData;
                    }},
                {"data": "can_create_order",responsivePriority:5,render:function (data,type,full,meta) {
                        let szData = "";
                        if(data==false){
                            szData = '<span class="pull-right text-danger pr-2"><i class="fa fa-times"></i> </span>';
                        }else{
                            szData = '<span class="pull-right text-success pr-2"><i class="fa fa-check"></i> </span>';
                        }
                        return szData;
                    }},
                {"data": "can_create_withdraw",responsivePriority:5,render:function (data,type,full,meta) {
                        let szData = "";
                        if(data==false){
                            szData = '<span class="pull-right text-danger pr-2"><i class="fa fa-times"></i> </span>';
                        }else{
                            szData = '<span class="pull-right text-success pr-2"><i class="fa fa-check"></i> </span>';
                        }
                        return szData;
                    }},
                {render: function () {
                        return "";
                    }}
            ]
        });

        $('#apiList tbody').on( 'click', 'button', function () {
            var data = otable.row( $(this).parents('tr') ).data();

            $("#edtPublicKey").val(data.public_key);
            $("#edtSecretKey").val(data.secret_key);

            $(".api-permission").each(function(key,obj){
                let permName = $(obj).data("permission");
                let objName = "#"+permName;
                $(objName).prop("checked",data[permName]);
            });

            let tag = $(this).data("action");
            let title = "";
            switch (tag){
                case "view":
                       title = translate["DISPLAY_API_KEY_ENTRY"];
                       $(".edtActionType").val("view");
                       $(".btnUpdateApiKeyEntry").hide();
                       $(".btnSaveApiKeyEntry").hide();
                       $(".btnCreateApiKeyEntry").hide();
                       $(".btnDeleteApiKeyEntry").hide();
                    break;
                case "delete":
                        title = translate["DELETE_API_KEY_ENTRY"];
                        $(".edtActionType").val("delete");
                        $(".btnUpdateApiKeyEntry").hide();
                        $(".btnSaveApiKeyEntry").hide();
                        $(".btnCreateApiKeyEntry").hide();
                        $(".btnDeleteApiKeyEntry").show();
                    break;
                case "update":
                        title = translate["UPDATE_API_KEY_ENTRY"];
                        $(".edtActionType").val("update");
                        $(".btnUpdateApiKeyEntry").show();
                        $(".btnSaveApiKeyEntry").hide();
                        $(".btnCreateApiKeyEntry").hide();
                        $(".btnDeleteApiKeyEntry").hide();
                    break;
                case "stats":
                    break;
                default:
                    break;
            }
            $("#apiEntryTitle").text(title);
            $(".edtPermissionList").slideDown();
            $('#frmAddNewAPIKey').modal('toggle');
        } );
    };

    this.loadReferrals = function () {
        var referralTable = $('#referralList').DataTable({
            "processing": true,	language: { search: "", "decimal":",",
                "thousands": "."
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "paging": true,
            "info": false,
            "bSort" : true,
            "serverSide": true,
            "stateSave": true,
            "fixedColumns": true,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "lengthMenu": [[10, 15, 30, 50,70,100, -1], [10, 15, 30, 50,70,100, translate["ALL"]]],
            fixedHeader: true,
            "ajax": {
                "type": "POST",
                "url": TPersonal.settingsReferralsEndpointURI,
                "data": function (json) {
                    return JSON.stringify(json);
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            columnDefs: [ {
                className: 'control',
                orderable: false,
                targets:   -1
            },
                { "width": "25%", "targets": 0 },
                { "width": "35%", "targets": 1 },
                { "width": "15%", "targets": 2 },
                { "width": "25%", "targets": 3},
            ],
            "columns": [
                {"data": "name",responsivePriority: 1},
                {"data": "email",responsivePriority: 2},
                {"data": "date",responsivePriority: 3},
                {"data": "referral_fee",responsivePriority: 4,className:'text-right'
                },
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.initWallet = function () {


            var walletTable = $('#walletTable').DataTable({
                "processing": true,	language: { search: "", "decimal":",",
                    "thousands": "."
                },

                "ajax": {
                    "type": "POST",
                    "url": TPersonal.walletListEndPointURI,
                    "data": function (json) {
                        return JSON.stringify({"coin_id": TPersonal.coinID});
                    },
                    "dataSrc": function ( val ) {

                        var fullData = val.data;
                        var chartData=[];
                         for ( var i=0, ien=fullData.length ; i<ien ; i++ ) {
                             let obj = fullData[i];
                             if ((obj.can_create == false) & (obj.fixedBalance>0)){

                            chartData.push({name:obj.coin_symbol,
                                                                x:parseFloat(obj.processingBalance),
                                                                y:parseFloat(obj.fixedBalance),
                                                                z:parseFloat(obj.balance),
                                                                dx:obj.balance,
                                                                dy:obj.processingBalance,
                                                                dz:obj.fixedBalance});
                             }
                        }

                         $('#walletChartPanel').highcharts({
                            chart: {
                                type: 'variablepie'
                            },
                            title: {
                                text: ''
                            },
                            credits: {
                                enabled: false
                            },
                            tooltip: {
                                headerFormat: '',
                                pointFormat: '<span style="color:{point.color}">\u25CF</span> <b> {point.name}</b><br/>' +
                                translate["AVAIBLE_BALANCE"]+': <b> {point.dx} </b><br/>' +
                                translate["USED_BALANCE"]+': <b>{point.dy}</b><br/>' +
                                translate["TOTAL_BALANCE"]+': <b>{point.dz}</b><br/>'
                            },
                            series: [{
                                minPointSize: 0,
                                innerSize: '50%',
                                zMin: 0,
                                name: 'currencies',
                                data: chartData

                            }]
                        });




                        return val.data;
                    },
                    "contentType": "application/json; charset=utf-8",
                    "dataType": "json",
                    "processData": false,
                    "headers": {
                        'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                    }
                },

                "language": {
                    url: '/localization/'+localeCode+'.json'
                },
                "paging": false,
                "info": false,
                "bSort" : true,
                "stateSave": true,
                responsive: {
                    details: {
                        type: 'column',
                        target: -1
                    }
                },
                "lengthMenu": [[10, 15, 30, 50,70,100, -1], [10, 15, 30, 50,70,100, translate["ALL"]]],
                fixedHeader: true,
                columnDefs: [ {
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                    { "width": "20%", "targets": 0 },
                    { "width": "25%", "targets": 1,className:"text-left vertical-middle" },
                    { "width": "25%", "targets": 2,className:"text-left vertical-middle"  },
                    { "width": "25%", "targets": 3,className:"text-left vertical-middle"  },
                    { "width": "15%", "targets": 4,className:"text-right vertical-middle"  },
                    {
                        orderable: false,
                        targets:   5,
                        "width": "10%",
                        className: 'dt-body-right'
                    }],
                "columns": [
                    {"data": "coin",responsivePriority: 1,render: function (data, type, full, meta) {
                        szData='<div class="kt-coin-card-v2">\n' +
'<div class="kt-coin-card-v2__pic">\n' +
'<img width="32.5px" height="32.5px" src="'+full.coin_icon+'">\n' +
'</div>\n' +
'<div class="kt-coin-card-v2__details"><a class="kt-coin-card-v2__name" href="#">' +
 full.coin_symbol +
'</a> <span class="kt-coin-card-v2__email">'+full.coin+'</span>' +
'</div>\n' +
'</div>';
                            return szData;
                        }},
                    {"data": "balance",responsivePriority: 5, render:function(data,type,full,meta){
                        return TPersonal.fixZeroValues(full.balance);
                        }},
                    {"data": "processingBalance",responsivePriority: 3, render:function(data,type,full,meta){
                            return TPersonal.fixZeroValues(full.processingBalance);
                        }},
                    {"data": "fixedBalance",responsivePriority: 2, render:function(data,type,full,meta){
                            return TPersonal.fixZeroValues(full.fixedBalance);
                        }},
                    {"data": "action",responsivePriority: 2,render:function(data,type,full,meta){

                        if(full.can_create == true){
                            var data_str = encodeURIComponent(JSON.stringify(full));
                            return '<div class="progressPanel btn-group btn-sm" role="group">' +
                                '<button class="createWalletBtn btn btn-light btn-sm" data-info="'+data_str+'">' +
                                '<i class="fas fa-wallet text-warning"></i> '+full.action.create.title+'</button>' +
                                '</div>'
                        }else{

return '<div class="btn-group btn-sm" role="group">' +
    '<a href="'+full.action.deposit.url+'"' +
    'class="btn btn-light btn-sm">\n' +
    '<i class="fas fa-download text-success"></i> '+full.action.deposit.title+'</a>' +
    '<a href="'+full.action.withdraw.url+'"' +
    'class="btn btn-light btn-sm">\n' +
    '<i class="fas fa-upload text-danger"></i> '+full.action.withdraw.title+'</a>' +
    '</div>'
                        }

                        }},
                    {render: function () {
                            return "";
                        }}
                ]
            });











    };

    this.loadUserActivityLogs = function () {
        otable = $('#listLogs').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": false,
            "fixedColumns": true,
            "paging": true,
            "bFilter": true,
            "bInfo": true,
            "ordering": true,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "ajax": {
                "type": "POST",
                "url": TPersonal.settingsLogsEndPointURI,
                "data": function (json) {
                    return JSON.stringify(json);
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "columnDefs": [
                {
                    "width": "5%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "20%", "targets": 0 },
                { "width": "20%", "targets": 1 },
                { "width": "50%", "targets": 2,"className": "text-left"},
                { "width": "50%", "targets": 3,"className": "text-left"},

            ],
            "columns": [
                {"data": "date",responsivePriority: 0},
                {"data": "ip",responsivePriority: 1},
                {"data": "platform",responsivePriority: 2},
                {"data": "description",responsivePriority: 3},
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.loadOpenedOrders = function () {
        let inHome = CURRENT_SEGMENT == "market";

        if ($.fn.DataTable.isDataTable( '#lstOpenOrders' ) ) {
            $("#lstOpenOrders").DataTable().ajax.reload();
            return false;
        }


      $('#lstOpenOrders').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "fixedColumns": true,
             paging: true,
             searching: !inHome,
            "bLengthChange": !inHome,
            "bFilter": !inHome,
            "bInfo": false,
            "ordering": false,
            "autoWidth": false,
            "lengthMenu": [[5,10, 15, 30, 50,70,100, -1], [5,10, 15, 30, 50,70,100, translate["ALL"]]],
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "ajax": {
                "type": "POST",
                "url": TPersonal.openOrdersEndPointURI,
                "data": function (json) {
                    json.coin_id =  TPersonal.coinID;
                    json.currency_id = TPersonal.currencyID;
                    json.limit = inHome?10:-1;
                    return JSON.stringify(json);
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "rowCallback": function( row, data, index ) {
                if(data.style=="sell"){
                    $(row).find('td:eq(0)').css('color', "rgb(234, 0, 112)");
                }else{
                    $(row).find('td:eq(0)').css('color', "rgba(0,150,70,1)");
                }
            },
            "columnDefs": [
                {
                    "width": "5%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "10%", "targets": 0, "className": "text-left",orderable: true},
                { "width": "10%", "targets": 1, "className": "text-left",orderable: true},
                { "width": "15%", "targets": 2, "className": "text-center",orderable: true},
                { "width": "15%", "targets": 3, "className": "text-center",orderable: true},
                { "width": "15%", "targets": 4, "className": "text-right",orderable: true},
                { "width": "15%", "targets": 5, "className": "text-right",orderable: true},
                { "width": "15%", "targets": 6, "className": "text-right",orderable: true},
                { "width": "5%", "targets": 7, "className": "text-center",orderable: true},
            ],
            "columns": [
                {"data": "type",responsivePriority: 0},
                {"data": "symbol",responsivePriority: 1},
                {"data": "date",responsivePriority: 7},
                {"data": "price",responsivePriority: 3},
                {"data": "amount",responsivePriority: 2},
                {"data": "summary",responsivePriority: 5},
                {"data": "completed",responsivePriority: 6},
                {"data": "recid",responsivePriority: 7, render: function (data, type, full, meta) {
                        return "<a class='btnMarketCancelOrder' data-id='"+data+"'><i class='fas fa-trash-alt text-danger btnMarketCancelOrder'></i></a>";
                    }
                },
                {"data": "id",responsivePriority: 0},
            ]
        });
    };

    this.loadUserOrderHistory = function () { //Hesap hareketleri
        orderHistory = $('#accountLogs').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "fixedColumns": true,
            "paging": true,
            "bLengthChange": false,
            "bInfo": true,
            "lengthMenu": [[3,5,10, 15, 30, 50,70,100, -1], [3,5,10, 15, 30, 50,70,100, translate["ALL"]]],
            "ordering": true,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "ajax": {
                "type": "POST",
                "url": TPersonal.userOrderHistoryEndPointURI,
                "data": function (json) {
                    return JSON.stringify(json);
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "columnDefs": [
                {
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "15%", "targets": 0, "className": "text-left"},
                { "width": "15%", "targets": 1, "className": "text-left"},
                { "width": "20%", "targets": 2, "className": "text-left"},
                { "width": "15%", "targets": 3, "className": "text-right"},
                { "width": "15%", "targets": 4, "className": "text-right"},
                { "width": "10%", "targets": 4, "className": "text-left"},
                { "width": "25%", "targets": 5, "className": "text-left"},
            ],
            "columns": [
                {"data": "date",responsivePriority: 2},
                {"data": "type",responsivePriority: 1},
                {"data": "currency",responsivePriority: 1},
                {"data": "amount",responsivePriority: 3},
                {"data": "fee",responsivePriority: 3},
                {"data": "status",responsivePriority: 3},
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.loadOrderHistory = function () {
        let inHome = CURRENT_SEGMENT == "market";


        if ($.fn.DataTable.isDataTable( '#lstOrderHistory' ) ) {
            $("#lstOrderHistory").DataTable().ajax.reload();
            return false;
        }


        orderHistory = $('#lstOrderHistory').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "lengthMenu": [[5,10, 15, 30, 50,70,100, -1], [5,10, 15, 30, 50,70,100, translate["ALL"]]],
            "fixedColumns": false,
            "paging": true,
            "bLengthChange": !inHome,
            "bFilter": !inHome,
            "bInfo": !inHome,
            "ordering": false,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },

            "ajax": {
                "type": "POST",
                "url": TPersonal.orderHistoryEndPointURI,
                "data": function (json) {
                    json.coin_id=TPersonal.coinID;
                    json.currency_id=TPersonal.currencyID;
                    json.limit=inHome?10:-1;
                    return JSON.stringify(json);
                },
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "rowCallback": function( row, data, index ) {
                if(data.style=="sell"){
                    $(row).find('td:eq(0)').css('color', "rgb(234, 0, 112)");
                }else{
                    $(row).find('td:eq(0)').css('color', "rgba(0,150,70,1)");
                }
            },
            "columnDefs": [
                {
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "5%", "targets": 7, "className": "text-center"},
                { "width": "10%", "targets": 0, "className": "text-left"},
                { "width": "10%", "targets": 1, "className": "text-left"},
                { "width": "10%", "targets": 2, "className": "text-right"},
                { "width": "15%", "targets": 3, "className": "text-right"},
                { "width": "15%", "targets": 4, "className": "text-right"},
                { "width": "15%", "targets": 5, "className": "text-right"},
                { "width": "15%", "targets": 6, "className": "text-right"},
            ],
            "columns": [

                {"data": "type",responsivePriority: 2},
                {"data": "symbol",responsivePriority: 1},
                {"data": "date",responsivePriority: 7},
                {"data": "price",responsivePriority: 3},
                {"data": "amount",responsivePriority: 2},
                {"data": "summary",responsivePriority: 3},
                {"data": "fee",responsivePriority: 3},
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.loadDepositHistory = function () {


        otable = $('#lstDepositRequests').DataTable({
            "processing": false,
            "serverSide": false,
            "stateSave": false,
            "fixedColumns": true,
            "paging": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": false,
            "ordering": false,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "ajax": {
                "type": "POST",
                "url": TPersonal.loadDepositHistoryEndPointURI,
                "contentType": "application/json; charset=utf-8",
                "dataType": "json",
                "processData": false,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "columnDefs": [
                {
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "10%", "targets": 0, "className": "text-left"},
                { "width": "30%", "targets": 1, "className": "text-left"},
                { "width": "10%", "targets": 2, "className": "text-left"},
                { "width": "10%", "targets": 3, "className": "text-left"},
                { "width": "10%", "targets": 4, "className": "text-left"},
                { "width": "5%", "targets": 5, "className": "text-left"},
                { "width": "5%", "targets": 6, "className": "text-left"},
            ],
            "columns": [
                {"data": "date",responsivePriority: 0},
                {"data": "coin",responsivePriority: 1},
                {"data": "amount",responsivePriority: 2},
                {"data": "status",responsivePriority: 3},
                {
                    "data": "txinfo",responsivePriority: 4, render: function (data) {
                        if(data) {
                            if(data.txid != null) {
                                return "<a href='"+data.explorer+"' target='_blank'><small>"+data.txid+"</small></a>";
                            } else {
                                return "-";
                            }
                        } else {
                            return "-";
                        }
                    }
                },
                {"data":"txinfo",render: function (data) {
                        return "<button class='btn btn-success js-tooltip copy-txid'  data-toggle='tooltip' data-placement='bottom' title='"+translate["COPY_TO_CLIPBOARD"]+"' data-txid='"+data.txid+"'>"+translate["COPY"]+"</button>";
                    }},
                {render: function () {
                        return "";
                    }}
            ]
        });
    };


    this.loadWithdrawRequests = function () {
        let inHome = CURRENT_SEGMENT === "market";
        otable = $('#lstWithdrawRequests').DataTable({
            "processing": true,
            "serverSide": true,
            "stateSave": true,
            "fixedColumns": true,
            "paging": true,
            "bLengthChange": true,
            "bFilter": true,
            "bInfo": false,
            "ordering": false,
            "autoWidth": false,
            responsive: {
                details: {
                    type: 'column',
                    target: -1
                }
            },
            "language": {
                url: '/localization/'+localeCode+'.json'
            },
            "ajax": {
                "type": "POST",
                "url": TPersonal.listWithdrawRequestsEndPointURI,
                "contentType": "application/json; charset=utf-8",
                "data": function (d) {
                    return JSON.stringify(d);
                },
                "dataType": "json",
                "processData": true,
                "headers": {
                    'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
                }
            },
            "columnDefs": [
                {
                    "width" : "5%",
                    className: 'control',
                    orderable: false,
                    targets:   -1
                },
                { "width": "10%", "targets": 0, "className": "text-left"},
                { "width": "20%", "targets": 1, "className": "text-left"},
                { "width": "15%", "targets": 2, "className": "text-left"},
                { "width": "50%", "targets": 3, "className": "text-left"},
                { "width": "10%", "targets": 4, "className": "text-left"},
                { "width": "10%", "targets": 5, "className": "text-left"},
                { "width": "10%", "targets": 6, "className": "text-left"},
                { "width": "5%", "targets": 7, "className": "text-left"},
                { "width": "5%", "targets": 8, "className": "text-left"},

            ],
            "columns": [
                {"data": "coin",responsivePriority: 1},
                {"data": "amount",responsivePriority: 2},
                {"data": "date",responsivePriority: 3},
                {"data": "status",responsivePriority: 10},
                {
                    "data": "txid",responsivePriority: 11, render: function (data) {
                        if(data) {
                            if(data.txid != null) {
                                return "<i class='fa fa-copy js-tooltip copy-txid' data-toggle='tooltip' data-placement='bottom' title='"+translate["COPY_TO_CLIPBOARD"]+"' data-txid='"+data.txid+"'></i> " +
                                    "<a href='"+data.explorer+"' target='_blank'><small>"+data.txid+"</small></a>";
                            } else {
                                return "-";
                            }
                        } else {
                            return "-";
                        }
                    }
                },
                {"data": "ref",responsivePriority: 6},
                {
                    "data": "recid",responsivePriority: 7, render: function (data, type, full, meta) {
                        //return "<button type='button' data-id='"+data+"' class='btn btnMarketCancelOrder btn-warning rounded-soft btn-sm btn-block m-0'>İptal</button>";
                        if(data.status < 4) {
                            return "<a class='btnDeleteWithdrawRequest btn btn-sm btn-danger text-white' data-withdraw='"+data.id+"' data-status='"+data.status+"'><i class='far fa-trash-alt'></i> "+translate["CANCEL_WITHDRAW_REQUEST"]+"</a>";
                        } else {
                            return "<i class='fa fa-ban'></i>";
                        }
                    }
                },
                {"data": "description",responsivePriority: 15},
                {render: function () {
                        return "";
                    }}
            ]
        });
    };

    this.deleteWithdrawRequest = function (id) {
        let defer = $.Deferred();
        $.ajax({
            type: "POST",
            url: this.deleteWithdrawRequestEndPointURI,
            data: {id: id},
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
            },
            error: function (req, status, err) {
                defer.reject(err);
            },
        });
        return defer.promise();
    };


    this.createAPIKey = function () {
        $.ajax({
            type: "POST",
            url:this.createAPIKeyEndPointURI,
            success: function (data) {
                if(data.success == true){
                    $(".edtSecretKey").val(data.data.secret_key);
                    $(".edtPublicKey").val(data.data.public_key);
                    $(".edtPermissionList").show( "slow");
                    $(".btnCreateApiKeyEntry").hide();
                    $(".btnSaveApiKeyEntry").show().removeClass("hidden");

                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                } else {
                    $(".edtPermissionList").slideUp();
                    $(".edtSecretKey").val("");
                    $(".edtPublicKey").val("");

                    $(".btnCreateApiKeyEntry").show();
                    $(".btnSaveApiKeyEntry").hide();
                    $(".btnDeleteApiKeyEntry").hide();
                    $(".btnUpdateApiKeyEntry").hide();

                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };

    this.saveAPIKey = function (serializedData) {
        $.ajax({
            type: "POST",
            url: TPersonal.storeAPIKeyEndPointURI,
            data: serializedData,
            success: function (data) {
                if(data.success == true){
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                    $('#apiList').DataTable().ajax.reload();
                } else {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                }

                $('#frmAddNewAPIKey').modal('toggle');
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };


    this.createTransaction = function (serializedData, transactionType) {
        let transactionURI = "";
        if (transactionType === "buy") {

            if(IS_IEO == true){
                transactionURI = this.createIEOBuyTransactionEndPointURI;
            }else{
                transactionURI = this.createBuyTransactionEndPointURI;
            }

        } else if (transactionType === "sell") {
            if(IS_IEO == true){
                transactionURI = this.createIEOSellTransactionEndPointURI;
            }else{
                 transactionURI = this.createSellTransactionEndPointURI;
            }
        } else {
            return false;
        }

        $("#edtAmountBuyInt").val("");
        $("#edtPriceBuy").val("");
        $("#edtSummaryBuy").val("");
        $("#edtFeeBuy").val("");
        $("#edtAmountSellInt").val("");
        $("#edtPriceSell").val("");
        $("#edtSummarySell").val("");
        $("#edtFeeSell").val("");
        $("#divFeeSell").text('-');
        $("#divFeeBuy").text('-');

        $.ajax({
            type: "POST",
            url: transactionURI,
            data: serializedData,
            success: function (data) {
                if(data.success == true){
                    $("#edtAmountBuyInt").val("");
                    $("#edtPriceBuy").val("");
                    $("#edtSummaryBuy").val("");
                    $("#edtFeeBuy").val("");
                    $("#edtAmountSellInt").val("");
                    $("#edtPriceSell").val("");
                    $("#edtSummarySell").val("");
                    $("#edtFeeSell").val("");
                    $("#divFeeSell").text('-');
                    $("#divFeeBuy").text('-');

                   // $('#lstOpenOrders').DataTable().ajax.reload();
                   // $('#lstOrderHistory').DataTable().ajax.reload();
                    showNotify(data.type,data.title,data.message);
                } else {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "");
                }
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };


    this.cancelTransaction = function (transactionId) {
        let transactionURI = this.cancelTransactionEndPointURI;
        let json = {_token: this.CSRF_TOKEN, transactionId:transactionId};

        $.ajax({
            type: "POST",
            url: transactionURI,
            data: json,
            success: function (response) {
                  dataResponse=response;

                showMessage(response.type, response.title, response.message, true, translate["OK"]).then(function (response) {
                    if(dataResponse.success){
                        //location.reload();
                        $('#lstOpenOrders').DataTable().ajax.reload();
                        $('#lstOrderHistory').DataTable().ajax.reload();
                    }
                });
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };

    this.fillOrderBookTable = function (data, type, table) {

        let color = "";
        if (type === "sell") {
            color = "229, 85, 65, 0.2";
        } else if (type === "buy") {
            color = "0, 206, 125, 0.2";
        } else {
            return false;
        }
        let tableData = "";
        jQuery.each(data, function (i, val) {
            tableData += "<tr data-total='"+val.totalPrice+"' data-amount='"+val.totalAmount+"' data-price='"+val.price+"'>" +
                "<td class='text-left'>" + TPersonal.fixZeroValues(val.price) + "</td>" +
                "<td class='text-right'>" + val.totalAmount + "</td>" +
                "<td class='text-right'>" + TPersonal.fixZeroValues(val.calculatedTotalAmount) + "</td>" +
                "<td class='bar'><div class='"+(type === "sell" ? "ask" : "bid")+" percent' style='width: "+val.depth+"%;'></div></td></tr>";
        });

        $("." + table).html(tableData);

    };

    this.fillOrderHistoryTable = function (data) {

        let tableData = "";
        jQuery.each(data, function (i, val) {

            if (val.color == 2) {
                style = "mh-sell";
            } else if (val.color == 1) {
                style="mh-buy";
            } else {
                style="mh-sameprice";
            }

            tableData += "<tr><td>" + val.date + "</td>" +
                "<td class='text-left "+style+"'>" + val.amount + "</td>" +
                "<td class='text-right "+style+"'>" + TPersonal.fixZeroValues(val.price) + "</td></tr>";
        });

        $(".marketOrderHistory").html(tableData);

    };


    this.fillCoinsTable = function (data) {
        let tableData = "";

        jQuery.each(data, function (i, val) {

            //let isActive = (val.coin_id == TPersonal.coinID && val.currency_id == TPersonal.currencyID);

            let pair = val.currency + "-" + val.coin;

            let row = $('[data-pair="'+pair+'"]');
            row.find("#lastprice").html(TPersonal.fixZeroValues(val.lastprice));
            row.find("#volume").html(val.volume);
            row.find("#change").html(val.change);

            /*tableData += "<tr class='pair' data-coinid='"+val.coin_id+"' data-currencyid='"+val.currency_id+"' " +
                "data-pair='"+pair+"' "+(val.coin_id != TPersonal.selectedTabID ? "style='display: none;'" : "")+">" +
                "<td class='"+(isActive ? "active" : "")+"'><span class='fav-btn cur-hand'></span>" +
                "<img src='/images/"+val.slug+".svg' width='16px'> "+val.currency+"</td>" +
                "<td id='lastprice'>" + val.lastprice + "</td>" +
                "<td id='volume' class='text-right'>" + val.volume + "</td></tr>";*/
        });

        //$(".coinsTable").html(tableData);
    };

    this.updateVolumes = function (data) {
        jQuery.each(data, function (i, val) {
            $('[data-pair="'+i+'"] output').text(val);
        });
    };



    this.loadMarketData = function () {
        $("#edtAmountBuyInt").val("");
        $("#edtPriceBuy").val("");
        $("#edtSummaryBuy").val("");
        $("#edtFeeBuy").val("");
        $("#edtAmountSellInt").val("");
        $("#edtPriceSell").val("");
        $("#edtSummarySell").val("");
        $("#edtFeeSell").val("");
        $("#divFeeSell").text('-');
        $("#divFeeBuy").text('-');

        var endPointURI = "";
        if(IS_IEO == true){
            endPointURI=this.IEOEndPointURI;
        }else{
            endPointURI=this.MarketEndPointURI;
        }

        var json = {coinId: this.coinID, currencyId: this.currencyID};
        var object = this;
        $.ajax({
            type: "POST",
            url: endPointURI,
            data: json,
            dataType: 'JSON',
            headers: {
                'X-CSRF-TOKEN': TPersonal.CSRF_TOKEN
            },
            success: function (data) {
                $("#edtCoinWalletBalance").val(data.balance);
                  $("#edtCurrencyWalletBalance").val(data.currencybalance);
                  $(".marketBuyCurrencySymbol").html(data.currency.symbol);

                TPersonal.activeCoin = data.coin;
                TPersonal.activeCurrency = data.currency;

                TPersonal.fiatBalance = data.fiatbalance;
                TPersonal.cryptoBalance = data.balance;

                $(".userCurrentBalance").html(data.balance + " " + data.coin.symbol);

                //TPersonal.fillOrderBookTable(data.buy, "buy", "orderbookBuyTable");
                //TPersonal.fillOrderBookTable(data.sell, "sell", "orderbookSellTable");
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });

    };

    this.createWalletRequest = function (data) {
        let defer = $.Deferred();
        walletName = data.coin;
        coinId = data.id;
        let json = {_token: this.CSRF_TOKEN, coinId: coinId, walletName: walletName};
        $.ajax({
            type: "POST",
            url: this.createWalletEndPointURI,
            data: json,
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
            },
            failure: function (errMsg) {
                defer.reject(err);
                alert(errMsg);
            }
        });
        return defer.promise();
    };

    this.deleteDepositRequest = function (currency) {
        let defer = $.Deferred();

        $.ajax({
            type: "POST",
            url: this.deleteDepositEndPointURI,
            data: {currency: currency},
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
            },
            error: function (req, status, err) {
                defer.reject(err);
            },
        });
        return defer.promise();
    };

    this.cancelWithdrawRequest = function (e) {
        $.ajax({
            type: "POST",
            url: this.cancelWithdrawEndPointURI,
            dataType: 'JSON',
            success: function (data) {
                window.location = TPersonal.withdrawURL;
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
    };

    this.deleteBankAccount = function (id) {
        var defer = $.Deferred();

        $.ajax({
            type: "POST",
            url: this.deleteBankAccountEndPointURI,
            data: {id: id},
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
            },
            error: function (req, status, err) {
                defer.reject(err);
            },
        });

        return defer.promise();
    },

    this.selectPair = function (pair) {
        if(IS_IEO == true){
        window.location = this.pairIEOEndPointURI + pair;
        }else{
            window.location = this.pairEndPointURI + pair;
        }
    }
}


$(document).ready(function () {

    AOS.init({
        easing: 'ease-in-out-sine',
        duration: 600,
        disable: 'mobile'
    });

    function formatCoinSelect(option) {
        if(option.disabled == true){
            return option.text;
        }
        if (!option.id) {
            return option.text;
        }

        let icon = option.element.dataset.icon;

        let image = '<img style="margin-top: -2px !important; margin-right: 5px !important;" width="24px" height="24px" src="'+icon+'"/>';
        var ob = image + option.text + " <span class='text-muted'>"+option.element.dataset.symbol+"</span>";
        return ob;
    };


    function formatCountry(option) {
        if(option.disabled == true){
            return option.text;
        }
        if (!option.id) {
            return option.text;
        }

        let icon = option.element.dataset.icon;

        let image = '<img style="margin-top: -2px !important; margin-right: 5px !important;" width="16px" height="11px" src="/images/flags/'+icon+'.png"/>';
        var ob = image + option.text + " <span class='text-muted'>"+option.element.dataset.symbol+"</span>";
        return ob;
    };


    function formatBankAccountSelect(option) {
        if (!option.id) {
            return option.text;
        }
        var ob = '<i class="fa ' + option.element.dataset.symbol + '"></i> ' + option.text;
        return ob;
    };

    function formatCallingCodeSelect(option) {
        if (!option.id) {
            return option.text;
        }
        var ob = '<i class="fa ' + option.element.dataset.symbol + '"></i> ' + option.text;
        return ob;
    };


    function calculateWithdrawSummary(){

        let amount = new BigNumber($("#withdrawAmount").val());
        let fee = new BigNumber($("#withdrawFee").val());
        let total = new BigNumber(0);
        if((!amount.isNaN()) &&
            (!fee.isNaN()) &&
            (amount.isFinite()) &&
            (fee.isFinite())){
            total = amount.minus(fee);

            if(total>0){
                $("#withdrawTotalAmount").val(total.decimalPlaces(8).toFixed(8));
            }else{
                total = new BigNumber(0);
                $("#withdrawTotalAmount").val(total.decimalPlaces(8).toFixed(8));
            }
        }else{
            total = new BigNumber(0);
            $("#withdrawTotalAmount").val(total.decimalPlaces(8).toFixed(8));

        }
    }




    $("#country").select2({
        theme: 'bootstrap4',
        placeholder: function(){
            $(this).data('placeholder');
        },
        templateResult: formatCountry,
        templateSelection: formatCountry,
        escapeMarkup: function (m) {
            return m;
        },
    }).on('select2:select', function (e) {
        $('#calling_code').prop('selectedIndex', $("#country option:selected").index()).change();
    });

    $("#calling_code").select2({
        theme: 'bootstrap4',
        placeholder: function(){
            $(this).data('placeholder');
        },
        templateResult: formatCountry,
        templateSelection: formatCountry,
        escapeMarkup: function (m) {
            return m;
        },
    }).on('select2:select', function (e) {
        var data = e.params.data.element.dataset;


    });



    $(".withdrawCoinList").select2({
        theme: 'bootstrap4',
        placeholder: function(){
            $(this).data('placeholder');
        },
        templateResult: formatCoinSelect,
        templateSelection: formatCoinSelect,
        escapeMarkup: function (m) {
            return m;
        },
    }).on('select2:select', function (e) {
        var data = e.params.data.element.dataset;
        $(".coin-symbol").text(data.symbol);
        $(".walletAvaibleBalance").val(data.balance);
        $(".walletProcessingBalance").val(data.processingbalance);
        $(".walletTotalBalance").val(data.fixedbalance);
        $("#withdrawFee").val(data.fee);
        $("#minimumWithdrawAmount").val(data.minwithdraw)

        $( ".withdrawInputPanel" ).fadeOut( "fast" );
        $( ".withdrawInputPanel" ).fadeIn( "slow" );
        calculateWithdrawSummary();

    });

    $('.toggle_radio input:radio').click(function() {

        let avaibleBalance = new BigNumber($(".withdrawCoinList").find(":selected").data("balance").toString().replace(/,/g,""));
        let ratio = parseFloat($(this).data("index"));
        let amount = avaibleBalance.multipliedBy(ratio);
        $("#withdrawAmount").val(amount.decimalPlaces(8).toFixed(8));
        calculateWithdrawSummary();
    });

    $(".selectBankAccount").select2({
        theme: 'bootstrap4',
       // width: "100%",
        templateResult: formatBankAccountSelect,
        minimumResultsForSearch: Infinity,
        templateSelection: formatBankAccountSelect,
        escapeMarkup: function (m) {
            return m;
        }
    });

    $(".selectCallingCode").select2({
        theme: 'bootstrap4',
        width: "100%",
        templateResult: formatCallingCodeSelect,
        minimumResultsForSearch: Infinity,
        templateSelection: formatCallingCodeSelect,
        escapeMarkup: function (m) {
            return m;
        }
    });



    $(document).on('click touch', '.currency-tabs .tab-item', function (e) {
        var selectedCoin = $(this).data("coinid");

        $(".currency-tabs .tab-item").removeClass("active");
        $(this).addClass("active");
        TPersonal.selectedTabID = $(this).data("coinid");

        $(".coinsTable .pair").each(function () {
           if($(this).data("coinid") != selectedCoin) {
               $(this).hide();
           } else {
               $(this).show();
           }
        });
        e.preventDefault();
    });

    $(document).on('click', '.fav-btn.filter', function (e) {

        $(this).toggleClass("checked");

        let isChecked = $(this).hasClass("checked");

        $(".coinsTable .pair").each(function () {
            let favBtn = $(this).find(".fav-btn");

            if(!favBtn.hasClass("checked")) {
                favBtn.parent().parent().toggleClass("hidden");
            }
        });
    });

    $(document).on('click', '.coinsTable .pair .fav-btn', function (e) {
        e.stopPropagation();

        var selectedPair = $(this).parent().parent().data("pair");
        var obj = $(this);

        let defer = $.Deferred();
        $.ajax({
            type: "POST",
            url: TPersonal.updateFavoritePairsEndPointURI,
            data: {pair: selectedPair},
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
                if(data.action == "add") {
                    obj.addClass("checked");
                } else {
                    obj.removeClass("checked");
                }
            },
            error: function (req, status, err) {
                defer.reject(err);

                window.location.href = document.location.origin + '/login';
            },
        });
        return defer.promise();
    });


    $(document).on('click', '.coinsTable .pair', function (e) {
        let pair = $(this).data('pair');
        TPersonal.selectPair(pair);
        e.preventDefault();
    });

    $(".btnMarketBuyAsset").click(function () {
        $(".btnMarketBuyAsset").addClass("bg-gradient-blue text-white");
        $(".btnMarketBuyAsset").removeClass("bg-white");
        $(".btnMarketBuyAsset").removeClass("text-dark-sky");

        $(".btnMarketSellAsset").removeClass("bg-gradient-blue");
        $(".btnMarketSellAsset").removeClass("text-white");
        $(".btnMarketSellAsset").addClass("bg-white");
        $(".btnMarketSellAsset").addClass("text-dark-sky");


        $(".market-buy").removeClass("hidden");
        $(".market-sell").hide();
        $(".market-buy").show();

        $(".marketBtnBuyNow").removeClass("hidden");
        $(".marketBtnSellNow").hide();
        $(".marketBtnBuyNow").show();
    });

    $(".btnMarketSellAsset").click(function () {
        $(".btnMarketSellAsset").addClass("bg-gradient-blue text-white");
        $(".btnMarketSellAsset").removeClass("bg-white");
        $(".btnMarketSellAsset").removeClass("text-dark-sky");

        $(".btnMarketBuyAsset").removeClass("bg-gradient-blue");
        $(".btnMarketBuyAsset").removeClass("text-white");
        $(".btnMarketBuyAsset").addClass("bg-white");
        $(".btnMarketBuyAsset").addClass("text-dark-sky");

        $(".market-sell").removeClass("hidden");
        $(".market-buy").hide();
        $(".market-sell").show();

        $(".marketBtnSellNow").removeClass("hidden");
        $(".marketBtnBuyNow").hide();
        $(".marketBtnSellNow").show();
    });

    $(".marketBtnBuyNow").click(function () {
        szMarketBuyData = $("#frmMarketBuy").serialize();

    });


    $('#walletTable tbody').on( 'click', '.createWalletBtn', function () {
        var data = $(this).data("info");
        data = JSON.parse(decodeURIComponent(data));
        var progressBar =
            '<div class="progress-outer">'+
            '<div class="progress">'+
            '<div class="progress-bar progress-bar-info progress-bar-striped active" style="width:100%; box-shadow:-1px 10px 10px rgba(91, 192, 222, 0.7);">'+
            '<div class="progress-value">100%</div>'+
            '</div></div>'+
            '</div>';
        if(data.can_create == true){
           let progressContainer = $(this);
            showMessage("info", translate["WARNING"], translate["CREATE_WALLET_VERIFY"], true, translate["YES"], true, translate["NO"]).then(function (bReturn) {
                if (bReturn) {
                    progressContainer.parents("td").html(progressBar);
                    TPersonal.createWalletRequest(data).then(function (response) {
                        let dataMessage = response;
                        showMessage(response.type, response.title, response.message, true, translate["OK"]).then(function (response) {
                                $('#walletTable').DataTable().ajax.reload();

                        });
                    });
                }else{
                    $('#walletTable').DataTable().ajax.reload();
                }
            })


        }


    } );







    $("#delete_deposit_request").on("click", function (e) {
        showMessage("warning", translate["ARE_YOU_SURE"], translate["DELETE_REQUEST_QUESTION"], true, translate["YES_DELETE"], true, translate["CANCEL"]).then(function (bReturn) {
            if (bReturn) {
                TPersonal.deleteDepositRequest($(this).data("currency")).then(function (data) {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "").then(function (bReturn) {
                        if (bReturn) {
                            location.reload();
                        }
                    });
                });
            }
        });
        e.preventDefault();
    });

    $("#btnGetNewAddress").on("click", function (e) {
        let defer = $.Deferred();
        coinId = $(this).data('coinid');
        let json = {_token: this.CSRF_TOKEN, coinId: coinId};
        $.ajax({
            type: "POST",
            url: TPersonal.getNewAddressEndPointURI,
            data: json,
            dataType: 'JSON',
            success: function (data) {
                defer.resolve(data);
                showMessage(data.type, data.title, data.message, true, translate["OK"], false, "").then(function (bReturn) {
                    if (bReturn) {
                        if(data.type == "success") location.reload();
                    }
                });
            },
            failure: function (errMsg) {
                defer.reject(err);
                alert(errMsg);
            }
        });
        return defer.promise();
    });





    new ClipboardJS('.btn.copy');
    function copyToClipboard(text, el) {
        var copyTest = document.queryCommandSupported('copy');
        var elOriginalText = el.attr('data-original-title');

        if (copyTest === true) {
            var copyTextArea = document.createElement("textarea");
            copyTextArea.value = text;
            document.body.appendChild(copyTextArea);
            copyTextArea.select();
            try {
                var successful = document.execCommand('copy');
                var msg = successful ? translate["COPIED"] : translate["COULDNT_COPIED"];
                el.attr('data-original-title', msg).tooltip('show');
            } catch (err) {
                console.log(translate["UNABLE_TO_COPY"]);
            }
            document.body.removeChild(copyTextArea);
            el.attr('data-original-title', elOriginalText);
        } else {
            // Fallback if browser doesn't support .execCommand('copy')
            window.prompt(translate["CLIPBOARD_INSTRUCTIONS"], text);
        }
    }

    //$('.js-tooltip').tooltip();

    $('.copy-wallet-address').click(function() {
        var text = $("#wallet-address").val();
        var el = $(this);
        copyToClipboard(text, el);
    });

    $('.copy-ref_code').click(function() {
        var text = $("#ref_code").val();
        var el = $(this);
        copyToClipboard(text, el);
    });

    $('#copyWalletTag').click(function() {
        var text = $("#wallet-tag").val();
        var el = $(this);
        copyToClipboard(text, el);
    });



    $(document).on('click', ".copy-txid", function (event) {
        var text = $(this).data("txid");
        var el = $(this);
        copyToClipboard(text, el);
    });

    //Verify Withdraw Page
    $(".verify_withdraw_form #save_account").change(function () {
        $('.verify_withdraw_form #account_name').toggleClass('show');
    });

    $(".useAccount").click(function (e) {

        $("input#iban").val($(this).parent().parent().find(".accountIBAN").text());

        $("select#bank_id").val($(this).data('bank_id')).change();

        e.preventDefault();

    });

    $(".removeAccount").click(function (e) {
        var button = this;

        showMessage("warning", translate["VERIFY"], translate["DELETE_RECORD_VERIFY"], true, translate["YES_DELETE"], true, translate["CANCEL"]).then(function (bReturn) {
            if (bReturn) {
                TPersonal.deleteBankAccount($(button).data('id')).then(function () {
                    location.reload();
                });
            }
        });
        e.preventDefault();
    });

    $("#btnMarketBuy").click(function (e) {
        var serializedData = $("#frmMarketBuy").serialize();
        TPersonal.createTransaction(serializedData, "buy");
        e.preventDefault();
    });

    $("#btnMarketSell").click(function (e) {
        var serializedData = $("#frmMarketSell").serialize();
        TPersonal.createTransaction(serializedData, "sell");
        e.preventDefault();
    });

    $('#listAirdrops tbody').on( 'click', '.btn-airdrop-details', function (event) {
        event.preventDefault();
        let airdropId = $(this).data("id");
        TPersonal.loadAirdropDetails(airdropId);
    });


    $(document).on('click', ".btnExpandTable", function (event) {
        event.preventDefault();
        let buttonObject = $(this);
        let tableObjectStr = $(this).data("object");

        let wrapper =  "#"+tableObjectStr+"_wrapper";
        let tableObject = "#"+tableObjectStr;

        $(wrapper).toggle();

        var isVisible = $( wrapper ).is( ":visible" );
        if(isVisible){
            $(buttonObject).html('<i class="fa fa-angle-double-down"></i>');
        }else{
            $(buttonObject).html('<i class="fa fa-angle-double-up"></i>');
        }




    });

    $(document).on('click', "a.btnMarketCancelOrder", function (event) {
        event.preventDefault();
        let transactionId = $(this).data("id");
        TPersonal.cancelTransaction(transactionId);
    });

    $(document).on('click', ".btnDeleteWithdrawRequest", function (event) {
        event.preventDefault();
        var withdraw_id = $(this).data("withdraw");

        showMessage("warning", translate["ARE_YOU_SURE"], translate["DEP_REQ_WILL_BE_DELETED"], true, translate["CONTINUE"], true, translate["CANCEL"]).then(function (bReturn) {
            if (bReturn) {
                TPersonal.deleteWithdrawRequest(withdraw_id).then(function (data) {
                    showMessage(data.type, data.title, data.message, true, translate["OK"], false, "").then(function (bReturn) {
                        if (bReturn) {
                            location.reload();
                        }
                    });
                });
            }
        });
    });

    $(".btnAddNewApiKey").click(function (e) {
        e.preventDefault();
        $(".edtPublicKey").val("");
        $(".edtSecretKey").val("");

        title = translate["CREATE_API_KEY_ENTRY"];
        $("#apiEntryTitle").text(title);

        $(".btnCreateApiKeyEntry").show();
        $(".btnSaveApiKeyEntry").hide();
        $(".btnDeleteApiKeyEntry").hide();
        $(".btnUpdateApiKeyEntry").hide();

        $(".edtPermissionList").slideUp();
    });

    $(".btnSaveApiKeyEntry").click(function (e) {
        $("#edtActionType").val("save");
        let serializedData = $("#frmApiKey").serialize();
        TPersonal.saveAPIKey(serializedData);
    });

    $(".btnCreateApiKeyEntry").click(function (e) {
        e.preventDefault();
        $(".edtPublicKey").val("");
        $(".edtSecretKey").val("");
        $(".edtPermissionList").slideUp();
        TPersonal.createAPIKey();
    });

    $(".btnDeleteApiKeyEntry").click(function (e) {
        $("#edtActionType").val("delete");
        let serializedData = $("#frmApiKey").serialize();
        TPersonal.saveAPIKey(serializedData);
    });

    $(".btnUpdateApiKeyEntry").click(function (e) {
        $("#edtActionType").val("update");
        let serializedData = $("#frmApiKey").serialize();
        TPersonal.saveAPIKey(serializedData);
    });

    $("#btnIncrementSellAmount").click(function (e) {
        e.preventDefault();
        let val = 0;
        val = parseInt($("#edtAmountSellInt").val()) || 0;
        val=val+1;
        $("#edtAmountSellInt").val(val);
        calculateBuy(undefined,true,undefined,false,false);
    });


    $("#btnIncrementBuyAmount").click(function (e) {
        e.preventDefault();
        let val = 0;
        val = parseInt($("#edtAmountBuyInt").val()) || 0;
        val=val+1;
        $("#edtAmountBuyInt").val(val);
        calculateBuy(undefined,true,undefined,false,true);
    });

    $('.orderbook tbody').on('click', 'tr', function () {
        let total = $(this).data("total");
        let amount = $(this).data("amount");
        let price = $(this).data("price");
        let objDetect = this.parentElement.getAttribute("class").toString();
        if(objDetect==="orderbookBuyTable"){
            let fee = currency(total) * $("#coinFee").val();
            fee = Math.round(fee*Math.pow(10,4))/Math.pow(10,4)
            setVal("#edtSummarySell", total);
            setVal("#edtPriceSell", price);
            setVal("#edtPriceBuy", price);
            setVal("#edtAmountSellInt", amount);
            setVal("#edtFeeSell", fee.toFixed(4));
            if(fee>0) $("#divFeeSell").text(fee.toFixed(4));
        }else{
            let fee = currency(amount) * $("#currencyFee").val();
            fee = Math.round(fee*Math.pow(10,4))/Math.pow(10,4)
            setVal("#edtSummaryBuy", total);
            setVal("#edtPriceBuy", price);
            setVal("#edtPriceSell", price);
            setVal("#edtAmountBuyInt", amount);
            setVal("#edtFeeBuy", fee.toFixed(4));
            if(fee>0) $("#divFeeBuy").text(fee.toFixed(4));
        }

        $("#edtPriceBuy").change();
        $("#edtAmountSellInt").change();

    });

    $(".orderbook-short").on('click', function (e){
        let originHeight = $(".exnce_pairs").innerHeight();
        let maxHeight =(originHeight - 81.4)+"px";
        let itemHeight =((originHeight/2) - 54)+"px";
        let bidHeight = ((originHeight - 81.4) - 54) + "px";
        let filter = $(this).data("filter");
        switch(filter){
            case 0:
                $(".orderbook-panels").css( "height", itemHeight ).removeClass("open").slideDown("fast").show();
                break;
            case 1:
                $(".orderbook-panels").first().slideUp("slow").removeClass("open");
                $(".orderbook-panels").last().css( "height", maxHeight).addClass("open").slideDown("fast").show();
                break;
            case 2:
                $(".orderbook-panels").first().css( "height", bidHeight ).addClass("open").slideDown("fast").show();
                $(".orderbook-panels").last().slideUp("slow").removeClass("open");
                break;
            default:
                $(".orderbook-panels").css( "height", itemHeight ).removeClass("open").slideDown("fast").show();
                break;
        }
    });

    $("#edtAmountBuyInt").change(function(event) {
        calculateBuy(event,true,this,false,true);
    }).keyup(function(event) {
        calculateBuy(event,true,this,false,true);
    }).keydown(function( event ) {
        calculateBuy(event,true,this,false,true);
    });

    $("#edtPriceBuy").change(function(event) {
        calculateBuy(event,true,this,false,true);
    }).keyup(function(event) {
        calculateBuy(event,true,this,false,true);
    }).keydown(function( event ) {
        calculateBuy(event,true,this,false,true);
    });

    $("#edtSummaryBuy").change(function(event) {
        calculateBuy(event,true,this,true,true);
    }).keyup(function(event) {
        calculateBuy(event,true,this,true,true);
    }).keydown(function(event) {
        calculateBuy(event,true,this,true,true);
    });

    $("#edtAmountSellInt").change(function(event) {
        calculateBuy(event,true,this,false,false);
    }).keyup(function(event) {
        calculateBuy(event,true,this,false,false);
    }).keydown(function( event ) {
        calculateBuy(event,true,this,false,false);
    });

    $("#edtPriceSell").change(function(event) {
        calculateBuy(event,true,this,false,false);
    }).keyup(function(event) {
        calculateBuy(event,true,this,false,false);
    }).keydown(function( event ) {
        calculateBuy(event,true,this,false,false);
    });

    $("#withdrawAmount").each(function(){
        $(this).keydown(function (e){
            calculateWithdrawSummary();

            if(e.which == 190 && $(this).val().indexOf(".") >= 0) return false;
            if( (e.which != 190 && e.which != 8 && e.which != 46 && e.which != 37 && e.which != 39 && e.which != 9)
                && !(e.which>=48 && e.which<=57)
                && !(e.which>=96 && e.which<=105) ) return false;
        });

        $(this).on('change', function(){
            calculateWithdrawSummary();
        });

        $(this).keyup(function() {
            calculateWithdrawSummary();
        });

        $(this).change(function() {
            calculateWithdrawSummary();
        });
    });


    $("#frmMarketSell :input, #frmMarketBuy :input").each(function(){
        $(this).keydown(function (e){
            if(e.which == 190 && $(this).val().indexOf(".") >= 0) return false;
            if( (e.which != 190 && e.which != 8 && e.which != 46 && e.which != 37 && e.which != 39 && e.which != 9)
                && !(e.which>=48 && e.which<=57)
                && !(e.which>=96 && e.which<=105) ) return false;
        });

        $(this).keypress(function (e){
            let amt = $(this);
            if (isNaN(amt.val())) {
                amt.val(0);
            }
            if (amt.val().indexOf(".") > -1 && (amt.val().split('.')[1].length > 7)) {
                amt.val(amt.val().substring(0, amt.val().length - 1));
            }


        });
    });

    function validate(s) {
        var rgx = /^[0-9]*\.?[0-9]*$/;
        return s.match(rgx);
    }

    $("#edtSummarySell").change(function(event) {
        calculateBuy(event,true,this,true,false);
    }).keyup(function(event) {
        calculateBuy(event,true,this,true,false);
    });

    $(".market-buy .ratio-item").on("click", function (e) {
        var ratio = $(this).data("val");
        setVal("#edtSummaryBuy", (TPersonal.coinBalanceFloat * ratio).toFixed(8));
        $("#edtSummaryBuy").trigger("change");
    });

    $(".market-sell .ratio-item").on("click", function (e) {
        var ratio = $(this).data("val");
        setVal("#edtAmountSellInt", (TPersonal.currencyBalanceFloat * ratio).toFixed(8));
        $("#edtAmountSellInt").trigger("change");
    });


    $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust()
            .responsive.recalc();
    });


    var activeSlide = $('.value-list .active').index();

    $('.value-list').slick({
        infinite: true,
        centerMode: false,
        centerPadding: '40px',
        slidesToShow: 4,
        rows: 0,
        //initialSlide: activeSlide,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 769,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 3
                }
            },
            {
                breakpoint: 360,
                settings: {
                    slidesToShow: 2
                }
            },
        ]
    });

    $(document).on("click",'.btn-follow-airdrop', function (e){
        var onAirdrop   = $(this).data('airdropid');
        TPersonal.updateFollow(true,onAirdrop);
    });
    $(document).on("click",'.btn-unfollow-airdrop', function (e){
        var onAirdrop   = $(this).data('airdropid');
        TPersonal.updateFollow(false,onAirdrop);
    });

    $(document).on('mouseover', '#stars li', function (e) {
        var onStar = parseInt($(this).data('value'), 10);
        var onAirdrop = parseInt($(this).data('id'), -1);
        $(this).parent().children('li.star').each(function(e){
            if (e < onStar) {
                $(this).addClass('hover');
            }
            else {
                $(this).removeClass('hover');
            }
        });
    }).on('mouseout', '#stars li', function(){
        $(this).parent().children('li.star').each(function(e){
            $(this).removeClass('hover');
        });
    }).on('click', '#stars li', function(){
        var onStar = parseInt($(this).data('value'), 10);
        var stars = $(this).parent().children('li.star');
        for (i = 0; i < stars.length; i++) {
            $(stars[i]).removeClass('selected');
        }
        for (i = 0; i < onStar; i++) {
            $(stars[i]).addClass('selected');
        }
        var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
        var onAirdrop   = $('#stars li.selected').last().data('airdropid');

        responseMessage(ratingValue,onAirdrop);
    });



    function responseMessage(ratingValue,airdropId) {
        TPersonal.updateRatingValue(ratingValue,airdropId);
    }

    var afterChange = function(slider,i) {
        //var slideHeight = jQuery(slider.$slides[i] ).height();
        //jQuery(slider.$slider ).height( slideHeight);
        $(this)
    };

    $('.slick-slide').on('click', function (e){
        let slideClicked = $(e.currentTarget).attr("data-slick-index");
    });



    $('ul.nav li.dropdown').hover(function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
    }, function() {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
    });

    $('#id_card_front, #id_card_back, #selfie').bind('change', function() {
        if(this.files[0].size >= 5*1024*1024) {
            this.value = this.defaultValue;
            showMessage("error", "Hata", translate["UPLOAD_ERROR"], true, translate["OK"], false, "");
        }
    });

    $(".disableAuthenticator").click(function (e) {
        var serializedData = $("#frmDisableAuthenticator").serialize();
        $.ajax({
            type: "POST",
            url: TPersonal.disableAuthenticatorEndPointURI,
            data: serializedData,
            success: function (data) {
                showMessage(data.type, data.title, data.message, true, "OK").then(function () {
                    if(data.type == "warning") location.reload();
                });
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
        e.preventDefault();
    });

    $(".enableAuthenticator").click(function (e) {
        var serializedData = $("#frmAuthenticator").serialize();
        $.ajax({
            type: "POST",
            url: TPersonal.enableAuthenticatorEndPointURI,
            data: serializedData,
            success: function (data) {
                showMessage(data.type, data.title, data.message, true, "OK").then(function () {
                    if(data.type == "success") location.reload();
                });
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
        e.preventDefault();
    });

    $("#btnShowGoogle2FA").click(function (e) {
        $("#divGoogle2FA").slideToggle("slow");
    });

    $("#btnDisableGoogle2FA").click(function (e) {
        $("#divDisableGoogle2FA").slideToggle("slow");
    });

    $(".btnOpenCamera").click(function (e) {
        e.preventDefault();
        TPersonal.loadWebCamera();
    });

    $(".darkTheme").click(function (e) {
        e.preventDefault();
        TPersonal.switchTheme("DARK");
    });

    $(".lightTheme").click(function (e) {
        e.preventDefault();
        TPersonal.switchTheme("LIGHT");
    });

    $("#btnActivateSms2FA").click(function (e) {
        $.ajax({
            type: "POST",
            url: TPersonal.enableSMS2FAEndPointURI,
            success: function (data) {
                showMessage(data.type, data.title, data.message, true, "OK").then(function () {
                    location.reload();
                });
            },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
        e.preventDefault();
    });

    $(".btn-copy").tooltip('show');


    TPersonal.switchTheme(TPersonal.getTheme());
    TPersonal.setCoinID(selectedCoin);
    TPersonal.setCurrencyID(selectedCurrency);
    TPersonal.selectedTabID = selectedCoin;
    TPersonal.initWallet();
    TPersonal.loadOpenedOrders();
    TPersonal.loadOrderHistory();
    TPersonal.loadUserActivityLogs();
    TPersonal.loadUserOrderHistory();
    TPersonal.loadWithdrawRequests();
    TPersonal.loadDepositHistory();
    TPersonal.loadAPIKeys();
    TPersonal.loadReferrals();
    TPersonal.loadSupportRequests();
    TPersonal.initDepthChart();
    TPersonal.initAirdrops();
    TPersonal.closeSidebar();

});


function requestCameraAccess(){
    TPersonal.loadWebCamera();
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}

function replaceAll(str, find, replace) {
    str=str.toString();
    return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function enforceFloat(object) {
    var valid = /^\-?\d+\.\d*$|^\-?[\d]*$/;
    var number = /\-\d+\.\d*|\-[\d]*|[\d]+\.[\d]*|[\d]+/;
    if (object.value.charAt(0) != "." && !valid.test(object.value)) {
        var n = object.value.match(number);
        object.value = n ? n[0] : '';
    }
}

function setVal(identifier, value) {
    $(identifier).val(value);
    $(identifier).attr("value",value);
}

function calculateBuy(event,control,object,istotal,isbuy){
    var decAmount = 0;
    var priceBuy = 0;
    var summary = 0;
    var feeTotal = 0;
    var feeAmount = 0;




    if(isbuy === true){
        feeAmount = $("#currencyFee").val();
        szAmountInt = "#edtAmountBuyInt";
        szPrice = "#edtPriceBuy";
        szSummary = "#edtSummaryBuy";
        szFee = "#edtFeeBuy";
        szDivFee = "#divFeeBuy";
    }else{
        feeAmount = $("#coinFee").val();
        szAmountInt = "#edtAmountSellInt";
        szPrice = "#edtPriceSell";
        szSummary = "#edtSummarySell";
        szFee = "#edtFeeSell";
        szDivFee = "#divFeeSell";
    }

    let bnAmount = new BigNumber($(szAmountInt).val());
    let bnPriceBuy = new BigNumber($(szPrice).val());
    let bnSummary = new BigNumber($(szSummary).val());
    let bnFee = new BigNumber(feeAmount);
    let bnTotalFee = new BigNumber(0);

    if(istotal==false) {
        if((!bnAmount.isNaN()) &&
            (!bnPriceBuy.isNaN()) &&
            (bnAmount.isFinite()) &&
            (bnPriceBuy.isFinite()) &&
            (!bnFee.isNaN()) &&
            (bnFee.isFinite())){

            bnSummary = bnAmount.multipliedBy(bnPriceBuy);
            setVal(szSummary, bnSummary.decimalPlaces(TPersonal.COIN_DECIMAL).toFixed(TPersonal.COIN_DECIMAL));
            if(isbuy === true) {
                bnTotalFee = new BigNumber(bnAmount.multipliedBy(bnFee)).decimalPlaces(TPersonal.CURRENCY_DECIMAL).toFixed(TPersonal.CURRENCY_DECIMAL);
            } else {
                bnTotalFee = new BigNumber(bnSummary.multipliedBy(bnFee)).decimalPlaces(TPersonal.COIN_DECIMAL).toFixed(TPersonal.COIN_DECIMAL);
            }
        }else{
            setVal(szSummary,"");
            bnTotalFee = new BigNumber(0);
        }
    } else {

        if((!bnSummary.isNaN()) &&
            (!bnPriceBuy.isNaN()) &&
            (bnSummary.isFinite()) &&
            (bnPriceBuy.isFinite())){

            BigNumber.set({ DECIMAL_PLACES: TPersonal.COIN_DECIMAL, ROUNDING_MODE: BigNumber.ROUND_UP });

            if(bnPriceBuy.isGreaterThan(0)){
                if(bnSummary.isGreaterThan(0)){
                    let totalAmount = new BigNumber(0);
                    totalAmount = bnSummary.dividedBy(bnPriceBuy);
                    if((!totalAmount.isNaN()) &&
                         (totalAmount.isFinite()) && (!totalAmount.isLessThanOrEqualTo(0))){
                    }else{
                        totalAmount = new BigNumber(0);
                    }
                    setVal(szAmountInt, totalAmount.decimalPlaces(18));
                }
            }
            if(isbuy == true) {
                bnTotalFee = new BigNumber(bnAmount.multipliedBy(bnFee)).decimalPlaces(TPersonal.CURRENCY_DECIMAL).toFixed(TPersonal.CURRENCY_DECIMAL);
            } else {
                bnTotalFee = new BigNumber(bnSummary.multipliedBy(bnFee)).decimalPlaces(TPersonal.COIN_DECIMAL).toFixed(TPersonal.COIN_DECIMAL);
            }
        }
    }


    bnTotalFee = new BigNumber(bnTotalFee);
    if((!bnTotalFee.isNaN()) &&
        (bnTotalFee.isFinite())){
    }else{
        bnTotalFee = new BigNumber(0);
    }
    setVal(szFee, bnTotalFee.decimalPlaces(TPersonal.COIN_DECIMAL).toFixed(TPersonal.COIN_DECIMAL));
    $(szDivFee).text(bnTotalFee.decimalPlaces(TPersonal.COIN_DECIMAL).toFixed(TPersonal.COIN_DECIMAL));

}

if(CURRENT_SEGMENT === "market"){
TPersonal.setCoinID(selectedCoin);
TPersonal.setCurrencyID(selectedCurrency);
let socket = io.connect(EXCHANGE_SOCKET_ADDRESS, {secure: true});
socket.on('connect', function () {
    if(CURRENT_SEGMENT === "market"){
        TPersonal.loadMarketData();
    }
});

socket.on('message', function (message) {


    if(message == null){
        return;
    }

    if(CURRENT_SEGMENT != "market"){
        return;
    }
    message = JSON.parse(message);

    if (typeof message == 'undefined') {
        return;
    }

    if (typeof message.stats != 'undefined') {
        if ((TPersonal.coinID == message.coin.id) && (TPersonal.currencyID == message.currency.id)) {
            TPersonal.updateHeaderStatistics(message.stats.html);
        }
    }

    if (typeof message.volumes != 'undefined') {
        TPersonal.updateVolumes(message.volumes);
    }

    if((TPersonal.coinID == message.coin.id) && (TPersonal.currencyID == message.currency.id)){
        dLastPrice = 0.00;
        if (typeof message.market_history !== 'undefined') {
            if(message.market_history.length>0){
                dLastPrice = message.market_history[0].price;
            }
        }

        TPersonal.COIN_DECIMAL = message.decimals.coin;
        TPersonal.CURRENCY_DECIMAL = message.decimals.currency;

        TPersonal.fillOrderBookTable(message.buy, "buy", "orderbookBuyTable");
        TPersonal.fillOrderBookTable(message.sell, "sell", "orderbookSellTable");
        TPersonal.fillDepthChart(message.sell,message.buy, dLastPrice);
        TPersonal.fillCoinsTable(message.pairsData);

        if(IS_IEO == false){
            document.title = SELECTED_PAIR_INFO + " " + message.pairsData[SELECTED_PAIR_INFO].lastprice + " | " + EXCHANGE_NAME;
        }

        $(".lastPrice .price").text(0);
        $(".lastPrice").removeClass("up down");
        if (typeof message.market_history != 'undefined') {
            TPersonal.fillOrderHistoryTable(message.market_history);
            if (typeof message.market_history[0] != 'undefined') {
                $(".lastPrice .price").text(message.market_history[0].price);
                if (message.market_history[0].color == 1) {
                    $(".lastPrice").addClass("up");
                } else if (message.market_history[0].color == 2) {
                    $(".lastPrice").addClass("down");
                }
            }
        }
    }

    if ((typeof message.balance != 'undefined') && (message.balance.token == CURRENT_USER_HASH))
    {
        if (typeof message.coin.id != 'undefined') {
            if ((TPersonal.coinID == message.coin.id) && (TPersonal.currencyID == message.currency.id)) {



                TPersonal.coinBalanceFloat = message.balance.coinBalanceFloat;
                TPersonal.currencyBalanceFloat = message.balance.currencyBalanceFloat;
                let coinSlug = message.currency.slug;
                $(".marketCoinName").html(message.coin.name);
                $(".marketCoinSymbol").html(message.currency.symbol);//coin
                $(".marketSellCoinSymbol").html(message.currency.symbol);//coin
                $(".orderbook-buy-amount-symbol").html(message.coin.symbol);
                $(".orderbook-sell-amount-symbol").html(message.coin.symbol);
                $("#edtBuyCoin").attr("value", message.coin.symbol);
                $("#edtBuyCurrency").attr("value", message.currency.symbol);
                $("#edtSellCoin").attr("value", message.coin.symbol);
                $("#edtSellCurrency").attr("value", message.currency.symbol);
                $("#coinSymbol").text(message.coin.symbol);
                $("#currencySymbol").text(message.currency.symbol);
                $(".img-coin-logo").attr("src", "/images/coins/" + coinSlug + ".svg");
                $("#coinBalance").text(message.balance.coinBalance);
                $("#currencyBalance").text(message.balance.currencyBalance);
                $(".userCurrentBalance").text(message.balance.coinBalance);
                TPersonal.loadOpenedOrders();
                TPersonal.loadOrderHistory();
            }
        }
    }
});
}