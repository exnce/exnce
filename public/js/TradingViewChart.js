class TradingViewChart {
    constructor(divId, baseUrl) {
        this.BaseUrl = baseUrl;
        this.DivId = divId;
        this.widget = null;
    }

    getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    }

    load(marketId, marketPair) {
        var url = document.location.origin + '/api';
        var retVal = "DARK";

        if(localStorage.getItem("DarkExchange") !== null){
            retVal = localStorage.getItem("DarkExchange");
        }else{
            retVal = "LIGHT";
        }

        var styleName = "/css/chart.css";
        var chartBGColor = "#ffffff";
        var chartGridColor ="#3a4c59";
        var chartTextColor = "#848e9c";
        if(retVal != "LIGHT"){
            styleName = "/css/chart.css";
            chartBGColor = "#10161A";
            chartGridColor ="#3a4c59";
            chartTextColor = "#848e9c";
        }else{
            chartBGColor = "#ffffff";
            chartGridColor ="#e1ecf2";
            chartTextColor = "#55555";
            styleName = "/css/chart-light.css";
        }

        var upColor = "#70a800";
        var downColor = "#ea0070";



        if (this.widget != null) {
            this.widget.remove();
        }
        this.widget = new TradingView.widget({
            fullscreen: true,
            symbol: marketPair,
            autosize: true,
            allow_symbol_change: false,
            save_image: true,
            hideideas: true,
            hideSymbolSearch: true,
            debug: false,
            interval: '60',
            container_id: "chart-container",
            datafeed: new Datafeeds.UDFCompatibleDatafeed(url),
            library_path: "/charting_library/",
            locale: getParameterByName("lang") || 'en',
            charts_storage_url: 'https://saveload.tradingview.com',

            drawings_access: { type: 'black', tools: [{ name: "Trend Line" }] },
            charts_storage_api_version: "1.1",
            client_id: 'exnce.com',
            user_id: 'exnce',
            style: 1,
            padding: 0,
            timezone: "Europe/Istanbul",
            pricescale: 100000000,
            volume_precision:8,
            disabled_features: ["volume_force_overlay", "header_symbol_search"],


            studies: [
                "Volume@tv-basicstudies",
                "MASimple@tv-basicstudies",
            ],
            "overrides": {
                "pricescale":100000000,
                "mainSeriesProperties.candleStyle.borderColor": "#C400CB",
                "mainSeriesProperties.candleStyle.borderDownColor": "#ea0070",
                "mainSeriesProperties.candleStyle.borderUpColor": "#70a800",
                "mainSeriesProperties.candleStyle.downColor": "#ea0070",
                "mainSeriesProperties.candleStyle.drawBorder": true,
                "mainSeriesProperties.candleStyle.drawWick": true,
                "mainSeriesProperties.candleStyle.upColor": "#70a800",
                "mainSeriesProperties.candleStyle.wickDownColor": "#ea0070",
                "mainSeriesProperties.candleStyle.wickUpColor": "#70a800",
                "mainSeriesProperties.showCountdown": false,
                "mainSeriesProperties.style": 1,
                "paneProperties.background":chartBGColor,
                "paneProperties.topMargin":20,
                "paneProperties.bottomMargin":5,
                "average true range.precision": 8,
                "scalesProperties.backgroundColor": "#151a1e",
                "scalesProperties.lineColor": "#182026",
                "scalesProperties.textColor": chartTextColor,
                "dataWindowProperties.background":"cyan",
                "study_Overlay@tv-basicstudies.areaStyle.color1": "blue",
                "study_Overlay@tv-basicstudies.areaStyle.color2": "blue",
                "study_Overlay@tv-basicstudies.areaStyle.linecolor": "blue",
                "study_Overlay@tv-basicstudies.barStyle.downColor": "blue",
                "study_Overlay@tv-basicstudies.barStyle.upColor": "blue",
                "study_Overlay@tv-basicstudies.lineStyle.color": "blue",

                "symbolWatermarkProperties.color": "rgba(0, 0, 0, 0)",
                "symbolWatermarkProperties.transparency": 85,

                "paneProperties.gridProperties.color": "#e1ecf2",
                "paneProperties.vertGridProperties.color": chartGridColor,
                "paneProperties.horzGridProperties.color": chartGridColor,
                "volumePaneSize": "medium",
                "mainSeriesProperties.showPriceLine": true

            },

            "dataWindowProperties":{
                "background":"rgba(255, 254, 206, 0.2)",
                border:"rgba( 96, 96, 144, 1)",
                font:"Verdana",
                fontBold:false,
                fontItalic:false,
                fontSize:10,
                transparency:80,
                visible: true},

            "studies_overrides": {
                "precision": 8,
                "volume.volume.color.0": downColor,
                "volume.volume.color.1": upColor,
                "volume.volume.transparency": 70,
                "volume.volume ma.color": "#FF0000",
                "volume.volume ma.transparency": 30,
                "volume.volume ma.linewidth": 5,
                "volume.show ma": true,
                "bollinger bands.median.color": "#33FF88",
                "bollinger bands.upper.linewidth": 7

            },

            popup_width: 800,
            popup_height: 250,
            precision: 4,

            // custom_css_url: localStorage.theme && localStorage.theme == "black" ? '/css/chart-dark.css' : '/css/chart.css',
            custom_css_url: styleName,
            theme: 'Dark',//localStorage.theme && localStorage.theme == "black" ? 'Dark' : 'Light', //White, Grey, Blue, Black

            enabled_features: [
                //"create_volume_indicator_by_default",
                //"use_localstorage_for_settings",
                "dont_show_boolean_study_arguments",
                "hide_last_na_study_output",
                "header_chart_type",
                "save_chart_properties_to_local_storage",
                "adaptive_logo",
                "header_widget",
                "symbol_info",
                "header_compare",
                "header_chart_type",
                "display_market_status",
                "symbol_search_hot_key",
                "compare_symbol",
                "border_around_the_chart",
                "remove_library_container_border",
                "symbol_info",
                "header_interval_dialog_button",
                "show_interval_dialog_on_key_press",
                //"volume_force_overlay",
                "move_logo_to_main_pane",
                "study_templates",
                "control_bar",
                "keep_left_toolbar_visible_on_small_screens",
            ],

        });
    }
}