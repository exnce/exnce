function startAnimation() {
    $("#logo-loading").removeClass("rotate-loading").addClass("rotate-loading");

}
function stopAnimation() {
    $("#logo-loading").removeClass("rotate-loading");

}

var EXNCE = {};

(function () {
    function Metamask(CSRF) {
        this.initialize(CSRF);
    }

    Metamask.prototype = {
        initialize: function (CSRF) {
            this._network = 1;// ethereum main network
            this.CSRF_TOKEN = CSRF;
            this._metamask = this;
            this._reserve_wallet = "YOUR_ETHEREUM_ERC20_RESERVE_WALLET_ADDRESS";
            this._message =  "Signing this message proves your ownership of your Ethereum wallet address to EXNCE without giving EXNCE access to any sensitive information. Message ID: @";
            this._account = "-";
            this._signature="-";
            this._timestamp = 0;



            this.getTimeStamp= function(){
                return this._timestamp;
            }

            this.setTimeStamp= function(ts){
                this._timestamp = ts;
            }

            this.getSignature= function(){
                return this._signature;
            }

            this.setSignature= function(signature){
                this._signature = signature;
            }

            this.getAccount= function(){
                return this._account;
            }

            this.setAccount= function(account){
                this._account = account;
            }

            this.getSignedMessage = function () {
                return this._message;
            }

            this.setSignedMessage = function(data){
                this._message = data;
            }

            this.checkAccount = function(userAccount,callback){
                this.szTitle = "Checking Account!";
                if(userAccount.toString().toLowerCase() === this.getAccount().toLowerCase()){
                    this.szMsgType = "info";
                    this.szDescription = 'Account is valid.';
                    this.bResult = true;
                }else{
                    this.szMsgType = "error";
                    this.szDescription = 'Account is incorrect! Please switch metamask account to you are logged in EXNCE';
                    this.bResult = false;
                }
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription});
                return this.bResult;
            }

            this.isInstalled = function(callback){
                this.szTitle = "Metamask Installation";
                this.bResult = false;
                if (typeof window.web3 !== 'undefined'){
                    this.szMsgType = "success";
                    this.szDescription = 'MetaMask is installed';
                    this.bResult = true;
                }else if(typeof window.ethereum !== 'undefined') {
                    this.szMsgType = "success";
                    this.szDescription = 'MetaMask is installed';
                    this.bResult = true;
                } else{
                    this.szMsgType = "error";
                    this.szDescription = 'MetaMask is not installed';
                    this.bResult = false;
                }
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription});
                return this.bResult;
            }

            this.isLocked =  async function(callback){
                let that = this;
                var retObject={bResult:false,szMsgType:"",szDescription:"",szAccount:""};
                this.szTitle = "Metamask Status";

                if (typeof window.ethereum !== 'undefined'){
                        await ethereum.enable().then((account) => {
                            let bNetworkStatus = true;
                            if(typeof ethereum != "undefined"){
                                if(typeof ethereum.networkVersion != "undefined"){
                                    if (ethereum.networkVersion != that._network) {
                                        retObject.szMsgType = "error";
                                        retObject.szDescription = 'This application requires the main network, please switch it in your MetaMask UI.';
                                        retObject.bResult = false;
                                        bNetworkStatus = false;
                                    }
                                }
                            }
                            if(bNetworkStatus){
                                web3.eth.defaultAccount = account[0];
                                that.setAccount(account[0]);
                                retObject.bResult = true;
                                retObject.szMsgType = "success";
                                retObject.szDescription = 'Metamask is accessible...';
                            }

                        }).catch(function (reason) {
                            retObject.szMsgType = "error";
                            retObject.szDescription = reason.stack;
                            retObject.bResult = false;
                        });
                } else if (typeof window.web3 !== 'undefined') {
                    let promise = new Promise((valid, invalid) => {
                        web3.eth.getAccounts(function(err, accounts){
                            if (err != null) {
                                retObject.szMsgType = "error";
                                retObject.szDescription = err;
                                retObject.bResult = false;
                                retObject.szAccount = "";
                            } else if (accounts.length === 0) {
                                retObject.szMsgType = "error";
                                retObject.szDescription = 'Could not read your accounts from MetaMask. Try unlocking it.';
                                retObject.bResult = false;
                                retObject.szAccount = "";
                            } else {
                                retObject.szMsgType = "info";
                                retObject.szDescription = 'MetaMask is accessible';
                                retObject.bResult = true;
                                retObject.szAccount = accounts[0]
                            }
                            valid(retObject);
                        });
                    });
                    let result = await promise;
                    that.setAccount(result.szAccount);
                    retObject.bResult = result.bResult;
                    retObject.szMsgType =  result.szMsgType;
                    retObject.szDescription = result.szDescription;
                }else{
                    let msg = "type of window.ethereum :"+typeof window.ethereum;
                    msg += "\r\ntype of window.web3:"+typeof window.web3;
                    msg += "\r\nMetaMask is not accessible!";

                    retObject.szMsgType = "error";
                    retObject.szDescription = msg;
                    retObject.bResult = false;
                }
                this.szMsgType = retObject.szMsgType;
                this.szDescription = retObject.szDescription;
                this.bResult = retObject.bResult;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription});
                return this.bResult;
            }

            this.checkNetwork = async function(callback) {
                let that = this;
                var retObject={szTitle:"",bResult:false,szMsgType:"",szDescription:""};
                retObject.szTitle = "Ethereum Network";
                let networkState = new Promise((valid, invalid) => {
                    web3.version && web3.version.getNetwork((err, netId) => {
                        if (err != null) {
                            retObject.szMsgType = "error";
                            retObject.szDescription = err;
                            retObject.bResult = false;
                        } else if(netId != that._network){
                            retObject.szMsgType = "error";
                            retObject.szDescription = 'This application requires the main network, please switch it in your MetaMask UI.';
                            retObject.bResult = false;
                        } else{
                            retObject.szMsgType = "info";
                            retObject.szDescription = 'Metamask is working on ethereum main network.';
                            retObject.bResult = true;
                        }
                        valid(retObject);
                    });
                });

                let result =  await networkState;
                this.szTitle = result.szTitle;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription});
                return this.bResult;
            }

            this.signMessage = async function(callback) {
                this.szTitle = "Metamask Sign";
                this.szMsgType = "info";
                this.szDescription = 'Signing Message...';
                this.bResult = false;
                var ts = new Date().getTime();
                var message = this.getSignedMessage()+ts+".";
                var that = this;
                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let promise = new Promise((valid, invalid) => {
                    web3.personal.sign(web3.toHex(message),that.getAccount(),function (err,res) {
                       if (err != null) {
                           retObject.szMsgType = "error";
                           retObject.szTitle="MetaMask Message Signature";
                           retObject.szDescription = "User denied message signature...";
                           retObject.bResult = false;
                       }else{
                           retObject.szMsgType = "success";
                           retObject.szTitle="MetaMask Message Signature";
                           retObject.szDescription = "User accepted message signature...";
                           retObject.bResult = true;
                           retObject.szData = res;
                           that.setSignature(res);
                           that.setTimeStamp(ts)
                       }
                       valid(retObject);
                    });
                });
                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,message:message,ts:ts,signature:this.szData});
                return this.bResult;
            }

            this.verifyMessage = async function(callback) {
                this.szTitle = "Metamask Verify Message";
                this.szMsgType = "info";
                this.szDescription = 'Verifying Message...';
                this.bResult = false;
                var ts = this.getTimeStamp();
                var message = this.getSignedMessage()+ts+".";
                var that = this;
                var signature = this.getSignature();
                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let promise = new Promise((valid, invalid) => {
                    message = web3.toHex(message);
                    web3.personal.ecRecover(message, signature, function(error, result) {
                        if(!error) {
                            if(result.toLowerCase() === that.getAccount().toLowerCase()){
                                retObject.szMsgType = "success";
                                retObject.szTitle="MetaMask Message Signature";
                                retObject.szDescription = "Message has been verified successfuly!";
                                retObject.bResult = true;
                                retObject.szData = result;
                            }else{
                                retObject.szMsgType = "error";
                                retObject.szTitle="MetaMask Message Signature";
                                retObject.szDescription = "Message cannot be verified!";
                                retObject.bResult = false;
                                retObject.szData = null;
                            }
                            valid(retObject);
                        } else {
                            retObject.szMsgType = "error";
                            retObject.szTitle="Message cannot be verified!";
                            retObject.szDescription = error;
                            retObject.bResult = false;
                            retObject.szData = null;
                            valid(retObject);
                        }
                    });

                });
                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,message:message,ts:ts,signature:this.szData});
                return this.bResult;
            }


            this.logIn = async function(check = false, callback) {
                this.szTitle = "Metamask Authentication";
                this.szMsgType = "info";
                this.szDescription = 'Processing...';
                this.bResult = false;
                var key = this.getTimeStamp();
                var message = this.getSignedMessage()+key+".";
                var that = this;
                var signature = this.getSignature();
                var account = this.getAccount();
                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let promise = new Promise((valid, invalid) => {
                    let postData = {"auth_type":"metamask","_token": that.CSRF_TOKEN,"check":check,"account":account, "signature":signature,"output":account,"message":message,"key":key};
                    $.ajax({
                        type: "POST",
                        url: "/loginvia",
                        data: postData,
                        crossDomain: true,
                        success: function (data) {
                            if(data.success === true){
                                retObject.szMsgType = "success";
                                retObject.szTitle="MetaMask Authentication";
                                retObject.szDescription = 'MetaMask authentication successfuly!';
                                retObject.bResult = data.success;
                                retObject.szData = null;
                            }else{
                                retObject.szMsgType = "error";
                                retObject.szTitle="MetaMask Message Signature";
                                retObject.szDescription = 'MetaMask authentication failed!';
                                retObject.bResult = false;
                                retObject.szData = null;
                            }
                            valid(retObject);
                        },
                        failure: function (errMsg) {
                            retObject.szMsgType = "error";
                            retObject.szTitle="MetaMask Message Signature";
                            retObject.szDescription = 'MetaMask authentication failed!';
                            retObject.bResult = false;
                            retObject.szData = null;
                            valid(retObject);
                        },
                        error: function(xhr, textStatus, errorThrown) {
                            retObject.szMsgType = "error";
                            retObject.szTitle="MetaMask Message Signature";
                            retObject.szDescription = 'MetaMask authentication failed!';
                            retObject.bResult = false;
                            retObject.szData = null;
                            valid(retObject);
                        }
                    });
                });
                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,message:message,ts:key,signature:this.szData});
                return this.bResult;
            }

            this.checkBalance = async function(token, callback) {
                let that = this;
                this.szTitle = "Balance Checking";
                this.szMsgType = "info";
                this.szDescription = 'Checking Balance...';
                this.bResult = false;

                let minABI = [
                    {
                        "constant":true,
                        "inputs":[{"name":"_owner","type":"address"}],
                        "name":"balanceOf",
                        "outputs":[{"name":"balance","type":"uint256"}],
                        "type":"function"
                    },
                    {
                        "constant":true,
                        "inputs":[],
                        "name":"decimals",
                        "outputs":[{"name":"","type":"uint8"}],
                        "type":"function"
                    }
                ];

                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let promise = new Promise((valid, invalid) => {
                    if(token === ""){
                        web3.eth.getBalance(that.getAccount(), (err, balance) => {
                            if(!err){
                                retObject.szData = web3.fromWei(balance,"ether")
                                retObject.szMsgType = "success";
                                retObject.bResult = true;
                                retObject.szDescription = 'Balance received successfuly.';
                            }else{
                                retObject.szData = "0";
                                retObject.szMsgType = "error";
                                retObject.bResult = false;
                                retObject.szDescription = 'Balance cannot be received!';
                            }
                            valid(retObject);
                        });
                    }else{
                        let contract = web3.eth.contract(minABI).at(token);
                        contract.balanceOf(that.getAccount(), (error, balance) => {
                            contract.decimals((err, decimals) => {
                                if(!err){
                                    balance = balance.div(10**decimals);
                                    retObject.szData = balance.toString();
                                    retObject.szMsgType = "success";
                                    retObject.szDescription = 'Balance received successfuly.';
                                    retObject.bResult = true;
                                }else{
                                    retObject.szData = "0";
                                    retObject.bResult = false;
                                    retObject.szMsgType = "error";
                                    retObject.szDescription = 'Balance cannot be received!';
                                }
                                valid(retObject);
                            });
                        });
                    }
                });
                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,balance:this.szData});
                return this.bResult;
            }


            this.sendEthereum = async function(contract,amount) {
                this.szTitle = "ETH Deposit";
                this.szMsgType = "info";
                this.szDescription = 'Processing...';
                this.bResult = false;
                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let depositAmount = web3.toHex(web3.toWei(web3.toBigNumber(amount)));
                const method = 'eth_sendTransaction';
                const payload = {
                    method: method,
                    params: [{
                        from: this.getAccount(),
                        to: this._reserve_wallet,
                        value: depositAmount,
                    }],
                    from: this.getAccount(),
                };
                const provider = window['ethereum'] || window.web3.currentProvider;
                let promise = new Promise((valid, invalid) => {
                    provider.sendAsync(payload, function (err, response) {
                        const rejected = 'User denied transaction signature.';
                        if (response.error && response.error.message.includes(rejected)) {
                            retObject.szData = "";
                            retObject.bResult = false;
                            retObject.szMsgType = "error";
                            retObject.szDescription = "We can\'t take your money without your permission.";
                            valid(retObject);
                        }
                        if (err) {
                            retObject.szData = "";
                            retObject.bResult = false;
                            retObject.szMsgType = "error";
                            retObject.szDescription = "There was an issue, please try again.";
                            valid(retObject);
                        }
                        if (response.result) {
                            retObject.szData = response.result;
                            retObject.bResult = true;
                            retObject.szMsgType = "info";
                            retObject.szDescription = "Thank you for your generosity!";
                            valid(retObject);
                        }
                    })
                });
                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                return {success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,tx:this.szData};
            }

            this.sendToken = async function(contract,amount) {
                this.szTitle = "Token Deposit";
                this.szMsgType = "info";
                this.szDescription = 'Processing...';
                this.bResult = false;
                var retObject={bResult:false,szMsgType:"",szDescription:"",szData:null};
                let depositAmount = web3.toHex(web3.toWei(web3.toBigNumber(amount)));

                let promise = new Promise((valid, invalid) => {
                        retObject.szData = "";
                        retObject.bResult = false;
                        retObject.szMsgType = "error";
                        retObject.szDescription = "Token deposits currently under of maintenance!";
                        valid(retObject);
                });

                let result = await promise;
                this.szMsgType = result.szMsgType;
                this.szDescription = result.szDescription;
                this.bResult = result.bResult;
                this.szData = result.szData;
                return {success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,tx:this.szData};
            }


            this.sendDeposit = async function(contract,amount,callback) {
                var that = this;
                let promise;
                if(contract != ""){
                    promise = new Promise((valid, invalid) => {
                        valid(that.sendToken(contract,amount));
                    });
                }else{
                     promise = new Promise((valid, invalid) => {
                        valid(that.sendEthereum(contract,amount));
                    });
                }
                let result = await promise;
                this.szMsgType = result.type;
                this.szDescription = result.description;
                this.bResult = result.success;
                this.szData = result.tx;
                this.szTitle = result.title;
                callback({success:this.bResult,type:this.szMsgType,title:this.szTitle,description:this.szDescription,balance:this.szData});
                return this.bResult;
            }

        }
    }
    EXNCE.Metamask = Metamask;
}());

var MetamaskAuth = new EXNCE.Metamask($('meta[name="csrf-token"]').attr('content'));

function logData(data){
    showNotify(data.type, data.title,data.description);
}

async function checkMetamask(login,e) {
    startAnimation();

    if (MetamaskAuth.isInstalled(function (data) {
        logData(data);
    }).then === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.isLocked(function (data) {
        logData(data);
        $("#edtAddress").val(MetamaskAuth.getAccount());
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.checkNetwork(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.signMessage(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.verifyMessage(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.logIn(login,function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }else{
        window.location = "/personal";
    }
}


$('#metamaskLogin').on('click', function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    checkMetamask(false);
});



async function metamaskCheckBalance(balanceInput,contractObject,accountObject) {
    let contractAddr = $(contractObject).val();
    let userAccount = $(accountObject).val();

    if (MetamaskAuth.isInstalled(function (data) {
        logData(data);
    }).then === false) {
        return false;
    }

    if (await MetamaskAuth.isLocked(function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.checkNetwork(function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.checkAccount(userAccount, function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.checkBalance(contractAddr, function (data) {
        $(balanceInput).val(data.balance);
        logData(data);
    }) === false) {
        return false;
    }


}

async function metamaskMakeDeposit(depositAmount,contractObject,accountObject) {
    let contractAddr = $(contractObject).val();
    let userAccount = $(accountObject).val();
    let totalAmount = $(depositAmount).val();
    if (MetamaskAuth.isInstalled(function (data) {
        logData(data);
    }).then === false) {
        return false;
    }

    if (await MetamaskAuth.isLocked(function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.checkNetwork(function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.checkAccount(userAccount, function (data) {
        logData(data);
    }) === false) {
        return false;
    }

    if (await MetamaskAuth.sendDeposit(contractAddr,totalAmount, function (data) {
        logData(data);
    }) === false) {
        return false;
    }
}

async function metamaskSwap(check){
    if (MetamaskAuth.isInstalled(function (data) {
        logData(data);
    }).then === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.isLocked(function (data) {
        logData(data);
        $("#edtAddress").val(MetamaskAuth.getAccount());
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.checkNetwork(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.signMessage(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.verifyMessage(function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }

    if (await MetamaskAuth.logIn(check,function (data) {
        logData(data);
    }) === false) {
        stopAnimation();
        return false;
    }else{
       $(".btnConnectToMetamask").addClass("disabled").prop('disabled', true);
       $(".btnConvertMyAccount").removeClass("disabled").prop('disabled', false);
    }
}
$('.btnConnectToMetamask').on('click', function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    metamaskSwap(true);
});