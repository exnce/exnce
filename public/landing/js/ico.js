var clock;
var selectedLanguage;

$(document).ready(function() {
    // Set dates.
    var futureDate  = new Date("February 21, 2019 00:00 AM GMT+3");
    var currentDate = new Date();

    // Calculate the difference in seconds between the future and current date
    var diff = futureDate.getTime() / 1000 - currentDate.getTime() / 1000;

    // Calculate day difference and apply class to .clock for extra digit styling.
    function dayDiff(first, second) {
        return (second-first)/(1000*60*60*24);
    }

    if (dayDiff(currentDate, futureDate) < 100) {
        $('.clock').addClass('twoDayDigits');
    } else {
        $('.clock').addClass('threeDayDigits');
    }

    if(diff < 0) {
        diff = 0;
    }

    // Instantiate a coutdown FlipClock
    clock = $('.clock').FlipClock(diff, {
        clockFace: 'DailyCounter',
        countdown: true,
        language: selectedLanguage,
        showSeconds: false
    });


    /* Initialize Swiper */
    var swiper = new Swiper('.swiper-container', {
        slidesPerView: 4,
        grabCursor: true,
        navigation: {
            nextEl: '.next-slide',
            prevEl: '.prev-slide',
        },
        /* Responsive breakpoints */
        breakpoints: {
            /* when window width is <= 576px */
            576: {
                slidesPerView: 1,
            },
            /* when window width is <= 767px */
            767: {
                slidesPerView: 2,
            },
            /* when window width is <= 992px */
            992: {
                slidesPerView: 3,
            }
        }
    });

    if ($(window).width() < 992) {
        swiper.slideTo(2, 1000, false);
    }

    $(window).resize(function() {
        if ($(window).width() < 992) {
            swiper.slideTo(2, 1000, false);
        }
    });


    $('#news').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]

    });

    /* Video Modal */

    var $videoSrc;
    $('.video-btn').click(function() {
        $videoSrc = $(this).data("src");
    });

    $('#ico-modal').on('shown.bs.modal', function(e) {

        $("#video").attr('src', $videoSrc + "?autoplay=1&amp;rel=0&amp;showinfo=0&amp;modestbranding=1");
    })

    $('#ico-modal').on('hide.bs.modal', function(e) {
        $("#video").attr('src', $videoSrc);
    })


    /* Join US */
    $('.join-us').click(function() {
        window.open($(this).data("src"), '_blank');
    });

    $('.buy-on-latoken').click(function() {
        window.open($(this).data("src"), '_blank');
    });

    /* Menu navbar toggler animation */
    $('.navbar .navbar-toggler').click(function(event) {
        console.log("asd");
        $('.navbar.mobile').toggleClass('open', 2000, "swing");
    });


});
