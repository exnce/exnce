<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDecimalPlacesToCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coins', function (Blueprint $table) {
            $table->integer("decimals")->after('slug')->default(8);
            $table->string('dec_point', 1)->after("decimals")->default(".");
            $table->string('thousands_sep', 1)->after("dec_point")->default(",");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coins', function (Blueprint $table) {
            $table->dropColumn("decimals");
            $table->dropColumn("dec_point");
            $table->dropColumn("thousands_sep");
        });
    }
}
