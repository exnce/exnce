<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCoinDetailsToCoinDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coin_details', function (Blueprint $table) {
            $table->string("coinmarketcap")->after("medium")->nullable();
            $table->string("coingecko")->after("coinmarketcap")->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coin_details', function (Blueprint $table) {
            $table->dropColumn("coinmarketcap");
            $table->dropColumn("coingecko");
        });
    }
}
