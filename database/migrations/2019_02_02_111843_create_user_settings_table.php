<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateUserSettingsTable extends Migration
{

    use SoftDeletes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("country_id");
            $table->string("city");
            $table->integer("crypto_withdraw_email")->default(1);
            $table->integer("crypto_withdraw_sms")->default(0);
            $table->integer("crypto_deposit_email")->default(1);
            $table->integer("crypto_deposit_sms")->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_settings');
    }
}
