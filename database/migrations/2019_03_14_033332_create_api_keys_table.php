<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    use SoftDeletes;
    public function up()
    {
        Schema::create('user_api_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->longText("private_key")->default("");
            $table->longText("public_key")->default("");
            $table->longText("secret_key")->default("");
            $table->boolean("can_access_user_info")->default(false);
            $table->boolean("can_access_balance_info")->default(false);
            $table->boolean("can_create_order")->default(false);
            $table->boolean("can_create_withdraw")->default(false);
            $table->integer("status")->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_api_keys');
    }
}
