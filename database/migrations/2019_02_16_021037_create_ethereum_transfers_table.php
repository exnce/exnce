<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateEthereumTransfersTable extends Migration
{

    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ethereum_transfers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("deposit_id");
            $table->integer("coin_id");
            $table->string("txid",66);
            $table->string("address",42);
            $table->decimal("amount",20,8)->default(0.00);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ethereum_transfers');
    }
}
