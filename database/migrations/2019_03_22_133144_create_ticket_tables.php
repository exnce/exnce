<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateTicketTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    use SoftDeletes;

    public function up()
    {
        Schema::create('ticket_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('key');
            $table->string('color');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_priorities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string("key");
            $table->string('color');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string("key");
            $table->string('color');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_categories_users', function (Blueprint $table) {
            $table->integer('category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->longText('content')->nullable();
            $table->integer('status_id')->unsigned();
            $table->integer('priority_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('agent_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->timestamp('completed_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->text('content');
            $table->longText('html')->nullable();
            $table->longText('content')->change();
            $table->integer('user_id')->unsigned();
            $table->integer('ticket_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_audits', function (Blueprint $table) {
            $table->increments('id');
            $table->text('operation');
            $table->integer('user_id')->unsigned();
            $table->integer('ticket_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('ticket_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang')->unique()->nullable();
            $table->string('slug')->unique()->index();
            $table->mediumText('value');
            $table->mediumText('default');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ticket_audits');
        Schema::drop('ticket_comments');
        Schema::drop('tickets');
        Schema::drop('ticket_categories_users');
        Schema::drop('ticket_categories');
        Schema::drop('ticket_priorities');
        Schema::drop('ticket_statuses');
        Schema::drop('ticket_settings');

    }
}
