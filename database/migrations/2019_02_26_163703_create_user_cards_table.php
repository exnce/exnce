<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateUserCardsTable extends Migration
{

    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
/*
        ID UserID CartType CardNFCID CardICID CardMCID CardPrivateID CartPIN CartNO CartExpirationDate CardCVV SoftDeletes

public String atr;
    public String cardNo;
    public String cardSerialNo;
    public int cardType;
    public String countryCode;
    public String expireDate;
    public boolean isCardSerialNo;
    public String serviceCode;
    public String track1;
    public String track2;
    public String track3;
    public String uuid;*/
        Schema::create('user_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->enum('type', ['MAG', 'SCC0', 'SCCL0', 'Mifare', 'SAM0', 'Felica']);
            $table->integer("card_countrycode");
            $table->string("card_nfcid");
            $table->string("card_icid");
            $table->string("card_mcid");
            $table->string("card_privateid");
            $table->string("card_pin");
            $table->string("card_no");
            $table->string("card_expirationdate");
            $table->string("card_cvv");
            $table->string("card_servicecode");
            $table->string("track1");
            $table->string("track2");
            $table->string("track3");
            $table->string("uuid");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_cards');
    }
}
