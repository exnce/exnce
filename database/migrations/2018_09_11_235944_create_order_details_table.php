<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    use SoftDeletes;

    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("order_id");
            $table->integer("txSeq");
            $table->string("txFrom",100);
            $table->string("txTo",100);
            $table->string("txAmount",50);
            $table->string("txCurrency",50);
            $table->string("txDesc",255);
            $table->string("txBlock",255);
            $table->string("txHash",255);
            $table->string("txCreateDate",25);
            $table->string("txConfirmDate",25);
            $table->integer("txConfirmRate");
            $table->integer("txStatus");
            $table->text("json_data");


            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
