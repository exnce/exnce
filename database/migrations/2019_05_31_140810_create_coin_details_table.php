<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinDetailsTable extends Migration
{
    use SoftDeletes;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coin_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("coin_id");
            $table->longText("description")->nullable();
            $table->string("video")->nullable();
            $table->string("whitepaper")->nullable();
            $table->string("website")->nullable();
            $table->string("telegram")->nullable();
            $table->string("github")->nullable();
            $table->string("twitter")->nullable();
            $table->string("reddit")->nullable();
            $table->string("facebook")->nullable();
            $table->string("youtube")->nullable();
            $table->string("email")->nullable();
            $table->string("medium")->nullable();
            $table->string("phone")->default("");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coin_details');
    }
}
