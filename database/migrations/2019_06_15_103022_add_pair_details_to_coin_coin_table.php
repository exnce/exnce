<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPairDetailsToCoinCoinTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coin_coin', function (Blueprint $table) {
            $table->decimal("last_volume",20,8)->after("pair_id")->default(0.00);
            $table->decimal("last_change",20,8)->after("last_volume")->default(0.00);
            $table->decimal("last_price",20,8)->after("last_change")->default(0.00);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coin_coin', function (Blueprint $table) {
            $table->dropColumn('last_volume');
            $table->dropColumn('last_change');
            $table->dropColumn('last_price');
        });
    }
}
