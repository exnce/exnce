<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateIcoCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    use SoftDeletes;


    public function up()
    {
        Schema::create('ico_cards', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("coin_id");
            $table->decimal("amount",20,8)->default(0.00);
            $table->dateTime("start_date");
            $table->dateTime("finish_date");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ico_cards');
    }
}
