<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankAndCurrencyDetailsToBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->integer("bank_id")->after('id');
            $table->integer("currency_id")->after('bank_id');
            $table->string("surname",50)->after('name')->default("");
            $table->tinyInteger("status")->after('iban')->default(0);
            $table->dropColumn("currency");
            $table->dropColumn("holder");


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
            $table->dropColumn("bank_id");
            $table->dropColumn("currency_id");
            $table->dropColumn("surname");
            $table->dropColumn("status");
            $table->string("holder",50)->after('iban')->default("");
            $table->string("currency",3)->after('holder')->default("");

        });
    }
}
