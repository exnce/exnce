<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullTextIndexToTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {

            //$table->dropColumn("transaction_type");

            DB::statement('ALTER TABLE transactions ADD FULLTEXT fulltext_ref_code_index (ref_code)');
            $table->integer("transaction_type")->after("currency_id")->default(0);
            $table->index('user_id');
            $table->index("coin_id");
            $table->index("transaction_type");
            $table->index("currency_id");
            $table->index("status");
            $table->index("price");
            $table->index("completed_amount");
            $table->index("remaining_amount");
            $table->index("total");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            DB::statement('ALTER TABLE transactions DROP INDEX fulltext_ref_code_index');
            $table->dropIndex(['user_id']);
            $table->dropIndex(["coin_id"]);
            $table->dropIndex(["currency_id"]);
            $table->dropIndex(["transaction_type"]);
            $table->dropIndex(["status"]);
            $table->dropIndex(["price"]);
            $table->dropIndex(["completed_amount"]);
            $table->dropIndex(["remaining_amount"]);
            $table->dropIndex(["total"]);
            $table->dropColumn("transaction_type");

        });
    }
}
