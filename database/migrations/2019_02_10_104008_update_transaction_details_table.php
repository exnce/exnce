<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            $table->enum('type', ['buy', 'sell'])->after("user_id");
            $table->integer("currency_id")->after("user_id");
            $table->integer("coin_id")->after("user_id");

            $table->decimal("amount",20,8)->after("type")->default(0.00);
            $table->decimal("fee",20,8)->after("total")->default(0.00);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_details', function (Blueprint $table) {
            $table->dropColumn("coin_id");
            $table->dropColumn("currency_id");
            $table->dropColumn("type");
            $table->dropColumn("amount");
            $table->dropColumn("fee");
        });
    }
}
