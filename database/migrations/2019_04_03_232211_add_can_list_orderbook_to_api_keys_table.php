<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCanListOrderbookToApiKeysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_api_keys', function (Blueprint $table) {
            $table->boolean("can_access_orderbook")->default(false)->after("can_create_withdraw");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_api_keys', function (Blueprint $table) {
            $table->dropColumn("can_access_orderbook");
        });
    }
}
