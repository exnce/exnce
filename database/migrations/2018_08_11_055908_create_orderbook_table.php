<?php

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderbookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    use SoftDeletes;
    public function up()
    {
        Schema::create('orderbooks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->integer("coin_id");
            $table->integer("currency_id");
            $table->enum('type', ['buy', 'sell']);
            $table->decimal("amount",20,8)->default(0.00);
            $table->decimal("price",20,8)->default(0.00);
            $table->decimal("total",20,8)->default(0.00);
            $table->decimal("fee",20,8)->default(0.00);
            $table->decimal("real_amount",20,8)->default(0.00);
            $table->decimal("completed_amount",20,8)->default(0.00);
            $table->decimal("remaining_amount",20,8)->default(0.00);
            $table->boolean("is_completed")->default(true);
            $table->tinyInteger("status")->default(0);
            $table->string("ref_code");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderbooks');
    }
}
