<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserIdToWalletAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('wallet_addresses', function (Blueprint $table) {
            $table->bigInteger("user_id")->after("id")->default(0);
            $table->bigInteger("wallet_id")->after("user_id")->default(0);
            $table->string("password")->change();
            $table->string("address")->change();
            $table->string("destination_tag")->after("payment_id")->default("");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('wallet_addresses', function (Blueprint $table) {
            $table->dropColumn("user_id");
            $table->dropColumn("wallet_id");
            $table->string("address",32)->change();
            $table->string("password",32)->change();
            $table->dropColumn("destination_tag");

        });
    }
}
