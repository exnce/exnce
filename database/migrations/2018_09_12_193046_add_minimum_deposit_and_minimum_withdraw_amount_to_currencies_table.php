<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMinimumDepositAndMinimumWithdrawAmountToCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->decimal("min_deposit",20,8)->after("thousands_sep")->default(0.00);
            $table->decimal("min_withdraw",20,8)->after("min_deposit")->default(0.00);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('currencies', function (Blueprint $table) {
            $table->dropColumn('min_deposit');
            $table->dropColumn('min_withdraw');
        });
    }
}
