<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateIdentityApprovalsTable extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('identity_approvals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("user_id");
            $table->string("hash");
            $table->string("id_card_front");
            $table->string("id_card_back");
            $table->string("selfie");
            $table->tinyInteger("status")->default(0);
            $table->string("description");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('identity_approvals');
    }
}

