<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;


class CreateAirdropTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    use SoftDeletes;

    public function up()
    {
        Schema::create('airdrops', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("coin_id");
            $table->string("name");
            $table->string("model");
            $table->decimal("amount",20,8)->default(0.00);
            $table->integer("followers")->default(0);
            $table->longText("description");
            $table->dateTime("start_date");
            $table->dateTime("finish_date");
            $table->tinyInteger("status")->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airdrops');
    }
}
