<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateMetamaskTable extends Migration
{
    use SoftDeletes;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metamasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger("user_id")->default(-1);
            $table->string("address")->unique();;
            $table->string("key");
            $table->text("signature");
            $table->boolean("is_valid")->default(false);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metamasks');
    }
}
