<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFeeToOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->string("txFee",255)->after("txBlock");
            $table->string("txData",255)->after("txFee");
            $table->string("txPrevHash",255)->after("txData");

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details', function (Blueprint $table) {
            $table->dropColumn("txFee");
            $table->dropColumn("txData");
            $table->dropColumn("txPrevHash");
        });
    }
}
