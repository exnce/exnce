<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeFieldToUserBanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_banks', function (Blueprint $table) {
            $table->boolean("currency_type")->after('user_id')->default(true);
            $table->integer("currency_id")->after('currency_type');
            $table->string("payment_address",120)->after('currency_id');
            $table->string("destination_tag",120)->after('payment_address');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_banks', function (Blueprint $table) {
            $table->dropColumn("currency_type");
            $table->dropColumn("currency_id");
            $table->dropColumn("payment_address");
            $table->dropColumn("destination_tag");
        });
    }
}
