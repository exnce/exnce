<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CoinTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $ERC20Explorer = "https://etherscan.io/tx/%s";
        $BTCExplorer = "https://www.blockchain.com/btc/tx/%s";
        $USDTExplorer = "https://omniexplorer.info/tx/%s";
        $XLMExplorer = "https://stellarchain.io/tx/%s";
        $XRPExplorer = "https://bithomp.com/explorer/%s";

      //  DB::table('coins')->truncate();


        $coins = [
            ["name" => "Ethereum", "symbol" => "ETH", "slug" => "eth", "status" => 0, "min_deposit" => 0.01, "decimals" => 8,"min_withdraw" => 0.003, "fee" => 0.001, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Pundi X", "symbol" => "NPXS", "contract" => "0xa15c7ebe1f07caf6bff097d8a589fb8ac49ae5b3", "slug" => "npxs", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 500, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Holo", "symbol" => "HOT", "contract" => "0x6c6ee5e31d828de241282b9606c8e98ea48526e2", "slug" => "hot", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 500, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "OmiseGo", "symbol" => "OMG", "contract" => "0xd26114cd6ee289accf82350c8d8487fedb8a0c07", "slug" => "omg", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 1, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Augur", "symbol" => "REP", "contract" => "0x1985365e9f78359a9b6ad760e32412f4a445e862", "slug" => "rep", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 0.1, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Binance", "symbol" => "BNB", "contract" => "0xf629cbd94d3791c9250152bd8dfbdf380e2a3b9c", "slug" => "bnb", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 0.4, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Enjin", "symbol" => "ENJ", "contract" => "0xb8c77482e45f1f44de1745f52c74426c631bdd52", "slug" => "enj", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Zilliqa", "symbol" => "ZIL", "contract" => "0x05f4a42e251f2d52b8ed15e9fedaacfcef1fad27", "slug" => "zil", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Decentraland", "symbol" => "MANA", "contract" => "0x0f5d2fb29fb7d3cfee444a200298f468908cc942", "slug" => "mana", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "0x", "symbol" => "ZRX", "contract" => "0xe41d2489571d322189246dafa5ebde1f4699f498", "slug" => "zrx", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Populous", "symbol" => "PPT", "contract" => "0xd4fa1460f537bb9085d22c7bccb5dd450ef28e3a", "slug" => "ppt", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Polymath", "symbol" => "POLY", "contract" => "0x9992ec3cf6a55b00978cddf2b27bc6882d88d1ec", "slug" => "poly", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "DigixDAO", "symbol" => "DGD", "contract" => "0xe0b7927c4af23765cb51314a0e0521a9645f0e2a", "slug" => "dgd", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Bancor","symbol" => "BNT", "contract" => "0x1f573d6fb3f13d689ff844b4ce37794d79a7ff1c", "slug" => "bnt", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Dent","symbol" => "DENT", "contract" => "0x3597bfd533a99c9aa083587b074434e61eb0a258", "slug" => "dent", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Herbalist Token","symbol" => "HERB", "contract" => "0x04a020325024f130988782bd5276e53595e8d16e", "slug" => "herb", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "TrueUSD","symbol" => "TUSD", "contract" => "0x0000000000085d4780B73119b644AE5ecd22b376", "slug" => "tusd", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Maker","symbol" => "MKR", "contract" => "0x9f8f72aa9304c8b593d555f12ef6589cc3a579a2", "slug" => "mkr", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Multicoin","symbol" => "MTCN", "contract" => "0xf6117cc92d7247f605f11d4c942f0feda3399cb5", "slug" => "mtcn", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Xaviera","symbol" => "XTS", "contract" => "0x36232b1328e49a043434e71c02c0dc2be278e975", "slug" => "xts", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Stellar","symbol" => "XLM", "contract" => "", "slug" => "xlm", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 0, "fee" => 5, "processing_fee" => 0.001,"explorer" => $XLMExplorer],
            ["name" => "XTRLPay","symbol" => "XTRLPAY", "contract" => "0x76a435B51bAeae457324406da02ee7E3473288B5", "slug" => "xtrlpay", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 30000, "fee" => 25000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "ETHplode","symbol" => "ETHPLO", "contract" => "0xe0c6CE3e73029F201e5C0Bedb97F67572A93711C", "slug" => "ethplo", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 50, "fee" => 50, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Erubicoin","symbol" => "ERBC", "contract" => "0x034A0E66F589e8b6F72155fdBde01eF233A24283", "slug" => "erbc", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 30000, "fee" => 25000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "0xLitecoin","symbol" => "0xLTC", "contract" => "0x8c152e933e7f20C3cca0ab7CAd4D4BF1bc6E00e4", "slug" => "0xltc", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 30000, "fee" => 25000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Andrey Voronkov Ventures Promo","symbol" => "AVVP", "contract" => "0x783ba0062326861ee76e0e15429594922e9fe2f5", "slug" => "avvp", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "HUBBS","symbol" => "HUBBS", "contract" => "0x1E999EE452EaFbCfd6B8f038Bb6cabbB533dC1b9", "slug" => "hubbs", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Revelation Coin Project Token","symbol" => "REV", "contract" => "0xe6Be436DF1Ff96956dfe0b2b77FAB84EDe30236F", "slug" => "rev", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Revelation Silver","symbol" => "REVS", "contract" => "0x4f1ff4e15936ee10851f397900345b84dcc23836", "slug" => "revs", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Revelation Gold","symbol" => "REVG", "contract" => "0xe64aa06ecc9d3259446f652a390067946d804ae2", "slug" => "revg", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Revelation Platinum","symbol" => "REVP", "contract" => "0xf4fb70638c4a83ffcec413a026b99a272b6f9933", "slug" => "revp", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Revelation Diamond","symbol" => "REVD", "contract" => "0x8931076db8984d7d044bd7cd4b96bc7e9058e683", "slug" => "revd", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Orium","symbol" => "ORM", "contract" => "0xd51e852630DeBC24E9e1041a03d80A0107F8Ef0C", "slug" => "orm", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Illume","symbol" => "ILT", "contract" => "0xc5005344d52758ee2264be257a198b50f884711b", "slug" => "ilt", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "NGSCoin","symbol" => "NGS", "contract" => "0x9BaAF63b5B4C10600F82A934640dd9C070A5f260", "slug" => "ngs", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Ethereum Money","symbol" => "ETHMNY", "contract" => "0xbf4a2ddaa16148a9d0fa2093ffac450adb7cd4aa", "slug" => "ethmny", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Grow Token","symbol" => "GRWT", "contract" => "0x832ec40433b457028a1d27434f3b9fdf81aaecbe", "slug" => "grwt", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Arma Coin","symbol" => "GZM", "contract" => "0x0a680e503fd9ae14b62444c75ffb4bef1f105666", "slug" => "gzm", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "XGHOST","symbol" => "XGO", "contract" => "0x4158bd308187f1E2426eFA443033851a1f9C1568", "slug" => "xgo", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Oryx","symbol" => "ORX", "ieo"=>true, "contract" => "0xCf2E607160f83C10cEb5861A992CB4CE93710B3c", "slug" => "orx", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Almeela","symbol" => "KZE","ieo"=>true, "contract" => "0x8de67d55c58540807601dbf1259537bc2dffc84d", "slug" => "kze", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0,"explorer" => $ERC20Explorer],
            ["name" => "Almeedex","symbol" => "ALDX","ieo"=>true, "contract" => "0x155d31ea88a110779b560689c6e33798b1a405f9", "slug" => "aldx", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "StarterCoin","symbol" => "STAC", "contract" => "0x9a005c9a89bd72a4bd27721e7a09a3c11d2b03c4", "slug" => "stac", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "BitBall","symbol" => "BTB", "contract" => "0x06e0feb0d74106c7ada8497754074d222ec6bcdf", "slug" => "btb", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "DAI","symbol" => "DAI", "contract" => "0x89d24a6b4ccb1b6faa2625fe562bdd9a23260359", "slug" => "dai", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 10, "fee" => 1, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Ripple","symbol" => "XRP", "contract" => "", "slug" => "xrp", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 25, "fee" => 5, "processing_fee" => 0.001,"explorer" => $XRPExplorer],
            ["name" => "Swapcoinz","symbol" => "SPZ", "contract" => "0xeeaf33534f7e8628b281da3aa01271d4ce22619d", "slug" => "spz", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 1000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Mero Currency","symbol" => "MRO", "contract" => "0x6ff313fb38d53d7a458860b1bf7512f54a03e968", "slug" => "mro", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 1000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Miracle Token","symbol" => "MRCL","ieo"=>true, "contract" => "0x2534409daa29b07682020d07eac9ae01c34acec0", "slug" => "mrcl", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 1000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],
            ["name" => "Bitball Treasure","symbol" => "BTRS", "contract" => "0x2932746414a6766a2b4a472cb3a2624f5667fd83", "slug" => "btrs", "decimals" => 8, "status" => 0, "min_deposit" => 0, "min_withdraw" => 3000, "fee" => 1000, "processing_fee" => 0.001,"explorer" => $ERC20Explorer],

        ];

        foreach ($coins as $coin){
            $checkCoin = \App\Coin::where("symbol",$coin["symbol"])->where("name",$coin["name"])->withTrashed()->get()->first();
            if(!$checkCoin){
                App\Coin::create($coin);
            }else{
                $checkCoin->decimals = $coin["decimals"];
                $checkCoin->name = $coin["name"];
                $checkCoin->slug = $coin["slug"];
                $checkCoin->explorer = $coin["explorer"];

                if (array_key_exists('contract', $coin)) {
                    $checkCoin->contract = $coin["contract"];
                }

                $checkCoin->ieo = isset($coin["ieo"]) ? $coin["ieo"] : false;

                if($checkCoin->min_withdraw < $coin["min_withdraw"]){
                    $checkCoin->min_withdraw = $coin["min_withdraw"];
                }

                if($checkCoin->processing_fee < $coin["processing_fee"]){
                $checkCoin->processing_fee = $coin["processing_fee"];
                }


                if($checkCoin->ieo == true){
                    $checkCoin->processing_fee = $coin["processing_fee"];
                }

                $checkCoin->save();
            }
         }
    }
}
