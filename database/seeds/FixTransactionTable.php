<?php

use Illuminate\Database\Seeder;

class FixTransactionTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $transactions = \App\Transaction::get();
        foreach($transactions as $transaction){
            if($transaction->type == "sell"){
                $transaction->transaction_type = 1;
            }else{
                $transaction->transaction_type = 0;
            }
            $transaction->save();
        }
    }
}
