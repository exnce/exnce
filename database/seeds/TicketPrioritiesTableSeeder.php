<?php

use Illuminate\Database\Seeder;

class TicketPrioritiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $priorites = getSupportPriority();
        foreach($priorites as $priority => $key) {
            $checkPrority = App\TicketPriority::where("key",$key["key"])->first();
            if (!$checkPrority){
                App\TicketPriority::create(["name"=>$key["value"], "key"=>$key["key"]]);
            }
        }
    }
}
