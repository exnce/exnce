<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CreateAirdropEntries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('airdrops')->truncate();


        $airdrops = [
            ["owner_id"=>7633,
                "symbol" => "HUBBS",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-01 12:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-11 00:00:00'),
                "name"=>"HUUBS Airdrop", "model" => "Public Airdrop",
                "amount"=>5000000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "ETHPLO",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-10 12:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-20 00:00:00'),
                "name"=>"ETHPLODE Airdrop", "model" => "Public Airdrop",
                "amount"=>5000000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "ILT",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-10 12:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-20 00:00:00'),
                "name"=>"ILLUME Airdrop", "model" => "Public Airdrop",
                "amount"=>5000000,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>8013,
                "symbol" => "AVVP",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-10 22:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-20 00:00:00'),
                "name"=>"Andrey Voronkov Ventures Promo Airdrop", "model" => "Public Airdrop",
                "amount"=>2000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "ETHMNY",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-14 00:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-24 00:00:00'),
                "name"=>"ETHMONEY Airdrop", "model" => "Public Airdrop","amount"=>1000000,"followers"=>0,
                "description"=>"", "status" => 3],



            ["owner_id"=>2,
                "symbol" => "ORX",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-23 00:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-02 00:00:00'),
                "name"=>"ORYX Airdrop", "model" => "Public Airdrop","amount"=>1000000,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>2,
                "symbol" => "STAC",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-05-31 00:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-9 00:00:00'),
                "name"=>"STAC Airdrop", "model" => "Public Airdrop","amount"=>5000,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>2,
                "symbol" => "BTB",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-03 00:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-13 00:00:00'),
                "name"=>"BitBall Airdrop", "model" => "Public Airdrop","amount"=>25000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "HUBBS",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-17 00:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-17 00:00:00'),
                "name"=>"Kripto Para Turkiye Telegram Channel Airdrop", "model" => "Private Airdrop","amount"=>1000000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "SPZ",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-20 06:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-30 06:00:00'),
                "name"=>"SPZ Airdrop", "model" => "Public Airdrop","amount"=>100000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "ALDX",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-20 06:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-30 06:00:00'),
                "name"=>"ALDX Private Airdrop", "model" => "Private Airdrop","amount"=>5000000,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>2,
                "symbol" => "ETHMNY",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-06-24 02:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-07 02:00:00'),
                "name"=>"ETHMNY Special Airdrop", "model" => "Public Airdrop","amount"=>500000,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>2,
                "symbol" => "MRO",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-02 03:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-12 03:00:00'),
                "name"=>"Mero Currency Public Airdrop", "model" => "CANCELLED","amount"=>100000,"followers"=>0,
                "description"=>"", "status" => 3],

            ["owner_id"=>2,
                "symbol" => "AVVP",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-02 03:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-07-12 03:00:00'),
                "name"=>"AVVP EXNCE Reward", "model" => "Private Airdrop","amount"=>200,"followers"=>0,
                "description"=>"", "status" => 3],


            ["owner_id"=>2,
                "symbol" => "MRCL",
                "start_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-08-07 03:00:00'),
                "finish_date"=>Carbon::createFromFormat('Y-m-d H:i:s', '2019-08-17 03:00:00'),
                "name"=>"Miracle Token Public Airdrop", "model" => "Private Airdrop","amount"=>2000000,"followers"=>0,
                "description"=>"", "status" => 3],
        ];


        foreach($airdrops as $airdrop){
            $checkCoin = \App\Coin::where("symbol",$airdrop["symbol"])->get()->first();

            if($checkCoin){

                $newAirdrop = [
                    "owner_id"=>$airdrop["owner_id"],
                    "coin_id"=>$checkCoin->id,
                    "name"=>$airdrop["name"],
                    "model"=>$airdrop["model"],
                    "amount"=>$airdrop["amount"],
                    "start_date"=>$airdrop["start_date"],
                    "finish_date"=>$airdrop["finish_date"],
                    "followers"=>0,
                    "status"=>$airdrop["status"]];

                    $checkAirdrop = \App\Airdrop::where("coin_id",$checkCoin->id)->where("owner_id",$airdrop["owner_id"])->where("status",0)->get()->first();
                    if(!$checkAirdrop){
                        $savedAirdrop =  App\Airdrop::create($newAirdrop);
                    }else{
                        if($checkAirdrop->owner_id <= 0){
                            $checkAirdrop->owner_id = $airdrop["owner_id"];
                        }
                        $checkAirdrop->start_date = $airdrop["start_date"];
                        $checkAirdrop->finish_date = $airdrop["finish_date"];
                        $checkAirdrop->model = $airdrop["model"];
                        $checkAirdrop->status = $airdrop["status"];
                        $checkAirdrop->save();
                    }
            }
        }




    }
}
