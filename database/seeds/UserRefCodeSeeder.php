<?php

use Illuminate\Database\Seeder;

class UserRefCodeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = \App\User::get();
        foreach($users as $user){
            if($user->ref_code == "")
            {
                $user->ref_code = $user->id + 10000;
                $user->save();
            }
        }
    }
}
