<?php

use Illuminate\Database\Seeder;

class AddFakeRecordsOnReferencesForTest extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $refs = \App\UserReference::get();

        foreach($refs as $ref){


            App\UserReference::create(["user_id"=>$ref->user_id,"reference_id"=>$ref->reference_id,"created_at"=>$ref->created_at,"updated_at"=>$ref->updated_at]);

        }

    }
}
