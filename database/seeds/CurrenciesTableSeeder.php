<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = [
            [
                "name" => "TÜRK LİRASI",
                "symbol"=>"TRY",
                "slug" => "turk-lirasi",
                "decimals"=>4,
                "dec_point"=>",",
                "thousands_sep"=>".",
                "status" => 0
            ],
//            [
//                "name" => "UNITED STATES DOLLAR",
//                "symbol"=>"USD",
//                "slug" => "united-states-dollar",
//                "decimals"=>4,
//                "dec_point"=>",",
//                "thousands_sep"=>".",
//                "status" => 1
//            ],
//            [
//                "name" => "EURO",
//                "symbol"=>"EUR",
//                "slug" => "euro",
//                "decimals"=>4,
//                "dec_point"=>",",
//                "thousands_sep"=>".",
//                "status" => 1
//            ]
        ];

        foreach ($currencies as $currency){
            $checkCurrency = App\Currency::where("name",$currency["name"])->where("symbol",$currency["symbol"])->first();
            if (!$checkCurrency){
                App\Currency::create($currency);
            }
        }
    }
}
