<?php

use Illuminate\Database\Seeder;

class FillCoinDetails extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    /*
     *
              [
                "symbol" => "NPXS",
                "description" => "",
                "twitter"=>"",
                "telegram"=>"",
                "facebook" => "",
                "reddit"=> "",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "",
                "email" => "",
                "github" => "",
                "website" =>""
            ],

     */
    public function run()
    {

        DB::table('coin_details')->truncate();

        $coinList = [
            [
                "symbol" => "ETH",
                "description" => "Ethereum is an open platform that enables developers to build and deploy decentralized applications, Ethereum allows participants to run decentralized blockchain applications called Smart contracts! Smart contracts are highly secure and run with a perfect digital history making them auditable since these smart contracts can be programmed without any chance of downtime censorship or fraud. The Ethereum blockchain and smart contracts form a shared global supercomputer that can move/send value across the world, represent ownership & transmit tokenized assets and digitize many more complex financial applications. This allows developers to create many things all without a middleman and all immutable much like what the internet did for information, Ethereum has the power to open up the financial system to the world and build a safer more accessible and fair economy for everyone to participate in..",
                "twitter"=>"https://twitter.com/ethereum",
                "telegram"=>"",
                "facebook" => "https://www.facebook.com/ethereumproject",
                "reddit"=> "https://www.reddit.com/r/ethereum/",
                "youtube" => "https://www.youtube.com/user/ethereumproject/featured",
                "medium" => "",
                "whitepaper" => "ethereum.pdf",
                "email" => "",
                "github" => "https://github.com/ethereum",
                "website" => "https://www.ethereum.org",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/ethereum/",
                "coingecko" => "https://www.coingecko.com/en/coins/ethereum"
            ],
            [
                "symbol" => "XLM",
                "description" => "Stellar is an open-source, distributed payments infrastructure. It is a leapfrog technology that connects people, payment systems, and banks with a focus on the developing world. It lets you facilitate multi-currency and asset transactions quickly, reliably, and for a fraction of a penny by using a crypto-asset called Lumens (XLM) as a bridge.",
                "twitter"=>"https://twitter.com/stellarorg",
                "telegram"=>"",
                "facebook" => "https://www.facebook.com/stellarfoundation",
                "reddit"=> "https://www.reddit.com/r/stellar",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "stellar.pdf",
                "email" => "",
                "github" => "https://github.com/stellar",
                "website" =>"https://www.stellar.org/",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/stellar/",
                "coingecko" => "https://www.coingecko.com/en/coins/stellar"
            ],
            [
                "symbol" => "HUBBS",
                "description" => "An Extablished Ads Artificial Intelligence Protocol Collaboratively engineer impactful niches for business technologies.",
                "twitter"=>"https://twitter.com/hubbsnetwork",
                "telegram"=>"https://t.me/hubbscoin",
                "facebook" => "",
                "reddit"=> "",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "",
                "email" => "",
                "github" => "",
                "website" => "https://myhubb.online",
                "coinmarketcap" => "",
                "coingecko" => "https://www.coingecko.com/en/coins/myhubb"

            ],
            [
                "symbol" => "ORM",
                "description" => "ORIUM (Game Utility Token) is a decentralized and fair Ethereum-based online mobile (P2P) game and supported by community member. Users have the option of playing on their own or versus other players instead of against a centralized game house. To place a bet, there must be game room to join or can be banker to open game room. The player can adjust the amount to bet or choose the game room suitable for everyone involved. There can be both a public or a private game room.",
                "twitter"=>"https://twitter.com/OriumOfficial",
                "telegram"=>"https://t.me/oriumofficial",
                "facebook" => "https://www.facebook.com/oriumofficial",
                "reddit"=> "https://www.reddit.com/u/oriumofficial",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "orium.pdf",
                "email" => "contact@oriumcoin.com",
                "github" => "",
                "website" =>"https://www.oriumcoin.com/",
                "coinmarketcap" => "",
                "coingecko" => "https://www.coingecko.com/en/coins/orium"
            ],
            [
                "symbol" => "AVVP",
                "description" => "Voronkov Ventures is accelerator for breakthrough technologies, founded by Dr. Andrey Voronkov",
                "twitter"=>"https://twitter.com/voronkovio",
                "telegram"=>"https://t.me/joinchat/AAAAAFBiVoqhU5GX1xwo_A",
                "facebook" => "https://www.facebook.com/andrewcrower/",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UCcuIRzCDZKrikUUkJxaxn5w",
                "medium" => "",
                "whitepaper" => "avvp.pdf",
                "email" => "a@voronkov.io",
                "github" => "https://github.com/voronkov-ventures",
                "website" =>"https://andreyvoronkov.com",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],

            [
                "symbol" => "ILT",
                "description" => "Illume took the next step by creating the multichain protocol and multichain token ILT.",
                "twitter"=>"https://twitter.com/illume_token",
                "telegram"=>"https://t.me/illumechat",
                "facebook" => "",
                "reddit"=> "",
                "youtube" => "",
                "medium" => "https://medium.com/@illume_token",
                "whitepaper" => "illume.pdf",
                "email" => "",
                "github" => "https://github.com/illumeproject",
                "website" => "https://illumetech.site",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],
            [
                "symbol" => "ETHPLO",
                "description" => "A deflationary decentralized store of value based on the Ethereum blockchain. Whenever ETHPLO is transferred, 0.5% of the amount will be burned.",
                "twitter"=>"https://twitter.com/ETHplode",
                "telegram"=>"https://t.me/ETHplode",
                "facebook" => "",
                "reddit"=> "https://www.reddit.com/r/ETHplode/",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "",
                "email" => "hello@ethplode.org",
                "github" => "https://github.com/ETHplode/",
                "website" => "https://ethplode.org",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/ethplode/",
                "coingecko" => "https://www.coingecko.com/en/coins/ethplode"
            ],
            [
                "symbol" => "ETHMNY",
                "description" => "ERC20 AND ERC223 CODE TOKEN, DESIGN FOR MASS ADOPTION, NO ICO, NO WHITEPAPER, NO PRE-SALE, THE TOKEN IS DESIGN TO BE USED WORLDWIDE, TO SIMPLIFY THE USAGE OF ERC THROUGH TOKENIZATION AND EASY FOR PUBLIC, WE SEEK THE ADVANCEMENT IN MOBILE INDUSTRY AS TO PROVIDE REAL WORLD UTILITY FOR ETHEREUM MONEY, TO ADVANCE THE TECHNOLOGY FOR IT TO ADD MORE UTILITY, TO ALLOW PEOPLE ALL OVER THE WORLD TO USE ETHMNY AS A ONE ONLINE CURRENCY, WE HAVE A GREAT TEAM, WE HOPE YOU ALSO ADOPT THIS PROJECT AND BE PART OF IT, DESIGN FOR ALL. ETHMNY IS NO BOUNDARIES.",
                "twitter"=>"https://twitter.com/ethmny",
                "telegram"=>"https://t.me/ethmny",
                "facebook" => "",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UC62_Bznto9o0pEmslm8y3WA",
                "medium" => "",
                "whitepaper" => "",
                "email" => "",
                "github" => "",
                "website" => "https://ethmny.com",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],
            [
                "symbol" => "STAC",
                "description" => "The​ ​next​ ​step​ ​in​ ​the​ ​CoinStarter​ ​journey​ ​is​ ​the​ ​launch​ ​of​ ​the​ ​StarterCoin.​ ​​ ​A​ ​purpose-built​ ​Coin using​ ​the​ ​CoinStarter​ ​network​ ​that​ ​will​ ​allow​ ​members​ ​to​ ​take​ ​an​ ​active​ ​role​ ​in​ ​the​ ​development of​ ​the​ ​company. StarterCoins​ ​are​ ​a​ ​cryptocurrency​ ​which​ ​gives​ ​rights​ ​and​ ​privileges​ ​to​ ​their​ ​owners​ ​when​ ​using the​ ​CoinStarter​ ​platform​ ​and​ ​its​ ​family​ ​of​ ​services.StarterCoins​ ​are​ ​available​ ​at​ ​a​ ​discount during​ ​the​ ​ICO.​ ​​ ​Any​ ​unsold​ ​coins​ ​during​ ​the​ ​ICO​ ​burned.​ ​​ ​​ ​Within​ ​a​ ​few​ ​weeks​ ​following​ ​the​ ​ICO, StarterCoins​ ​will​ ​be​ ​available​ ​on​ ​most​ ​exchanges.",
                "twitter"=>"https://twitter.com/realCoinStarter",
                "telegram"=>"",
                "facebook" => "",
                "reddit"=> "",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "startercoin.pdf",
                "email" => "",
                "github" => "",
                "website" => "https://www.coinstarter.com",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/startercoin/",
                "coingecko" => ""
            ],
            [
                "symbol" => "ORX",
                "description" => "Oryx is a blockchain based E-Commerce platform that will combine more than one project within its economy and suitable for customers to buy and sell products, help to verify transactions with high accuracy, and the job will be updated based on the level of project development at each stage. E-commerce in the Middle East is growing, there are increasing numbers of large players in this business, such as traders, brokers, banks, insurance companies and public institutions both national and on international firms.",
                "twitter"=>"https://twitter.com/oryx_project",
                "telegram"=>"https://t.me/oryxtoken",
                "facebook" => "https://www.facebook.com/oryxproject",
                "reddit"=> "https://www.reddit.com/user/OryXTOKEN",
                "youtube" => "https://www.youtube.com/channel/UCc2l9xTHPbDreS4DvQzMFaQ?view_as=subscriber",
                "medium" => "",
                "whitepaper" => "oryx.pdf",
                "email" => "oryx@4oryx.com",
                "github" => "https://github.com/oryxtoken",
                "website" => "https://4oryx.com/",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],
            [
                "symbol" => "HOT",
                "description" => "Holo is a pragmatic compromise — a bridge for people to transition toward greater and greater decentralization and autonomy. But since Holo must interface with the centralized systems of today, some parts of it are also centralized. So this is not a typical crypto project or paper. We've already built a fully distributed platform in Holochain. Now we want to take it to the mainstream. Holo is the bridge to get there.",
                "twitter"=>"https://twitter.com/holochain",
                "telegram"=>"https://t.me/channelHolo",
                "facebook" => "https://www.facebook.com/holochain.design/",
                "reddit"=> "https://www.reddit.com/r/holochain/",
                "youtube" => "",
                "medium" => "https://medium.com/holochain",
                "whitepaper" => "holochain.pdf",
                "email" => "",
                "github" => "https://github.com/holochain/holochain-rust",
                "website" => "https://holochain.org",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],
            [
                "symbol" => "NPXS",
                "description" => "Pundi X aims to bring in the next billion crypto users as it allows users to buy, sell, use cryptocurrency anywhere and anytime.",
                "twitter"=>"https://twitter.com/PundiXLabs",
                "telegram"=>"https://t.me/Pundix",
                "facebook" => "https://www.facebook.com/pundixlabs/",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UCOIf6WeLEzZi3DQxzenTZeA",
                "medium" => "",
                "whitepaper" => "npxs.pdf",
                "email" => "contact@pundix.com",
                "github" => "",
                "website" =>"https://pundix.com/",
                "coinmarketcap" =>"https://coinmarketcap.com/currencies/pundi-x/",
                "coingecko" => "https://www.coingecko.com/en/coins/pundi-x"
            ],

            [
                "symbol" => "DAI",
                "description" => "The Dai Stablecoin is a collateral-backed cryptocurrency whose value is stable relative to the US Dollar. We believe that stable digital assets like Dai Stablecoin are essential to realizing the full potential of blockchain technology.",
                "twitter"=>"https://twitter.com/MakerDAO",
                "telegram"=>"https://t.me/makerdaoOfficial",
                "facebook" => "",
                "reddit"=> "https://www.reddit.com/r/MakerDAO/",
                "youtube" => "https://www.youtube.com/MakerDAO",
                "medium" => "",
                "whitepaper" => "dai.pdf",
                "email" => "bd@makerdao.com",
                "github" => "https://github.com/makerdao/",
                "website" =>"https://makerdao.com",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/dai/",
                "coingecko" => "https://www.coingecko.com/en/coins/dai"
            ],

            [
                "symbol" => "XRP",
                "description" => "Ripple also called XRP coin is traded throughout many different cryptocurrency exchanges and is one of the top traded coins now. The Ripple network is an open payment network for digital currency as well as a holding company. Ripple aims to create and enable a global network of financial institutions and banks to use Ripple software to lower cost of international payments. Ripple calls this global network the \"Internet of Value\" and operates on the XRP ledger which is an open source product created by Ripple. Ripple allows a secured and cheap way to move money around in a very fast and cheap way for business and for people.",
                "twitter"=>"https://twitter.com/Ripple",
                "telegram"=>"",
                "facebook" => "https://www.facebook.com/ripplepay",
                "reddit"=> "",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "xrp.pdf",
                "email" => "support@ripple.com",
                "github" => "https://github.com/ripple",
                "website" =>"https://ripple.com/xrp/",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/ripple/",
                "coingecko" => "https://www.coingecko.com/en/coins/ripple"
            ],

            [
                "symbol" => "KZE",
                "description" => "Almeela token is a blockchain-based decentralized funding platform(Almee Up) for high tech projects that is backed by physical gold. Most transactions today are conducted digitally, and physical gold cannot be used directly. The only solution of this problem is a crypto currency which is backed by gold and could be divided into decimals.",
                "twitter"=>"https://www.almeela.com",
                "telegram"=>"https://t.me/almeelaeng",
                "facebook" => "https://www.facebook.com/groups/314790882454449/",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/watch?v=L4X5e43kuLs",
                "medium" => "https://medium.com/@vebitcointeknoloji/almeela-kze-coin-vebitcoin-de-e80a3f52e4f0",
                "whitepaper" => "almeela.pdf",
                "email" => "info@almeela.com",
                "github" => "",
                "website" =>"https://www.almeela.com",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/almeela/",
                "coingecko" => "https://www.coingecko.com/en/ico/almeela"
            ],

            [
                "symbol" => "ALDX",
                "description" => "Almeedex is a new generation crypto-currency exchange that uses ‘’Augmented Reality Technology’’ which is integrated with its own platform. In this sense, Almeedex takes a revolutionary step in the field of financial technology.",
                "twitter"=>"https://twitter.com/almeedexchange",
                "telegram"=>"https://t.me/Almeedexchange",
                "facebook" => "https://www.facebook.com/groups/314790882454449/",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/watch?v=idu4X5-Cd8o",
                "medium" => "https://medium.com/search?q=almeedex",
                "whitepaper" => "almeedex.pdf",
                "email" => "support@almeedeex.com",
                "github" => "",
                "website" =>"https://www.almeedex.com",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],

            [
                "symbol" => "SPZ",
                "description" => "Swapcoinz is a block chain based playground solution. Amusement parks usually operates a variety of attractions such as games, shows, water rides, mechanical rides, theme exhibits as well as picnic grounds and refreshment stands. We intend to favorably compete with other amusement parks in world To achieve this, we are willing to invest in competent and experienced professionals who would help run our business from the start to an enviable position in the industry. SPZ will be used for gaining access to all our Swapplay Amusement Parks...",
                "twitter"=>"https://twitter.com/swapcoinz",
                "telegram"=>"https://t.me/swapcoinzgroup",
                "facebook" => "https://www.facebook.com/swapcoinzz/",
                "reddit"=> "https://reddit.com/u/swapcoinz",
                "youtube" => "",
                "medium" => "",
                "whitepaper" => "swapcoinz.pdf",
                "email" => "enquiry@swapcoinz.org",
                "github" => "",
                "website" =>"https://swapcoinz.org",
                "coinmarketcap" => "",
                "coingecko" => ""
            ],

            [
                "symbol" => "BTB",
                "description" => "Bitball aims to become a cryptocurrency that is accepted as a mode of payment in real-life scenarios. Bitball’s mission is to build an ecosystem to bridge the gap between digital currencies, exchanges, and customers. It seeks to facilitate cross-border transactions with a user-friendly interface. Phase 4 of the project involves building an online barter system for goods and services for international participants based on cryptocurrency and fiat. BitBall will be used as a means for users on its upcoming Ebarter platform to sell or buy any products or services around the world at low cost. Bitball's mission is to serve as a long-term investment",
                "twitter"=>"https://twitter.com/BitBall_Erc20",
                "telegram"=>"https://t.me/joinchat/HUHBjUnb3OMaB8LfkuFWPw",
                "facebook" => "https://www.facebook.com/bit.ball.921",
                "reddit"=> "https://www.reddit.com/u/Bitball/",
                "youtube" => "https://www.youtube.com/watch?v=qFWlP6wGMUE",
                "medium" => "https://medium.com/@bitballerc20",
                "whitepaper" => "bitball.pdf",
                "email" => "support@bitball-btb.com",
                "github" => "https://github.com/BitballErc20",
                "website" =>"https://www.bitball-btb.com/",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/bitball/",
                "coingecko" => "https://www.coingecko.com/en/coins/bitball"
            ],

            [
                "symbol" => "MRO",
                "description" => "Mero Currency, The First Low 0.001% Burn-Rate Deflationary Token, Open Source.  Yet MRO is very unique with a low burn rate! Mero is secure, the contract code is verified. With full encryption and a verifiable decenatralized blockchain, transfer Mero with confidence and security. With a massive peer to peer network you are able to instantly transfer Mero across the world with no boundaries and virtually zero fees.",
                "twitter"=>"https://twitter.com/merocurrency",
                "telegram"=>"https://t.me/merocurrency",
                "facebook" => "",
                "reddit"=> "https://www.reddit.com/r/MeroCurrency/",
                "youtube" => "https://www.youtube.com/watch?v=yKX5uk_ynRU",
                "medium" => "https://medium.com/@merocurrency",
                "whitepaper" => "",
                "email" => "info@merocurrency.com",
                "github" => "https://github.com/merocurrencytoken",
                "website" =>"https://www.merocurrency.com",
                "coinmarketcap" => "",
                "coingecko" => "https://www.coingecko.com/en/coins/mero-currency"
            ],


            [
                "symbol" => "REV",
                "description" => "The Revelation Coin Project has developed, implemented and now is promoting a bartering system (REV coins) for those who wish to bypass the future mandatory one world currency system.",
                "twitter"=>"https://twitter.com/revelationcoin",
                "telegram"=>"https://t.me/revelationcoin",
                "facebook" => "https://www.facebook.com/Revelationcoin/",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UCHgdtFfgJkSOtM-QMSVd08A",
                "medium" => "https://medium.com/@Revelationcoin",
                "whitepaper" => "revelationcoin.pdf",
                "email" => "support@revelationcoin.org",
                "github" => "",
                "website" => "https://revelationcoin.org/",
                "coinmarketcap" => "",
                "coingecko" => "https://www.coingecko.com/en/coins/revelation-coin"

            ],


            [
                "symbol" => "MRCL",
                "description" => "World's First Decentralize Video Sharing Platform – Blockchain based video sharing platform which is dedicated to providing a secure and fast video sharing system to its users and which can as well provide means of earnings to the community at large. The platform helps advertisers and filmmakers to manage their media advertising promotions. The users of the system can earn MRCL token as a reward while sharing media contents.",
                "twitter"=>"https://twitter.com/mirctoken",
                "telegram"=>"https://t.me/miracletoken",
                "facebook" => "https://www.facebook.com/miracletokenpage",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UClET2SkSY5SeW1iZcBiWqFQ",
                "medium" => "",
                "whitepaper" => "miracletoken.pdf",
                "email" => "support@miracletoken.org",
                "github" => "",
                "website" => "https://www.miracletoken.org",
                "coinmarketcap" => "",
                "coingecko" => "https://www.coingecko.com/en/coins/miracle-token",
                "video" => "https://www.youtube.com/embed/w1MFeGZUMSc"
            ],

            [
                "symbol" => "GZM",
                "description" => "Armacoin (GZM) is designed for advertising/media companies. GZM functions like a billboard, where advertisers pay 1 GZM to place a message in the contract line. According to the team, advertisers can place this message on billboards in cities, on websites and all visitors will see what is specified in the global parameter of the message.",
                "twitter"=>"https://twitter.com/ArmacoinG",
                "telegram"=>"https://t.me/ArmacoinGZM",
                "facebook" => "https://m.facebook.com/armacoin.gzm",
                "reddit"=> "",
                "youtube" => "https://www.youtube.com/channel/UCIEa2rsBlNuaepdD1VNRs5w",
                "medium" => "https://medium.com/@RafaelCriptomoeda/armacoin-has-the-main-objective-is-to-take-the-blockchain-technology-to-all-the-lares-and-to-all-6eec71f1caa2",
                "whitepaper" => "armacoin.pdf",
                "email" => "info@armacoin.info",
                "github" => "https://github.com/dimabarsu/GZM-Armacoin/",
                "website" => "https://armacoin.info",
                "coinmarketcap" => "https://coinmarketcap.com/currencies/armacoin/",
                "coingecko" => "https://www.coingecko.com/en/coins/armacoin",
                "video" => "https://www.youtube.com/watch?v=5AkbaKhjD7c"

            ],

        ];


        foreach ($coinList as $coinInfo){
            $coin = \App\Coin::where("symbol",$coinInfo["symbol"])->get()->first();
            if($coin){
                unset($coinInfo["symbol"]);

                if (array_key_exists("video",$coinInfo))
                {
                    $coinInfo["video"]=$coinInfo["video"];
                }else{
                    $coinInfo["video"]="";
                }



                $coinInfo["phone"]="";
                $coinInfo["coin_id"] = $coin->id;

                if($coinInfo["whitepaper"] != ""){
                    $coinInfo["whitepaper"] = sprintf("%s/whitepapers/%s",env("APP_URL"),$coinInfo["whitepaper"]);
                }
                \App\CoinDetail::create($coinInfo);
            }

        }




    }
}
