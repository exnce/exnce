<?php

use Illuminate\Database\Seeder;

class TestAirdropOwners extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $airdrop = \App\Airdrop::get()->first();

        $coin = \App\Coin::where("symbol","HUBBS")->get()->first();


        $users = \App\User::where("id",">",10)->get()->take(50);

        foreach ($users as $user){
            $user->follow($airdrop->id,\App\Airdrop::class);

            $walletData = ["user_id"=>$user->id,
                "type"=>false,
                "coin_id"=>$coin->id,
                "name"=>$coin->name,
                "balance"=>0
                ];

            $checkWallet = $user->wallets()->where("coin_id",$coin->id)->get()->first();
            if(!$checkWallet){
                $checkWallet=\App\Wallet::create($walletData);
            }

            $walletAddress = $checkWallet->address()->get()->last();
            if(!$walletAddress){
                $walletAddressData = [
                    "user_id"=>$user->id,
                    "wallet_id"=>$checkWallet->id,
                    "coin_id"=>$coin->id,
                    "address_type"=>"ERC20",
                    "address"=>"TEST_ADDRESS_FOR_USER".$user->id."--".$coin->id,
                    "password"=>"TEST_WALLET_PASSWORD",
                    "payment_id"=>"TEST_WALLET_PAYMENT_ID",
                    "destination_tag"=>"TEST_WALLET_DEST_TAG",
                    "status"=>0
                ];
                $walletAddress = \App\WalletAddress::create($walletAddressData);
            }

        }


    }
}
