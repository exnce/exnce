<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PairTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //php artisan db:seed --class=PairTableSeeder

        DB::table('coin_coin')->truncate();

        $delistedTokens = ["ILT","0xLTC","XTRLPAY",
            "AAVP","ERBC","MTCN","XTS","FTRM",
            "BNT","MANA","HERB","BNB","HOT",
            "POLY","OMG","REP","ENJ","ZIL",
            "ZRX","PPT","DGD","DENT","NGS",
            "REVD","REVP","REVG","REVS","XGO","MRO","KZE","ALDX","HUBBS","MRCL","GRWT","NPXS","MKR"
        ];

        $baseList = ["ETH"];
        $eth = \App\Coin::where("symbol","ETH")->get()->first();
        $coins = \App\Coin::where("id","<>", $eth->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){

            if(!in_array($coin->symbol,$delistedTokens)){
                  $pairItem = ["coin_id" => $eth->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                  $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$eth->id)->get()->first();

                  if(!$checkPair){
                      \App\CoinCoin::create($pairItem);
                  }
            }
        }

        $baseList = ["ETH","XRP"];
        $xrp = \App\Coin::where("symbol","XRP")->get()->first();
        $coins = \App\Coin::where("id","<>", $xrp->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){
            if(!in_array($coin->symbol,$delistedTokens)){

                $pairItem = ["coin_id" => $xrp->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$xrp->id)->get()->first();
                if(!$checkPair){
                    \App\CoinCoin::create($pairItem);
                }
            }
        }

        $baseList = ["XLM","ETH","XRP"];
        $xlm = \App\Coin::where("symbol","XLM")->get()->first();
        $coins = \App\Coin::where("id","<>", $xlm->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){

            if(!in_array($coin->symbol,$delistedTokens)){
                $pairItem = ["coin_id" => $xlm->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$xlm->id)->get()->first();
                if(!$checkPair){
                    \App\CoinCoin::create($pairItem);
                }
            }
        }

        $baseList = ["XLM","ETH","TUSD"];
        $tusd = \App\Coin::where("symbol","TUSD")->get()->first();
        $coins = \App\Coin::where("id","<>", $tusd->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){

            if(!in_array($coin->symbol,$delistedTokens)){
                $pairItem = ["coin_id" => $tusd->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$tusd->id)->get()->first();

                if(!$checkPair){
                    \App\CoinCoin::create($pairItem);
                }
            }
        }

        $baseList = ["XLM","ETH","TUSD","DAI"];
        $mkr = \App\Coin::where("symbol","DAI")->get()->first();
        $coins = \App\Coin::where("id","<>", $mkr->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){
            if(!in_array($coin->symbol,$delistedTokens)){
                $pairItem = ["coin_id" => $mkr->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$mkr->id)->get()->first();

                if(!$checkPair){
                    \App\CoinCoin::create($pairItem);
                }
            }
        }


        $baseList = ["XLM","ETH","TUSD","DAI","BTB"];
        $btb = \App\Coin::where("symbol","BTB")->get()->first();
        $coins = \App\Coin::where("id","<>", $btb->id)->whereNotIn("symbol",$baseList)->get();
        $pairList = [];
        foreach($coins as $coin){
            if(!in_array($coin->symbol,$delistedTokens)){
                $pairItem = ["coin_id" => $btb->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$btb->id)->get()->first();
                if(!$checkPair){
                    \App\CoinCoin::create($pairItem);
                }
            }
        }


//        $baseList = ["XLM","ETH","TUSD","DAI","BTB","ETHPLO"];
//        $ethplo = \App\Coin::where("symbol","ETHPLO")->get()->first();
//        $coins = \App\Coin::where("id","<>", $ethplo->id)->whereNotIn("symbol",$baseList)->get();
//        $pairList = [];
//        foreach($coins as $coin){
//            if(!in_array($coin->symbol,$delistedTokens)){
//                $pairItem = ["coin_id" => $ethplo->id, "pair_id" => $coin->id,"display_index"=>$coin->display_index];
//                $checkPair = \App\CoinCoin::where("pair_id",$coin->id)->where("coin_id",$ethplo->id)->get()->first();
//                if(!$checkPair){
//                    \App\CoinCoin::create($pairItem);
//                }
//            }
//        }

    }
}
