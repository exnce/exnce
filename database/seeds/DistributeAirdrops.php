<?php

use App\Airdrop;
use App\Coin;
use App\CoinDeposit;
use App\Order;
use App\UserBank;
use Illuminate\Database\Seeder;

class DistributeAirdrops extends Seeder
{
    /**DistributeAirdropsDistributeAirdrops
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coin = Coin::where("symbol","MRCL")->get()->first();
        if($coin){
              $airdrop = Airdrop::where("coin_id",$coin->id)->get()->first();
              if($airdrop){
                  $hunters = $airdrop->followers()->get();
                  $hunterCount = $airdrop->followers()->count();
                  $amount = 2000000/($hunterCount);

                  foreach($hunters as $hunter){

                      $airdropUser = $hunter;
                      if($airdropUser){

                          $wallet=$airdropUser->wallets()->where("type", false)->where("coin_id", $airdrop->coin_id)->get()->first();
                          if($wallet){
                              $walletAddress = $wallet->address()->get()->last();
                              if($walletAddress){
                                  $payment_address = $walletAddress->address;


                                  $checkUserBank = $airdrop->owner->user_banks()
                                      ->where("amount",$amount)
                                      ->where("surname","AIRDROP")
                                      ->where("currency_type",$wallet->type)
                                      ->where("currency_id",$airdrop->coin->id)
                                      ->where("payment_address",$payment_address)->get()->first();

                                  if(!$checkUserBank) {
                                      $userBank = [
                                          "user_id" => $airdrop->owner->id,
                                          "bank_id" => -1,
                                          "currency_type"=>$wallet->type,
                                          "currency_id"=>$airdrop->coin->id,
                                          "iban_number"=>"-",
                                          "name"=>$airdropUser->name,
                                          "surname"=>"AIRDROP",
                                          "amount"=>$amount,
                                          "payment_address" => $payment_address,
                                          "destination_tag" => "AIRDROP"
                                      ];
                                      $checkUserBank = UserBank::create($userBank);

                                      $orderArray = ["user_id" => $airdrop->owner->id,
                                          "type" => "withdraw",
                                          "currency_type"=>false,
                                          "currency_id"=>-1,
                                          "coin_id"=>$airdrop->coin_id,
                                          "amount" => $amount,
                                          "fee" => 0.00,
                                          "bank_id" => $checkUserBank->id,
                                          "transfer_code" => generateTransactionCode(),
                                          "description" => $airdrop->name,
                                          "txId"=> $airdrop->name,
                                          "status"=>5
                                      ];
                                      $order = Order::create($orderArray);

                                      $ownerWallet = $airdrop->owner->wallets()->where("type", false)->where("coin_id", $airdrop->coin_id)->get()->first();
                                      $ownerWallet->balance = $ownerWallet->balance - $amount;
                                      $ownerWallet->save();

                                      $wallet->balance = $wallet->balance + $amount;
                                      $wallet->save();



                                      $depositRequest = ["blockNumber" => $airdrop->id,
                                          "txid" => "EXNCEAIRDROP_".$airdrop->name. "-". $airdropUser->name . "-".$order->id,
                                          "coin_id"=>$airdrop->coin_id,
                                          "address"=>$payment_address,
                                          "memo_tag"=>"AIRDROP",
                                          "amount" => $amount,
                                          "status"=>7
                                      ];

                                      CoinDeposit::create($depositRequest);


                                  }



                              }else{
                                  $payment_address = "-";
                              }
                          }else{
                              $payment_address = "-";
                          }



                      }else{
                          // invalid airdrop user!
                      }



                  }




              }




        $airdrop->status = 3;
        $airdrop->save();


        }
    }
}
