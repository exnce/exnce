<?php

use Illuminate\Database\Seeder;

class DelistTokenOrCoin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coins = App\Coin::withTrashed()->get();
        foreach($coins as $coin){

            $delistedTokens = [
                                "AAVP","ERBC","MTCN","XTS","FTRM",
                                "BNT","MANA","HERB","BNB","HOT","POLY","OMG",
                                "REP","ENJ","ZIL","ZRX","PPT","DGD","DENT","NGS"];

            if(in_array($coin->symbol,$delistedTokens)){
               $pairs = \App\CoinCoin::where("pair_id",$coin->id)->get();
               if($pairs){
                   foreach($pairs as $pair){
                       $pair->delete();
                   }
               }
               $walletAddress = \App\WalletAddress::where("coin_id",$coin->id)->get();
                foreach($walletAddress as $walletAddr){
                    $walletAddr->status=3;
                    $walletAddr->save();
                    $walletAddr->delete();
                }
                $wallets = \App\Wallet::where("coin_id",$coin->id)->get();
                foreach($wallets as $wallet){
                    $wallet->status=3;
                    $wallet->save();
                    $wallet->delete();
                }

               $coin->status = 3;
               $coin->save();
               $coin->delete();
            }
        }
    }
}
