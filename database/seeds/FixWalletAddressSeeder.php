<?php

use Illuminate\Database\Seeder;

class FixWalletAddressSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $walletAddress = \App\WalletAddress::where("address","0xd1714E65fe76F07DAd0add5087E742c0c05aa0bc")->get()->first();
        if($walletAddress){
            $walletAddress->address="0xcFdEab0874b960E4B5e445534639B23F82C92D4f";
            $walletAddress->password="EXNCE_EXNCE_EXNCE_EXNCE_EXNCE_EXNCE_EXNCE_EXNCE";
            $walletAddress->save();
        }
    }
}
