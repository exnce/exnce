<?php

use Illuminate\Database\Seeder;

class UserCardsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cards = [
            [
                "user_id" => 190,
                "type" => "Mifare",
                "card_countrycode" => 90,
                "card_nfcid" => "",
                "card_icid" => "",
                "card_mcid" => "",
                "card_privateid" => "",
                "card_pin" => "",
                "card_no" => "3268415926600047",
                "card_expirationdate" => "1126",
                "card_cvv" => "056",
                "card_servicecode" => "",
                "track1" => "",
                "track2" => "",
                "track3" => "",
                "uuid" => "362370C3",
            ],
        ];

        foreach ($cards as $card){
            $checkCard = \App\UserCard::where("card_no", $card["card_no"])->first();
            if (!$checkCard){
                \App\UserCard::create($card);
            }
        }

    }
}
