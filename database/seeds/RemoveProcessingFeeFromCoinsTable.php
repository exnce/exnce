<?php

use Illuminate\Database\Seeder;

class RemoveProcessingFeeFromCoinsTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coins = App\Coin::where("status",0)->get();

        foreach ($coins as $coin){
            if($coin){
                $coin->processing_fee = 0.00;

                if($coin->symbol == "ETH"){
                    $coin->min_withdraw = 0.05;
                    $coin->fee = 0.005;
                }

                if($coin->symbol == "XLM"){
                    $coin->min_withdraw = 5;
                    $coin->fee = 8;
                }

                if($coin->symbol == "NPXS"){
                    $coin->min_withdraw = 2000;
                    $coin->fee = 0;
                }

                if($coin->symbol == "HERB"){
                    $coin->min_withdraw = 20000;
                    $coin->fee=25000;
                }

                if($coin->symbol == "HOT"){
                    $coin->min_withdraw = 1300;
                    $coin->fee=3500;
                }

                if($coin->symbol == "OMG"){
                    $coin->min_withdraw = 1;
                    $coin->fee=3;
                }

                if($coin->symbol == "REP"){
                    $coin->min_withdraw = 0.1;
                    $coin->fee=225;
                }

                if($coin->symbol == "ENJ"){
                    $coin->min_withdraw = 8;
                    $coin->fee=15;
                }

                if($coin->symbol == "BNB"){
                    $coin->min_withdraw = 1;
                    $coin->fee = 0.1;
                }

                if($coin->symbol == "TUSD"){
                    $coin->min_withdraw = 10;
                    $coin->fee = 10;
                }

                if($coin->symbol == "XTS"){
                    $coin->min_withdraw = 200000;
                    $coin->fee = 500000;
                }

                if($coin->symbol == "ETHPLO"){
                    $coin->min_withdraw = 0.00;
                    $coin->fee = 0.00;
                    $coin->processing_fee = 0.00;
                }

                if($coin->symbol == "XTRLPAY"){
                    $coin->min_withdraw = 500000;
                    $coin->fee = 500000;
                    $coin->processing_fee = 0.9;
                }

                $coin->save();
            }
        }
    }
}
