<?php

use App\Airdrop;
use App\Coin;
use Illuminate\Database\Seeder;

class DistributeCustomAirdrop extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $userList = ["gmail@gmail.com",
                    "gmail@gmail.com"];


        $coin = Coin::where("symbol","HUBBS")->get()->first();
        if($coin) {
            //$airdrop = Airdrop::where("status",0)->where("coin_id",$coin->id)->get()->first();
            $airdrop = Airdrop::where("coin_id", $coin->id)->get()->last();
            if ($airdrop) {
                foreach ($userList as $user){
                    $hunter = App\User::where("email",$user)->get()->first();
                    if($hunter){
                    $hunter->follow($airdrop->id,\App\Airdrop::class);
                    $rating = new willvincent\Rateable\Rating;
                    $rating->rating = 5;
                    $rating->user_id = $hunter->id;
                    $airdrop->ratings()->save($rating);
                    }else{

                    }

                }
            }

        }
    }

}
