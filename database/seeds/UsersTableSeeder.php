<?php

use App\User;
use Illuminate\Database\Seeder;
use jeremykenedy\LaravelRoles\Models\Permission;
use jeremykenedy\LaravelRoles\Models\Role;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userRole = Role::where('name', '=', 'User')->first();
        $adminRole = Role::where('name', '=', 'Admin')->first();
        $permissions = Permission::all();



            $admins = User::where('user_type', '=', 0)->get();
            foreach($admins as $admin){
                $admin->attachRole($adminRole);
                foreach ($permissions as $permission) {
                    $admin->attachPermission($permission);
                }
            }


            $users =  User::where('user_type', '=', 1)->get();
            foreach($users as $user){
                //$user->attachRole($userRole);
            }



    }
}
