<?php

use Illuminate\Database\Seeder;

class DeleteStellarWallets extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $coin = \App\Coin::where("symbol","XLM")->get()->first();
        if($coin){
            $wallets = \App\Wallet::where("coin_id",$coin->id)->get();
            foreach ($wallets as $wallet){
               if($wallet){
                   $walletAddress = \App\WalletAddress::where("coin_id",$coin->id)->where("wallet_id",$wallet->id)->get();
                   if($walletAddress){
                    foreach($walletAddress as $address){
                        $address->forceDelete();
                    }
                }
                $wallet->forceDelete();
               }
            }

        }

    }
}
