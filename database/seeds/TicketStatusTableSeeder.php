<?php

use Illuminate\Database\Seeder;

class TicketStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statusList = getSupportStatus();
        foreach($statusList as $status => $key) {
            $checkStatus = App\TicketStatus::where("key",$key["key"])->first();
            if (!$checkStatus){
                App\TicketStatus::create(["name"=>$key["value"], "key"=>$key["key"],"color"=>$key["color"]]);
            }
        }
    }
}
