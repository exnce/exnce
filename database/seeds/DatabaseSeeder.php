<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $this->call(CoinTableSeeder::class);
        $this->call(CreateAdminAccount::class);
        $this->call(TurkeyBankListSeeder::class );
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CountriesSeeder::class);
        $this->call(SecurityPhotosSeeder::class);
        $this->call(BankAccountTableSeeder::class);
        $this->call(WalletAddressesTableSeeder::class);
        $this->call(TicketPrioritiesTableSeeder::class);
        $this->call(TicketCategoriesTableSeeder::class );
        $this->call(TicketStatusTableSeeder::class );
        $this->call(UserRefCodeSeeder::class);
        $this->call(PairTableSeeder::class);
        $this->call(DelistTokenOrCoin::class);
        $this->call(CreateAirdropEntries::class);
        $this->call(PermissionsTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(ConnectRelationshipsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(FillCoinDetails::class);


    }
}
