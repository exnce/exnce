<?php

use Illuminate\Database\Seeder;

class BankAccountTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $accounts = [
            ["name" => "Arabic Test Bank A.Ş.", "iban" => "TR123456789012345678901234", "holder" => "TEST BANK ACCOUNT", "currency" => "TRY"],
            ["name" => "Arabic TestBank T.A.Ş.", "iban" => "TR345678901234567890123412", "holder" => "TEST BANK ACCOUNT", "currency" => "TRY"],

        ];

        foreach ($accounts as $account){
            $checkAccount = App\BankAccount::where("name",$account["name"])->where("iban",$account["iban"])->first();
            if (!$checkAccount){
                //App\BankAccount::create($account);
            }
        }
    }
}
