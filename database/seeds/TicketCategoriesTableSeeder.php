<?php

use Illuminate\Database\Seeder;

class TicketCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = getSupportCategories();
        foreach($categories as $category => $key) {
            $checkCategory = App\TicketCategory::where("key",$key["key"])->first();
            if (!$checkCategory){
                App\TicketCategory::create(["name"=>$key["value"], "key"=>$key["key"]]);
            }
        }
    }
}
