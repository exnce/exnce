<?php

use Illuminate\Database\Seeder;

class CreateAdminAccount extends Seeder
{
    /**
     * Run the database seeds.
     *
     *
     *
     *
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where("email","admin@exnce.com")
            ->first();
        if(!$user){
            $user =  new \App\User();
            $user->name = "Exchange Admin";
            $user->verified=9;
            $user->security_photo=rand(1,8);
            $user->email = "admin@exnce.com";
            $user->phone = "+90 530 016 06 80";
            $user->user_type = 0;
            $user->status = 1;
            $user->password = bcrypt("adminpassword");
            $user->save();
        }

        $pKey=generatePrivateKey("BURN_EXNCE_BURN");;
        $first_user = \App\User::where("email","user@exnce.com")
            ->first();
        if(!$first_user){
            $first_user =  new \App\User();
            $first_user->name = "Exchange Admin";
            $first_user->verified=2;
            $first_user->security_photo=rand(1,8);
            $first_user->email = "user@exnce.com";
            $first_user->phone = "+90 530 016 06 80";
            $first_user->private_key = $pKey;
            $first_user->public_key = generatePublicKey($pKey);
            $first_user->user_type = 1;
            $first_user->status = 1;
            $first_user->password = bcrypt("userpassword");
            $first_user->save();
        }
    }
}
