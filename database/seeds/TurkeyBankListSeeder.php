<?php

use Illuminate\Database\Seeder;

class TurkeyBankListSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
                ["bank_name"=>"Türkiye Cumhuriyeti Ziraat Bankası A.Ş."],
                ["bank_name"=>"Türkiye Halk Bankası A.Ş."],
                ["bank_name"=>"Akbank T.A.Ş."],
                ["bank_name"=>"Türk Ekonomi Bankası A.Ş."],
                ["bank_name"=>"Türkiye İş Bankası A.Ş."],
                ["bank_name"=>"Yapı ve Kredi Bankası A.Ş."],
                ["bank_name"=>"Denizbank A.Ş."],
                ["bank_name"=>"HSBC Bank A.Ş."],
                ["bank_name"=>"ING Bank A.Ş."],
                ["bank_name"=>"Türkiye Garanti Bankası A.Ş."],
                ["bank_name"=>"QNB Finansbank A.Ş."]
        ];

        foreach($banks as $bank) {
            $checkBank = App\Bank::where("bank_name",$bank["bank_name"])->first();
            if (!$checkBank){
                App\Bank::create($bank);
            }
        }

    }
}
