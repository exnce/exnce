<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RemovePrivateKeys extends Seeder
{
    /**
     *
    php artisan db:seed --class=RemovePrivateKeys

     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('wallet_addresses')
            ->update(['password' => ""]);
    }
}
