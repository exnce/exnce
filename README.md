Bilgilendirme

    Kesinlikle master isimli branch'e push edilmeyecek.
    Kesinlikle develop isimli branch'e push edilmeyecek.
    Issuelar yaratilmadan herhangi bir commit yapılmayacak
    Herhangi bir issue ile ilgili çalışma yapılırken issue ile ilgili branch'in source branch'i kesinlikle develop olmalı.
    Hiçkimse gitlab üzerinden merge yapmayacak.
    Projede herhangi bir hata duzeltmesi issue oluşturulmadan yapılmayacak.
    Issue ile ilgili oluşturulan branch içerisinde o issueden farklı hiçbir iyileştirme çalışması yapılmayacak.
    Proje içerisinde oluşturulacak yeni classlar controllerlar migrationlar ve seedler kesinlikle php artisan komutu ile yaratılmalıdır.    


Certainly, the branch named master will not be pushed.
*      It will definitely not be pushed to the branch named develop.
*      No commitments will be made until issuers are created
*      When working on any issue, the source branch of the issue must be develop.
*      Nobody's going to merge over gitlab.
*      The project will not be corrected without any issue correction.
*      Issue related to the issuance created in the branch will not be made any improvement studies.
*      New classes, controllers, migrations and seeds should be created with php artisan command.



Requipments

* REDIS
* MYSQL
* PHP
* COMPOSER
* NPM
* GIT


Proje Laravel Kullanılarak Geliştirilecektir.
Laravel Dökümantasyonu :
https://laravel.com/docs/5.6

sudo apt-get install php-gd

sudo apt-get install php7.2-mysql

composer dump-autoload

### Ilk Kurulum Icin
php artisan migrate:refresh --seed

sudo /opt/lampp/lampp start
 
#### Socket Sistemi
npm install --save laravel-echo

npm install --save socket.io-client

npm install -g laravel-echo-server

laravel-echo-server init

laravel-echo-server start

#### Laravel Kontrolleri

sudo supervisorctl stop all
 
sudo supervisorctl reread

sudo supervisorctl reload

sudo supervisorctl status


#### Redisi Baslatmak
redis-server


#### NPM START

npm run prod

#### yedek




calismaya baslamadan once;
Sunucudaki mevcut branchler listesi
git remote show origin

Sunucudaki MASTER isimli branchden kalitim alarak yeni bir branch yaratmak
git checkout -b YENIBRANCHADI origin/master

Degisiklikler yapildiktan sonra degisiklik yapilan butun dosyalarin gonderilmek uzere toplanmasi

git add .

Secilen dosyalarin commitlenmesi;
git commit -m "commit mesajin"

Commit edilen dosyalarin sunucuya gonderilmesi
git push origin YENIBRANCHADI

git checkout master
git merge origin/YENIBRANCHADI


#### Email gonderebilmesi ve callback servislerinin calisabilmesi icin
php artisan queue:work 








 