<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserApiKey extends Model
{
    use SoftDeletes;
    public $timestamps = true;
    protected $fillable = ["id","user_id","private_key","public_key","secret_key","can_access_user_info","can_access_balance_info","can_create_order","can_create_withdraw"];

    protected $hidden = [
        'id', 'user_id','private_key',"secret_key"
    ];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }
}
