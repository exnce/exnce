<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockTrack extends Model
{
    public $timestamps = true;

    protected $fillable = ["network_type","blockNumber"];

}
