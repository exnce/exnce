<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SecurityPhoto extends Model
{
    use SoftDeletes;
    protected $fillable = ["image"];
}
