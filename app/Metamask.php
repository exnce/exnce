<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Metamask extends Model
{
    protected $fillable = ["user_id","address","key","signature","is_valid"];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }
}
