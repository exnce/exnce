<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transaction extends Model
{
    use SoftDeletes;
    public $timestamps = true;

    protected $fillable = ["user_id","coin_id","currency_id","transaction_type","type","amount","real_amount","completed_amount","remaining_amount","price","total","fee","processing_fee","ref_code","status", "updated_at"];

    protected $hidden = [
        'id','coin_id','user_id','currency_id','created_at','deleted_at','status','ref_code'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coin(){
        return $this->belongsTo(Coin::class,"coin_id","id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        //return $this->belongsTo(Currency::class,"currency_id","id");
        return $this->belongsTo(Coin::class,"currency_id","id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details() {
        return $this->hasMany(TransactionDetail::class,"transaction_id");
    }

    public function destDetails() {
        return $this->hasMany(TransactionDetail::class,"dest_transaction_id");
    }


    /*    public function detail() {
            return $this->belongsTo(TransactionDetail::class, "")
        }*/
}
