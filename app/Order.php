<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = ["user_id","type","amount","fee","bank_id","transfer_code","operator_id","description","txid","status","currency_type","currency_id","coin_id"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank(){
        return $this->belongsTo(Bank::class,"bank_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_bank(){
        return $this->belongsTo(UserBank::class,"bank_id", "id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coin(){
        return $this->belongsTo(Coin::class,"coin_id", "id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency(){
        return $this->belongsTo(Currency::class,"currency_id", "id");
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank_account(){
        return $this->belongsTo(BankAccount::class,"bank_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function operator(){
        return $this->belongsTo(User::class,"operator_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detail(){
        return $this->belongsTo(OrderDetail::class,"id","order_id");
    }
}
