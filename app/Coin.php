<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Coin extends Model
{
    use SoftDeletes;

    protected $fillable = ["name","symbol","slug","min_deposit","min_withdraw","status","decimals","explorer","display_index"];

    protected $hidden = [
        'created_at','updated_at','deleted_at', 'contract', 'propertyid', 'fee'
    ];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('display_index', 'desc');
        });
    }

    /**
     * @param $currency_id
     * @return mixed
     */
    public function lastPrice($currency_id){
        return $this->belongsTo(TransactionDetail::class,"id","coin_id")
            ->where("type", "buy")
            ->where("currency_id",$currency_id)
            ->orderBy("id","desc")->value("price");
    }

    /**
     * @param $currency_id
     * @return string
     */
    public function volume($currency_id){
        $transactionSummary = TransactionDetail::where("coin_id",$this->id)->where("currency_id",$currency_id)->where("type","buy")
            ->where('updated_at', '>', Carbon::now()->subDay())->sum("amount");

        return number_format($transactionSummary, 2, ".", "");
    }

    public function change($currency_id){
        $dayTransactions = TransactionDetail::where("coin_id",$this->id)->where("currency_id",$currency_id)
            ->where('updated_at', '>', Carbon::now()->subDay())->get();

        if(!$dayTransactions->isEmpty()) {
            $open = $dayTransactions->first()->price;
            $last = $dayTransactions->last()->price;
            $diff = $last - $open; //todo: last yerine ask olmalı
            $changeRatio = round(($diff * 100) / $open ,2);
            return $changeRatio;
        } else {

            return 0;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function listPairs(){
        return $this->belongsToMany(Coin::class,null,"coin_id","pair_id")
            ->withPivot("pair_id");
    }


    /**
     * @return mixed
     */
    public function detail(){
        return $this->belongsTo(CoinDetail::class, "id","coin_id");
    }

}
