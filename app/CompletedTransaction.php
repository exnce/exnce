<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CompletedTransaction extends Model
{
    protected $fillable = ["user_id", "coin_id", "currency_id", "related_id", "type", "price", "total", "fee", "ref_code"];

    protected $hidden = ["id","user_id", "coin_id", "currency_id", "related_id","type", "fee", "ref_code", "created_at", "deleted_at"];

    public $timestamps = true;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coin(){
        return $this->belongsTo(Coin::class,"coin_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        //return $this->belongsTo(Currency::class,"currency_id","id");
        return $this->belongsTo(Coin::class,"currency_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
/*    public function transaction() {
        return $this->belongsTo(Transaction::class,"id","related_id");
    }*/
}
