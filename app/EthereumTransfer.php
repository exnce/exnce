<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EthereumTransfer extends Model
{
    use SoftDeletes;

    protected $fillable = ["deposit_id","coin_id","txid","address","amount","status"];

    public function wallet_address(){
        return $this->belongsTo(WalletAddress::class,"address","address");
    }
}
