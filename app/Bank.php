<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable = ["bank_name","status"];
    //BUTUN BANKALARDIR

    /**
     * @return mixed
     */
    public static function banks()
    {
        return self::where("status",1)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_bank()
    {
        return $this->hasMany(UserBank::class);
    }
}
