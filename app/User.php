<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanLike;
use Overtrue\LaravelFollow\Traits\CanFavorite;
use Overtrue\LaravelFollow\Traits\CanSubscribe;
use Overtrue\LaravelFollow\Traits\CanVote;
use Overtrue\LaravelFollow\Traits\CanBookmark;
use jeremykenedy\LaravelRoles\Traits\HasRoleAndPermission;
class User extends Authenticatable
{
    use Notifiable, HasApiTokens,HasRoleAndPermission;
    use CanFollow, CanBookmark, CanLike, CanFavorite, CanSubscribe, CanVote;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', "phone","calling_code","currency_id", 'user_type', 'email_token','verified','security_photo','private_key','public_key','campain_code','callback_url','is_metamask'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','private_key','public_key', 'google2fa_secret'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function wallets(){
        return $this->hasMany(Wallet::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cards(){
        return $this->hasMany(UserCard::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function user_banks(){
        #todo:status alani eklenecek migration;
        return  $this->hasMany(UserBank::class);//->where("status",1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders(){
        return  $this->hasMany(Order::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(){
        return $this->hasMany(Transaction::class,"user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function completedTransactions(){
        return $this->hasMany(TransactionDetail::class,"user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        return $this->belongsTo(Currency::class,"currency_id")->withTrashed();
    }

    /**
     * @param bool $includeSymbol
     * @param null $coinId
     * @param bool $isCurrency
     * @return float|string
     */
    public function user_balance($includeSymbol=true,$coinId=null,$isCurrency=true,$includeWithdrawAmount=true,$user = null,$includeTransactions=true){

        if(!$user){
            $user = auth()->user();
        }

        $currency= Coin::where("id",$coinId)->withTrashed()->get()->first();


        $balanceVal = 0.00;
        $withdrawVal = 0.00;
        $transactionSummary = 0.00;


        if($currency){
            $balanceWallet = Wallet::where("user_id",$user->id)->where("coin_id",$coinId)->withTrashed()->get()->first();
            if($balanceWallet){
                $balanceVal = $balanceWallet->balance;
            }


            if($includeTransactions) {
                // İşlemde Olan Transactionlarin Hesaplanmasi
                $transactionSummary = $user->transactions()->where("currency_id",$coinId)->where("type","sell")->where("status","<",2)->sum("amount");
                $transactionSummary += $user->transactions()->where("coin_id",$coinId)->where("type","buy")->where("status","<",2)->sum("total");



                $sells = $user->transactions()->where("coin_id",$coinId)->where("type","sell")->where("status", 1)->get();
                foreach ($sells as $transaction) {
                    $transactionSummary -= $transaction->details()->sum("total") - $transaction->details()->sum("fee");
                }

                $buys = $user->transactions()->where("currency_id",$coinId)->where("type","buy")->where("status", 1)->get();
                foreach ($buys as $transaction) {
                    $transactionSummary -= $transaction->details()->sum("amount") - $transaction->details()->sum("fee");
                }

            } else {
                $transactionSummary += $user->transactions()->where("currency_id",$coinId)->where("status", 1)->where("type", "sell")->sum("completed_amount");

                $openTransactions = $user->transactions()->where("coin_id",$coinId)->where("status", 1)->where("type", "sell")->get();
                foreach ($openTransactions as $transaction) {
                    $transactionSummary -= $transaction->details()->sum("total");
                }

                $transactionSummary -= $user->transactions()->where("currency_id",$coinId)->where("status", 1)->where("type", "buy")->sum("completed_amount");
                $openTransactions = $user->transactions()->where("coin_id",$coinId)->where("status", 1)->where("type", "buy")->get();
                foreach ($openTransactions as $transaction) {
                    $transactionSummary += $transaction->details()->sum("total");
                }
            }


            $balanceVal = $balanceVal - floatval($transactionSummary);


            //Çekim Taleplerinin Hesaplanmasi
            if($includeWithdrawAmount == true){
                $withdrawVal = $user->orders()->where("coin_id",$coinId)->where("status","<",2)->get()->sum(function ($row) {
                    return $row->amount + $row->fee;
                });

                $balanceVal = $balanceVal - $withdrawVal;
            }

            if($includeSymbol == true){
                $balanceVal = number_format($balanceVal, $currency->decimals,$currency->dec_point,$currency->thousands_sep);
                $balanceVal = sprintf("%s %s",$balanceVal, $currency->symbol);
            }
        }else{
            $balanceVal = 0.00;
        }

        return  $balanceVal;
    }

    /**
     * @param int $content_type
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\BelongsTo|\Illuminate\Database\Query\Builder|null|object
     */
    public function lastSms($content_type = 0){
        return $this->belongsTo(Interaction::class, 'id', 'user_id')
            ->whereIn("status", [0,1])
            ->where("type", "sms")
            ->where("content_type", $content_type)
            ->latest()->first();
    }

    /**
     * @return bool
     */
    public function identityApproval($status) {
        return $this->belongsTo(IdentityApproval::class, 'id', 'user_id')->where("status", $status)->exists();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_setting() {
        return $this->belongsTo(UserSetting::class,"id","user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function btcWallets() {
        return $this->hasMany(BtcAddress::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function apiKeys(){
        return $this->hasMany(UserApiKey::class,"user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function references(){
        return $this->hasMany(UserReference::class,"reference_id","id");
    }

    public function reference(){
        return $this->belongsTo(UserReference::class,"id","user_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets(){
        return $this->hasMany(Ticket::class,"user_id","id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favorites(){
        return $this->hasMany(UserFavorite::class,"user_id","id");
    }



}
