<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BillingDetail extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = ["user_id","nationality","tckn","first_name","last_name","birth_date","province","town","district","address"];
}
