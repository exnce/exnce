<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMeta extends Model
{
    protected $fillable = ["user_id","platform","platform_user_id","platform_user_name","platform_user_image","platform_user_token","platform_user_email","platform_user_data"];
}
