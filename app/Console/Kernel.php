<?php

namespace App\Console;

use App\Http\Controllers\RPC\BitcoinController;
use App\Http\Controllers\RPC\MoneroController;
use App\Http\Controllers\RPC\OmniController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\RPC\EthereumController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

//        $schedule->call(function (){
//            $ethereum = new EthereumController();
//            $ethereum->checkCoinDeposits();
//            $ethereum->checkEthereumTransfers();
//        })->everyMinute();
//
//        $schedule->call(function (){
//            $bitcoin = new BitcoinController();
//            $bitcoin->checkCoinDeposits();
//
//            $omni = new OmniController();
//            $omni->checkCoinDeposits();
//
//            $monero = new MoneroController();
//            $monero->checkCoinDeposits();
//        })->everyTenMinutes();
//
//        $schedule->call(function (){
//            EthereumController::deleteUnconfirmedCoinDeposits();
//        })->daily();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
