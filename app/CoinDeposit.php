<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CoinDeposit extends Model
{
    use SoftDeletes;

    protected $fillable = ["blockNumber","txid","coin_id","address","amount", "status","memo_tag", "confirmations", "created_at"];


    public function coin(){
        return $this->belongsTo(Coin::class, "coin_id","id")->withTrashed();
    }
    public function wallet(){
        return $this->belongsTo(WalletAddress::class,"address","address")->withTrashed();
    }


}
