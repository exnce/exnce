<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSetting extends Model
{
    use SoftDeletes;

    protected $fillable = ["user_id","country_id","city","crypto_deposit_email","crypto_withdraw_sms","crypto_deposit_email","crypto_deposit_sms"];


}
