<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReserveTransfer extends Model
{
    use SoftDeletes;

    protected $fillable = ["deposit_id","type","status","txid"];
}