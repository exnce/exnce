<?php
use App\Coin;
use App\CoinDeposit;
use App\CompletedTransaction;
use App\Order;
use Carbon\Carbon;
use \App\BtcAddress;
use \App\WalletAddress;


function getDefaultBaseCoin(){
    return "ETH";
}

function getDefaultSubCoin(){
    return "XLM";
}

/* IMPORTANT : DO NOT CHANGE */
function getAPISecretKeyGenerateParam(){
    return "EXNCE_API_SECRET_KEY_PARAM_EXNCE_API_SECRET_KEY_PARAM";
}
function getAPISecretKeyPrefix(){
    return "EXNCE_EXCHANGE_SECRET_KEYS";
}
/* IMPORTANT : DO NOT CHANGE */

function getCoinSymbol($coin_id){
    $coin = \App\Coin::where("id",$coin_id)->get()->first();
    return $coin->symbol;
}

/**
 * @param $name
 * @return array
 */
function splitName($name) {
    $parts = explode(' ', $name);
    return array(
        'firstname' => array_shift($parts),
        'lastname' => array_pop($parts),
        'middlename' => join(' ', $parts)
    );
}


/**
 * @param $a
 * @param $b
 * @param string $operator
 * @return mixed
 */
function _bccomp($a, $b, $operator = '=')
{
    return version_compare(bccomp($a, $b), 0, $operator);
}

function randHash() {
    return md5(rand(1,400000000) + time());
}

/**
 * @param bool $isAPI
 * @return mixed
 */
function getCoinsStatusInformation($isAPI = false){
    $coins = Coin::get();

    $closedTradings = ["REV","REVS","REVG","REVP","REVD"];

    $delistingTokens = ["XTRLPAY","ILT","XGO","0xLTC","HUBBS","KZE","ALDX"];
    foreach($coins as &$coin){
        if((in_array($coin->symbol,$delistingTokens))){
            $coin["can_trade"]  = false;
            $coin["maintenance_note"] = "Delisting...";
            $coin["can_deposit"] = false;
            $coin["can_withdraw"] = true;
        }else if($coin->symbol == "ORX"){
            $coin["can_trade"]  = false;
            $coin["can_withdraw"]  = true;
            $coin["can_deposit"] = true;
            $coin["maintenance_note"] = "Waiting for presale...";

        }
        else if($coin->symbol == "SPZ"){
            $coin["can_trade"]  = true;
            $coin["can_withdraw"]  = false;
            $coin["can_deposit"] = false;
            $coin["maintenance_note"] = "Under of maintenence...";
        }
        else if($coin->symbol == "MRCL"){
            $coin["can_trade"]  = false;
            $coin["can_withdraw"]  = false;
            $coin["can_deposit"] = true;
            $coin["maintenance_note"] = "Under of maintenence...";
        }
        else if(in_array($coin->symbol,$closedTradings)){
            $coin["can_trade"]  = false;
            $coin["can_withdraw"]  = true;
            $coin["can_deposit"] = true;
            $coin["maintenance_note"] = "No volume...";
        } else{
            $coin["can_trade"] = true;
            $coin["maintenance_note"] = "-";
            $coin["can_deposit"] = true;
            $coin["can_withdraw"] = true;
        }

        if($coin->symbol == "ETH"){
            $coin["pending_deposits"]  = 0; // because of gas fee...
        }else{
            $coin["pending_deposits"] = CoinDeposit::where("coin_id",$coin->id)->where("status",0)->count();
        }

        $coin["pending_withdrawals"] = Order::where("coin_id",$coin->id)->where("status",1)->count();
        if($isAPI == true){
            unset($coin["explorer"]);
            unset($coin["display_index"]);
            unset($coin["dec_point"]);
            unset($coin["decimals"]);
            unset($coin["thousands_sep"]);
            unset($coin["pending_withdrawals"]);
            unset($coin["ieo"]);
            unset($coin["pending_deposits"]);
            unset($coin["id"]);
            unset($coin["status"]);
        }
    }



    return $coins;
}

function get_starred_email($email) {
    $szEmail = $email;
    $mail_part = explode("@", $szEmail);
    $mail_part[0] = get_starred($mail_part[0]);
    return implode("@", $mail_part);
}

function getSEOMetaTags(Coin $coin = null, Coin $currency = null, $page){

    $szDescription = "";
    $szTitle = "";

    switch ($page){
        case "airdrops":
            $szTitle = "EXNCE AIRDROP";
            $szDescription = "Are you looking for free crypto airdrops? Try exnce.com! EXNCE airdrop is your free access to new token projects. Uniting airdrop channels and projects worldwide, EXNCE creates a new one-stop airdrop mode with fast, efficient and credible guarantee for all our customers.";
        break;
        case "status":
            $szTitle = "System Status";
            $szDescription = " The System Status page is a monitoring tool designed by our team that provides near real-time information about availability and performance, as well as incoming and outgoing transaction status. This is an evolving tool that will include more information on the system performance, aiming at making our system processes as transparent as possible.";
            break;
        case "market":

            if(($coin) && ($currency)){
                $szDescription = _i("Сurrent %s (%s) / %s (%s) exchange rate, Real-time market data: buy and sell rate, prices, charts, depth, order book.");
                $szDescription = sprintf($szDescription, $coin->name,$coin->symbol,$currency->name,$currency->symbol);
                $szTitle = _i("%s/%s | Buy %s Coin | %s");
                $szTitle = sprintf($szTitle, $coin->symbol,$currency->symbol,$coin->name,env("APP_NAME"));
            }
            break;
        default:
            $szTitle="EXNCE is the most advanced cryptocurrency exchange";
            $szDescription="Exnce is a world-leading digital asset exchange, providing advanced financial services to traders globally by using blockchain technology.";
            break;
    }


    $brandLogo = asset("/images/brand-thumbnail.png");

    $retStr = "";
    $retStr .= '<meta name="description" content="'.$szDescription.'"/>'."\n";
    $retStr .= '<link rel="canonical" href="'.url()->current().'" />'."\n";
    $retStr .= '<meta property="og:locale" content="'.app()->getLocale().'" />'."\n";
    $retStr .= '<meta property="og:type" content="article" />'."\n";
    $retStr .= '<meta property="og:title" content="'.$szTitle.'" />'."\n";
    $retStr .= '<meta property="og:description" content="'.$szDescription.'" />'."\n";
    $retStr .= '<meta property="og:url" content="'.url()->current().'" />'."\n";
    $retStr .= '<meta property="og:site_name" content="'.env("APP_NAME").'" />'."\n";
    $retStr .= '<meta property="article:publisher" content="https://twitter.com/exncecom" />'."\n";
    $retStr .= '<meta property="og:image" content="'.$brandLogo.'" />'."\n";
    $retStr .= '<meta property="og:image:secure_url" content="'.$brandLogo.'" />'."\n";
    $retStr .= '<meta name="twitter:card" content="summary_large_image" />'."\n";
    $retStr .= '<meta name="twitter:description" content="'.$szDescription.'" />'."\n";
    $retStr .= '<meta name="twitter:title" content="'.$szTitle.'" />'."\n";
    $retStr .= '<meta name="twitter:site" content="@exncecom" />'."\n";
    $retStr .= '<meta name="twitter:image" content="'.$brandLogo.'" />'."\n";
    $retStr .= '<meta name="twitter:creator" content="@exncecom" />'."\n";
    return $retStr;
}


function get_starred($str_name) {
    $words = explode(" ", $str_name);
    $nameList = [];
    foreach($words as $str){
        $str_length = mb_strlen($str,"utf-8");
        if(($str_length != 0) && ($str_length-2>0)){
        $nameList[] = mb_substr($str, 0, 1,"utf-8").str_repeat('*', $str_length - 2).mb_substr($str, $str_length - 1, 1,"utf-8");
        }else{
            $nameList[]="*";
        }
    }
    return implode(" ",$nameList);
}

function calculateUserReferralBalance($userId){
    return "0.0000000";
}

function format_coin_amount($amount, Coin $coin, $includeSymbol = true) {
    $formatted_amount = number_format($amount, $coin->decimals,$coin->dec_point,$coin->thousands_sep);

    if($includeSymbol == true){
        $formatted_amount = sprintf("%s %s",$formatted_amount, $coin->symbol);
    }

    return $formatted_amount;
}

function getHeaderHomeStatistics($coinId=null,$currencyId=null){
    if($coinId && $currencyId) {
        $coin =     \App\Coin::where("id",$coinId)->get()->first();
        $currency = \App\Coin::where("id",$currencyId)->get()->first();
    } else {
        $coin =     \App\Coin::where("symbol",getDefaultBaseCoin())->get()->first();
        $currency = \App\Coin::where("symbol",getDefaultSubCoin())->get()->first();
    }

    $startDate = Carbon::now()->subDay();
    $endDate = Carbon::now();

    $dayTransactions = \App\TransactionDetail::where("type", "buy")
        ->whereBetween("updated_at", [$startDate, $endDate])
        ->where("coin_id",$coin->id)->where("currency_id",$currency->id)->get();


    $market_history = \App\TransactionDetail::where("coin_id",$coin->id)
        ->where("currency_id",$currency->id)
        ->where("type", "buy")
        ->orderBy("id", "desc")
        ->take(3)->get();

    for($i = count($market_history)-1; $i >= 0; $i--) {
        if(!@$oldPrice) $oldPrice = $market_history[$i]->price;

        if($market_history[$i]->price > $oldPrice) {
            $market_history[$i]->color = 1;
        } elseif($market_history[$i]->price < $oldPrice) {
            $market_history[$i]->color = 2;
        } else {
            $market_history[$i]->color = 0;
        }

        $oldPrice = $market_history[$i]->price;
    }

    $szPriceClass = "text-dark";
    if(count($market_history)>0){
        switch ($market_history[0]->color){
            case 0: $szPriceClass = "text-dark";break;
            case 1: $szPriceClass = "text-success"; break;
            case 2: $szPriceClass = "text-danger";break;
        }
    }


    $lastPrice = (!$dayTransactions->isEmpty() ? floattostr($dayTransactions->last()->price) : 0);

    $minPrice = 0.00;
    $maxPrice = 0.00;
    $changeRatio = 0.00;

    if(!$dayTransactions->isEmpty()) {
        $minPrice = floattostr($dayTransactions->min("price"));
        $maxPrice = floattostr($dayTransactions->max("price"));

        $open = floattostr($dayTransactions->first()->price);
        $diff = $lastPrice - $open; //todo: lastPrice yerine ask olmalı
        $changeRatio = round(($diff * 100) / $open ,2);
    }

    $szChangeClass = "";
    if($changeRatio > 0) {
        $szChangeClass = "text-sucess";
    } elseif($changeRatio < 0) {
        $szChangeClass = "text-danger";
    } else {
        $szChangeClass = "text-dark";
    }

/*
.mh-sameprice{
.mh-buy{
.mh-sell{
*/

    $szPriceHTML = array("color"=>$szPriceClass, "data"=>$lastPrice,"symbol"=>$coin->symbol);
    $szChange24HHtml = array("color"=> $szChangeClass,"data" => $changeRatio,"symbol"=>"%");
    $sz24SMaxPriceHTML = array("color"=>"text-dark","data"=> $maxPrice, "symbol" =>$coin->symbol);
    $sz24SMinPriceHTML = array("color"=>"text-dark","data"=> $minPrice,"symbol"=>$coin->symbol);


    $html = [
            "price24h"=>$szPriceHTML,
            "change24h"=>$szChange24HHtml,
            "priceMax24h"=>$sz24SMaxPriceHTML,
            "priceMin24h"=>$sz24SMinPriceHTML
    ];


    $data = [
        "coin"=>$coin,
        "currency"=>$currency,
        "html"=>$html,
        "price"=>$lastPrice,
        "minimum_price"=>($minPrice),
        "maximum_price" => ($maxPrice),
        "volume" => $coin->volume($currency->id),
        "change_value"=>"",
        "change_ratio"=>floatval($changeRatio)

    ];


    return $data;
}

function floattostr( $val )
{
    preg_match( "#^([\+\-]|)([0-9]*)(\.([0-9]*?)|)(0*)$#", trim($val), $o );
    return $o[1].sprintf('%d',$o[2]).($o[3]!='.'?$o[3]:'');
}


function generateTransactionCode() {
    return strtoupper(substr(md5(uniqid(mt_rand(), true)) , rand(0, 21), 15));
}

function generatePrivateKey($password) {
    return strtoupper(md5(uniqid(mt_rand(), true).$password));
}

function generatePublicKey($privateKey){
    return strtoupper(md5($privateKey));
}

function toFixed($number, $dec_length){
    $szformat = "%.0".$dec_length."F";
    return sprintf($szformat, $number);
}


function getCoinByPaymentAddress($paymentAddress)
{
   $address = \App\BtcAddress::where("address",$paymentAddress)->withTrashed()->get()->first();
   if(!$address){
       $address = \App\WalletAddress::where("address",$paymentAddress)->get()->first();
   }

   $coin = null;
   if($address){
       $coin = \App\Coin::where("id",$address->coin_id)->first();
   }
    return $coin;
}



function DepositStatusCode($status){

    $statusCodes=[
        _i("Waiting for approval..."),
        _i("Deposit has been completed successfully."),
        _i("Deposit has been cancelled successfully."),
        _i("User canceled Deposit."),
        _i("User request for deposits has been canceled."),
        _i("Deposits has been accepted by Operator."),
        _i("Gas Price for Tokens."),
        _i("AIRDROP has been installed.")
    ];
    return $statusCodes[$status];
}

function WithdrawAdminStatusCode($status){
    $statusCodes=[
        _i("Kullanıcı çekim talebini oluşturdu ancak henüz onaylamadı..."),//0
        _i("Kullanıcının çekim talebini onaylanamanız gerekmektedir."),//1
        _i("Kullanıcı Çekim Talebini Onayladı. Talebi Onay Bekliyor..."),//2
        _i("Kullanıcı Ödeme Talebini Onaylaması Bekleniyor."),//3
        _i("İşlem İptal Edildi."),//4
        _i("İşlem Tamamlandı."),//5
        _i("Yetersiz Bakiye Sebebi ile Kullanıcının İsteği Reddedildi.")//6
    ];
    return $statusCodes[$status];
}


function WithdrawStatusCode($status){
    $statusCodes=[
        _i("You must approve the transaction of your withdrawal request..."),//0
        _i("Your withdrawal request has been processed."),//1
        _i("User Approves Shooting Request. Request Waiting for Approval..."),//2
        _i("Pending User Confirmation of Payment."),//3
        _i("Your transaction has been canceled."),//4
        _i("Completed."),//5
        _i("User's Request Denied Due to Insufficient Balance.")//6
    ];
    return $statusCodes[$status];
}

function getCryptoCurrencyStatusCodes(){
   return [
       0=>_i("Active"),
       1=>_i("Passive"),
       2=>_i("Suspended!"),
       3=>_i("Removed."),
       4=>_i("Testing...")
   ];
}

function CryptocurrencyStatusCode($status){
    $statusCodes= getCryptoCurrencyStatusCodes();
    return $statusCodes[$status];
}

function getFiatCurrencyStatusCodes(){
    return [
        0=>_i("Active"),
        1=>_i("PAssive"),
        2=>_i("Suspended"),
        3=>_i("Removed"),
        4=>_i("Testing...")
    ];
}

function FiatcurrencyStatusCode($status){
    $statusCodes= getFiatCurrencyStatusCodes();
    return $statusCodes[$status];
}

function getBankStatusCodes(){
    return [
        0=>_i("Passive"),
        1=>_i("Active"),
        2=>_i("Removed")
    ];
}
function BanksStatusCode($status){
    $statusCodes= getBankStatusCodes();
    return $statusCodes[$status];
}

function getBankAccountStatusCodes(){
    return [
        0=>_i("Passive"),
        1=>_i("Active")
    ];
}

function BankAccountsStatusCode($status){
    $statusCodes= getBankAccountStatusCodes();
    return $statusCodes[$status];
}


function getUserPermissionStatusCodes(){
    return [
        0=>_i("Admin"),
        1=>_i("User")
    ];
}
function UserStatusPermissionCode($status){
    $statusCodes= getUserPermissionStatusCodes();
    return $statusCodes[$status];
}

function getTransactionStatusCodes() {
    return [
        0=>_i("Waiting..."),
        1=>_i("Processing..."),
        2=>_i("Completed"),
        3=>_i("Cancelled")
    ];
}

function TransactionStatusCodes($status){
    $statusCodes=getTransactionStatusCodes();
    return $statusCodes[$status];
}

function SmsContent($type) {
    $contents=[
        _i("Mobil dogrulama kodunuz: %s"),
        _i("Tek kullanimlik sifreniz: %s . Bu sifreyi borsa personeli dahil hic kimseyle paylasmayiniz."),
    ];
    return $contents[$type];
}

function getIdentityApprovalStatusCodes() {
    return [
        _i("Waiting for approval"),
        _i("Rejected"),
        _i("Approved"),
    ];
}


function getSupportPriority(){
    return [
       array("key"=>"low","value"=> _i("Low")),
       array("key"=>"normal","value"=> _i("Normal")),
       array("key"=>"high","value"=> _i("High"))
    ];
}

function getSupportCategories(){
    return [
        array("key"=>"technical_support", "value"=> _i("Deposit")),
        array("key"=>"technical_support", "value"=> _i("Withdrawal")),
        array("key"=>"technical_support", "value"=> _i("Technical Support")),
        array("key"=>"customer_services","value"=> _i("Customer Services")),
        array("key"=>"finance", "value"=>_i("Other Financial Services"))
    ];
}

function getSupportStatus(){
    return [
        array("key"=>"pending", "value"=> _i("Waiting..."),"color"=>"#FFBF00"),
        array("key"=>"answered", "value"=> _i("Answered."),"color"=>"#0080FF"),
        array("key"=>"solved","value"=> _i("Solved."),"color"=>"#01DF01"),
        array("key"=>"unresolved","value"=> _i("Unsolved."),"color"=>"#FF0040"),
        array("key"=>"routed","value"=> _i("Forwarded."),"color"=>"#8000FF"),
        array("key"=>"bug", "value"=>_i("Error!"),"color"=>"#FF3333")
    ];
}

function getSingleSupportStatus($statusKey){
    $statusList = getSupportStatus();
    foreach($statusList as $status => $value){
        if($statusKey == $status){
            return $value;
        }
    }
}

function IdentityApprovalStatusCode($status) {
    $statusCodes=getIdentityApprovalStatusCodes();
    return $statusCodes[$status];
}


function getAPIKeyAccessPermissions(){
    return
    [
        array("key"=>"can_access_user_info", "title"=>_i("Can access user informations")),
        array("key"=>"can_access_balance_info", "title"=>_i("Can access user balance informations")),
        array("key"=>"can_access_orderbook", "title"=>_i("Can get orderbook bids and asks")),
        array("key"=>"can_create_order", "title"=>_i("Can create order")),
        array("key"=>"can_create_withdraw", "title"=>_i("Can create withdraw request"))
    ];

}

function getUserAccountTypes(){
    $accountTypes = [
        0 => _i("Administrator"),
        1 => _i("Personal Account"),
        2 => _i("Support"),
        3 => _i("Support Manager"),
        4 => _i("Merchant Account"),
        5 => _i("Vendor Account"),
        6 => _i("VIP Account")
    ];
    return $accountTypes;
}

function getUserAccountType($accountType){
    $accountTypes =getUserAccountTypes();
    return $accountTypes[$accountType];
}

?>