<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserReference extends Model
{
    protected $fillable = ["user_id","reference_id"];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

    public function reference_user(){
        return $this->belongsTo(User::class,"reference_id","id");
    }

}
