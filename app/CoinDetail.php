<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoinDetail extends Model
{

    protected $fillable = [
        "coin_id",
        "description",
        "video",
        "whitepaper",
        "website",
        "telegram",
        "github",
        "twitter",
        "reddit",
        "facebook",
        "email",
        "phone"];

}
