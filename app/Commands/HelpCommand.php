<?php

namespace App\Commands;
use App\Coin;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
/**
 * Class HelpCommand.
 */
class HelpCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = 'help';

    /**
     * @var string Command Description
     */
    protected $description = 'Help command, Get a list of commands';

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {
        return;
    }
}
