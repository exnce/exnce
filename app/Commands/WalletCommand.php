<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 6/29/19
 * Time: 4:00 AM
 */

namespace App\Commands;


use App\Coin;
use App\User;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Illuminate\Support\Facades\Storage;
 use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class WalletCommand extends Command
{

    /**
     * @var string Command Name
     */
    protected $name = "wallet";

    /**
     * @var string Command Description
     */
    protected $description = "Get Wallet [eg: /wallet ETH test@exnce.com]";

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {

        $parameters = explode ( " " , $arguments);
        if( count($parameters)==2){
            $symbol = $parameters[0];
            $userMail = $parameters[1];


            $coin = Coin::where("symbol",$symbol)->get()->first();
            if($coin){
                $user = User::where("email",$userMail)->get()->first();
                if($user){
                    $wallet = $user->wallets()->where("type", false)->where("coin_id", $coin->id)->get()->first();

                    if($wallet){

                        $responseData = "<b>Payment Address:</b>".$wallet->address()->get()->last()->address.PHP_EOL;
                        if($wallet->address()->get()->last()->address_type != "ERC20"){
                            $responseData .= "<b>Destination Tag:</b>".(10000000000 + $wallet->address()->get()->last()->id).PHP_EOL;
                            $qrData = "address://".$wallet->address()->get()->last()->address."?&tag=".(10000000000 + $wallet->address()->get()->last()->id);

                        }else{
                            $qrData = "address://".$wallet->address()->get()->last()->address;
                        }

                        $path = public_path() . '/images/' . "brand-thumbnail.png";

                        if(!File::exists($path)) {
                            return response()->json(['message' => 'Image not found.'], 404);
                        }

                        $file = File::get($path);
                        $type = File::mimeType($path);

                        $image = QrCode::format('png')
                            ->color(6,165,221)
                            ->mergeString($file, .25)
                            ->errorCorrection('H')->margin(0)->size(756)->generate( $qrData );


                        $str_hash = randHash();

                        $imageName = "walletqrimages/".$str_hash.".png";
                        Storage::disk('local')->put($imageName, $image);
                        $storagePath = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
                        $fullImagePath = $storagePath.$imageName;

                        $this->replyWithChatAction(['action' => Actions::TYPING]);
                        $this->replyWithPhoto(['photo' => $fullImagePath,
                            'caption' => "EXNCE ". $coin->name." WALLET"]);
                        $this->replyWithMessage(['text' => $responseData,'parse_mode'=>'html']);


                    }


                }
            }


        }else{
            return;
        }


    }

}