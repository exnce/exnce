<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 6.09.2019
 * Time: 10:24
 */

namespace App\Commands;

use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
class StatusCommand extends Command
{
    /**
     * @var string Command Name
     */
    protected $name = "status";

    /**
     * @var string Command Description
     */
    protected $description = "System Status [eg: /status ethplo]";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {


        $canWrite = true;
        $statusList = getCoinsStatusInformation(true);
        $responseData = "<b>EXNCE System Status</b>". PHP_EOL.PHP_EOL;
        foreach ($statusList as $statusItem){

            if($arguments){
                $canWrite = false;

                if(strcasecmp($statusItem->symbol, $arguments) == 0){
                    $canWrite = true;
                }
            }else{
                $canWrite = true;
            }

            if($canWrite == true){
                $responseData .=
                    "<b>Asset:</b>" . $statusItem->name . "(" . $statusItem->symbol .")". PHP_EOL.
                    "<b>Deposit:</b>" . ($statusItem->can_deposit ? "Active" : "Offline") . PHP_EOL.
                    "<b>Withdrawal:</b>" .($statusItem->can_withdraw ? "Active" : "Offline") . PHP_EOL.
                    "<b>Trade:</b>" . ($statusItem->can_trade ? "Active" : "Offline") . PHP_EOL.
                    "<b>Maintenance Note:</b>" . ($statusItem->maintenance_note) . PHP_EOL.PHP_EOL;
                }
            }

        $this->replyWithChatAction(['action' => Actions::TYPING]);
        $this->replyWithMessage(['text' => $responseData,'parse_mode'=>'html']);

    }
}