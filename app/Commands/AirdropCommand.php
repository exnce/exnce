<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 6/29/19
 * Time: 2:08 AM
 */

namespace App\Commands;


use App\Airdrop;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Telegram\Bot\Laravel\Facades\Telegram;

class AirdropCommand extends Command
{


    /**
     * @var string Command Name
     */
    protected $name = "airdrop";

    /**
     * @var string Command Description
     */
    protected $description = "Running Airdrops [eg: /airdrop list]";

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {

        if(!$arguments){
            return;
        }

        $responseData = "";

        $hasResponse = false;

        if($arguments == "list"){
            $airdrops = Airdrop::orderBy("id", "desc")->get();
            foreach ($airdrops as $airdrop){
               $responseData.="<b>Name:</b> ".$airdrop->name.PHP_EOL;
               $responseData.="<b>Asset:</b> ".$airdrop->coin->name.PHP_EOL;
               $responseData.="<b>Symbol:</b> ".$airdrop->coin->symbol.PHP_EOL;
               $responseData.="<b>Finish Date:</b> ".$airdrop->finish_date.PHP_EOL;
               $responseData.="<b>Model:</b> ". $airdrop->model.PHP_EOL;
               $responseData.="<b>Asset Amount:</b> ". $airdrop->amount.PHP_EOL.PHP_EOL;
               $hasResponse = true;
            }
        }
        if($hasResponse){
            $this->replyWithChatAction(['action' => Actions::TYPING]);
            $this->replyWithMessage(['text' => $responseData,'parse_mode'=>'html']);
        }
    }
}