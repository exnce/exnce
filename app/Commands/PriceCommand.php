<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 6/28/19
 * Time: 2:07 AM
 */

namespace App\Commands;


use App\Coin;
use App\CoinCoin;
use Carbon\Carbon;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class PriceCommand extends Command
{


    /**
     * @var string Command Name
     */
    protected $name = "price";

    /**
     * @var string Command Description
     */
    protected $description = "Coin's Prices [eg: /price ETHPLO]";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {

        if($arguments){
            $coin = Coin::where("symbol",$arguments)->first();
            if($coin){
                $includeFees = false;
                $pairList = [];
                $pairs = CoinCoin::with(["pair","coin","price"])->where("pair_id",$coin->id)->orderBy("coin_coin.display_index","desc")->get();


                $responseData = "";

                $hasResponse = false;
                foreach($pairs as $pair) {
                    $coin = $pair->pair;
                    $currency = $pair->coin;

                    if(!$currency->ieo){
                            $lastPrice = $pair->price()->where("currency_id",$currency->id)->where("type", "buy")->orderBy("id","desc")->value("price");
                            $volume =   $pair->price()->where("currency_id",$currency->id)->where("type", "buy")->where('updated_at', '>', Carbon::now()->subDay())->sum("amount");
                            $change = $coin->change($currency->id);
                            $volume = number_format($volume, 2, ".", "");

                            $responseData .=
                                "<b>Pair   :</b>" . $currency->symbol . "/" . $coin->symbol . PHP_EOL.
                                "<b>Price  :</b>" . toFixed( $lastPrice?$lastPrice:0,8) . PHP_EOL.
                                "<b>Volume :</b>" . $volume . PHP_EOL.
                                "<b>Change :</b>" . $change . " %" . PHP_EOL.PHP_EOL;

                        $hasResponse = true;
                    }
                }
                if($hasResponse){
                    $this->replyWithChatAction(['action' => Actions::TYPING]);
                    $this->replyWithMessage(['text' => $responseData,'parse_mode'=>'html']);
                }

            }
        }



        // Trigger another command dynamically from within this command
        // When you want to chain multiple commands within one or process the request further.
        // The method supports second parameter arguments which you can optionally pass, By default
        // it'll pass the same arguments that are received for this command originally.
       // $this->triggerCommand('subscribe');
    }
}