<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 6/28/19
 * Time: 3:05 AM
 */

namespace App\Commands;


use App\Coin;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;

class InfoCommand extends Command
{


    /**
     * @var string Command Name
     */
    protected $name = "info";

    /**
     * @var string Command Description
     */
    protected $description = "Coin's Information [eg: /info ETHPLO]";

    /**
     * @inheritdoc
     */
    public function handle($arguments)
    {
        $responseData = "";

        $hasResponse = false;
        if($arguments){
            $coin = Coin::where("symbol",$arguments)->first();
             if($coin){

                 $detail=$coin->detail()->first();

                 $whitePaper = "";
                 $facebook = "";
                 $youtube = "";
                 $website = "";
                 $medium = "";
                 $github = "";
                 $reddit = "";
                 $email = "";
                 $description = "";
                 $twitter="";
                 $telegram = "";
                 $coingecko = "";
                 $coinmarketcap = "";
                 if($detail){
                     $whitePaper = $detail->whitepaper;
                     $website = $detail->website;
                     $facebook = $detail->facebook;
                     $telegram = $detail->telegram;
                     $github = $detail->github;
                     $reddit = $detail->reddit;
                     $email = $detail->email;
                     $medium = $detail->medium;
                     $youtube = $detail->youtube;
                     $description = $detail->description;
                     $twitter = $detail->twitter;
                     $coinmarketcap = $detail->coinmarketcap;
                     $coingecko = $detail->coingecko;
                 }

                 $responseData =
                     "<b>Name :</b>".$coin->name. PHP_EOL.
                     "<b>Symbol :</b>".$coin->symbol. PHP_EOL.
                     "<b>Coinmarketcap :</b>".$coinmarketcap. PHP_EOL.
                     "<b>Coingecko :</b>".$coingecko. PHP_EOL.
                     "<b>WhitePaper :</b>".$whitePaper. PHP_EOL.

                     "<b>Email :</b>".$email. PHP_EOL.
                     "<b>Web Site :</b>".$website. PHP_EOL.
                     "<b>Facebook :</b>".$facebook. PHP_EOL.
                     "<b>Telegram :</b>".$telegram. PHP_EOL.
                     "<b>Twitter :</b>".$twitter. PHP_EOL.
                     "<b>Reddit :</b>".$reddit. PHP_EOL.
                     "<b>Youtube :</b>".$youtube. PHP_EOL.
                     "<b>Medium :</b>".$medium. PHP_EOL.
                     "<b>Github :</b>".$github. PHP_EOL.
                     "<b>Description :</b>". PHP_EOL.
                     $description. PHP_EOL;

                 $hasResponse = true;

                 if($hasResponse){
                    $this->replyWithChatAction(['action' => Actions::TYPING]);
                    $this->replyWithMessage(['text' => $responseData,'parse_mode'=>'html']);
                 }

            }
        }
      //  $this->triggerCommand('subscribe');
    }

}