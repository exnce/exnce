<?php
/**
 * Created by PhpStorm.
 * User: lenovo
 * Date: 9.08.2019
 * Time: 15:50
 */

namespace App\Commands;
use Telegram\Bot\Actions;
use Telegram\Bot\Commands\Command;
use Stichoza\GoogleTranslate\GoogleTranslate;

class TranslateCommand extends Command
{

    /**
     * @var string Command Name
     */
    protected $name = "translate";

    /**
     * @var string Command Description
     */
    protected $description = "Translate [eg: /translate EN IT Hello EXNCE!]";

    /**
     * {@inheritdoc}
     */
    public function handle($arguments)
    {

        $parameters = explode ( " " , $arguments);
        if( count($parameters)>=3){
            $source = $parameters[0];
            $destination = $parameters[1];
            if((strlen($source) == 2) && (strlen($destination) == 2))
            {
                unset($parameters[0]);
                unset($parameters[1]);
                $text = implode(" ",$parameters);
                $tr = new GoogleTranslate(); // Translates to 'en' from auto-detected language by default
                $tr->setSource($source); // Translate from English
                $tr->setTarget($destination); // Translate to Georgian
                if(strlen($text)<5){
                    $translatedText = "Message is to short!";
                }else{
                    try {
                        $translatedText =  $tr->translate($text);
                        //$translatedText = "Message is too short!";
                        $translatedText = $translatedText  ? $translatedText  : "Invalid Message!";
                    } catch (\Exception $e) {
                        $translatedText = "An error occurred please try again later!";
                    }

                    if($translatedText == null){
                        $translatedText = "An error occurred please try again later!";
                    }
                }
                $this->replyWithChatAction(['action' => Actions::TYPING]);
                $this->replyWithMessage(['text' => $translatedText. PHP_EOL]);
            }else{
                $this->replyWithChatAction(['action' => Actions::TYPING]);
                $this->replyWithMessage(['text' => "Invalid Language Code!". PHP_EOL]);
            }
        }else{
            return;
        }


    }

}