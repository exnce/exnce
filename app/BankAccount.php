<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BankAccount extends Model
{
    use SoftDeletes;
    public $timestamps = false;
    protected $fillable = ["bank_id","currency_id","name","surname","iban","status"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bank(){
        return $this->belongsTo(Bank::class,"bank_id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency(){
        return $this->belongsTo(Currency::class,"currency_id");
    }

}
