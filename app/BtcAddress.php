<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BtcAddress extends Model
{
    use SoftDeletes;

    protected $fillable = ["user_id","coin_id","wallet_id","address","status","password"];

    public function wallet(){
        return $this->belongsTo(Wallet::class,"wallet_id","id");
    }

}
