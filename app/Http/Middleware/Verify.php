<?php

namespace App\Http\Middleware;

use Closure;


class Verify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $segment1 = $request->segment(1);
        $segment2 = $request->segment(2);

        $user = auth()->user();
        if($user->user_type != 0){

            $canRedirect = false;
            if($segment1 != "personel"){
                if($segment2 != "verify"){
                    $canRedirect = true;
                }
            }

            if($canRedirect && $user->verified == 0) {
                return redirect()->route("verify-email");

            } /*elseif($canRedirect && $user->verified == 1) {
                return redirect()->route("verify-mobile");

            } elseif($canRedirect && $user->verified == 2) {
                return redirect()->route('verify-sms');

            }*/

        }

        return $next($request);
    }
}
