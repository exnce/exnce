<?php

namespace App\Http\Middleware;

use App\Personal;
use Closure;

class CheckUserGroup
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $user = \Auth::guard($guard);
        $segment1 = $request->segment(1);
        $segment2 = $request->segment(2);

        if(($segment1 == "admin" || $segment1 == "vendor") && $user->check() && $user->user()->user_type == 1){
            return redirect("personal");
        }elseif(($segment1 == "personal" || $segment1 == "vendor") && $user->check() && $user->user()->user_type == 0){
            return redirect("admin");
        }elseif(($segment1 == "personal" || $segment1 == "admin") && $user->check() && $user->user()->user_type == 2){
            return redirect("vendor");
        }

       /* if ($user->user() && !$user->user()->status){
            $user->logout();
            return redirect('personal/restricted-area');
        }
        */
        return $next($request);
    }
}
