<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Crypt;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Validation\Factory;
use App\User;

class ValidateAuthenticatorRequest extends FormRequest
{
    /**
     *
     * @var \App\User
     */
    private $user;
    /**
     * @var Factory
     */
    private $factory;

    /**
     * ValidateAuthenticatorRequest constructor.
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $factory->extend(
            'valid_token',
            function ($attribute, $value, $parameters, $validator) {
                $secret = Crypt::decrypt($this->user->google2fa_secret);

                $Google2FA = new Google2FA();
                return $Google2FA->verifyKey($secret, $value);
            },
            _i("Kod geçersiz")
        );

        $factory->extend(
            'used_token',
            function ($attribute, $value, $parameters, $validator) {
                $key = $this->user->id . ':' . $value;

                return !Cache::has($key);
            },
            _i("Bu kod daha önce kullanılmış")
        );

        $this->factory = $factory;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        try {
            $this->user = User::query()->findOrFail(
                session('2fa:user:id')
            );
        } catch (Exception $exc) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'totp' => 'bail|required|digits:6|valid_token|used_token',
        ];
    }
}
