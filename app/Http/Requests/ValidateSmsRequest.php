<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use Illuminate\Validation\Factory;
use App\User;
use App\Http\Controllers\TwoFactorController;

class ValidateSmsRequest extends FormRequest
{
    /**
     *
     * @var \App\User
     */
    private $user;
    /**
     * @var Factory
     */
    private $factory;

    /**
     * ValidateSmsRequest constructor.
     * @param Factory $factory
     */
    public function __construct(Factory $factory)
    {
        $factory->extend(
            'valid_sms',
            function ($attribute, $value, $parameters, $validator) {
                $last_sms = User::find($this->user->id)->lastSms(1); //Tek kullanimlik sifre tipi

                $timelimit = (new TwoFactorController())->timelimit;
                return ( $last_sms->content == $value && (time() - $last_sms->created_at->timestamp) < $timelimit);
            },
            _i("Geçersiz sms kodu")
        );

        $this->factory = $factory;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        try {
            $this->user = User::query()->findOrFail(
                session('2fa:user:id')
            );
        } catch (Exception $exc) {
            return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sms_code' => 'required|digits:6|valid_sms',
        ];
    }
}
