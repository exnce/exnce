<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/10/19
 * Time: 1:24 PM
 */

namespace App\Http\Controllers\Auth;


use App\UserMeta;
use Laravel\Socialite\Facades\Socialite;

class TwitterController
{

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function redirectToTwitter()

    {

        return Socialite::driver('twitter')->redirect();

    }


    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function handleTwitterCallback()

    {



        try {

            $user = auth()->user();
            if($user){
                $twitterUser = Socialite::driver('twitter')->user();

                $create["platform"] = "TWITTER";
                $create["user_id"]=$user->id;
                $create['platform_user_name'] = $twitterUser->getName();
                $create['platform_user_data'] = $twitterUser->nickname;
                $create['platform_user_id'] = $twitterUser->getId();
                $checkMeta = UserMeta::where("platform","TWITTER")->where("platform_user_id",$twitterUser->getId())->where("user_id",$user->id)->get()->first();
                if(!$checkMeta) {
                    $userMeta = UserMeta::create($create);
                }
                return redirect()->route('social-confirm');

            }else{
                return redirect("login");
            }
        } catch (Exception $e) {
            return redirect("login");
        }

    }


}