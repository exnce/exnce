<?php

namespace App\Http\Controllers\Auth;

use App\Jobs\CampainCallBack;
use App\Jobs\SendVerificationEmail;
use App\Rules\BlackListEmail;
use App\User;
use App\Http\Controllers\Controller;
use App\UserReference;
use App\UserSetting;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

use App\Rules\Captcha;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/personal';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator =  Validator::make($data, [

            'name' => 'required|string|max:255',
            'email' => ['required','string','email','max:255','unique:users',new BlackListEmail],
            'password' => 'required|string|min:6|confirmed',
            'agreement' => 'accepted',
            "g-recaptcha-response" => new Captcha(),

        ]);

        return $validator;

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $szPassword=bcrypt($data['password']);
        $privateKey=generatePrivateKey($szPassword);
        $publicKey=generatePublicKey($privateKey);
        $emailTokenKey=generatePrivateKey(base64_encode($data['email']));


        $campainCode = "";
        $callbackURL = "";
        if (array_key_exists('campain_code', $data)) {
            $campainCode=$data["campain_code"];
        }
        if (array_key_exists('callback_url', $data)) {
            $callbackURL = $data["callback_url"];
        }

        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $szPassword,
            'email_token' => $emailTokenKey,
            'security_photo' => \App\SecurityPhoto::inRandomOrder()->first()->id,
            'private_key' => $privateKey,
            'public_key' => $publicKey,
            'campain_code'=>$campainCode,
            'callback_url'=>$callbackURL,

            'status' => 0,
            'verified'=>0
        ]);

        UserSetting::create([
            'user_id' => $user->id,
        ]);

        $user->ref_code = $user->id + 10000;
        $user->verified=0;
        $user->status=0;
        $user->save();

        if (array_key_exists('reference_code', $data)) {
           $checkReferenceUser = User::where("ref_code", $data["reference_code"])->get()->first();
           if($checkReferenceUser){
               UserReference::create(["user_id"=>$user->id, "reference_id"=>$checkReferenceUser->id]);
           }
        }

        return $user;
    }

    /**
     * Handle a registration request for the application.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));

        dispatch(new SendVerificationEmail($user));
        dispatch(new CampainCallBack($user));
        return redirect()->route("verify-email");
    }

}
