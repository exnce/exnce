<?php


namespace App\Http\Controllers\Auth;


use App\User;

use App\Http\Controllers\Controller;

use App\UserMeta;
use Socialite;

use Exception;

use Auth;


class FacebookController extends Controller

{


    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function redirectToFacebook()

    {

        return Socialite::driver('facebook')->redirect();

    }


    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function handleFacebookCallback()

    {

        try {

            $user = auth()->user();
            if($user){
            $facebookUser = Socialite::driver('facebook')->user();
            $create["platform"] = "FACEBOOK";
            $create["user_id"]=$user->id;
            $create['platform_user_name'] = $facebookUser->getName();
            $create['platform_user_data'] = $facebookUser->getEmail();
            $create['platform_user_id'] = $facebookUser->getId();
            $checkMeta = UserMeta::where("platform","FACEBOOK")->where("platform_user_id",$facebookUser->getId())->where("user_id",$user->id)->get()->first();
            if(!$checkMeta) {
                $userMeta = UserMeta::create($create);
            }
            return redirect()->route('social-confirm');

            }else{
                return redirect("login");
            }
        } catch (Exception $e) {
            return redirect("login");
        }

    }

}