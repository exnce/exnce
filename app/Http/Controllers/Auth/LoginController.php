<?php

namespace App\Http\Controllers\Auth;

use App\SecurityPhoto;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ValidateAuthenticatorRequest;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/personal/ieo';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated(Request $request, $user)
    {
        $message=_i("%s isimli kullancı sisteme giriş yaptı!");
        $message=sprintf($message,$user->name);
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log($message);

        if($user->user_type == 0) // admin
        {
            return redirect("/admin");
        }

        //mobil dogrulamadan gecmediyse iki asamali sms dogrulama yapilmasin
        if ($user->verified == 3 && $user->two_factor == 1) {
            Auth::logout();

            $request->session()->put('2fa:user:id', $user->id);
            $request->session()->put('2fa:user:name', $user->name);
            $request->session()->put('2fa:user:security_photo', $user->security_photo);
            $request->session()->put('2fa:user:phone', $user->phone);

            return redirect('sms/validate');
        } else if ($user->two_factor == 2 && $user->google2fa_secret) {
            Auth::logout();

            $request->session()->put('2fa:user:id', $user->id);
            $request->session()->put('2fa:user:name', $user->name);
            $request->session()->put('2fa:user:security_photo', $user->security_photo);

            return redirect('authenticator/validate');
        }

        /*        if($user->verified < 3 && env("DISALLOW_LOGIN")==true) {
                    $this->guard()->logout();
                    $request->session()->invalidate();
                    return redirect("/");
                }*/

        return redirect()->intended($this->redirectTo);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function getValidateToken()
    {
        if (session('2fa:user:id')) {

            $userName = session('2fa:user:name');
            $securityPhoto = session('2fa:user:security_photo');

            $data = [
                "userName" => $userName,
                "image" => base64_encode( SecurityPhoto::find($securityPhoto)->first()->image),
            ];

            return view('2fa/authenticator-validate', $data);
        }

        return redirect('login');
    }

    /**
     * @param ValidateAuthenticatorRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postValidateToken(ValidateAuthenticatorRequest $request)
    {
        //get user id and create cache key
        $userId = $request->session()->pull('2fa:user:id');
        $key    = $userId . ':' . $request->totp;

        //use cache to store token to blacklist
        Cache::add($key, true, 4);

        //login and redirect user
        Auth::loginUsingId($userId);

        return redirect()->intended($this->redirectTo);
    }

    public function logout(Request $request)
    {
        $user = \Auth::user();
        $message=_i("%s isimli kullanıcı sistemden çıkış yaptı!");
        $message=sprintf($message,$user->name);
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log($message);

        $this->guard()->logout();
        $request->session()->invalidate();
        return redirect("/login");
    }

}
