<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 30.08.2018
 * Time: 13:58
 */

namespace App\Http\Controllers\Admin;


use App\Bank;
use App\BankAccount;
use App\Currency;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BankAccountController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {

        $currencyList = Currency::get();
        $bankList = Bank::get();
        $data = [
            "statusCodes" => getBankAccountStatusCodes(),
            "currencyList" => $currencyList,
            "bankList" => $bankList
        ];
        return view("admin.definations.bank-account", $data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getBankAccounts(){
        $bankAccountList = BankAccount::get();
        $datatable = DataTables::of($bankAccountList)
            ->editColumn("status", function($bankAccount){
                return BankAccountsStatusCode($bankAccount->status);
            })
            ->addColumn("bank_name", function($bankAccount){
                return ($bankAccount->bank->bank_name);
            })
            ->addColumn("currency_type", function($bankAccount){
                return ($bankAccount->currency->name);
            })
            ->addColumn("account_owner", function($bankAccount){
                return ($bankAccount->name . " " . $bankAccount->surname);
            });
        return $datatable->make(true);
    }


    /**
     * @param Request $request
     * @param $bankAccountId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBankAccountDetails(Request $request, $bankAccountId){
        $bankAccount = BankAccount::find($bankAccountId);
        $user = auth()->user();
        $currencyList = Currency::get();
        $bankList = Bank::get();
        $data = [
            "bankAccount"=>$bankAccount,
            "currencyList" => $currencyList,
            "bankList" => $bankList,
            "statusCodes"=>getBankAccountStatusCodes()];
        return view("admin.definations.bank-account-detail",$data);
    }


    public function addBankAccount(Request $request)
    {
        $user = auth()->user();

        $bank_name = $request->input("bank_name");
        $currency_type = $request->input("currency_type");
        $name = $request->input("name");
        $surname = $request->input("surname");
        $iban = $request->input("iban");
        $status = $request->input("status");
        $status = array_search($status, getBankAccountStatusCodes());

        $rules = [
            'bank_name' => 'required|string|min:5|max:255',
            'currency_type' => 'required|string|min:3|max:50',
            'name' => 'required|string|min:5|max:100',
            'surname' => 'required|string|min:5|max:255',
            'iban' => 'required|string|min:10|max:70',
        ];

        $validated = Validator::make($request->all(), $rules);
        if ($validated->fails()) {
            $is_success = false;
            $type = "error";
            $message = _i("Lütfen girmiş olduğunuz değerleri kontrol ediniz");
            $title = _i("Error!");
        } else {
            $bank = Bank::where("bank_name", $bank_name)->first();
            $currency = Currency::where("name", $currency_type)->first();
            $bankaccount = BankAccount::where("iban", $iban)->first();

            if (!$bank) {
                $is_success = false;
                $type = "error";
                $message = _i("Bu banka sistemde tanımlı değil!");
                $title = _i("Error!");
            }
            if (!$currency) {
                $is_success = false;
                $type = "error";
                $message = _i("Bu para birimi sistemde tanımlı değil!");
                $title = _i("Error!");
            }
            if ($bankaccount) {
                $is_success = false;
                $type = "error";
                $message = _i("Bu banka hesabı sistemde zaten tanımlı!");
                $title = _i("Error!");
            } else {
                $bankAccountData = [
                    "bank_id" => $bank->id,
                    "currency_id" => $currency->id,
                    "name" => $name,
                    "surname" => $surname,
                    "iban" => $iban,
                    "status" => $status
                ];
                $bankAccount = BankAccount::create($bankAccountData);
                $is_success = true;
                $type = "success";
                $message = _i("Banka hesabı ekleme işlemi başarı ile gerçekleşti.");
                $title = _i("Bilgi");
            }
        }
        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBankAccount(Request $request)
    {
        $user = auth()->user();

        $bank_account_id = $request->input("bank_account_id");
        $bank_name = $request->input("bank_name");
        $currency_type = $request->input("currency_type");
        $name = $request->input("name");
        $surname = $request->input("surname");
        $iban = $request->input("iban");
        $status = $request->input("status");
        $status = array_search($status, getBankAccountStatusCodes());

        $rules = [
            'bank_name' => 'required|string|min:5|max:255',
            'currency_type' => 'required|string|min:3|max:50',
            'name' => 'required|string|min:5|max:100',
            'surname' => 'required|string|min:5|max:255',
            'iban' => 'required|string|min:10|max:70',
        ];

        $validated = Validator::make($request->all(), $rules);
        if ($validated->fails()) {
            $is_success = false;
            $type = "error";
            $message = _i("Lütfen girmiş olduğunuz değerleri kontrol ediniz");
            $title = _i("Error!");
        } else {
            $bank = Bank::where("bank_name", $bank_name)->first();
            $currency = Currency::where("name", $currency_type)->first();

            $bankaccount = BankAccount::find($bank_account_id);

            if (!$bank) {
                $is_success = false;
                $type = "error";
                $message = _i("Bu banka sistemde tanımlı değil!");
                $title = _i("Error!");
            }
            if (!$currency) {
                $is_success = false;
                $type = "error";
                $message = _i("Bu para birimi sistemde tanımlı değil!");
                $title = _i("Error!");
            }
            if (!$bankaccount) {
                $is_success = false;
                $type = "error";
                $message = _i("Böyle bir banka hesabı sistemde tanımlı değil!");
                $title = _i("Error!");
            } else {
                    $bankaccount->bank_id = $bank->id;
                    $bankaccount->currency_id=$currency->id;
                    $bankaccount->name = $name;
                    $bankaccount->surname = $surname;
                    $bankaccount->iban = $iban;
                    $bankaccount->status = $status;
                    $bankaccount->save();

                $is_success = true;
                $type = "success";
                $message = _i("Banka hesabı güncelleme işlemi başarı ile gerçekleşti.");
                $title = _i("Bilgi");
            }
        }
        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json($data, 200);
    }

}