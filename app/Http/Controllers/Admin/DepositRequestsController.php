<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/16/18
 * Time: 1:34 PM
 */

namespace App\Http\Controllers\Admin;


use App\Coin;
use App\CoinDeposit;
use App\Currency;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\Wallet;
use App\WalletAddress;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class DepositRequestsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){



        return view("admin.orders.deposit-requests");
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getDepositOrders(){
        $depositOrderList = CoinDeposit::withTrashed();

        $datatable = DataTables::of($depositOrderList)
            ->addColumn("coin", function($deposit){
               return  $deposit->coin->name;
            })
            ->addColumn("txid", function($deposit){
                return "<a target='_blank' href='".sprintf($deposit->coin->explorer,$deposit->txid)."'>".$deposit->txid."</a>";
            })
            ->addColumn("user", function($deposit){
                if(($deposit->memo_tag != "-") && ($deposit->memo_tag != "AIRDROP")){
                    $walletId = -1;
                    if(is_numeric($deposit->memo_tag))  {
                        $walletId = floatval( $deposit->memo_tag)-10000000000;
                    }else{
                        $walletId = -1;
                    }
                    $walletId = floatval( $deposit->memo_tag)-10000000000;
                    if($walletId < 0){$walletId = -1;}
                    $wallet = WalletAddress::where("id",$walletId)->where("coin_id",$deposit->coin->id)->withTrashed()->get()->first();

                    if(!$wallet){
                        $exnceAccount = User::where("email","arenersan@exnce.com")->get()->first();
                        $exnceAccountWallet = $exnceAccount->wallets()->where("coin_id",$deposit->coin->id)->get()->first();
                        $wallet = WalletAddress::where("id",$exnceAccountWallet->id)->withTrashed()->get()->first();
                    }

                }else{
                    $wallet = $deposit->wallet()->withTrashed()->get()->first();
                }

                return "<a target='_blank' href='".route("user-detail", $wallet->user()->get()->first()->id)."'>".$wallet->user()->get()->first()->name."</a>";

            })
            ->addColumn("amount", function($deposit){
               return $deposit->amount;
            })
            ->addColumn("can_confirm",function ($deposit){

                if(($deposit->memo_tag != "-") && ($deposit->memo_tag != "AIRDROP")){
                    $walletId = floatval( $deposit->memo_tag)-10000000000;
                    if($walletId < 0){$walletId = -1;}
                    $wallet = WalletAddress::where("id",$walletId)->where("coin_id",$deposit->coin->id)->withTrashed()->get()->first();

                    if(!$wallet){
                        $exnceAccount = User::where("email","arenersan@exnce.com")->get()->first();
                        $exnceAccountWallet = $exnceAccount->wallets()->where("coin_id",$deposit->coin->id)->get()->first();
                        $wallet = WalletAddress::where("id",$exnceAccountWallet->id)->withTrashed()->get()->first();
                    }

                }else{
                    $wallet = $deposit->wallet;
                }

                if($deposit->coin_id != $wallet->coin->id){
                    return false;
                }else{
                    return true;
                }
            })
            ->editColumn("status", function($deposit){

                if(($deposit->memo_tag != "-") && ($deposit->memo_tag != "AIRDROP")){
                    $walletId = floatval( $deposit->memo_tag)-10000000000;
                    if($walletId < 0){$walletId = -1;}
                    $wallet = WalletAddress::where("id",$walletId)->where("coin_id",$deposit->coin->id)->withTrashed()->get()->first();

                    if(!$wallet){
                        $exnceAccount = User::where("email","arenersan@exnce.com")->get()->first();
                        $exnceAccountWallet = $exnceAccount->wallets()->where("coin_id",$deposit->coin->id)->get()->first();
                        $wallet = WalletAddress::where("id",$exnceAccountWallet->id)->withTrashed()->get()->first();
                    }

                }else{
                    $wallet = $deposit->wallet;
                }

                if($deposit->coin_id != $wallet->coin->id){
                    $deposit->status = 6;
                }
                return DepositStatusCode($deposit->status);
            })->escapeColumns([]);
        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @param $withdrawId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails(Request $request, $depositId){
        $deposit = CoinDeposit::find($depositId);
        $user = auth()->user();


        if(($deposit->coin->symbol == "XLM") || ($deposit->coin->symbol == "XRP")){
            $walletId = -1;
            $memoTag = floatval($deposit->memo_tag)-10000000000;
            if($memoTag>0){
                $walletId = $memoTag;
            }
            $wallet = WalletAddress::where("id",$walletId)->withTrashed()->get()->first();

            if(!$wallet){
                $exnceAccount = User::where("email","arenersan@exnce.com")->get()->first();
                $exnceAccountWallet = $exnceAccount->wallets()->where("coin_id",$deposit->coin->id)->get()->first();
                $wallet = WalletAddress::where("id",$exnceAccountWallet->id)->withTrashed()->get()->first();

            }


        }else {
            $wallet =  $deposit->wallet()->withTrashed()->get()->first();
        }


        $ownerUser =  $wallet->user()->get()->first();
        $balance = 0.00;



        $data = [
                 "walletBalance"=>0.00,
                 "owner"=>$ownerUser,
                 "deposit"=>$deposit];


            return view("admin.orders.deposit-crypto-requests-detail",$data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function approveDeposit(Request $request){
        $orderId = $request->input("order_id");

        $description = $request->input("description");
        $user = auth()->user();

        $deposit = CoinDeposit::find($orderId);

        if($deposit){
            if($deposit->status == 1){
                $is_success = false;
                $type = "warning";
                $title = _i("Error!");
                $message = _i("Zaten onaylanmış bir para yatırma talebini tekrar onaylayamazsiniz!");
            }else{

                $depositOwner = null;
                if(($deposit->coin->symbol == "XLM") || ($deposit->coin->symbol == "XRP")){
                    $walletId = -1;
                    $memoTag = floatval($deposit->memo_tag)-10000000000;
                    if($memoTag>0){
                        $walletId = $memoTag;
                    }
                    $wallet = WalletAddress::where("id",$walletId)->get()->first();

                    if(!$wallet){
                        $exnceAccount = User::where("email","arenersan@exnce.com")->get()->first();
                        $exnceAccountWallet = $exnceAccount->wallets()->where("coin_id",$deposit->coin->id)->get()->first();
                        $wallet = WalletAddress::where("id",$exnceAccountWallet->id)->withTrashed()->get()->first();
                    }

                }else {
                    $wallet =  $deposit->wallet()->get()->first();
                }

                $depositOwner = null;

                if($wallet){
                  $depositOwner = $wallet->user()->get()->first();
                }


                if($depositOwner){

                    $depositWallet = $wallet->wallet()->get()->first();

                    $depositWallet->balance += $deposit->amount;
                    $depositWallet->save();
                    $deposit->status = 1;
                    $deposit->save();

                    $is_success = true;
                    $type = "info";
                    $title = _i("İşlem Başarılı");
                    $message = _i("Para yatırma işlemi başarı ile gerçekleşti.");

                }else{
                    $is_success = false;
                    $type = "error";
                    $title = _i("İşlem Başarısız");
                    $message = _i("Kullanıcının böyle bir para yatırma talebi bulunamadı!");
                }
            }

        }else{
            $is_success = false;
            $type = "error";
            $title = _i("İşlem Başarısız");
            $message = _i("Böyle bir para yatırma talebi bulunamadı!");
        }
        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelDeposit(Request $request){
        $orderId = $request->input("order_id");
        $description = $request->input("description");
        $user = auth()->user();

        $order = Order::find($orderId);
        if($order){
            if($order->status==5){
                $tryWallet  =  Wallet::with(["currency" => function ($query){
                    return $query->select(["id","name","symbol","slug"])->where("symbol","TRY");
                }])
                    ->where("type", true)
                    ->where("user_id",$order->user_id)
                    ->get()->first();
                $tryWallet->balance = $tryWallet->balance - $order->amount;
                $tryWallet->save();
            }
            $order->status = 4;
            $order->description = $description;
            $order->operator_id = $user->id;
            $order->save();
            $is_success = true;
            $type = "info";
            $title = _i("İşlem Başarılı");
            $message = _i("Para yatırım talebi iptal işlemi başarı ile gerçekleşti.");
        }else{
            $is_success = false;
            $type = "error";
            $title = _i("İşlem Başarısız");
            $message = _i("Böyle bir para yatırma talebi bulunamadı!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

}