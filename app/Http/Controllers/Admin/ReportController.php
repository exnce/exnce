<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/29/19
 * Time: 7:33 AM
 */

namespace App\Http\Controllers\Admin;


use App\Coin;
use App\CoinDeposit;
use App\Order;
use App\TransactionDetail;
use App\Wallet;
use Illuminate\Http\Request;

class ReportController
{
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        if ($user) {
            return view("admin.reports.index");
        } else {
            return redirect()->route("login");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function balanceReports(Request $request)
    {
        $user = auth()->user();
        if ($user) {
            $coinList = Coin::get();
            foreach ($coinList as &$coin) {
                $totalBalance = Wallet::where("coin_id", $coin->id)->sum('balance');
                $totalFee = Order::where("type", "withdraw")->where("coin_id", $coin->id)->where("status",5)->sum('fee');
                $totalWithdraw = Order::where("type", "withdraw")->where("coin_id", $coin->id)->where("status",5)->sum('amount');
                $totalDeposit= CoinDeposit::where("coin_id", $coin->id)->sum('amount');
                $totalTransactionBuyFee = TransactionDetail::where("type", "buy")->where("currency_id", $coin->id)->sum('fee');
                $totalTransactionSellFee = TransactionDetail::where("type", "sell")->where("coin_id", $coin->id)->sum('fee');



                $coin->total_balance = $totalBalance;
                $coin->total_fee = $totalFee;
                $coin->total_withdraw = $totalWithdraw;
                $coin->total_deposit = $totalDeposit;
                $coin->total_buy_fee = $totalTransactionBuyFee;
                $coin->total_sell_fee = $totalTransactionBuyFee;


                $coin->cold_storage = $totalBalance - $totalFee - $totalTransactionBuyFee - $totalTransactionSellFee;
            }
            $data = ["coins" => $coinList];
            return view("admin.reports.balance", $data);
        }else{
            return redirect()->route("login");
        }
    }
}