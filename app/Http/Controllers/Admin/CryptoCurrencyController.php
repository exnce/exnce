<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 29.08.2018
 * Time: 22:29
 */

namespace App\Http\Controllers\Admin;


use App\Coin;
use App\Http\Controllers\Controller;
use Illuminate\View\View;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;


class CryptoCurrencyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(){

        $data = ["statusCodes"=>getCryptoCurrencyStatusCodes()];
        return view("admin.definations.cryptocurrency",$data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getCoins(){
        $coinList = Coin::get();
        $datatable = DataTables::of($coinList)
            ->editColumn("status", function($coin){
                return CryptocurrencyStatusCode($coin->status);
            })
            ->addColumn("min_deposit", function($coin){
                return $coin->min_deposit;
            })
        ->addColumn("min_withdraw", function($coin){
            return $coin->min_withdraw;
        })
            ->addColumn("fee", function($coin){
                return $coin->fee;
            })
        ->addColumn("processing_fee", function($coin){
            return $coin->processing_fee;
        })
            ->addColumn("id", function($coin){
                return $coin->id;
            });
        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @param $coinId
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function getCoinDetails(Request $request, $coinId){
        $coin = Coin::find($coinId);
        $user = auth()->user();
        $data = ["coin"=>$coin,"statusCodes"=>getCryptoCurrencyStatusCodes()];
        return view("admin.definations.cryptocurrency-detail",$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCoin(Request $request){
        $user = auth()->user();
        $coinId = $request->input("coin_id");
        $name = $request->input("name");
        $slug = $request->input("slug");
        $symbol = $request->input("symbol");
        $decimals = $request->input("decimals");
        $status = $request->input("status");
        $fee = $request->input("fee");
        $min_deposit = $request->input("min_deposit");
        $min_withdraw = $request->input("min_withdraw");
        $processing_fee = $request->input("processing_fee");
        $display_index = $request->input("display_index");

        $coin = Coin::find($coinId);
        if($coin){
            $status = array_search($status, getCryptoCurrencyStatusCodes());
            $coin->name=$name;
            $coin->slug=$slug;
            $coin->decimals=$decimals;
            $coin->symbol=$symbol;
            $coin->status=$status;
            $coin->min_deposit = $min_deposit;
            $coin->min_withdraw = $min_withdraw;
            $coin->processing_fee = $processing_fee;
            $coin->fee = $fee;
            $coin->display_index = $display_index;
            $coin->save();

            $is_success = true;
            $type="success";
            $message = _i("Kripto para güncelleme işlemi başarı ile gerçekleşti.");
            $title = _i("Bilgi");
        }else{
            $is_success = false;
            $type="error";
            $message = _i("Sisteme tanımlı böyle bir kripto para mevcut değildir!");
            $title = _i("Error!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCoin(Request $request){
        $user = auth()->user();
        $name = $request->input("name");
        $slug = $request->input("slug");
        $symbol = $request->input("symbol");
        $decimals = $request->input("decimals");
        $status = $request->input("status");
        $status = array_search($status, getCryptoCurrencyStatusCodes());
        $coin = Coin::where("name",$name)->where("slug",$slug)->where("symbol",$symbol)->first();

        if($coin){
            $is_success = false;
            $type="error";
            $message = _i("Bu kripto para zaten sistemde ekli!");
            $title = _i("Error!");
        }else {
            $coinData = ["name" => $name,
                "slug" => $slug,
                "decimals" => $decimals,
                "symbol" => $symbol,
                "status" => $status
            ];
            $coin = Coin::create($coinData);
                $is_success = true;
                $type="success";
                $message = _i("Kripto para ekleme işlemi başarı ile gerçekleşti.");
                $title = _i("Bilgi");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

}