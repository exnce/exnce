<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/15/19
 * Time: 10:46 PM
 */

namespace App\Http\Controllers\Admin;


use App\BlockTrack;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Request;

class BlockScannerController extends Controller
{

    public function index(){

        $count = BlockTrack::get()->last()->id;
        $deleteMaxId = $count-30;
        $deletedRows = BlockTrack::where('id','<',$deleteMaxId)->delete();
        $blockList = BlockTrack::orderBy("blockNumber","desc")->get();
        $data=["blocks"=>$blockList];
        return view("admin.server.block-status", $data);


    }


    public function addBlock(Request $request,$blockId){



        $count = BlockTrack::get()->last()->id;
        $deleteMaxId = $count+2;
        $deletedRows = BlockTrack::where('id','<',$deleteMaxId)->delete();

        $simpleBlock = ["network_type"=>"ERC20","blockNumber"=>$blockId,Carbon::now()];
        BlockTrack::create($simpleBlock);


        $blockList = BlockTrack::orderBy("blockNumber","desc")->get();
        $data=["blocks"=>$blockList];
        return view("admin.server.block-status", $data);


    }

}