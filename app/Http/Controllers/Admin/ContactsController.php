<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/28/19
 * Time: 7:57 PM
 */

namespace App\Http\Controllers\Admin;


use App\Contact;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class ContactsController extends Controller
{
    public function index(Request $request){
        return view("admin.contacts.index");
    }

    public function getContacts(){
        $contacts = Contact::where("status",0);
        $datatable = DataTables::of($contacts)
            ->addColumn("date", function($contact){
            return $contact->created_at;
        });
        return $datatable->make(true);
    }

}