<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/16/18
 * Time: 1:47 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Ticket;
use App\TicketCategory;
use App\TicketComment;
use App\TicketPriority;
use App\TicketStatus;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class SupportRequestsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $user = auth()->user();


        if($user){
            $unsolved = 0;
            $solved = 0;
            $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
            $solved = Ticket::where("status_id", $solvedStatusId)->count();
            $unsolved = Ticket::where("status_id","<>", $solvedStatusId)->count();

            $data = [
                "user"=>$user,
                "solved" => $solved,
                "unsolved" => $unsolved
            ];

            return view("admin.support.index",$data);
        }else{
            return redirect()->route("login");
        }
    }

    public function loadSupportRequests(Request $request){
        $user = auth()->user();
        $isCompleted = $request->post("is_completed");

        if($user){

            $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
            if($isCompleted == true) {
                $operator = "=";
            }else{
                $operator = "<>";
            }

            $tickets = Ticket::where("status_id",$operator,$solvedStatusId);
            $datatable = DataTables::of($tickets)
                ->addColumn("agent", function($ticket){
                    $agentName = "-";
                    if($ticket->agent){
                        $agentName = $ticket->agent->name;
                    }
                    return $agentName;
                })
                ->addColumn("date", function($ticket){
                    return  $ticket->updated_at;
                })
                ->addColumn("user", function($ticket){
                    return  $ticket->user->name;
                })
                ->addColumn("userdetail", function($ticket){
                    return  route("user-detail",$ticket->user->id);
                })
                ->addColumn("color", function ($ticket){
                    return  $ticket->status->color;
                })
                ->addColumn("status", function($ticket){
                    return  _i($ticket->status->name);
                })
                ->addColumn("detail",function ($ticket){
                    return route("admin-support-detail",$ticket->id);
                });
            return $datatable->make(true);
        }else{
            return redirect()->route("login");
        }
    }


    public function ticketDetail($ticketId = null){
        $user = auth()->user();
        if($user){
            if($ticketId){
                $ticket = Ticket::where("id",$ticketId)->get()->first();

                if($ticket){

                    $unsolved = 0;
                    $solved = 0;
                    $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
                    $solved = Ticket::where("status_id", $solvedStatusId)->count();
                    $unsolved =  Ticket::where("status_id","<>", $solvedStatusId)->count();

                    $data = [
                        "priorities"=>getSupportPriority(),
                        "categories"=>getSupportCategories(),
                        "statuslist"=>getSupportStatus(),
                        "user"=>$user,
                        "solved" => $solved,
                        "unsolved" => $unsolved,
                        "ticket" => $ticket
                    ];

                    return view("admin.support.detail", $data);
                }else{
                    $message = _i("Böyle bir destek talebi bulunamadı!");
                    return redirect()->back()->with('message', $message);
                }
            }
        }
        else{
            return redirect()->route("login");
        }

    }


    public function sendComment(Request $request, $ticketId = null){
        $user = auth()->user();

        if($user){
            if($ticketId){
                $ticket = Ticket::where("id",$ticketId)->get()->first();
                if($ticket){

                    $statusId = TicketStatus::where("key",$request->input("status"))->get()->first()->id;
                    $priorityId = TicketPriority::where("key",$request->input("priority"))->get()->first()->id;
                    $categoryId = TicketCategory::where("key",$request->input("category"))->get()->first()->id;
                    $agentId = -1;

                    $ticket->status_id = $statusId;
                    $ticket->priority_id = $priorityId;
                    $ticket->agent_id = $agentId;
                    $ticket->category_id = $categoryId;
                    $ticket->save();

                    $ticketComment = new TicketComment();
                    $ticketComment->content = $request->input("comment");
                    $ticketComment->user_id = $user->id;
                    $ticketComment->ticket_id = $ticket->id;
                    $ticketComment->save();
                    return redirect()->back();
                }else{
                    $message = _i("Böyle bir destek talebi bulunamadı!");
                    return redirect()->back()->with('message', $message);
                }
            }
        }
        else{
            return redirect()->route("login");
        }

    }
}