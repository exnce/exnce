<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 30.08.2018
 * Time: 13:58
 */

namespace App\Http\Controllers\Admin;


use App\Bank;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class BankController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $data = ["statusCodes"=>getBankStatusCodes()];
        return view("admin.definations.bank",$data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getBanks(){
        $bankList = Bank::get()->where("status","<>","2");
        $datatable = DataTables::of($bankList)
            ->editColumn("status", function($bank){
                return BanksStatusCode($bank->status);
            })
            ->addColumn("id", function($bank){
                return ($bank->id);
            });
        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @param $bankId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBankDetails(Request $request, $bankId){
        $bank = Bank::find($bankId);
        $user = auth()->user();
        $data = ["bank"=>$bank,"statusCodes"=>getBankStatusCodes()];
        return view("admin.definations.bank-detail",$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBank(Request $request){
        $user = auth()->user();
        $bankId = $request->input("bank_id");
        $name = $request->input("bank_name");
        $status = $request->input("status");

        $bank = Bank::find($bankId);
        if($bank){
            $status = array_search($status, getBankStatusCodes());
            $bank->bank_name=$name;
            $bank->status=$status;
            $bank->save();

            $is_success = true;
            $type="success";
            $message = _i("Banka güncelleme işlemi başarı ile gerçekleşti.");
            $title = _i("Bilgi");
        }else{
            $is_success = false;
            $type="error";
            $message = _i("Sisteme tanımlı böyle bir banka mevcut değildir!");
            $title = _i("Error!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addBank(Request $request){
        $user = auth()->user();
        $name = $request->input("bank_name");
        $status = $request->input("status");

        $status = array_search($status, getBankStatusCodes());

        $bank = Bank::where("bank_name",$name)->where("status","<>","2")->first();

        if($bank){
            $is_success = false;
            $type="error";
            $message = _i("Bu banka zaten sistemde ekli!");
            $title = _i("Error!");
        }else {
            $bankData = ["bank_name" => $name,
                "status" => $status
            ];
            $bank = Bank::create($bankData);
            $is_success = true;
            $type="success";
            $message = _i("Banka ekleme işlemi başarı ile gerçekleşti.");
            $title = _i("Bilgi");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }
}