<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 02:31
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Merchant;
use App\User;
use App\Order;
use App\IdentityApproval;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function index(){
        $memberCount = User::count();
        $merchantCount = Merchant::count();
        $depositCount = Order::where("status","<>",10)->where("type","deposit")->count();
        $withdrawCount = Order::with("user_bank")->with("coin")->where("status","<>",10)->where("type","withdraw")->count();
        $identitiyVerifiyCount = IdentityApproval::where("status",0)->count();




        $supportCount = 0;

        $data = [
                "memberCount"=>$memberCount,
                "merchantCount"=>$merchantCount,
                "supportCount"=>$supportCount,
                "identityVerifyCount"=>$identitiyVerifiyCount,
                "depositCount"=>$depositCount,
                "withdrawCount"=>$withdrawCount];
        return view("admin.dashboard.index",$data);
    }

}