<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/2/19
 * Time: 3:56 AM
 */

namespace App\Http\Controllers\Admin;


use App\Airdrop;
use App\CoinDeposit;
use App\Http\Controllers\Controller;
use App\Order;
use App\User;
use App\UserBank;
use Illuminate\Http\Request;

class AirdropController extends Controller
{
    public function index(Request $request){

        if(auth()->user()){
            $airdrops = Airdrop::get();
            $data = ["airdrops"=>$airdrops];
            return view("admin.airdrop.index",$data);
        }

    }

    public function details(Request $request, $airdropId = null){

        if(auth()->user()){

            if($airdropId != null){
                $airdrop = Airdrop::where("id",$airdropId)->get()->first();

                if($airdrop){
                    $followers = $airdrop->followers()->get();
                    $data = ["users"=>$followers,
                             "airdrop"=>$airdrop];
                    return view("admin.airdrop.detail",$data);
                }else{
                    // invalid airdrop
                }
            }else{
                // invalid airdrop
            }
        }else{
            //invalid user
        }
    }

    public function distribute(Request $request, $airdropId){

    }

    public function store(Request $request, $airdropId = null){

        if(auth()->user()){
            $user = auth()->user();
            if($airdropId != null){
                $airdrop = Airdrop::where("id",$airdropId)->get()->first();
                if($airdrop){

                    $userId = $request->input("userId");
                    $amount = $request->input("amount");



                    $airdropUser = User::where("id",$userId)->get()->first();
                    if($airdropUser){

                        $wallet=$airdropUser->wallets()->where("type", false)->where("coin_id", $airdrop->coin_id)->get()->first();
                        if($wallet){
                            $walletAddress = $wallet->address()->get()->last();
                            if($walletAddress){
                                $payment_address = $walletAddress->address;


                                $checkUserBank = $airdrop->owner->user_banks()
                                    ->where("amount",$amount)
                                    ->where("surname","AIRDROP")
                                    ->where("currency_type",$wallet->type)
                                    ->where("currency_id",$airdrop->coin->id)
                                    ->where("payment_address",$payment_address)->get()->first();

                                if(!$checkUserBank) {
                                    $userBank = [
                                        "user_id" => $airdrop->owner->id,
                                        "bank_id" => -1,
                                        "currency_type"=>$wallet->type,
                                        "currency_id"=>$airdrop->coin->id,
                                        "iban_number"=>"-",
                                        "name"=>$airdropUser->name,
                                        "surname"=>"AIRDROP",
                                        "amount"=>$amount,
                                        "payment_address" => $payment_address,
                                        "destination_tag" => "AIRDROP"
                                    ];
                                    $checkUserBank = UserBank::create($userBank);

                                    $orderArray = ["user_id" => $airdrop->owner->id,
                                        "type" => "withdraw",
                                        "currency_type"=>false,
                                        "currency_id"=>-1,
                                        "coin_id"=>$airdrop->coin_id,
                                        "amount" => $amount,
                                        "fee" => 0.00,
                                        "bank_id" => $checkUserBank->id,
                                        "transfer_code" => generateTransactionCode(),
                                        "description" => $airdrop->name,
                                        "txId"=> $airdrop->name,
                                        "status"=>5
                                    ];
                                    $order = Order::create($orderArray);

                                    $ownerWallet = $airdrop->owner->wallets()->where("type", false)->where("coin_id", $airdrop->coin_id)->get()->first();
                                    $ownerWallet->balance = $ownerWallet->balance - $amount;
                                    $ownerWallet->save();

                                    $wallet->balance = $wallet->balance + $amount;
                                    $wallet->save();



                                    $depositRequest = ["blockNumber" => $airdrop->id,
                                        "txid" => "EXNCEAIRDROP_".$airdrop->name. "-". $airdropUser->name . "-".$order->id,
                                        "coin_id"=>$airdrop->coin_id,
                                        "address"=>$payment_address,
                                        "memo_tag"=>"AIRDROP",
                                        "amount" => $amount,
                                        "status"=>7
                                    ];

                                    CoinDeposit::create($depositRequest);


                                }



                            }else{
                                $payment_address = "-";
                            }
                        }else{
                            $payment_address = "-";
                        }



                    }else{
                        // invalid airdrop user!
                    }
                }else{
                    //invalid airdrop
                }
            }else{
                //invalid airdrop
            }
        }else{
            // invalid user
        }


        return redirect()->route('airdrop-distribution');
    }




}