<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 29.08.2018
 * Time: 22:32
 */

namespace App\Http\Controllers\Admin;


use App\Currency;
use App\Http\Controllers\Controller;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class FiatCurrencyController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $data = ["statusCodes"=>getFiatCurrencyStatusCodes()];
        return view("admin.definations.fiatcurrency",$data);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function getCurrencies(){
        $currencyList = Currency::get();
        $datatable = DataTables::of($currencyList)
            ->editColumn("status", function($coin){
                return FiatcurrencyStatusCode($coin->status);
            })
            ->addColumn("id", function($coin){
                return $coin->id;
            });
        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @param $currencyId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCurrencyDetails(Request $request, $currencyId){
        $currency = Currency::find($currencyId);
        $user = auth()->user();
        $data = ["currency"=>$currency,"statusCodes"=>getFiatCurrencyStatusCodes()];
        return view("admin.definations.fiatcurrency-detail",$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCurrency(Request $request){
        $user = auth()->user();
        $currencyId = $request->input("currency_id");
        $name = $request->input("name");
        $slug = $request->input("slug");
        $symbol = $request->input("symbol");
        $decimals = $request->input("decimals");
        $status = $request->input("status");

        $currency = Currency::find($currencyId);
        if($currency){
            $status = array_search($status, getFiatCurrencyStatusCodes());
            $currency->name=$name;
            $currency->slug=$slug;
            $currency->decimals=$decimals;
            $currency->symbol=$symbol;
            $currency->status=$status;
            $currency->save();

            $is_success = true;
            $type="success";
            $message = _i("Para birimi güncelleme işlemi başarı ile gerçekleşti.");
            $title = _i("Bilgi");
        }else{
            $is_success = false;
            $type="error";
            $message = _i("Sisteme tanımlı böyle bir para birimi mevcut değildir!");
            $title = _i("Error!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addCurrency(Request $request){
        $user = auth()->user();
        $name = $request->input("name");
        $slug = $request->input("slug");
        $symbol = $request->input("symbol");
        $decimals = $request->input("decimals");
        $status = $request->input("status");
        $status = array_search($status, getFiatCurrencyStatusCodes());
        $currency = Currency::where("name",$name)->where("slug",$slug)->where("symbol",$symbol)->first();

        if($currency){
            $is_success = false;
            $type="error";
            $message = _i("Bu para birimi zaten sistemde ekli!");
            $title = _i("Error!");
        }else {
            $currencyData = ["name" => $name,
                "slug" => $slug,
                "decimals" => $decimals,
                "symbol" => $symbol,
                "status" => $status
            ];
            $currency = Currency::create($currencyData);
            $is_success = true;
            $type="success";
            $message = _i("Para birimi ekleme işlemi başarı ile gerçekleşti.");
            $title = _i("Bilgi");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }
}