<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/16/18
 * Time: 1:34 PM
 */

namespace App\Http\Controllers\Admin;


use App\Coin;
use App\Currency;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Personal\WalletController;
use App\Order;
use App\User;
use App\Wallet;
use App\WalletAddress;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class WithdrawRequestsController extends Controller
{
    public function index()
    {


        return view("admin.orders.withdraw-requests");
    }

    public function getWithdrawOrders(Request $request){
        $withdrawOrderList = Order::with("user_bank")->with("coin")->where("status","<>",10)->where("type","withdraw")->withTrashed();

        $datatable = DataTables::of($withdrawOrderList)
            ->addColumn("name", function($order){
                return "<a target='_target' href='".route("user-detail",$order->user->id)."'>".$order->user->name."</a>";

            })
            ->addColumn("bank_name", function($order){
                if($order->currency_type == true){
                    return  $order->user_bank->bank->bank_name;
                }else{
                    return $order->coin->name;

                }
            })
            ->addColumn("operator", function($order){
                $operator="-";
                if($order->operator){
                    $operator = $order->operator->name;
                }
                return $operator;
            })
            ->editColumn("status", function($order){
                return WithdrawAdminStatusCode($order->status);
            })
            ->editColumn("amount", function($order){
                return number_format($order->amount, 8, ",", ".");
            })
            ->escapeColumns([]);

        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @param $withdrawId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails(Request $request, $withdrawId){
        $order = Order::where("id",$withdrawId)->withTrashed()->get()->first();
        $user = auth()->user();
        $balance = 0.00;

        $orderOwner = User::where("id",$order->user_id)->get()->first();


        $currency=null;
        if($order->currency_type == true) {
            $currency = Currency::find($order->currency_id);
        }else{
            $currency = Coin::where("id",$order->coin_id)->withTrashed()->get()->first();
        }


        $balance = $orderOwner->user_balance(false,$currency->id,$order->currency_type,false,$orderOwner);

        $receiverAddress = $order->user_bank->payment_address;

        $walletIsRegistered = WalletAddress::where("address", $receiverAddress)->withTrashed()->get()->first();

        if($walletIsRegistered) {
            $walletOwner = $walletIsRegistered->user;

            $receiverInformation = [
                "id" => $walletOwner->id,
                "name" => $walletOwner->name,
            ];
        }

        $userPersonalWallet = $orderOwner->wallets()->where("type", false)->where("coin_id", $currency->id)->withTrashed()->get()->first();
        $userPersonalWallet = $userPersonalWallet->address()->get()->last();


        //$order->amount = number_format($order->amount, 8, ",", ".");
        $data = [
            "description" => $userPersonalWallet,
            "withdrawOrder"=>$order,
            "walletBalance" => $balance, //number_format($balance, 8, ",", ".")
            "receiver" => $walletIsRegistered ? $receiverInformation : false,
        ];
        //todo: kendine gondermesini engelle

        if($order->currency_type == true) {
            return view("admin.orders.withdraw-requests-detail",$data);

        }else
        {
            return view("admin.orders.withdraw-crypto-requests-detail",$data);
        }
    }

    public function approveWithdraw(Request $request){
        $orderId = $request->input("order_id");
        $description = $request->input("description");
        $txId = $request->input("txId");
        $user = auth()->user();

        $order = Order::where("id",$orderId)->withTrashed()->get()->first();

        if($order){

            $currency=null;
            if($order->currency_type == true) {
                $currency = Currency::find($order->currency_id);
            }else{
                $currency = Coin::where("id",$order->coin_id)->withTrashed()->get()->first();
            }

            $wallet = Wallet::with(["currency" => function ($query) use($currency){
                return $query->select(["id","name","symbol","slug"])->where("symbol",$currency->symbol);
            }])
                ->where("type", $order->currency_type)
                ->where("user_id",$order->user_id)
                ->where("coin_id",$order->coin_id)
                ->withTrashed()
                ->get()->first();

            if($wallet->balance < $order->amount){
                $is_success = false;
                $type = "error";
                $title = _i("Error!");
                $message = _i("Müşterinin bakiyesi yetersizdir.");

                if($order->description != $description){
                    $order->description=$description;
                }
                if($order->txId != $txId){
                    if($txId != ""){
                        $order->description=$description;
                        $order->txId = $txId;
                    }
                }
                $order->save();
            }
            else if($order->status==5){

                if($order->description != $description){
                    $order->description=$description;
                }
                if($order->txId != $txId){
                    if($txId != ""){
                        $order->txId = $txId;
                    }
                }
                $order->save();
                $is_success = false;
                $type = "warning";
                $title = _i("Error!");
                $message = _i("Zaten onaylanmış bir para çekim talebini tekrar onaylayamazsınız!");
            }else{
                if($order->currency_type == false) {
                    $cryptoWallet = Wallet::where("id", $wallet->id)->withTrashed()->get()->first();
                    $receiver = $order->user_bank->payment_address;
                    $amount = $order->amount;
                    $fee = $order->fee;

                    $result = WalletController::sendTransaction($currency, $receiver, $amount, $fee);

                    activity("approveWithdraw")
                        ->performedOn($cryptoWallet)
                        ->withProperties($order)
                        ->log(sprintf("%s %d nolu çekim talebini onayladı.", $user->name, $order->id));

                    if($result["success"]===true){
                        $order->status = 5;
                        $order->description = $description;
                        $order->operator_id = $user->id;
                        $order->txId = $result["response"];
                        $order->save();

                        if($fee) {
                            $wallet->balance = $wallet->balance - $order->amount - $order->fee;
                        } else {
                            $wallet->balance = $wallet->balance - $order->amount;
                        }
                        $wallet->save();
                        $is_success = true;
                        $type = "info";
                        $title = _i("İşlem Başarılı");
                        $message = _i("Kripto para gonderimi başarı ile gerçekleşti.");
                    } else if($result["success"]===false) {
                        $is_success = $result["success"];
                        $type = "error";
                        $title = $result["message"];
                        $message = $result["response"];
                    } else {
                        $is_success = false;
                        $type = "error";
                        $title = _i("Transfer yapılamıyor");
                        $message = _i("Cüzdan sunucusuna bağlanılamadı!");
                    }
                }
            }
        }else{
            $is_success = false;
            $type = "error";
            $title = _i("İşlem Başarısız");
            $message = _i("Böyle bir para çekim talebi bulunamadı!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    public function cancelWithdraw(Request $request){
        $orderId = $request->input("order_id");
        $description = $request->input("description");
        $user = auth()->user();

        $order = Order::find($orderId);
        if($order){
            if($order->status==5){
                $currency=null;
                if($order->currency_type == true) {
                    $currency = Currency::find($order->currency_id);
                }else{
                    $currency = Coin::find($order->coin_id);
                }

                $tryWallet  =  Wallet::with(["currency" => function ($query) use($currency){
                    return $query->select(["id","name","symbol","slug"])->where("symbol",$currency->symbol);
                }])
                    ->where("type", $order->currency_type)
                    ->where("user_id",$order->user_id)
                    ->get()->first();
                $tryWallet->balance = $tryWallet->balance + $order->amount;
                $tryWallet->save();
            }


            if($order->status == 4){
                $is_success = true;
                $type = "error";
                $title = _i("Error!");
                $message = _i("Zaten iptal edilmiş bir işlemi iptal tekrar iptal edemezsiniz!");

            }else{
                $order->status = 4;
                $order->description = $description;
                $order->operator_id = $user->id;
                $order->save();
                $is_success = true;
                $type = "info";
                $title = _i("İşlem Başarılı");
                $message = _i("Para çekim talebi iptal işlemi başarı ile gerçekleşti.");
            }
        }else{
            $is_success = false;
            $type = "error";
            $title = _i("İşlem Başarısız");
            $message = _i("Böyle bir para çekim talebi bulunamadı!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }
}