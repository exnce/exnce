<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/16/18
 * Time: 1:48 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\IdentityApproval;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

class VerificationRequestsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index() {
        //$IdentityApproval = IdentityApproval::where()

        return view("admin.users.verification-requests");
    }

    public function getIdentityApprovals() {
        //todo: 150 kayıttan fazlası tabloyu bozuyor
        $identityApprovalsList = IdentityApproval::where("status",">",-1)->orderBy('id','DESC');

        $datatable = DataTables::of($identityApprovalsList)
            ->addColumn("user_name", function($approval){
                return $approval->user->name;
            })
            ->editColumn("status", function($approval){
                return IdentityApprovalStatusCode($approval->status);
            })
            ->editColumn("updated_at", function($approval){
                return $approval->updated_at;
            });

        return $datatable->make(true);
    }

    public function getIdentityApprovalDetail(Request $request, $approvalId) {
        $approval = IdentityApproval::find($approvalId);

        $data = [
            "approval" => $approval,
        ];

        return view("admin.users.verification-requests-detail",$data);
    }

    public function updateIdentityApprovalDetail(Request $request) {
        $user = auth()->user();

        $approvalId = $request->input("approvalId");
        $status = $request->input("status");
        $description = $request->input("description");

        $approval = IdentityApproval::find($approvalId);

        if($approval) {
            $approval->status = $status;
            $approval->description = $description;
            $approval->save();

            activity("approveIdentity")
                ->performedOn($approval)
                ->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
                ->log(sprintf("%s %d nolu kimlik onayını güncelledi.", $user->name, $approval->id));

            $is_success = true;
                $type = "info";
                $title = _i("İşlem başarılı");
                $message = _i("Onay talebi güncellendi.");
        } else {
            $is_success = false;
            $type = "error";
            $title = _i("İşlem başarısız");
            $message = _i("Onay talebi bulunamadı!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return response()->json( $data,200);
    }

    public function showDocument($approvalId, $docType) {

        $approval = IdentityApproval::find($approvalId);

        if($docType == "idcard") {
            $path = storage_path("app/identity_approvals/".$approval->hash.$approval->id_card_front);
        } else if($docType == "selfie") {
            $path = storage_path("app/identity_approvals/".$approval->hash.$approval->selfie);
        }

        if (!File::exists($path)) {
            abort(404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;

    }

}