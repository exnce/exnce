<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 29.07.2018
 * Time: 09:20
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

class TransferRequestsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view("admin.transfers.transfers");
    }
}