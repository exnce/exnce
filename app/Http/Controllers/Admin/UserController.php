<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 12.09.2018
 * Time: 23:18
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\User;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    function index(){
        return view("admin.users.list");
    }

    public function confirmUsers(Request $request){
        User::where("status",0)->update(['status' => 1,'verified'=>1]);
        return redirect()->route("users");

    }

    public function getUsers(){
        $userList = User::where("status","<>",10);//->get();
        $datatable = DataTables::of($userList)
            ->addColumn("name", function($user){
                return $user->name;
            })

            ->editColumn("user_type", function($user){
                return UserStatusPermissionCode($user->user_type);
            });
        return $datatable->make(true);
    }

    public function getDetails(Request $request, $userId=null){

        $user = User::find($userId);
        $userWallets = $user->wallets()->get();
        foreach ($userWallets as $wallet) {
            $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
            if ($wallet->type == false) {
                $wallet->balance = number_format($wallet->balance, $wallet->coin->decimals, $wallet->coin->dec_point, $wallet->coin->thousands_sep);
            } else {
                $wallet->balance = number_format($wallet->balance, $wallet->currency->decimals, $wallet->currency->dec_point, $wallet->currency->thousands_sep);
            }
        }
        foreach ($userWallets as &$wallet) {
            $wallet->fixedBalance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user, false);
            $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
            $wallet->processingBalance = $wallet->fixedBalance - $wallet->balance;
            $wallet->processingBalance = number_format($wallet->processingBalance, $wallet->coin->decimals, $wallet->coin->dec_point, $wallet->coin->thousands_sep);


            if ($wallet->type == false) {
                $wallet->fixedBalance = number_format($wallet->fixedBalance, $wallet->coin->decimals, $wallet->coin->dec_point, $wallet->coin->thousands_sep);
            } else {
                $wallet->fixedBalance = number_format($wallet->fixedBalance, $wallet->currency->decimals, $wallet->currency->dec_point, $wallet->currency->thousands_sep);
            }

            if ($wallet->type == false) {
                $wallet->balance = number_format($wallet->balance, $wallet->coin->decimals, $wallet->coin->dec_point, $wallet->coin->thousands_sep);
            } else {
                $wallet->balance = number_format($wallet->balance, $wallet->currency->decimals, $wallet->currency->dec_point, $wallet->currency->thousands_sep);
            }
        }
        $data = ["user"=>$user,"wallets"=>$userWallets];
        return view("admin.users.details",$data);
    }

    public function loginAsUser(Request $request, $userId = null){
        $user = auth()->user();
        if(!$user){
            //logout;
        }
        if (!$userId) {
            return redirect()->route("user-detail",$userId);
        }

        $ownerUser = User::where("id",$userId)->get()->first();

        if(!$ownerUser){
            return redirect()->route("user-detail",$userId);
        }

        Auth::logout();
        Auth::loginUsingId($ownerUser->id);
        return redirect("/personal");


    }

    public function updateUserInfo(Request $request, $userId=null){
        $user = auth()->user();

        if (!$userId) {
            return redirect()->route("user-detail",$userId);
        }

        $ownerUser = User::where("id",$userId)->get()->first();
        if(!$ownerUser){
            return redirect()->route("user-detail",$userId);
        }

        $ownerUser->name=$request->post("name");
        $ownerUser->email=$request->post("email");
        $ownerUser->phone=$request->post("phone");
        $ownerUser->verified=$request->post("verified");
        $ownerUser->status=$request->post("status");
        //$ownerUser->calling_code=$request->post("calling_code");
        $ownerUser->user_type=$request->post("user_type");
        $ownerUser->two_factor = $request->post("two_factor");
        $ownerUser->save();

        return redirect()->route("user-detail",$userId);

    }
    public function updateWalletInfo(Request $request, $userId=null){
        $user = auth()->user();

        if (!$userId) {
            return redirect()->route("user-detail",$userId);
        }

        $ownerUser = User::find($userId);
        if(!$ownerUser){
            return redirect()->route("user-detail",$userId);
        }

        $walletId = $request->post("id");

        $currencyWallet = $ownerUser->wallets->where("id",$walletId)->first();
        if(!$currencyWallet){
            return redirect()->route("user-detail",$userId);
        }

        $currencyWallet->coin_id=$request->post("coin_id");
        $currencyWallet->type=$request->post("type");
        $currencyWallet->name=$request->post("name");
        $currencyWallet->balance=$request->post("balance");

        $currencyWallet->save();
        return redirect()->route("user-detail",$userId);

    }
}