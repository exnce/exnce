<?php

namespace App\Http\Controllers;

use App\SecurityPhoto;
use App\User;
use App\Interaction;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Http\Request;
use \ParagonIE\ConstantTime\Base32;
use App\Http\Requests\ValidateSmsRequest;
use App\Http\Controllers\Personal\InteractionController;

class TwoFactorController extends Controller
{
    use ValidatesRequests;

    public $timelimit = 120;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/personal';

    /**
     * Google2FAController constructor.
     */
    public function __construct()
    {
        $this->middleware('web');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enableGoogle2FA(Request $request)
    {
        $user = $request->user();
        $secret = $request->input("secret");
        $value = $request->input("authCode");

        $Google2FA = new Google2FA();
        if($Google2FA->verifyKey($secret, $value)) {
            $is_success = true;
            $type = "success";
            $message = _i("Google Authenticator ile giriş aktifleştirildi.");
            $title = _i("Info!");

            $user->two_factor = 2;
            $user->google2fa_secret = Crypt::encrypt($secret);
            $user->save();
        } else {
            $is_success = false;
            $type = "error";
            $message = _i("Kodu doğru girdiğinizden emin olun!");
            $title = _i("Error!");
        }

        activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("Google Authenticator aktifleştirildi."));

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function disableGoogle2FA(Request $request)
    {
        $user = $request->user();
        $secret = Crypt::decrypt($user->google2fa_secret);
        $value = $request->input("authCode");

        $Google2FA = new Google2FA();
        if($Google2FA->verifyKey($secret, $value)) {
            $is_success = true;
            $type = "warning";
            $message = _i("Google Authenticator ile giriş devre dışı bırakıldı.");
            $title = _i("Info!");

            $user->two_factor = 0;
            $user->google2fa_secret = "";
            $user->save();
        } else {
            $is_success = false;
            $type = "error";
            $message = _i("Kodu doğru girdiğinizden emin olun!");
            $title = _i("Error!");
        }

        activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("Google Authenticator devre dışı bırakıldı."));

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json( $data,200);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function enableSMS2FA(Request $request) {
        $user = $request->user();

        if($user->two_factor == 2 || $user->two_factor == 0) {
            $is_success = true;
            $type = "success";
            $message = _i("SMS ile giriş aktifleştirildi.");
            $title = _i("Info!");

            $user->two_factor = 1;
            $user->google2fa_secret = "";
            $user->save();
        } else {
            $is_success = false;
            $type = "error";
            $message = _i("Kodu doğru girdiğinizden emin olun!");
            $title = _i("Error!");
        }

        activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("SMS doğrulama aktifleştirildi."));

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json( $data,200);
    }

    /**
     * @return string
     */
    public function generateSecret()
    {
        $randomBytes = random_bytes(10);

        return Base32::encodeUpper($randomBytes);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendSMS() {
        $userId = session('2fa:user:id');

        if ($userId) {

            $userName = session('2fa:user:name');
            $securityPhoto = session('2fa:user:security_photo');
            $userPhone = session('2fa:user:phone');

            /// Kullanıcının kayıtlı telefonuna sms gönder ++++++
            /// SMS gönderiminde hata olursa login sayfasına hata koduyla beraber döndür ++++++++
            /// SMS giderse doğrulama sayfasına yönlendir ++++++++
            /// 2dk içinde yeniden sms gönderilmesini engelle +++++++++

            $last_sms = User::find($userId)->lastSms(1); //Tek kullanimlik sifre tipi
            if( ($last_sms && (time() - $last_sms->created_at->timestamp) >= $this->timelimit) || !$last_sms ) {
                $sms_code = rand(100000,999999);
                $last_sms = Interaction::create([
                    'user_id' => $userId,
                    'type' => 'sms',
                    'target' => $userPhone,
                    'content' => $sms_code,
                    'content_type'=> 1, //Tek kullanimlik sifre tipi
                ]);

                $smsapi = new InteractionController();
                $sentSms = $smsapi->sendSms($last_sms);

                if(!$sentSms) {
                    $last_sms->status = -1;
                    $last_sms->save();
                    session()->forget(['2fa:user:id', '2fa:user:name', '2fa:user:security_photo', '2fa:user:phone']);
                    return redirect()->route("login")->with("login:error", _i("Şuan SMS gönderiminde merkez kaynaklı bir hata bulunmakta. Daha sonra tekrar deneyiniz."));
                }
            }

            $timeleft = ($this->timelimit - (time() - $last_sms->created_at->timestamp));
            $minutes = floor(($timeleft / 60) % 60);
            $seconds = $timeleft % 60;

            $data = [
                "userName" => $userName,
                "userPhone" => $userPhone,
                "image" => base64_encode( SecurityPhoto::find($securityPhoto)->first()->image),
                "sms" => $last_sms->content, // todo: kaldır
                "minutes" => $minutes,
                "seconds" => $seconds,
            ];

            return view("2fa.sms-validate", $data);
        }

        return redirect('login');
    }

    /**
     * @param ValidateSmsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function validateSMS(ValidateSmsRequest $request) {
        $userId = $request->session()->pull('2fa:user:id');

        Auth::loginUsingId($userId);

        $last_sms = \auth()->user()->lastSms(1); //Tek kullanimlik sifre tipi
        $last_sms->status = 2;
        $last_sms->save();

        return redirect()->intended($this->redirectTo);
    }

}
