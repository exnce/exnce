<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 6/16/19
 * Time: 2:20 PM
 */

namespace App\Http\Controllers\Site;


use App\Coin;
use App\CoinDeposit;
use App\Http\Controllers\Controller;
use App\Order;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    public function index(Request $request){


        $coins=getCoinsStatusInformation();
        $data = ["coins"=>$coins,
                "metaTags"=>getSEOMetaTags(null,null,"status"),
        ];
        return view("site.status",$data);

    }

}