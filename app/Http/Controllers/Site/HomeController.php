<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 02:25
 */

namespace App\Http\Controllers\Site;

use App\Coin;
use App\Http\Controllers\Controller;
use App\Metamask;
use App\User;
use Locale;
use Illuminate\Http\Request;
use App\Rules\BlackListEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Xinax\LaravelGettext\Facades\LaravelGettext;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

class HomeController extends Controller
{
    public function __construct()
    {
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){

        $coins = Coin::orderBy("id","desc")
            ->limit(3)->get();

        $data = [
            "metaTags"=>getSEOMetaTags(null,null,"home"),
            "coins"=>$coins];


        return view("site.index",$data);
    }

    public function backup(){

        $coins = Coin::orderBy("id","asc")->get();
        $data = [
            "coins"=>$coins];
        return view("site.backup",$data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function login(){
        if(auth()->check()) {
            return \redirect()->route("dashboard");
        } else {
            return view("auth.login-new");
        }
    }

    /**
     * @param Request $request
     * @param null $referenceId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function register(Request $request, $referenceId = null){
        if(auth()->check()) {
            return \redirect()->route("dashboard");
        } else {
            $hasRef = false;
            $user = null;

            $hasCampain = false;
            $campainCode = "";
            $callbackURL = "";

            if($request->has("campain_code")){
                $hasCampain = true;
                $campainCode = $request->get("campain_code");
            }

            if($request->has("callback_url")){
                $hasCampain = true;
                $callbackURL = $request->get("callback_url");
            }

            if($referenceId){
                $user = \App\User::where("ref_code",$referenceId)->get()->first();
                if($user){
                    $hasRef = true;
                }
            }

            $data = ["reference_id"=>$referenceId,
                     "user"=>$user,
                     "has_campain"=>$hasCampain,
                     "campain_code" => $campainCode,
                     "callback_url"=>$callbackURL,
                     "has_ref"=>$hasRef];



            $lang = substr($request->server('HTTP_ACCEPT_LANGUAGE'),0,2);

            $view = "auth.register";

            if($lang == "tr"){
                $view = "auth.invalid-country";
            }


            return view($view,$data);
        }
    }

    public function loginMetamask(Request $request){
        $account = $request->post("account");
        $key = $request->post("key");
        $message = $request->post("message");
        $output = $request->post("output");
        $signature = $request->post("signature");

        $checkMetamask = Metamask::where("address",$account)->first();
        if(!$checkMetamask){
            $pKey=generatePrivateKey($account);
            $first_user = new User();
            $first_user->name = $account;
            $first_user->verified=2;
            $first_user->security_photo=rand(1,8);
            $first_user->email = "metamask".$account;
            $first_user->phone = "METAMASK";
            $first_user->private_key = $pKey;
            $first_user->public_key = generatePublicKey($pKey);
            $first_user->user_type = 1;
            $first_user->status = 1;
            $first_user->is_metamask = true;
            $first_user->password = bcrypt($pKey.$account);
            $first_user->save();

            $checkMetamask =  new Metamask();
            $checkMetamask->user_id = $first_user->id;
            $checkMetamask->address = $account;
            $checkMetamask->key=$key;
            $checkMetamask->signature=$signature;
            $checkMetamask->is_valid = false;
            $checkMetamask->save();
        }else{
            $first_user = $checkMetamask->user;
        }
        $success = false;
        if($first_user){
            Auth::logout();
            Auth::loginUsingId($first_user->id);
            $success = true;
        }else{
            $success = false;
        }
        return array("success"=>$success);
    }

    public function loginVia(Request $request){

        $auth_type = $request->post("auth_type");
        switch ($auth_type){
            case "metamask":
                return $this->loginMetamask($request);
                break;
            default:
                return "BURN_BABY_BURN!!!";
                break;
        }
   }

    /**
     * @param null $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changeLang($locale=null)
    {
        LaravelGettext::setLocale($locale);
        return Redirect::to(URL::previous());
    }

}