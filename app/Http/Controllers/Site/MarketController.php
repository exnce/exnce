<?php

namespace App\Http\Controllers\Site;

use App\Coin;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\TransactionDetail;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class MarketController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if(auth()->user())
            return redirect()->action("\App\Http\Controllers\Personal\MarketController@index");

        $activeCoinID = session("activeCoinID");
        $activeCurrencyID = session("activeCurrencyID");

        if($activeCoinID && $activeCurrencyID) {
            $activeCoin = Coin::find($activeCoinID);
            if(!$activeCoin) return redirect()->route("market");

            $activeCurrency = $activeCoin->listPairs()->find($activeCurrencyID);
            if(!$activeCurrency) return redirect()->route("market");

        } else {
            $activeCoin = Coin::where("symbol", getDefaultBaseCoin())->first();
            $activeCurrency = $activeCoin->listPairs()->where("symbol", getDefaultSubCoin())->first();

            session([
                "activeCoinID"=>$activeCoin->id,
                "activeCurrencyID"=>$activeCurrency->id,
            ]);
        }


        if($activeCurrency->symbol == "ORX"){
            return redirect()->route("pair",["XLM","ETH"]);
        }

        if( $activeCoin->processing_fee == 0 ) {
            $activeCurrency->processing_fee = 0;
        } else if( $activeCurrency->processing_fee == 0 ) {
            $activeCoin->processing_fee = 0;
        }

        $baseCoins = DB::table('coin_coin')->groupBy("coin_id")->orderBy("id","asc")->get();
        $baseCoins = Arr::pluck($baseCoins,"","coin_id");
        foreach ($baseCoins as $coin_id => &$baseCoin) {
            $coin = Coin::find($coin_id);
            $baseCoin = $coin->symbol;
        }






        $data = [
            "activeCoin"=>$activeCoin,
            "activeCurrency"=>$activeCurrency,
            "pairList"=>$this->getPairs(),
            "baseCoins"=>$baseCoins,
            "favorites"=>[],
            "metaTags"=>getSEOMetaTags($activeCoin,$activeCurrency,"market")
        ];
        return view("site.home",$data);
    }

    public function pair(Request $request, $currency = null, $coin = null){

        if(auth()->user())
            return redirect()->action("\App\Http\Controllers\Personal\MarketController@pair", [
                "pair" => $currency,
                "coin" => $coin,
            ]);

        $activeCoinID = session("activeCoinID");
        $activeCurrencyID = session("activeCurrencyID");

        if((($currency) && ($coin)) && ($coin != -1) && ($currency!=-1)){
            $activeCoin = Coin::where("symbol", $coin)->first();
            if(!$activeCoin) return redirect()->route("market");

            $activeCurrency = $activeCoin->listPairs()->where("symbol", $currency)->first();
            if(!$activeCurrency) return redirect()->route("market");

        } else if($activeCoinID && $activeCurrencyID) {
            $activeCoin = Coin::find($activeCoinID);
            $activeCurrency = $activeCoin->listPairs()->find($activeCurrencyID);
            if(!$activeCurrency) return redirect()->route("market");

        } else {
            $activeCoin = Coin::where("symbol", getDefaultBaseCoin())->first();
            $activeCurrency = $activeCoin->listPairs()->where("symbol", getDefaultSubCoin())->first();
        }

        if($activeCurrency->symbol == "ORX"){
            return redirect()->route("pair",["XLM","ETH"]);
        }

        if(($currency && $coin) || ($activeCoinID && $activeCurrencyID)) {

            session([
                "activeCoinID"=>$activeCoin->id,
                "activeCurrencyID"=>$activeCurrency->id,
            ]);
        }

        if( $activeCoin->processing_fee == 0 ) {
            $activeCurrency->processing_fee = 0;
        } else if( $activeCurrency->processing_fee == 0 ) {
            $activeCoin->processing_fee = 0;
        }

        $baseCoins = DB::table('coin_coin')->groupBy("coin_id")->orderBy("id","asc")->get();
        $baseCoins = Arr::pluck($baseCoins,"","coin_id");
        foreach ($baseCoins as $coin_id => &$baseCoin) {
            $coin = Coin::find($coin_id);
            $baseCoin = $coin->symbol;
        }

        $data = [
            "activeCoin"=>$activeCoin,
            "activeCurrency"=>$activeCurrency,
            "pairList"=>$this->getPairs(),
            "baseCoins"=>$baseCoins,
            "favorites"=>[],
            "metaTags"=>getSEOMetaTags($activeCoin,$activeCurrency,"market")
        ];

        return view("site.home",$data);
    }

    public function getPairs() {
        $pairList = [];
        $pairs = DB::table('coin_coin')->get();

        foreach($pairs as $pair) {
            $coin = Coin::find($pair->coin_id);
            $currency = Coin::find($pair->pair_id);

            if(!$currency->ieo) {
                $pairList[$currency->symbol . "/" . $coin->symbol] = [
                    "name" => $currency->symbol . "/" . $coin->symbol,
                    "volume" => $coin->volume($currency->id),
                    "change" => $coin->change($currency->id),
                    "coin" => $coin->symbol,
                    "currency" => $currency->symbol,
                    "lastprice" => toFixed($coin->lastPrice($currency->id) ? $coin->lastPrice($currency->id) : 0, 8),
                    "slug" => $currency->slug,
                    "coin_id" => $coin->id,
                    "currency_id" => $currency->id,
                    "coin_name" => $currency->name
                ];
            }
        }

        return $pairList;
    }


    public function getOrderBookWithStats($coinId, $currencyId){

        if(($coinId == -1) || ($currencyId == -1)){
            return null;
        }

        $coin = Coin::find($coinId);



        $activeCurrency = $coin->listPairs()->find($currencyId);


        if($coin){
            $buyTransactions = Transaction::select("transactions.*"
                ,DB::raw('SUM(remaining_amount) as totalAmount')
                ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
                ->where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("type","buy")
                ->where("status","<",2)
                ->groupBy("price")
                ->orderBy("price","desc")
                ->take(20)->get();

            $sellTransactions =  Transaction::select("transactions.*"
                ,DB::raw('SUM(remaining_amount) as totalAmount')
                ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
                ->where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("type","sell")
                ->where("status","<",2)
                ->groupBy("price")
                ->orderBy("price","desc")
                ->get()->reverse()->take(20);


            /*            $transactions_list = Transaction::whereIn("status",[1,2])
                            ->where("coin_id",$coin->id)
                            ->where("currency_id",$activeCurrency->id)
                            ->where("type", "buy")->take(50)->pluck("id")->toArray();

                        $market_history = TransactionDetail::whereIn("transaction_id", $transactions_list)
                            ->orderBy("id", "desc")->get();*/

            $market_history = TransactionDetail::where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("type", "buy")
                ->orderBy("id", "desc")
                ->take(50)->get();

            for($i = count($market_history)-1; $i >= 0; $i--) {
                if(!@$oldPrice) $oldPrice = $market_history[$i]->price;

                if($market_history[$i]->price > $oldPrice) {
                    $market_history[$i]->color = 1;
                } elseif($market_history[$i]->price < $oldPrice) {
                    $market_history[$i]->color = 2;
                } else {
                    $market_history[$i]->color = 0;
                }

                $oldPrice = $market_history[$i]->price;
            }

            $coins = Coin::where("status", 0)->get();

            //$summaryTransactions=$buyTransactions->sum("totalAmount");
            $sellSummary = $sellTransactions->sum("totalPrice");
            $buySummary = $buyTransactions->sum("totalPrice");

            $deepBuy = 0;
            $calculatedBuy = 0;
            foreach($buyTransactions as &$buy){
                $buy->totalAmount = floatval($buy->totalAmount);
                $buy->totalPrice = number_format($buy->totalPrice, "8", ".", "");
                //$buy->totalPrice = floatval($buy->totalPrice);
                //$buy->price = floatval($buy->price);
                $deepBuy += floatval($buy->totalPrice);
                $calculatedBuy += floatval($buy->totalPrice);
                $buy->depth = ($deepBuy * 100 / $buySummary);
                $buy->calculatedTotalAmount = sprintf('%.8f', $calculatedBuy);;
            }

            $deepSell=0;
            $calculatedSell = 0;
            foreach($sellTransactions as &$sell){
                $sell->totalAmount = floatval($sell->totalAmount);
                $sell->totalPrice = number_format($sell->totalPrice, "8", ".", "");
                //$sell->totalPrice = floatval($sell->totalPrice);
                //$sell->price = floatval($sell->price);
                $deepSell += floatval($sell->totalPrice);
                $calculatedSell += floatval($sell->totalPrice);
                $sell->depth =  ($deepSell * 100 / $sellSummary);
                $sell->calculatedTotalAmount =  sprintf('%.8f', $calculatedSell);
            }

            foreach($market_history as &$history){
                //$history->completed_amount = floatval($history->completed_amount);
                //$history->price = floatval($history->price);
                $history->amount = floatval($history->amount);

                $date = Carbon::parse($history->updated_at)->format('H:i:s');
                unset($history->updated_at);
                $history->date = $date;
            }

            //todo: perbar volume güncellemesi
            /*            foreach($coins as &$cryptoCurrency){
                            $cryptoCurrency->lastPrice = number_format($cryptoCurrency->lastPrice($activeCurrency->id), 4, $cryptoCurrency->dec_point, $cryptoCurrency->thousands_sep);
                            $cryptoCurrency->volume = $cryptoCurrency->volume($activeCurrency->id);
                        }*/

            $pairsData = [];


            $volumes = [];
            $pairs = DB::table('coin_coin')->get();
            foreach($pairs as $pair) {
                $pair_coin = Coin::find($pair->coin_id);
                $pair_currency = Coin::find($pair->pair_id);

                $volumes[$pair_currency->symbol . "/" . $pair_coin->symbol] = $pair_coin->volume($pair_currency->id);
            }




        } else {
            $buyTransactions = [];
            $sellTransactions = [];
            $is_success = false;
            $type = "error";
            $message = _i("Bu kripto para birimi sistemde tanımlı değildir!");
            $title = _i("Error!");
        }

        $stats = getHeaderHomeStatistics($coinId,$currencyId);

        $data = [
            "success"=>true,
            "coin" => $coin,
            "coin_id"=>$coin->id,
            "stats"=>$stats,
            "buy"=>$buyTransactions,
            "sell"=>$sellTransactions,
            "market_history"=>($market_history?$market_history:false),
            //"coins"=>$coins,
            "pairsData"=>$this->getPairs(),
            "volumes"=>$volumes,
            "currency" => $activeCurrency,
        ];

        return $data;
    }

    /**
     * @param Request $request
     * @param null $coinId
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadMarketData(Request $request, $coinId = null, $currencyId = null)
    {
        if($request->filled("coinId") && $request->filled("currencyId")) {
            $coinId = $request->input("coinId");
            $currencyId = $request->input("currencyId");
        }else{
            $coinId = Coin::where("symbol", getDefaultBaseCoin())->first()->id;
            $currencyId = Coin::where("symbol", getDefaultSubCoin())->first()->id;
        }




        $user = auth()->user();
        $transactionData = self::getOrderBookWithStats($coinId,$currencyId);

        $redis=\Redis::connection();
        $redis->publish("message",json_encode($transactionData));

        $coin = Coin::find($coinId);
        $activeCurrency = Coin::find($currencyId);

        $data=["success"=>true,"coin" => $coin,"currency" => $activeCurrency];
        return response()->json( $data,200);
    }

}