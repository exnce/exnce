<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/30/19
 * Time: 8:03 AM
 */

namespace App\Http\Controllers\Site;


use App\Airdrop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AirdropController extends Controller
{
    public function index(Request $request)
    {

        if(auth()->user()){
            return redirect()->route("airdrops");
        }

        $airdrops = Airdrop::orderBy("id", "desc")->whereNotIn("id",[11,9])->get();

        $data = ["airdrops" => $airdrops,
            "metaTags"=>getSEOMetaTags(null,null,"airdrops")
        ];

        return view("airdrop.index", $data);
    }

    public function details(Request $request,$airdropId = null)
    {

        if($airdropId){
            $airdrop = Airdrop::where("id", $airdropId)->get()->first();

            if($airdrop){
                $data = ["airdrop" => $airdrop];
                return view("airdrop.details", $data);
            }else{
                return redirect()->route("airdrops");
            }
        }
    }
}