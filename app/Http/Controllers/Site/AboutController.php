<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/9/19
 * Time: 3:00 PM
 */

namespace App\Http\Controllers\Site;


use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function index(){

        $data = [
            "metaTags"=>getSEOMetaTags(null,null,"about")
        ];
        return view("site.about",$data);


    }
}