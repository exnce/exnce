<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/9/19
 * Time: 12:20 PM
 */

namespace App\Http\Controllers\Site;


use App\Coin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Personal\MarketController;

class FeeController extends Controller
{
    public function  index(){


        $market = new MarketController();
        $pairs = $market->getPairs(true);


        $coins = Coin::where("status",0)->get();
        $data = ["pairs"=>$pairs,
                 "metaTags"=>getSEOMetaTags(null,null,"fees"),
                 "coins"=>$coins];
        return view("site.fees",$data);

    }

}