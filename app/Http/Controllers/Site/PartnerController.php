<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/28/19
 * Time: 7:24 AM
 */

namespace App\Http\Controllers\Site;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PartnerController extends Controller
{

    public function index(Request $request){
        return view("site.partners");
    }
}