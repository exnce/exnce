<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 3/28/19
 * Time: 3:38 PM
 */

namespace App\Http\Controllers\RPC;


use App\Coin;
use App\Http\Controllers\Controller;
use App\User;
use App\Wallet;
use App\WalletAddress;
use GuzzleHttp\Exception\RequestException;
use phpseclib\Math\BigInteger;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class XLMController extends Controller
{

    public function sendGETRequest(){

    }

    public function createWallet(Request $request,User $user,Coin $coin,$walletName)
    {


        $szEndPointURL = env("ETH_CALLBACK_URI")."?symbol=".$coin->symbol;

        try {
            $client = new Client();
            $client = $client->request("GET", $szEndPointURL, []);
            $status = $client->getStatusCode();
            $response = json_decode($client->getBody());
            $is_success = true;
            $message = _i("The connection to the Stellar service has been successfully established!");
        } catch (Exception $exception) {
            $is_success = false;
            $message = _i("Couldn't connect to Stellar!");
            $response = $exception->getMessage();
        } catch (RequestException $exception) {
            $is_success = false;
            $message = _i("Couldn't connect to Stellar!");
            $response = $exception->getMessage();
        } catch (GuzzleException $exception) {
            $is_success = false;
            $message = _i("Couldn't connect to Stellar!");
            $response = $exception->getMessage();
        }

        if ($is_success == true) {
            if ($response->status == true) {
                $wallet = Wallet::create([
                    "user_id" => $user->id,
                    "type" => false,
                    "coin_id" => $coin->id,
                    "name" => $walletName,
                    "balance " => 0.00,
                    "status" => 0
                ]);
                $walletAddress = new WalletAddress();
                $walletAddress->user_id = $user->id;
                $walletAddress->wallet_id = $wallet->id;
                $walletAddress->coin_id = $coin->id;
                $walletAddress->address = $response->address;
                $walletAddress->password = $response->privateKey;
                $walletAddress->payment_id = "";
                $walletAddress->destination_tag = "";
                $walletAddress->address_type="XLM";
                $walletAddress->status = 0;
                $walletAddress->save();
                $wallet->save();

                $message = _i("The wallet for %s cryptocurrency has was created successfuly.");
                $message = sprintf($message, $coin->name);
                $title = _i("Info!");
                $is_success = true;
                $type = "success";
                return array("message"=>$message, "title"=>$title, "type"=>$type,"success"=>$is_success);
            }
            else
            {
                $is_success = false;
            }
        }


        $message = $response;
        $title = _i("Error!");
        $is_success = false;
        $type = "error";
        return array("message"=>$message, "title"=>$title, "type"=>$type,"success"=>$is_success);



    }


}