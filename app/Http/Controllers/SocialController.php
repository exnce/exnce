<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/10/19
 * Time: 1:13 PM
 */

namespace App\Http\Controllers;


use App\UserMeta;
use Illuminate\Http\Request;

class SocialController
{
    public function index(Request $request){


        $user = auth()->user();

        $hasFacebook = false;
        if($user){

            $userMetaFacebook = UserMeta::where("platform","FACEBOOK")->where("user_id",$user->id)->get()->first();
            if($userMetaFacebook){
                $hasFacebook = true;

            }




        }

        $data = ["hasFacebook"=>$hasFacebook];
        return view('airdrop.confirm',$data);
    }

}