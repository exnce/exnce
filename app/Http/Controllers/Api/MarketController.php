<?php

namespace App\Http\Controllers\Api;

use App\TransactionDetail;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Coin;
use App\Transaction;
use Illuminate\Support\Facades\Redis;

class MarketController extends Controller
{

    public function createBuy(Request $request){

        $user = $request->user();

        $reqPair = explode("/", $request->post("pair"));

        $valBuyCoin = $reqPair[1];
        $valBuyCurrency = $reqPair[0];

        $coin = Coin::where("symbol",$valBuyCoin)->first();
        $currency = $coin->listPairs()->where("symbol",$valBuyCurrency)->first();

        $valAmountBuyInt = $request->post("amount");

        $sellTransactions = Transaction::where("type", "sell")
            ->whereIn("status", [0,1])
            ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
            ->selectRaw('min(price) as bid')->first();
        $valPriceBuy = $sellTransactions->bid;

        $valSummaryBuy = floatval($valAmountBuyInt) * floatval($valPriceBuy);
        $valFeeBuy = 0;




        $coinWallet = $user->wallets()->where("type",0)->where("coin_id",$coin->id)->first();
        $currencyWallet = $user->wallets()->where("type",0)->where("coin_id",$currency->id)->first();

        $is_success = true;
        $type="";
        $message="";
        $title="";

        $amount = floatval($valAmountBuyInt);
        $balance = 0.00;
        if($currencyWallet){
            $balance = floatval($user->user_balance(false,$coin->id,false,true,$user));
        }

        /*        $amount = number_format($amount, $coin->decimals,$coin->dec_point,$coin->thousands_sep);
                $balance = number_format($balance, $currency->decimals,$currency->dec_point,$currency->thousands_sep);*/


        $summaryBuy = number_format($valSummaryBuy, $currency->decimals, $currency->dec_point,"");
        $priceBuy = number_format($valPriceBuy, $currency->decimals, $currency->dec_point,"");
        $valFeeBuy = number_format($valFeeBuy, $currency->decimals,$currency->dec_point,"");

        if($coin->processing_fee > 0 && $currency->processing_fee > 0) {
            $feeAmount = $currency->processing_fee;
        } else {
            $feeAmount = 0;
        }

        //$mFee = $valFeeBuy;
        $mCalculatedFee = $amount * $feeAmount;
        $mCalculatedTotal = $priceBuy * $amount;

        $zeroBalance = 0.00;

        if(!$currency){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi sistemde tanımlı değildir!", $valBuyCurrency);
            $title = _i("Error!");
        } else if(!$coin){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi sistemde tanımlı değildir!", $valBuyCoin);
            $title = _i("Error!");
        } else if($coin && $coin->status != 0){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi henüz etkin değildir!", $coin->name);
            $title = _i("Error!");
        } else if($currency && $currency->status != 0){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi henüz etkin değildir!", $currency->name);
            $title = _i("Error!");
        } else if(!$currencyWallet){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimine ait cüzdan, hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!", $valBuyCurrency);
            $title = _i("Error!");
        } else if(!$coinWallet){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimine cüzdan hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!", $valBuyCoin);
            $title = _i("Error!");
        } else if($amount<=0.00){
            $is_success = false;
            $type = "error";
            $message = _i("Girilen miktar hatalıdır!");
            $title = _i("Error!");
        } else if($priceBuy <= $zeroBalance){
            $message = _i("Girilen fiyat hatalıdır!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        } else if($summaryBuy <= $zeroBalance){
            $message = _i("Toplam tutar 0'dan küçük olamaz!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        } else if($mCalculatedTotal > $balance){
            $message = _i("%s kullanılabilir cüzdan bakiyeniz yetersizdir!");
            $message=sprintf($message,$coin->name);
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        }

        if($is_success == true) {

            $transaction=Transaction::create([
                "user_id" => $user->id,
                "coin_id" => $coin->id,
                "currency_id" => $currency->id,
                "type" => "buy",
                "amount" => $amount,
                "real_amount" => $amount,
                "completed_amount" => 0.00,
                "remaining_amount" => $amount,
                "price" => $valPriceBuy,
                "total" => $valSummaryBuy,
                "fee" => $mCalculatedFee, //$mFee
                "processing_fee" => floatval($feeAmount),
                "ref_code" => generateTransactionCode(),
                "status" => 0
            ]);

            self::completeTransaction($transaction,$user,$coin,$currency);

            $type = "success";
            $message=_i("Alış işlemi emriniz başarı ile oluşturulmuştur.");
            $title="Bilgi";


            $market = new \App\Http\Controllers\Personal\MarketController();
            $transactionData=$market->getOrderBookWithStats($coin->id,$currency->id);

            $redis=Redis::connection();
            $redis->publish("message",json_encode($transactionData));
        }

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message,
        ];
        return $data;
    }
    
    public function completeTransaction(Transaction $transaction, User $user, Coin $coin, Coin $currency) {

        $szTransactionType = "-";
        if($transaction->type == "buy"){
            $szTransactionType = "sell";
        }else if($transaction->type=="sell"){
            $szTransactionType="buy";
        }


        $market = new \App\Http\Controllers\Personal\MarketController();

        // ayni tutarda yeterli miktarda olan diger islemlerden parca parca alim işleminin saglanmasi
        $priceComparison = ($transaction->type == "buy" ? ">=" : "<=");
        $orderBy = ($transaction->type == "buy" ? "asc" : "desc");

        $checkTransactions = Transaction::whereIn("status", [0,1])
            ->where("coin_id",$coin->id)
            ->where("currency_id",$currency->id)
            ->where("type",$szTransactionType)
            //->where("user_id","<>",$user->id)
            ->where("price",$priceComparison,$transaction->price)->orderBy("price", $orderBy)->get();

        foreach ($checkTransactions as $checkTransaction) {

            if($transaction->status < 2) {
                if ($transaction->remaining_amount >= $checkTransaction->remaining_amount) {

                    $calculatedTotal = floatval($checkTransaction->remaining_amount) * floatval($checkTransaction->price);
                    $transactionDetails = [
                        [
                            "user_id" => $transaction->user_id,
                            "transaction_id" => $transaction->id,
                            "dest_transaction_id" => $checkTransaction->id,
                            "coin_id" => $coin->id,
                            "currency_id" => $currency->id,
                            "type" => $transaction->type,
                            "amount" => $checkTransaction->remaining_amount,
                            "price" => $checkTransaction->price,
                            "total" => $calculatedTotal,
                            "fee" => $calculatedTotal * $transaction->processing_fee,
                        ],
                        [
                            "user_id" => $checkTransaction->user_id,
                            "transaction_id" => $checkTransaction->id,
                            "dest_transaction_id" => $transaction->id,
                            "coin_id" => $coin->id,
                            "currency_id" => $currency->id,
                            "type" => $checkTransaction->type,
                            "amount" => $checkTransaction->remaining_amount,
                            "price" => $checkTransaction->price,
                            "total" => $calculatedTotal,
                            "fee" => $calculatedTotal * $checkTransaction->processing_fee,
                        ]
                    ];

                    foreach ($transactionDetails as $transactionDetail) {
                        TransactionDetail::create($transactionDetail);
                    }


                    $transaction->completed_amount += $checkTransaction->remaining_amount;
                    $transaction->remaining_amount -= $checkTransaction->remaining_amount;
                    $transaction->status = 1;
                    $transaction->save();


                    $checkTransaction->completed_amount += $checkTransaction->remaining_amount;
                    $checkTransaction->remaining_amount -= $checkTransaction->remaining_amount;
                    $checkTransaction->status = 2;
                    $checkTransaction->save();

                    $market->updateUserWalletBalance($checkTransaction);

                } else {

                    $calculatedTotal = floatval($transaction->remaining_amount) * floatval($checkTransaction->price);
                    $transactionDetails = [
                        [
                            "user_id" => $transaction->user_id,
                            "transaction_id" => $transaction->id,
                            "dest_transaction_id" => $checkTransaction->id,
                            "coin_id" => $coin->id,
                            "currency_id" => $currency->id,
                            "type" => $transaction->type,
                            "amount" => $transaction->remaining_amount,
                            "price" => $checkTransaction->price,
                            "total" => (floatval($transaction->remaining_amount) * floatval($checkTransaction->price)),
                            "fee" => $calculatedTotal * $transaction->processing_fee,
                        ],
                        [
                            "user_id" => $checkTransaction->user_id,
                            "transaction_id" => $checkTransaction->id,
                            "dest_transaction_id" => $transaction->id,
                            "coin_id" => $coin->id,
                            "currency_id" => $currency->id,
                            "type" => $checkTransaction->type,
                            "amount" => $transaction->remaining_amount,
                            "price" => $checkTransaction->price,
                            "total" => (floatval($transaction->remaining_amount) * floatval($checkTransaction->price)),
                            "fee" => $calculatedTotal * $checkTransaction->processing_fee,
                        ]
                    ];

                    foreach ($transactionDetails as $transactionDetail) {
                        TransactionDetail::create($transactionDetail);
                    }


                    $checkTransaction->completed_amount += $transaction->remaining_amount;
                    $checkTransaction->remaining_amount -= $transaction->remaining_amount;
                    $checkTransaction->status = 1;
                    $checkTransaction->save();

                    $transaction->completed_amount += $transaction->remaining_amount;
                    $transaction->remaining_amount -= $transaction->remaining_amount;
                    $transaction->save();

                    $market->updateUserWalletBalance($transaction);
                }
            }
        }

        $transaction->status = 2;
        $transaction->save();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSell(Request $request){
        $user = auth()->user();

        $valAmountSellInt = $request->input("edtAmountSellInt");
        $valSellCoin = $request->input("edtSellCoin");
        $valSellCurrency = $request->input("edtSellCurrency");
        $valPriceSell = $request->input("edtPriceSell");
        $valSummarySell = $request->input("edtSummarySell");
        $valFeeSell = $request->input("edtFeeSell");

        $coin = Coin::where("symbol",$valSellCoin)->first();
        $currency = $coin->listPairs()->where("symbol",$valSellCurrency)->first();

        $coinWallet = $user->wallets()->where("type",false)->where("coin_id",$coin->id)->first();
        $currencyWallet = $user->wallets()->where("type",false)->where("coin_id",$currency->id)->first();

        $balance = 0.00;
        if($currencyWallet){
            $balance = floatval($user->user_balance(false,$currency->id,true,true,$user));
            $coinBalance = floatval($user->user_balance(false,$coin->id,false,true,$user));
        }

        $is_success = true;
        $type="";
        $message="";
        $title="";
        $amount = floatval($valAmountSellInt);

        $summarySell = number_format($valSummarySell, $currency->decimals,$currency->dec_point,"");
        $valFeeSell = number_format($valFeeSell, $currency->decimals,$currency->dec_point,"");

        if($coin->processing_fee > 0 && $currency->processing_fee > 0) {
            $feeAmount = $coin->processing_fee;
        } else {
            $feeAmount = 0;
        }

        //$mFee = $valFeeSell;
        $mCalculatedFee = $summarySell * floatval($feeAmount);

        if(!$currency){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi sistemde tanımlı değildir!", $valSellCoin);
            $title = _i("Error!");
        } else if(!$coin){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi sistemde tanımlı değildir!", $valSellCurrency);
            $title = _i("Error!");
        } else if($coin && $coin->status != 0){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi henüz etkin değildir!", $coin->symbol);
            $title = _i("Error!");
        } else if($currency && $currency->status != 0){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimi henüz etkin değildir!", $currency->symbol);
            $title = _i("Error!");
        } else if(!$coinWallet){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimine ait cüzdan, hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!", $valSellCoin);
            $title = _i("Error!");
        } else if(!$currencyWallet){
            $is_success = false;
            $type = "error";
            $message = _i("%s kripto para birimine ait cüzdan, hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!", $valSellCurrency);
            $title = _i("Error!");
        } else if($amount<=0){
            $is_success = false;
            $type = "error";
            $message = _i("Girilen miktar hatalıdır!");
            $title = _i("Error!");
        } else if($valPriceSell <= 0){
            $message = _i("Girilen fiyat hatalıdır!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        } /*else if($coinWallet && $coinWallet->balance <= 0.00){
            $message = _i("Bakiyeniz yetersizdir!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        }*/ else if($valSummarySell <= 0.00){
            $message = _i("Toplam tutar 0'dan küçük olamaz!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        } /*else if(floatval($mFee) !== floatval($mCalculatedFee)){
            $message = _i("Komisyon oranı geçersizdir!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        }*/ else if($valSummarySell != (toFixed(($valPriceSell * $amount),8))){
            $message = _i("Girilen miktar ve fiyat çarpımı değeri gerçek tutarı yansıtmamaktadır!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        } else if( floatval($balance) < floatval($amount)){
            $message = _i("%s (%s) kripto para birimine ait cüzdanınızın bakiyesi yetersizdir!");
            $message = sprintf($message,$currency->name,$currency->symbol);
            $title = _i("Error!");
            $is_success = false;
        } /*else if(floatval($coinBalance)<$mCalculatedFee){
            $message = _i("%s bakiyeniz komisyon için yetersizdir!");
            $message = sprintf($message,$coin->name);
            $title = _i("Error!");
            $is_success = false;
        } else if($feeAmount>0 && $mCalculatedFee<=0){
            $message = _i("Bu tutarda işlem yapamazsınız!");
            $title = _i("Error!");
            $is_success = false;
        }*/


        if($is_success == true) {
            $transaction = Transaction::create([
                "user_id" => $user->id,
                "coin_id" => $coin->id,
                "currency_id" => $currency->id,
                "type" => "sell",
                "amount" => $amount,
                "real_amount" => $amount,
                "completed_amount" => 0.00,
                "remaining_amount" => $amount,
                "price" => $valPriceSell,
                "total" => $valSummarySell,
                "fee" => $mCalculatedFee,
                "processing_fee" => floatval($feeAmount),
                "ref_code" => generateTransactionCode(),
                "status" => 0
            ]);

            //dispatch(new CompleteTransaction($user,$transaction,$coin,$currency));

            self::completeTransaction($transaction,$user,$coin,$currency);


            $transactionData=self::getOrderBookWithStats($coin->id,$currency->id);
            $redis=\Redis::connection();

            $redis->publish("message",json_encode($transactionData));

            $type = "success";
            $message=_i("Satış işlemi emriniz başarı ile oluşturulmuştur.");
            //$message = sprintf($message,$amount,$balance);
            $title="Bilgi";
        }

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json( $data,200);
    }

    
}