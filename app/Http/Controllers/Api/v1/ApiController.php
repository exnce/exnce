<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/3/19
 * Time: 8:26 PM
 */

namespace App\Http\Controllers\Api\v1;


use App\Coin;
use App\CoinCoin;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\TransactionDetail;
use App\User;
use App\UserApiKey;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public function index(){
        return view("api.v1.index");
    }

    public function walletProfileAPI(Request $request, $memberId = null){

        $user = null;
        $userWallets = null;


        if($memberId){
            $user = User::where(DB::raw('md5(email)') , $memberId)->get()->first();
        }

        if($user){
            $userWallets = $user->wallets()->get();
            $renderObj = $request->get("exnceRenderObj");
            $site = $request->get("site");



            $walletList = null;
            foreach ($userWallets as $wallet) {



                    $addressInfo =$wallet->address()->get()->last();

                   $walletInf = array(
                    "id"=>$wallet->coin->symbol,
                    "text"=>$wallet->coin->name,
                    "paymentAddress" => $addressInfo->address,
                    "paymentId" => $addressInfo->payment_id,
                    "destinationTag" => $addressInfo->destination_tag,
                    "assetName" => $wallet->coin->name,
                    "assetSymbol" => $wallet->coin->symbol,
                    "assetLogo" => asset("/images/coins/".$wallet->coin->slug.".svg"));

                  $walletList[] = $walletInf;


            }


        }



        $exncecom = '<b style="border:1px solid #d3d3d3; background: #f6f6f6; border-radius:4px; padding:4px;"><span style="color:#ff0000;">e</span><span style="color:#ff7f00;">x</span><span style="color:#ffaf00;">n</span><span style="color:#ffdf00;">c</span><span style="color:#00ff00;">e</span><span style="color:#00ffff;">.</span><span style="color:#0080ff;">c</span><span style="color:#0000ff;">o</span><span style="color:#8b00ff;">m</span></b>';

        $exncecom = '<a target="_blank" href="https://exnce.com">'.$exncecom.'</a>';
        $cssData = '.select2-container .select2-selection--single{box-sizing:border-box;cursor:pointer;display:block;height:28px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--single .select2-selection__rendered{display:block;padding-left:8px;padding-right:20px;overflow:hidden;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-selection--single .select2-selection__clear{position:relative}.select2-container[dir=rtl] .select2-selection--single .select2-selection__rendered{padding-right:8px;padding-left:20px}.select2-container .select2-selection--multiple{box-sizing:border-box;cursor:pointer;display:block;min-height:32px;user-select:none;-webkit-user-select:none}.select2-container .select2-selection--multiple .select2-selection__rendered{display:inline-block;overflow:hidden;padding-left:8px;text-overflow:ellipsis;white-space:nowrap}.select2-container .select2-search--inline{float:left}.select2-container .select2-search--inline .select2-search__field{box-sizing:border-box;border:none;font-size:100%;margin-top:5px;padding:0}.select2-container .select2-search--inline .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-dropdown{background-color:#fff;border:1px solid #aaa;border-radius:4px;box-sizing:border-box;display:block;position:absolute;left:-100000px;width:100%;z-index:1051}.select2-results{display:block}.select2-results__options{list-style:none;margin:0;padding:0}.select2-results__option{padding:6px;user-select:none;-webkit-user-select:none}.select2-results__option[aria-selected]{cursor:pointer}.select2-container--open .select2-dropdown{left:0}.select2-container--open .select2-dropdown--above{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--open .select2-dropdown--below{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-search--dropdown{display:block;padding:4px}.select2-search--dropdown .select2-search__field{padding:4px;width:100%;box-sizing:border-box}.select2-search--dropdown .select2-search__field::-webkit-search-cancel-button{-webkit-appearance:none}.select2-search--dropdown.select2-search--hide{display:none}.select2-close-mask{border:0;margin:0;padding:0;display:block;position:fixed;left:0;top:0;min-height:100%;min-width:100%;height:auto;width:auto;opacity:0;z-index:99;background-color:#fff}.select2-hidden-accessible{border:0!important;clip:rect(0 0 0 0)!important;-webkit-clip-path:inset(50%)!important;clip-path:inset(50%)!important;height:1px!important;overflow:hidden!important;padding:0!important;position:absolute!important;width:1px!important;white-space:nowrap!important}.select2-container--default .select2-selection--single{background-color:#fff;border:1px solid #aaa;border-radius:4px}.select2-container--default .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--default .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:700}.select2-container--default .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--default .select2-selection--single .select2-selection__arrow{height:26px;position:absolute;top:1px;right:1px;width:20px}.select2-container--default .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--default[dir=rtl] .select2-selection--single .select2-selection__clear{float:left}.select2-container--default[dir=rtl] .select2-selection--single .select2-selection__arrow{left:1px;right:auto}.select2-container--default.select2-container--disabled .select2-selection--single{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection--single .select2-selection__clear{display:none}.select2-container--default.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--default .select2-selection--multiple{background-color:#fff;border:1px solid #aaa;border-radius:4px;cursor:text}.select2-container--default .select2-selection--multiple .select2-selection__rendered{box-sizing:border-box;list-style:none;margin:0;padding:0 5px;width:100%}.select2-container--default .select2-selection--multiple .select2-selection__rendered li{list-style:none}.select2-container--default .select2-selection--multiple .select2-selection__placeholder{color:#999;margin-top:5px;float:left}.select2-container--default .select2-selection--multiple .select2-selection__clear{cursor:pointer;float:right;font-weight:700;margin-top:5px;margin-right:10px}.select2-container--default .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove{color:#999;cursor:pointer;display:inline-block;font-weight:700;margin-right:2px}.select2-container--default .select2-selection--multiple .select2-selection__choice__remove:hover{color:#333}.select2-container--default[dir=rtl] .select2-selection--multiple .select2-search--inline,.select2-container--default[dir=rtl] .select2-selection--multiple .select2-selection__choice,.select2-container--default[dir=rtl] .select2-selection--multiple .select2-selection__placeholder{float:right}.select2-container--default[dir=rtl] .select2-selection--multiple .select2-selection__choice{margin-left:5px;margin-right:auto}.select2-container--default[dir=rtl] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--default.select2-container--focus .select2-selection--multiple{border:solid #000 1px;outline:0}.select2-container--default.select2-container--disabled .select2-selection--multiple{background-color:#eee;cursor:default}.select2-container--default.select2-container--disabled .select2-selection__choice__remove{display:none}.select2-container--default.select2-container--open.select2-container--above .select2-selection--multiple,.select2-container--default.select2-container--open.select2-container--above .select2-selection--single{border-top-left-radius:0;border-top-right-radius:0}.select2-container--default.select2-container--open.select2-container--below .select2-selection--multiple,.select2-container--default.select2-container--open.select2-container--below .select2-selection--single{border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--default .select2-search--dropdown .select2-search__field{border:1px solid #aaa}.select2-container--default .select2-search--inline .select2-search__field{background:0 0;border:none;outline:0;box-shadow:none;-webkit-appearance:textfield}.select2-container--default .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--default .select2-results__option[role=group]{padding:0}.select2-container--default .select2-results__option[aria-disabled=true]{color:#999}.select2-container--default .select2-results__option[aria-selected=true]{background-color:#ddd}.select2-container--default .select2-results__option .select2-results__option{padding-left:1em}.select2-container--default .select2-results__option .select2-results__option .select2-results__group{padding-left:0}.select2-container--default .select2-results__option .select2-results__option .select2-results__option{margin-left:-1em;padding-left:2em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-2em;padding-left:3em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-3em;padding-left:4em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-4em;padding-left:5em}.select2-container--default .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option .select2-results__option{margin-left:-5em;padding-left:6em}.select2-container--default .select2-results__option--highlighted[aria-selected]{background-color:#5897fb;color:#fff}.select2-container--default .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic .select2-selection--single{background-color:#f7f7f7;border:1px solid #aaa;border-radius:4px;outline:0;background-image:-webkit-linear-gradient(top,#fff 50%,#eee 100%);background-image:-o-linear-gradient(top,#fff 50%,#eee 100%);background-image:linear-gradient(to bottom,#fff 50%,#eee 100%);background-repeat:repeat-x}.select2-container--classic .select2-selection--single:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--single .select2-selection__rendered{color:#444;line-height:28px}.select2-container--classic .select2-selection--single .select2-selection__clear{cursor:pointer;float:right;font-weight:700;margin-right:10px}.select2-container--classic .select2-selection--single .select2-selection__placeholder{color:#999}.select2-container--classic .select2-selection--single .select2-selection__arrow{background-color:#ddd;border:none;border-left:1px solid #aaa;border-top-right-radius:4px;border-bottom-right-radius:4px;height:26px;position:absolute;top:1px;right:1px;width:20px;background-image:-webkit-linear-gradient(top,#eee 50%,#ccc 100%);background-image:-o-linear-gradient(top,#eee 50%,#ccc 100%);background-image:linear-gradient(to bottom,#eee 50%,#ccc 100%);background-repeat:repeat-x}.select2-container--classic .select2-selection--single .select2-selection__arrow b{border-color:#888 transparent transparent transparent;border-style:solid;border-width:5px 4px 0 4px;height:0;left:50%;margin-left:-4px;margin-top:-2px;position:absolute;top:50%;width:0}.select2-container--classic[dir=rtl] .select2-selection--single .select2-selection__clear{float:left}.select2-container--classic[dir=rtl] .select2-selection--single .select2-selection__arrow{border:none;border-right:1px solid #aaa;border-radius:0;border-top-left-radius:4px;border-bottom-left-radius:4px;left:1px;right:auto}.select2-container--classic.select2-container--open .select2-selection--single{border:1px solid #5897fb}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow{background:0 0;border:none}.select2-container--classic.select2-container--open .select2-selection--single .select2-selection__arrow b{border-color:transparent transparent #888 transparent;border-width:0 4px 5px 4px}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--single{border-top:none;border-top-left-radius:0;border-top-right-radius:0;background-image:-webkit-linear-gradient(top,#fff 0,#eee 50%);background-image:-o-linear-gradient(top,#fff 0,#eee 50%);background-image:linear-gradient(to bottom,#fff 0,#eee 50%);background-repeat:repeat-x}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--single{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0;background-image:-webkit-linear-gradient(top,#eee 50%,#fff 100%);background-image:-o-linear-gradient(top,#eee 50%,#fff 100%);background-image:linear-gradient(to bottom,#eee 50%,#fff 100%);background-repeat:repeat-x}.select2-container--classic .select2-selection--multiple{background-color:#fff;border:1px solid #aaa;border-radius:4px;cursor:text;outline:0}.select2-container--classic .select2-selection--multiple:focus{border:1px solid #5897fb}.select2-container--classic .select2-selection--multiple .select2-selection__rendered{list-style:none;margin:0;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__clear{display:none}.select2-container--classic .select2-selection--multiple .select2-selection__choice{background-color:#e4e4e4;border:1px solid #aaa;border-radius:4px;cursor:default;float:left;margin-right:5px;margin-top:5px;padding:0 5px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove{color:#888;cursor:pointer;display:inline-block;font-weight:700;margin-right:2px}.select2-container--classic .select2-selection--multiple .select2-selection__choice__remove:hover{color:#555}.select2-container--classic[dir=rtl] .select2-selection--multiple .select2-selection__choice{float:right}.select2-container--classic[dir=rtl] .select2-selection--multiple .select2-selection__choice{margin-left:5px;margin-right:auto}.select2-container--classic[dir=rtl] .select2-selection--multiple .select2-selection__choice__remove{margin-left:2px;margin-right:auto}.select2-container--classic.select2-container--open .select2-selection--multiple{border:1px solid #5897fb}.select2-container--classic.select2-container--open.select2-container--above .select2-selection--multiple{border-top:none;border-top-left-radius:0;border-top-right-radius:0}.select2-container--classic.select2-container--open.select2-container--below .select2-selection--multiple{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}.select2-container--classic .select2-search--dropdown .select2-search__field{border:1px solid #aaa;outline:0}.select2-container--classic .select2-search--inline .select2-search__field{outline:0;box-shadow:none}.select2-container--classic .select2-dropdown{background-color:#fff;border:1px solid transparent}.select2-container--classic .select2-dropdown--above{border-bottom:none}.select2-container--classic .select2-dropdown--below{border-top:none}.select2-container--classic .select2-results>.select2-results__options{max-height:200px;overflow-y:auto}.select2-container--classic .select2-results__option[role=group]{padding:0}.select2-container--classic .select2-results__option[aria-disabled=true]{color:grey}.select2-container--classic .select2-results__option--highlighted[aria-selected]{background-color:#3875d7;color:#fff}.select2-container--classic .select2-results__group{cursor:default;display:block;padding:6px}.select2-container--classic.select2-container--open .select2-dropdown{border-color:#5897fb}.select2-search--dropdown .select2-search__field{padding:12px;width:100%;box-sizing:border-box}.select2-container--bootstrap4 .select2-selection--single{height:calc(2.25rem + 2px)!important}.select2-container--bootstrap4 .select2-selection--single .select2-selection__placeholder{color:#757575;line-height:2.25rem!important}.select2-container--bootstrap4 .select2-selection--single .select2-selection__arrow{position:absolute;top:50%;right:3px;width:20px}.select2-container--bootstrap4 .select2-selection--single .select2-selection__arrow b{top:60%;border-color:#ced4da transparent transparent;border-style:solid;border-width:5px 4px 0;width:0;height:0;left:50%;margin-left:-9px;margin-top:-2px;position:absolute}.select2-container--bootstrap4 .select2-selection--single .select2-selection__rendered{line-height:2.25rem}.select2-search--dropdown .select2-search__field{border:1px solid #ced4da;border-radius:.25rem;padding:12px}.select2-results__message{color:#6c757d}.select2-container--bootstrap4 .select2-selection--multiple{min-height:calc(2.25rem + 2px)!important;height:calc(2.25rem + 2px)!important}.select2-container--bootstrap4 .select2-selection--multiple .select2-selection__rendered{-webkit-box-sizing:border-box;box-sizing:border-box;list-style:none;margin:0;padding:0 5px;width:100%}.select2-container--bootstrap4 .select2-selection--multiple .select2-selection__choice{color:#343a40;border:1px solid #bdc6d0;border-radius:.25rem;padding:0;padding-right:5px;cursor:pointer;float:left;margin-top:.3em;margin-right:5px}.select2-container--bootstrap4 .select2-selection--multiple .select2-selection__choice__remove{color:#bdc6d0;font-weight:700;margin-left:3px;margin-right:1px;padding-right:3px;padding-left:3px;float:left}.select2-container--bootstrap4 .select2-selection--multiple .select2-selection__choice__remove:hover{color:#343a40}.select2-container :focus{outline:0}.select2-container--bootstrap4 .select2-selection{border:1px solid #ced4da;border-radius:.25rem!important;width:100%}.select2-container--bootstrap4.select2-container--focus.select2-container--open .select2-selection{border-bottom:none;border-bottom-left-radius:0;border-bottom-right-radius:0}select.is-invalid~.select2-container--bootstrap4 .select2-selection{border-color:#dc3545}select.is-valid~.select2-container--bootstrap4 .select2-selection{border-color:#28a745}.select2-container--bootstrap4 .select2-dropdown{border-color:#ced4da;border-radius:.25rem!important}.select2-container--bootstrap4 .select2-dropdown .select2-results__option[aria-selected=true]{background-color:#e9ecef}.select2-container--bootstrap4 .select2-results__option--highlighted,.select2-container--bootstrap4 .select2-results__option--highlighted.select2-results__option[aria-selected=true]{background-color:#007bff;color:#f8f9fa}.select2-container--bootstrap4 .select2-results__option[role=group]{padding:0}.select2-container--bootstrap4 .select2-results__group{padding:6px;display:list-item;color:#6c757d}.select2-container--bootstrap4 .select2-selection__clear{width:1.2em;height:1.2em;line-height:1.15em;padding-left:.3em;margin-top:.5em;border-radius:100%;background-color:#6c757d;color:#f8f9fa;float:right;margin-right:.3em}.select2-container--bootstrap4 .select2-selection__clear:hover{background-color:#343a40}.select2-container--open .select2-dropdown--below{margin-top:4px!important}.exncedotcom{font-family:Arial,sans-serif;border-radius:4px;padding:0;overflow:hidden;margin:0;border:1px solid #c3c8cf;width:100%;height:100%}.exncedotcom>.collapse:not(.show){display:none}.exncedotcom>.row{padding:5px}.exncedotcom .form-control{border:1px solid #c3c8cf;border-radius:4px;width:99.3%!important;height:30px!important;padding:5px!important}.exncedotcom .leftside{width:160px;border:none!important;height:160px!important;float:left!important}.exncedotcom .rightside{width:calc(100% - 164px);height:100%;float:left;display:block!important}.exncecoinitem{display:block;vertical-align:middle;align-content:center;align-items:center;align-self:center;padding:4px}.exncecoinname{display:block;float:left;font-size:14px;font-weight:700;margin-left:5px}.exncecoinsymbol{margin-left:5px;color:#c9c9c9}.exncecoinlogo{margin-top:0;float:left;align-content:center;vertical-align:middle;display:block;margin-right:5px!important;width:28px!important;height:28px!important;max-height:28px!important;min-height:28px!important}';
        $htmlData = '<div class="exncedotcom"> <div class="row"> <div class="col"> <select class="form-control" name="exnceAssetList" id="exnceAssetList"> </select> </div></div><div id="exncePaymentDetails" name="exncePaymentDetails" class="row collapse"> <div class="col"> <div class="leftside"> <img id="exncePaymentQR" name="exncePaymentQR" width="160px" height="160px" src="https://cdn.qrstuff.com/images/default_qrcode.png"/> </div><div class="rightside"> <label for="exnceWalletAddress">Wallet Address</label> <input id="exnceWalletAddress" name="exnceWalletAddress" type="text" class="form-control" value=""> </div></div></div></div>';

        $message = "Lütfen %s isimli siteye kayıt olduğunuz eposta adresi ile %s\'a kayıt olarak cüzdan tanımlayınız!";
        $message = sprintf($message,$site,$exncecom);
        if(is_array($walletList)==false){
            $htmlData = "<p>".$message."</p>";
        }


        $data = array("wallets"=>json_encode(array("results"=>$walletList)),
        "css"=>"<style type=\'text/css\'>".$cssData."</style>","html"=>$htmlData,"render"=>$renderObj);
        return response()->view("api.v1.profile",$data)->header('Content-Type', 'application/javascript');;
    }

    public function checkApiEntries(Request $request){
        $publicKey = $request->header("publicKey");
        $secretKey = $request->header("secretKey");
        $is_success = false;
        $apiEntry = null;
        $message = _i("Bilinmeyen Komut!");
        if(!$publicKey){
            $is_success = false;
            $message = _i("Geçersiz Public Key!");
        } elseif (!$secretKey){
            $is_success = false;
            $message = _i("Geçersiz Secret Key!");
        }
        if(($publicKey) && ($secretKey)){
            $apiEntry = UserApiKey::where("public_key",$publicKey)->get()->first();
            if(!$apiEntry){
                $is_success = false;
                $message = _i("Geçersiz Secret Key veya Public Key!");
            }else{
                $encryptedSecret = md5($apiEntry->secret_key.getAPISecretKeyPrefix());
                if($encryptedSecret != $secretKey){
                    $is_success = false;
                    $message = _i("Geçersiz Secret Key!").$encryptedSecret."--".$secretKey;
                }else{
                    if($apiEntry->can_access_orderbook==false){
                        $is_success = false;
                        $message = _i("Bu api anahtarının tahtaya erişim izni yok!");
                    }else{
                        $is_success = true;
                        $message = _i("İşlem başarılı!");
                    }
                }
            }
        }
        return ["status"=>true, "success"=>$is_success, "message"=>$message,"apiEntry"=>$apiEntry];
    }

    public function openOrders(Request $request){
        $is_success = false;
        $message = "";
        $apiEntry = null;
        $response = self::checkApiEntries($request);
        $is_success = $response["success"];
        $message = $response["message"];
        $apiEntry = $response["apiEntry"];

        $open_orders = null;
        $bids = [];
        $asks = [];
        $orders = [];
        if($is_success == true){
            $user = $apiEntry->user;
            $open_orders = $user->transactions()->where("status","<",2)->orderBy("id", "desc")->get();
            foreach($open_orders as &$order){
                //$order->type = $order->type == "buy" ? "bids" : "asks";
                $order->orderId = $order->id;
                $order->clientOrderId = $order->ref_code;
                $order->symbol = sprintf("%s/%s", $order->currency->symbol, $order->coin->symbol);
                $order->time = $order->created_at->timestamp;
                $order->updateTime = $order->updated_at->timestamp;
                $order->origQty = $order->amount;
                $order->executedQty = $order->completed_amount;
                $order->side = strtoupper($order->type);
                unset($order->amount);
                unset($order->completed_amount);
                unset($order->updated_at);
                unset($order->currency);
                unset($order->coin);
                if($order->type == "buy"){
                    unset($order->type);
                    $order->type="LIMIT";
                    $bids[] = $order;
                }else{
                    unset($order->type);
                    $order->type="LIMIT";
                    $asks[] = $order;
                }
            }
           $orders = ["bids"=>$bids, "asks"=>$asks];

        }
        $data = ["status"=>true, "success"=>$is_success, "message"=>$message,"orders"=>$orders];

        return $data;
    }

    public function createBuy(Request $request){
        $is_success = false;
        $message = "";
        $apiEntry = null;
        $response = self::checkApiEntries($request);
        $is_success = $response["success"];
        $message = $response["message"];
        $apiEntry = $response["apiEntry"];
        $open_orders = null;
        $bids = [];
        $asks = [];
        $orders = [];
        if($is_success == true){
            $symbol = $request->post("symbol");
            $side = $request->post("side");
            $type = $request->post("type");
            $quantity = $request->post("quantity");
            $price = $request->post("price");
            $currencies = explode("/", $symbol);
            if(sizeof($currencies)!=2){
                $is_success = false;
                $message = _("Alis islemine ait symbol parametresi hatalidir!");
            }else if((!$side) || ($side != "BUY")){
                $is_success = false;
                $message = _("Alis islemine ait side parametresi hatalidir!");
            }else if((!$type) || ($type != "LIMIT")){
                $is_success = false;
                $message = _("Alis islemine ait type parametresi hatalidir!");
            }elseif((!$price) || ($price == "")){
                $is_success = false;
                $message = _("Alis islemine ait price parametresi hatalidir!");
            }elseif((!$quantity) || ($quantity == "")){
                $is_success = false;
                $message = _("Alis islemine ait quantity parametresi hatalidir!");
            }
            if($is_success == true){
                $request->request->add(["edtPriceBuy"=>$price]);
                $request->request->add(["edtAmountBuyInt"=>$quantity]);
                $request->request->add(["edtBuyCoin"=>$currencies[1]]);
                $request->request->add(["edtBuyCurrency"=>$currencies[0]]);
                $request->request->add(["edtSummaryBuy"=>$quantity * $price]);
                $request->request->add(["edtFeeBuy"=>$quantity * 0.001]);
                $market = new \App\Http\Controllers\Personal\MarketController();
                return $market->createBuy($request,$apiEntry->user()->get()->first());
            }
        }
        $data = ["status"=>true, "success"=>$is_success, "message"=>$message];
        return $data;
    }

    public function createSell(Request $request){
        $is_success = false;
        $message = "";
        $apiEntry = null;
        $response = self::checkApiEntries($request);
        $is_success = $response["success"];
        $message = $response["message"];
        $apiEntry = $response["apiEntry"];
        $open_orders = null;
        $bids = [];
        $asks = [];
        $orders = [];
        if($is_success == true){
            $symbol = $request->post("symbol");
            $side = $request->post("side");
            $type = $request->post("type");
            $quantity = $request->post("quantity");
            $price = $request->post("price");
            $currencies = explode("/", $symbol);
            if(sizeof($currencies)!=2){
                $is_success = false;
                $message = _("Satis islemine ait symbol parametresi hatalidir!");
            }else if((!$side) || ($side != "SELL")){
                $is_success = false;
                $message = _("Satis islemine ait side parametresi hatalidir!");
            }else if((!$type) || ($type != "LIMIT")){
                $is_success = false;
                $message = _("Satis islemine ait type parametresi hatalidir!");
            }elseif((!$price) || ($price == "")){
                $is_success = false;
                $message = _("Satis islemine ait price parametresi hatalidir!");
            }elseif((!$quantity) || ($quantity == "")){
                $is_success = false;
                $message = _("Satis islemine ait quantity parametresi hatalidir!");
            }
            if($is_success == true){
                $request->request->add(["edtPriceSell"=>$price]);
                $request->request->add(["edtAmountSellInt"=>$quantity]);
                $request->request->add(["edtSellCoin"=>$currencies[1]]);
                $request->request->add(["edtSellCurrency"=>$currencies[0]]);
                $request->request->add(["edtSummarySell"=>$quantity * $price]);
                $request->request->add(["edtFeeSell"=>(($quantity * $price) * 0.001)]);
                $market = new \App\Http\Controllers\Personal\MarketController();
                return $market->createSell($request,$apiEntry->user()->get()->first());
            }
        }
        $data = ["status"=>true, "success"=>$is_success, "message"=>$message];
        return $data;
    }


    public function getExchangeInfo(Request $request){


        $symbols = [];

            $pairs = CoinCoin::with(["pair","coin","price"])->orderBy("coin_coin.display_index","desc")->get();

            foreach($pairs as $pair) {
                $coin = $pair->pair;
                $currency = $pair->coin;
                if(!$currency->ieo){
                    $symbols[] = array(
                        "symbol"=>$currency->symbol . "/" . $coin->symbol,
                        "status"=> "TRADING",
                        "baseAsset"=>$currency->symbol,
                        "baseAssetPrecision"=> 8,
                        "quoteAsset"=> $coin->symbol,
                        "quotePrecision"=> 8,
                        "orderTypes"=>array("LIMIT"),
                        "icebergAllowed"=>false,
                        "filters" => array()
                    );
                    }
                }

        $data = ["status"=>true,
            "success"=>true,
            "message" => "Exchange information has been listed successfuly...",
            "timezone"=>"UTC",
            "serverTime" => time(),
            "rateLimits" => array(),
            "exchangeFilters"=>array(),
            "symbols"=>$symbols];

        return response()->json($data);
    }


    public function publicDepthOrderBook(Request $request){


        $symbol = $request->get("symbol");
        $limit = $request->get("limit");

        $symbol = $symbol ? $symbol : null;
        $limit = $limit ? $limit : 0;
        $limit = is_numeric($limit) ? $limit : 100;

        $success = true;

        if($limit == 0){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Illegal characters found in parameter 'limit'; Valid limits [5, 10, 20, 50, 100, 500, 1000]"];
            return response()->json($data);
        }

        if(!$symbol){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $pairs = explode("-", $symbol);

        if(count($pairs)==1){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }



        $coinSymbol = $pairs[0];
        $currencySymbol = $pairs[1];

        if((strlen($coinSymbol)>10) || (strlen($currencySymbol)>10)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $coin= Coin::where("symbol",$coinSymbol)->first();
        $currency = Coin::where("symbol",$currencySymbol)->first();

        if((!$coin) || (!$currency)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }


        $checkPair = CoinCoin::where("coin_id",$currency->id)->where("pair_id",$coin->id)->first();
        if(!$checkPair){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }


        $buyTransactions = Transaction::select("transactions.*"
            ,DB::raw('SUM(remaining_amount) as totalAmount')
            ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
            ->where("coin_id",$currency->id)
            ->where("currency_id",$coin->id)
            ->where("transaction_type",0)
            ->where("status","<",2)
            ->groupBy("price")
            ->orderBy("price","desc")
            ->take($limit)->get();



        $sellTransactions =  Transaction::select("transactions.*"
            ,DB::raw('SUM(remaining_amount) as totalAmount')
            ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
            ->where("coin_id",$currency->id)
            ->where("currency_id",$coin->id)
            ->where("transaction_type",1)
            ->where("status","<",2)
            ->groupBy("price")
            ->orderBy("price","desc")
            ->get()->reverse()->take($limit);

        $bids = [];
        $asks = [];

        $deepBuy = 0;
        $calculatedBuy = 0;
        foreach($buyTransactions as &$buy){
            $buy->price = sprintf('%.'.$coin->decimals.'f', floatval($buy->price));
            $buy->totalAmount =  sprintf('%.'.$currency->decimals.'f', floatval($buy->totalAmount));
            $buy->price = number_format($buy->price, $coin->decimals, ".", "");

            $bids[] = array($buy->price,$buy->totalAmount);


        }

        $deepSell=0;
        $calculatedSell = 0;
        foreach($sellTransactions as &$sell){
            $sell->price = sprintf('%.'.$coin->decimals.'f', floatval($sell->price));
            $sell->totalAmount =   sprintf('%.'.$currency->decimals.'f', floatval($sell->totalAmount));

            $sell->price = number_format($sell->price, $currency->decimals, ".", "");

            $asks[] = array($sell->price,$sell->totalAmount);

        }

        $orders = ["bids"=>$bids, "asks"=>$asks];
        $message = "Successfuly";
        $data = ["status"=>true, "success"=>$success, "message"=>$message,"data"=>$orders];

        return $data;
    }


    public function cancelOrder(Request $request){

    }


    public function sendPing(Request $request){
        $data = ["status"=>true,
            "success"=>true,
            "message" => "Pong"];
        return response()->json($data);
    }

    public function sendTime(Request $request){
        $data = ["status"=>true,
            "success"=>true,
            "timezone"=>"UTC",
            "serverTime" => time()
        ];
        return response()->json($data);
    }

    public function historicalTrades(Request $request){
        $symbol = $request->get("symbol");
        $limit = $request->get("limit");
        $fromId = $request->get("fromId");

        $symbol = $symbol ? $symbol : null;
        $limit = $limit ? $limit : 5;
        $limit = is_numeric($limit) ? $limit : 5;
        $fromId = is_numeric($fromId) ? $fromId : 0;


        $success = true;

        if(($limit == 0)||($limit>1000)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Illegal characters found in parameter 'limit'; Valid limits [500, MAX:1000]"];
            return response()->json($data);
        }

        if(!$symbol){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $pairs = explode("-", $symbol);

        if(count($pairs)==1){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }



        $coinSymbol = $pairs[0];
        $currencySymbol = $pairs[1];

        if((strlen($coinSymbol)>10) || (strlen($currencySymbol)>10)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $coin= Coin::where("symbol",$coinSymbol)->first();
        $currency = Coin::where("symbol",$currencySymbol)->first();

        if((!$coin) || (!$currency)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }


        $checkPair = CoinCoin::where("coin_id",$currency->id)->where("pair_id",$coin->id)->first();
        if(!$checkPair){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $market_history = TransactionDetail:: select('id',"price",DB::raw("amount as qty"), DB::raw('UNIX_TIMESTAMP(created_at) as time'))

            ->where("coin_id",$currency->id)
            ->where("currency_id",$coin->id)
            ->where("type", "buy");

        if($fromId > 0){
            $market_history = $market_history->where("id",">=",$fromId);
        }
        $data = $market_history->orderBy("id", "desc")->take($limit)->get();

        return response()->json($data);
    }


    public function publicOrderBook(Request $request){


        $symbol = $request->get("symbol");
        $limit = $request->get("limit");

        $symbol = $symbol ? $symbol : null;
        $limit = $limit ? $limit : 0;
        $limit = is_numeric($limit) ? $limit : 100;

        $success = true;

        if($limit == 0){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Illegal characters found in parameter 'limit'; Valid limits [5, 10, 20, 50, 100, 500, 1000]"];
            return response()->json($data);
        }

        if(!$symbol){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $pairs = explode("-", $symbol);

        if(count($pairs)==1){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }



        $coinSymbol = $pairs[0];
        $currencySymbol = $pairs[1];

        if((strlen($coinSymbol)>10) || (strlen($currencySymbol)>10)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }

        $coin= Coin::where("symbol",$coinSymbol)->first();
        $currency = Coin::where("symbol",$currencySymbol)->first();

        if((!$coin) || (!$currency)){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }


        $checkPair = CoinCoin::where("coin_id",$currency->id)->where("pair_id",$coin->id)->first();
        if(!$checkPair){
            $success = false;
            $data = ["status"=>true, "success"=>$success, "message"=>"Invalid symbol."];
            return response()->json($data);
        }


        $buyTransactions = Transaction::select("transactions.*"
            ,DB::raw('SUM(remaining_amount) as totalAmount')
            ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
            ->where("coin_id",$currency->id)
            ->where("currency_id",$coin->id)
            ->where("transaction_type",0)
            ->where("status","<",2)
            ->groupBy("price")
            ->orderBy("price","desc")
            ->take($limit)->get();



        $sellTransactions =  Transaction::select("transactions.*"
            ,DB::raw('SUM(remaining_amount) as totalAmount')
            ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
            ->where("coin_id",$currency->id)
            ->where("currency_id",$coin->id)
            ->where("transaction_type",1)
            ->where("status","<",2)
            ->groupBy("price")
            ->orderBy("price","desc")
            ->get()->reverse()->take($limit);

        $bids = [];
        $asks = [];

        $deepBuy = 0;
        $calculatedBuy = 0;
        foreach($buyTransactions as &$buy){
            $buy->price = sprintf('%.'.$coin->decimals.'f', floatval($buy->price));
            $buy->totalAmount =  sprintf('%.'.$currency->decimals.'f', floatval($buy->totalAmount));
            $buy->price = number_format($buy->price, $coin->decimals, ".", "");

            $bids[] = array($buy->price,$buy->totalAmount);


        }

        $deepSell=0;
        $calculatedSell = 0;
        foreach($sellTransactions as &$sell){
            $sell->price = sprintf('%.'.$coin->decimals.'f', floatval($sell->price));
            $sell->totalAmount =   sprintf('%.'.$currency->decimals.'f', floatval($sell->totalAmount));

            $sell->price = number_format($sell->price, $currency->decimals, ".", "");

            $asks[] = array($sell->price,$sell->totalAmount);

        }

        $orders = ["timestamp"=>time(), "bids"=>$bids, "asks"=>$asks];
        $message = "Operation is successful";
        $data = ["status"=>true, "success"=>$success, "message"=>$message,"data"=>$orders];

        return $data;
    }

}