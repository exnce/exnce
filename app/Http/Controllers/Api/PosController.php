<?php

namespace App\Http\Controllers\Api;

use App\Transaction;
use App\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Http\Controllers\Personal\MarketController;
use App\Coin;
use App\Http\Controllers\Api\MarketController as ApiMarketController;

class PosController extends Controller
{
/*    public function isLogged($email, $password)
    {
        $user = User::whereEmail($email)->first();
        if (!$user) return null;
        if (Hash::check($password, $user->password)) {
            return $user;
        } else return null;
    }

    public function getUserInfo(User $user)
    {

        if ($user) {

        }
        $userdata = $user->toArray();
        return array("user_info" => $userdata);

    }

    public function loginUser(Request $request)
    {
        //todo: oauth login ve response token
        $data = [];
        $hasError = false;
        $szMessage = "Kullanıcı giris işlemi başarı ile gerçekleşti.";
        return ["status" => true, "success" => !$hasError, "type" => "info", "title" => "İşlem Başarılı", "message" => $szMessage, "data" => $data];
    }*/

    public function marketBuy(Request $request)
    {

        $market = new ApiMarketController();
        $response = $market->createBuy($request);

        $isSuccess = $response["success"];
        $szType = $response["type"];
        $szTitle = $response["title"];
        $szMessage = $response["message"];


        return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => []];
    }

    public function tickerData(Request $request)
    {

        $market = new MarketController();
        $pairs = $market->getPairs();
        $data = [];

        foreach ($pairs as $pair) {
            $coin = Coin::find($pair["coin_id"]);
            $currency = Coin::find($pair["currency_id"]);

            $startDate = Carbon::now()->subDay();
            $endDate = Carbon::now();

            $high = 0;
            $low = 0;

            $prices = TransactionDetail::where("type", "buy")
                ->whereBetween("updated_at", [$startDate, $endDate])
                ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                ->selectRaw('min(price) as minPrice, max(price) as maxPrice')->first(); //lastPrice ve firstPrice burada alınabilir mi bakılacak

            if($prices->maxPrice) $high = $prices->maxPrice;
            if($prices->minPrice) $low = $prices->minPrice;



            $lastPrice = 0;
            $firstPrice = 0;

            $transactions = TransactionDetail::where("type", "buy")
                ->whereBetween("updated_at", [$startDate, $endDate])
                ->where("coin_id",$coin->id)->where("currency_id",$currency->id)->get();

            if($transactions->count() > 0) {
                $lastPrice = $transactions->last()->price;
                $firstPrice = $transactions->first()->price;
            }



            $buyTransactions = Transaction::where("type", "buy")
                ->whereIn("status", [0,1])
                ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                ->selectRaw('max(price) as ask')->first();

            $sellTransactions = Transaction::where("type", "sell")
                ->whereIn("status", [0,1])
                ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                ->selectRaw('min(price) as bid')->first();

            $data[] = [
                "pair" => $pair["name"],
                "high" => $high,
                "last" => $lastPrice,        //son fiyat
                "timestamp" => time(),
                "bid" => $sellTransactions->bid,         //satış için en uygun fiyat
                "volume" => $coin->volume($pair["currency_id"]),
                "low" => $low,
                "ask" => $buyTransactions->ask,         //alış için en uygun fiyat
                "open" => $firstPrice,        //ilk fiyat
    //            "average" => 0,
    //            "daily" => 0,
    //            "dailyPercent" => 0,
                "coin" => $pair["coin"],
                "dest" => $pair["currency"],
            ];
        }

        $hasError = false;
        $szMessage = "Ticker Verisi Eklendi.";

        return ["status" => true, "success" => !$hasError, "type" => "info", "title" => "İşlem Başarılı", "message" => $szMessage, "data" => $data];
    }

    public function pairData(Request $request)
    {

        $market = new MarketController();
        $reqPair = explode("/", $request->post("pair"));
        $pair = $market->getPair($reqPair[1], $reqPair[0]);

        $coin = Coin::find($pair["coin_id"]);
        $currency = Coin::find($pair["currency_id"]);

        $startDate = Carbon::now()->subDay();
        $endDate = Carbon::now();

        $high = 0;
        $low = 0;

        $prices = TransactionDetail::where("type", "buy")
            ->whereBetween("updated_at", [$startDate, $endDate])
            ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
            ->selectRaw('min(price) as minPrice, max(price) as maxPrice')->first(); //lastPrice ve firstPrice burada alınabilir mi bakılacak

        if($prices->maxPrice) $high = $prices->maxPrice;
        if($prices->minPrice) $low = $prices->minPrice;



        $lastPrice = 0;
        $firstPrice = 0;

        $transactions = TransactionDetail::where("type", "buy")
            ->whereBetween("updated_at", [$startDate, $endDate])
            ->where("coin_id",$coin->id)->where("currency_id",$currency->id)->get();

        if($transactions->count() > 0) {
            $lastPrice = $transactions->last()->price;
            $firstPrice = $transactions->first()->price;
        }



        $buyTransactions = Transaction::where("type", "buy")
            ->whereIn("status", [0,1])
            ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
            ->selectRaw('max(price) as ask')->first();

        $sellTransactions = Transaction::where("type", "sell")
            ->whereIn("status", [0,1])
            ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
            ->selectRaw('min(price) as bid')->first();

        $data[] = [
            "pair" => $pair["name"],
            "high" => $high,
            "last" => $lastPrice,        //son fiyat
            "timestamp" => time(),
            "bid" => $sellTransactions->bid,         //satış için en uygun fiyat
            "volume" => $coin->volume($pair["currency_id"]),
            "low" => $low,
            "ask" => $buyTransactions->ask,         //alış için en uygun fiyat
            "open" => $firstPrice,        //ilk fiyat
//            "average" => 0,
//            "daily" => 0,
//            "dailyPercent" => 0,
            "coin" => $pair["coin"],
            "dest" => $pair["currency"],
        ];

        $hasError = false;
        $szMessage = "Pair Verisi Alindi.";

        return ["status" => true, "success" => !$hasError, "type" => "info", "title" => "İşlem Başarılı", "message" => $szMessage, "data" => $data];
    }

    public function coinData(Request $request)
    {
        $coins = Coin::select("id", "name", "symbol", "slug", "decimals", "dec_point", "thousands_sep")->where("status", 0)->orderBy("name", "asc")->get();

        foreach ($coins as &$coin) {
            $usdtPrice = Coin::find(11)->lastPrice($coin->id);
            if(!$usdtPrice) $usdtPrice = 0;

            $coin->image = asset(sprintf("images/coins/%s.png", $coin->slug));
            $coin->price = $usdtPrice;
        }

        $data = $coins;

        $hasError = false;
        $szMessage = "Coin verisi alindi.";

        return ["status" => true, "success" => !$hasError, "type" => "info", "title" => "İşlem Başarılı", "message" => $szMessage, "data" => $data];
    }

    public function invalidRequest(Request $request, $message)
    {
        return ["status" => true, "success" => false, "type" => "error", "title" => "Geçersiz İşlem", "message" => $message];
    }


    public function endPoint(Request $request)
    {

        $data = [];
        $hasAction = $request->has("action");

        if ($hasAction == true) {
            $action = $request->post("action");
            switch ($action) {
/*                case "LOGIN":
                    $data = $this->loginUser($request);
                    break;*/
                case "TICKER":
                    $hasPair = $request->has("pair");
                    if ($hasPair) {
                        $data = $this->pairData($request);
                    } else {
                        $data = $this->tickerData($request);
                    }
                    break;

                case "COINS":
                    $data = $this->coinData($request);
                    break;

                default:
                    $data = $this->invalidRequest($request, "Undefined Request!");
                    break;
            }
        } else {
            $data = $this->invalidRequest($request, "Invalid Request!");
        }

        return response()->json($data, 200);
    }
}
