<?php

namespace App\Http\Controllers\Api;

use App\Jobs\SendVerificationEmail;
use App\SecurityPhoto;
use App\User;
use App\UserSetting;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\MarketController;

class AuthController extends Controller
{
    use Authorizable;
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request)
    {

//
//        $validator = Validator::make($request->all(), [
//            'name' => 'required|string',
//            'email' => 'required|string|email|unique:users',
//            'password' => 'required|string|confirmed'
//        ]);
//
//        if ($validator->fails()) {
//            $isSuccess = false;
//            $szType = "error";
//            $szTitle = _i("İşlem Başarısız");
//            $szMessage = _i("Kayit esnasinda hata.");
//
//            return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => $validator->messages()];
//        } else {
//            $szPassword=bcrypt($request->password);
//            $privateKey=generatePrivateKey($szPassword);
//            $publicKey=generatePublicKey($privateKey);
//            $emailTokenKey=generatePrivateKey(base64_encode($request->email));
//
//            $user = User::create([
//                'name' => $request->name,
//                'email' => $request->email,
//                'password' => $szPassword,
//                'email_token' => $emailTokenKey,
//                'security_photo' => SecurityPhoto::inRandomOrder()->first()->id,
//                'private_key' => $privateKey,
//                'public_key' => $publicKey
//            ]);
//
//            UserSetting::create([
//                'user_id' => $user->id,
//            ]);
//
//            dispatch(new SendVerificationEmail($user));
//
//            $isSuccess = true;
//            $szType = "info";
//            $szTitle = _i("İşlem Başarılı");
//            $szMessage = _i("Kayit işlemi başarılı.");
//
//            return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => []];
//        }
    }

    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials)) {
            $isSuccess = false;
            $szType = "error";
            $szTitle = _i("İşlem Başarısız");
            $szMessage = _i("Giris yapilamadi.");

            return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => []];
        }


        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $data =  response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);

        $isSuccess = true;
        $szType = "info";
        $szTitle = _i("İşlem Başarılı");
        $szMessage = _i("Giris işlemi başarılı.");

        return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => $data];
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        $isSuccess = true;
        $szType = "info";
        $szTitle = "İşlem Başarılı";
        $szMessage = "Logout işlemi başarılı.";

        return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => []];
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        $user = $request->user();

        $isSuccess = true;
        $szType = "info";
        $szTitle = "İşlem Başarılı";
        $szMessage = "User bilgisi alindi.";

        $user->wallets = $user->wallets()->get();
        foreach ($user->wallets as &$wallet) {
            $wallet->available_balance = $user->user_balance(false,$wallet->coin_id,false,true);
        }

        $user->cards = $user->cards()->get();

        return ["status" => true, "success" => $isSuccess, "type" => $szType, "title" => $szTitle, "message" => $szMessage, "data" => $user];
    }









}
