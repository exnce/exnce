<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 3/31/19
 * Time: 6:22 PM
 */

namespace App\Http\Controllers\Api;
use App\Coin;
use App\Http\Controllers\Controller;
use App\Transaction;
use App\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ApiController extends Controller
{

    public function loadTickerData(Request $request){


            $market = new \App\Http\Controllers\Personal\MarketController();
            $pairs = $market->getPairs();
            $data = [];

            foreach ($pairs as $pair) {
                $coin = Coin::find($pair["coin_id"]);
                $currency = Coin::find($pair["currency_id"]);

                $startDate = Carbon::now()->subDay();
                $endDate = Carbon::now();

                $high = 0;
                $low = 0;

                $prices = TransactionDetail::where("type", "buy")
                    ->whereBetween("updated_at", [$startDate, $endDate])
                    ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                    ->selectRaw('min(price) as minPrice, max(price) as maxPrice')->first(); //lastPrice ve firstPrice burada alınabilir mi bakılacak

                if($prices->maxPrice) $high = $prices->maxPrice;
                if($prices->minPrice) $low = $prices->minPrice;



                $lastPrice = 0;
                $firstPrice = 0;

                $transactions = TransactionDetail::where("type", "buy")
                    ->whereBetween("updated_at", [$startDate, $endDate])
                    ->where("coin_id",$coin->id)->where("currency_id",$currency->id)->get();

                if($transactions->count() > 0) {
                    $lastPrice = $transactions->last()->price;
                    $firstPrice = $transactions->first()->price;
                }



                $buyTransactions = Transaction::where("type", "buy")
                    ->whereIn("status", [0,1])
                    ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                    ->selectRaw('max(price) as ask')->first();

                $sellTransactions = Transaction::where("type", "sell")
                    ->whereIn("status", [0,1])
                    ->where("coin_id",$coin->id)->where("currency_id",$currency->id)
                    ->selectRaw('min(price) as bid')->first();

                $data[] = [
                    "pair" => $pair["name"],
                    "high" => $high,
                    "last" => $lastPrice,        //son fiyat
                    "timestamp" => time(),
                    "bid" => $sellTransactions->bid == null ? 0.00 : $sellTransactions->bid,         //satış için en uygun fiyat
                    "volume" => $coin->volume($pair["currency_id"]),
                    "low" => $low,
                    "ask" => $buyTransactions->ask == null ? 0.00 : $buyTransactions->ask,
                    "open" => $firstPrice,
                    "change"=> $coin->change($pair["currency_id"]),
                    "coin" => $pair["coin"],
                    "dest" => $pair["currency"],
                ];
            }

            $time = time();
            $hasError = false;
            $szMessage = _i("Ticker has been listed successfuly.");

            return ["status" => true, "success" => !$hasError,"time"=>$time, "type" => "info", "title" => _i("Success"), "message" => $szMessage, "data" => $data];


    }



}