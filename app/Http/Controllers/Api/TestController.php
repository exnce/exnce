<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/20/19
 * Time: 4:41 AM
 */

namespace App\Http\Controllers\Api;


use App\Coin;
use App\CoinCoin;
use App\CoinDeposit;
use App\Transaction;
use App\User;
use App\Wallet;
use App\WalletAddress;
use Auth;
use Carbon\Carbon;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
 use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class TestController
{
    public function index(Request $request){
        $statusList = getCoinsStatusInformation(true);
        $responseData = "";
        foreach ($statusList as $statusItem){
            $responseData .=
                "<b>Asset   :</b>" . $statusItem->symbol . "/" . $statusItem->symbol . PHP_EOL.
                "<b>Deposit :</b>" . ($statusItem->can_deposit ? "Active" : "Offline").
                "<b>Withdrawal :</b>" .($statusItem->can_withdraw ? "Active" : "Offline") . PHP_EOL.PHP_EOL.
                "<b>Trade :</b>" . ($statusItem->can_trade ? "Active" : "Offline") . PHP_EOL.PHP_EOL.
                "<b>Note :</b>" . ($statusItem->maintenance_note) . PHP_EOL.PHP_EOL;
        }

        dd($responseData);

        $wallets = CoinDeposit::select("address","id")->where("memo_tag","-")->get();
        $coins = Coin::where("contract","<>","")->get();

        $data = ["wallets" => $wallets,
            "coins"=>$coins

        ];
        return view("test.balance", $data);



    }


}