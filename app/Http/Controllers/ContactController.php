<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Rules\Captcha;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rules\BlackListEmail;
use Illuminate\Support\Facades\Validator;
use App\Message;

class ContactController extends Controller
{
    public function index(){
        return  view("site.contact");
    }
    protected function validator(array $data)
    {
        $validator =  Validator::make($data, [

            'fname' => 'required|string|max:255',
            'lname' => 'required|string|max:255',
            'email' => ['required','string','email','max:255',new BlackListEmail],
            'message' => 'required|string',
            "g-recaptcha-response" => new Captcha()

        ]);

        return $validator;

    }

    public function sendMessage(Request $request){
        $validator = $this->validator($request->all());


        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();


        } else {

            Contact::create(array("name" => $request->input("fname"),
                "lastname"=>$request->input("lname"),
                "email" =>  $request->input("email"),
                "message" =>  $request->input("message"),
                "status" => 0
                ));

            return redirect()->back()->with("is_success","DONE");
        }

    }
}
