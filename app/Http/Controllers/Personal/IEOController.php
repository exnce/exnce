<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 5/18/19
 * Time: 5:18 PM
 */

namespace App\Http\Controllers\Personal;


use Akaunting\Money\Money;
use App\Coin;
use App\CoinCoin;
use App\CoinCoins;
use App\Currency;
use App\Events\NewMarketEvent;
use App\Http\Controllers\Controller;
use App\Jobs\CompleteTransaction;
use App\Transaction;
use App\TransactionDetail;
use App\User;
use App\Wallet;
use JavaScript;
use Jenssegers\Agent\Agent;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;
use App\CompletedTransaction;
use Illuminate\Support\Arr;
use App\UserFavorite;


class IEOController
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {

        $user = auth()->user();
        if($user){

            $userFavorites = $user->favorites()->get()->pluck("pair")->toArray();
            $pairLists = Coin::where("symbol","ETH")->get();




            $baseListCoins = [];
            foreach($pairLists as $pair){
                if($pair->symbol=="ETH"){
                $baseListCoins[$pair->id] = $pair->symbol;
                }
            }

            $pairLists = Coin::where("ieo",true)->where("symbol","<>","ORX")->where("symbol","<>","ALDX")->where("symbol","<>","KZE")->get();
            $data = [
                "activeCoin"=>null,
                "activeCurrency"=>null,
                "pairList"=>$pairLists,
                "baseCoins"=>$baseListCoins,
                "favorites"=>$userFavorites,
                "metaTags"=>getSEOMetaTags(null,null,"ieo")

            ];

            $viewType = "personal.ieo.index";
            return view($viewType,$data);
        }else{
            return redirect()->route("login");
        }
    }

    public function pair(Request $request, $currency = null, $coin = null,$user = null)
    {
        if(!$user){
            $user = auth()->user();
        }

        if($user){
            $activeCoinID = session("activeCoinID");
            $activeCurrencyID = session("activeCurrencyID");

            if($currency && $coin) {
                $activeCoin = Coin::where("symbol", $coin)->get()->first();
                if(!$activeCoin) return redirect()->route("market");
                $activeCurrency =Coin::where("symbol", $currency)->get()->first();
                if(!$activeCurrency) return redirect()->route("market");

            } else if($activeCoinID && $activeCurrencyID) {
                $activeCoin = Coin::where("id", $activeCoinID)->get()->first();
                $activeCurrency = Coin::where("id", $activeCurrencyID)->get()->first();
                if(!$activeCurrency) return redirect()->route("market");

            } else {
                $activeCoin = Coin::where("symbol", getDefaultBaseCoin())->get()->first();
                $activeCurrency = Coin::where("symbol", getDefaultSubCoin())->get()->first();
            }



            if($activeCurrency->symbol == "ORX"){
                return redirect()->route("pair",["XLM","ETH"]);
            }

            if(($currency && $coin) || ($activeCoinID && $activeCurrencyID)) {

                session([
                    "activeCoinID"=>$activeCoin->id,
                    "activeCurrencyID"=>$activeCurrency->id,
                ]);
            }



            if( $activeCoin->processing_fee == 0 ) {
                $activeCurrency->processing_fee = 0;
            } else if( $activeCurrency->processing_fee == 0 ) {
                $activeCoin->processing_fee = 0;
            }

            $userFavorites = $user->favorites()->get()->pluck("pair")->toArray();
            $pairLists = CoinCoin::with(["pair"])->groupBy("coin_id")->orderBy("coin_coin.display_index","desc")->get();
            $baseListCoins = [];
            foreach($pairLists as $pair){
                if($pair->pair->symbol == "ETH"){
                $baseListCoins[$pair->pair->id] = $pair->pair->symbol;
                }
            }



            $data = [
                "activeCoin"=>$activeCoin,
                "activeCurrency"=>$activeCurrency,
                "pairList"=>$this->getPairs(),
                "baseCoins"=>$baseListCoins,
                "favorites"=>$userFavorites,
                "metaTags"=>getSEOMetaTags($activeCoin,$activeCurrency,"market")
            ];


            $agent = new Agent();
            if($agent->isDesktop()){
                $viewType = "personal.ieo.detail";
            }else{
                $viewType = "personal.ieo.detail-mobile";
            }



            return view($viewType,$data);
        }else{
            return redirect()->route("login");
        }
    }

    public function getPairs($includeFees = false,$isFirstTime = false) {
        $pairList = [];
        $pairs = CoinCoin::with(["pair","coin","price"])->where("coin_id",1)->orderBy("coin_coin.display_index","desc")->get();



        foreach($pairs as $pair) {


//            $coin = Coin::where("id",$pair->coin_id)->get()->first();
//            $currency = Coin::where("id",$pair->pair_id)->get()->first();

            $coin = $pair->pair;
            $currency = $pair->coin;



//            dd($pair->pair()->first()->symbol,
//                $pair->coin()->first()->symbol,
//                $coin->symbol,$currency->symbol);


            if(($currency->ieo) &&($currency->symbol != "ORX")) {
                if($includeFees == false){

                    $lastPrice = 0;
                    $volume = 0;
                    $change = 0;


                    $lastPrice = $pair->price()->where("currency_id",$currency->id)->where("type", "buy")->orderBy("id","desc")->value("price");
                    $volume =   $pair->price()->where("currency_id",$currency->id)->where("type", "buy")->where('updated_at', '>', Carbon::now()->subDay())->sum("amount");
                    $change = $coin->change($currency->id);

                    $volume = number_format($volume, 2, ".", "");


                    $pairList[$currency->symbol . "/" . $coin->symbol] = [
                        "name" => $currency->symbol . "/" . $coin->symbol,
                        "volume" => $volume,
                        "change" => $change . " %",
                        "coin" => $coin->symbol,
                        "currency" => $currency->symbol,
                        "lastprice" => toFixed( $lastPrice?$lastPrice:0,8),
                        "slug" => $currency->slug,
                        "coin_id" => $coin->id,
                        "currency_id" => $currency->id,
                        "coin_name" =>$currency->name
                    ];

                }else{

                    if($currency->ieo == true){
                        $pairList[$currency->symbol . "/" . $coin->symbol] = [
                            "name" => $currency->symbol . "/" . $coin->symbol,
                            "maker_fee" => $coin->processing_fee,
                            "taker_fee" => $currency->processing_fee,
                        ];
                    }
                }
            }
        }
        return $pairList;
    }

    public function getPair($coinSymbol, $currencySymbol) {
        $coin = Coin::where("symbol", $coinSymbol)->get()->first();
        $currency = Coin::where("symbol", $currencySymbol)->get()->first();

        $lastPrice = $coin->lastPrice($currency->id);
        $pairData = [
            "name" => $currency->symbol . "/" . $coin->symbol,
            "volume" => $coin->volume($currency->id),
            "change" => $coin->change($currency->id),
            "coin" => $coin->symbol,
            "currency" => $currency->symbol,
            "lastprice" => $lastPrice?$lastPrice:0,
            "slug" => $currency->slug,
            "coin_id" => $coin->id,
            "currency_id" => $currency->id,
        ];

        return $pairData;
    }

    public function updateFavoritePairs(Request $request) {
        $reqPair = $request->post("pair");

        $user = auth()->user();

        $userFavorite = $user->favorites()->where("pair", $reqPair)->first();
        if($userFavorite) {

            $isSuccess = true;
            $action = "del";
            $pair = $reqPair;

            $userFavorite->forceDelete();
        } else {
            $userFavorite = UserFavorite::create([
                    "user_id" => $user->id,
                    "pair" => $reqPair
                ]
            );
            $isSuccess = true;
            $action = "add";
            $pair = $reqPair;
        }

        $data = [
            "success" => $isSuccess,
            "action" => $action,
            "pair" => $pair
        ];

        return response()->json($data);
    }

    /**
     *
     */
    public function store(){

        $transaction=null;
        broadcast(new NewMarketEvent($transaction))->toOthers();
    }


    /**
     * @param Request $request
     * @param User|null $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function cancelTransaction(Request $request,User $user = null){
        $transactionId = $request->post("transactionId");

        if(!$user){
            $user = auth()->user();
        }

        if($user){
            $transaction = Transaction::where("id",$transactionId)->where("status","<",2)->get()->first();
            $is_success = true;
            if(!$transaction){
                $is_success = false;
                $type = "error";
                $message = _i("Invalid order!");
                $title = _i("Error!");
            }else if($transaction && $transaction->user_id != $user->id){
                $is_success = false;
                $type = "error";
                $message = _i("This order does not belong to you.");
                $title = _i("Error!");
            }else if($transaction && $transaction->status == 3){
                $is_success = false;
                $type = "error";
                $message = _i("This order was already cancelled!");
                $title = _i("Error!");
            }else if($transaction && $transaction->status == 2){
                $is_success = false;
                $type = "error";
                $message = _i("You cannot cancel a completed order!");
                $title = _i("Error!");
            }

            $coin = $transaction->coin;
            $currency = $transaction->currency;
            $ownerUser = $transaction->user;


            $currencyWallet = $ownerUser->wallets->where("type", false)->where("coin_id",$currency->id)->first();
            $coinWallet = $ownerUser->wallets->where("type",false)->where("coin_id",$coin->id)->first();

            if(!$currencyWallet){
                $is_success = false;
                $type = "error";
                $message = _i("Cannot find wallet, please create a new wallet!");
                $title = _i("Error!");
            }
            if(!$coinWallet)
            {
                $is_success = false;
                $type = "error";
                $message = _i("Wallet not found! Please create a new wallet!");
                $title = _i("Error!");
            }

            if($is_success == true){
                if($transaction->type == "sell"){
                    $transaction->status = 3;
                    if($transaction->completed_amount > 0) {
                        $total = $transaction->details()->get()->sum("total");
                        $currencyWallet->balance -= floatval($transaction->completed_amount);
                        $coinWallet->balance += $total; //floatval($transaction->completed_amount) * floatval($transaction->price);
                        if($transaction->processing_fee > 0) {
                            $coinWallet->balance -= floatval($transaction->completed_amount) * floatval($transaction->price) * floatval($transaction->processing_fee);
                        }
                    }
                }else{
                    $transaction->status = 3;
                    if($transaction->completed_amount > 0) {
                        $total = $transaction->details()->get()->sum("total");
                        $currencyWallet->balance += floatval($transaction->completed_amount);
                        $coinWallet->balance -= $total; //floatval($transaction->completed_amount) * floatval($transaction->price);
                        if($transaction->processing_fee > 0) {
                            $currencyWallet->balance -= floatval($transaction->completed_amount) * floatval($transaction->price) * floatval($transaction->processing_fee);
                        }
                    }
                }

                if($currencyWallet->balance < 0){
                    $is_success = false;
                    $type = "error";
                    $message = _i("A error occured cancellation on transaction! Code : 1");
                    $title = _i("Error!");
                }

                if($coinWallet->balance < 0){
                    $is_success = false;
                    $type = "error";
                    $message = _i("A error occured cancellation on transaction. Code : 2");
                    $title = _i("Error!");
                }

                if($is_success == true){
                    $currencyWallet->save();
                    $coinWallet->save();
                    $transaction->save();

                    $user = auth()->user();
                    $transactionData = self::getOrderBookWithStats($user, $coin->id,$currency->id);
                    $redis=\Redis::connection();
                    $redis->publish("message",json_encode($transactionData));

                    $is_success = true;
                    $type = "success";
                    $message = _i("The cancellation was successful.");
                    $title = _i("Info!");
                }else{
                    $is_success = false;
                    $type = "error";
                    $message = _i("A error occured cancellation on transaction. Code : 3");
                    $title = _i("Error!");
                }
            }

            $data = [
                "success" => $is_success,
                "type" => $type,
                "title" => $title,
                "message" => $message];
            return response()->json( $data,200);
        }else{
            return redirect()->route("login");
        }

    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return mixed
     * @throws \Exception
     */
    public function openTransactions(Request $request, User $user = null){ // OK
        if(!$user){
            $user = auth()->user();
        }
        $startDate = \Carbon\Carbon::today();

        $coin_id = $request->input("coin_id");
        $currency_id = $request->input("currency_id");

        $limit = $request->input("limit");
        if($coin_id > 0) {
            $open_orders = $user->transactions()->where("status","<",2)->where("coin_id", $coin_id)->where("currency_id", $currency_id)->orderBy("id", "desc");
        } else {
            $open_orders = $user->transactions()->where("status","<",2)->orderBy("id", "desc");
        }

        $datatable = DataTables::of($open_orders)
            ->addColumn("recid", function($transaction){
                return $transaction->id;
            })
            ->addColumn("id", function($transaction){
                return "";
            })
            ->addColumn("style", function($transaction){
                return $transaction->type;
            })
            ->addColumn("type", function($transaction){
                return $transaction->type == "buy" ? _i("BUY") : _i("SELL");
            })
            ->addColumn("symbol", function($transaction){
                return sprintf("%s/%s", $transaction->currency->symbol, $transaction->coin->symbol);
            })
            ->addColumn("date", function($transaction){
                return $transaction->updated_at;
            })
            ->addColumn("price", function($transaction){
                return sprintf("%s %s",number_format($transaction->price, $transaction->coin->decimals, $transaction->currency->coin, $transaction->currency->coin),$transaction->coin->symbol);
            })
            ->addColumn("amount", function($transaction){
                return sprintf("%s %s",number_format($transaction->amount, $transaction->currency->decimals, $transaction->currency->dec_point, $transaction->currency->thousands_sep),$transaction->currency->symbol);
            })
            ->addColumn("summary", function($transaction){
                return sprintf("%s %s",number_format($transaction->total, $transaction->coin->decimals, $transaction->coin->dec_point, $transaction->coin->thousands_sep),$transaction->coin->symbol);

            })
            ->addColumn("completed", function($transaction){
                return sprintf("%s %s",number_format($transaction->completed_amount, $transaction->currency->decimals, $transaction->currency->dec_point, $transaction->currency->thousands_sep),$transaction->currency->symbol);
            });
        return $datatable->make(true);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws \Exception
     */
    public function completedTransactions(Request $request,User $user = null){
        if(!$user){
            $user = auth()->user();
        }
        $startDate = \Carbon\Carbon::today();

        $coin_id = $request->input("coin_id");
        $currency_id = $request->input("currency_id");

        $limit = $request->input("limit");
        if($coin_id > 0) {
            $closed_orders = $user->completedTransactions()->where("coin_id", $coin_id)->where("currency_id", $currency_id)->orderBy("id", "desc");
        } else {
            $closed_orders = $user->completedTransactions()->orderBy("id", "desc");
        }

        $datatable = DataTables::of($closed_orders)
            ->addColumn("id", function($transaction){
                return "";
            })
            ->addColumn("style", function($transaction){
                return $transaction->type;
            })
            ->addColumn("type", function($transaction){
                return ($transaction->type == "buy" ? _i("BUY") : _i("SELL"));
            })
            ->addColumn("symbol", function($transaction){
                return sprintf("%s/%s", $transaction->currency->symbol, $transaction->coin->symbol);
            })
            ->addColumn("date", function($transaction){
                return $transaction->updated_at;
            })
            ->addColumn("price", function($transaction){
                return sprintf("%s %s",number_format($transaction->price, $transaction->coin->decimals, $transaction->coin->dec_point, $transaction->coin->thousands_sep),$transaction->coin->symbol);
            })
            ->addColumn("amount", function($transaction){
                return sprintf("%s %s",number_format($transaction->amount, $transaction->currency->decimals, $transaction->currency->dec_point, $transaction->currency->thousands_sep),$transaction->currency->symbol);
            })
            ->addColumn("summary", function($transaction){
                return sprintf("%s %s",number_format($transaction->total, $transaction->coin->decimals, $transaction->coin->dec_point, $transaction->coin->thousands_sep),$transaction->coin->symbol);

            })
            ->addColumn("fee", function($transaction){
                if($transaction->type == "sell") {
                    return sprintf("%s %s",number_format($transaction->fee, $transaction->coin->decimals, $transaction->coin->dec_point, $transaction->coin->thousands_sep),$transaction->coin->symbol);
                } else {
                    return sprintf("%s %s",number_format($transaction->fee, $transaction->currency->decimals, $transaction->currency->dec_point, $transaction->currency->thousands_sep),$transaction->currency->symbol);
                }
            });
        return $datatable->make(true);
    }
    /*
     * amount
     * completed
     *
     * */
    public function getCoinDetails($method, $parameters)
    {
        parent::__call($method, $parameters); // TODO: Change the autogenerated stub
    }


    /**
     * @param $coinId
     * @param $currencyId
     */
    public function getOrderBookWithStats(User $user, $coinId, $currencyId){




        $coin = Coin::where("id",$coinId)->get()->first();
        if($coin){
            $activeCurrency = Coin::where("id",$currencyId)->get()->first();
            $buyTransactions = Transaction::select("transactions.*"
                ,DB::raw('SUM(remaining_amount) as totalAmount')
                ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
                ->where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("transaction_type",0)
                ->where("status","<",2)
                ->groupBy("price")
                ->orderBy("price","desc")
                ->take(20)->get();



            $sellTransactions =  Transaction::select("transactions.*"
                ,DB::raw('SUM(remaining_amount) as totalAmount')
                ,DB::raw('SUM(remaining_amount * price) as totalPrice'))
                ->where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("transaction_type",1)
                ->where("status","<",2)
                ->groupBy("price")
                ->orderBy("price","desc")
                ->get()->reverse()->take(20);

            $market_history = TransactionDetail::where("coin_id",$coin->id)
                ->where("currency_id",$activeCurrency->id)
                ->where("type", "buy")
                ->orderBy("id", "desc")
                ->take(50)->get();

            for($i = count($market_history)-1; $i >= 0; $i--) {
                if(!@$oldPrice) $oldPrice = $market_history[$i]->price;

                if($market_history[$i]->price > $oldPrice) {
                    $market_history[$i]->color = 1;
                } elseif($market_history[$i]->price < $oldPrice) {
                    $market_history[$i]->color = 2;
                } else {
                    $market_history[$i]->color = 0;
                }
                $oldPrice = $market_history[$i]->price;
            }

            $sellSummary = $sellTransactions->sum("totalPrice");
            $buySummary = $buyTransactions->sum("totalPrice");

            if($buySummary == 0){$buySummary = 1;}
            if($sellSummary == 0){$sellSummary = 1;}

            $deepBuy = 0;
            $calculatedBuy = 0;
            foreach($buyTransactions as &$buy){
                $buy->price = sprintf('%.'.$coin->decimals.'f', floatval($buy->price));
                $buy->totalAmount =  sprintf('%.'.$activeCurrency->decimals.'f', floatval($buy->totalAmount));
                $buy->totalPrice = number_format($buy->totalPrice, $coin->decimals, ".", "");
                $deepBuy += floatval($buy->totalPrice);
                $calculatedBuy += floatval($buy->totalPrice);
                $buy->depth = ($deepBuy * 100 / $buySummary);
                $buy->calculatedTotalAmount = sprintf('%.'.$coin->decimals.'f', $calculatedBuy);
            }

            $deepSell=0;
            $calculatedSell = 0;
            foreach($sellTransactions as &$sell){
                $sell->price = sprintf('%.'.$coin->decimals.'f', floatval($sell->price));
                $sell->totalAmount =   sprintf('%.'.$activeCurrency->decimals.'f', floatval($sell->totalAmount));

                $sell->totalPrice = number_format($sell->totalPrice, $activeCurrency->decimals, ".", "");
                $deepSell += floatval($sell->totalPrice);
                $calculatedSell += floatval($sell->totalPrice);
                $sell->depth =  ($deepSell * 100 / $sellSummary);
                $sell->calculatedTotalAmount =  sprintf('%.'.$coin->decimals.'f', $calculatedSell);
            }

            foreach($market_history as &$history){
                $history->amount = floattostr($history->amount);
                $date = Carbon::parse($history->updated_at)->format('H:i:s');
                unset($history->updated_at);
                $history->date = $date;
            }

            $pairsData = [];
            $volumes = [];
            $pairs = DB::table('coin_coin')->get();
            foreach($pairs as $pair) {
                $pair_coin = Coin::where("id",$pair->coin_id)->get()->first();
                $pair_currency = Coin::where("id",$pair->pair_id)->get()->first();
                if($pair_currency->ieo == true){
                    $volumes[$pair_currency->symbol . "/" . $pair_coin->symbol] = $pair_coin->volume($pair_currency->id);
                }
            }

            $coinBalance = $user->user_balance(true, $coin->id, 0, true, $user);
            $coinBalanceFloat = $user->user_balance(false, $coin->id, 1, true, $user);
            $currencyBalance = $user->user_balance(true, $activeCurrency->id, 1, true, $user);
            $currencyBalanceFloat = $user->user_balance(false, $activeCurrency->id, 1, true, $user);

            $balanceInfo = [
                "token"=>$user->public_key,
                "coinBalance"=>$coinBalance,
                "currencyBalance"=>$currencyBalance,
                "coinBalanceFloat"=>$coinBalanceFloat,
                "currencyBalanceFloat"=>$currencyBalanceFloat
            ];
            $is_success = true;
            $szCoinId = $coin->id;
        } else {
            $buyTransactions = [];
            $sellTransactions = [];
            $is_success = false;
            $type = "error";
            $message = _i("This crypto currency is not defined in the system!");
            $title = _i("Error!");
            $szCoinId = -1;
            $market_history = [];
            $volumes = [];
            $activeCurrency = -1;
            $balanceInfo = null;
        }

        $stats = null;
        if ($is_success == true){
            $stats = getHeaderHomeStatistics($coinId,$currencyId);
        }
        $data = [
            "decimals"=>array("coin"=>$coin->decimals, "currency"=>$activeCurrency->decimals),
            "success"=>$is_success,
            "coin" => $coin,
            "coin_id"=>$szCoinId,
            "stats"=>$stats,
            "buy"=>$buyTransactions,
            "sell"=>$sellTransactions,
            "market_history"=>($market_history?$market_history:false),
            //"coins"=>$coins,
            "pairsData"=>$this->getPairs(),
            "volumes"=>$volumes,
            "currency" => $activeCurrency,
            "balance" => $balanceInfo,
        ];


        return $data;
    }

    /**
     * @param Request $request
     * @param null $coinId
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadMarketData(Request $request, $coinId = null, $currencyId = null)
    {
        if($request->filled("coinId") && $request->filled("currencyId")) {
            $coinId = $request->input("coinId");
            $currencyId = $request->input("currencyId");
        }else{
            $coinId = Coin::where("symbol", getDefaultBaseCoin())->first()->id;
            $currencyId = Coin::where("symbol", getDefaultSubCoin())->first()->id;
        }

        $user = auth()->user();
        $transactionData = self::getOrderBookWithStats($user, $coinId,$currencyId);

        $redis=\Redis::connection();
        $redis->publish("message",json_encode($transactionData));

        $coin = Coin::where("id",$coinId)->get()->first();
        $activeCurrency = Coin::where("id",$currencyId)->get()->first();

        if($coin){
            $balance = $user->user_balance(false,$coinId,false,true,$user);
            $balance = number_format($balance,$coin->decimals,$coin->dec_point,$coin->thousands_sep);
        }else{
            $balance = 0.00;
        }

        if($activeCurrency){
            $currencyBalance = $user->user_balance(false,$currencyId,true,true,$user);
            $currencyBalance = number_format($currencyBalance,$activeCurrency->decimals,$activeCurrency->dec_point,$activeCurrency->thousands_sep);
        }else{
            $currencyBalance = 0.00;
        }
        $data=["success"=>true,"coin" => $coin,"currency" => $activeCurrency,"balance"=>$balance,"currencybalance"=>$currencyBalance];
        return response()->json( $data,200);
    }

    /**
     * @param Request $request
     * @param User|null $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function createBuy(Request $request, User $user = null){

        $details = null;
        if(!$user){
            $user = auth()->user();
        }

        if($user){
            $valAmountBuyInt = $request->input("edtAmountBuyInt");
            $valBuyCoin = $request->input("edtBuyCoin");
            $valBuyCurrency = $request->input("edtBuyCurrency");
            $valPriceBuy = $request->input("edtPriceBuy");
            $valSummaryBuy = $request->input("edtSummaryBuy");
            $valFeeBuy = $request->input("edtFeeBuy");

            $coin = Coin::where("symbol",$valBuyCoin)->get()->first();
            $currency = Coin::where("symbol",$valBuyCurrency)->get()->first();

            $coinWallet = $user->wallets()->where("type",0)->where("coin_id",$coin->id)->first();

            $currencyWallet = $user->wallets()->where("type",0)->where("coin_id",$currency->id)->first();
            $is_success = true;
            $type="";
            $message="";
            $title="";

            $amount = floatval($valAmountBuyInt);
            $balance = 0.00;
            if($currencyWallet){
                $balance = floatval($user->user_balance(false,$coin->id,false,true,$user));
            }

            $summaryBuy = number_format($valSummaryBuy, $currency->decimals, $currency->dec_point,"");
            $priceBuy = number_format($valPriceBuy, $currency->decimals, $currency->dec_point,"");
            $valFeeBuy = number_format($valFeeBuy, $currency->decimals,$currency->dec_point,"");



            $hasFee = true;
            if($coin->processing_fee > 0 && $currency->processing_fee > 0) {
                $feeAmount = $currency->processing_fee;
                $hasFee = true;
            } else {
                $feeAmount = 0;
                $hasFee=false;
            }

            $mCalculatedFee = $amount * $feeAmount;
            $mCalculatedTotal = $priceBuy * $amount;

            $zeroBalance = 0.00;
            if(!$currencyWallet){
                $is_success = false;
                $type = "error";
                $message = _i("%s the wallet of the crypto currency is not defined in your account. Please define a new wallet!", $valBuyCurrency);
                $title = _i("Error!");
            } else if(!$coinWallet){
                $is_success = false;
                $type = "error";
                $message = _i("%s the wallet of the crypto currency is not defined in your account. Please define a new wallet!", $valBuyCoin);
                $title = _i("Error!");
            } else if($balance<=0.00000000){
                $is_success = false;
                $type = "error";
                $message = _i("The balance of your %s (%s) crypto currency wallet is insufficient!");
                $message = sprintf($message,$coin->name,$coin->symbol);
                $title = _i("Error!");
            }else if($balance<$mCalculatedTotal){
                $is_success = false;
                $type = "error";
                $message = _i("The balance of your %s (%s) crypto currency wallet is insufficient!");
                $message = sprintf($message,$coin->name,$coin->symbol);
                $title = _i("Error!");
            }else if(!$currency){
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not defined in the system!", $valBuyCurrency);
                $title = _i("Error!");
            } else if(!$coin){
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not defined in the system!", $valBuyCoin);
                $title = _i("Error!");
            } else if($coin && $coin->status != 0){
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not yet active!", $coin->name);
                $title = _i("Error!");
            } else if($currency && $currency->status != 0){
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not yet active!", $currency->name);
                $title = _i("Error!");
            } else if($amount<=0.00){
                $is_success = false;
                $type = "error";
                $message = _i("The amount entered incorrect!");
                $title = _i("Error!");
            } else if($priceBuy <= $zeroBalance){
                $message = _i("The price entered incorrect!");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if($summaryBuy <= $zeroBalance){
                $message = _i("The total amount cannot be less than 0!");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if(floatval($mCalculatedTotal) > floatval($balance)){
                $message = _i("Your %s usable wallet balance is insufficient!");
                $message=sprintf($message,$coin->name);
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if($hasFee == true){
                if($mCalculatedFee<0.00000001){
                    $message = _i("The fee amount is incorrect!");
                    $title = _i("Error!");
                    $is_success = false;
                    $type = "error";
                }
            } else if($valPriceBuy != 0.00000110){
                $message = _i("The price is incorrect! MRCL IEO Price Is : 0.00000110 ETH");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if($amount < 10000){
                $message = _i("The price is incorrect! MRCL IEO Minimum Amount  Is : 10000 MRCL");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            }


            if($is_success == true) {
                $transactionCode = generateTransactionCode();
                $checkTransaction = Transaction::where("user_id", $user->id)
                    ->where("coin_id", $coin->id)
                    ->where("currency_id", $currency->id)
                    ->where("transaction_type", 0)
                    ->where("type", "buy")
                    ->where("amount", $amount)
                    ->where("real_amount", $amount)
                    ->where("completed_amount", 0.00)
                    ->where("remaining_amount", $amount)
                    ->where("price", $valPriceBuy)
                    ->where("total" ,$valSummaryBuy)
                    ->where("fee", $mCalculatedFee)
                    ->where("processing_fee" ,floatval($feeAmount))
                    ->where("status",0)->get()->first();

                if(!$checkTransaction){
                    $transaction=Transaction::create([
                        "user_id" => $user->id,
                        "coin_id" => $coin->id,
                        "currency_id" => $currency->id,
                        "transaction_type" => 0,
                        "type" => "buy",
                        "amount" => $amount,
                        "real_amount" => $amount,
                        "completed_amount" => 0.00,
                        "remaining_amount" => $amount,
                        "price" => $valPriceBuy,
                        "total" => $valSummaryBuy,
                        "fee" => $mCalculatedFee, //$mFee
                        "processing_fee" => floatval($feeAmount),
                        "ref_code" => $transactionCode,
                        "status" => 0
                    ]);
                    self::completeTransaction($transaction,$user,$coin,$currency) ;
                    $is_success = true;
                    $type = "success";
                    $message=_i("Order has been created successfully.");
                    $title= _i("Info");
                    $details = ["clientOrderId"=>$transactionCode];
                }else{
                    $is_success = false;
                    $type = "error";
                    $message = _i("Duplicated transaction not allowed!");
                    $title = _i("Error");
                    $details = ["clientOrderId" => $transactionCode];
                }
            }

            $data = [
                "success" => $is_success,
                "type" => $type,
                "title" => $title,
                "message" => $message,"data"=>$details];
            return response()->json( $data,200);
        }else{
            return redirect()->route("login");
        }
    }


    public function updateUserWalletBalance(Transaction $transaction){
        if(!$transaction){return;}

        $user = $transaction->user;
        $coin = $transaction->coin;
        $currency = $transaction->currency;

        if($transaction->status==2){
            $coinWallet = $user->wallets->where("type",false)->where("coin_id",$coin->id)->first();
            $currencyWallet = $user->wallets->where("type",false)->where("coin_id",$currency->id)->first();
            if($transaction->type == "sell"){
                $total = $transaction->details()->get()->sum("total");
                $fee = $transaction->details()->get()->sum("fee");
                $currencyWallet->balance -= floatval($transaction->amount);
                $coinWallet->balance += floatval($total)-floatval($fee);
                $currencyWallet->save();
                $coinWallet->save();
            }else if($transaction->type == "buy"){
                $total = $transaction->details()->get()->sum("total");
                $fee = $transaction->details()->get()->sum("fee");
                $currencyWallet->balance += floatval($transaction->amount)-floatval($fee);
                $coinWallet->balance -= floatval($total);
                $currencyWallet->save();
                $coinWallet->save();
            }
        }

        $transactionData=self::getOrderBookWithStats($user, $coin->id,$currency->id);
        $redis=\Redis::connection();
        $redis->publish("message",json_encode($transactionData));
    }

    public function completeTransaction(Transaction $transaction, User $user, Coin $coin, Coin $currency) {
        $szTransactionType = "-";
        if($transaction->type == "buy"){
            $szTransactionType = "sell";
            $iTransactionType = 1;
        }else if($transaction->type=="sell"){
            $szTransactionType="buy";
            $iTransactionType = 0;
        }

        if($transaction->status < 2)
        {
            $priceComparison = ($szTransactionType == "sell" ? "<=" : ">=");
            $orderBy = ($szTransactionType == "sell" ? "asc" : "desc");

            $checkTransactions = Transaction::whereIn("status", [0,1])
                ->where("coin_id",$coin->id)
                ->where("currency_id",$currency->id)
                ->where("transaction_type",$iTransactionType)
                ->where("user_id","<>",$user->id)
                ->where("price",$priceComparison,$transaction->price)->orderBy("price", $orderBy)->get();


            foreach ($checkTransactions as $checkTransaction) {


                if($transaction->amount<=$transaction->details()->sum("amount")){
                    $transaction->status=2;
                    $transaction->save();
                }

                if ($transaction->status < 2) {

                    if ($transaction->remaining_amount >= $checkTransaction->remaining_amount)
                    {



                        $calculatedTotal = floatval($checkTransaction->remaining_amount) * floatval($checkTransaction->price);
                        $transactionDetails = [
                            [
                                "user_id" => $transaction->user_id,
                                "transaction_id" => $transaction->id,
                                "dest_transaction_id" => $checkTransaction->id,
                                "coin_id" => $coin->id,
                                "currency_id" => $currency->id,
                                "type" => $transaction->type,
                                "amount" => $checkTransaction->remaining_amount,
                                "price" => $checkTransaction->price,
                                "total" => $calculatedTotal,
                                "fee" => $calculatedTotal * $transaction->processing_fee,
                            ],
                            [
                                "user_id" => $checkTransaction->user_id,
                                "transaction_id" => $checkTransaction->id,
                                "dest_transaction_id" => $transaction->id,
                                "coin_id" => $coin->id,
                                "currency_id" => $currency->id,
                                "type" => $checkTransaction->type,
                                "amount" => $checkTransaction->remaining_amount,
                                "price" => $checkTransaction->price,
                                "total" => $calculatedTotal,
                                "fee" => $calculatedTotal * $checkTransaction->processing_fee,
                            ]
                        ];

                        foreach ($transactionDetails as $transactionDetail) {
                            TransactionDetail::create($transactionDetail);
                        }

                        $transaction->completed_amount += $checkTransaction->remaining_amount;
                        $transaction->remaining_amount -= $checkTransaction->remaining_amount;
                        $transaction->status = 1;
                        $transaction->save();

                        $checkTransaction->completed_amount += $checkTransaction->remaining_amount;
                        $checkTransaction->remaining_amount -= $checkTransaction->remaining_amount;
                        $checkTransaction->status = 2;
                        $checkTransaction->save();

                        if ($transaction->remaining_amount <= 0.000000009) {
                            $transaction->status = 2;
                            $transaction->save();
                            self::updateUserWalletBalance($transaction);
                        }
                        self::updateUserWalletBalance($checkTransaction);

                    }
                    else
                    {
                        $calculatedTotal = floatval($transaction->remaining_amount) * floatval($checkTransaction->price);
                        $transactionDetails = [
                            [
                                "user_id" => $transaction->user_id,
                                "transaction_id" => $transaction->id,
                                "dest_transaction_id" => $checkTransaction->id,
                                "coin_id" => $coin->id,
                                "currency_id" => $currency->id,
                                "type" => $transaction->type,
                                "amount" => $transaction->remaining_amount,
                                "price" => $checkTransaction->price,
                                "total" => (floatval($transaction->remaining_amount) * floatval($checkTransaction->price)),
                                "fee" => $calculatedTotal * $transaction->processing_fee,
                            ],
                            [
                                "user_id" => $checkTransaction->user_id,
                                "transaction_id" => $checkTransaction->id,
                                "dest_transaction_id" => $transaction->id,
                                "coin_id" => $coin->id,
                                "currency_id" => $currency->id,
                                "type" => $checkTransaction->type,
                                "amount" => $transaction->remaining_amount,
                                "price" => $checkTransaction->price,
                                "total" => (floatval($transaction->remaining_amount) * floatval($checkTransaction->price)),
                                "fee" => $calculatedTotal * $checkTransaction->processing_fee,
                            ]
                        ];

                        foreach ($transactionDetails as $transactionDetail) {
                            TransactionDetail::create($transactionDetail);
                        }


                        $checkTransaction->completed_amount += $transaction->remaining_amount;
                        $checkTransaction->remaining_amount -= $transaction->remaining_amount;
                        $checkTransaction->status = 1;
                        $checkTransaction->save();

                        $transaction->completed_amount += $transaction->remaining_amount;
                        $transaction->remaining_amount -= $transaction->remaining_amount;
                        $transaction->status = 2;
                        $transaction->save();

                        if ($checkTransaction->remaining_amount <= 0.000000009) {
                            $checkTransaction->status = 2;
                            $checkTransaction->save();
                            self::updateUserWalletBalance($checkTransaction);
                        }
                        self::updateUserWalletBalance($transaction);

                    }


                }
            }

        }

        $transactionData=self::getOrderBookWithStats($user, $coin->id,$currency->id);
        $redis=\Redis::connection();
        $redis->publish("message",json_encode($transactionData));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createSell(Request $request, User $user = null){
        if(!$user){
            $user = auth()->user();
        }

        if($user) {
            $details = null;
            $valAmountSellInt = $request->input("edtAmountSellInt");
            $valSellCoin = $request->input("edtSellCoin");
            $valSellCurrency = $request->input("edtSellCurrency");
            $valPriceSell = $request->input("edtPriceSell");
            $valSummarySell = $request->input("edtSummarySell");
            $valFeeSell = $request->input("edtFeeSell");

            $coin = Coin::where("symbol", $valSellCoin)->get()->first();
            $currency = Coin::where("symbol", $valSellCurrency)->get()->first();


            $coinWallet = $user->wallets()->where("type", false)->where("coin_id", $coin->id)->first();
            $currencyWallet = $user->wallets()->where("type", false)->where("coin_id", $currency->id)->first();

            $balance = 0.00;
            if ($currencyWallet) {
                $balance = floatval($user->user_balance(false, $currency->id, true, true, $user));
                $coinBalance = floatval($user->user_balance(false, $coin->id, false, true, $user));
            }

            $is_success = true;
            $type = "";
            $message = "";
            $title = "";
            $amount = floatval($valAmountSellInt);

            $summarySell = number_format($valSummarySell, $currency->decimals, $currency->dec_point, "");
            $valFeeSell = number_format($valFeeSell, $currency->decimals, $currency->dec_point, "");

            $hasFee = true;
            if ($coin->processing_fee > 0 && $currency->processing_fee > 0) {
                $feeAmount = $coin->processing_fee;
                $hasFee = true;
            } else {
                $feeAmount = 0;
                $hasFee = false;
            }

            //$mFee = $valFeeSell;
            $mCalculatedFee = $summarySell * floatval($feeAmount);


            if (!$coinWallet) {
                $is_success = false;
                $type = "error";
                $message = _i("The wallet of %s crypto currency is not defined in your account. Please create your wallet!", $valSellCoin);
                $title = _i("Error!");
            } else if (!$currencyWallet) {
                $is_success = false;
                $type = "error";
                $message = _i("The wallet of %s crypto currency is not defined in your account. Please create your wallet!", $valSellCurrency);
                $title = _i("Error!");
            } else if ($balance <= 0.00000000) {
                $is_success = false;
                $type = "error";
                $message = _i("The balance of your %s (%s) crypto currency wallet is insufficient!");
                $message = sprintf($message, $currency->name, $currency->symbol);
                $title = _i("Error!");
            } else if (!$currency) {
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not defined in the system!", $valSellCoin);
                $title = _i("Error!");
            } else if (!$coin) {
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not defined in the system!", $valSellCurrency);
                $title = _i("Error!");
            } else if ($coin && $coin->status != 0) {
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not activated yet!", $coin->symbol);
                $title = _i("Error!");
            } else if ($currency && $currency->status != 0) {
                $is_success = false;
                $type = "error";
                $message = _i("%s crypto currency is not activated yet!", $currency->symbol);
                $title = _i("Error!");
            } else if ($amount <= 0) {
                $is_success = false;
                $type = "error";
                $message = _i("The amount is incorrect!");
                $title = _i("Error!");
            } else if ($valPriceSell <= 0) {
                $message = _i("The price is incorrect!");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if ($valSummarySell <= 0.00) {
                $message = _i("Total amount cannot be less than 0!");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if ($valSummarySell != (toFixed(($valPriceSell * $amount), $coin->decimals))) {
                $message = _i("The amount entered and the price multiplication value do not reflect the actual amount!") . "----" . $valSummarySell . "----" . (toFixed(($valPriceSell * $amount), $coin->decimals));
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if (floatval($balance) < floatval($amount)) {
                $message = _i("The balance of your %s (%s) crypto currency wallet is insufficient!");
                $message = sprintf($message, $currency->name, $currency->symbol);
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            } else if ($hasFee == true) {

                if ($mCalculatedFee < 0.00000001) {
                    $message = _i("The fee amount is incorrect!");
                    $title = _i("Error!");
                    $is_success = false;
                    $type = "error";
                }
            }


            if ($is_success == true) {


                $transactionCode = generateTransactionCode();


                $checkTransaction = Transaction::where("user_id", $user->id)
                    ->where("coin_id", $coin->id)
                    ->where("currency_id", $currency->id)
                    ->where("transaction_type", 1)
                    ->where("type", "sell")
                    ->where("amount", $amount)
                    ->where("real_amount", $amount)
                    ->where("completed_amount", 0.00)
                    ->where("remaining_amount", $amount)
                    ->where("price", $valPriceSell)
                    ->where("total", $valSummarySell)
                    ->where("fee", $mCalculatedFee)
                    ->where("processing_fee", floatval($feeAmount))
                    ->where("status", 0)->get()->first();


                if (!$checkTransaction) {
                    $transaction = Transaction::create([
                        "user_id" => $user->id,
                        "coin_id" => $coin->id,
                        "currency_id" => $currency->id,
                        "transaction_type" => 1,
                        "type" => "sell",
                        "amount" => $amount,
                        "real_amount" => $amount,
                        "completed_amount" => 0.00,
                        "remaining_amount" => $amount,
                        "price" => $valPriceSell,
                        "total" => $valSummarySell,
                        "fee" => $mCalculatedFee,
                        "processing_fee" => floatval($feeAmount),
                        "ref_code" => $transactionCode,
                        "status" => 0
                    ]);

                    self::completeTransaction($transaction, $user, $coin, $currency);
                    $type = "success";
                    $message = _i("Your order has been successfully created.");
                    $title = _i("Info");
                    $details = ["clientOrderId" => $transactionCode];
                } else {
                    $is_success = false;
                    $type = "error";
                    $message = _i("Duplicated transaction not allowed!");
                    $title = _i("Error");
                    $details = ["clientOrderId" => $transactionCode];
                }
            }

            $data = [
                "success" => $is_success,
                "type" => $type,
                "title" => $title,
                "message" => $message, "data" => $details];
            return response()->json($data, 200);
        }else{
            return redirect()->route("login");

        }
    }
}