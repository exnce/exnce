<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 4/30/19
 * Time: 8:03 AM
 */

namespace App\Http\Controllers\Personal;


use App\Airdrop;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AirdropController extends Controller
{
    public function index(Request $request)
    {

        $airdrops = Airdrop::orderBy("id", "desc")->get();

        $data = ["airdrops" => $airdrops,
            "metaTags"=>getSEOMetaTags(null,null,"airdrops")
        ];
        return view("airdrop.index", $data);
    }

    public function details(Request $request,$airdropId = null)
    {

        if($airdropId){
            $airdrop = Airdrop::where("id", $airdropId)->get()->first();

            if($airdrop){
                $data = ["airdrop" => $airdrop];
                return view("airdrop.details", $data);
            }else{
                return redirect()->route("airdrops");
            }
        }
    }

    public function updateRatings(Request $request){
        $user = auth()->user();
        $is_success = false;
        $type = "error";
        $title = "Error!";
        $message  = "Invalid User!";
        if($user){
           $airdropId = $request->input("airdropId");
           $ratio = $request->input("rating");

            $airdrop = Airdrop::where("id", $airdropId)->get()->first();
            if($airdrop){
                $rating = new \willvincent\Rateable\Rating;
                $rating->rating = $ratio;
                $rating->user_id = $user->id;
                $airdrop->ratings()->save($rating);

                $is_success = true;
                $type = "info";
                $title = _i("Info");
                $message  = _i("Thank you for taking your time for the review.");

            }else{
                $is_success = false;
                $type = "error";
                $title = _i("Error!");
                $message  = _i("Invalid Airdrop!");
            }
        }

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];

        return response()->json( $data,200);
    }

    public function doFollow(Request $request)
    {
        $user = auth()->user();
        $is_success = false;
        $type = "error";
        $title = "Error!";
        $message  = "Invalid User!";
        if($user){

            $airdropId = $request->input("airdropId");
            $follow = $request->input("follow");


            $airdrop = Airdrop::where("id", $airdropId)->get()->first();
            if($airdrop){

                if($follow == "true"){
                    $user->follow($airdropId,\App\Airdrop::class);
                     $message  = _i("Thank you for taking your time for the follow.");
                }else{
                    $user->unfollow($airdropId,\App\Airdrop::class);
                    $message  = _i("Thank you for taking your time for the unfollow.");

                }
                $is_success = true;
                $type = "info";
                $title = _i("Info");

            }else{
                $is_success = false;
                $type = "error";
                $title = _i("Error!");
                $message  = _i("Invalid Airdrop!");
            }

        }

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];

        return response()->json( $data,200);
    }

    }