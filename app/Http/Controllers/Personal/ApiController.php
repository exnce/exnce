<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 3/18/19
 * Time: 10:31 PM
 */

namespace App\Http\Controllers\Personal;

use App\UserApiKey;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{



    public function generateAPIKey(Request $request){
        $user = auth()->user();
        $config = array(
            "digest_alg" => "sha1",
            "private_key_bits" => 512,
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        );
        $szEncryptionKey = generatePrivateKey(getAPISecretKeyGenerateParam());
        $res = openssl_pkey_new($config);
        @openssl_pkey_export($res, $privKey);
        if($privKey == null){
            $message = _i("API Key cant be created!");
            $title = _i("Error");
            $type="error";
            $is_success = false;
            $certInfo = ["public_key"=>"FAILED",
                "secret_key"=>"FAILED"];
            $data = [
                "message" => $message,
                "title" => $title,
                "type" => $type,
                "success" => $is_success,
                "data" =>$certInfo];
            return response()->json($data, 200);
        }
        $pubKey = openssl_pkey_get_details($res);
        $pubKey = $pubKey["key"];

        openssl_public_encrypt($szEncryptionKey, $encrypted, $pubKey);

        openssl_private_decrypt($encrypted, $decrypted, $privKey);
        // dd($pubKey,$szEncryptionKey,$encrypted,$decrypted,$pubKey,$privKey);

        $message = _i("API Key has been created successfuly.");
        $title = _i("Info");
        $type="success";
        $is_success = true;

        $pubKey =  str_replace("\n", '', $pubKey);
        $pubKey = str_replace('-----BEGIN PUBLIC KEY-----', '', $pubKey);
        $pubKey = trim(str_replace('-----END PUBLIC KEY-----', '', $pubKey));

        $certInfo = ["public_key"=>$pubKey,
            "secret_key"=>md5($szEncryptionKey.getAPISecretKeyPrefix())];

        $keyEntry = ["user_id" => $user->id,"private_key"=>$privKey,"public_key" => $pubKey,"secret_key"=>$szEncryptionKey, "status" => 0];

        $checkKey = UserApiKey::where("secret_key",$szEncryptionKey)->where("user_id",$user->id)->first();
        if (!$checkKey){
            UserApiKey::create($keyEntry);
        } else {
            $message = _i("API Key cant be created!");
            $title = _i("Error");
            $type="error";
            $is_success = false;
        }

        $data = [
            "message" => $message,
            "title" => $title,
            "type" => $type,
            "success" => $is_success,
            "data" =>$certInfo];

        return response()->json($data, 200);

    }

    public function apiAccess(Request $request){
        $user = auth()->user();
        $defaultPermissions = getAPIKeyAccessPermissions();

        $data = [
            "user"=>$user,
            "api_permissions"=>$defaultPermissions
        ];
        return view("personal.settings.api.apilist", $data);
    }



    public function loadAPIEntries(Request $request)
    {
        $user = auth()->user();
        $data = [];

        if($user) {
            $data = $user->apiKeys()->get();
            $datatable = DataTables::of($data)
                ->addColumn("id", function($key){
                    return $key->id;
                })
                ->addColumn("secret_key", function($key){
                    return md5($key->secret_key.getAPISecretKeyPrefix());
                });
            return $datatable->make(true);
        } else {
            return redirect()->back();
        }

    }

    public function storeAPIKey(Request $request){
        $user = auth()->user();

        if($user) {

            $action = $request->input("edtActionType");
            $publicKey = $request->input("edtPublicKey");
            $secretKey = $request->input("edtSecretKey");

            $avaiblePermissions = getAPIKeyAccessPermissions();
            $permissionList = [];
            foreach ($avaiblePermissions as $permission) {
                $isEnabled = false;
                if ($request->input($permission["key"], false) == "on") {
                    $isEnabled = true;
                } else {
                    $isEnabled = false;
                }
                $permissionList[$permission["key"]] = $isEnabled;
            }

            $apiKey = $user->apiKeys()->where("public_key", $publicKey)->first();

            $is_success = false;
            $title = "";
            $message = "";
            if ($apiKey) {
                if (md5($apiKey->secret_key.getAPISecretKeyPrefix()) == $secretKey) {

                    switch ($action){
                        case "save":
                            foreach ($permissionList as $permission => $value){
                                $apiKey[$permission] = $value;
                            }
                            $apiKey->save();
                            $title = _i("Info");
                            $message = _i("API access privileges has been updated.");
                            $is_success = true;
                            break;
                        case "delete":
                            $apiKey->delete();
                            $title = _i("Info");
                            $message = _i("API access key has been removed!");
                            $is_success = true;
                            break;
                        case "update":
                            foreach ($permissionList as $permission => $value){
                                $apiKey[$permission] = $value;
                            }
                            $apiKey->save();
                            $title = _i("Info");
                            $message = _i("API access privileges has been updated.");
                            $is_success = true;
                            break;
                        default:
                            $title = _i("Error!");
                            $message = _i("Unknown action!");
                            $is_success = false;
                            break;
                    }

                } else {
                    $message = _i("Cannot find the Secret Key!");
                    $is_success = false;
                    $title = _("Error!");

                }
            } else {
                $message = _i("Cannot find the Public Key!");
                $title = _("Error!");
                $is_success = false;
            }
        }

        $data = [
            "message" => $message,
            "title" => $title,
            "type" => $is_success == true ? "success" : "error",
            "success" => $is_success];

        return response()->json($data, 200);
    }

}