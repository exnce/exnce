<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 13:42
 */

namespace App\Http\Controllers\Personal;

use App\BankAccount;
use App\Coin;
use App\CoinDeposit;
use App\Country;
use App\Currency;
use App\Order;
use App\Http\Controllers\Controller;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use stdClass;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
class DepositController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();



        $coins = Coin::orderBy("id", "asc")->get();
        $currencies = Currency::orderBy("id", "asc")->get();

        foreach ($currencies as &$currency) {
            $country = Country::where("currency_code", $currency->symbol)->get()->first();
            if ($country) {

                $currency->icon = $country->currency_symbol;
            }
        }

        $message = _i("Kullanıcı para yatırma ekranını görüntüledi.");
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);

        $user_orders = $user->orders->where("type", "deposit");
        $data = [
            "currencies" => $currencies,
            "coins" => $coins,
            "orders" => $user_orders];

        if($user->is_metamask)
        {
            $view = "personal.maintenance";
        } else{
            $view = "personal.deposit.deposit";
        }

        return view($view, $data);
    }

    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function depositFiatCurrency(Request $request, $currencySymbol = null)
    {



        $user = auth()->user();
        if (!$currencySymbol) {
            return redirect()->route("deposit");
        }
        $currency = Currency::where("symbol", $currencySymbol)->get()->first();

        $currencies = Currency::orderBy("id", "asc")->get();
        foreach ($currencies as &$tempcurrency) {
            $country = Country::where("currency_code", $tempcurrency->symbol)->get()->first();
            if ($country) {

                $tempcurrency->icon = $country->currency_symbol;
            }
        }

        if (!$currency) {
            return redirect()->route("deposit");
        }

        $depositRequest = Order::where('user_id', $user->id)
            ->where("type", "deposit")
            ->where("currency_type", true)// false coin true fiat
            ->where("currency_id", $currency->id)
            ->where("status", 0)->first();

        $banks = BankAccount::where("currency_id", $currency->id)->get();
        if ($depositRequest) {
            $bankAccount = BankAccount::find($depositRequest->bank_id);
            $data = [
                "banks" => $banks,
                "bank" => $bankAccount,
                "currency" => $currency,
                "currencies" => $currencies,
                "amount" => number_format($depositRequest->amount, 2, ',', ''),
                "description" => $depositRequest->transfer_code,
            ];

            $message = _i("Kullanıcı %s (%s) isimli para birimine ait yatırım talebi onaylama ekranını görüntüledi.");
            $message = sprintf($message, $currency->name, $currency->symbol);
            activity("personal")
                ->causedBy($user)
                ->performedOn($user)
                ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                ->log($message);
            return view("personal.deposit.fiat-verify", $data);

        }

        $data = ["banks" => $banks,
            "currency" => $currency,
            "currencies" => $currencies];

        $message = _i("Kullanıcı %s (%s) isimli para birimine ait para yatırıma ekranını görüntüledi.");
        $message = sprintf($message, $currency->name, $currency->symbol);
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);

        return view("personal.deposit.deposit-fiat", $data);

    }

    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function depositCryptoCurrency(Request $request, $currencySymbol = null)
    {



        $user = auth()->user();
        if (!$currencySymbol) {
            return redirect()->route("deposit");
        }
        $coin = Coin::where("symbol", $currencySymbol)->get()->first();
        $coins = Coin::orderBy("id", "asc")->get();
        if (!$coin) {
            return redirect()->route("deposit");
        }
        $wallet = $user->wallets()->where("type", false)->where("coin_id", $coin->id)->get()->first();


        if($wallet->address()->get()->last()->address_type != "ERC20"){
            $qrData = "address://".$wallet->address()->get()->last()->address."?&tag=".(10000000000 + $wallet->address()->get()->last()->id);

        }else{
            $qrData = "address://".$wallet->address()->get()->last()->address;
        }

        $path = public_path() . '/images/' . "brand-thumbnail.png";

        if(!File::exists($path)) {
            return response()->json(['message' => 'Image not found.'], 404);
        }

        $file = File::get($path);
        $type = File::mimeType($path);

        $image = QrCode::format('png')
            ->color(6,165,221)
            ->mergeString($file, .25)
            ->errorCorrection('H')->margin(0)->size(225)->generate( $qrData );

        $encodedImage = base64_encode($image);


        $data = ["coin" => $coin, "coins" => $coins, "wallet" => $wallet,"image" => $encodedImage];


        $message = _i("Kullanıcı %s (%s) isimli kripto para birimine ait para yatırıma ekranını görüntüledi.");
        $message = sprintf($message, $coin->name, $coin->symbol);
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);


        if($coin->status == 0){
            $view = "personal.deposit.cryptocurrency";
        }else{
            $view = "personal.deposit.deposit-disable";
        }

        return view($view, $data);

    }


    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createDepositRequest(Request $request, $currencySymbol = null)
    {
        $user = auth()->user();
        $bankId = $request->input("bank_id");
        $amount_integer = $request->input("amount_integer");
        $amount_decimal = $request->input("amount_decimal");
        $currency_id = $request->input("currency_id");
        $currency_type = $request->input("currency_type");
        $coin_id = $request->input("coin_id");


        if ($currency_type == true) {

            $rules = [
                'bank_id' => 'required|integer|min:0',
                'amount_integer' => 'required|integer|min:100',
                'amount_decimal' => 'required|numeric|min:0',
                'currency_id' => 'required|integer|min:0',
                'currency_type' => 'required|boolean'
            ];

            $checkValid = Validator::make($request->all(), $rules);
            if ($checkValid->fails()) {

                return redirect()->route('deposit-fiat-create', $currencySymbol)->withErrors($checkValid)->withInput();
            }

            $currency = Currency::where("id", $currency_id)->first();
            $message = _i("Kullanıcı %s (%s) isimli para birimine ait toplamda %s tutarında bir para yatırma talebi oluşturdu.");
            $message = sprintf($message, $currency->name, $currency->symbol, floatval($amount_integer . "." . $amount_decimal));
            activity("personal")
                ->causedBy($user)
                ->performedOn($user)
                ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                ->log($message);

            $orderArray = ["user_id" => $user->id,
                "type" => "deposit",
                "currency_type" => true,
                "currency_id" => $currency_id,
                "coin_id" => -1,
                "amount" => floatval($amount_integer . "." . $amount_decimal),
                "bank_id" => $bankId,
                "transfer_code" => generateTransactionCode()
            ];
            $order = Order::create($orderArray);
        } else {

            $currency = Currency::where("id", $currency_id)->first();
            $message = _i("Kullanıcı %s (%s) isimli kripto para birimine ait toplamda %s tutarında bir kripto para yatırma talebi oluşturdu.");
            $message = sprintf($message, $currency->name, $currency->symbol, floatval($amount_integer . "." . $amount_decimal));
            activity("personal")
                ->causedBy($user)
                ->performedOn($user)
                ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                ->log($message);

            $orderArray = ["user_id" => $user->id,
                "type" => "deposit",
                "currency_type" => false,
                "currency_id" => -1,
                "coin_id" => $coin_id,
                "amount" => floatval($amount_integer . "." . $amount_decimal),
                "bank_id" => $bankId,
                "transfer_code" => generateTransactionCode()
            ];
            $order = Order::create($orderArray);
        }
        return redirect()->route('deposit-fiat', $currencySymbol);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteDepositRequest(Request $request)
    {
        $user = auth()->user();
        $orderRequest = Order::where('user_id', $user->id)->where("status", "0")->where("type", "deposit")->first();
        if ($orderRequest) {
            $orderRequest->status = 3;
            $orderRequest->save();
            //$orderRequest->delete();
            $is_success = true;
            $type = "success";
            $title = _i("Bilgi");
            $message = _i("%s referans kodlu %s tutarındaki para yatırma işleminiz talebiniz ile iptal edildi!");
            $message = sprintf($message, $orderRequest->transfer_code, $orderRequest->amount);
        } else {
            $is_success = false;
            $type = "error";
            $title = _i("Hata");
            $message = _i("Herhangi bir para yatırma işlemi talebiniz bulunamadı!");
        }

        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json($data, 200);
    }


    public function loadDepositRequests(Request $request) {
        $user = auth()->user();
        $startDate = \Carbon\Carbon::today();

        // kullanicinin cuzdanlarinin addresslerinin alinmasi


        //todo: btc_adddress ve wallet_addresses migrate edilecek
        $userWallets = $user->wallets()->get();




        $walletHashArr = [];
        $memoTagList = [];
        foreach($userWallets as $wallet){
             $walletHashArr = array_merge($walletHashArr, $wallet->address()->get()->pluck("address")->toArray());
             $memoTagList = array_merge($memoTagList, $wallet->address()->get()->pluck("id")->toArray());
        }


        foreach($memoTagList as &$item){

            $item = 10000000000 +  $item;
        }
        $memoTagList[]="-";
        $memoTagList[]="AIRDROP";



        $depositList = [];
        $depositRequests = CoinDeposit::whereIn("memo_tag",$memoTagList)->whereIn('address', $walletHashArr)->where("blockNumber","<>",0)->orderBy("updated_at","desc")->withTrashed()->get();

        foreach ($depositRequests as $deposit){
            $walletAddress = $deposit->wallet;
            if($walletAddress->coin->id == $deposit->coin->id){
                $depositList[] = array(
                    "address"=>$deposit->address,
                    "amount" => $deposit->amount,
                    "created_at" => $deposit->created_at,
                    "txid" => $deposit->txid,
                    "status" => $deposit->status
                );
            }
        }

        $datatable = DataTables::of($depositList)
            ->addColumn("coin", function($deposit){
                $coin = getCoinByPaymentAddress($deposit["address"]);
                if($coin){
                    $coin = sprintf("%s (%s)",$coin->name,$coin->symbol);
                }else{
                    $coin = "-";
                }
                return $coin;
            })
            ->addColumn("amount", function($deposit){
                return $deposit["amount"];
            })
            ->addColumn("date", function($deposit){
                return $deposit["created_at"];
            })
            ->addColumn("status", function($deposit){
                return DepositStatusCode($deposit["status"]);
            })
            ->addColumn("txinfo", function($deposit){

                $coin = getCoinByPaymentAddress($deposit["address"]);
                if($coin){
                    $explorerUrl = sprintf($coin->explorer,$deposit["txid"]);
                }else{
                    $explorerUrl = "-";
                }

                 return [
                    "txid"=>$deposit["txid"],
                    "explorer"=>$explorerUrl,
                ];
            });
        return $datatable->make(true);
    }

}