<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/9/18
 * Time: 12:36 AM
 */

namespace App\Http\Controllers\Personal;

use App\BtcAddress;
use App\Coin;
use App\Http\Controllers\Controller;
use App\Http\Controllers\RPC\ElectroneumController;
use App\Http\Controllers\RPC\ETHController;
use App\Http\Controllers\RPC\MoneroController;
use App\Http\Controllers\RPC\XLMController;
use App\Http\Controllers\RPC\XRPController;
use App\Wallet;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\WalletAddress;
use App\Http\Controllers\RPC\EthereumController;
use App\Http\Controllers\RPC\BitcoinController;
use App\Http\Controllers\RPC\OmniController;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();
//        $coins = Coin::where("status", 0)->where("deleted_at",null)->get();
//
//        $data = [
//            "coins" => $coins
//        ];

        if($user->is_metamask)
        {
            $view = "personal.maintenance";
        } else{
            $view = "personal.wallet";
        }

        return view($view);//, $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function createWallet(Request $request)
    {
        $user = auth()->user();

        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log("Kullanıcı, yeni bir cüzdan yaratma ekranını görüntüledi.");

        if (($request->filled("coinId")) && ($request->filled("walletName"))) {
            $coinId = $request->input("coinId");
            $walletName = $request->input("walletName");
            $is_success = true;
        } else {
            $is_success = false;
        }

        if ($is_success == true) {
            $coin = Coin::find($coinId);
            if ($coin) {

                $checkWallet = $user->wallets()->where("coin_id", $coin->id)->where("type", false)->first();
                if (!$checkWallet) {

                    switch ($coin->symbol) {


                        case "BTC":
                            $bitcoin = new BitcoinController();
                            $response = $bitcoin->createWallet($user, $coin, $walletName);
                            break;

                        case "USDT":
                            $omni = new OmniController();
                            $response = $omni->createWallet($user, $coin, $walletName);
                            break;

                        case "ETN":
                            $response = ElectroneumController::createWallet($user, $coin, $walletName);
                            break;

                        case "XMR":
                            $monero = new MoneroController();
                            $response = $monero->createWallet($user, $coin, $walletName);
                            break;

                        case "XLM":
                            $stellar = new XLMController();
                            $response = $stellar->createWallet($request,$user,$coin,$walletName);
                            break;
                        case "XRP":
                            $ripple = new XRPController();
                            $response = $ripple->createWallet($request,$user,$coin,$walletName);
                            break;
                        default: // ERC20 TOKENS
                            $ethereum = new ETHController();
                            $response = $ethereum->createWallet($request,$user,$coin,$walletName);
                            break;

                    }

                    if ($response["success"] == false) {
                        $message = _i("An error has occurred on the %s crypto currency server. Please try again later.");
                        $message = sprintf($message, $coin->name);
                        $title = _i("Failed To Create a Wallet!");
                        $is_success = false;
                        $type = "error";
                    } else {
                        $message = _i("%s crypto currency wallet has been created.");
                        $message = sprintf($message, $coin->name);
                        $title = _i("Info!");
                        $is_success = true;
                        $type = "success";

                        //NotifyWallet::dispatch($subscribe_endpoint."/{$coinId}/{$response->address}");
                    }

                } else {
                    $message = _i("You already have a wallet for the %s crypto currency!");
                    $message = sprintf($message, $coin->name);
                    $title = _i("Failed to create wallet!");
                    $is_success = false;
                    $type = "error";
                }
            } else {
                $message = _i("This crypto currency not defined in the system!");
                $title = _i("Error!");
                $is_success = false;
                $type = "error";
            }
        } else {
            $message = _i("No crypto currency selected or no wallet name entered!");
            $title = _i("Error!");
            $is_success = false;
            $type = "error";
        }

        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);

        $data = [
            "message" => $message,
            "title" => $title,
            "type" => $type,
            "success" => $is_success];

        return response()->json($data, 200);
    }

    public static function sendTransaction(Coin $coin, $receiver, $amount, $fee) {

       switch ($coin->symbol){
           case "XLM":


               break;
           default:
               break;
       }
/*
        switch ($coin->symbol) {
            case "ETH":
            case "KZE":
            case "NPXS":
            case "ALDX":
            case "HOT":
            case "OMG":
            case "REP":
            case "BNB":
            case "ENJ":
            case "HERB":
            case "DENT":
                $ethereum = new EthereumController();
                $response = $ethereum->sendTransaction($coin, $receiver, $amount, $fee);
                break;

            case "BTC":
                $bitcoin = new BitcoinController();
                $response = $bitcoin->sendTransaction($coin, $receiver, $amount, $fee);
                break;

            case "USDT":
                $omni = new OmniController();
                $response = $omni->sendTransaction($coin, $receiver, $amount, $fee);
                break;

            case "ETN":
                $response = ElectroneumController::sendTransaction($coin, $receiver, $amount, $fee);
                break;

            case "XMR":
                $monero = new MoneroController();
                $response = $monero->sendTransaction($coin, $receiver, $amount, $fee);
                break;

        }

*/

        $response = ["success"=>true,"response"=>"UNKNOWN_TX_FORMAT"];

        return $response;
    }

    public function getNewAddress(Request $request){

        $user = $request->user();

        $coin_id = $request->input("coinId");
        $coin = Coin::find($coin_id);

        switch ($coin->symbol) {
            case "BTC":
                $bitcoin = new BitcoinController();
                $response = $bitcoin->getNewAddress($user, $coin);
                break;

            case "USDT":
                $omni = new OmniController();
                $response = $omni->getNewAddress($user, $coin);
                break;

        }

        return $response;

    }

    public function loadWallets(Request $request){

        $user = auth()->user();
       // $userWallets = $user->wallets()->get();


        $userWallets = DB::table('wallets')
            ->rightJoin('coins', function ($join) {
                $join->on('wallets.coin_id', '=', 'coins.id')
                    ->where("wallets.user_id",auth()->user()->id);
            })->where("coins.status",0)->orderByRaw("coin_id","desc")->get();

        foreach ($userWallets as &$wallet) {
            if(($wallet->user_id) && ($wallet->coin_id)){
                $wallet->fixedBalance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user, false);
                $wallet->balance      = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
                $wallet->processingBalance = $wallet->fixedBalance - $wallet->balance;
            if ($wallet->type == false) {
                $wallet->fixedBalance = number_format($wallet->fixedBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                $wallet->balance = number_format(floatval($wallet->balance), $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                $wallet->processingBalance = number_format($wallet->processingBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                $wallet->can_create = false;
            }
            }else{
                $wallet->processingBalance = 0.00000000;
                $wallet->fixedBalance = 0.00000000;
                $wallet->can_create = true;
            }
        }

        $datatable = DataTables::of($userWallets)
            //->removeColumn("id")
            ->removeColumn("user_id")
            ->removeColumn("type")
           // ->removeColumn("coin_id")
            ->removeColumn("created_at")
            ->removeColumn("updated_at")
            ->removeColumn("deleted_at")
        ->addColumn("coin_icon", function($wallet){
            return asset("/images/coins/".strtolower($wallet->slug).".svg");
        })
            ->addColumn("balance", function($wallet){
                return $wallet->balance;
            })
        ->addColumn("coin", function($wallet){
            return $wallet->name;
        })
        ->addColumn("coin_symbol", function($wallet){
            return $wallet->symbol;
        })


            ->addColumn("action", function($wallet){
                if(($wallet->coin_id) && ($wallet->user_id)){
                    return array(
                        "deposit"=>
                            array(
                                "url"=>$wallet->type == true ? route("deposit-fiat",$wallet->symbol) : route("deposit-crypto",$wallet->symbol),
                                "title"=>_i("Deposit")
                                ),
                        "withdraw"=>array("url"=>$wallet->type == true ? route("withdraw-fiat",$wallet->symbol) : route("withdraw-crypto",$wallet->symbol),
                            "title"=>_i("Withdraw")));
                }else{
                    return array("create"=>array("title"=>"Create Wallet"), "deposit"=>array("url"=>"","title"=>""), "withdraw"=>array("url"=>"","title"=>""));
                }

            })

        ;
        return $datatable->make(true);
    }
}