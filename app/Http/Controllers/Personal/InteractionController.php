<?php

namespace App\Http\Controllers\Personal;

use App\Interaction;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use Plivo\RestClient;

class InteractionController extends Controller
{
/*    private $credentials = [
        "username",
        "password",
        "sender",
    ];*/

    private $credentials = [
        "authId",
        "authToken",
        "sender",
    ];

    /**
     * InteractionController constructor.
     */
    public function __construct()
    {
/*        $this->credentials["username"] = env("ILETIMERKEZI_USERNAME");
        $this->credentials["password"] = env("ILETIMERKEZI_PASSWORD");
        $this->credentials["sender"] = env("ILETIMERKEZI_SENDER");*/

        $this->credentials["authId"] = env("PLIVO_AUTH_ID");
        $this->credentials["authToken"] = env("PLIVO_AUTH_TOKEN");
        $this->credentials["sender"] = env("PLIVO_SENDER");

    }


    public function sendSms(Interaction $interaction) {
        $text = sprintf(SmsContent($interaction->content_type), $interaction->content);

        $client = new RestClient($this->credentials["authId"], $this->credentials["authToken"]);
        try {
            $response = $client->messages->create(
                $this->credentials["sender"],
                [$interaction->target],
                $text
            );

            $interaction->status = 1;
            $interaction->save();

            activity("interactions")->performedOn($interaction)->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                        ->log("Sms gonderimi başarılı");

            return true;
        } catch (\Throwable $exception) {
            activity("interactions")->performedOn($interaction)->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                ->log("Sms gönderirken hata oluştu");

            return false;
        }

    }

    /**
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    /*    public function sendSms(Interaction $interaction) {
            $text = sprintf(SmsContent($interaction->content_type), $interaction->content);
            $number = $interaction->user->calling_code.$interaction->user->phone;

            $xml = "
            <request>
                <authentication>
                    <username>{$this->credentials["username"]}</username>
                    <password>{$this->credentials["password"]}</password>
                </authentication>

                <order>
                    <sender>{$this->credentials["sender"]}</sender>
                    <message>
                        <text>{{$text}}</text>
                        <receipents>
                            <number>{{$number}}</number>
                        </receipents>
                    </message>
                </order>
            </request>";

            try {
    //            $client = new Client();
    //            $response = $client->request("POST", "https://api.iletimerkezi.com/v1/send-sms", [
    //                'headers' => ['Content-Type' => 'text/xml; charset=UTF8'],
    //                'body' => $xml
    //            ]);
    //
    //            if($response->getReasonPhrase() == "İşlem başarılı"){
                    $interaction->status = 1;
                    $interaction->save();

                    activity("interactions")->performedOn($interaction)->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                        ->log("Sms gonderimi başarılı");

                return true;
    //            }
            } catch (\Throwable $exception) {
                activity("interactions")->performedOn($interaction)->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                    ->log("Sms gonderirken hata olustu");

                return false;
            }

        }*/

}
