<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 13:43
 */

namespace App\Http\Controllers\Personal;

use App\Bank;
use App\Coin;
use App\Country;
use App\Currency;
use App\Http\Controllers\Controller;
use App\Order;
use App\UserBank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Yajra\DataTables\DataTables;

class WithdrawController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $user = auth()->user();

        $userWallets = DB::table('wallets')
            ->rightJoin('coins', function ($join) {
                $join->on('wallets.coin_id', '=', 'coins.id')
                    ->where("wallets.user_id",auth()->user()->id);
            })->where("coins.status",0)->orderByRaw("coin_id","desc")->get();



        foreach ($userWallets as $wallet) {
            if(($wallet->user_id) && ($wallet->coin_id)){
                $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
                if ($wallet->type == false) {
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                } else {
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                }
            }
        }

        foreach ($userWallets as &$wallet) {
            if(($wallet->user_id) && ($wallet->coin_id)){
                $wallet->fixedBalance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user, false);
                $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
                $wallet->processingBalance = $wallet->fixedBalance - $wallet->balance;
                $wallet->processingBalance = number_format($wallet->processingBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                if ($wallet->type == false) {
                    $wallet->fixedBalance = number_format($wallet->fixedBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                    $wallet->can_create = false;
                }
            }else{
                $wallet->processingBalance = "";
                $wallet->fixedBalance = "";
                $wallet->can_create = true;

            }
            $wallet->coin_icon = asset("/images/coins/".strtolower($wallet->slug).".svg");
            $wallet->minwithdraw = $wallet->min_withdraw;
        }


        $user_orders = $user->orders->where("type","withdraw");



        if($user->is_metamask)
        {
            $view = "personal.maintenance";
        } else{
            $view = "personal.withdraw.withdraw";
        }

        $data = [
            "wallets"=>$userWallets,
            "orders"=>$user_orders];
        return view($view,$data);
    }

    public function create(Request $request){
        $user = auth()->user();
        $withdrawAmount = $request->input("withdrawAmount");
        $withdrawTotal= $request->input("withdrawTotalAmount");
        $withdrawFee = $request->input("withdrawFee");
        $withdrawAddress = $request->input("withdrawAddress");
        $destinationTag = $request->input("withdrawTAG");
        $rules = [];
        $checkValid = Validator::make($request->all(), $rules);

        if(!$destinationTag){
            $destinationTag = "";
        }

        $is_success=true;
        $messageKey="";
        $messageValue="";

        if($user){
            $activeWallet = $request->input("activeWallet");
            $coin = Coin::where("symbol",$activeWallet)->get()->first();

            if($coin){
                $wallet = $user->wallets()->where("coin_id",$coin->id)->get()->last();


                if(!$wallet){
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue= _i("This crypto currency is not defined in your wallet account. Please create your wallet!");
                    $checkValid->errors()->add($messageKey, $messageValue);

                }

                if($wallet) {


                    $avaibleBalance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);

                    if ($avaibleBalance < ($withdrawTotal + $coin->fee)) {
                        $messageKey = "less_balance";
                        $messageValue = _i("Your wallet account does not have enough funds!");
                        $checkValid->errors()->add($messageKey, $messageValue);
                        $is_success = false;
                    }


                     if($withdrawTotal<=0){
                         $messageKey="wrong_amount";
                         $messageValue= _i("Please check the amount you have written!");
                         $checkValid->errors()->add($messageKey, $messageValue);
                         $is_success = false;
                     }

                    if($withdrawAmount<=0){
                        $messageKey="wrong_amount";
                        $messageValue= _i("Please check the amount you have written!");
                        $checkValid->errors()->add($messageKey, $messageValue);
                        $is_success = false;
                    }

                    $walletAddress = $wallet->address()->get()->last()->address;
                    if($walletAddress == $withdrawAddress){
                        $messageKey="wrong_receiver";
                        $messageValue=_("You can't send money to yourself!");
                        $checkValid->errors()->add($messageKey, $messageValue);
                        $is_success = false;
                    }

                    if($withdrawAmount < $coin->min_withdraw){
                        $messageKey="min_withdraw";
                        $messageValue=_i("Check the minimum withdraw amount.");
                        $checkValid->errors()->add($messageKey, $messageValue);
                        $is_success = false;
                    }

                    if($is_success==true){
                        $checkUserBank = $user->user_banks()->where("currency_type",$coin->type)->where("currency_id",$coin->id)->where("payment_address",$withdrawAddress)->first();
                        if(!$checkUserBank) {
                            $userBank = [
                                "user_id" => $user->id,
                                "bank_id" => -1,
                                "currency_type"=>$wallet->type,
                                "currency_id"=>$coin->id,
                                "iban_number"=>"-",
                                "name"=>$coin->name,
                                "surname"=>$coin->symbol,
                                "payment_address" => $withdrawAddress,
                                "destination_tag" => $destinationTag
                            ];
                            $checkUserBank = UserBank::create($userBank);
                        }

                        $orderArray = ["user_id" => $user->id,
                            "type" => "withdraw",
                            "currency_type"=>false,
                            "currency_id"=>-1,
                            "coin_id"=>$coin->id,
                            "amount" => $withdrawTotal,
                            "fee" => $coin->fee,
                            "bank_id" => $checkUserBank->id,
                            "transfer_code" => generateTransactionCode(),
                            "status"=>1
                        ];
                        $order = Order::create($orderArray);
                    }

                }



            }else{
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue= _i("This crypto currency is not defined in our database!");
                    $checkValid->errors()->add($messageKey, $messageValue);
            }
        }else{
            $is_success = false;
            $messageKey="invalid_user";
            $messageValue= _i("This user is not defined in our exchange!");
            $checkValid->errors()->add($messageKey, $messageValue);
        }

if($is_success){
    return redirect()->route('withdraw')->with("success",true);
}else{
   return redirect()->route('withdraw')->withErrors($checkValid)->withInput();
}
}


    public function listWithdrawRequests(Request $request) {
        $user = auth()->user();
        $startDate = \Carbon\Carbon::today();

        $limit = $request->input("limit");

        $withdrawRequests = $user->orders()->orderBy("id","desc")->withTrashed();

        $datatable = DataTables::of($withdrawRequests)
            ->addColumn("recid", function($withdraw){
                return [
                    "id" => $withdraw->id,
                    "status" => $withdraw->status
                ];
            })
            ->addColumn("id", function($withdraw){
                return "";
            })
            ->addColumn("coin", function($withdraw){
                return $withdraw->currency_type == true ? $withdraw->currency->name : $withdraw->coin->name;
            })
            ->addColumn("amount", function($withdraw){
                return $withdraw->amount;
            })
            ->addColumn("date", function($withdraw){
                return $withdraw->created_at;
            })
            ->addColumn("status", function($withdraw){
                return WithdrawStatusCode($withdraw->status);
            })
            ->addColumn("txid", function($withdraw){
               $explorerUrl = sprintf( $withdraw->coin->explorer, $withdraw->txId);
                return [
                    "txid"=>$withdraw->txId,
                    "explorer"=>$explorerUrl,
                ];
            })
            ->addColumn("ref", function($withdraw){
                return $withdraw->transfer_code;
            });
        return $datatable->make(true);
    }

    public function deleteWithdrawRequest(Request $request)
    {
        $user = auth()->user();
        $withdraw_id = $request->input("id");

        $orderRequest = Order::where('user_id', $user->id)
            ->where("id", $withdraw_id)
            ->where("status", "<=", 1)
            ->where("type", "withdraw")
            ->first();

        if ($orderRequest && $orderRequest->status <= 1) {
            $orderRequest->status = 4;
            $orderRequest->save();
            //$orderRequest->delete();
            $is_success = true;
            $type = "success";
            $title = _i("Bilgi");
            $message = _i("%s referans kodlu %s tutarındaki kripto para çekme talebiniz iptal edildi!");
            $message = sprintf($message, $orderRequest->transfer_code, $orderRequest->amount);

            activity("personal")
                ->causedBy($user)
                ->performedOn($orderRequest)
                ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
                ->log($message);
        } else {
            $is_success = false;
            $type = "error";
            $title = _i("Hata");
            $message = _i("Bekleyen kripto para çekme talebiniz bulunamadı!");
        }

        $data = [
            "success" => $is_success,
            "type" => $type,
            "title" => $title,
            "message" => $message];
        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function withdrawFiatCurrency(Request $request, $currencySymbol=null)
    {
        $user = auth()->user();

        if(!$currencySymbol){
            return redirect()->route("withdraw");
        }
        $currency = Currency::where("symbol",$currencySymbol)->get()->first();

        $currencies = Currency::orderBy("id","asc")->get();
        foreach($currencies as &$tempcurrency){
            $country = Country::where("currency_code",$tempcurrency->symbol)->get()->first();
            if($country){
                $tempcurrency->icon=$country->currency_symbol;
            }
        }

        $balance = 0.00;
        $balance = $user->user_balance(true,$currency->id,true);

        $bankAccounts = Bank::banks();
        $userBankAccounts = $user->user_banks()
            ->where("currency_type",true)
            ->where("currency_id",$currency->id)
            ->where('name', '<>', '')
            ->where('saved', 1)
            ->get();

        $data = [
            "bankAccounts"=>$bankAccounts,
            "userBankAccounts"=>$userBankAccounts,
            "currencies"=>$currencies,
            "currency"=>$currency,
            "balance"=>$balance
        ];

    $checkOrder = $user->orders()->where("currency_type",true)->where("currency_id",$currency->id)->where("status",0)->where("type","withdraw")->first();
    if ($checkOrder){
        return redirect()->route("withdraw-confirm-fiat",$currencySymbol);
        }
    return view("personal.withdraw.fiat-withdraw",$data);

    }


    /**
     * @param Request $request
     * @param null $currencySymbol
     * @param null $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptOrCancelWithdrawRequest(Request $request, $currencySymbol=null, $action=null){
        if((!$action) || (!$currencySymbol)){
            return redirect()->route("withdraw");
        }

        $currency = Currency::where("symbol",$currencySymbol)->get()->first();

        if((!$currency)){
            return redirect()->route("withdraw");
        }

        $user = auth()->user();
        $save_account = ($request->input("save_account") == "on");
        $account_name = $request->input("account_name");

        $checkOrder = $user->orders()->where("currency_type",true)->where("currency_id",$currency->id)->where("status",0)->where("type","withdraw")->first();
        if($checkOrder){
            if( $save_account == true ) {
                $userBank = $checkOrder->user_bank()->orderBy('id', 'DESC')->first();
                $userBank->update(array('name' => $account_name));
            }

            if($action == "cancel"){
                $checkOrder->user_bank()->orderBy('id', 'DESC')->first()->delete();
                $checkOrder->update(array('status' => 0));
                $checkOrder->delete();
            }else if($action =="accept"){
                $checkOrder->update(array(
                    'status' => 1,
                    'saved' => 1,
                ));
            }
        }
        return redirect()->route("withdraw");
    }

    /**
     * @param Request $request
     * @param null $currencySymbol
     * @param null $action
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptOrCancelCryptoWithdrawRequest(Request $request, $currencySymbol=null, $action=null){
        if((!$action) || (!$currencySymbol)){
            return redirect()->route("withdraw");
        }

        $currency = Coin::where("symbol",$currencySymbol)->get()->first();

        if((!$currency)){
            return redirect()->route("withdraw");
        }

        $user = auth()->user();
        $save_account = ($request->input("save_account") == "on");
        $account_name = $request->input("account_name");

        $checkOrder = $user->orders()->where("currency_type",false)->where("coin_id",$currency->id)->where("status",0)->where("type","withdraw")->first();
        if($checkOrder){
            if( $save_account == true ) {
                $userBank = $checkOrder->user_bank()->first();
                $userBank->update(array(
                    'name' => $account_name,
                    'saved' => 1,
                ));
            }

            if($action == "cancel"){
                $checkOrder->user_bank()->first()->delete();
                $checkOrder->update(array('status' => 0));
                $checkOrder->delete();
            }else if($action =="accept"){
                $checkOrder->update(array('status' => 1));
            }
        }
        return redirect()->route("withdraw");
    }


    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function confirmFiatWithdrawRequest(Request $request, $currencySymbol=null){
        $user = auth()->user();

        if(!$currencySymbol){
            return redirect()->route("withdraw");
        }
        $currency = Currency::where("symbol",$currencySymbol)->get()->first();

        $currencies = Currency::orderBy("id","asc")->get();
        foreach($currencies as &$tempcurrency){
            $country = Country::where("currency_code",$tempcurrency->symbol)->get()->first();
            if($country){
                $tempcurrency->icon=$country->currency_symbol;
            }
        }

        $balance = 0.00;
        $balance = $user->user_balance(true,$currency->id,true);


        $checkOrder = $user->orders()->where("currency_type",true)->where("currency_id",$currency->id)->where("status",0)->where("type","withdraw")->first();
        if (!$checkOrder){
           return redirect()->route("withdraw-fiat-create",$currency->symbol);
        }
        $bankInfo = $checkOrder->user_bank()->orderBy('id', 'DESC')->first();

        $ibanNo = $bankInfo->iban_number;
        $bankName = $bankInfo->bank->bank_name;

        $data = [
            "bank_name"=>$bankName,
            "iban"=>$ibanNo,
            "amount"=>$checkOrder->amount,
            "transfer_code"=>$checkOrder->transfer_code,
            "status"=>$checkOrder->status,
            "user_fullname"=>$user->name,
            "currencies"=>$currencies,
            "currency"=>$currency
        ];

        return  view("personal.withdraw.fiat-confirm",$data);
    }

    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function confirmCryptoWithdrawRequest(Request $request, $currencySymbol=null){
        $user = auth()->user();

        if(!$currencySymbol){
            return redirect()->route("withdraw");
        }
        $coin = Coin::where("symbol",$currencySymbol)->get()->first();

        $coins = Coin::orderBy("id","asc")->get();

        $checkOrder = $user->orders()->where("currency_type",false)->where("coin_id",$coin->id)->where("status",0)->where("type","withdraw")->first();
        if (!$checkOrder){
            return redirect()->route("withdraw-crypto-create",$coin->symbol);
        }
        $bankInfo = $checkOrder->user_bank()->first();

        $data = [
            "order"=>$checkOrder,
            "bank"=>$bankInfo,
            "coins"=>$coins,
            "coin"=>$coin
        ];

        return  view("personal.withdraw.crypto-confirm",$data);
    }


    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function withdrawCryptoCurrency(Request $request, $currencySymbol=null)
    {
        $user = auth()->user();

        if(!$currencySymbol){
            return redirect()->route("withdraw");
        }
        $coin = Coin::where("symbol",$currencySymbol)->get()->first();
        if(!$coin){
            return redirect()->route("withdraw");
        }

        $userWallets = DB::table('wallets')
            ->rightJoin('coins', function ($join) {
                $join->on('wallets.coin_id', '=', 'coins.id')
                    ->where("wallets.user_id",auth()->user()->id);
            })->where("coin_id",$coin->id)->orderByRaw("coin_id","desc")->get();

        foreach ($userWallets as $wallet) {
            if(($wallet->user_id) && ($wallet->coin_id)){
                $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
                if ($wallet->type == false) {
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                } else {
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                }
            }
        }

        foreach ($userWallets as &$wallet) {
            if(($wallet->user_id) && ($wallet->coin_id)){
                $wallet->fixedBalance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user, false);
                $wallet->balance = $user->user_balance(false, $wallet->coin_id, $wallet->type, true, $user);
                $wallet->processingBalance = $wallet->fixedBalance - $wallet->balance;
                $wallet->processingBalance = number_format($wallet->processingBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                if ($wallet->type == false) {
                    $wallet->fixedBalance = number_format($wallet->fixedBalance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                    $wallet->balance = number_format($wallet->balance, $wallet->decimals, $wallet->dec_point, $wallet->thousands_sep);
                    $wallet->can_create = false;
                }
            }else{
                $wallet->processingBalance = "";
                $wallet->fixedBalance = "";
                $wallet->can_create = true;

            }
            $wallet->coin_icon = asset("/images/coins/".strtolower($wallet->slug).".svg");
            $wallet->minwithdraw = $wallet->min_withdraw;
        }

        $user_orders = $user->orders->where("type","withdraw");


        $data = [
            "wallets"=>$userWallets,
            "orders"=>$user_orders];
        return view("personal.withdraw.withdraw",$data);

    }


    /**
     * @param Request $request
     * @param null $currencySymbol
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createWithdrawRequest(Request $request, $currencySymbol=null) {
        $user = auth()->user();

       // dd($request->all());

        if(!auth()->user()->identityApproval(2)) {
            return redirect()->route("withdraw");
        }

        $bankId = $request->input("bank_id");
        $amount_integer = $request->input("amount_integer");
        $amount_decimal = $request->input("amount_decimal");
        $currency_id = $request->input("currency_id");
        $currency_type = $request->input("currency_type");
        $coin_id = $request->input("coin_id");
        $iban = $request->input("iban");

        $bank = Bank::find($bankId);

        if(!$currencySymbol){
            return redirect()->route("withdraw");
        }
        $currency_type = $currency_type == 0 ? false : true;


        if($currency_type == true){
            $currency = Currency::where("symbol",$currencySymbol)->where("id",$currency_id)->get()->first();
            if(!$currency){
                return redirect()->route("withdraw");
            }
        }else{
            $coin = Coin::where("symbol",$currencySymbol)->where("id",$currency_id)->get()->first();
            $coin_fee = floatval($coin->fee);

            if(!$coin){
                return redirect()->route("withdraw");
            }
        }


        if($currency_type == true){

            $rules = [
                'bank_id' => 'required|integer|min:0',
                'amount_integer' => 'required|integer|min:100',
                'amount_decimal' => 'nullable|numeric|min:0',
                'currency_id' => 'required|integer|min:0',
                'currency_type' => 'required|boolean',
                "iban"=>'required|string|min:20|max:50'
            ];

            $checkValid = Validator::make($request->all(), $rules);
            $withdrawAmount = floatval($amount_integer.".".$amount_decimal);



            if($checkValid->fails()) {

                $is_success=false;
                $messageKey="";
                $messageValue="";

                $wallet = $user->wallets()->where("coin_id",$currency->id)->where("type",true)->get()->first();
                $walletBalance = $user->user_balance(false,$currency->id,true,true);

                if(!$wallet){
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue=_i("Bu para birimine ait cüzdan hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!");
                }else{
                    $is_success=true;
                }

                if($is_success==false){
                    $checkValid->errors()->add($messageKey, $messageValue);
                }

                if($wallet && $walletBalance<$withdrawAmount){
                    $messageKey="less_balance";
                    $messageValue=_i("Cüzdan hesabınızda yeterli bakiye bulunmamaktadır!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                }
                return redirect()->route('withdraw-fiat-create',$currencySymbol)->withErrors($checkValid)->withInput();
            }else{

                $is_success=true;
                $messageKey="";
                $messageValue="";

                $wallet = $user->wallets()->where("coin_id",$currency->id)->where("type",true)->get()->first();
                $walletBalance = $user->user_balance(false,$currency->id,true,true);

                if(!$wallet){
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue=_i("Bu para birimine ait cüzdan hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!");
                    $checkValid->errors()->add($messageKey, $messageValue);

                }

                if($wallet && $walletBalance<$withdrawAmount){
                    $messageKey="less_balance";
                    $messageValue=_i("Cüzdan hesabınızda yeterli bakiye bulunmamaktadır!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                    $is_success = false;
                }

                if ($is_success == false){
                     return redirect()->route('withdraw-fiat-create',$currencySymbol)->withErrors($checkValid)->withInput();
                }
            }

            $checkUserBank = $user->user_banks()->where("currency_type",true)->where("currency_id",$currency->id)->where("iban_number",$iban)->first();
            if(!$checkUserBank) {
                $userBank = [
                    "user_id" => $user->id,
                    "bank_id" => $bank->id,
                    "currency_type"=>$currency_type,
                    "currency_id"=>$currency->id,
                    "name" => "",
                    "surname" => "",
                    "iban_number" => $iban
                ];
                $checkUserBank = UserBank::create($userBank);
            }

            $orderArray = ["user_id" => $user->id,
                "type" => "withdraw",
                "currency_type"=>true,
                "currency_id"=>$currency_id,
                "coin_id"=>-1,
                "amount" => floatval($amount_integer.".".$amount_decimal),
                "bank_id" => $checkUserBank->id,
                "transfer_code" => generateTransactionCode()
            ];
            $order = Order::create($orderArray);
        }else{



            $rules = [
                'amount_integer' => 'required|integer',
                'amount_decimal' => 'nullable|numeric|min:0',
                'currency_id' => 'required|integer|min:0',
                'currency_type' => 'required|boolean',
                "iban"=>'required|string|min:10|max:70'
            ];

            $checkValid = Validator::make($request->all(), $rules);

            if($checkValid->fails()) {

                $is_success=false;
                $messageKey="";
                $messageValue="";

                $wallet = $user->wallets()->where("coin_id",$coin->id)->where("type",false)->get()->first();
                $walletBalance = $user->user_balance(false,$coin->id,false,true);

                $withdrawAmount = floatval($amount_integer.".".$amount_decimal);

                if(!$wallet){
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue= _i("Bu para birimine ait cüzdan hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!");
                }else{
                    $is_success=true;
                }

                if($is_success==false){
                    $checkValid->errors()->add($messageKey, $messageValue);
                }

                if($wallet && $walletBalance<$withdrawAmount+$coin_fee){
                    $messageKey="less_balance";
                    $messageValue= _i("Cüzdan hesabınızda yeterli bakiye bulunmamaktadır!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                }

                return redirect()->route('withdraw-crypto-create',$currencySymbol)->withErrors($checkValid)->withInput();
            }else{

                $is_success=true;
                $messageKey="";
                $messageValue="";

                $wallet = $user->wallets()->where("coin_id",$coin->id)->where("type",false)->get()->first();
                $walletBalance = $user->user_balance(false,$coin->id,false,true);
                $withdrawAmount = floatval($amount_integer.".".$amount_decimal);

                if(!$wallet){
                    $is_success = false;
                    $messageKey="invalid_wallet";
                    $messageValue= _i("Bu kripto para birimine ait cüzdan hesabınızda tanımlı değildir. Lütfen cüzdan tanımlayınız!");
                    $checkValid->errors()->add($messageKey, $messageValue);

                }

                if($wallet && $walletBalance<$withdrawAmount+$coin_fee){
                    $messageKey="less_balance";
                    $messageValue= _i("Cüzdan hesabınızda yeterli bakiye bulunmamaktadır!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                    $is_success = false;
                }

                if($withdrawAmount==0){
                    $messageKey="wrong_amount";
                    $messageValue= _i("Lütfen yazdığınız tutarı kontrol ediniz!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                    $is_success = false;
                }

                $walletAddress = $wallet->address()->get()->last()->address;
                if($walletAddress == $iban){
                    $messageKey="wrong_receiver";
                    $messageValue=_("Kendinize gönderim yapamazsınız!");
                    $checkValid->errors()->add($messageKey, $messageValue);
                    $is_success = false;
                }

                if($withdrawAmount < $coin->min_withdraw){
                    $messageKey="min_withdraw";
                    $messageValue=_i("Minimum çekim tutarınına dikkat ediniz.");
                    $checkValid->errors()->add($messageKey, $messageValue);
                    $is_success = false;
                }

                if ($is_success == false){
                    return redirect()->route('withdraw-crypto-create',$currencySymbol)->withErrors($checkValid)->withInput();
                }
            }

            $checkUserBank = $user->user_banks()->where("currency_type",$currency_type)->where("currency_id",$coin->id)->where("payment_address",$iban)->first();
            if(!$checkUserBank) {
                $userBank = [
                    "user_id" => $user->id,
                    "bank_id" => -1,
                    "currency_type"=>$currency_type,
                    "currency_id"=>$coin->id,
                    "iban_number"=>"-",
                    "name"=>$coin->name,
                    "surname"=>$coin->symbol,
                    "payment_address" => $iban,
                    "destination_tag" => $bankId
                ];
                $checkUserBank = UserBank::create($userBank);
            }

            $orderArray = ["user_id" => $user->id,
                "type" => "withdraw",
                "currency_type"=>false,
                "currency_id"=>-1,
                "coin_id"=>$coin->id,
                "amount" => $withdrawAmount,
                "fee" => $coin_fee,
                "bank_id" => $checkUserBank->id,
                "transfer_code" => generateTransactionCode()
            ];
            $order = Order::create($orderArray);


        }

        if($currency_type == true){
            return redirect()->route('withdraw-fiat',$currencySymbol);
        }else{
            return redirect()->route('withdraw-crypto',$currencySymbol);
        }

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete_bank_account(Request $request) {
        $user = auth()->user();

        $id = $request->input("id");
        $bank = $user->user_banks()->where("id",$id)->first();

        if( $bank ) {
            $bank->delete();
            $success = true;
        }

        return response()->json(["success" => $bank],200);
    }

}