<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 15.09.2018
 * Time: 08:56
 */

namespace App\Http\Controllers\Personal;

use App\Coin;
use App\Country;
use App\Currency;
use App\Http\Controllers\Controller;
use App\UserApiKey;
use App\IdentityApproval;
use function foo\func;
use PragmaRX\Google2FA\Google2FA;
use Spatie\Activitylog\Models\Activity;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;
use App\Http\Controllers\TwoFactorController;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\UserSetting;
use Illuminate\Support\Facades\Validator;

class SettingsController extends Controller
{

    public function index(){
        $user = auth()->user();
        $countries = Country::all();

        $userSettings = $user->user_setting()->first();

        if(!$userSettings) {
            $userSettings = UserSetting::create([
                'user_id' => $user->id,
            ]);
        }

        $data = [
            "user"=>$user,
            "countries"=>$countries,
            "countryId"=>$userSettings->country_id,
            "city"=>$userSettings->city,
        ];

        return view("personal.settings.account", $data);
    }

    public function updateAccountDetails(Request $request) {
        $user = auth()->user();

        $validatedData = $request->validate([
            'country' => 'required|string',
            'city' => 'required|string',
            'calling_code' => 'required|integer',
            'phone_number' => 'required|string|min:10|max:15',
        ]);

        if($validatedData) {
            $user_setting = $user->user_setting;
            $user_setting->country_id = $request->input("country");
            $user_setting->city = $request->input("city");
            $user_setting->save();

            $callingCode = $request->input('calling_code');
            $phoneNumber = $request->input('phone_number');
            $user->phone=$callingCode.$phoneNumber;
            $user->save();

            return redirect()->back();
        } else {
            return redirect()->back()->withErrors($validatedData);
        }
    }


    public function listTransactions($transactionStatus=null) {
        $user = auth()->user();

        $data = [
            "transactionStatus"=>$transactionStatus
        ];

        return view("personal.settings.orders",$data);
    }

    public function activityLogs(Request $request){
        $user = auth()->user();
        $data = [];

        $message = _i("Kullanıcı, sistem üzerindeki hareket kayıtlarını görüntüledi.");
        activity("personal")
            ->causedBy($user)
            ->performedOn($user)
            ->withProperties(['ip_address' => request()->ip(), 'user_agent' => request()->server('HTTP_USER_AGENT')])
            ->log($message);

        return view("personal.settings.logs",$data);
    }

    public function loadActivityLogs(Request $request){
        $user = auth()->user();
        $startDate = \Carbon\Carbon::today();
        $logHistory = Activity::where("causer_id",$user->id)->orderBy("updated_at","desc")->get();

        /*
        $lastLoggedActivity->subject; //returns an instance of an eloquent model
        $lastLoggedActivity->causer; //returns an instance of your user model
        $lastLoggedActivity->getExtraProperty('customProperty'); //returns 'customValue'
        $lastLoggedActivity->description; //returns 'Look, I logged something'
        */

        $datatable = DataTables::of($logHistory)
            ->addColumn("id", function($log){
                return "";
            })
            ->addColumn("date", function($log){
                return $log->created_at;
            })
            ->addColumn("ip", function($log){
                return $log->getExtraProperty('ip_address');
            })
            ->addColumn("platform", function($log){
                return $log->getExtraProperty('user_agent');
            })

            ->addColumn("description", function($log){
                return $log->description;
            })

        ;
        return $datatable->make(true);
    }

    public function loadUserOrderListHistory(){
        $user = auth()->user();
        $orderList = $user->orders;

        $datatable = DataTables::of($orderList)
        ->editColumn("type", function($order){
            $szType = $order->type == "deposit" ? _i("Deposit") : _i("Withdraw");
            return $szType;
        })
        ->addColumn("subject", function($order){
            return "";
        })
        ->addColumn("currency", function($order){
            if($order->currency_type == true){
              $currency = Currency::where("id",$order->currency_id)->get()->first();
            }else{
              $currency = Coin::where("id",$order->coin_id)->get()->first();
            }
            return sprintf("%s (%s)",$currency->name,$currency->symbol);
        })
        ->addColumn("status", function($order){
            if($order->type=="deposit"){
                $szStatus = DepositStatusCode($order->status);
            }else if($order->type=="withdraw"){
                $szStatus = WithdrawStatusCode($order->status);
            }
            return $szStatus;
        })
        ->addColumn("description", function($order){
            return $order->description;
        })
        ->addColumn("date", function($order){
            return $order->updated_at;
        })
        ->addColumn("amount", function($order){
            return $order->amount;
        })
        ->addColumn("fee", function($order){
            return $order->fee;
        });

        return $datatable->make(true);
    }

    public function accountActivityLogs(Request $request){
        $user = auth()->user();
        $data = [];
        return view("personal.settings.account-logs",$data);
    }

    public function accountVerification(Request $request) {
        $approval = IdentityApproval::where("user_id", $request->user()->id)->where("status", ">=", 0)->latest()->first();

        if($approval) {
            return view("personal.settings.verification")->with("approval", $approval);
        }

        return view("personal.settings.verification");
    }

    public function securityPreferences(Request $request) {

        $user = auth()->user();

        if($user->two_factor < 2) { //Google2FA aktif değilse
            $Google2FAController = new TwoFactorController();
            $secret = $Google2FAController->generateSecret();

            $Google2FA = new Google2FA();
            $imageDataUri = $Google2FA->getQRCodeUrl(
                $request->getHttpHost(),
                $user->email,
                $secret
            );

            $data = [
                "is_active" => false,
                "image" => $imageDataUri,
                "secret" => $secret,
                "user" => $user,
            ];
        } else { //Google 2fa aktif ise
            $data = [
                "is_active" =>true,
                "user" => $user,
            ];
        }

        return view("personal.settings.security",$data);
    }


    public function contactPreferences(Request $request){
        $user = auth()->user();

        $data = [
            "settings"=>$user->user_setting,
        ];

        //dd($data);

        return view("personal.settings.contact",$data);
    }

    public function updateContactPreferences(Request $request){
        $user = auth()->user();

        $user_setting = $user->user_setting;
        $user_setting->crypto_deposit_email = boolval($request->input("EmailOnCryptoDeposit"));
        $user_setting->crypto_deposit_sms = boolval($request->input("SmsOnCryptoDeposit"));
        $user_setting->crypto_withdraw_email = boolval($request->input("EmailOnCryptoWithdraw"));
        $user_setting->crypto_withdraw_sms = boolval($request->input("SmsOnCryptoWithdraw"));
        $user_setting->save();

        $data = [
            "settings"=>$user_setting,
        ];

        activity("user_settings")->performedOn($user_setting)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("İletişim tercihleri güncellendi"));

        return view("personal.settings.contact",$data);
    }


    public function changePassword(Request $request){
        $user = \auth()->user();

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches

            activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
                ->log(_i("Başarısız parola değiştirme girişimi."));

            return redirect()->back()->with("error",_i("Mevcut parolanızı doğru yazdığınızdan emin olun ve tekrar deneyin."));
        }
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","Yeni parolanız eskisiyle aynı olamaz. Lütfen yeni bir parola yazın.");
        }
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        ]);
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("Giriş parolası güncellendi."));

        return redirect()->back()->with("success","Parolanızı değiştirdiniz!");
    }

}
