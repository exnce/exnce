<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 3/22/19
 * Time: 2:08 PM
 */

namespace App\Http\Controllers\Personal;


use App\Http\Controllers\Controller;
use App\Ticket;
use App\TicketCategory;
use App\TicketComment;
use App\TicketPriority;
use App\TicketStatus;
use function foo\func;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class SupportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        $user = auth()->user();


        if($user){
            $unsolved = 0;
            $solved = 0;
            $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
            $solved = $user->tickets->where("status_id", $solvedStatusId)->count();
            $unsolved = $user->tickets->where("status_id","<>", $solvedStatusId)->count();

            $data = [
                "user"=>$user,
                "solved" => $solved,
                "unsolved" => $unsolved
            ];

            return view("personal.support.index", $data);
        }else{
            return redirect()->route("login");
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(){
        $user = auth()->user();

        if($user){
            $unsolved = 0;
            $solved = 0;
            $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
            $solved = $user->tickets->where("status_id", $solvedStatusId)->count();
            $unsolved = $user->tickets->where("status_id","<>", $solvedStatusId)->count();

            $data = [
                "user"=>$user,
                "priorities"=>getSupportPriority(),
                "categories"=>getSupportCategories(),
                "solved" => $solved,
                "unsolved" => $unsolved
            ];
            return view("personal.support.create", $data);
        }else{
            return redirect()->route("login");
        }

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function loadSupportRequests(Request $request){
        $user = auth()->user();
        $isCompleted = $request->post("is_completed");

        if($user){

            $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
            if($isCompleted == true) {
                $operator = "=";
            }else{
                $operator = "<>";
            }

            $tickets = $user->tickets()->where("status_id",$operator,$solvedStatusId);
            $datatable = DataTables::of($tickets)
                ->addColumn("agent", function($ticket){
                    $agentName = "-";
                    if($ticket->agent){
                        $agentName = $ticket->agent->name;
                    }
                    return $agentName;
                })
                ->addColumn("date", function($ticket){
                    return  $ticket->updated_at;
                })
                ->addColumn("color", function ($ticket){
                    return  $ticket->status->color;
                })
                ->addColumn("status", function($ticket){
                    return  _i($ticket->status->name);
                })
                ->addColumn("detail",function ($ticket){
                    return route("support-detail",$ticket->id);
                });
            return $datatable->make(true);
        }else{
            return redirect()->route("login");
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createSupportRequest(Request $request){
       $user = auth()->user();
       $this->validate($request, [
            'subject'     => 'required|min:3',
            'description'     => 'required|min:6',
            'priority' => 'required|exists:ticket_priorities,key',
            'category' => 'required|exists:ticket_categories,key',
        ]);

       if($user){
            $ticket = new Ticket();
            $ticket->content = $request->input("description");
            $ticket->subject = $request->input("subject");
            $ticket->priority_id = TicketPriority::where("key",$request->input("priority"))->get()->first()->id;
            $ticket->category_id = TicketCategory::where("key", $request->input("category"))->get()->first()->id;
            $ticket->user_id = $user->id;
            $ticket->agent_id = -1;
            $ticket->status_id = TicketStatus::where("key","pending")->get()->first()->id;
            $ticket->save();
            $message = _i("Your support request has been successfully created.");
            return redirect()->back()->with('message', $message);
       }else{
            return redirect()->route("login");
       }
    }

    public function ticketDetail($ticketId = null){
        $user = auth()->user();
        if($user){
            if($ticketId){
                $ticket = $user->tickets()->where("id",$ticketId)->get()->first();

                if($ticket){

                    $unsolved = 0;
                    $solved = 0;
                    $solvedStatusId = TicketStatus::where("key","solved")->get()->first()->id;
                    $solved = $user->tickets->where("status_id", $solvedStatusId)->count();
                    $unsolved = $user->tickets->where("status_id","<>", $solvedStatusId)->count();

                    $data = [
                        "user"=>$user,
                        "solved" => $solved,
                        "unsolved" => $unsolved,
                        "ticket" => $ticket
                    ];

                    return view("personal.support.detail", $data);
                }else{
                    $message = _i("No such support request was found!");
                    return redirect()->back()->with('message', $message);
                }
            }
        }
        else{
            return redirect()->route("login");
        }

    }

    public function reopenTicket($ticketId = null){
        $user = auth()->user();

        if($user){
            if($ticketId){
                $ticket = $user->tickets()->where("id",$ticketId)->get()->first();
                if($ticket){
                    $ticket->status_id = 1;
                    $ticket->save();
                    $message = _i("The support request is reactivated.");
                    return redirect()->back()->with('message', $message);
                }else{
                    $message = _i("No such request for support has been found!");
                    return redirect()->back()->with('message', $message);
                }
            }
        }
        else{
            return redirect()->route("login");
        }

    }

    public function sendComment(Request $request, $ticketId = null){
        $user = auth()->user();

        if($user){
            if($ticketId){
                $ticket = $user->tickets()->where("id",$ticketId)->get()->first();
                if($ticket){
                    $ticketComment = new TicketComment();
                    $ticketComment->content = $request->input("comment");
                    $ticketComment->user_id = $user->id;
                    $ticketComment->ticket_id = $ticket->id;
                    $ticketComment->save();
                    return redirect()->back();
                }else{
                    $message = _i("No such request for support has been found!");
                    return redirect()->back()->with('message', $message);
                }
            }
        }
        else{
            return redirect()->route("login");
        }

    }





}