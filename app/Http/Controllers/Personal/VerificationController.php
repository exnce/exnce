<?php
/**
 * Created by PhpStorm.
 * User: oem
 * Date: 7/23/18
 * Time: 3:04 AM
 */

namespace App\Http\Controllers\Personal;

use App\Country;
use App\Http\Controllers\Controller;
use App\SecurityPhoto;
use App\User;
use App\Interaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\BillingDetail;
use Illuminate\Support\Carbon;

class VerificationController extends Controller
{

    private $timelimit = 120;


    public function verifyEmail(Request $request){

        $data = [];
        return view("personal.verification.email",$data);

    }


    /**
     * @param Request $request
     * @param null $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function verifyEmailRequest(Request $request, $token=null){

        if(!$token){
            return redirect()->route('login');
        }

        $user = User::where("email_token",$token)->first();

        if(!$user){
            $is_success = false;
            $type="error";
            $message = _i("There is no such user registered to the system!");
            $title = _i("Email cannot be verified!");
        }else{

            $user->verified=1;
            $user->save();
            $is_success = true;
            $type="success";
            $message = _i("Your account has been successfully verified. You can login with your email address and password.");
            $title = _i("Email Verified!");
        }

        $data = [
            "success"=>$is_success,
            "type"=>$type,
            "title"=>$title,
            "message"=>$message];
        return view("personal.verification.email-confirm",$data);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function verifyMobile(Request $request){
        $user = auth()->user();

        if($user->verified == 0){
            return redirect()->route('verify-email');
        }else if($user->verified == 3){
            return redirect()->route('dashboard');
        }else if($user->verified==2){
            return redirect()->route('verify-sms');
        }

        if(($request->filled("calling_code")) || ($request->filled("phone_number"))){
            $callingCode = $request->input('calling_code');
            $phoneNumber = $request->input('phone_number');

            $rules = [
                'calling_code' => 'required|integer',
                'phone_number' => 'required|string|min:6|max:15',
            ];

            $checkValid = Validator::make($request->all(), $rules);
            if($checkValid->fails()) {
                return redirect()->route('verify-mobile')->withErrors($checkValid)->withInput();
            }

            $last_sms = $user->lastSms(0); //Register mobil tanimalama tipi
            if( ($last_sms && (time() - $last_sms->created_at->timestamp) >= $this->timelimit) || !$last_sms ) {
                $sms_code = rand(100000,999999);
                $last_sms = Interaction::create([
                    'user_id' => $user->id,
                    'type' => 'sms',
                    'target' => $callingCode.$phoneNumber,
                    'content' => $sms_code,
                    'content_type'=> 0,
                ]);
            }

            $smsapi = new InteractionController();
            $sentSms = $smsapi->sendSms($last_sms);

            if(!$sentSms) {
                $last_sms->status = -1;
                $last_sms->save();
                auth()->logout();
                return redirect()->route("login")->with("login:error", _i("Şuan SMS gönderiminde merkez kaynaklı bir hata bulunmakta. Daha sonra tekrar deneyiniz."));
            }

            //sms gonderme başarılı ise dogrulama adimina gec
            $user->phone=$callingCode.$phoneNumber;
            $user->verified=2;
            $user->save();
            
            return redirect()->route('verify-sms');
        }

        if($user) {
            $image = base64_encode( SecurityPhoto::find($user->security_photo)->first()->image);
        }
        $countryList = Country::select('id', 'name', 'calling_code')
            ->orderByRaw("FIELD(id,792) DESC")
            ->orderBy("name","asc")->get();
        $data = [
            "image"=>$image,
            "user"=>$user,
            "countries"=>$countryList
        ];
        return view("personal.verification.mobile",$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function verifySMS(Request $request){

        $user = auth()->user();

        if($user->verified == 0){
            return redirect()->route('verify-email');
        }

        if($user->verified == 1){
            return redirect()->route('verify-mobile');
        }

        if($user->verified == 3){
            return redirect()->route('dashboard');
        }

        $last_sms = $user->lastSms(0); //Register mobil tanimalama tipi

        if($request->filled("sms_code")) {
            $rules = [
                'sms_code' => 'required|digits:6'
            ];
            $checkValid = Validator::make($request->all(), $rules);
            if($checkValid->fails()) {
                return redirect()->route('verify-sms')->withErrors($checkValid)->withInput();
            }
            $sms_code = $request->input('sms_code');

            if($last_sms->content == $sms_code){
                $last_sms->status = 1;
                $last_sms->save();

                $user->verified=3;
                $user->two_factor=1; // 2fa default olarak sms ayarliyoruz
                $user->save();

                return redirect()->route('dashboard');
            }
        }


        if( ($last_sms && (time() - $last_sms->created_at->timestamp) >= $this->timelimit) || !$last_sms ) {
            $sms_code = rand(100000,999999);
            $last_sms = Interaction::create([
                'user_id' => $user->id,
                'type' => 'sms',
                'target' => $user->phone,
                'content' => $sms_code,
                'content_type'=> 0,
            ]);

            $smsapi = new InteractionController();
            $sentSms = $smsapi->sendSms($last_sms);

            if(!$sentSms) {
                $last_sms->status = -1;
                $last_sms->save();
                auth()->logout();
                return redirect()->route("login")->with("login:error", _i("Şuan SMS gönderiminde merkez kaynaklı bir hata bulunmakta. Daha sonra tekrar deneyiniz."));
            }
        }

        $image = base64_encode( SecurityPhoto::find($user->security_photo)->first()->image);

        $timeleft = ($this->timelimit - (time() - $last_sms->created_at->timestamp));
        $minutes = floor(($timeleft / 60) % 60);
        $seconds = $timeleft % 60;


        $data = [
            "userName"=>$user->name,
            "userPhone"=>$user->phone,
            "image"=>$image,
            "sms"=>$last_sms->content,
            "minutes"=>$minutes,
            "seconds"=>$seconds,
        ];
        return view("personal.verification.sms",$data);
    }

}