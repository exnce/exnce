<?php
/**
 * Created by PhpStorm.
 * User: e
 * Date: 3/18/19
 * Time: 5:04 PM
 */

namespace App\Http\Controllers\Personal;


use App\Http\Controllers\Controller;
use App\User;
use App\UserReference;
use Yajra\DataTables\DataTables;
use Illuminate\Http\Request;

class ReferralsController extends Controller
{

    public function index(Request $request)
    {
        $user = $request->user();
        $data = ["user"=>$user];
        return view("personal.settings.references.index",$data);
    }

    public function loadAccountRReferrals(Request $request){
        $user = auth()->user();
        $data = [];

        $len = $request->input("length");

        if($user) {

            $data = UserReference::with('user')->where('reference_id',$user->id);
            $datatable = DataTables::of($data)
                ->addColumn("name", function($ref_user){
                    if($ref_user->user){
                      return get_starred($ref_user->user->name);
                    }
                    return "-";
                })
                ->addColumn("email", function($ref_user){
                    if($ref_user->user){
                        return  get_starred_email($ref_user->user->email);
                    }
                    return "-";
                })
                ->addColumn("date", function($ref_user){
                    return $ref_user->created_at;
                })
                ->addColumn("referral_fee", function($ref_user){
                    if($ref_user->user){
                        return calculateUserReferralBalance($ref_user->user->id);
                    }
                    return "0.0000000";
                })
                ->removeColumn("created_at")
                ->removeColumn("updated_at");
            return $datatable->make(true);
        } else {
            return redirect()->back();
        }
    }

}