<?php

namespace App\Http\Controllers\Personal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\IdentityApproval;
use Illuminate\Support\Facades\Storage;

class IdentityApprovalController extends Controller
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verifyId(Request $request){
        $user = auth()->user();

        if($user->identityApproval(0)) {
            return redirect()->back();
        }

        $validatedData = $request->validate([
            'id_card_front' => 'required|file|max:5120|mimes:jpg,jpeg,bmp,png,pdf',
            'selfie' => 'required|file|max:5120|mimes:jpg,jpeg,bmp,png,pdf',
        ]);

        if($validatedData) {
            $req_id_card_front = $request->file("id_card_front");
            $req_selfie = $request->file("selfie");
            
            $str_hash = randHash();
            $str_id_card_front = $req_id_card_front->getClientOriginalName();
            $str_selfie = $req_selfie->getClientOriginalName();

            $req_id_card_front->storeAs('identity_approvals', $str_hash.$str_id_card_front);
            $req_selfie->storeAs('identity_approvals', $str_hash.$str_selfie);

            IdentityApproval::create([
                "user_id" => $user->id,
                "hash" => $str_hash,
                "id_card_front" => $str_id_card_front,
                "selfie" => $str_selfie,
            ]);

            activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
                ->log(_i("Kimlik belgeleri onaya gönderildi."));

            return redirect()->route("account-verification");
        } else {
            return redirect()->route("account-verification")->withErrors($validatedData);
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancelVerifyId(Request $request) {
        $user = auth()->user();
        $waitingApproval = IdentityApproval::where("user_id", $request->user()->id)->where("status", 0)->latest()->first();

        $path_id_card_front =   "identity_approvals/" . $waitingApproval->hash . $waitingApproval->id_card_front;
        $path_selfie        =   "identity_approvals/" . $waitingApproval->hash . $waitingApproval->selfie;
        Storage::delete([$path_id_card_front, $path_selfie]);

        $waitingApproval->status = -1;
        $waitingApproval->save();
        $waitingApproval->delete();

        activity("users")->performedOn($user)->withProperties(['ip_address' => request()->ip(),'user_agent'=>request()->server('HTTP_USER_AGENT')])
            ->log(_i("Kimlik onayı iptal edildi."));

        return redirect()->route("account-verification");
    }
}
