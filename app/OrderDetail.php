<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = [
            "order_id",
            "txSeq",
            "txFrom",
            "txTo",
            "txAmount",
            "txCurrency",
            "txDesc",
            "txBlock",
            "txHash",
            "txCreateDate",
            "txConfirmDate",
            "txConfirmRate",
            "txStatus",
            "txFee",
            "txData",
            "txPrevHash",
            "json_data"
        ];
}
