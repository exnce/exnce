<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Merchant extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = ["id","name","phone","address","country"];
}
