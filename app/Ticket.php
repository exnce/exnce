<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends Model
{
    use SoftDeletes;

    protected $fillable = ["subject","content","html","status_id","prority_id","user_id","agent_id","category_id"];
    protected $dates = ['completed_at'];


    public function status(){
        return $this->belongsTo(TicketStatus::class,"status_id","id");
    }

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }


    public function agent(){
        return $this->belongsTo(User::class,"agent_id","id");
    }

    public function priority(){
        return $this->belongsTo(TicketPriority::class,"priority_id","id");
    }

    public function category(){
        return $this->belongsTo(TicketCategory::class,"category_id","id");
    }

    public function comments(){
        return $this->hasMany(TicketComment::class,"ticket_id","id");
    }

}
