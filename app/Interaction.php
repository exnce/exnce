<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Interaction extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = ["user_id","type","target","content","content_type","status"];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

}
