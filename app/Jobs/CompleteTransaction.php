<?php

namespace App\Jobs;

use App\Coin;
use App\Currency;
use App\Http\Controllers\Personal\MarketController;
use App\Transaction;
use App\TransactionDetail;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CompleteTransaction implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $transaction;
    protected $currency;
    protected $coin;
    protected $user;
    public $tries = 1;

    /**
     * CompleteTransaction constructor.
     * @param User $user
     * @param Transaction $transaction
     * @param Coin $coin
     * @param Currency $currency
     */
    public function __construct(Transaction $transaction,User $user,  Coin $coin, Coin $currency)
    {
        $this->user = $user;
        $this->transaction = $transaction;
        $this->coin=$coin;
        $this->currency = $currency;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $marketController = new MarketController();
        $marketController->completeTransaction($this->transaction, $this->user, $this->coin, $this->currency);
    }
}
