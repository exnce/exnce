<?php

namespace App\Jobs;

use App\User;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CampainCallBack implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    protected $user;
    public $tries = 1;


    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if($this->user->campain_code != ""){
            if($this->user->callback_url != ""){

                $params =  [
                    'form_params' => [
                        'name' => $this->user->name,
                        'email' => $this->user->email,
                        'campain_code' => $this->user->campain_code,
                        'ref_code'=>$this->user->ref_code
                    ]];


                try {
                    $client = new Client();
                    $client->request('POST', $this->user->callback_url, $params);

                    activity("job")
                        ->withProperties(["done"])
                        ->log("callback request yapildi.");
                } catch(\Throwable $e) {
                    activity("job")
                        ->withProperties(["error"])
                        ->log("callback request yapilamadi.");
                    if ($this->attempts() < $this->tries)
                        $this->release(60);
                    throw $e;
                } catch (GuzzleException $e) {
                    activity("job")
                        ->withProperties(["error"])
                        ->log("callback request yapilamadi.");
                    throw $e;
                }

            }
        }
    }
}
