<?php

namespace App\Jobs;

use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class NotifyWallet implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $uri;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($uri)
    {
        $this->uri = $uri;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        try {
            $client = new Client();
            $client = $client->request("GET", $this->uri);
            $response   = json_decode($client->getBody());
            //Message::create(array("message" =>  $response));

            activity("job")
                ->withProperties($response)
                ->log("subscribe.js'e yeni cuzdan bildirimi yapildi.");
        } catch(\Throwable $e) {
            if ($this->attempts() < $this->tries)
                $this->release(60);
            throw $e;
        }

    }


}
