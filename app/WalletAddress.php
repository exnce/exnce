<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WalletAddress extends Model
{
    use SoftDeletes;
    protected $fillable = ["coin_id","user_id","name","address","address_type","password", "status", "payment_id","destination_tag"];

    public function wallet(){
        return $this->belongsTo(Wallet::class,"wallet_id","id")->withTrashed();
    }

    public function coin(){
        return $this->belongsTo(Coin::class,"coin_id","id")->withTrashed();
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function deposits(){

        return $this->hasMany(CoinDeposit::class,"address","address")->withTrashed();
    }
}
