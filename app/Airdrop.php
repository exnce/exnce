<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use willvincent\Rateable\Rateable;
use Overtrue\LaravelFollow\Traits\CanBeLiked;
use Overtrue\LaravelFollow\Traits\CanBeFavorited;
use Overtrue\LaravelFollow\Traits\CanBeVoted;
use Overtrue\LaravelFollow\Traits\CanBeBookmarked;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;

class Airdrop extends Model
{
    use Rateable;
    use CanBeLiked, CanBeFavorited, CanBeVoted, CanBeBookmarked,CanBeFollowed;
    public function coin() {
        return $this->belongsTo(Coin::class,"coin_id");
    }

    public function owner(){
        return $this->belongsTo(User::class,"owner_id","id");
    }
}
