<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
class UserBank extends Model
{
    use SoftDeletes;
    protected $fillable = ["user_id","currency_type","currency_id","payment_address","saved","destination_tag","bank_id","name","surname","iban_number","amount"];

    public function bank(){
        return $this->belongsTo(Bank::class,"bank_id","id");
    }

}
