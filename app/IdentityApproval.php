<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class IdentityApproval extends Model
{
    use SoftDeletes;

    public $timestamps = true;

    protected $fillable = ["user_id","hash","id_card_front","id_card_back","selfie","status","description"];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

}


