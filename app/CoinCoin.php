<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class CoinCoin extends Model
{
    protected $table = 'coin_coin';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('display_index', 'desc');
        });
    }

    public function pair(){
        return $this->belongsTo(Coin::class,"coin_id","id")->select("id","symbol");
    }

    public function coin(){
        return $this->belongsTo(Coin::class,"pair_id","id");
    }

    public function price(){

        return $this->hasMany(TransactionDetail::class,"coin_id","coin_id");
    }


}
