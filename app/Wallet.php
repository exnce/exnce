<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Wallet extends Model
{
    use SoftDeletes;

    protected $fillable = ["user_id","type","coin_id","name","wallet_address","destination_tag","private_key","public_key","balance","status"];

    protected $hidden = [
        'wallet_address', 'destination_tag','private_key','public_key','balance','status'
    ];

    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }
    public function coin(){
        return $this->belongsTo(Coin::class)->withTrashed();
    }
    public function currency() {
        return $this->belongsTo(Currency::class,"coin_id")->withTrashed();
    }
    public function address(){
        return $this->hasMany(WalletAddress::class)->withTrashed();
    }

}
