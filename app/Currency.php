<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends Model
{
    use SoftDeletes;
    protected $fillable = ["name","symbol","slug","min_deposit","min_withdraw","status"];

    protected $hidden = [
        'id', 'created_at','updated_at','deleted_at','min_deposit','min_withdraw','status'
    ];
}
