<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCard extends Model
{
    use SoftDeletes;

    protected $fillable = ["user_id","type","card_countrycode","card_nfcid","card_icid","card_mcid","card_privateid","card_pin","card_no","card_expirationdate","card_cvv","card_servicecode","track1","track2","track3","uuid"];

    protected $hidden = [];
}
