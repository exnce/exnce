<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $fillable = ["transaction_id","dest_transaction_id","user_id","coin_id","currency_id","type","fee","amount","price","total"];

    protected $hidden = ["transaction_id","dest_transaction_id","user_id","coin_id","currency_id","type","fee","total","created_at","deleted_at"];

    public $timestamps = true;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function coin(){
        return $this->belongsTo(Coin::class,"coin_id","id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function currency() {
        //return $this->belongsTo(Currency::class,"currency_id","id");
        return $this->belongsTo(Coin::class,"currency_id","id")->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,"user_id","id");
    }

}
