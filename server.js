var server;
var app = require('express')();
const wallet = require('./node/wallet');
const stellar = require('./node/xlm');
var allowedOrigins = "*:*";
const Web3 = require("web3");
var mysql = require('mysql');
const util = require('util');
var redis = require('redis');
var fs = require("fs");
const fetch     = require('node-fetch');

const DEBUG = false;
var blockNumberTrack=10;
var socketServerPort=1973;
var socketAppServerPort=1989;
var stellarLastCursor=1; // or load where you left off
const BigNumber = require('bignumber.js');
var sayac = 0 ;
var StellarSdk = require('stellar-sdk');
var stellarServer;
var abi = require('human-standard-token-abi');
var coinList;
var erc20TokenList;
require('dotenv').config();
//var blockchain = require('blockchain.info');


var closedLedger = 0;
const RippleAPI = require('ripple-lib').RippleAPI;
const rippleApi       = new RippleAPI({ server: 'wss://s2.ripple.com' });
const rippleWallets   = [ 'rna8qC8Y9uLd2vzYtSEa1AJcdD3896zQ9S' ];
var parseBalanceChanges = require("ripple-lib-transactionparser").parseBalanceChanges;


var _lastClosedLedger = function (ledgerIndex) {
    var i = parseInt(ledgerIndex);
    if (ledgerIndex > closedLedger) {
        closedLedger = ledgerIndex;
        console.log('# LEDGER CLOSED: ', closedLedger);
    }
};


var dbConfig = {
    host: process.env.DB_HOST,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    port: process.env.DB_PORT
};

async function _initCoinList() {
    return await getCoins()
}

async function _initContractList(){
    return await getContracts();
}

_initCoinList().then(function (result) {
     coinList = result;
});

_initContractList().then(function (result) {
    erc20TokenList = result;
});




async function getWallets(walletType) {
    let connection;
    try {
        connection = await mysql.createConnection(dbConfig);
        const query = util.promisify(connection.query).bind(connection);
        let result = await query('select address from wallet_addresses where address_type="'+walletType+'";');
        result = JSON.parse(JSON.stringify(result));
        result = result.map((val) => {
            return val["address"].toLowerCase();
        });
        return result;
    }catch (e) {
        if (connection && connection.end) connection.end();
    }  finally {
        if (connection && connection.end) connection.end();
    }
    return true;
}

async function getContracts() {
    let connection;
    try {
        connection = await mysql.createConnection(dbConfig);
        const query = util.promisify(connection.query).bind(connection);
        let result = await query('select id,contract from coins where contract!="" and status!=3;');
        result = JSON.parse(JSON.stringify(result))
        return result;
    }catch (e) {
        if (connection && connection.end) connection.end();
    }  finally {
        if (connection && connection.end) connection.end();
    }
    return true;
}

async function getCoins() {
    let connection;
    try {
        connection = await mysql.createConnection(dbConfig);
        const query = util.promisify(connection.query).bind(connection);
        let result = await query('select id,symbol from coins;');
        result = JSON.parse(JSON.stringify(result))
        return result;
    }catch (e) {
        if (connection && connection.end) connection.end();
    }  finally {
        if (connection && connection.end) connection.end();
    }
    return true;
}

async function getLastBlockCursor(networkType) {
    let connection;
    try {
        connection = await mysql.createConnection(dbConfig);
        const query = util.promisify(connection.query).bind(connection);
        let result = await query('SELECT network_type,blockNumber FROM `block_tracks` WHERE id=(SELECT MAX(id) FROM `block_tracks`) and network_type="'+networkType+'" limit 1;');
        result = JSON.parse(JSON.stringify(result));
        return result;
    }catch (e) {
        if (connection && connection.end) connection.end();
    }  finally {
        if (connection && connection.end) connection.end();
    }
    return true;
}

async function setLastBlockCursor(networkType, blockNumber) {
    let connection;
    let data = [[
        networkType,
        blockNumber,
        new Date()
    ]];
    try {
        connection = await mysql.createConnection(dbConfig);
        const query = util.promisify(connection.query).bind(connection);
        let sql = 'insert into block_tracks (network_type, blockNumber,created_at) values ?';
        let result = await query(sql, [data]);
        return result;
    }catch (e) {
        console.log(e);
        return false;
    } finally {
        if (connection && connection.end) connection.end();
    }
    return false;
}

const storeData  = async function (data) {
    let databaseConnection;
    try {

        databaseConnection = await mysql.createConnection(dbConfig);
        const query = util.promisify(databaseConnection.query).bind(databaseConnection);
        let sql = 'insert into coin_deposits (blockNumber,txid,coin_id,address,memo_tag, amount,status,confirmations, created_at) values ?';
        let result = await query(sql, [data], function(err,rows){
            if(err)
            {
                if(err.code == 'ER_DUP_ENTRY' || err.errno == 1062)
                {
                    console.log('Here you can handle duplication')
                }
                else{
                    console.log('Other error in the query')
                }
            }else{
                console.log('No error in the query')
            }
        });
        return true;
    }catch (e) {
        console.log("SAVE_FAILED");
        return false;
    } finally {
       if (databaseConnection && databaseConnection.end) databaseConnection.end();
    }
    return true;
};

function getProvider() {
    return new Web3.providers.WebsocketProvider("wss://mainnet.infura.io/ws/v3/ed0b078730f6405e84d54d1b620c9a41");
}
const web3 = new Web3(getProvider());

const receiveTransactions = web3.eth.subscribe('newBlockHeaders', (error, blockHeader) => {
    if (error){
                console.log('Hata!', blockHeader);
                return console.error(error);
    }
}).on('data', async (blockHeader) => {
    let iCurrentBlock = blockHeader.number - 259720;
    let firstBlock = 7485692;
   readTxs(iCurrentBlock,true);
   readTxs(blockHeader.number-10000,false);
   readTxs(blockHeader.number,false);

});

function getaddr(addr) {
    addr = addr + "";
    let s = addr.substr(26);
    s = "0x" + s;
    return s;
}

async function readTxs(currentBlock,save) {
    console.log("ERC2O"+currentBlock.toString());
        let wallets = await getWallets("ERC20");
        if(!Array.isArray(wallets)){
            console.log("SERVER KAPALI");
            return;
        }

    if(!Array.isArray(erc20TokenList)){
        console.log("SERVER KAPALI");
        return;
    }

        erc20TokenList.forEach(contract => {
           let contractAddress = contract.contract;
           web3.eth.getPastLogs({fromBlock:currentBlock,toBlock: currentBlock,address:contractAddress})
                .then(transactions => {

                    transactions.forEach(transaction => {
                        let txId = transaction.transactionHash;
                        let token = contractAddress;
                        let value = transaction.data;
                        let to = "";
                        let confirmations = currentBlock - transaction.blockNumber;

                        if(transaction.topics.length<3){ return;}
                        switch (transaction.topics.length){
                            case 3:
                                to = getaddr(transaction.topics[2]).toString().toLowerCase();
                                break;
                            case 2:
                                to = getaddr(transaction.topics[1]).toString().toLowerCase();
                                break;
                            default:
                                to = transaction.to.toLowerCase();
                                break;
                        }
                        if (wallets.indexOf(to) > -1) {
                            let tokenContract =  new  web3.eth.Contract(abi, token,{ from: to , gas: 100000});
                            tokenContract.methods.decimals.call().then(decimals => {
                                let amount = value;
                                let calculatedAmount = web3.utils.toBN(web3.utils.fromWei(amount,"wei"));
                                var _divisor = new web3.utils.toBN(10).pow(web3.utils.toBN(decimals));
                                let decimalPlaces = web3.utils.toBN(decimals).toNumber();
                                var calculatedDeposit = new BigNumber(calculatedAmount).dividedBy(_divisor).decimalPlaces(decimalPlaces).toFixed(decimalPlaces);
                                let data = [[
                                    currentBlock,
                                    txId,
                                    contract.id,
                                    to,
                                    "-",
                                    calculatedDeposit,
                                    0,
                                    confirmations,
                                    new Date()
                                ]];
                                console.log("SAVING_TRANSACTION:",to);
                                storeData(data);
                                console.log("SAVED_TRANSACTION:",to);
                            });
                        }else{
                            DEBUG && console.log("TOKEN_TRANSACTION_NOT_FOUND",currentBlock);
                        }

                    });
                }).catch(err => console.log("getPastLogs failed", err));
          });

    web3.eth.getBlock(currentBlock, async function (err, block) {

        if(err){
            DEBUG && console.log("HATA_TESPIT_EDILDI");
            return;
        }
        if(block == null){
            DEBUG && console.log("NULL_BLOCK");
            return;
        }
        if (!block.transactions) {
            DEBUG && console.log("NULL_TRANSACTION_BLOCK");
            return;
        }

        if (block.hasOwnProperty('transactions')) {
            let transactions = block.transactions;
            let totalTransactions = transactions.length;
            transactions.forEach(function(txId) {
                DEBUG && console.log("ETHEREUM_TRANSFER");
                DEBUG && console.log("WALLETS", wallets.length);
                let confirmations = 0;
                web3.eth.getTransaction(txId, function (err, transaction) {
                    if(err){
                        return;
                    }
                    if(transaction == null){
                        return;
                    }

                    confirmations = currentBlock - transaction.blockNumber;
                    try {
                        let to = transaction.to;
                        if (to == null) {
                            DEBUG && console.log("CONTRAT_CREATION");
                            return;
                        }
                        to = to.toLowerCase();
                        if (wallets.indexOf(to) > -1) {
                            let value = transaction.value;
                            let calculatedAmount = web3.utils.fromWei(web3.utils.toBN(value), "ether");
                            let data = [[
                                currentBlock,
                                txId,
                                1,
                                to,
                                "-",
                                calculatedAmount,
                                0,
                                confirmations,
                                new Date()
                            ]];
                            console.log("SAVING_ETH_TRANSACTION:",to);
                            storeData(data);
                            console.log("SAVED_ETH_TRANSACTION:",to);

                        }
                    } catch (ex) {
                        console.log(ex);
                        console.log("ETHEREUM:ERROR", txId);
                    }
                })
            });
        }
    });
    if (save == true){
        setLastBlockCursor("ERC20",currentBlock);
    }
}

readTxs(8202507);

// *********** STELLAR ************
let stellarHandler = async function (record) {
    let coinFilter = coinList.filter(item => (item.symbol.toLowerCase() == "xlm"));
    record.transaction()
        .then(function(txn) {
            console.log("STELLAR GELDI");
            let customer = txn.memo;
            if(record.to.toLowerCase() != stellar.baseAccount.publicKey.toLowerCase()){
                return;
            }
            if (record.asset_type != 'native') {

            } else {
                if(record.transaction_successful == true){
                    let data = [[
                    record.paging_token,
                    record.transaction_hash,
                    coinFilter[0].id,
                    record.to,
                    customer,
                    parseFloat(record.amount),
                    0,
                    0,
                    new Date()
                ]];
                storeData(data);
                }else{
                    console.log("FAILED_TRANSACTION");
                }
            }
        })
        .catch(function(err) {
        });
    return true;
};
if (JSON.parse(process.env.APP_DEBUG)) {
    stellarServer = new StellarSdk.Server('https://horizon-testnet.stellar.org');
    StellarSdk.Network.useTestNetwork();
}else{
    stellarServer = new StellarSdk.Server('https://horizon.stellar.org');
    StellarSdk.Network.usePublicNetwork();
}
StellarSdk.Config.setAllowHttp(true);
let callBuilder = stellarServer.payments().forAccount(stellar.baseAccount.publicKey);
callBuilder.cursor("now");
callBuilder.stream({onmessage: stellarHandler});



// *********** RIPPLE ************

var _storeTransaction = async function (tx) {
    if (tx.ledger_index <= closedLedger) {

       if(!Array.isArray(coinList)){
           console.log("SERVER KAPALI");
           return;
       }

       let coinFilter = coinList.filter(item => (item.symbol.toLowerCase() == "xrp"));

        var destinationTag = tx.DestinationTag;
        var transferAmount = (parseFloat(tx.Amount)/1000/1000);

        if(typeof destinationTag === "undefined") {
            destinationTag = "10000015430";//exnce market account
        }

        let data = [[
            tx.ledger_index,
            tx.hash,
            coinFilter[0].id,
            tx.Destination,
            destinationTag,
            transferAmount,
            0,
            0,
            new Date()
        ]];

        storeData(data);

        // var transactionJson = JSON.stringify({
        //     hash: tx.hash,
        //     ledger: tx.ledger_index,
        //     from: tx.Account,
        //     to: tx.Destination,
        //     xrp: transferAmount,
        //     tag: destinationTag
        // })


    } else {
        console.log('Error: got transaction < lastClosedLedger (' + closedLedger + ')', tx)
    }
}

var _bootstrap = function () {
    rippleApi.connection._ws.on('message', function(m){
        var message = JSON.parse(m)
        if (message.type === 'ledgerClosed') {
            _lastClosedLedger(message.ledger_index)
        }
        if (message.type === 'response' && typeof message.id !== 'undefined') {
            if (typeof message.result.transactions !== 'undefined' && message.result.transactions.length > 0) {
                message.result.transactions.filter(function (f) {
                    return f.validated &&  f.tx.TransactionType === 'Payment' && typeof f.tx.Amount === 'string' && f.meta.TransactionResult === 'tesSUCCESS'
                }).forEach(function (t) {
                      _storeTransaction(t.tx);
                })
            }
        }
    });

    rippleWallets.forEach(function (_w, k) {
        console.log('Processing history: @' + k, _w)
        rippleApi.connection._ws.send(JSON.stringify({
            id: k + 1,
            command: "account_tx", account: _w,
            ledger_index_min: -1, ledger_index_max: -1,
            binary: false, count: false, limit: 1000,
            descending: true
        }))
    });

    rippleApi.connection.on('transaction', (t) => {
        if (t.transaction.TransactionType === 'Payment' && t.meta.TransactionResult === 'tesSUCCESS') {
            var tx = t.transaction;
            tx.ledger_index = t.ledger_index;
            console.log(tx);
            if(rippleWallets.indexOf(tx.Destination) > -1){
                _storeTransaction(tx);
            }


        }
    });

    return rippleApi.connection.request({
        command: 'subscribe',
        accounts: rippleWallets
    })

};

rippleApi.on('error', (errorCode, errorMessage) => {
    console.log(errorCode + ': ' + errorMessage)
});
rippleApi.on('connected', () => {
    console.log('<< connected >>');
});
rippleApi.on('disconnected', (code) => {
    // code - [close code](https://developer.mozilla.org/en-US/docs/Web/API/CloseEvent) sent by the server
    // will be 1000 if this was normal closure
    console.log('<< disconnected >> code:', code)
});

rippleApi.connect().then(() => {
    rippleApi.getServerInfo().then(function (server) {
        _lastClosedLedger(server.validatedLedger.ledgerVersion);
        _bootstrap();
    })
}).then(() => {
    // return api.disconnect()
}).catch(console.error);




if (JSON.parse(process.env.APP_DEBUG)) {
    console.log("running on http");
    server = require('http').Server(app);
} else {
    console.log("running on https");
    const privateKey = fs.readFileSync('/etc/letsencrypt/archive/socket.exnce.com/privkey4.pem', 'utf8');
    const certificate = fs.readFileSync('/etc/letsencrypt/archive/socket.exnce.com/cert4.pem', 'utf8');
    const ca = fs.readFileSync('/etc/letsencrypt/archive/socket.exnce.com/chain4.pem', 'utf8');

    var options = {
        key: privateKey,
        cert: certificate,
        ca: ca,
        requestCert: false,
        rejectUnauthorized: false
    };
    server = require('https').Server(options, app);
}

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Origin,X-Requested-With,content-type');
    next()
});

var io = require('socket.io')(server);
io.set('origins', allowedOrigins);
server.listen(socketServerPort);
io.on('connection', function (socket) {
    console.log("Client connected!");
    var redisClient = redis.createClient();
    redisClient.subscribe('message');
    redisClient.on("message", function (channel, message) {
        socket.emit(channel, message);
    });
    socket.on('disconnect', function () {
        redisClient.quit();
    });
});
app.listen(socketAppServerPort);
app.use('/wallet', wallet);
