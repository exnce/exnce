let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js').sass('resources/assets/sass/app.scss', 'public/css');
mix.js('public/js/personal/personal.js', 'public/js/personal/personal.min.js');
mix.js('public/js/script.js', 'public/js/script.min.js');



mix.styles([
    'public/components/bootstrap/css/bootstrap.min.css',
    'public/fontawesome/css/all.min.css',
    'public/fontawesome/css/solid.min.css',
    'public/fontawesome/css/brands.min.css',
    'public/css/master.css',
    'public/css/circle.css',
    'public/css/animate.min.css',
    'public/css/chart.css',
    'public/css/slick.css',
    'public/css/swiper.css',
    'public/components/aos/aos.css',
    'public/components/select2/css/select2.min.css',
    'public/components/animsition/css/animsition.css',
    'public/components/datatables/datatables.min.css',
    'public/components/toastr/build/toastr.min.css',
    'public/components/summernote/summernote.css',
    'public/components/summernote/summernote-bs4.css',
    'public/css/dark.css'

], 'public/css/style.css');
