<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Airdrop;
use App\Coin;
use App\CoinDeposit;
use App\Wallet;
use App\Transaction;
use App\TransactionDetail;
use Plivo\RestClient;
use Spatie\Activitylog\Models\Activity;

Auth::routes();
Route::namespace("Site")->group(function() {
    //Route::get("/","MarketController@index")->name("home");
    Route::get('/', 'AirdropController@index')->name("home");

//    Route::prefix("market")->group( function () {
//        Route::get("/{pair}-{coin}", "MarketController@pair")->name("pair");
//        Route::post("/load","MarketController@loadMarketData");
//    });

    Route::prefix("market")->group( function () {
        Route::get("/{pair}-{coin}", "HomeController@login")->name("pair");
    });

 //   Route::get('/', 'AirdropController@index')->name("home");


    //Route::get("/backup74be16979710d4c4e7c6647856088456","HomeController@backup")->name("backup");
    Route::get("/login","HomeController@login")->name("login");
    Route::post("/loginvia","HomeController@loginVia")->name("loginvia");
    Route::get("/register","HomeController@register")->name("register");
    Route::get("/register/{referenceId}","HomeController@register")->name("register-reference");

    Route::get('/lang/{locale}', 'HomeController@changeLang')->name("select-language");
});


Route::get('/contact', "ContactController@index")->name("contact");
Route::post("/contact","ContactController@sendMessage");

Route::prefix("verify")->group( function () {
    Route::get("/email", "Personal\VerificationController@verifyEmail")->name("verify-email");
    Route::get("/email/{token}", 'Personal\VerificationController@verifyEmailRequest')->name("verify-email-request");
});

Route::prefix("agreements")->group( function () {
    Route::get("/user-agreement", function (){return view("agreements.user-agreement");})->name("user-agreement");
    Route::get("/privacy-policy", function (){return view("agreements.privacy-policy");})->name("privacy-policy");
    Route::get("/aml", function (){return view("agreements.aml");})->name("aml");
    Route::get("/kyc", function (){return view("agreements.kyc");})->name("kyc");
    Route::get("/cookie-policy", function (){return view("agreements.cookie-policy");})->name("cookie-policy");
    Route::get("/personal-data-protection", function (){return view("agreements.personal-data-protection");})->name("personal-data-protection");
});

Route::get('/sms/validate', 'TwoFactorController@sendSMS')->name("send-sms");
Route::post('/sms/validate', ['middleware' => 'throttle:60,1', 'uses' => 'TwoFactorController@validateSMS'])->name("sms-validate");

Route::get('/authenticator/validate', 'Auth\LoginController@getValidateToken');
Route::post('/authenticator/validate', ['middleware' => 'throttle:60,1', 'uses' => 'Auth\LoginController@postValidateToken'])->name("authenticator-validate");

Route::post('/authenticator/enable', 'TwoFactorController@enableGoogle2FA');
Route::post('/authenticator/disable', 'TwoFactorController@disableGoogle2FA')->name("disable-g2fa");
Route::post('/sms2fa/enable', 'TwoFactorController@enableSMS2FA');

Route::get('/airdrops', 'Site\AirdropController@index')->name("siteairdrops");
Route::get('/airdrops/details/{airdropId}', 'Site\AirdropController@details')->name("siteairdropdetails");

Route::get('/fees', 'Site\FeeController@index')->name("fees");
Route::get('/status', 'Site\StatusController@index')->name("status");
Route::get('/about', 'Site\AboutController@index')->name("about");
Route::get('/partners', 'Site\PartnerController@index')->name("partners");



Route::get('/social', 'SocialController@index')->name("social-confirm");


Route::get('auth/facebook', 'Auth\FacebookController@redirectToFacebook');
Route::get('auth/facebook/callback', 'Auth\FacebookController@handleFacebookCallback');


Route::get('auth/twitter', 'Auth\TwitterController@redirectToTwitter');
Route::get('auth/twitter/callback', 'Auth\TwitterController@handleTwitterCallback');

Route::get("/testpoint", function (\Illuminate\Http\Request $request){


});


Route::prefix('api')->group(function () {
    Route::prefix("docs")->group(function () {
        Route::get('/', 'Api\v1\ApiController@index')->name("api-docs-index");
    });
});