<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 02:28
 */

use \App\Http\Middleware\Verify;

Route::middleware("auth")->group( function () {
   Route::namespace("Personal")->middleware(Verify::class)->group(function(){

       Route::get("/", "MarketController@index","dashboard")->name("dashboard");

       Route::post("/changePassword","SettingsController@changePassword")->name("change-password");
       Route::post("/verifyId","IdentityApprovalController@verifyId")->name("verify-id");
       Route::post("/cancelVerifyId","IdentityApprovalController@cancelVerifyId")->name("cancel-id-verify");


       Route::prefix("airdrops")->group( function () {
           Route::get('/', 'AirdropController@index')->name("airdrops");
           Route::get('/details/{airdropId}', 'AirdropController@details')->name("airdropdetails");
           Route::post("/update/ratings", "AirdropController@updateRatings");
           Route::post("/update/follow", "AirdropController@doFollow");


       });

       Route::prefix("verify")->group( function () {
           Route::get("/mobile", "VerificationController@verifyMobile")->name("verify-mobile");
           Route::post("/mobile", "VerificationController@verifyMobile")->name("verify-mobile-confirm");


           Route::get("/sms", "VerificationController@verifySMS")->name("verify-sms");
           Route::post("/sms", "VerificationController@verifySMS")->name("verify-sms-confirm");

          // Route::match(['get', 'post'], "/smsverification", "VerificationController@smsVerification","smsverification")->name("smsverification");


/*           Route::get("/pin", "VerificationController@verifyPIN")->name("verify-pin");
           Route::get("/identity", "VerificationController@verifyIdentity")->name("verify-identity");
           Route::get("/bill", "VerificationController@verifyBill")->name("verify-bill");*/
       });

       Route::prefix("wallet")->group( function () {
            Route::get("/", "WalletController@index")->name("wallet");
            Route::post("/create", "WalletController@createWallet");
           Route::post("/load-wallets", "WalletController@loadWallets");
           Route::post("/get-new-address", "WalletController@getNewAddress");
       });

       Route::prefix("market")->group( function () {
           Route::get("/{pair}-{coin}", "MarketController@pair")->name("pair");
           Route::get("/", "MarketController@index")->name("market");
           //Route::get("/{coin}", "MarketController@index")->name("market");
           Route::post("/load","MarketController@loadMarketData");
           Route::post("/buy", "MarketController@createBuy");
           Route::post("/sell", "MarketController@createSell");
           Route::post("/history","MarketController@loadMarketHistoryData");
           Route::post("/cancel","MarketController@cancelTransaction");
           Route::post("/open","MarketController@openTransactions");
           Route::post("/completed","MarketController@completedTransactions");

           Route::post("/fav","MarketController@updateFavoritePairs");
       });



       Route::prefix("ieo")->group( function () {
           Route::get("/", "IEOController@index")->name("ieo");
           Route::get("/{pair}-{coin}", "IEOController@pair")->name("ieopair");
           Route::post("/load","IEOController@loadMarketData");
           Route::post("/buy", "IEOController@createBuy");
           Route::post("/sell", "IEOController@createSell");
           Route::post("/fav","IEOController@updateFavoritePairs");

       });



       Route::prefix("support")->group( function () {
           Route::get("/", "SupportController@index")->name("support");
           Route::get("/completed", "SupportController@index")->name("support-completed");
           Route::get("/create", "SupportController@create")->name("support-create");
           Route::get("/detail/{ticketId}", "SupportController@ticketDetail")->name("support-detail");
           Route::post("/detail/{ticketId}", "SupportController@sendComment");

           Route::get("/detail/{ticketId}/reopen", "SupportController@reopenTicket")->name("support-reopen");
           Route::post("/get","SupportController@loadSupportRequests");
           Route::post("/create","SupportController@createSupportRequest");

       });

       Route::prefix("account")->group( function () {
           Route::get("/", "SettingsController@index")->name("account");
           Route::get("/logs", "SettingsController@activityLogs")->name("account-logs");
           Route::get("/apis", "ApiController@apiAccess")->name("account-apis");
           Route::post("/apis/get","ApiController@loadAPIEntries");
           Route::post("/apis/create","ApiController@generateAPIKey");
           Route::post("/apis/store","ApiController@storeAPIKey");


           Route::prefix("orders")->group(function (){

               Route::get("/orders","SettingsController@accountActivityLogs")->name("account-orders");
               Route::post("/get","SettingsController@loadUserOrderListHistory");

               Route::get("/{transactionStatus}","SettingsController@listTransactions")->name("list-transactions");

           });



           Route::get("/referrals", "ReferralsController@index")->name("account-referrals");
           Route::post("/referrals/get","ReferralsController@loadAccountRReferrals");

           Route::post("/logs/get","SettingsController@loadActivityLogs");

           Route::get("/verification","SettingsController@accountVerification")->name("account-verification");
           Route::get("/security","SettingsController@securityPreferences")->name("account-security");
           Route::get("/contact","SettingsController@contactPreferences")->name("account-contact");

           Route::post("/update-contact-preferences","SettingsController@updateContactPreferences")->name("update-contact-preferences");
           Route::post("/update-account-details","SettingsController@updateAccountDetails")->name("update-account-details");
       });

       Route::prefix("deposit")->group( function () {
           Route::get("/", "DepositController@index")->name("deposit");
/*           Route::get("/fiat/{currencySymbol}", "DepositController@depositFiatCurrency")->name("deposit-fiat");
           Route::post("/fiat/{currencySymbol}", "DepositController@createDepositRequest")->name("deposit-fiat-create");*/

           Route::get("/crypto/{currencySymbol}", "DepositController@depositCryptoCurrency")->name("deposit-crypto");
           Route::post("/create-deposit-request", "DepositController@createDepositRequest");
           Route::post("/delete-deposit-request", "DepositController@deleteDepositRequest");
           Route::post("/load-withdraw-requests","DepositController@loadDepositRequests");

       });

       Route::prefix("withdraw")->group( function () {
           Route::get("/", "WithdrawController@index")->name("withdraw");


/*           Route::get("/fiat/{currencySymbol}", "WithdrawController@withdrawFiatCurrency")->name("withdraw-fiat");
           Route::post("/fiat/{currencySymbol}", "WithdrawController@createWithdrawRequest")->name("withdraw-fiat-create");
           Route::get("/fiat/{currencySymbol}/confirm", "WithdrawController@confirmFiatWithdrawRequest")->name("withdraw-confirm-fiat");
           Route::post("/fiat/{currencySymbol}/confirm/{action}", "WithdrawController@acceptOrCancelWithdrawRequest");

           Route::get("/fiat/{currencySymbol}/verify", "WithdrawController@confirmWithdrawRequest")->name("withdraw-verify");*/


           Route::get("/crypto/{currencySymbol}", "WithdrawController@withdrawCryptoCurrency")->name("withdraw-crypto");
           Route::post("/crypto", "WithdrawController@createWithdrawRequest")->name("withdraw-crypto-create");
           Route::get("/crypto/{currencySymbol}/confirm", "WithdrawController@confirmCryptoWithdrawRequest")->name("withdraw-confirm-crypto");
           Route::post("/crypto/{currencySymbol}/confirm/{action}", "WithdrawController@acceptOrCancelCryptoWithdrawRequest");


           Route::post("/create", "WithdrawController@create");
           Route::get("/verification", "WithdrawController@verification");
           Route::post("/verification", "WithdrawController@verify_post");
           Route::post("/cancel", "WithdrawController@cancel");

           Route::post("/list-withdraw-requests","WithdrawController@listWithdrawRequests");
           Route::post("/delete-withdraw-request","WithdrawController@deleteWithdrawRequest");
       });

       // TODO: Route icine alinacak
       Route::post("/delete-bank-account", "WithdrawController@delete_bank_account");

       Route::prefix("trade")->group( function () {
           Route::get("/", "TradeController@index")->name("trade");
       });

   });
});