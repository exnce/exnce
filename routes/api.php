<?php

use App\Coin;
use App\TransactionDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login', 'Api\AuthController@login');
  //  Route::post('signup', 'Api\AuthController@signup');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
        Route::post('buy', 'Api\AuthController@marketBuy');
    });
});

Route::prefix("kernel")->group( function () {
    Route::post('/pos', 'Api\PosController@endPoint');

    Route::group(['middleware' => 'auth:api'], function() {
        Route::post('buy', 'Api\PosController@marketBuy');
    });
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
//    Route::prefix("docs")->group(function (){
//        Route::get('/', 'v1\ApiController@index')->name("api-docs-index");
//    });

    Route::prefix("v1")->group( function () {
       Route::get("/getWallets/{memberId}", "v1\ApiController@walletProfileAPI")->name("walletProfileAPI");
       Route::post("/openOrders", "v1\ApiController@openOrders");
       Route::post("/createBuy", "v1\ApiController@createBuy");
       Route::post("/createSell", "v1\ApiController@createSell");
       Route::post("/cancelOrder", "v1\ApiController@cancelOrder");

       Route::get("/ticker", function (Request $request){
            $apiController = new \App\Http\Controllers\Api\ApiController();
            $tickerData = $apiController->loadTickerData($request);
            return response()->json($tickerData);
        })->name("tickerv1");


        Route::get("/currencies", function (Request $request){
            return response()->json(getCoinsStatusInformation(true));
        })->name("currenciesv1");

        Route::get("/depth", "v1\ApiController@publicDepthOrderBook")->name("depthv1");
        Route::get("/exchangeInfo", "v1\ApiController@getExchangeInfo")->name("exchangeInfov1");
        Route::get("/ping", "v1\ApiController@sendPing")->name("pingv1");
        Route::get("/time", "v1\ApiController@sendTime")->name("timev1");
        Route::get("/historicalTrades", "v1\ApiController@historicalTrades")->name("historicalTradesv1");
        Route::get("/orderbook", "v1\ApiController@publicOrderBook")->name("orderbookv1");





    });


    Route::prefix("bot")->group( function () {


        Route::get("/webhook", "v1\TelegramController@index")->name("botv1");

        Route::post('/TELEGRAMID:SECRETKEY/webhook', function () {
            $update = Telegram::commandsHandler(true);
            return 'ok';
        });

        Route::post('/webhook', function () {
            $update = Telegram::commandsHandler(true);
            return 'ok';
        });

    });




    Route::get('/config', function () {

        $config = [
            "supports_group_request" => false,
            "supported_resolutions" => ["1", "5", "15", "30", "60", "180", "360", "720", "D", "W", "M"],
            "supports_time" => true,
            "supports_marks" => false,
            "supports_search" => true,
            "supports_timescale_marks"=>false,
            "exchanges"	=>null,
            "symbols_types"	=>null
        ];

        return response()->json($config);

    });

    //http://almeedex.local:8000/api/time
    Route::get('/time', function () {

        return response()->json(microtime(true));

    });

    Route::get("/ticker", function (Request $request){

        $apiController = new \App\Http\Controllers\Api\ApiController();
        $tickerData = $apiController->loadTickerData($request);

        return response()->json($tickerData);
    })->name("ticker");




    Route::get('/symbols', function (Request $request) {

        $json = array (
            'minmov' => '1',
            'has_empty_bars' => false,
            'exchange' => env("APP_NAME"),
            'has_weekly_and_monthly' => true,
            'exchange_listed' => env("APP_NAME"),
            'exchange_traded' => env("APP_NAME"),
            'session' => '24x7',
            'description' => $request->symbol,
            'fractional' => false,
            'has_daily' => true,
            "supported_resolutions" => ["1", "5", "15", "30", "60", "180", "360", "720", "D", "W", "M"],
            'data_status' => 'streaming',
            'minmov1' => 1,
            'minmov2' => 0,
            'has_dwm'=>true,
            'has_seconds' => false,
            'timezone' => 'Europe/Istanbul',
            "type" => "bitcoin",
            "has_intraday"=> true,
            "pricescale" => 100000000,
            'ticker' => $request->symbol,
            'volume_precision' => 8,
            'name' => $request->symbol,
            'has_no_volume' => false,
            "intraday_multipliers"=>["60","240","480","720"]



        );

        return response()->json($json);

    });

    //http://almeedex.local:8000/api/history?symbol=KZE-ETH&resolution=60&from=1544296904&to=1549480964
    Route::get('/history', function (Request $request) {

        $symbol = explode("-", $request->get("symbol"));
        $coin = Coin::where("symbol", $symbol[1])->first();
        $currency = Coin::where("symbol", $symbol[0])->first();

        $resolution = $request->get("resolution");
        $from = $request->get("from");
        $to = $request->get("to");

        $uniqueKey = sprintf("OHLCKEY%s%s%s%s",
            $symbol[0], $symbol[1],
            Carbon::createFromTimestamp($from)->second(0)->timestamp,
            Carbon::createFromTimestamp($to)->second(0)->timestamp);
        $redis=Redis::connection();
        $cache = $redis->get($uniqueKey);

        if($cache) {
            $data = json_decode($cache);
            return response()->json($data);

        } else {

            $t = [];
            $o = [];
            $h = [];
            $l = [];
            $c = [];
            $v = [];
            $s = "no_data";

            $minMaxDates = TransactionDetail::where("type", "buy")
                ->whereBetween("created_at", [
                    Carbon::createFromTimestamp($from),
                    Carbon::createFromTimestamp($to)
                ])->where("coin_id", $coin->id)->where("currency_id", $currency->id)
                ->selectRaw("min(created_at) as minDate, max(created_at) as maxDate")->first();

            if($minMaxDates->minDate) {
                $minDate = Carbon::createFromTimeString($minMaxDates->minDate)->timestamp;
                if($from < $minDate) {
                    $startDate = $minDate;
                } else {
                    $startDate = (int)$from;
                }

                $maxDate = Carbon::createFromTimeString($minMaxDates->maxDate)->timestamp;
                if($to > $maxDate) {
                    $to = $maxDate;
                }

                while ($startDate < $to):
                    switch ($resolution) {
                        case "1":
                            $endDate = Carbon::createFromTimestamp($startDate)->addMinutes(1)->timestamp;
                            break;
                        case "5":
                            $endDate = Carbon::createFromTimestamp($startDate)->addMinutes(5)->timestamp;
                            break;
                        case "15":
                            $endDate = Carbon::createFromTimestamp($startDate)->addMinutes(15)->timestamp;
                            break;
                        case "30":
                            $endDate = Carbon::createFromTimestamp($startDate)->addMinutes(30)->timestamp;
                            break;
                        case "60":
                            $endDate = Carbon::createFromTimestamp($startDate)->addHour(1)->timestamp;
                            break;
                        case "180":
                            $endDate = Carbon::createFromTimestamp($startDate)->addHour(3)->timestamp;
                            break;
                        case "360":
                            $endDate = Carbon::createFromTimestamp($startDate)->addHour(6)->timestamp;
                            break;
                        case "720":
                            $endDate = Carbon::createFromTimestamp($startDate)->addHour(12)->timestamp;
                            break;
                        case "D":
                            $endDate = Carbon::createFromTimestamp($startDate)->addDay(1)->timestamp;
                            break;
                        case "W":
                            $endDate = Carbon::createFromTimestamp($startDate)->addWeek(1)->timestamp;
                            break;
                        case "M":
                            $endDate = Carbon::createFromTimestamp($startDate)->addMonth(1)->timestamp;
                            break;
                        default:
                            $endDate = Carbon::createFromTimestamp($startDate)->addHour(1)->timestamp;
                            break;
                    }
                    if ($endDate > $to) $endDate = $to;

                    $transactions = TransactionDetail::where("type", "buy")
                        ->whereBetween("created_at", [
                            Carbon::createFromTimestamp($startDate),
                            Carbon::createFromTimestamp($endDate)
                        ])->where("coin_id", $coin->id)->where("currency_id", $currency->id)->get();

                    if (!$transactions->isEmpty()) {
                        $t[] = $transactions->first()->created_at->timestamp;
                        $o[] = $transactions->last()->price;
                        $h[] = $transactions->max("price");
                        $l[] = $transactions->min("price");
                        $c[] = $transactions->first()->price;
                        $v[] = $transactions->sum("amount");
                    }

                    $startDate = $endDate + 1;
                endwhile;
            }

            if (!empty($t)) $s = "ok";
            $data = ["t" => $t, "o" => $o, "h" => $h, "l" => $l, "c" => $c, "v" => $v, "s" => $s];

            $redis->set($uniqueKey, json_encode($data));
            return response()->json($data);
        }
    });



    Route::post('/1.0/charts', function () {

        $json = array (
            'data' =>
                array (
                    'timestamp' => microtime(true),
                    'id' => 623135089874267,
                    'name' => '623135089874267-ALDXKZE',
                ),
            'status' => 'ok',
        );

        return response()->json($json);

    });


    Route::get('/testpoint', "TestController@index")->name("testpoint");








});



