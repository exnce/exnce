<?php

use App\Broadcasting\OrderChannel;
use App\Broadcasting\TransactionChannel;

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
