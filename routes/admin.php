<?php
/**
 * Created by PhpStorm.
 * User: digibyte
 * Date: 4.07.2018
 * Time: 02:28
 */
Route::middleware("auth")->group( function () {
    Route::namespace("Admin")->group( function(){
        Route::get("/", "DashboardController@index")->name("/");
        Route::get("/dashboard", "DashboardController@index")->name("dashboard-sub");

        Route::get("/user-wallets/{coin_symbol}", function ($coin_symbol = null){
            $coins = [
                "ETH"=>1,
                "KZE"=>2,
                "NPXS"=>3,
                "ALDX"=>4,
            ];

            $wallets =\App\Wallet::where("coin_id", $coins[$coin_symbol])->orderBy("user_id","asc")->get();


            return view("admin.users.wallets", ["wallets" => $wallets]);

        });

        Route::prefix("approval")->group( function () {
            Route::get("/verification-requests", "VerificationRequestsController@index")->name("verification");
            Route::post("/requests/get", "VerificationRequestsController@getIdentityApprovals");
            Route::post("/requests/get/{requestId}", "VerificationRequestsController@getIdentityApprovalDetail");
            Route::post("/requests/update", "VerificationRequestsController@updateIdentityApprovalDetail");
            Route::get("/showDocument/{approvalId}/{docType}", "VerificationRequestsController@showDocument");
        });

        Route::prefix("transfers")->group( function () {
            Route::get("/requests", "TransferRequestsController@index")->name("transfers-requests");
        });

        Route::prefix("server")->group( function () {
            Route::get("/blocks", "BlockScannerController@index")->name("block-scan");
            Route::get("/blocks/add/{blockId}", "BlockScannerController@addBlock")->name("block-add");

        });

        Route::prefix("reports")->group( function () {
            Route::get("/", "ReportController@index")->name("reports");
            Route::get("/balance-report","ReportController@balanceReports")->name("balance-reports");

        });


        Route::prefix("contacts")->group( function () {
            Route::get("/", "ContactsController@index")->name("contacts");
            Route::post("/get", "ContactsController@getContacts");
        });

        Route::prefix("users")->group( function () {
            Route::get("/", "UserController@index")->name("users");
            Route::post("/confirm", "UserController@confirmUsers");

            Route::post("/get", "UserController@getUsers");
            Route::get("/detail/{userId}", "UserController@getDetails")->name("user-detail");
            Route::get("/detail/{userId}/login", "UserController@loginAsUser")->name("user-login");
            Route::post("/detail/{userId}/updateUser", "UserController@updateUserInfo");
            Route::post("/detail/{userId}/updateWallet", "UserController@updateWalletInfo");
        });

        Route::prefix("airdrops")->group( function () {
            Route::get("/", "AirdropController@index")->name("airdrop-distribution");
            Route::get("/detail/{airdropId}", "AirdropController@details")->name("admin-airdrop-detail");
            Route::post("/detail/{airdropId}", "AirdropController@store");

        });


        Route::prefix("support")->group( function () {
                    Route::get("/", "SupportRequestsController@index")->name("admin-support-requests");
                    Route::get("/completed", "SupportRequestsController@index")->name("admin-support-completed");
                    Route::get("/create", "SupportRequestsController@create")->name("support-create");
                    Route::get("/detail/{ticketId}", "SupportRequestsController@ticketDetail")->name("admin-support-detail");
                    Route::post("/detail/{ticketId}", "SupportRequestsController@sendComment");

                    Route::get("/detail/{ticketId}/reopen", "SupportRequestsController@reopenTicket")->name("admin-support-reopen");
                    Route::post("/get","SupportRequestsController@loadSupportRequests");
                    Route::post("/create","SupportRequestsController@createSupportRequest");
                });


        Route::prefix("orders")->group( function () {
            Route::prefix("deposit")->group( function () {
                Route::get("/requests", "DepositRequestsController@index")->name("deposit-requests");
                Route::post("/requests/get", "DepositRequestsController@getDepositOrders");
                Route::post("/requests/get/{requestId}", "DepositRequestsController@getDetails");
                Route::post("/requests/cancel", "DepositRequestsController@cancelDeposit");
                Route::post("/requests/approve", "DepositRequestsController@approveDeposit");
            });

            Route::prefix("withdraw")->group( function () {
                Route::get("/requests", "WithdrawRequestsController@index")->name("withdraw-requests");
                Route::post("/requests/get", "WithdrawRequestsController@getWithdrawOrders");
                Route::post("/requests/get/{requestId}", "WithdrawRequestsController@getDetails");
                Route::post("/requests/cancel", "WithdrawRequestsController@cancelWithdraw");
                Route::post("/requests/approve", "WithdrawRequestsController@approveWithdraw");
            });
        });

        Route::prefix("definations")->group( function () {
            Route::get("/", "DefinationController@index")->name("definations");
            Route::prefix("cryptocurrency")->group( function () {
                Route::get("/", "CryptoCurrencyController@index")->name("cryptocurrency-definations");
                Route::post("/get", "CryptoCurrencyController@getCoins");
                Route::post("/get/{coinId}", "CryptoCurrencyController@getCoinDetails");
                Route::post("/update", "CryptoCurrencyController@updateCoin");
                Route::post("/add", "CryptoCurrencyController@addCoin");
            });

            Route::prefix("fiatcurrency")->group( function () {
                Route::get("/", "FiatCurrencyController@index")->name("fiatcurrency-definations");
                Route::post("/get", "FiatCurrencyController@getCurrencies");
                Route::post("/get/{currencyId}", "FiatCurrencyController@getCurrencyDetails");
                Route::post("/update", "FiatCurrencyController@updateCurrency");
                Route::post("/add", "FiatCurrencyController@addCurrency");
            });

            Route::prefix("bank")->group( function () {
                Route::get("/", "BankController@index")->name("bank-definations");
                Route::post("/get", "BankController@getBanks");
                Route::post("/get/{bankId}", "BankController@getBankDetails");
                Route::post("/update", "BankController@updateBank");
                Route::post("/add", "BankController@addBank");
            });

            Route::prefix("bank-account")->group( function () {
                Route::get("/", "BankAccountController@index")->name("bank-account-definations");
                Route::post("/get", "BankAccountController@getBankAccounts");
                Route::post("/get/{accountId}", "BankAccountController@getBankAccountDetails");
                Route::post("/update", "BankAccountController@updateBankAccount");
                Route::post("/add", "BankAccountController@addBankAccount");
            });
        });

    });
});