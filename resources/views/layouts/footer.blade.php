@php
    $totalUserCount = \App\User::get()->count();
@endphp
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="bar1"></div>
        <div class="bar2"></div>
        <div class="bar3"></div>
        <div class="bar4"></div>
        <div class="bar5"></div>
    </div>
</div>

<div class="merchant-footer ltr  fixed-footer">
    <footer role="navigation" class="mer-footer">
        <div class="row">
            <div class="col-12">
                <div class="mer-nav-wrapper">
                    <ul class="mer-nav-main">
                        <li><a href="{{route("contact")}}">{{ _i("Contact") }}</a></li>
                        <li><a href="{{route("fees")}}">{{ _i("Fees") }}</a></li>
                        <li><a href="{{route("status")}}">{{ _i("Status") }}</a></li>
                        <li class="hidden"><a href="#">{{ _i("Security") }}</a></li>
                        <li><a href="{{route("about")}}">{{ _i("About") }}</a></li>
                        <li><a href="{{route("api-docs-index")}}">{{ _i("API") }}</a></li>
                        <li><a href="{{route("partners")}}">{{ _i("Partners") }}</a></li>
                    </ul>
                    <ul class="mer-nav-secondary">
                        <li> {{env("APP_COPYRIGHT_TEXT")}}</li>
                    </ul>
                </div>

                <div class="mer-legal-nav-wrapper">
                    <ul class="mer-lang-toggle" style="display: block;">
                        <li><a class="lang-toggle {{ app()->getLocale() == "en" ? "selected" : "" }}" data-language="en_US" href="{{ route("select-language", ["en_US"]) }}">{{ _i("English") }}</a></li>
                        <li class="d-none"><a class="lang-toggle {{ app()->getLocale() == "fr" ? "selected" : "" }}" data-language="fr_FR" href="{{ route("select-language", ["fr_FR"]) }}">{{ _i("Français") }}</a></li>
                        <li class="d-none"><a class="lang-toggle {{ app()->getLocale() == "es" ? "selected" : "" }}" data-language="es_ES" href="{{ route("select-language", ["es_ES"]) }}">{{ _i("Español") }}</a></li>
                        <li class="d-none"><a class="lang-toggle {{ app()->getLocale() == "zh" ? "selected" : "" }}" data-language="zh_CN" href="{{ route("select-language", ["zh_CN"]) }}">{{ _i("中文") }}</a></li>

                        <li class="list-inline-item social-icon w-25 text-left"><a target="_blank"><i class="fas fa-2x fa-users"></i></a> | <span class="member-count">{{$totalUserCount}}</span></li>
                        <li class="list-inline-item social-icon"><a href="https://www.coingecko.com/en/exchanges/exnce?utm_content=exnce&utm_medium=search_exchange&utm_source=exnce" target="_blank"><img style="vertical-align:top !important;" width="24px" height="24px" src="{{asset("/images/coingecko.svg")}}"/></a></li>

                        <li class="list-inline-item social-icon"><a href="https://twitter.com/exncecom" target="_blank"><i class="fab fa-2x fa-twitter" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://www.reddit.com/user/exnce" target="_blank"><i class="fab fa-2x fa-reddit" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://t.me/exncecom" target="_blank"><i class="fab fa-2x fa-telegram" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://www.facebook.com/exncecom/" target="_blank"><i class="fab fa-2x fa-facebook" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://www.youtube.com/channel/UCMYmuS7xYqQ8BggH1BMKIuw" target="_blank"><i class="fab fa-2x fa-youtube" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://github.com/exnce/exnce" target="_blank"><i class="fab fa-2x fa-github" area-hidden="true"></i></a></li>
                        <li class="list-inline-item social-icon"><a href="https://disqus.com/by/exnce/" target="_blank"><i class="fab fa-2x fa-discourse" area-hidden="true"></i></a></li>



                    </ul>
                    <ul class="mer-nav-tertiary">
                        <li><a href="{{ route("user-agreement") }}" target="_blank">{{ _i("Terms and Conditions") }}</a></li>
                        <li><a href="{{ route("privacy-policy") }}" target="_blank">{{ _i("Privacy Policy") }}</a></li>
                        <li><a href="{{ route("cookie-policy") }}" target="_blank">{{ _i("Cookie Policy") }}</a></li>
                        <li class="hidden"><a href="{{ route("personal-data-protection") }}">{{ _i("Protection of Personal Data") }}</a></li>
                        <li class="hidden" ><a href="{{ route("aml") }}" target="_blank">{{ _i("AML Policy") }}</a></li>
                        <li class="hidden"><a href="{{ route("kyc") }}" target="_blank">{{ _i("KYC Policy") }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</div>


<script>

    var translate =
        {
            "OK"                        : "{{ _i("Ok") }}",
            "ALL"                       : "{{ _i("All") }}",
            "COPY_TO_CLIPBOARD"         : "{{ _i("Copy to clipboard") }}",
            "COPY"                      : "{{ _i("Copy") }}",
            "WARNING"                   : "{{ _i("Warning") }}",
            "CREATE_WALLET_VERIFY"      : "{{ _i("A New Wallet Will Be Created. Do you confirm?") }}",
            "YES"                       : "{{ _i("Yes") }}",
            "NO"                        : "{{ _i("No") }}",
            "ARE_YOU_SURE"              : "{{ _i("Are you sure?") }}",
            "DELETE_REQUEST_QUESTION"   : "{{ _i("If you continue, your request will be deleted!") }}",
            "YES_DELETE"                : "{{ _i("Yes, delete it!") }}",
            "CANCEL"                    : "{{ _i("Cancel") }}",
            "COPIED"                    : "{{ _i("Copied!") }}",
            "COULDNT_COPIED"            : "{{ _i("Unable to copy!") }}",
            "UNABLE_TO_COPY"            : "{{ _i("Unable to copy!") }}",
            "CLIPBOARD_INSTRUCTIONS"    : "{{ _i("You can use Ctrl + C or Cmd + C to copy to the clipboard.") }}",
            "VERIFY"                    : "{{ _i("Confirm") }}",
            "DELETE_RECORD_VERIFY"      : "{{ _i("Do you really want to delete this record?") }}",
            "DEP_REQ_WILL_BE_DELETED"   : "{{ _i("If you continue, your withdraw request will be canceled!") }}",
            "CONTINUE"                  : "{{ _i("Continue") }}",
            "UPLOAD_ERROR"              : "{{ _i("The file size to upload is 5 mb. can't be greater than that!") }}",
            "UPDATE_API_KEY_ENTRY"      : "{{ _i("Update API Access Key") }}",
            "DELETE_API_KEY_ENTRY"      : "{{ _i("Delete API Access Key") }}",
            "CREATE_API_KEY_ENTRY"      : "{{ _i("Create New API Access Key") }}",
            "DISPLAY_API_KEY_ENTRY"     : "{{ _i("API Access Key Details") }}",
            "DEPTH_CHART_PRICE"         : "{{ _i("Price") }}",
            "DEPTH_CHART_ACTUAL_PRICE"  : "{{ _i("Current Price") }}",
            "DEPTH_CHART_BUY"           : "{{ _i("Bids") }}",
            "DEPTH_CHART_SELL"          : "{{ _i("Asks") }}",
            "CANCEL_WITHDRAW_REQUEST"   : "{{_("Cancel")}}",
            "AVAIBLE_BALANCE"           : "{{_("Available Balance")}}",
            "USED_BALANCE"              : "{{_("In Order")}}",
            "TOTAL_BALANCE"             : "{{_("Total Balance")}}",


        };

    var localeCode = "{{ app()->getLocale() }}";
</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.1.1/socket.io.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="{{asset("js/jquery.min.js") }}?version={{env("JS_VERSION")}}"></script>


<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TimelineMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/TweenMax.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/gsap/1.20.3/plugins/ScrollToPlugin.min.js'></script>


<script src="{{asset("components/bignumber/bignumber.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/summernote/summernote.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/datatables/datatables.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/toastr/build/toastr.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/animsition/js/animsition.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/bootstrap/js/bootstrap.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/aos/aos.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/select2/js/select2.min.js") }}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("js/script.js")}}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("js/sweetalert.min.js")}}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("js/clipboard.min.js")}}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("components/currency/src/currency.js") }}?version={{env("JS_VERSION")}}"></script>

<script src="{{asset("js/metamask.js")}}?version={{env("JS_VERSION")}}"></script>


<script>
    var user_hash="";
    var selectedCoin=-1;
    var selectedCurrency=-1;
    var EXCHANGE_SOCKET_ADDRESS="";
    var SELECTED_PAIR_INFO = "";
    var LOGIN_VIA_URL = "{{route("loginvia")}}";
    var CURRENT_USER_HASH = "{{generateTransactionCode()}}";
    var EXCHANGE_NAME = "{{env("APP_NAME")}}";
    var CURRENT_SEGMENT = "{{request()->segment(2)}}";
    var IS_IEO = false;
    if(CURRENT_SEGMENT == "ieo"){
        CURRENT_SEGMENT = "market";
        IS_IEO = true;
    }


    @if(auth()->user())
        CURRENT_USER_HASH = "{{ auth()->user() ? auth()->user()->public_key : generateTransactionCode() }}";
    @endif
        EXCHANGE_SOCKET_ADDRESS = "{{ env("EXCHANGE_SOCKET_LOCAL_MODE") ? env("EXCHANGE_SOCKET_SERVER_LOCAL") : env("EXCHANGE_SOCKET_SERVER_REMOTE") }}";
    @if(@$activeCoin)
        selectedCoin = "{{$activeCoin->id}}";
    @endif
    @if(@$activeCurrency)
        selectedCurrency = "{{$activeCurrency->id}}";
    @endif

    @if(@$selectedPair!=null)
        SELECTED_PAIR_INFO = "{{$selectedPair}}";
    @endif
    searchVisible = 0;
    transparent = true;
</script>

@if(request()->segment(1) =="personal")
    @if(env("APP_DEBUG"))
        <script src="{{asset("js/personal/personal.js")}}?version={{env("JS_VERSION")}}"></script>
    @else
        <script src="{{asset("js/personal/personal.min.js")}}?version={{env("JS_VERSION")}}"></script>
    @endif
@endif

@if(!auth()->user())
    <script src="{{asset("js/site/site.js")}}?version={{env("JS_VERSION")}}"></script>
@endif

<script src="{{asset("js/slick.min.js")}}?version={{env("JS_VERSION")}}"></script>
@if(request()->segment(1) == "admin")
    <script src="{{asset("js/admin/admin.js")}}?version={{env("JS_VERSION")}}"></script>

@else

@endif

@if(request()->segment(1) == "login" || request()->segment(1) == "register" || request()->segment(2) == "reset" )
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endif


<script src="https://code.highcharts.com/highcharts.js"></script>
@if(request()->segment(2)=="wallet")
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/variable-pie.js"></script>
@endif

@if(request()->segment(1) != "admin")
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-141502454-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-141502454-1');
    </script>
@endif



