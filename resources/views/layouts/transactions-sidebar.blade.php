
<nav id="sidebar-nav">
    <div class="list-group">
        <a href="{{ route("list-transactions", "open") }}" class="list-group-item {{request()->segment(3) == "open" ? "active" : ""}}"><i class="fas fa-list-ul"></i> <span>{{_i("Open Orders")}}</span></a>
        <a href="{{ route("list-transactions", "completed") }}" class="list-group-item {{request()->segment(3) == "completed" ? "active" : ""}}"><i class="fas fa-list-ol"></i> <span>{{_i("Order History")}}</span></a>
    </div>
</nav>
