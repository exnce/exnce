
@extends('layouts.site')
@section("title",_i("Kullanıcı Girişi"))
@section('content')
    <section class="mt-3 container">
        <div class="row py-3 justify-content-center">
            <div class="col-lg-8">
                <div class="card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                    <div class="card-header">{{ _i('Kullanıcı Girişi') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login') }}" aria-label="{{ _i('Kullanıcı Girişi') }}">
                            @csrf

                            @if(session("login:error"))
                                <div class="row mb-0">
                                    <div class="alert alert-danger">{{session("login:error")}}</div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label for="email"
                                       class="col-sm-4 col-form-label text-md-right">{{ _i("E-Mail Adresi") }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email"
                                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} bordered-input"
                                           name="email" value="{{ old('email') }}" required autofocus>

                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password"
                                       class="col-md-4 col-form-label text-md-right">{{ _i('Şifre') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} bordered-input"
                                           name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="checkbox d-inline">
                                        <label>
                                            <input type="checkbox"
                                                   name="remember" {{ old('remember') ? 'checked' : '' }}> {{ _i('Beni Hatırla') }}
                                        </label>
                                    </div>
                                    <a class="btn btn-link p-0 float-right" href="{{ route('password.request') }}">
                                        {{ _i('Şifremi Unuttum') }}
                                    </a>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-6 offset-md-4">
                                    <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"></div>
                                    @if ($errors->has('g-recaptcha-response'))
                                        <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        {{ _i('Giriş Yap') }}
                                    </button>

                                    <a class="btn btn-success btn-block" href="{{ route('register') }}">
                                        {{ _i('Kayıt Ol') }}
                                    </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


