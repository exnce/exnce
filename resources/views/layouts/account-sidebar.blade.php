
<nav id="sidebar-nav">
    <div class="list-group">
        <a href="{{route("account")}}" class="list-group-item {{ !request()->segment(3) ? "active" : "" }}"><i class="fa fa-user"></i> <span>{{_i("Account Informations")}}</span></a>
        <a href="{{route("account-orders")}}" class="list-group-item {{ request()->segment(3) == "orders" ? "active" : "" }}"><i class="fa fa-university"></i> <span>{{_i("Account Logs")}}</span></a>
        <a href="{{route("account-logs")}}" class="list-group-item {{ request()->segment(3) == "logs" ? "active" : "" }}"><i class="fa fa-cogs"></i> <span>{{_i("User Logs")}}</span></a>
        <a href="{{route("account-verification")}}" class="list-group-item {{ request()->segment(3) == "verification" ? "active" : "" }}"><i class="fa fa-id-card"></i> <span>{{_i("Identity Approval")}}</span></a>
        <a href="{{route("account-security")}}" class="list-group-item {{ request()->segment(3) == "security" ? "active" : "" }}"><i class="fas fa-shield-alt"></i> <span>{{_i("Security Settings")}}</span></a>
        <a href="{{route("account-referrals")}}" class="list-group-item {{ request()->segment(3) == "referrals" ? "active" : "" }}"><i class="far fa-handshake"></i> <span>{{_i("Referrals")}}</span></a>
        <a href="{{route("account-contact")}}" class="list-group-item {{ request()->segment(3) == "contact" ? "active" : "" }}"><i class="fa fa-address-card"></i> <span>{{_i("Contact Settngs")}}</span></a>
        <a href="{{route("account-apis")}}" class="list-group-item {{ request()->segment(3) == "apis" ? "active" : "" }}"><i class="fa fa-code"></i> <span>{{_i("API Access")}}</span></a>
    </div>

    <div class="list-group mt-2">
        <a href="{{ route('logout') }}"
           onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="list-group-item"><i class="fas fa-power-off text-danger"></i> <span>{{_i("Sign Out")}}</span></a>
    </div>
</nav>

