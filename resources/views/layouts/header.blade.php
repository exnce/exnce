@if(env("APP_DEBUG") == true)
<link rel="stylesheet" href="{{ asset("components/bootstrap/css/bootstrap.min.css") }}?version={{env("JS_VERSION")}}" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset("fontawesome/css/all.min.css") }}?version={{env("JS_VERSION")}}" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset("fontawesome/css/solid.min.css") }}?version={{env("JS_VERSION")}}" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset("fontawesome/css/brands.min.css") }}?version={{env("JS_VERSION")}}" crossorigin="anonymous">
<link rel="stylesheet" href="{{ asset("css/circle.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("css/master.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("components/aos/aos.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("components/select2/css/select2.min.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("components/animsition/css/animsition.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("components/datatables/datatables.min.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("components/toastr/build/toastr.min.css") }}?version={{env("JS_VERSION")}}">
<link rel="stylesheet" href="{{ asset("css/dark.css") }}?version={{env("JS_VERSION")}}">
<link href="{{ asset("components/summernote/dist/summernote.css") }}?version={{env("JS_VERSION")}}" rel="stylesheet">
<link href="{{ asset("components/summernote/dist/summernote-bs4.css") }}?version={{env("JS_VERSION")}}" rel="stylesheet">
@else
<link rel="stylesheet" href="{{ asset("css/style.css") }}?version={{env("JS_VERSION")}}">
@endif


