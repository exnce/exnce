<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield("title")</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">

    <link rel="manifest" href="{{asset("manifest.json")}}">
    <meta name="csrf-token" content="{{csrf_token()}}">
    @if(@isset($metaTags))
        {!!html_entity_decode($metaTags)!!}
    @endif
    <link rel="shortcut icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">
    <link rel="icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">
    <!-- Bootstrap CSS -->
    @include("layouts.header")
</head>
@php
$user = auth()->user();
@endphp
<body class="dark bg-init">
<div class="animsition">
    <main role="main">
        <section id="sub-main">
            <div class="navbar-fixed">
                <div class="white-navbar">

                    <nav class="navbar navbar-expand-lg navbar-light d-lg-flex mh-100 container-fluid">
                        <a class="navbar-brand" href="{{route("dashboard")}}">
                            <img src="{{asset("images/logo.svg")}}" class="brand-logo" alt="{{env("APP_NAME")}}"/>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="{{env("APP_NAME")}}">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="navbar-collapse collapse pull-right" id="navbarSupportedContent">
                            <ul class="nav navbar-nav">

                                <li class="{{request()->segment(2) == "market" || !request()->segment(2) ? "active" : ""}}">
                                    <a href="{{ route("market") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fas fa-exchange-alt fa-fw"></i>
                                      </span>
                                        {{ _i("Market") }}</a>
                                </li>

                                <li class="{{request()->segment(2) == "ieo" ? "active" : ""}}">

                                    <a href="{{ route("ieo") }}">
                                           <span class="nav-icon">
                                        <i class="fa-2lg fas fa-project-diagram fa-fw"></i>
                                      </span>
                                        {{ _i("IEO") }}</a>
                                </li>

                                <li class="{{request()->segment(2) == "wallet" ? "active" : ""}}">

                                    <a href="{{ route("wallet") }}">
                                           <span class="nav-icon">
                                        <i class="fa-2lg fas fa-wallet fa-fw"></i>
                                      </span>
                                        {{ _i("Wallet") }}</a>
                                </li>

                                <li class="{{request()->segment(3) == "orders" ? "active" : ""}}">

                                    <a href="{{ route("list-transactions", "open") }}">
                                          <span class="nav-icon">
                                        <i class="fa-2lg fas fa-tasks fa-fw"></i>
                                      </span>
                                        {{ _i("Orders") }}</a>
                                </li>
                                {{--
                                <li class="nav-item px-3 py-2 {{request()->segment(2) == "trade" ? "sub-nav-active" : ""}}">
                                    <a class="nav-link text-navy-blue" href="{{ route("trade") }}">Kolay Alış - Satış</a>
                                </li>
                                --}}

                                <li class="{{request()->segment(2) == "withdraw" ? "active" : ""}}">

                                    <a href="{{ route("withdraw") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fa fa-upload fa-fw"></i>
                                      </span>
                                        {{ _i("Withdraw") }}</a>
                                </li>
                                <li class="{{request()->segment(2) == "deposit" ? "active" : ""}}">

                                    <a href="{{ route("deposit") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fa fa-download fa-fw"></i>
                                      </span>
                                        {{ _i("Deposit") }}</a>
                                </li>

                                <li class="{{request()->segment(2) == "support" ? "active" : ""}}">

                                    <a href="{{ route("support") }}">
                                          <span class="nav-icon">
                                        <i class="fa-2lg fas fa-headset fa-fw"></i>
                                      </span>
                                        {{_i("Support")}}</a>
                                </li>

                                <li class="{{request()->segment(2) == "airdrops" ? "active" : ""}}"><a href="{{ route('airdrops') }}">
                                            <span class="nav-icon">
                                        <i class="fa-2lg fas fa-grin-stars fa-fw"></i>
                                      </span>
                                        {{_i("Airdrop")}}</a></li>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </ul>



                            <ul class="nav navbar-nav ml-auto">

                                <li class="">
                                    <div class="btn-group-vertical p-0">
                                        <button type="button" class="lightTheme btn btn-sm btn-outline-light">
                                            <i class="fa-2lg fas fa-sun fa-fw text-warning"></i>
                                        </button>
                                        <button type="button" class="darkTheme btn btn-sm btn-outline-light">
                                            <i class="fa-2lg fas fa-moon fa-fw text-dark-sky"></i>
                                        </button>
                                    </div>

                                </li>

                                <li class="{{request()->segment(2) == "account" ? "active" : ""}}">
                                    <a class="nav-user arrow-none" href="{{ route("account") }}" role="button" aria-haspopup="false" aria-expanded="false">
                                    <span class="account-user-avatar nav-icon">
                                        <i class="fa-2lg fas fa-user-cog fa-fw"></i>
                                    </span>
                                        <span>
                                        <span class="account-user-name">{{$user->name}}</span>
                                         <span class="account-position">{{ getUserAccountType($user->user_type)}}</span>

                                    </span>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{ route("logout") }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          <span class="nav-icon">
                                        <i class="fa-2lg fas fa-sign-out-alt fa-fw text-danger"></i>
                                      </span>
                                        {{_i("Sign Out")}}</a>
                                </li>
                            </ul>


                        </div>

                    </nav>


                </div>


            </div>
        </section>
        <section class="mt-80 mb-5">
            @yield("content")
            @include("layouts.footer-content")
        </section>
    </main>
</div>

@include("layouts.footer")
@yield("script")
</body>
</html>
