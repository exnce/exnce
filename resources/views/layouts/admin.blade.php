
<!doctype html>
<html lang="tr">
<head>
    <title>@yield("title")</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">

    <link rel="shortcut icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">
    <link rel="icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">

    <!-- Bootstrap CSS -->
    @include("layouts.header")
</head>
@php
    $solvedStatusId = App\TicketStatus::where("key","solved")->get()->first()->id;
    $unsolvedTickets = App\Ticket::where("status_id","<>", $solvedStatusId)->count();
    $user = auth()->user();

@endphp
<body class="bg-init">
<div class="animsition">
    <main role="main">
        <section id="sub-main">
            <div class="navbar-fixed">
                <div class="white-navbar">
                    <nav class="navbar navbar-expand-lg navbar-light d-lg-flex mh-100 container-fluid">
                        <a class="navbar-brand" href="{{route("dashboard")}}">
                            <img src="{{asset("images/logo.svg")}}" class="brand-logo" alt="EXNCE"/>
                        </a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="EXNCE">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <div class="navbar-collapse collapse pull-right" id="navbarSupportedContent">
                            <ul class="nav navbar-nav">

                                <li class="{{request()->segment(3)."-".request()->segment(4) == "deposit-requests" ? "active" : ""}}">

                                    <a href="{{ route("deposit-requests") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fas fa-exchange-alt fa-fw"></i>
                                      </span>
                                        {{ _i("Deposits") }}<sup class="badge badge-pill badge-warning">0</sup></a>
                                </li>
                                <li class="{{request()->segment(3)."-".request()->segment(4) == "withdraw-requests" ? "active" : ""}}">

                                    <a href="{{ route("withdraw-requests") }}">
                                           <span class="nav-icon">
                                        <i class="fa-2lg fas fa-wallet fa-fw"></i>
                                      </span>
                                        {{ _i("Withdrawals") }}<sup class="badge badge-pill badge-warning">0</sup></a>
                                </li>
                                <li class="{{request()->segment(3) == "verification-requests" ? "active" : ""}}">

                                    <a href="{{ route("verification") }}">
                                          <span class="nav-icon">
                                        <i class="fa-2lg fas fa-tasks fa-fw"></i>
                                      </span>
                                        {{ _i("Verifications") }}<sup class="badge badge-pill badge-warning">0</sup></a>
                                </li>


                                <li class="{{request()->segment(2) == "support" ? "active" : ""}}">

                                    <a href="{{ route("admin-support-requests") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fa fa-headset fa-fw"></i>
                                      </span>
                                        {{ _i("Support") }}<sup class="badge badge-pill badge-warning">{{$unsolvedTickets}}</sup></a>
                                </li>

                                <li class="{{request()->segment(2) == "server" ? "active" : ""}}">

                                    <a href="{{ route("block-scan") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fas fa-compass fa-fw"></i>
                                      </span>
                                        {{ _i("Server") }}</a>
                                </li>

                                <li class="{{request()->segment(2) == "reports" ? "active" : ""}}">

                                    <a href="{{ route("reports") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fas fa-hands fa-fw"></i>
                                      </span>
                                        {{ _i("Reports") }}</a>
                                </li>


                                <li class="{{request()->segment(2) == "users" ? "active" : ""}}">

                                    <a href="{{ route("users") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fa fa-user fa-fw"></i>
                                      </span>
                                        {{ _i("Users") }}<sup class="badge badge-pill badge-warning">{{$unsolvedTickets}}</sup></a>
                                </li>

                                <li class="{{request()->segment(2) == "contacts" ? "active" : ""}}">

                                    <a href="{{ route("contacts") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fa fa-user fa-fw"></i>
                                      </span>
                                        {{ _i("Contact") }}<sup class="badge badge-pill badge-warning">0</sup></a>
                                </li>

                                <li class="{{request()->segment(2) == "definations" ? "active" : ""}}">

                                    <a href="{{ route("definations") }}">
                                        <span class="nav-icon">
                                        <i class="fa-2lg fas fa-tools fa-fw"></i>
                                      </span>
                                        {{ _i("Definations") }}<sup class="badge badge-pill badge-warning">{{$unsolvedTickets}}</sup></a>
                                </li>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </ul>


                            <ul class="nav navbar-nav ml-auto">


                                <li class="{{request()->segment(2) == "account" ? "active" : ""}}">
                                    <a class="nav-user arrow-none" href="{{ route("account") }}" role="button" aria-haspopup="false" aria-expanded="false">
                                    <span class="account-user-avatar nav-icon">
                                        <i class="fa-2lg fas fa-user-cog fa-fw"></i>
                                    </span>
                                        <span>
                                        <span class="account-user-name">{{$user->name}}</span>
                                         <span class="account-position">{{ getUserAccountType($user->user_type)}}</span>

                                    </span>
                                    </a>
                                </li>

                                <li class="">
                                    <a href="{{ route("logout") }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                          <span class="nav-icon">
                                        <i class="fa-2lg fas fa-sign-out-alt fa-fw text-danger"></i>
                                      </span>
                                        {{_i("Çıkış")}}</a>
                                </li>
                            </ul>
                        </div>

                    </nav>
                </div>




            </div>
        </section>
        <section id="panel" class="mt-80">
            <div class="col-md-12 pt-3">
                <div class="row">
{{--
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 p-1 pt-0">
                        <div class="sidebar card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">

                            <ul id="accordion1" class="nav nav-pills flex-column list-unstyled">
                                <a class="menu-title mb-2" data-toggle="collapse" href="#item-0" data-parent="#accordion1">Raporlar</a>
                                <div class="nav-item">
                                    <div id="item-0" class="collapse">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a href="#">Hesap Hareketleri</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#">Kulla</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#">Sub 1-3</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a class="menu-title mb-2" data-toggle="collapse" href="#item-15" data-parent="#accordion1">Kullanıcı İşlemleri</a>
                                <div class="nav-item">
                                    <div id="item-15" class="collapse {{request()->segment(2) == "users" ? "show" : ""}}">
                                        <ul class="nav flex-column">
                                            <li>
                                                <a href="{{ route("users") }}">Kullanıcı Listesi</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a class="menu-title mb-2" data-toggle="collapse" href="#item-20" data-parent="#accordion1">Duyuru İşlemleri</a>
                                <div class="nav-item">
                                    <div id="item-20" class="collapse {{request()->segment(2) == "announcement" ? "show" : ""}}">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a href="#">Sub 2-1</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#">Sub 2-2</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="#">Sub 2-3</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <a class="menu-title mb-2" data-toggle="collapse" href="#item-3" data-parent="#accordion1">Tanımlamalar</a>
                                <div class="nav-item">
                                    <div id="item-3" class="collapse {{request()->segment(2) == "definations" ? "show" : ""}}">
                                        <ul class="nav flex-column">
                                            <li class="nav-item">
                                                <a href="{{ route("cryptocurrency-definations") }}">Kripto Para Tanımlamaları</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route("fiatcurrency-definations") }}">Döviz Cinsi Tanımlamaları</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route("bank-definations") }}">Banka Tanımlamaları</a>
                                            </li>
                                            <li class="nav-item">
                                                <a href="{{ route("bank-account-definations") }}">Banka Hesabı Tanımlamaları</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </ul>

                        </div>
                    </div>
--}}
                    <div class="col-12 mb-5">
                        @yield("content")
                    </div>
                </div>
            </div>
        </section>

    </main>

</div>
@include("layouts.footer")
@yield("script")
</body>
</html>
