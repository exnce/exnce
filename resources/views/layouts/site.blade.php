
<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>@yield("title")</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="yandex-verification" content="ef7d7d35fe6f5951" />
    <link rel="shortcut icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">
    <link rel="icon" href="{{asset("images/favicon.ico")}}" type="image/x-icon">
    <meta name="google-site-verification" content="5ZPE5U7508yV9mEP3EXB_AdBdXw2H8WdnbRZ9_R_32E" />
     @if(@isset($metaTags))
     {!!html_entity_decode($metaTags)!!}
     @endif
    <!-- Bootstrap CSS -->
    @include("layouts.header")
    @yield('head')
</head>
@php
    $activeCoinID = session("activeCoinID");
    $activeCurrencyID = session("activeCurrencyID");

    if($activeCoinID && $activeCurrencyID) {
        $statsData = getHeaderHomeStatistics($activeCoinID, $activeCurrencyID);
    } else {
        $statsData = getHeaderHomeStatistics();
    }

    if($statsData["change_ratio"] > 0) {
        $szChangeColor = "text-success";
    } elseif($statsData["change_ratio"] < 0) {
        $szChangeColor = "text-danger";
    } else {
        $szChangeColor = "";
    }
@endphp
<body class="bg-init">
<div class="animsition">
    <main role="main">
        <section id="sub-main">
            <div class="navbar-fixed">
                <div class="white-navbar">


                    <nav class="navbar navbar-expand-lg navbar-light d-lg-flex mh-100 container-fluid">
                        <a class="navbar-brand" href="{{route("home")}}">
                            <img src="{{asset("images/logo.svg")}}" class="brand-logo" alt="EXNCE"/>
                        </a>



                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                                aria-expanded="false" aria-label="{{env("APP_NAME")}}">
                            <span class="navbar-toggler-icon"></span>
                        </button>


                        <div class="navbar-collapse collapse pull-right" id="navbarSupportedContent">

                            <ul class="nav navbar-nav ml-auto">

                                <li class=""><a href="{{ route('siteairdrops') }}">
                                            <span class="nav-icon">
                                        <i class="fa-2lg fas fa-grin-stars fa-fw"></i>
                                      </span>
                                        {{_i("Airdrops")}}</a></li>

                                <li class=""><a href="{{ route('login') }}">
                                            <span class="nav-icon">
                                        <i class="fa-2lg fas fa-user-lock fa-fw"></i>
                                      </span>
                                        {{_i("Sign In")}}</a></li>
                                <li class=""><a href="{{ route('register') }}">
                                            <span class="nav-icon">
                                        <i class="fa-2lg fas fa-user-plus fa-fw"></i>
                                      </span>{{_i("Sign Up")}}</a></li>
                                <li class=""><a href="{{ route('password.request') }}">
                                            <span class="nav-icon">
                                        <i class="fa-2lg fas fa-user-cog fa-fw"></i>
                                      </span>
                                        {{_i("Forgot Password")}}</a></li>

                            </ul>


                        </div>

                    </nav>






                </div>
            </div>
        </section>
        <section class="mb-5 {{request()->segment(1) == "market" || !request()->segment(1) ? "mt-80" : "mt-80"}}">
            @yield("content")
        </section>
    </main>
</div>
@include("layouts.footer")
@yield("script")
</body>
</html>
