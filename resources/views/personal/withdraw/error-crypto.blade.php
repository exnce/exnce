
<div class="row">
    <div class="col-12 text-center">

    </div>
    <div class="col-12">
        @if($coin->status != 0)
         <p class="alert alert-danger mt-3 mb-0 ">{{ _i("%s (%s) isimli kripto para birimi sisteme eklenmiş ancak henüz aktif edilmemiştir.", $coin->name, $coin->symbol)}}</p>
        @elseif(!$wallet)
        <p class="alert alert-danger mt-3 mb-0 ">{{ _i("%s (%s) isimli kripto para birimine ait yeni bir cüzdan tanımlamanız eklemeniz gerekmektedir.", $coin->name, $coin->symbol) }}</p>
        @endif
            {{--@else(!auth()->user()->identityApproval(2))--}}
        {{--<p class="alert alert-danger mt-3 mb-0 ">{{ _i('%s (%s) çekme işlemi için hesabınızı onaylatmanız gerekmektedir.', $coin->name, $coin->symbol) }}</p>--}}
        {{--@endif--}}
    </div>
</div>
