
@extends('layouts.personal')
@section("title",_i("Kripto Para Çekme Onayı"))

@php
$szSubTitle= _i("%s çekme işlemini onaylamanız beklenmektedir...");
$szSubTitle = sprintf($szSubTitle,$coin->symbol);
@endphp
@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-12">


                    <form method="post">
                        @csrf
                        <div class="pb-4" id="accordion">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3 verify_withdraw_form">
                                <div class="card-header">
                                    <h5 class="mb-0">{{_i("Kripto Para Çekme Talebi Onayı")}}</h5>
                                    <span class="d-none d-lg-block d-md-block">{{$szSubTitle}}</span>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>{{ _i("Para Birimi:") }}</b></span>
                                        <input type="text"  readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $coin->name }} ({{$coin->symbol}})">
                                    </div>
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>{{ _i("Transfer Edilecek Adres:") }}</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $bank->payment_address }}">
                                    </div>

                                    <div class="form-group {{($coin->symbol != "XRP" ? "hidden":"")}}">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>{{ _i("Transfer Destination Tag:") }}</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $bank->destination_tag }}">
                                    </div>

                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>{{ _i("Transfer Edilecek Tutar:") }}</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ format_coin_amount($order->amount, $coin) }}">
                                    </div>

                                    @if (floatval($order->fee)>0)
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>{{ _i("Toplam Tutar:") }}</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ format_coin_amount($order->amount+$order->fee, $coin) }}">
                                    </div>
                                    @endif

                                    <div class="form-check">
                                        <input id="save_account" name="save_account" class="form-check-input" type="checkbox">
                                        <label class="form-check-label font-weight-bold text-dark-bluish text-left" for="save_account">{{ _i("Kayıtlı hesaplara eklensin mi?") }}</label>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control rounded-soft bordered-input colored-placeholder" id="account_name" name="account_name" type="text" placeholder="{{ _i("Hesap Kayıt Adı") }}">
                                    </div>
                                    <div class="form-group">
                                        <button id="cancel_withdraw_button" type="submit" value="cancel" formaction="{{route("withdraw-confirm-crypto",$coin->symbol)}}/cancel" name="cancel_withdraw_button" class="btn btn-danger btn-block pull-left">{{ _i("TALEBİ İPTAL ET") }}</button>
                                        <button id="verify_withdraw_button" type="submit" value="accept" formaction="{{route("withdraw-confirm-crypto",$coin->symbol)}}/accept" name="verify_withdraw_button" class="btn btn-primary btn-block pull-right">{{ _i("ONAYLIYORUM") }}</button>
                                    </div>
                                </div>
                            </div>

                        </div>

{{--
                        <div class="pb-4">
                            <p class="mb-0"><b>24 saatlik kalan kripto para çekme limitiniz: 250.000,00 {{$coin->symbol}}</b></p>
                            <p class="mb-0"><b>30 günlük kalan kripto para çekme limitiniz: 2.000.000,00 {{$coin->symbol}}</b></p>
                        </div>
--}}
                    </form>

                </div>

                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.withdraw.crypto-menu")
                </div>

            </div>

        </div>
    </section>
@endsection
