@extends('layouts.personal')
@section("title",_i("Kripto Para Çekme"))

@php
    $szDescription = _i("Minimum %s %s çekimi yapabilirsiniz.");
    $szDescription = sprintf($szDescription,floatval($coin->min_withdraw),$coin->name);

    $szFeeDescription = _i("%s transferlerinde işlemin hızlı gerçekleşmesi için %s fee kullanilir. Bu tutar madencilere aittir.");
    $szFeeDescription = sprintf($szFeeDescription,$coin->name,floatval($coin->fee));

    $szDeactivedCurrency = _i("%s (%s) isimli para birimi ile para yatırımı henüz aktif edilmemiştir.");
    $szDeactivedCurrency = sprintf($szDeactivedCurrency,$coin->name, $coin->symbol);

    $szDontHaveBankAccount = _i("Kayıtlı %s (%s) hesabınız yoktur.");
    $szDontHaveBankAccount = sprintf($szDontHaveBankAccount,$coin->name, $coin->symbol);

@endphp


@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12">

                    <form id="withdraw-form" action="{{route("withdraw-crypto-create",$coin->symbol)}}" method="post">
                        @csrf


                        <div class="pb-4" id="accordion">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="headingOne">
                                    <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h5 class="mb-0">{{_i("Kripto Para Transferi ile 7/24 anında para çekebilirsiniz.")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                        <span>{{$szDescription}}</span>
                                    </a>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">


                                    {{--@if ((!$wallet) || ($coin->status != 0) || (!auth()->user()->identityApproval(2)))--}}

                                    @if ((!$wallet) || ($coin->status != 0))
                                    <div class="card-body">
                                       @include("personal.withdraw.error-crypto")
                                    </div>
                                    @else
                                    <div class="card-body">

                                        <div class="form-group">
                                            <div class="form-group">
                                                @if ($errors->has('invalid_wallet'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('invalid_wallet') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif
                                                @if ($errors->has('less_balance'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('less_balance') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif
                                                @if ($errors->has('wrong_amount'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('wrong_amount') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif
                                                @if ($errors->has('wrong_receiver'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('wrong_receiver') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif
                                                @if ($errors->has('min_withdraw'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('min_withdraw') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif
                                                @if ($errors->has('iban'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ _i("Girilen ödeme adresi hatalıdır.") }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif

                                                @if ($errors->has("amount_integer") || $errors->has("amount_decimal") )
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ _i("Girilen tutar hatalıdır.") }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Para Birimi") }}</span>
                                            <input id="currency_name" type="text" disabled="disabled" class="form-control bordered-input colored-placeholder" value="{{$coin->name}} ({{$coin->symbol}})">
                                            <input type="text" readonly="true" id="currency_id" name="currency_id" class="form-control hidden" value="{{$coin->id}}"/>
                                            <input type="text" readonly="true" id="currency_type" name="currency_type" class="form-control hidden" value="0"/>

                                            @if ($errors->has('currency_id'))
                                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('currency_id') }}</strong>
                                             </span>
                                            @endif
                                        </div>


                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Cüzdan Bakiyeniz") }}</span>
                                            <input id="wallet_balance" type="text" disabled="disabled" class="form-control bordered-input colored-placeholder" value="{{$balance}}">
                                        </div>

                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Çekmek İstediğiniz Tutar ") }}</span>
                                            <div class="input-group">
                                                <input id="amount_integer" name="amount_integer" type="text" min="0" class="form-control bordered-input text-right border-right-1" placeholder="100">
                                                <span class="comma-seperator bg-gradient-blue">. </span>
                                                <input id="amount_decimal" name="amount_decimal" type="text" min="0" class="form-control bordered-input border-right-0" placeholder="00">
                                                <div class="input-group-addon currency-addon text-white sub-nav-active border-0" id="basic-addon2">
                                                    <div class="marketSellCoinSymbol mt-2 small-text pl-2 pr-2">{{ $coin->symbol }}</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Cüzdan Adresi") }}</span>
                                            <input id="iban" name="iban" type="text" maxlength="100" class="form-control bordered-input" />
                                        </div>

                                        <div class="form-group {{($coin->symbol != "XRP" ? "hidden":"")}}">
                                            <span class="font-weight text-dark">{{ _i("Destination Tag") }}</span>
                                            <input id="bank_id" name="bank_id" type="text" maxlength="100" class="form-control bordered-input" value="-" />
                                        </div>

                                        <div class="form-group">
                                            <button class="btn btn-primary btn-block sub-nav-active mt-3">{{$coin->symbol}} {{ _i("ÇEK") }}</button>
                                        </div>

                                        <div class="form-group">
                                            <div class="well">
                                                <ul>
                                                    @if($coin->min_withdraw > 0)
                                                    <li>{{$szDescription}}</li>
                                                    @endif
                                                    <li>{{$szFeeDescription}}</li>
                                                </ul>
                                            </div>
                                        </div>

                                    </div>
                                    @endif
                                </div>
                            </div>

                        </div>


                    </form>

                </div>
                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.withdraw.crypto-menu")
                </div>

                <div class="col-12">
                    <div class="card card rounded border-0 box-shadow  pt-3 px-3 py-3 mb-3">
                        <div class="card-header" id="headingBankAccounts">
                            <a data-toggle="collapse" data-target="#bankAccounts" aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="mb-0">{{ _i("Kayıtlı Hesaplar") }}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                            </a>
                            <span class="d-none d-lg-block d-md-block">{{_i("Önceki işlemlerinizde transferlerinizde kullanmış olduğunuz kripto para hesapları")}}</span>
                        </div>

                        <div id="bankAccounts" class="collapse show" aria-labelledby="headingBankAccounts" data-parent="#bankAccounts">
                            <div class="card-body">
                                @if ( count($userCoinAccounts) > 0 )
                                    <table class="table table-striped table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th width="20%">{{_i("Kayıt Adı")}}</th>
                                            <th width="30%">{{_i("Cüzdan Adresi")}}</th>
                                            <th width="30%">{{_i("Destination Tag")}}</th>
                                            <th width="10%" class="text-center">{{_i("Kullan")}}</th>
                                            <th width="10%" class="text-center">{{_i("Sil")}}</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($userCoinAccounts as $account)
                                            <tr>
                                                <td class="accountName">{{$account->name}}</td>
                                                <td class="accountIBAN">{{$account->payment_address}}</td>
                                                <td class="accountBankName">{{$account->destination_tag}}</td>

                                                <td align="center">
                                                    <a class="btn btn-success btn-block btn-sm useAccount" data-bank_id="{{$account->bank_id}}" href="#"><i class="fa fa-check "></i></a>
                                                </td>
                                                <td align="center">
                                                    <a class="btn btn-danger btn-block btn-sm removeAccount" data-id="{{$account->id}}" href="#"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="alert alert-info">{{$szDontHaveBankAccount}}</p>
                                @endif
                            </div>
                        </div>
                    </div>

{{--
                    <div class="col-12 pt-4">
                        <p class="mb-0"><b>24 saatlik kalan para çekme limitiniz: 250.000,00 {{$coin->symbol}}</b></p>
                        <p class="mb-0"><b>30 günlük kalan para çekme limitiniz: 2.000.000,00 {{$coin->symbol}}</b></p>
                    </div>
--}}
                </div>
            </div>
            </div>
        </div>
    </section>
@endsection
