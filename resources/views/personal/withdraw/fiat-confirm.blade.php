@extends('layouts.personal')
@section("title",_i("Para Çekme Onayı"))

@php
$szSubTitle= _i("%s çekme işlemini onaylamanız beklenmektedir...");
$szSubTitle = sprintf($szSubTitle,$currency->symbol);
@endphp
@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-12">


                    <form method="post">
                        @csrf
                        <div class="pb-4" id="accordion">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3 verify_withdraw_form">
                                <div class="card-header">
                                    <h5 class="mb-0">{{_i("Para Çekme Talebi Onayı")}}</h5>
                                    <span class="d-none d-lg-block d-md-block">{{$szSubTitle}}</span>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>Hesap Sahibi:</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $user_fullname  }}">
                                    </div>
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>Banka:</b></span>
                                        <input type="text"  readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $bank_name }}">
                                    </div>
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>IBAN:</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $iban }}">
                                    </div>
                                    <div class="form-group">
                                        <span class="font-weight-bold text-dark-bluish text-left"><b>Çekilecek Tutar:</b></span>
                                        <input type="text" readonly class="form-control rounded-soft bordered-input colored-placeholder" value="{{ $amount }} {{$currency->symbol}}">
                                    </div>
                                    <div class="form-check">
                                        <input id="save_account" name="save_account" class="form-check-input" type="checkbox">
                                        <label class="form-check-label font-weight-bold text-dark-bluish text-left" for="save_account">Kayıtlı hesaplara eklensin mi?</label>
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control rounded-soft bordered-input colored-placeholder" id="account_name" name="account_name" type="text" placeholder="Hesap Kayıt Adı">
                                    </div>
                                    <div class="form-group">
                                        <button id="cancel_withdraw_button" type="submit" value="cancel" formaction="{{route("withdraw-confirm-fiat",$currency->symbol)}}/cancel" name="cancel_withdraw_button" class="btn btn-danger btn-block pull-left">TALEBİ İPTAL ET</button>
                                        <button id="verify_withdraw_button" type="submit" value="accept" formaction="{{route("withdraw-confirm-fiat",$currency->symbol)}}/accept" name="verify_withdraw_button" class="btn btn-primary btn-block pull-right">ONAYLIYORUM</button>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="pb-4">
                            <p class="mb-0"><b>24 saatlik kalan para çekme limitiniz: 250.000,00 {{$currency->symbol}}</b></p>
                            <p class="mb-0"><b>30 günlük kalan para çekme limitiniz: 2.000.000,00 {{$currency->symbol}}</b></p>
                        </div>
                    </form>

                </div>

                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.withdraw.fiat-menu")
                </div>

            </div>

        </div>
    </section>
@endsection
