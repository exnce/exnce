@extends('layouts.personal')
@section("title",_i("Para Çekme"))

@php
    $szDescription = _i("Banka havalesini yalnızca %s %s ve üzeri havale işlemleri için kullanabilirsiniz.");
    $szDescription = sprintf($szDescription,$currency->min_withdraw, $currency->symbol);

    $szDeactivedCurrency = _i("%s (%s) isimli para birimi ile para çekimi henüz aktif edilmemiştir.");
    $szDeactivedCurrency = sprintf($szDeactivedCurrency,$currency->name, $currency->symbol);

    $szDontHaveBankAccount = _i("Kayıtlı %s (%s) hesabınız yoktur.");
    $szDontHaveBankAccount = sprintf($szDontHaveBankAccount,$currency->name, $currency->symbol);


@endphp


@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12">

                    <form id="withdraw-form" action="{{route("withdraw-fiat-create",$currency->symbol)}}" method="post">
                        @csrf


                        <div class="pb-4" id="accordion">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3">
                                <div class="card-header" id="headingOne">
                                    <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h5 class="mb-0">{{_i("Banka Havalesi ile 7/24 anında para çekebilirsiniz.")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{$szDescription}}</span>
                                    </a>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                @if($currency->status==0)
                                    <div class="card-body">

                                        <div class="form-group">
                                            @if ($errors->has('invalid_wallet'))
                                                <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('invalid_wallet') }}
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif
                                            @if ($errors->has('less_balance'))
                                                    <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('less_balance') }}
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                            @endif
                                            @if ($errors->has('iban'))
                                                    <div class="alert alert-danger alert-dismissible fade show">Girilen IBAN numarası hatalıdır.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                            @endif

                                            @if ($errors->has('currency_id'))
                                                <div class="alert alert-danger alert-dismissible fade show">Para birimi hatalıdır.
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            @endif

                                            @if ($errors->has("amount_integer") || $errors->has("amount_decimal") )
                                                    <div class="alert alert-danger alert-dismissible fade show">Girilen tutar hatalıdır.
                                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                            @endif
                                        </div>

                                        <div class="form-row">
                                        <div class="form-group col-lg-6 col-sm-12">
                                            <span class="font-weight text-dark">{{ _i("Para Birimi") }}</span>
                                            <input id="currency_name" type="text" disabled="disabled" class="form-control rounded-soft bordered-input colored-placeholder" value="{{$currency->name}} ({{$currency->symbol}})">
                                            <input type="text" readonly="true" id="currency_id" name="currency_id" class="form-control hidden" value="{{$currency->id}}"/>
                                            <input type="text" readonly="true" id="currency_type" name="currency_type" class="form-control hidden" value="{{true}}"/>
                                        </div>


                                        <div class="form-group col-lg-6 col-sm-12">
                                            <span class="font-weight text-dark">{{ _i("Cüzdan Bakiyeniz") }}</span>
                                            <input id="wallet_balance" type="text" disabled="disabled" class="form-control text-right rounded-soft bordered-input colored-placeholder" value="{{$balance}}">
                                        </div>
                                        </div>
                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Çekmek İstediğiniz Tutar ") }}</span>
                                            <div class="input-group">
                                                <input id="amount-integer" name="amount_integer" type="text" min="0" class="form-control text-right currency-input border-right-1" placeholder="100">
                                                <span class="comma-seperator bg-gradient-blue">. </span>
                                                <input id="amount-decimal" name="amount_decimal" type="text" min="0" class="form-control currency-input border-right-0" placeholder="00">
                                                <div class="input-group-addon currency-addon text-white sub-nav-active border-0" id="basic-addon2">
                                                    <div class="rotate mt-2 small-text pl-2 pr-2">{{ $currency->symbol }}</div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Lütfen Banka Seçiniz") }}</span>

                                            <select id="bank_id" name="bank_id" class="form-control selectBankAccount bordered-input js-states" placeholder="Choose anything" data-allow_clear="1">
                                                @foreach($bankAccounts as $bank)
                                                    <option data-symbol="fa-university" value="{{$bank->id}}">{{$bank->bank_name}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("IBAN") }}</span>
                                            <input id="iban" name="iban" type="text" maxlength="34" class="form-control bordered-input" />

                                        </div>

                                        <div class="form-group">
                                            <button class="btn btn-primary btn-block sub-nav-active mt-3">{{$currency->symbol}} {{ _i("ÇEK") }}</button>
                                            <p class="alert alert-warning mt-3">{{$szDescription}}</p>

                                        </div>
                                    </div>
                                @else
                                    <div class="card-body">
                                        <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Para Birimi") }}</span>
                                            <input id="currency_name" type="text" disabled="disabled" class="form-control rounded-soft bordered-input colored-placeholder" value="{{$currency->name}} ({{$currency->symbol}})">
                                            <input type="text" readonly="true" id="currency_id" name="currency_id" class="form-control hidden" value="{{$currency->id}}"/>
                                            <input type="text" readonly="true" id="currency_type" name="currency_type" class="form-control hidden" value="{{true}}"/>
                                        </div>

                                        <div class="alert alert-danger alert-dismissible fade show">{{ $szDeactivedCurrency}}

                                        </div>
                                    </div>
                                    @endif
                                </div>
                            </div>

                        </div>


                    </form>

                </div>
                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.withdraw.fiat-menu")
                </div>
                <div class="col-12">
                    <div class="card card rounded border-0 box-shadow  pt-3 px-3 py-3">
                        <div class="card-header" id="headingBankAccounts">
                            <a data-toggle="collapse" data-target="#bankAccounts" aria-expanded="true" aria-controls="collapseOne">
                                <h5 class="mb-0">{{ _i("Kayıtlı Hesaplar") }}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                            </a>
                            <span class="d-none d-lg-block d-md-block">{{_i("Önceki işlemlerinizde kullanmış olduğunuz banka hesapları")}}</span>
                        </div>

                        <div id="bankAccounts" class="collapse show" aria-labelledby="headingBankAccounts" data-parent="#bankAccounts">
                            <div class="card-body">
                                @if ( count($userBankAccounts) > 0 )
                                    <table class="table table-striped table-hover table-responsive">
                                        <thead>
                                        <tr>
                                            <th width="20%">Kayıt Adı</th>
                                            <th width="30%">Banka Adı</th>
                                            <th width="30%">IBAN</th>
                                            <th width="10%" class="text-center">Kullan</th>
                                            <th width="10%" class="text-center">Sil</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($userBankAccounts as $account)
                                            <tr>
                                                <td class="accountName">{{$account->name}}</td>
                                                <td class="accountBankName">{{$account->bank->bank_name}}</td>
                                                <td class="accountIBAN">{{$account->iban_number}}</td>
                                                <td align="center">
                                                    <a class="btn btn-success btn-block btn-sm useAccount" data-bank_id="{{$account->bank_id}}" href="#"><i class="fa fa-check "></i></a>
                                                </td>
                                                <td align="center">
                                                    <a class="btn btn-danger btn-block btn-sm removeAccount" data-id="{{$account->id}}" href="#"><i class="fa fa-remove"></i></a>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @else
                                    <p class="alert alert-info">{{$szDontHaveBankAccount}}</p>
                                @endif
                            </div>
                        </div>
                </div>

                <div class="col-12 pt-4">
                        <p class="mb-0"><b>24 saatlik kalan para çekme limitiniz: 250.000,00 {{$currency->symbol}}</b></p>
                        <p class="mb-0"><b>30 günlük kalan para çekme limitiniz: 2.000.000,00 {{$currency->symbol}}</b></p>
                </div>
            </div>
        </div>
    </section>
@endsection
