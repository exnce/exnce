@extends('layouts.personal')
@section("title",_i("Withdrawals"))
@section('content')
    <section id="panel" class="mt-3">
        <div class="container-fluid pt-3">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 px-0">

                    <div class="col">
                        <div class="pb-4" id="cryptoCurrency">
                            <div class="card card rounded box-shadow pt-3 px-3 py-3">
                                <div class="card-header" id="cryptoCurrencyCard">
                                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                                       aria-controls="collapseCrypto">
                                        <h5 class="mb-0">{{ _i("Withdrawals") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("You can make a withdrawal by selecting the crypto currency you want.") }}</span>
                                    </a>
                                </div>
                                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0">
                                        <div class="row">
                                            <div class="col-12 ">


                                                @if ($errors->any())
                                                    @foreach ($errors->all() as $error)

                                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                            <strong>{{_i("Error!")}}</strong> {{$error}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="{{_i("Close")}}">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                    @endforeach
                                                @endif

                                                    @if (session('status'))
                                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                            <strong>{{_i("Info")}}</strong> {{_i("Your withdraw request has been successfully created.")}}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="{{_i("Close")}}">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif


                                                <form method="post" action="{{route("withdraw")}}/create">
                                                    @csrf
                                                    <div class="form-row">

                                                        <div class="col-lg-12 col-sm-12 mb-3">
                                                            <div class="row">
                                                                <div class="col-lg-12 col-sm-12 col-xs-12 mb-2">
                                                                    <label>{{_i("Select Wallet")}}</label>
                                                                    <select name="activeWallet" id="activeWallet"
                                                                            class="withdrawCoinList form-control">
                                                                        <option disabled selected>{{_i("Please select a wallet")}}</option>
                                                                        @foreach($wallets as $wallet)
                                                                            @if($wallet->can_create == false)
                                                                                <option data-fee="{{$wallet->fee}}"
                                                                                        data-balance="{{$wallet->balance}}"
                                                                                        data-processingbalance="{{$wallet->processingBalance}}"
                                                                                        data-fixedbalance="{{$wallet->fixedBalance}}"
                                                                                        data-symbol="{{$wallet->symbol}}"
                                                                                        data-icon="{{$wallet->coin_icon}}"
                                                                                        data-minwithdraw="{{$wallet->minwithdraw}}"
                                                                                        value="{{$wallet->symbol}}" {{$wallet->can_create ? ' disabled="disabled "' : ""}} >{{$wallet->name}}</option>
                                                                            @endif
                                                                        @endforeach
                                                                    </select>

                                                                </div>
                                                            </div>

                                                            <div class="row">

                                                                <div class="col-12 withdrawInputPanel collapse">

                                                                    <div class="row">
                                                                        <div class="col-lg-4 col-sm-6 col-xs-6">
                                                                            <label>{{_i("Available Balance")}}</label>


                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input readonly="readonly" type="text"
                                                                                       class="form-control walletAvaibleBalance"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>

                                                                        </div>

                                                                        <div class="col-lg-4 col-sm-6 col-xs-6">
                                                                            <label>{{_i("In Order")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input readonly="readonly" type="text"
                                                                                       class="form-control walletProcessingBalance"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-4 col-sm-6 col-xs-6">
                                                                            <label>{{_i("Total Balance")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input readonly="readonly" type="text"
                                                                                       class="form-control walletTotalBalance"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">

                                                                        <div class="col-lg-12 mb-2">
                                                                            <label>{{_i(" Withdrawal Address ")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input name="withdrawAddress"
                                                                                       id="withdrawAddress" type="text"
                                                                                       class="form-control"
                                                                                       placeholder=""
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-12 mb-2">
                                                                            <label>{{_i(" Payment ID / Destination TAG / Memo TAG ")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text">TAG</span>
                                                                                </div>
                                                                                <input name="withdrawTAG"
                                                                                       id="withdrawTAG" type="text"
                                                                                       class="form-control"
                                                                                       placeholder="{{"You can leave this field blank if there is no Destination Tag, Payment ID or Memo Tag data."}}"
                                                                                       aria-describedby="">
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label>{{_i("Amount")}}</label>


                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input id="withdrawAmount"
                                                                                       name="withdrawAmount" type="text"
                                                                                       class="form-control withdrawAmount"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>

                                                                        </div>


                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label>{{_i("Minimum Withdraw Amount")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input id="minimumWithdrawAmount"
                                                                                       name="minimumWithdrawAmount"
                                                                                       readonly="readonly" type="text"
                                                                                       class="form-control"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr/>
                                                                    <div class="row">



                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label>{{_i("Transaction Fee:")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input id="withdrawFee"
                                                                                       name="withdrawFee"
                                                                                       readonly="readonly" type="text"
                                                                                       class="form-control"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label>{{_i("You Will Get")}}</label>
                                                                            <div class="input-group">
                                                                                <div class="input-group-prepend">
                                                                                    <span class="input-group-text coin-symbol">-</span>
                                                                                </div>
                                                                                <input id="withdrawTotalAmount"
                                                                                       name="withdrawTotalAmount"
                                                                                       readonly="readonly" type="text"
                                                                                       class="form-control"
                                                                                       placeholder="0.000000"
                                                                                       aria-describedby="" required>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row">


                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label></label>
                                                                            <div class="input-group">
                                                                                <div class="toggle_radio noselect">
                                                                                    <input type="radio"
                                                                                           checked="checked"
                                                                                           data-index="0.25"
                                                                                           class="toggle_option"
                                                                                           id="first_toggle"
                                                                                           name="toggle_option">
                                                                                    <input type="radio"
                                                                                           data-index="0.50"
                                                                                           class="toggle_option"
                                                                                           id="second_toggle"
                                                                                           name="toggle_option">
                                                                                    <input type="radio"
                                                                                           data-index="0.75"
                                                                                           class="toggle_option"
                                                                                           id="third_toggle"
                                                                                           name="toggle_option">
                                                                                    <input type="radio" data-index="1"
                                                                                           class="toggle_option"
                                                                                           id="forth_toggle"
                                                                                           name="toggle_option">
                                                                                    <label for="first_toggle"><p>25%</p>
                                                                                    </label>
                                                                                    <label for="second_toggle"><p>50%</p>
                                                                                    </label>
                                                                                    <label for="third_toggle"><p>75%</p>
                                                                                    </label>
                                                                                    <label for="forth_toggle"><p>100%</p>
                                                                                    </label>
                                                                                    <div class="toggle_option_slider">
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>



                                                                        <div class="col-lg-6 col-sm-6 col-xs-6">
                                                                            <label></label>
                                                                            <div class="input-group">
                                                                                <button class="btn btn-success btn-block">{{_i("Submit Request")}}</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </form>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 px-0">
                    <div class="col pl-lg-0">
                        <div class="pb-4" id="withdrawHistory">
                            <div class="card card rounded box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="withdrawHistoryCard">
                                    <a data-toggle="collapse" data-target="#collapseWithdrawHistory"
                                       aria-expanded="true"
                                       aria-controls="collapseWithdrawHistory">
                                        <h5 class="mb-0">{{ _i("Withdrawal History") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("Latest Withdrawal Requests") }}</span>
                                    </a>
                                </div>
                                <div id="collapseWithdrawHistory" class="collapse show">
                                    <div class="card-body pl-0 pr-0 pb-0">
                                        <div class="row">
                                            <div class="col-12">
                                                <table id="lstWithdrawRequests"
                                                       class="table display noborderradius nowrap table-hover dataTable no-footer" style="100%; margin-top: 10px !important;" cellspacing="0" role="grid">
                                                    <thead class="table-head border-0">
                                                    <tr>
                                                        <th class="border-top-0">{{ _i("Asset") }}</th>
                                                        <th class="border-top-0">{{ _i("Amount") }}</th>
                                                        <th class="border-top-0">{{ _i("Date") }}</th>
                                                        <th class="border-top-0">{{ _i("Status") }}</th>
                                                        <th class="border-top-0">{{ _i("TxID") }}</th>
                                                        <th class="border-top-0">{{ _i("Reference Code") }}</th>
                                                        <th class="border-top-0">{{ _i("Cancel") }}</th>
                                                        <th class="border-top-0">{{ _i("Description") }}</th>
                                                        <th class="border-top-0"></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
@endsection
