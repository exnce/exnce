
@extends('layouts.personal')
@section("title",_i("Para Çekme"))
@section('content')
    <section id="panel" class="mt-3">
        <div class="container-fluid pt-3">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 px-0">

                    <div class="col">
                        <div class="pb-4" id="cryptoCurrency">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="cryptoCurrencyCard">
                                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                                       aria-controls="collapseCrypto">
                                        <h5 class="mb-0">{{ _i("Kripto Para Çekme") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("İstediginiz kripto para birimini seçerek para çekimi yapabilirsiniz.") }}</span>
                                    </a>
                                </div>
                                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0 pt-0">
                                        <div class="row">
                                        <div class="col-12 ">
                                








                                            <table id="lstDepositCoinList" name="lstDepositCoinList"
                                                   class="table display nowrap table-hover dataTable no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
                                                <thead class="table-head border-0">
                                                <tr>
                                                    <th>{{_i("Kripto Para")}}</th>
                                                    <th class="text-right">{{_i("Minimum Çekim")}}</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @foreach($coins as $coin)
                                                    <tr>
                                                        <td>
                                                            <a href="{{route("withdraw-crypto",$coin->symbol)}}">
                                                                <div class="kt-coin-card-v2">
                                                                    <div class="kt-coin-card-v2__pic">
                                                                        <img src="{{asset("/images/coins/".strtolower($coin->slug).".svg")}}">
                                                                    </div>
                                                                    <div class="kt-coin-card-v2__details">
                                                                        <span class="kt-coin-card-v2__name">
                                                                            {{ $coin->symbol }}
                                                                        </span>
                                                                        <span class="kt-coin-card-v2__email">{{$coin->name}}</span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                        </td>
                                                        <td class="vertical-middle text-right">{{$coin->min_withdraw}}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12 px-0">
                    <div class="col pl-lg-0">
                        <div class="pb-4" id="withdrawHistory">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="withdrawHistoryCard">
                                    <a data-toggle="collapse" data-target="#collapseWithdrawHistory"
                                       aria-expanded="true"
                                       aria-controls="collapseWithdrawHistory">
                                        <h5 class="mb-0">{{ _i("İşlem Geçmişi") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("Son Yaptığınız Çekim Talepleri") }}</span>
                                    </a>
                                </div>
                                <div id="collapseWithdrawHistory" class="collapse show">
                                    <div class="card-body pl-0 pr-0 pb-0">
                                        <div class="row">
{{--
                                            @include("personal.withdraw.transactions")
--}}
                                            <div class="col-12">
                                                <table id="lstWithdrawRequests"
                                                       class="table display table-striped noborderradius nowrap table-hover">
                                                    <thead class="table-head border-0">
                                                    <tr>
                                                            <th class="border-top-0">{{ _i("Para Birimi") }}</th>
                                                            <th class="border-top-0">{{ _i("Miktar") }}</th>
                                                            <th class="border-top-0">{{ _i("Tarih") }}</th>
                                                            <th class="border-top-0">{{ _i("Durum") }}</th>
                                                            <th class="border-top-0">{{ _i("TxID") }}</th>
                                                            <th class="border-top-0">{{ _i("Referans Kodu") }}</th>
                                                            <th class="border-top-0">{{ _i("İptal") }}</th>
                                                            <th class="border-top-0">{{ _i("Açıklama") }}</th>
                                                            <th class="border-top-0"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
@endsection
