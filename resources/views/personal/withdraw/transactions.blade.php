
<div class="col-12">
    <table class="table table-striped table-hover">
        <thead>
        <tr>
            <th>{{ _i("Birim") }}</th>
            <th>{{ _i("Miktar") }}</th>
            <th>{{ _i("Tarih") }}</th>
            <th>{{ _i("Durum") }}</th>
        </tr>
        </thead>
        <tbody>
        @if(count($orders)>0)
            @foreach($orders as $order)
                <tr>
                    <td class="align-middle">{{ $order->currency_type == true ? $order->currency->name : $order->coin->name}}</td>
                    <td class="align-middle" class="text-right">{{ $order->amount }}</td>
                    <td class="align-middle js-tooltip" data-toggle="tooltip" data-placement="bottom" title="{{ $order->updated_at }}">{{ \Carbon\Carbon::parse($order->updated_at)->format("d/m/Y") }}</td>
                    <td class="align-middle">
                        <div>{{ WithdrawStatusCode($order->status) }}</div>
                        @if($order->status == 5)
                            <small><a href="https://etherscan.io/tx/{{ $order->txId }}" target="_blank">{{ substr($order->txId, 0, 20) }}...</a> <i class="fa fa-copy js-tooltip copy-txid" data-toggle="tooltip"
                                                                                                                                                    data-placement="bottom" title="{{ _i("Panoya kopyala") }}" data-txid="{{ $order->txId }}"></i> </small>
                        @endif
                    </td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" style="height: 59px;"><div class="text-center">{{ _i("Burası şuan boş.") }}</div></td>
            </tr>
        @endif

        </tbody>
    </table>
</div>
