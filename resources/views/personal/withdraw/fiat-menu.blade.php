
<div class="row">
    @foreach($currencies as $currency)
        <div class="col-6 col-sm-12 col-xs-6 col-md-6 col-lg-6 pb-4 text-center">
            <a href="{{route("withdraw-fiat",$currency->symbol)}}">
                <div class="dwcurrency-item box-shadow mx-auto">
                    <b class="font-weight-bold">{{$currency->icon}}</b>
                    <p class="p-0 m-0 font-weight-bold">{{$currency->symbol}}</p>
                </div>
            </a>
        </div>
    @endforeach

    <div class="col-6 col-sm-12 col-xs-6 col-md-6 col-lg-6 pb-4 text-center">
        <a href="{{route("withdraw")}}">
            <div class="dwcurrency-item box-shadow mx-auto">
                <p class="fa fa-3x fa-arrow-left pt-3"></p>
                <p class="p-0 m-0 font-weight-bold">{{ _i("GERİ DÖN")  }}</p>
            </div>
        </a>
    </div>
</div>
