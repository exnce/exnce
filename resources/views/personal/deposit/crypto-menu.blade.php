
<div class="row">



<div class="col-12 ">
    <div class="card card rounded border-0 box-shadow mb-5">
    <table
           class="table display nowrap table-hover border-0 dataTable no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
        <thead class="table-head border-0">
        <tr>
            <th>{{_i("Asset")}}</th>
            <th class="text-right">{{_i("Minimum Deposit")}}</th>
        </tr>
        </thead>
        <tbody>
        @foreach($coins as $coin)
            <tr>
                <td>
                    <a href="{{route("deposit-crypto",$coin->symbol)}}">
                        <div class="kt-coin-card-v2">
                            <div class="kt-coin-card-v2__pic">
                                <img src="{{asset("/images/coins/".strtolower($coin->slug).".svg")}}">
                            </div>
                            <div class="kt-coin-card-v2__details">
                                                                        <span class="kt-coin-card-v2__name">
                                                                            {{ $coin->symbol }}
                                                                        </span>
                                <span class="kt-coin-card-v2__email">{{$coin->name}}</span>
                            </div>
                        </div>
                    </a>
                </td>
                <td class="vertical-middle text-right">{{$coin->min_deposit}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
</div>
</div>

