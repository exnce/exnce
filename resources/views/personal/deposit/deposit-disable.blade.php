
@extends('layouts.personal')
@section("title", $coin->name." (".$coin->symbol.")"._i(" Deposit"))

@php
    $szDescription = _i("QR Transfers from your code or wallet address are transferred to your stock market account.");

    $szDeactivedCurrency = _i("Cryptocurrency deposit with %s (%s) has not been activated yet.");
    $szDeactivedCurrency = sprintf($szDeactivedCurrency,$coin->name, $coin->symbol);

@endphp

@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-12">
                    <div class="pb-4" id="accordion">
                        <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="mb-0">{{$coin->name}} ({{$coin->symbol}}){{_i(" Deposit")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{$szDescription}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">

                                        @include("personal.deposit.coin.disabled")

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.deposit.crypto-menu")
                </div>

            </div>

        </div>
    </section>
@endsection
