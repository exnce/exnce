
@extends('layouts.personal')
@section("title",_i("Deposit"))
@section('content')
    <section id="panel" class="mt-3">
        <div class="container-fluid pt-3">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 px-0">
{{--
                    <div class="col">
                        <div class="pb-4" id="fiatCurrency">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="fiatCurrencyCard">

                                    <a data-toggle="collapse" data-target="#collapseFiat" aria-expanded="true"
                                       aria-controls="collapseFiat">
                                        <h5 class="mb-0">Para Yatırma<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">İstediginiz para birimini seçip hemen para yatırabilirsiniz</span>
                                    </a>
                                </div>
                                <div id="collapseFiat" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0">
                                        <div class="row">

                                            @foreach($currencies as $currency)
                                                <div class="col-6 col-lg-3 col-xs-6 col-sm-6 text-center mb-3">
                                                    <a href="{{route("deposit-fiat",$currency->symbol)}}">
                                                        <div class="dwcurrency-item box-shadow mx-auto">
                                                            <b class="font-weight-bold">{{$currency->icon}}</b>
                                                            <p class="p-0 m-0 font-weight-bold">{{$currency->symbol}}</p>
                                                        </div>
                                                    </a>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
--}}

                    <div class="col">
                        <div class="pb-4" id="cryptoCurrency">
                            <div class="card card-rounded box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="cryptoCurrencyCard">
                                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                                       aria-controls="collapseCrypto">
                                        <h5 class="mb-0">{{ _i("Deposit Cryptocurrency") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("You can select the crypto currency you want and deposit money immediately.") }}</span>
                                    </a>
                                </div>
                                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0 pt-0">

                                        <div class="row">


                                            <div class="col-12 ">
                                                <table id="lstDepositCoinList" name="lstDepositCoinList"
                                                       class="table display nowrap table-hover dataTable no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
                                                    <thead class="table-head border-0">
                                                    <tr>
                                                        <th>{{_i("Asset")}}</th>
                                                        <th class="text-right">{{_i("Minimum Deposit")}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($coins as $coin)
                                                        <tr>
                                                            <td>
                                                                <a href="{{route("deposit-crypto",$coin->symbol)}}">
                                                                <div class="kt-coin-card-v2">
                                                                    <div class="kt-coin-card-v2__pic">
                                                                        <img src="{{asset("/images/coins/".strtolower($coin->slug).".svg")}}">
                                                                    </div>
                                                                    <div class="kt-coin-card-v2__details">
                                                                        <span class="kt-coin-card-v2__name">
                                                                            {{ $coin->symbol }}
                                                                        </span>
                                                                        <span class="kt-coin-card-v2__email">{{$coin->name}}</span>
                                                                    </div>
                                                                </div>
                                                            </a>
                                                            </td>
                                                            <td class="vertical-middle text-right">{{$coin->min_deposit}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 px-0">
                    <div class="col pl-lg-0">
                        <div class="pb-4" id="depositHistory">
                            <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">
                                <div class="card-header" id="depositHistoryCard">
                                    <a data-toggle="collapse" data-target="#collapseDepositHistory" aria-expanded="true"
                                       aria-controls="collapseDepositHistory">
                                        <h5 class="mb-0">{{ _i("Transfer History") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{ _i("Your Last Deposit Requests") }}</span>
                                    </a>
                                </div>
                                <div id="collapseDepositHistory" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0">
                                        <div class="row">
                                            @include("personal.deposit.transactions")
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </section>
@endsection
