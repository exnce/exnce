@extends('layouts.personal')
@section("title",_i("Para Yatırma"))
@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">


                <div class="col-lg-9 col-md-9 col-sm-12">
                        <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header">
                                <h5>{{_i("Banka Hesabı Bilgilerimiz")}}</h5>
                                <span>{{_i("Yatırma talebiniz oluşturuldu. Para transferi yapmanız bekleniyor...")}}</span>
                            </div>
                            <div class="card-body pb-0">

                                <div class="row pb-0 mb-0">

                        <div class="form-group btn-block mb-0">

                                    <span class="font-weight-bold text-dark-bluish text-left">Banka:</span>
                                    <div class="input-group mb-2">
                                        <input id="bank" class="form-control border-right-1 border-right-0 bordered-input colored-placeholder" value="{{ $bank->bank->bank_name }}" type="text"/>
                                        <button class="btn copy btn-primary sub-nav-active text-white input-group-addon pull-right" data-clipboard-target="#bank"><i class="fa fa-clipboard"></i></button>
                                    </div>

                                    <span class="font-weight-bold text-dark-bluish text-left">IBAN:</span>

                                    <div class="input-group mb-2">
                                        <input id="ibanx" class="form-control  border-right-0 bordered-input colored-placeholder" value="{{ $bank->iban }}" type="text"/>
                                        <button class="btn copy btn-primary sub-nav-active text-white input-group-addon pull-right" data-clipboard-target="#ibanx"><i class="fa fa-clipboard"></i></button>
                                    </div>


                                    <span class="font-weight-bold text-dark-bluish text-left">Hesap Sahibi:</span>
                                    <div class="input-group mb-2">
                                        <input id="holder" class="form-control  border-right-0  bordered-input colored-placeholder" value="{{ $bank->name . " ". $bank->surname }}" type="text">
                                        <button class="btn copy btn-primary sub-nav-active text-white input-group-addon pull-right" data-clipboard-target="#holder"><i class="fa fa-clipboard"></i></button>
                                    </div>

                                    <span class="font-weight-bold text-dark-bluish text-left">Açıklama:</span>
                                    <div class="input-group mb-0">
                                        <input id="description" class="form-control border-right-0  bordered-input colored-placeholder" value="{{ $description }}" type="text">
                                        <button class="btn copy btn-primary sub-nav-active text-white input-group-addon pull-right" data-clipboard-target="#description"><i class="fa fa-clipboard"></i></button>
                                    </div>


                            <p class="alert alert-success btn-block mt-4">Açıklama kısmına <b>{{ $description }}</b> yazarak, <b>sahibi olduğunuz</b> banka hesabınızdan <b>{{ $amount }}</b> <b>{{ $bank->currency->symbol }}</b> transfer ediniz.</p>

                        </div>

                                </div>



                            </div>
                        </div>
                        <div class="pt-4">
                            <span>{{_i("İsteğinizde düzenleme yapmak istiyorsanız mevcut isteği iptal edip yeni bir istek oluşturmalısınız.")}}</span>
                            <button id="delete_deposit_request" data-currency="{{$currency->symbol}}" class="btn btn-danger mt-2 btn-block"> {{_i("TALEBİ İPTAL ET") }}</button>
                        </div>
                </div>
                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.deposit.fiat-menu")
                </div>

            </div>

        </div>
    </section>
@endsection
