<div class="wallet-card box-shadow">
    <ul id="accordion1" class="nav nav-pills flex-column list-unstyled">
        <a class="menu-title mb-2" data-toggle="collapse" href="#item-0" data-parent="#accordion1">Döviz Yatırma</a>
        </li></a>
        <div class="nav-item">
            <div id="item-0" class="collapse">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="#">KullHesap Hareketleri</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Kulla</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Sub 1-3</a>
                    </li>
                </ul>
            </div>
        </div>
        <a class="menu-title mb-2" data-toggle="collapse" href="#item-15" data-parent="#accordion1">Kripto Para Yatırma</a>
        <div class="nav-item">
            <div id="item-15" class="collapse">
                <ul class="nav flex-column">
                    <li>
                        <a href="#">Kullanıcı Listesi</a>
                    </li>
                    <li>
                        <a href="#">Sub 1-2</a>
                    </li>
                    <li>
                        <a href="#">Sub 1-3</a>
                    </li>
                </ul>
            </div>
        </div>
        <a class="menu-title mb-2" data-toggle="collapse" href="#item-20" data-parent="#accordion1">Yardım</a>
        <div class="nav-item">
            <div id="item-20" class="collapse {{request()->segment(2) == "announcement" ? "show" : ""}}">
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="#">Sub 2-1</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Sub 2-2</a>
                    </li>
                    <li class="nav-item">
                        <a href="#">Sub 2-3</a>
                    </li>
                </ul>
            </div>
        </div>
    </ul>
</div>
