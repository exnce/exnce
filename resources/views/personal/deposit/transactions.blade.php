
<div class="col-12 ">
    <table id="lstDepositRequests" name="lstDepositRequests"
           class="table display noborderradius nowrap table-hover dataTable no-footer" style="100%; margin-top: 10px !important;" cellspacing="0" role="grid">
        <thead class="table-head border-0">
        <tr>
            <th>{{_i("Date")}}</th>
            <th>{{_i("Asset")}}</th>
            <th>{{_i("Amount")}}</th>
            <th>{{_i("Description")}}</th>
            <th>{{_i("Details")}}</th>
            <th>{{_i("Action")}}</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @if(count($orders)>0)
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->currency_type == true ? $order->currency->name : $order->coin->name}}</td>
                    <td class="text-right">{{ $order->amount }}</td>
                    <td>{{ $order->updated_at }}</td>
                    <td>{{DepositStatusCode($order->status)}}</td>
                </tr>
            @endforeach
        @else
            <tr>
                <td colspan="4" style="height: 59px;">
                    <div class="text-center">{{ _i("You have no deposit history.") }}</div>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
