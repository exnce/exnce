<div class="row">
    <div class="col-md-3 text-center">
        <div class="qrcode pb-2">

            <img src="data:image/png;base64, {!! $image !!} ">


        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="row p-3 "><label for="wallet-address"><b>{{ _i("Address") }}</b></label>
            <div class="input-group position-relative">
                <input id="wallet-address" class="form-control wallet-input" placeholder="{{ _i("Address") }}"
                       aria-describedby="addon-kopyala" spellcheck="false" value="{{$wallet->address()->get()->last()->address}}"
                       type="text" readonly>

                <button type="button" class="btn btn-default btn-copy js-tooltip copy-wallet-address" data-toggle="tooltip"
                        data-placement="bottom" title="{{ _i("Copy to Clipboard") }}">
                    <i class="fa fa-clipboard clipboard-icon"></i>
                </button>
            </div>
            <div class="form-text d-block">
                @if($coin->min_deposit > 0)
                    <small>{!! _i("Minimum deposit amount : <big class='text-primary'><b>%s %s</b></big>", floatval($coin->min_deposit), $coin->symbol) !!}
                        </br>
                        <span class="text-danger">{{ _i("Deposits under %s %s will not be refunded.", floatval($coin->min_deposit), $coin->symbol) }}</span></small>
                @endif
            </div>
        </div>
    </div>
</div>
