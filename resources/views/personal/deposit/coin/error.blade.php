<div class="row">
    <div class="col-12 text-center">

    </div>
    <div class="col-12">
        @if($coin->status != 0)
         <p class="alert alert-danger mt-3 mb-0 ">{{ _i('The crypto currency %s (%s) has been added to the system but has not yet been activated.', $coin->name, $coin->symbol) }}</p>
        @else
        <p class="alert alert-danger mt-3 mb-0 ">{{ _i('You need to define a wallet belonging to the crypto currency %s (%s).', $coin->name, $coin->symbol) }}</p>
        @endif
    </div>
</div>
