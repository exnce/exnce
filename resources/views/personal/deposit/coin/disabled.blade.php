<div class="row">
    <div class="col-12 text-center">

    </div>
    <div class="col-12">
            <p class="alert alert-danger mt-3 mb-0 ">{{ _i('The crypto currency %s (%s) deposit is not currently active.', $coin->name, $coin->symbol) }}</p>
    </div>
</div>
