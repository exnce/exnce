
<div class="row">
    <div class="col-md-3 col-sm-12 text-center">
        <div class="qrcode pb-2">

            <img src="data:image/png;base64, {!! $image !!} ">


        </div>
    </div>
    <div class="col-md-9 col-sm-12">
        <div class="pt-3 "><label for="wallet-address"><b>{{ _i("Address") }}</b></label>
            <div class="input-group position-relative">
                <input id="wallet-address" class="form-control wallet-input" placeholder="{{ _i("Address") }}"
                       aria-describedby="addon-kopyala" spellcheck="false" value="{{$wallet->address()->get()->last()->address}}"
                       type="text" readonly>

                <button type="button" class="btn btn-default btn-copy js-tooltip copy-wallet-address" data-toggle="tooltip"
                        data-placement="bottom" title="{{ _i("Copy") }}">
                    <i class="fa fa-clipboard clipboard-icon"></i>
                </button>
            </div>


            <label class="mt-3" for="wallet-tag"><b>{{ _i("Memo Tag") }}</b></label>
            <div class="input-group">
                <input id="wallet-tag" class="form-control wallet-input" placeholder="{{ _i("Memo Tag") }}"
                       aria-describedby="addon-kopyala" spellcheck="false" value="{{10000000000 + $wallet->address()->get()->last()->id}}"
                       type="text" readonly>

                <button id="copyWalletTag" type="button" class="btn btn-default btn-copy js-tooltip copy-wallet-tag-address" data-toggle="tooltip"
                        data-placement="bottom" title="{{ _i("Copy") }}">
                    <i class="fa fa-clipboard clipboard-icon"></i>
                </button>
            </div>

            <div class="form-text">

                <small class="text-danger"> {{_i("It is very important that you enter the MemoTag correctly. Otherwise Stellar will not be passed to your account.")}}</small><br/>
                <small class="text-danger">{{_i("Be sure to check the accuracy of the area you have pasted the address you copied.")}}</small>



                @if($coin->min_deposit > 0)
                    <small class="text-danger">{!! _i("Minimum deposit amoun is %s %s. Deposit amounts that are lesser than %s %s will not be returned.", floatval($coin->min_deposit), $coin->symbol, floatval($coin->min_deposit), $coin->symbol) !!}
                @endif
            </div>
        </div>
    </div>
</div>
