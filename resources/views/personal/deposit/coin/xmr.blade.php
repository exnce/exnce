
<div class="row">
    <div class="col-12 text-center">
        <div class="qrcode pb-2">

            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->errorCorrection('H')->margin(0)->size(200)->generate( $wallet->wallet->address )) !!} ">


        </div>
    </div>
    <div class="col-12">
        <div class="box-shadow p-3 "><label for="wallet-address"><b>{{ _i("Cüzdan Adresiniz") }}</b></label>
            <div class="input-group position-relative">
                <input id="wallet-address" class="form-control wallet-input" placeholder="{{ _i("Cüzdan Adresiniz") }}"
                       aria-describedby="addon-kopyala" spellcheck="false" value="{{$wallet->wallet->address}}"
                       type="text" readonly>

                <button type="button" class="btn btn-default btn-copy js-tooltip copy-wallet-address" data-toggle="tooltip"
                        data-placement="bottom" title="{{ _i("Panoya kopyala") }}">
                    <i class="fa fa-clipboard clipboard-icon"></i>
                </button>
            </div>
            <div class="form-text text-dark-bluish">
                @if($coin->min_deposit > 0)
                    <small>{!! _i("En az <big class='text-primary'><b>%s %s</b></big> gönderebilirsiniz.", floatval($coin->min_deposit), $coin->symbol) !!}
                        <span class="text-danger">{{ _i("%s %s altında yatırma işlemleri iade edilmeyecektir.", floatval($coin->min_deposit), $coin->symbol) }}</span></small>
                @endif
            </div>
        </div>
    </div>
</div>
