@extends('layouts.personal')
@section("title",_i("Para Yatırma"))

@php
   $szDescription = _i("Banka havalesini yalnızca %s %s ve üzeri havale işlemleri için kullanabilirsiniz.");
   $szDescription = sprintf($szDescription,$currency->min_deposit, $currency->symbol);

   $szDeactivedCurrency = _i("%s (%s) isimli para birimi ile para yatırımı henüz aktif edilmemiştir.");
   $szDeactivedCurrency = sprintf($szDeactivedCurrency,$currency->name, $currency->symbol);


@endphp

@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">

                <div class="col-lg-9 col-md-9 col-sm-12">

                            <div class="pb-4" id="accordion">
                                <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                                    <div class="card-header" id="headingOne">
                                        <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            <h5 class="mb-0">{{_i("Banka Havalesi ile 7/24 anında para yatırabilirsiniz.")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                            <span class="d-none d-lg-block d-md-block">{{$szDescription}}</span>
                                        </a>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                        @if(($currency->status == 0) && (count($banks)!=0))
                                        <div class="card-body">


                                            <form id="deposit-form" method="post" action="{{route("deposit-fiat-create",$currency->symbol)}}">
                                                @csrf

                                                <div class="form-group">
                                                    @if ($errors->has('invalid_wallet'))
                                                        <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('invalid_wallet') }}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif
                                                    @if ($errors->has('less_balance'))
                                                        <div class="alert alert-danger alert-dismissible fade show">{{ $errors->first('less_balance') }}
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif
                                                    @if ($errors->has('iban'))
                                                        <div class="alert alert-danger alert-dismissible fade show">Girilen IBAN numarası hatalıdır.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif

                                                    @if ($errors->has("amount_integer") || $errors->has("amount_decimal") )
                                                        <div class="alert alert-danger alert-dismissible fade show">Girilen tutar hatalıdır.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif

                                                    @if ($errors->has("bank_id"))
                                                        <div class="alert alert-danger alert-dismissible fade show">Banka hatalıdır.
                                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                    @endif
                                                </div>

                                            <div class="form-group">
                                                <span class="font-weight text-dark">{{ _i("Para Birimi") }}</span>
                                                <input id="currency_name" type="text" disabled="disabled" class="form-control bordered-input colored-placeholder" value="{{$currency->name}} ({{$currency->symbol}})">
                                                <input type="text" readonly="true" id="currency_id" name="currency_id" class="form-control hidden" value="{{$currency->id}}"/>
                                                <input type="text" readonly="true" id="currency_type" name="currency_type" class="form-control hidden" value="{{true}}"/>
                                            </div>

                                            <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Yatırılacak Tutar ") }}</span>
                                            <div class="input-group mb-3">
                                                <input id="amount_integer" name="amount_integer" type="text" min="0" class="form-control text-right  form-control currency-input border-right-1" placeholder="100">
                                                <span class="comma-seperator">. </span>
                                                <input id="amount_decimal" name="amount_decimal" type="text" min="0" class="form-control  currency-input border-right-0" placeholder="00">
                                                <div class="input-group-addon currency-addon text-white sub-nav-active border-0" id="basic-addon2">
                                                    <div class="rotate mt-2 small-text pl-2 pr-2">{{ $currency->symbol }}</div>
                                                </div>
                                            </div>
                                            </div>


                                            <div class="form-group">
                                            <span class="font-weight text-dark">{{ _i("Lütfen Banka Seçiniz") }}</span>

                                            <select id="bank_id" name="bank_id" class="form-control selectBankAccount bordered-input" placeholder="Choose anything" data-allow_clear="1">
                                                @foreach($banks as $bank)
                                                    <option data-symbol="fa-university" value="{{$bank->id}}"  data-id="{{$bank->id}}" data-bankid="{{$bank->id}}">{{$bank->bank->bank_name}}</option>
                                                @endforeach
                                            </select>


                                            </div>

                                            <div class="form-group">
                                            <button type="submit" class="btn btn-primary sub-nav-active btn-block mt-3">{{_i("YATIRMA TALEBİ OLUŞTUR")}}</button>
                                            </div>

                                            <ul class="warnings mt-3">
                                                <li>Hesabınıza {{$currency->name}} yatırmak için, seçtiğiniz banka hesaplarımızdan birini kullanın.</li>
                                                <li>{{_i("Kendinize ait bir hesaptan transfer yapın. ATM'lerden kartsız olarak yapılan ödemeler kabul edilmez.")}}</li>
                                                <li>{{_i("Size özel oluşturulacak işlem kodunu ödemenizin açıklama kısmına girin. İşlem kodu eksik ödemeler iade edilecektir.")}}</li>
                                                <li>{{_i("İade için destek sayfamızdan bize ulaşabilirsiniz.")}}</li>
                                            </ul>

                                            <p class="alert alert-warning mt-3">Banka havalesini yalnızca 100,00 {{$currency->symbol}} ve üzeri havale işlemleri için kullanabilirsiniz.</p>
                                            </form>
                                        </div>
                                            @else
                                            @include("personal.deposit.error-fiat")
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="pb-4">
                                <p class="mb-0"><b>24 saatlik kalan para yatırma limitiniz: 250.000,00 {{$currency->symbol}}</b></p>
                                <p class="mb-0"><b>30 günlük kalan para yatırma limitiniz: 2.000.000,00 {{$currency->symbol}}</b></p>
                            </div>



                </div>

                <div class="sidebar col-lg-3 col-md-3 col-sm-12">
                    @include("personal.deposit.fiat-menu")

                </div>

            </div>

        </div>
    </section>
@endsection
