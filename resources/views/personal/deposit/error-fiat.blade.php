<div class="row">
    <div class="col-12 text-center">

    </div>
    <div class="col-12">
        @if($currency->status != 0)
            <p class="alert alert-danger mt-3 mb-0 ">{{$currency->name}} ({{$currency->symbol}}) isimli para birimi sisteme eklenmiş ancak henüz aktif edilmemiştir.</p>
        @elseif(count($banks)==0)
            <p class="alert alert-danger mt-3 mb-0 ">{{$currency->name}} ({{$currency->symbol}}) yatirabileceginiz bir banka hesabımiz simdilik mevcut değildir.</p>
        @else
            <p class="alert alert-danger mt-3 mb-0 ">{{$currency->name}} ({{$currency->symbol}}) isimli para birimine ait yeni bir cüzdan tanımlamanız eklemeniz gerekmektedir.</p>
        @endif
    </div>
</div>
