@extends('layouts.personal')
@php
    $activeCoinID = session("activeCoinID");
    $activeCurrencyID = session("activeCurrencyID");

    if($activeCoinID && $activeCurrencyID) {
        $statsData = getHeaderHomeStatistics($activeCoinID, $activeCurrencyID);
    } else {
        $statsData = getHeaderHomeStatistics();
    }

    if($statsData["change_ratio"] > 0) {
        $szChangeColor = "text-success";
    } elseif($statsData["change_ratio"] < 0) {
        $szChangeColor = "text-danger";
    } else {
        $szChangeColor = "";
    }
    $symbol = "{$activeCurrency->symbol}-{$activeCoin->symbol}";
    $selectedPair = "{$activeCurrency->symbol}/{$activeCoin->symbol}";
@endphp
@section("title",_i($selectedPair))
@section('content')

    <div class="container-fluid">
        <div class="row" style="height: calc(100vh - 149px);">



            <div class=exnce_pairs>
                <div class="logo fa fa-angle-right"><span class=blink>_</span></div>
                <div class=handle>
                    <div class="v-bar-a v-bar"></div>
                    <div class="v-bar-b v-bar"></div>
                </div>
            </div>
            <div class=outer-most>
                <div class=side-panel>
                    <div class=side-panel-overlay>
                        <div class=about-me>

                            <div class="rounded border-0 box-shadow p-0">
                                <div class="card-body pt-0 pb-0 px-0">
                                    <div class="market-section">
                                        <div class="currency-tabs">
                                            @foreach ($baseCoins as $coin_id => $baseCoin)
                                                <div class="tab-item {{ ( $activeCoin->id == $coin_id ? "active" : "" ) }}"
                                                     data-coinid="{{ $coin_id }}">
                                                    <span class="tab-item-text">{{ $baseCoin }}</span>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div>
                                            <table class="table currency-table fixed-table mb-0">
                                                <thead class="table-head text-center">
                                                <tr>
                                                    <th class="border-0 text-left"><span class="fav-btn filter"></span></th>
                                                    <th class="sorting border-0 text-left pl-1">{{ _i("Pair") }}</th>
                                                    <th class="border-0 text-left pl-1">{{ _i("Price") }}
                                                    </th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Volume") }}</th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Change") }}</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>

                                        <div class="table-wrapper for-coinsTable">
                                            <table class="table table-hover currency-table fixed-table">
                                                <tbody class="coinsTable">
                                                @foreach ($pairList as $pair)
                                                    @php
                                                        $strPair = $pair["currency"] . "-" . $pair["coin"];
                                                        $activeClass = $pair["coin_id"] == $activeCoin->id && $pair["currency_id"] == $activeCurrency->id ? "active" : "";

                                                        if($pair["change"] > 0) {
                                                            $changeClass = "text-success";
                                                        } elseif($pair["change"] < 0) {
                                                            $changeClass = "text-danger";
                                                        } else {
                                                            $changeClass = "";
                                                        }

                                                    @endphp
                                                    <tr class="pair" data-coinid="{{ $pair["coin_id"] }}"
                                                        data-currency="{{ $pair["currency_id"] }}"
                                                        data-pair="{{ $strPair }}"
                                                        style="{{ ( $activeCoin->id != $pair["coin_id"] ? "display:none;" : "" ) }}">

                                                        <td class="{{ $activeClass }}"><span
                                                                    class="fav-btn {{ in_array($strPair, $favorites) ? "checked" : "" }}"></span>
                                                        </td>
                                                        <td>
                                                            <img src="{{asset("/images/coins/".strtolower($pair["slug"]).".svg")}}"
                                                                 width="16px"> {{ $pair["currency"] }}</td>
                                                        <td id="lastprice">{{ $pair["lastprice"]  }}</td>
                                                        <td id="volume" class="text-right">{{ $pair["volume"]  }}</td>
                                                        <td id="change" class="text-right {{ $changeClass }}">{{ $pair["change"]  }}%
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class=front-area>
                    <div class=content-container>
                        <div class="exnce_chart">


                        </div>
                        <div class=projects>


                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>



                                    </div>
                                </div>
                            </div>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                    </div>
                                </div>
                            </div>

                            <div class=skills>
                                <div class=skills-overlay>

                                </div>
                            </div>




                            <div class=projects-wrapper>
                                asdasdad
                            </div>
                            <div class=go-back-indicator><span class="back fas fa-long-arrow-alt-left"></span></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>


@endsection
