@extends('layouts.personal')
@php
    $activeCoinID = session("activeCoinID");
    $activeCurrencyID = session("activeCurrencyID");

    if($activeCoinID && $activeCurrencyID) {
        $statsData = getHeaderHomeStatistics($activeCoinID, $activeCurrencyID);
    } else {
        $statsData = getHeaderHomeStatistics();
    }

    if($statsData["change_ratio"] > 0) {
        $szChangeColor = "text-success";
    } elseif($statsData["change_ratio"] < 0) {
        $szChangeColor = "text-danger";
    } else {
        $szChangeColor = "";
    }
    $symbol = "{$activeCurrency->symbol}-{$activeCoin->symbol}";
    $selectedPair = "{$activeCurrency->symbol}/{$activeCoin->symbol}";
@endphp
@section("title",_i($selectedPair))
@section('content')

    <div class="container-fluid">
        <div class="row">



            <div class=exnce_pairs>
                <div class="logo fa fa-angle-right"><span class=blink>_</span></div>
                <div class=handle>
                    <div class="v-bar-a v-bar"></div>
                    <div class="v-bar-b v-bar"></div>
                </div>
            </div>
            <div class=outer-most>
                <div class=side-panel>
                    <div class=side-panel-overlay>
                        <div class=about-me>


                            <div class="card rounded border-0  p-0">
                                <div class="card-body pt-0 pb-0 px-0">
                                    <div class="market-section">
                                        <div class="currency-tabs">
                                            @foreach ($baseCoins as $coin_id => $baseCoin)
                                                <div class="tab-item {{ ( $activeCoin->id == $coin_id ? "active" : "" ) }}"
                                                     data-coinid="{{ $coin_id }}">
                                                    <span class="tab-item-text">{{ $baseCoin }}</span>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div>
                                            <table class="table currency-table fixed-table ">
                                                <thead class="table-head text-center">
                                                <tr>
                                                    <th class="border-0 text-left"><span class="fav-btn filter"></span></th>
                                                    <th class="sorting border-0 text-left pl-1">{{ _i("Pair") }}</th>
                                                    <th class="border-0 text-left pl-1">{{ _i("Price") }}
                                                    </th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Volume") }}</th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Change") }}</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>

                                        <div class="table-wrapper for-coinsTable">
                                            <table class="table table-hover currency-table mb-5">
                                                <tbody class="coinsTable">
                                                @foreach ($pairList as $pair)
                                                    @php
                                                        $strPair = $pair["currency"] . "-" . $pair["coin"];
                                                        $activeClass = $pair["coin_id"] == $activeCoin->id && $pair["currency_id"] == $activeCurrency->id ? "active" : "";

                                                        if($pair["change"] > 0) {
                                                            $changeClass = "text-success";
                                                        } elseif($pair["change"] < 0) {
                                                            $changeClass = "text-danger";
                                                        } else {
                                                            $changeClass = "";
                                                        }

                                                    @endphp
                                                    <tr class="pair" data-coinid="{{ $pair["coin_id"] }}"
                                                        data-currency="{{ $pair["currency_id"] }}"
                                                        data-pair="{{ $strPair }}"
                                                        style="{{ ( $activeCoin->id != $pair["coin_id"] ? "display:none;" : "" ) }}">

                                                        <td class="{{ $activeClass }}"><span
                                                                    class="fav-btn {{ in_array($strPair, $favorites) ? "checked" : "" }}"></span>
                                                        </td>
                                                        <td>{{ $pair["currency"] }}</td>
                                                        <td id="lastprice">{{ $pair["lastprice"]  }}</td>
                                                        <td id="volume" class="text-right">{{ $pair["volume"]  }}</td>
                                                        <td id="change" class="text-right {{ $changeClass }}">{{ $pair["change"]  }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>




                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class=front-area>
                    <div class=content-container>
                        <div class="exnce_chart">
                            <div class="row">


                                <div class="col-12">
                                    <ul class="nav nav-tabs pageination-table-title w-100" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active border-top-0 border-left-0 border-right-0 bg-transparent"
                                               data-toggle="tab" href="#tabCharts" role="tab" aria-controls="tabCharts"
                                               aria-selected="true">{{ _i("Chart") }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                               data-toggle="tab" href="#tabDepth" role="tab" aria-controls="tabDepth"
                                               aria-selected="true">{{ _i("Depth") }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                               id="islem-tab"
                                               data-toggle="tab" href="#tabOrders" role="tab"
                                               aria-selected="false">{{ _i("Orders") }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                               id="islem-tab"
                                               data-toggle="tab" href="#tabInfo" role="tab"
                                               aria-selected="false">{{ _i("Info") }}</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="tabCharts" role="tabpanel">
                                            <div id="chart-section">
                                                @include("personal.market.chart.chart")
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabDepth" role="tabpanel">
                                            <div id="chart-section">
                                                @include("personal.market.chart.depth")
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabOrders" role="tabpanel">


                                            <div class="row">
                                                <div class="col-12">
                                                    <h6 class="card-title text-left" id="orderBookTitle">{{_i("Open Orders")}}

                                                        <span class="float-right">

                                                        <ul class="orderbook-list-short">
                                                            <li class="">
                                                                <button type="button" data-object="lstOpenOrders" class="btnExpandTable btn btn-sm btn-outline-light text-warning">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                </button>
                                                            </li>
                                                        </ul>

                                                    </span>
                                                    </h6>
                                                    <table id="lstOpenOrders" style="100%;"
                                                           class="table table-striped noborderradius nowrap display table-hover ">
                                                        <thead class="table-head font-weight-bold border-0">
                                                        <tr>

                                                            <th class="border-top-0">{{ _i("Side") }}</th>
                                                            <th class="border-top-0">{{ _i("Pair") }}</th>
                                                            <th class="border-top-0">{{ _i("Date") }}</th>
                                                            <th class="border-top-0">{{ _i("Price") }}</th>
                                                            <th class="border-top-0">{{ _i("Amount") }}</th>
                                                            <th class="border-top-0">{{ _i("Total") }}</th>
                                                            <th class="border-top-0">{{ _i("Filled") }}</th>
                                                            <th class="border-top-0">{{ _i("Cancel") }}</th>
                                                            <th class="border-top-0"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>


                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="col-12">
                                                    <h6 class="card-title text-left" id="orderBookTitle">{{_i("Order History")}}

                                                        <span class="float-right">

                                                        <ul class="orderbook-list-short">
                                                            <li>
                                                                <button type="button" data-object="lstOrderHistory" class="btnExpandTable btn btn-sm btn-outline-light text-warning">
                                                                    <i class="fa fa-angle-double-down"></i>
                                                                </button>
                                                            </li>
                                                        </ul>

                                                    </span>
                                                    </h6>
                                                    <table id="lstOrderHistory" style="100%;"
                                                           class="table table-striped noborderradius nowrap display table-hover">
                                                        <thead class="table-head text-center font-weight-bold border-0">
                                                        <tr role="row">

                                                            <th class="border-top-0">{{ _i("Side") }}</th>
                                                            <th class="border-top-0">{{ _i("Pair") }}</th>
                                                            <th class="border-top-0">{{ _i("Date") }}</th>
                                                            <th class="border-top-0">{{ _i("Price") }}</th>
                                                            <th class="border-top-0">{{ _i("Amount") }}</th>
                                                            <th class="border-top-0">{{ _i("Total") }}</th>
                                                            <th class="border-top-0">{{ _i("Commission") }}</th>
                                                            <th class="border-top-0"></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        </tbody>
                                                    </table>
                                                </div>



                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabInfo" role="tabpanel">
                                            @include("personal.market.chart.info")
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=projects>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                        <div class="card rounded border-0  p-0">
                                            <div class="card-body pt-0 pb-0 px-0">
                                                <h6 class="card-title text-left" id="orderBookTitle">{{_i("Orderbook")}}
                                                    <span class="float-right">

                                                        <ul class="orderbook-list-short">
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="0" alt="orderbook-buysell"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAARCAYAAADdRIy+AAABF0lEQVQ4jaXUoUvDQRjG8c+m4MAgGoXhQBAMBpt/gMUoBq2KYBNhIIpZEcE8ZMjqgibLqqJZ25JgEDE5FsSB6Aze4NhUfvv5wHHPexzfe7h7ucxToSjSeZiXpNRgV/2WFvQTcBgLkc+j/h/gMq4jP44S9tIC17GPTIDksHmxO9VAux/gS/CvqAVfwgBuQp3pBzgZ/Ds+gq9gFJ+hTpwws1XtXdzeKU6gcHR4fDndPEnK+h0YKwATJ8yGzW3MRus1nMUHJx1ZjOEAaxGgieeobicdWTRQxorvVhnCPObSJOz04QNusRhOusMM7usjG39dWY/ixq5gNfgyrkLivhS/cg6PIWEerX5h3QlbqEY+lbq/r9O0oI6+AFb5Q71drBHQAAAAAElFTkSuQmCC"
                                                                       selected="">
                                                            </li>
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="1" alt="orderbook-buy"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAPCAYAAADkmO9VAAAAyklEQVQ4jcXSMUoDQRTG8d8sAdMJ6XOAFBZ2HiAHsLROY5tcInfYDWIbwQOk9gKxsxIsLOxCuu02RXbDsGiYLAt+8OC9Yfje/72ZkOd5pUcNEPo0DFV1HrAoCkieYpDauG/DdMLFy/nmEzn/SZjVlyvcRucbvEZ1SI0MIywxiwz2+GkRJkWGHVZ4wBBXmOKuC2Gzwy9scV/X77jB58f145/7+k3xozxHY6/wVhNfpDBfn/Ihvh13MUZ5qVmbsMQ6yjup/Q+fuho1OgDrcC9Xcb+0AgAAAABJRU5ErkJggg==">
                                                            </li>
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="2" alt="orderbook-sell"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAANCAYAAACpUE5eAAAAsUlEQVQ4jaWQMQoCMRBFX9BCsLIVhAXBu1hbeAFrGyux9gwLAfEEVjYeQM9gvY2lW4mFaGxGCEuUmfgh5P8Q3nzGXYoFkXZyT8hUu5HvuaAUsAuMIz8Azv8Ap8Ax8n2gBFa5wBmwBpxAOsB8vxzVQLAAr+JvwEF8CbSAk2RnAQ7FP4Cn+C3QA16STQ3rxPsGKKJsaphSJecjdUMXwu+/3nstC/jesCn9DpUNTDtUT9foDaE9InSwV1aPAAAAAElFTkSuQmCC">
                                                            </li>
                                                        </ul>

                                                    </span>
                                                </h6>

                                                <table class="table currency-table orderbook mb-0">
                                                    <thead class="table-head text-center">
                                                    <tr>
                                                        <th class="border-0 text-left"><span
                                                                    class="d-block">{{ _i("Price") }}
                                                                ({{$activeCoin->symbol}})</span></th>
                                                        <th class="border-0 text-right pr-1"><span
                                                                    class="d-block">{{ _i("Amount") }}
                                                                ({{$activeCurrency->symbol}})</span></th>
                                                        <th class="border-0 text-right"><span
                                                                    class="d-block">{{ _i("Total") }}
                                                                ({{ $activeCoin->symbol }})</span></th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                                <div class="table-wrapper orderbook-panels">
                                                    <table class="table table-hover currency-table orderbook sell">
                                                        <tbody class="orderbookSellTable">
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="lastPrice"><span class="price">0.00</span> <i
                                                            class="fa fa-arrow-up arrow"></i>
                                                </div>

                                                <div class="table-wrapper orderbook-panels">
                                                    <table class="table table-hover currency-table orderbook buy">
                                                        <tbody class="orderbookBuyTable">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                        <div class="card rounded border-0  p-0">
                                            <div class="card-body pt-0 pb-0 px-0">

                                                <div class="row">
                                                    <div class="col-12">
                                                        <ul class="nav nav-tabs pageination-table-title w-100" id="myTab" role="tablist">
                                                            <li class="nav-item">
                                                                <a class="nav-link active border-top-0 border-left-0 border-right-0 bg-transparent"
                                                                   data-toggle="tab" href="#tabTradeBuy" role="tab"
                                                                   aria-selected="true">{{ _i("Buy") }}</a>
                                                            </li>
                                                            <li class="nav-item">
                                                                <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                                                   data-toggle="tab" href="#tabTradeSell" role="tab"
                                                                   aria-selected="true">{{ _i("Sell") }}</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>


                                                <div class="tab-content" id="myTabContent">
                                                    <div class="tab-pane fade show active" id="tabTradeBuy" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div id="orderBookTitle" class="text-left card-title text-left mb-3">
                                                                    <small>{{_i("Balance: ")}}</small>
                                                                    <span id="coinBalance">0.00</span>
                                                                </div>

                                                                <div class="market-buy  pl-2 pt-1 pr-2">
                                                                    <form id="frmMarketBuy" name="frmMarketBuy" method="post" action="#">
                                                                        <div class="form-group hidden">
                                                                            <small class="font-weight-bold">Currency:</small>
                                                                            <input type="text" min="0" id="edtBuyCurrency" name="edtBuyCurrency"
                                                                                   class="form-control" value="{{$activeCoin->symbol}}">
                                                                            <small class="font-weight-bold">Coin:</small>
                                                                            <input type="text" min="0" id="edtBuyCoin" name="edtBuyCoin"
                                                                                   class="form-control" value="{{$activeCurrency->symbol}}">
                                                                        </div>



                                                                        <div class="row">
                                                                            <div class="col-12">

                                                                                <div class="input-group mb-3">
                                                                                    <input type="text" id="edtPriceBuy" name="edtPriceBuy"
                                                                                           class="form-control text-right currency-input border-right-0">

                                                                                    <div class="input-group-addon currency-addon text-white buySymbol border-0">
                                                                                        <div class="mt-1 small-text">{{$activeCoin->symbol}}</div>
                                                                                    </div>

                                                                                    <label class="innerLabel">{{ _i("Price") }}</label>
                                                                                </div>

                                                                                <div class="input-group mb-2">
                                                                                    <input type="text" min="0" id="edtAmountBuyInt" name="edtAmountBuyInt"
                                                                                           class="text-right form-control currency-input border-right-0">
                                                                                    <button tabindex="-1" type="button" id="btnIncrementBuyAmount"
                                                                                            name="btnIncrementBuyAmount"
                                                                                            class="pointer border border-left-0 border-right-1 text-dark-bluish">
                                                                                        <i class="fa fa-plus-circle text-dark-bluish" aria-hidden="true"></i>
                                                                                    </button>
                                                                                    <div class="input-group-addon currency-addon text-white buySymbol border-0">
                                                                                        <div class="marketSellCoinSymbol mt-1 small-text">{{ $activeCurrency->symbol }}</div>
                                                                                    </div>
                                                                                    <label class="innerLabel">{{ _i("Amount") }}</label>
                                                                                </div>

                                                                                <div class="input-group mb-3">
                                                                                    <div class="ratio-content">
                                                                                        <div class="ratio-item" data-val=".25">25%</div>
                                                                                        <div class="ratio-item" data-val=".50">50%</div>
                                                                                        <div class="ratio-item" data-val=".75">75%</div>
                                                                                        <div class="ratio-item" data-val="1">100%</div>
                                                                                        <div class="clear"></div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>

                                                                        </div>

                                                                        <div class="input-group">
                                                                            <input type="text" id="edtSummaryBuy" name="edtSummaryBuy"
                                                                                   class="form-control text-right currency-input border-right-0">
                                                                            <div class="input-group-addon currency-addon text-white buySymbol border-0">
                                                                                <div class="mt-1 small-text">{{$activeCoin->symbol}}</div>
                                                                            </div>
                                                                            <label class="innerLabel">{{ _i("Total") }}</label>
                                                                        </div>
                                                                        <div class="hidden">
                                                                            <div class="text-dark-bluish text-left mt-2 ml-3">
                                                                                <small class="font-weight-bold">{{_i("Commission")}}</small>
                                                                            </div>
                                                                            <div class="input-group mb-3">
                                                                                <input tabindex="-1" type="text" id="edtFeeBuy" name="edtFeeBuy"
                                                                                       class="ml-3 form-control text-right currency-input border-right-0"
                                                                                       readonly="true" aria-label="{{_i("Commission")}}"
                                                                                       value="0.00"/>
                                                                                <div class="input-group-addon currency-addon text-white buySymbol border-0">
                                                                                    <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </form>
                                                                    <div class="form-group">

                                                                        <div class="text-right">
                                                                            <small>{{_i("Commission (%s%s): ", "%", ($activeCurrency->processing_fee*100))}}</small>
                                                                            <small id="divFeeBuy">-</small>
                                                                            <small id="feeCurrencyLabel">{{ $activeCurrency->symbol }}</small>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group">

                                                                        <button type="button" id="btnMarketBuy" name="btnMarketBuy"
                                                                                class="marketBtnBuyNow btn btn-block bg-gradient-green font-weight-bold text-white">
                                                                            {{ _i("BUY") }}
                                                                        </button>

                                                                    </div>

                                                                </div>
                                                            </div>


                                                            <input type="hidden" id="coinFee" value="{{ floatval($activeCoin->processing_fee) }}">
                                                            <input type="hidden" id="currencyFee"
                                                                   value="{{ floatval($activeCurrency->processing_fee) }}">

                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="tabTradeSell" role="tabpanel">
                                                        <div class="row">
                                                            <div class="col-12">

                                                                <div id="orderBookTitle" class="text-left card-title text-left mb-3">
                                                                    <small>{{_i("Balance: ")}}</small>
                                                                    <span id="currencyBalance">0.00</span>
                                                                </div>

                                                                <div class="market-sell pl-2 pt-1 pr-2">

                                                                    <form id="frmMarketSell" name="frmMarketSell" method="post" action="#">

                                                                        <div class="form-group hidden">
                                                                            <input type="hidden" id="edtSellCurrency" name="edtSellCurrency"
                                                                                   value="{{$activeCoin->symbol}}">
                                                                            <input type="hidden" id="edtSellCoin" name="edtSellCoin"
                                                                                   value="{{$activeCurrency->symbol}}">
                                                                        </div>



                                                                        <div class="row">
                                                                            <div class="col-12">

                                                                                <div class="input-group mb-3">
                                                                                    <input type="text" id="edtPriceSell" name="edtPriceSell"
                                                                                           class="form-control text-right currency-input border-right-0">
                                                                                    <div class="input-group-addon currency-addon text-white sellSymbol border-0">
                                                                                        <div class="mt-1 small-text">{{$activeCoin->symbol}}</div>
                                                                                    </div>
                                                                                    <label class="innerLabel">{{ _i("Price") }}</label>
                                                                                </div>

                                                                                <div class="input-group mb-2">
                                                                                    <input type="text" min="0" id="edtAmountSellInt" name="edtAmountSellInt"
                                                                                           class="text-right form-control currency-input border-right-0">
                                                                                    <button type="button" tabindex="-1" id="btnIncrementSellAmount"
                                                                                            name="btnIncrementSellAmount"
                                                                                            class="pointer border border-left-0 border-right-1 text-dark-bluish">
                                                                                        <i class="fa fa-plus-circle text-dark-bluish" aria-hidden="true"></i>
                                                                                    </button>
                                                                                    <div class="input-group-addon currency-addon text-white sellSymbol border-0">
                                                                                        <div class="marketSellCoinSymbol mt-1 small-text">{{ $activeCurrency->symbol }}</div>
                                                                                    </div>
                                                                                    <label class="innerLabel">{{ _i("Amount") }}</label>
                                                                                </div>

                                                                                <div class="input-group mb-3">
                                                                                    <div class="ratio-content">
                                                                                        <div class="ratio-item" data-val=".25">25%</div>
                                                                                        <div class="ratio-item" data-val=".50">50%</div>
                                                                                        <div class="ratio-item" data-val=".75">75%</div>
                                                                                        <div class="ratio-item" data-val="1">100%</div>
                                                                                        <div class="clear"></div>
                                                                                    </div>
                                                                                </div>

                                                                            </div>



                                                                        </div>

                                                                        <div class="input-group">
                                                                            <input type="text" id="edtSummarySell" name="edtSummarySell"
                                                                                   class="form-control text-right currency-input border-right-0">
                                                                            <div class="input-group-addon currency-addon text-white sellSymbol border-0">
                                                                                <div class="mt-1 small-text">{{$activeCoin->symbol}}</div>
                                                                            </div>
                                                                            <label class="innerLabel">{{ _i("Total") }}</label>
                                                                        </div>

                                                                        <div class="hidden">
                                                                            <div class="text-dark-bluish text-left mt-2 ml-3">
                                                                                <small class="font-weight-bold">{{_i("Commission")}}</small>
                                                                            </div>
                                                                            <div class="input-group mb-2">
                                                                                <input tabindex="-1" type="text" id="edtFeeSell" name="edtFeeSell"
                                                                                       class="form-control text-right currency-input border-right-0"
                                                                                       readonly="true" aria-label="{{_i("Commission")}}"
                                                                                       value="0.00"/>
                                                                                <div class="input-group-addon currency-addon text-white sellSymbol border-0">
                                                                                    <div class="mt-1 small-text">{{$activeCoin->symbol}}</div>
                                                                                </div>
                                                                            </div>

                                                                        </div>

                                                                    </form>
                                                                    <div class="form-group">


                                                                        <div class="text-right">
                                                                            <small>{{_i("Commission (%s%s): ", "%", ($activeCoin->processing_fee*100))}}</small>
                                                                            <small id="divFeeSell">-</small>
                                                                            <small id="feeCurrencyLabel">{{ $activeCoin->symbol }}</small>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <button type="button" id="btnMarketSell" name="btnMarketSell"
                                                                                class="marketBtnSellNow btn btn-block bg-gradient-red font-weight-bold text-white">
                                                                            {{ _i("SELL") }}
                                                                        </button>
                                                                    </div>


                                                                </div>
                                                            </div>
                                                            <input type="hidden" id="coinFee" value="{{ floatval($activeCoin->processing_fee) }}">
                                                            <input type="hidden" id="currencyFee"
                                                                   value="{{ floatval($activeCurrency->processing_fee) }}">

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>









                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                        <div class="card rounded border-0  d-sm-block">
                                            <div class="card-body pt-0 px-0 pb-0">
                                                <h6 class="card-title text-left" id="orderHistoryTitle">{{_i("Price Information")}}</h6>
                                                <div class="row">
                                                    <div id="chart-coin-info" class="col-12">
                                                        <h2 class="coin-chart-head">
                                                            <img src="{{ asset("images/coins/{$statsData["currency"]->slug}.svg") }}"/>
                                                            <div>
                                                                <strong>{{$statsData["currency"]->symbol}}/{{$statsData["coin"]->symbol}}</strong>
                                                                <em lang="en">{{$statsData["currency"]->name}} / {{$statsData["coin"]->name}}</em>
                                                            </div>
                                                        </h2>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="row pl-2">
                                                            <div class="col-6">
                                                                <label>{{$statsData["currency"]->symbol}} {{ _i("Current Price") }}</label>
                                                                <div class="input-group-prepend">
                                                                    <span id="price24h-info">{{$statsData["price"]}}</span>&nbsp;&nbsp
                                                                    <small id="price24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Change") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="change24h-info">{{ $statsData["change_ratio"] }}</span>&nbsp
                                                                        <small id="change24h-symbol">%</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Highest Price") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="priceMax24h-info">{{$statsData["maximum_price"]}}</span>&nbsp
                                                                        <small id="priceMax24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Lowest Price") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="priceMin24h-info">{{$statsData["minimum_price"]}}</span>&nbsp
                                                                        <small id="priceMin24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="card rounded border-0  p-0">
                                            <div class="card-body pt-0 pb-0 px-0">
                                                <h6 class="card-title text-left" id="orderHistoryTitle">{{_i("Trade History")}}</h6>

                                                <table class="table currency-table">
                                                    <thead class="table-head text-center">
                                                    <tr>
                                                        <th class="border-0 text-left">{{ _i("Time") }}</th>
                                                        <th class="border-0  text-left pl-1"><span
                                                                    class="d-block">{{ _i("Amount") }} ({{$activeCurrency->symbol}})</span></th>
                                                        <th class="border-0 text-right px-0 pr-1"><span
                                                                    class="d-block">{{ _i("Price") }} ({{$activeCoin->symbol}})</span></th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                                <div class="table-wrapper for-marketOrderHistory">
                                                    <table class="table table-hover currency-table">
                                                        <tbody id="marketOrderHistory" class="marketOrderHistory">

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </div>




                            <div class=projects-wrapper>

                            </div>
                            <div class=go-back-indicator><span class="back fas fa-long-arrow-alt-left"></span></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
