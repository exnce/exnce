@extends('layouts.personal')
@php
    $activeCoinID = session("activeCoinID");
    $activeCurrencyID = session("activeCurrencyID");

    if($activeCoinID && $activeCurrencyID) {
        $statsData = getHeaderHomeStatistics($activeCoinID, $activeCurrencyID);
    } else {
        $statsData = getHeaderHomeStatistics();
    }

    if($statsData["change_ratio"] > 0) {
        $szChangeColor = "text-success";
    } elseif($statsData["change_ratio"] < 0) {
        $szChangeColor = "text-danger";
    } else {
        $szChangeColor = "";
    }
    $symbol = "{$activeCurrency->symbol}-{$activeCoin->symbol}";
    $selectedPair = "{$activeCurrency->symbol}/{$activeCoin->symbol}";
@endphp
@section("title",_i($selectedPair))
@section('content')

    <div class="container-fluid py-2">


        <div class="col-lg-12 col-md-12 col-sm-12 mb-3  order-3">

            <div class="card rounded box-shadow p-0">
                <div class="card-body pt-0 pb-0 px-0">
                    <div class="currency-tabs">
                        @foreach ($baseCoins as $coin_id => $baseCoin)
                            <div class="tab-item {{ ( $activeCoin->id == $coin_id ? "active" : "" ) }}"
                                 data-coinid="{{ $coin_id }}">
                                <span class="tab-item-text">{{ $baseCoin }}</span>
                            </div>
                        @endforeach
                    </div>
                    <div class="table-responsive p-0 scrollableTable">

                        <table class="table currency-table mb-2 p-0" style="white-space: nowrap;">
                            <thead class="table-head text-center">
                            <tr>
                                <th class="border-0 text-left favcolumn"><span class="fav-btn filter"></span></th>
                                <th class="sorting border-0 text-left pl-1 headcolumn">{{ _i("Pair") }}</th>
                                <th class="border-0 text-right pl-1">{{ _i("Price") }} </th>
                                <th class="border-0 text-right pr-1">{{ _i("24h Volume") }}</th>
                                <th class="border-0 text-right pr-1">{{ _i("24h Change") }}</th>
                            </tr>
                            </thead>

                            <tbody class="coinsTable">
                            @foreach ($pairList as $pair)

                                @php
                                    $strPair = $pair["currency"] . "-" . $pair["coin"];
                                    $activeClass = $pair["coin_id"] == $activeCoin->id && $pair["currency_id"] == $activeCurrency->id ? "active" : "";

                                    if($pair["change"] > 0) {
                                        $changeClass = "text-success";
                                    } elseif($pair["change"] < 0) {
                                        $changeClass = "text-danger";
                                    } else {
                                        $changeClass = "";
                                    }

                                @endphp

                                <tr class="pair home-pair" data-coinid="{{ $pair["coin_id"] }}"
                                    data-currency="{{ $pair["currency_id"] }}"
                                    data-pair="{{ $strPair }}"
                                    style="{{ ( $activeCoin->id != $pair["coin_id"] ? "display:none;" : "" ) }}">

                                    <td class="vertical-middle favcolumn {{ $activeClass }}"><span
                                                class="fav-btn {{ in_array($strPair, $favorites) ? "checked" : "" }}"></span>
                                    </td>
                                    <td class="headcolumn">
                                        <div class="kt-coin-card-v2">
                                            <div class="kt-coin-card-v2__pic">
                                                <img src="{{asset("/images/coins/".strtolower($pair["slug"]).".svg")}}">
                                            </div>
                                            <div class="kt-coin-card-v2__details">
                                                <a class="kt-coin-card-v2__name" href="#">
                                                    <b>{{ $pair["currency"] }}</b>
                                                </a> <span class="kt-coin-card-v2__email">{{$pair["coin_name"]}}</span>
                                            </div>
                                        </div>


                                    </td>
                                    <td class="text-right vertical-middle" id="lastprice">{{ $pair["lastprice"]  }}</td>
                                    <td id="volume" class="text-right vertical-middle">{{ $pair["volume"]  }}</td>
                                    <td id="change" class="text-right vertical-middle {{ $changeClass }}">{{ $pair["change"]  }}
                                    </td>
                                </tr>

                            @endforeach
                            </tbody>

                        </table>
                    </div>

                </div>
            </div>


        </div>
    </div>

    </div>




@endsection

