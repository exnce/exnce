@php
$detail=$activeCurrency->detail()->first();

$whitePaper = "";
$facebook = "";
$youtube = "";
$website = "";
$medium = "";
$github = "";
$reddit = "";
$email = "";
$description = "";
$twitter="";
$telegram = "";
$coingecko = "";
$coinmarketcap = "";
if($detail){
    $whitePaper = $detail->whitepaper;
    $website = $detail->website;
    $facebook = $detail->facebook;
    $telegram = $detail->telegram;
    $github = $detail->github;
    $reddit = $detail->reddit;
    $email = $detail->email;
    $medium = $detail->medium;
    $youtube = $detail->youtube;
    $description = $detail->description;
    $twitter = $detail->twitter;
    $coinmarketcap = $detail->coinmarketcap;
    $coingecko = $detail->coingecko;

}
@endphp
<div class="row">
    <div class="col-12">
        <h6 class="card-title text-left"  id="orderBookTitle">{{_i("Coin Details")}}
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li class="">
                        <button type="button" data-object="lstProjectDetails"
                                class="btnExpandTable btn btn-sm btn-outline-light text-warning">
                            <i class="fa fa-angle-double-down"></i>
                        </button>
                    </li>
                </ul>
            </span>
        </h6>
        <div id="lstProjectDetails_wrapper" class="w-100">


        <table id="lstProjectDetails"
               class="table table-striped noborderradius nowrap display">
            <thead class="table-head font-weight-bold border-0 w-100">

            <tr>
                <th>Key</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fas fa-tags"></i> Name</td>
                <td>{{$activeCurrency->name}}</td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fas fa-flag"></i> Symbol</td>
                <td>{{$activeCurrency->symbol}}</td>
            </tr>

            <tr>
                <td class="font-weight-bold"><img width="16px" height="16px" src="{{asset("/images/cmc.svg")}}"/> Coinmarketcap</td>
                <td><a target="_blank" href="{{$coinmarketcap}}">{{$coinmarketcap}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><img width="16px" height="16px" src="{{asset("/images/coingecko.svg")}}"/> Coingecko</td>
                <td><a target="_blank" href="{{$coingecko}}">{{$coingecko}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning far fa-file-pdf"></i> Whitepaper</td>
                <td><a target="_blank" href="{{$whitePaper}}">{{$whitePaper}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-telegram-plane"></i> Telgram</td>
                <td><a target="_blank" href="">{{$telegram}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-facebook"></i> Facebook</td>
                <td><a target="_blank" href="{{$facebook}}">{{$facebook}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-youtube"></i> Youtube</td>
                <td><a target="_blank" href="{{$youtube}}">{{$youtube}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-twitter"></i> Twitter</td>
                <td><a target="_blank" href="{{$twitter}}">{{$twitter}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-reddit-alien"></i> Reddit</td>
                <td><a target="_blank" href="{{$reddit}}">{{$reddit}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-github"></i> Github</td>
                <td><a target="_blank" href="{{$github}}">{{$github}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fab fa-medium"></i> Medium</td>
                <td><a target="_blank" href="{{$medium}}">{{$medium}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fas fa-globe-europe"></i> Website</td>
                <td><a target="_blank" href="{{$website}}">{{$website}}</a></td>
            </tr>
            <tr>
                <td class="font-weight-bold"><i class="text-warning fas fa-envelope-open-text"></i> Email</td>
                <td><a target="_blank" href="{{$email}}">{{$email}}</a></td>
            </tr>
            </tbody>
        </table>

        </div>
    </div>
    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">{{_i("Coin Description")}}
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectDescription"
                                class="btnExpandTable btn btn-sm btn-outline-light text-warning">
                            <i class="fa fa-angle-double-down"></i>
                        </button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectDescription_wrapper">
            <div class="row">
                <div class="col-12">
                    <p class="text-justify small-text p-3">
                        {{$description}}
                    </p>
                </div>
            </div>

        </div>

    </div>


</div>
