@extends('layouts.personal')
@section("title",_i("API Access"))


@section("content")
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row mx-auto">
                <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                    @include('layouts.account-sidebar')
                </div>

                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mt-3 mt-md-0 p-0">

                    <div class="pb-4" id="accordion">
                        <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <h5 class="mb-0">{{_i("API Keys")}}<span class="pull-right"><i
                                                    class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{_i("Creating an API private key provides access to markets and real-time trading services via a third-party site or application")}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body p-0 py-3">

                                    <table id="apiList" name="apiList"
                                           class="table display table-striped noborderradius nowrap wallet-table" style="100%; margin-top: 10px !important;" cellspacing="0">
                                        <thead class="table-head text-left">
                                        <tr>
                                            <td class="all">{{_i("Secret Key")}}</td>
                                            <td class="all">{{_i("Action")}}</td>
                                            @foreach(getAPIKeyAccessPermissions() as $permission)
                                                <td class="none">{{$permission["title"]}}</td>
                                            @endforeach
                                            <td></td>
                                        </tr>

                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>
                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="container-fluid py-3 clearfix">
                        <a title="Yeni API Erişim Anahtarı Oluştur" class="btnAddNewApiKey float-action-button pull-right border-0" data-toggle="modal" data-target="#frmAddNewAPIKey">+
                        </a>
                    </div>
                </div>

            </div>



            <div class="modal fade" id="frmAddNewAPIKey" tabindex="-1" role="dialog" aria-labelledby="frmAddNewAPIKey"
                 aria-hidden="true">
                <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 id="apiEntryTitle" class="modal-title">{{ _i("Yeni API Erişim Anahtarı Oluştur") }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true" class="text-white">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form id="frmApiKey" name="frmApiKey">
                                <div class="form-group hidden">
                                    <label class="text-dark-bluish">{{_i("Action Type : ")}}</label>
                                    <input name="edtActionType" id="edtActionType" type="text"
                                              class="edtActionType form-control bg-white mb-2"
                                              value="save" readonly="readonly">
                                </div>
                                <div class="form-group">
                                    <label class="text-dark-bluish">{{_i("Public Key : ")}}</label>
                                    <textarea name="edtPublicKey" id="edtPublicKey" type="text" rows="4"
                                           class="edtPublicKey form-control bg-white mb-2"
                                              placeholder="" data-allow_clear="1" readonly="readonly"></textarea>
                                </div>

                                <div class="form-group">
                                    <label class="text-dark-bluish">{{_i("Secret Key : ")}}</label>
                                    <input id="edtSecretKey" name="edtSecretKey" type="text"
                                           class="edtSecretKey form-control bg-white mb-2"
                                              placeholder="" data-allow_clear="1" readonly="readonly"/>
                                </div>

                                <div class="form-group">
                                    <div class="list-group edtPermissionList">
                                        @foreach($api_permissions as $permission)

                                        <a class="list-group-item">
                                            {{$permission["title"]}}
                                            <div class="material-switch pull-right">
                                                <input class="api-permission" data-permission="{{$permission["key"]}}" id="{{$permission["key"]}}" name="{{$permission["key"]}}" type="checkbox"/>
                                                <label for="{{$permission["key"]}}" class="btn-success"></label>
                                            </div>
                                        </a>
                                        @endforeach
                                    </div>
                                </div>
                            </form>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Close") }}</button>
                            <button type="button" class="btnCreateApiKeyEntry btn btn-gradient-blue">{{ _i("Create") }}</button>
                            <button type="button" class="btnSaveApiKeyEntry btn btn-success">{{ _i("Save") }}</button>
                            <button type="button" class="btnDeleteApiKeyEntry btn btn-danger">{{ _i("Delete") }}</button>
                            <button type="button" class="btnUpdateApiKeyEntry btn btn-info">{{ _i("Update") }}</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
