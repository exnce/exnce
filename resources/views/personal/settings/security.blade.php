@extends('layouts.personal')
@section("title",_i("Security Preferences"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">
            <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                @include('layouts.account-sidebar')
            </div>


            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mt-3 mt-md-0 p-0">
                <div class="row">
                    <div class="col-12 hidden">

                        <div class="pb-4">
                            <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                                <div class="card-header" id="headingProgram">
                                    <a data-toggle="collapse" data-target="#pnlFaceRecognition" aria-expanded="true"
                                       aria-controls="pnlFaceRecognition">
                                        <h5 class="mb-0">{{ _i("Yüz Tanıma Sistemi") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{_i("Yüz tanıma sistemi ile doğrulama yaparak ekstra güvenlik sağlayabilirsiniz.")}}</span>
                                    </a>
                                </div>

                                <div id="pnlFaceRecognition" class="collapse show" aria-labelledby="headingProgram">
                                    <div class="card-body p-0 py-3">

                                        <div class="form-row">


                                            <div class="col-12">



                                                    @include("personal.settings.security.face-recognition")


                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-12">

                        <div class="pb-4">
                            <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                                <div class="card-header">
                                    <a data-toggle="collapse" data-target="#pnlTwoFactorAuth" aria-expanded="true"
                                       aria-controls="pnlTwoFactorAuth">
                                        <h5 class="mb-0">{{ _i("Two-Factor Verification") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{_i("You can provide extra security by making two-step verification with Google Authenticator.")}}</span>
                                    </a>
                                </div>

                                <div id="pnlTwoFactorAuth" class="collapse show" aria-labelledby="headingProgram">
                                    <div class="card-body p-0 py-3">

                                        <div class="form-row">
                                            <div class="col-md-3 mb-3">
                                                <div class="qrcode pb-2 text-center">
                                                    @if($is_active==false)
                                                        <img class="img-thumbnail p-0"
                                                             src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->errorCorrection('H')->margin(0)->size(200)->generate( $image )) !!}">
                                                    @else
                                                        <img src="{{asset("/images/google_2fa.png")}}" width="170px"
                                                             height="170px" class="img-thumbnail">
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-9 col-sm-12 mb-3">
                                                @if($is_active==false)

                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <p class="alert alert-danger">{{_i("Google Authenticator is not enabled!")}}</p>

                                                            <p class="alert alert-info">{{_i("To enable the Google Authenticator application;")}}</p>

                                                        </div>

                                                        <div class="col-lg-12">
                                                            <p class="alert alert-success">{{_i("You can use the %s code for manual input.",$secret)}}</p>
                                                        </div>


                                                        <div class="col-12">
                                                            <div class="form-group">
                                                                <form method="post" id="frmAuthenticator">
                                                                    @csrf
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <label>{{_i("Authenticator Code")}}</label>
                                                                        </div>
                                                                        <input type="hidden" name="secret"
                                                                               value="{{$secret}}">
                                                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                            <input class="form-control" type="number"
                                                                                   name="authCode"
                                                                                   placeholder="{{_i("Authenticator Code")}}">
                                                                        </div>
                                                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                            <button type="submit"
                                                                                    class="btn btn-gradient-blue font-weight-bold form-control enableAuthenticator">{{_i("Activate")}}</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-12">
                                                            <p class="text-navy-blue">{{_i("Use the Google Authenticator app to read the QR code. Enter the code generated by the application in the field below. If you get the wrong error, read the QR code again.")}}</p>
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <p class="alert alert-success">{{_i("Google Authenticator is being used effectively.")}}</p>

                                                            <p class="alert alert-warning">{{_i("To disable the Google Authenticator app;")}}</p>

                                                        </div>
                                                        <div class="col-12">


                                                            <div class="form-group">
                                                                <form method="post" id="frmDisableAuthenticator">
                                                                    @csrf
                                                                    <div class="row">
                                                                        <div class="col-12">
                                                                            <p>{{_i("Enter the 6-digit code in Google Authenticator.")}}</p>
                                                                        </div>
                                                                        <div class="col-12">
                                                                            <label>{{_i("Authenticator kodu")}}</label>
                                                                        </div>
                                                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                            <input class="form-control"
                                                                                   type="number" name="authCode"
                                                                                   placeholder="{{_i("Authenticator kodu")}}">
                                                                        </div>
                                                                        <div class="col-lg-6 col-sm-12 col-xs-12">
                                                                            <button type="submit"
                                                                                    class="btn btn-gradient-blue font-weight-bold form-control disableAuthenticator">{{_i("Disable")}}</button>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div>


                                                        </div>

                                                        @endif


                                                    </div>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-12">

                        <div class="pb-4">
                            <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                                <div class="card-header">
                                    <a data-toggle="collapse" data-target="#pnlUpdatePassword" aria-expanded="true"
                                       aria-controls="pnlUpdatePassword">
                                        <h5 class="mb-0">{{ _i("Change Password") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                        <span class="d-none d-lg-block d-md-block">{{_i("Make sure that your registered email password and your existing access password are different.")}}</span>
                                    </a>
                                </div>

                                <div id="pnlUpdatePassword" class="collapse show">
                                    <div class="card-body p-0 py-3">

                                        <div class="form-row">
                                            <div class="col-12">

                                                <form class="form-horizontal" method="POST"
                                                      action="{{ route('change-password') }}">
                                                    @csrf

                                                    @if(session("error"))
                                                        <div class="form-group">
                                                            <p class="alert alert-danger">{{session("error")}}</p>
                                                        </div>
                                                    @elseif(session("success"))
                                                        <div class="form-group">
                                                            <p class="alert alert-success">{{session("success")}}</p>
                                                        </div>
                                                    @endif

                                                    <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                                                        <label>{{_i("Current Password")}}</label>
                                                        <input id="current-password" type="password"
                                                               class="form-control" name="current-password" required
                                                               placeholder="{{_i("Current Password")}}">

                                                        @if ($errors->has('current-password'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('current-password') }}</strong>
                                                            </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                                                        <label>{{_i("New Password")}}</label>

                                                        <input id="new-password" type="password"
                                                               class="form-control"
                                                               name="new-password" required
                                                               placeholder="{{_i("New Password")}}">

                                                        @if ($errors->has('new-password'))
                                                            <span class="help-block">
                                                                <strong>{{ $errors->first('new-password') }}</strong>
                                                             </span>
                                                        @endif
                                                    </div>

                                                    <div class="form-group">
                                                        <label>{{_i("New Password (Again)")}}</label>
                                                        <input id="new-password-confirm" type="password"
                                                               class="form-control" name="new-password_confirmation"
                                                               required
                                                               placeholder="{{_i("New Password (Again)")}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <input type="submit"
                                                               class="btn pull-right btn-gradient-blue font-weight-bold mt-1 btn-primary"
                                                               value="{{_i("Update")}}">
                                                    </div>
                                                </form>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    </div>
@endsection
