
@extends('layouts.personal')
@section("title",_i("User Account Transaction Records"))


@section("content")
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row mx-auto">
                <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                    @include('layouts.account-sidebar')
                </div>

                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mt-3 mt-md-0 p-0" >

                    <div class="pb-4" id="accordion">
                        <div class="card card-rounded box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="mb-0">{{_i("Account activities")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{_i("You can see the records of user account movements in detail.")}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body p-0 py-3">

                                    <table id="accountLogs" style="100%; margin-top: 10px !important;" cellspacing="0" role="grid" name="accountLogs" class="table table-striped table-hover">
                                        <thead class="table-head text-left font-weight-bold">
                                        <tr>

                                            <td>{{ _i("Date") }}</td>
                                            <td>{{ _i("Action") }}</td>
                                            <td>{{ _i("Asset") }}</td>
                                            <td>{{ _i("Amount") }}</td>
                                            <td>{{ _i("Fee") }}</td>
                                            <td>{{ _i("Status") }}</td>
                                            <td></td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                        <tfoot>

                                        </tfoot>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>




                </div>


            </div>

        </div>
    </section>
@endsection
