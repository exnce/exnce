
@extends('layouts.personal')

@php
    if($transactionStatus == "open") {
        $title = _i("Open Orders");
    } else {
        $title = _i("Completed Orders");
    }
@endphp

@section("title", $title)


@section("content")
    <div class="container-fluid pt-3">
        <div class="row mx-auto">

            <div class="col-xl-2 col-lg-2 pr-lg-3 col-md-3 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                @include('layouts.transactions-sidebar')
            </div>

            <div class="col-xl-10 col-lg-10 col-md-9 col-sm-12 px-0 mt-3 mt-md-0">
                <div class="pb-4" id="accordion">

                    @if($transactionStatus == "open")
                        <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <h5 class="mb-0">{{_i("Open Orders")}}<span class="pull-right"><i
                                                    class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{_i("You can see all your orders that are not yet completed in here.")}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body p-0 py-3">

                                    <table id="lstOpenOrders"
                                           class="table table-striped display no-footer table-hover" style="100%; margin-top: 10px !important;" cellspacing="0" role="grid">
                                        <thead class="table-head font-weight-bold border-0">
                                        <tr>
                                            <th class="border-top-0">{{ _i("Side") }}</th>
                                            <th class="border-top-0">{{ _i("Pair") }}</th>
                                            <th class="border-top-0">{{ _i("Date") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Price") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Amount") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Total") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Filled") }}</th>
                                            <th class="border-top-0">{{ _i("Cancel") }}</th>
                                            <th class="border-top-0"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    @elseif($transactionStatus == "completed")
                        <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <h5 class="mb-0">{{_i("Order History")}}<span class="pull-right"><i
                                                    class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{_i("You can see all your completed orders here.")}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body p-0 py-3">

                                    <table id="lstOrderHistory"
                                           class="table display table-striped noborderradius nowrap no-footer table-hover" style="100%; margin-top: 10px !important;" cellspacing="0" role="grid">
                                        <thead class="table-head text-center font-weight-bold border-0">
                                        <tr role="row">
                                            <th class="border-top-0 text-left">{{ _i("Side") }}</th>
                                            <th class="border-top-0 text-left">{{ _i("Pair") }}</th>
                                            <th class="border-top-0 text-left">{{ _i("Date") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Price") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Amount") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Total") }}</th>
                                            <th class="border-top-0 text-right">{{ _i("Fee") }}</th>
                                            <th class="border-top-0"></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    @endif
                </div>

            </div>

        </div>

    </div>
@endsection
