
@extends('layouts.personal')
@section("title",_i("Communication Preferences"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">
            <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                @include('layouts.account-sidebar')
            </div>
            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mt-3 mt-md-0">

                <div class="row">
                    <div class="col-lg-12 col-sm-12 m-auto p-0">
                        <div class="card card-rounded box-shadow pt-3 px-3 py-3 pb-3">

                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                   aria-controls="collapseOne">
                                    <h5 class="mb-0">{{_i("Communication Preferences")}}<span class="pull-right"><i
                                                    class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{_i("You can update your notification preferences for cryptocurrency deposit and withdrawals.")}}</span>
                                </a>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body p-0 py-3">
                                    <div id="divContactPreferences" class="m-auto">
                                        <form method="post" action="{{route("update-contact-preferences")}}">
                                            @csrf

                                            <table id="tblContactPreferences">
                                                <thead>
                                                <th></th>
                                                <th class="text-modern-blue">{{_i("E-Mail")}}</th>
                                                <th class="text-modern-blue">{{_i("SMS")}}</th>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>{{_i("Deposit")}}</td>
                                                    <td><input name="EmailOnCryptoDeposit"
                                                               type="checkbox" {{ $settings->crypto_deposit_email ? " checked" : "" }}>
                                                    </td>
                                                    <td><input name="SmsOnCryptoDeposit"
                                                               type="checkbox" {{ $settings->crypto_deposit_sms ? " checked" : "" }}>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>{{_i("Withdraw")}}</td>
                                                    <td><input name="EmailOnCryptoWithdraw"
                                                               type="checkbox" {{ $settings->crypto_withdraw_email ? " checked" : "" }}>
                                                    </td>
                                                    <td><input name="SmsOnCryptoWithdraw"
                                                               type="checkbox" {{ $settings->crypto_withdraw_sms ? " checked" : "" }}>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>

                                            <input type="submit" id="btnUpdateContactPreferences"
                                                   class="btn btn-block btn-gradient-blue font-weight-bold px-4 btn-primary mt-3"
                                                   value="{{_i("Save")}}">
                                            </input>
                                        </form>

                                        <div class="alert alert-warning text-center mt-3">
                                            {{_i("SMS notifications are free.")}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
