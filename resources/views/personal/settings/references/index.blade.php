@extends('layouts.personal')
@section("title",_i("Referrals"))


@section("content")
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row mx-auto">
                <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                    @include('layouts.account-sidebar')
                </div>


                <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 mt-3 mt-md-0 p-0">
                    <div class="row">
                        <div class="col-12">

                            <div class="pb-4">
                                <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                                    <div class="card-header" id="headingProgram">
                                        <a data-toggle="collapse" data-target="#refProgramBody" aria-expanded="true"
                                           aria-controls="refProgramBody">
                                            <h5 class="mb-0">{{_i("Referral Program")}}<span class="pull-right"><i
                                                            class="fa fa-angle-double-down"></i></span></h5>
                                            <span class="d-none d-lg-block d-md-block">{{_i("The information you need to participate in the referral program is below.")}}</span>
                                        </a>
                                    </div>

                                    <div id="refProgramBody" class="collapse show" aria-labelledby="headingProgram">
                                        <div class="card-body p-0 py-3">

                                            <div class="form-row">
                                                <div class="col-md-3 mb-3">
                                                    <div class="qrcode pb-2 text-center">
                                                        <img class="img-thumbnail" src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->errorCorrection('H')->margin(0)->size(160)->generate( route("register-reference",$user->ref_code))) !!} ">
                                                    </div>
                                                </div>
                                                <div class="col-lg-9 col-sm-12 mb-3">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-sm-6 col-xs-6">
                                                            <label>{{_i("Referral Code")}}</label>
                                                            <input readonly="readonly" type="text"
                                                                   class="form-control bg-white"
                                                                   value="{{$user->ref_code}}" required>
                                                        </div>
                                                        <div class="col-lg-3 col-sm-6 col-xs-6">
                                                            <label>{{_i("Commission Rate")}}</label>
                                                            <div class="input-group">
                                                                <div     class="input-group-prepend">
                                                                    <span class="input-group-text">%</span>
                                                                </div>
                                                                <input readonly="readonly" type="text"
                                                                       class="form-control bg-white" placeholder="20"
                                                                       aria-describedby="" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-6 col-sm-12">
                                                            <label>{{_i("Estimated Earnings")}}</label>
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text">XNC</span>
                                                                </div>
                                                                <input readonly="readonly" type="text"
                                                                       class="form-control bg-white"
                                                                       placeholder="0.00000000"
                                                                       aria-describedby="" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <label>{{_i("Referral URL")}}</label>

                                                            <div class="input-group position-relative">
                                                                <input class="form-control bg-white" id="ref_code"
                                                                       aria-describedby="addon-kopyala"
                                                                       spellcheck="false"

                                                                       value="{{ route("register-reference",$user->ref_code) }}"
                                                                       type="text" readonly>

                                                                <button type="button"
                                                                        class="btn btn-default btn-copy js-tooltip copy-ref_code"
                                                                        data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        title="{{ _i("Copy to Clipboard") }}">
                                                                    <i class="fa fa-clipboard clipboard-icon"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                        <div class="col-12 referal-shares mt-1 mb-3">
                                                            <ul class="mt-4">
                                                                <li>
                                                                    <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(route("register-reference",$user->ref_code))  }}"><i
                                                                                class="fab fa-2x fa-facebook"
                                                                                aria-hidden="true"></i></a></li>
                                                                <li><a target="_blank" href="https://twitter.com/home?status={{ urlencode(route("register-reference",$user->ref_code))  }}"><i class="fab fa-2x fa-twitter"
                                                                                   aria-hidden="true"></i></a></li>
                                                                <li><a target="_blank" href="https://plus.google.com/share?url={{urlencode(route("register-reference",$user->ref_code))}}"><i class="fab fa-2x fa-google-plus"
                                                                                   aria-hidden="true"></i></a></li>
                                                                <li>
                                                                    <a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={{urlencode(route("register-reference",$user->ref_code))}}"><i
                                                                                class="fab fa-2x fa-linkedin"
                                                                                aria-hidden="true"></i></a></li>
                                                                <li><a target="_blank" href="https://wa.me/?text={{urlencode(route("register-reference",$user->ref_code))}}"><i class="fab fa-2x fa-whatsapp"
                                                                                   aria-hidden="true"></i></a></li>
                                                            </ul>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="col-12">

                            <div class="pb-4" id="accordion">
                                <div class="card rounded box-shadow pt-3 px-3 py-3 pb-3">

                                    <div class="card-header" id="headingOne">
                                        <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                           aria-controls="collapseOne">
                                            <h5 class="mb-0">{{_i("Referral List")}}<span class="pull-right"><i
                                                            class="fa fa-angle-double-down"></i></span></h5>
                                            <span class="d-none d-lg-block d-md-block">{{_i("Users who have registered through your referral URL with their estimated earnings.")}}</span>
                                        </a>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                        <div class="card-body p-0 py-3">

                                            <table id="referralList" name="referralList"
                                                   class="table display table-striped nowrap"
                                                   style="100%; margin-top: 10px !important;" cellspacing="0">
                                                <thead class="table-head text-left">
                                                <tr>
                                                    <th>{{ _i("Name") }}</th>
                                                    <th>{{ _i("Email") }}</th>
                                                    <th>{{ _i("Date") }}</th>
                                                    <th>{{ _i("Commission") }}</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                                <tfoot>
                                                </tfoot>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </section>
@endsection
