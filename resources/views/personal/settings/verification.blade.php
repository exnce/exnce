@extends('layouts.personal')
@section("title",_i("Account Verification"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">
            <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                @include('layouts.account-sidebar')
            </div>
            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12 card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3 mt-3 mt-md-0 mb-3">
                @if(isset($approval) && $approval->status == 1)
                    <p class="alert alert-danger">
                        {{ _i("Your ID is not approved. Please reload your documents as requested from you and your ID and photo appear clearly.") }}
                        {{ $approval->description }}
                    </p>
                @endif

                @if(isset($approval) && $approval->status == 0)
                    <p>{{ _i("Documents confirming your identity are in the process of approval ...") }}</p>
                    <ul>
                        <li>{{ $approval->id_card_front }}</li>
                        <li>{{ $approval->selfie }}</li>
                    </ul>
                    <form method="post" action="{{ route("cancel-id-verify") }}">
                        @csrf
                        <div class="form-group">
                            <input type="submit" class="btn w-50 btn-danger font-weight-bold px-4 btn-primary"
                                   value="{{_i("Cancel")}}"/>
                        </div>
                    </form>

                @elseif(isset($approval) && $approval->status == 2)
                    <p class="alert alert-success">{{ _i("Your identity has been verified. You can now create a withdrawal requests.") }}</p>

                @else
                    <form method="post" id="frmVerifyID" enctype="multipart/form-data"
                          action="{{ route("verify-id") }}">
                        @csrf

                        <div class="row  mb-3">

                            <div class="col-lg-6 col-sm-12">
                               <div class="row">
                                <div class="col-12">
                                    <img src="{{asset("/images/identity-card.png")}}"
                                         class="img-thumbnail img-responsive noborder box-shadow">

                                </div>

                                <div class="col-12">
                                    <h3>{{_i("Identification Card")}}</h3>
                                    <p>{{_i("In order to verify your account in our stock market, you need to upload your current photo and ID card, passport or driver's license on which you are required to carry out high-limit transactions. Write a small note on the date of the day and the purpose of sending a transaction to our stock exchange.")}}</p>

                                    <div class="form-group">
                                        <label for="id_card_front" class="m-0">{{_i("Identity front face")}}</label>
                                        <input type="file"
                                               class="form-control{{ $errors->has('id_card_front') ? ' is-invalid' : '' }}"
                                               id="id_card_front" name="id_card_front"
                                               required
                                               accept="image/jpg, image/jpeg, image/bmp, image/png, application/pdf">
                                        @if ($errors->has('id_card_front'))
                                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('id_card_front') }}</strong>
                                </span>
                                        @endif
                                    </div>

                                </div>
                               </div>
                            </div>


                            <div class="col-6">
                                <div class="row">
                                    <div class="col-lg-12 col-sm-12">
                                        <img src="{{asset("/images/holding-card.png")}}"
                                             class="img-thumbnail img-responsive noborder box-shadow">

                                    </div>
                                    <div class="col-lg-12 col-sm-12">
                                        <h3>{{_i("Selfie Photo")}}</h3>
                                        <p>{{_i("With your ID, which you installed in the previous step, after you have written the text below, load your face and photo clearly as shown in the example above.")}}</p>
                                        <p>{{_i("Example: Date of the Day")}}</p>

                                        <div class="form-group">
                                            <input type="file"
                                                   class="form-control{{ $errors->has('selfie') ? ' is-invalid' : '' }}"
                                                   id="selfie" name="selfie"
                                                   required
                                                   accept="image/jpg, image/jpeg, image/bmp, image/png, application/pdf">

                                            @if ($errors->has('selfie'))
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('selfie') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-warning">{{_i("All files you upload should be in jpg, bmp, png or pdf format and less than 5 mb.")}}</div>

                                <input type="submit" id="btnVerifyID"
                                       class="btn btn-success" value="{{_i("Send")}}">
                                </input>
                            </div>

                        </div>
                    </form>
                @endif
            </div>
        </div>
    </div>
@endsection
