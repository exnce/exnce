<div class="wizard-container">
    <div class="wizard-card" data-color="blue" id="wizard">
        <form action="" method="">


            <div class="wizard-navigation">
                <ul>
                    <li><a href="#info" data-toggle="tab">{{_i("Bilgilendirme")}}</a></li>
                    <li><a href="#camera" data-toggle="tab">{{_i("Yuz Tanimlama")}}</a></li>
                    <li><a href="#save" data-toggle="tab">{{_i("Etkinlestirme")}}</a></li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="tab-pane" id="info">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4 class="info-text"> </h4>
                        </div>
                        <div class="col-sm-6">


                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="camera">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12">

                            <video id="webcamVideoPanel" width="480" height="320" autoplay></video>
                            <button class="btn btn-success btnOpenCamera" id="snap">{{_i("Kamerayi Ac")}}</button>
                            <button class="btn btn-success btnTakePicture" id="snap">{{_i("Resim Cek")}}</button>
                        </div>
                        <div class="col-lg-6 col-sm-12">
                            <canvas id="canvas" width="480" height="320"></canvas>
                        </div>



                    </div>
                </div>
                <div class="tab-pane" id="save">
                    <div class="row">
                        <h4 class="info-text"> Drop us a small description.</h4>

                    </div>
                </div>
            </div>
            <div class="wizard-footer">
                <div class="pull-right">
                    <input type='button' class='btn btn-next btn-fill btn-danger btn-wd' name='next' value='Next'/>
                    <input type='button' class='btn btn-finish btn-fill btn-danger btn-wd' name='finish'
                           value='Finish'/>
                </div>
                <div class="pull-left">
                    <input type='button' class='btn btn-previous btn-fill btn-default btn-wd' name='previous'
                           value='Previous'/>

                    <div class="footer-checkbox">
                        <div class="col-sm-12">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="optionsCheckboxes">
                                </label>
                                Subscribe to our newsletter
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </form>
    </div>
</div>





