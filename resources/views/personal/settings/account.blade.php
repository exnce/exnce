
@extends('layouts.personal')
@section("title",_i("User Account Information"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">
            <div class="col-xl-3 col-lg-4 pr-lg-3 col-md-4 pr-md-3 col-sm-12 pr-sm-0 pb-sm-3 px-0">
                @include('layouts.account-sidebar')
            </div>
            <div class="col-xl-9 col-lg-8 col-md-8 col-sm-12">


                <div class="row pb-4">
                    <div class="card card rounded box-shadow pt-3 px-3 py-3 w-100">
                        <div class="card-header" id="cryptoCurrencyCard">
                            <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                               aria-controls="collapseCrypto">
                                <h5 class="mb-0">{{ _i("My Account Information") }}<span class="pull-right"><i
                                                class="fa fa-angle-double-down"></i></span></h5>
                                <span class="d-none d-lg-block d-md-block">{{ _i("You can see your personal account informations in detail.") }}</span>
                            </a>
                        </div>
                        <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                            <div class="card-body pl-0 pr-0 pb-0">
                                <div class="row">

                                    <div class="col-lg-12 col-sm-12 m-0 mb-4">

                                        <form method="post" action="{{ route("update-account-details") }}">
                                            @csrf

                                            <div class="row">
                                                <div class="form-group col-lg-3 col-sm-12 pr-lg-0">
                                                    <label for="name">{{ _i("Member ID:") }}</label>
                                                    <input type="text" id="name" class="form-control" name="name" value="{{ $user->id + 10000 }}" disabled>
                                                </div>

                                                <div class="form-group col-lg-9 col-sm-12">
                                                    <label for="name">{{ _i("Name Lastname") }}</label>
                                                    <input type="text" id="name" class="form-control" name="name" value="{{ $user->name }}" disabled>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-6 pr-0">
                                                    <label for="country">{{ _i("Country") }}</label>
                                                    <select type="text" id="country" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" required>

                                                        <option disabled selected>{{ _i("Please select") }}</option>
                                                        @foreach($countries as $country)
                                                            <option data-symbol="{{$country->iso_3166_3}}"  data-icon="{{mb_strtolower($country->iso_3166_2)}}" value="{{$country->id}}" {{($countryId == $country->id || old('country') == $country->id ? "selected" : '')}}>{{$country->name}}</option>
                                                        @endforeach

                                                        @if ($errors->has('country'))
                                                            <span class="invalid-feedback" role="alert">
                                                                            <strong>{{ $errors->first('country') }}</strong>
                                                                        </span>
                                                        @endif
                                                    </select>
                                                </div>

                                                <div class="form-group col-6">
                                                    <label for="city">{{ _i("City") }}</label>
                                                    <input type="text" id="city" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') ? old('city') : $city }}" required>

                                                    @if ($errors->has('city'))
                                                        <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('city') }}</strong>
                                                                    </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col">
                                                    <label for="email">{{ _i("E-Mail") }}</label>
                                                    <input type="text" id="email" class="form-control" name="email" value="{{ $user->email }}" disabled>
                                                </div>
                                            </div>

                                            <div class="row">
                                                @if($user->phone)
                                                    <div class="form-group col">
                                                        <label for="mobile">{{ _i("Phone") }}</label>
                                                        <input type="text" id="mobile" class="form-control" name="mobile" value="+{{ $user->phone }}" disabled>
                                                    </div>
                                                @else
                                                    <div class="col-12">
                                                        <label>{{ _i("Phone") }}</label>
                                                    </div>


                                                    <div class="form-group col-lg-3 col-sm-12 pr-lg-0">
                                                        <select id="calling_code" name="calling_code"
                                                                class="form-control"
                                                                placeholder="{{_i("Please select!")}}" data-allow_clear="1">
                                                            <option value="" disabled selected>{{ _i("Please select country code!") }}</option>
                                                            @foreach($countries as $country)
                                                                <option data-symbol="(+{{$country->calling_code}})"  data-icon="{{mb_strtolower($country->iso_3166_2)}}"  {{$country["calling_code"] == $user->calling_code || $country["calling_code"] == old('calling_code') ? "selected='true'" : "" }} value="{{ $country["calling_code"] }}">{{ $country["name"] }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group col-lg-9 col-sm-12">
                                                        <input class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }} btn-block"
                                                               id="mobilenumber"
                                                               name="phone_number" type="number">

                                                        @if ($errors->has('phone_number'))
                                                            <span class="invalid-feedback" role="alert">
                                                                      <strong>{{ $errors->first('phone_number') }}</strong>
                                                                     </span>
                                                        @endif
                                                    </div>
                                                @endif
                                            </div>

                                            <input type="submit" class="btn  pull-right btn-gradient-blue font-weight-bold px-4 btn-primary" value="{{_i("Update")}}">

                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        </div>
    </div>
@endsection
