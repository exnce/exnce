@extends('layouts.personal')
@section("title",_i("Initial Exchange Offering "))
@section('content')

    <div class="container-fluid py-2">


        <div class="col-lg-12 col-md-12 col-sm-12 mb-3 p-1 order-3">


            <div class="container pt-3">
                <div class="card rounded border-0 box-shadow pt-3 px-3">
                    <div class="card-body">




                    <div class="row">


                        @foreach ($baseCoins as $coin_id => $baseCoin)
                            @foreach ($pairList as $pair)

                                @php
                                    $strPair = $pair->symbol . "-" . $baseCoin;
                                    $activeClass =  "";
                                @endphp


                                <div class="col-12 col-sm-12 col-md-6 col-lg-3 mb-4">
                                    <div class="card h-100 box-shadow card-bottom-border">
                                        <div class="card-span bg-light-success mx-auto"></div>
                                        <div class="card-body text-center">
                                            <div class="col-12 p-0">
                                                <div class="item">
                                                    <a href="#">
                                                        <div class="glassed">
                                                            <img src="{{asset("/images/coins/".strtolower($pair->slug).".svg")}}">
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>

                                            <p class="text-uppercase" >{{$pair->name}} {{$pair->symbol}}</p>

                                            <p class="text-dark-bluish font-weight-normal">0.00000110 ETH</p>
                                            <div class="c100 p100 small default center green">
                                                <span class="text-dark-bluish">10 DAY</span>
                                                <div class="slice"><div class="bar"></div><div class="fill"></div></div>
                                            </div>
                                        </div>
                                        <div class="card-footer card-bottom-border footer-card-border-green text-center px-0 border-top-0 border-left-0 border-right-0">
                                            <div class="price-banner bg-warning py-1 mb-3">
                                                <p class="lead font-weight-bold text-white mb-0">40.000.000</p>
                                            </div>
                                            <div class="price-banner bg-light-success py-1 mb-3">
                                                <p class="lead font-weight-bold text-white mb-0">28.08.2019 17:00:00</p>
                                            </div>
                                            <div class="price-banner bg-danger py-1 mb-3">
                                                <p class="lead font-weight-bold text-white mb-0">05.08.2019 06:59:00</p>
                                            </div>
                                            <a href="{{route("ieopair",[$pair->symbol,$baseCoin])}}" class="btn bg-light-success font-weight-bold px-4 text-white rounded-soft small-text" name="button">{{_i("Details")}}</a>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-9 col-md-6 col-sm-12">
                                    <div class="card h-100 box-shadow card-bottom-border">
                                        <div class="card-span bg-light-success mx-auto"></div>
                                        <div class="card-body text-center">
                                            <div class="col-12 p-0">



                                                    <iframe width="100%" height="460px" src="https://www.youtube.com/embed/w1MFeGZUMSc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            @endforeach
                        @endforeach




                    </div>
                </div>

            </div>


        </div>
    </div>

    </div>




@endsection

