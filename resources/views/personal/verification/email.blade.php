
@extends('layouts.site')
@section("title",_i("Email Verification"))
@section('content')
    <section id="panel" class="mt-3">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                        <div id="accordion">
                            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3">
                                <div class="card-header pb-0" id="headingOne">
                                    <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <h5 class="mb-0">{{_i("Email Verification")}}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                        <p class="text-success" >{{ _i("Your account was successfully created. We sent you an email to verify your email address.") }}
                                        </p>
                                    </a>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body">
                                        <ul class="mt-3">
                                            <li>{{ _i("You must verify your email address before you can log in to your account.") }}</li>
                                            <li>{{ _i("Please click the verification link to your email address.") }}</li>
                                            <li>{{ _i("Please check your junk mail if you can't see the verification mail between your emails.") }}</li>
                                        </ul>

                                        <a href="{{route("login")}}" class="btn btn-primary sub-nav-active mt-3">{{ _i("Sign In") }}</a>
                                    </div>
                                </div>
                            </div>

                        </div>



                </div>
            </div>

    </section>
@endsection
