@extends('layouts.personal')
@section('content')
    <div class="container pt-3">
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Son bir adım: Fatura bilgileriniz</h3></div>
                    <div class="panel-body">
                        <p>Hesabınızı onaylayabilmeniz için lütfen aşağıdaki alanları doldurunuz. <a href="#">Bilgi al</a></p>

                        <form method="post">
                            @csrf

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label class="control-label col-md-5">Uyruğunuz</label>
                                    <div class="col-md-7">
                                        @php
                                            $nationality = old('nationality') ? old('nationality') : 1;
                                        @endphp
                                        <label class="radio-inline control-label">
                                            <input {{ ($nationality==1 ? 'checked' : '') }} id="nationality" name="nationality" type="radio" value="1"> T.C. Vatandaşı
                                        </label>
                                        <label class="radio-inline control-label">
                                            <input {{ ($nationality==2 ? 'checked' : '') }} id="nationality" name="nationality" type="radio" value="2"> Yabancı
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="tckn">T.C. Kimlik No</label>
                                    <input type="text" id="tckn" class="form-control{{ $errors->has('tckn') ? ' is-invalid' : '' }}" name="tckn" value="{{ old('tckn') }}" required>

                                    @if ($errors->has('tckn'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('tckn') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    Kimlik bilgileri ile aynı olmalıdır. Eğer T.C. Vatandaşı değilseniz pasaport no. giriniz.
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6">
                                    <label for="first_name">Ad</label>
                                    <input type="text" id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required>

                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="last_name">Soyad</label>
                                    <input type="text" id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required>

                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-row col-md-6 col-xs-12">
                                <div class="form-row col-12">
                                    <label>Doğum Tarihi</label>
                                </div>

                                <div class="form-group col-md-3">
                                    <select class="form-control{{ $errors->has('birth_day') ? ' is-invalid' : '' }}" id="birth_day" name="birth_day" required>
                                        <option hidden selected></option>
                                        @for ($i = 1; $i <= 31; $i++)
                                            <option value="{{ $i }}" {{ ($i==old('birth_day') ? 'selected' : '') }}>{{ $i }}</option>
                                        @endfor
                                    </select>

                                    @if ($errors->has('birth_day'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('birth_day') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-5">
                                    <select class="form-control{{ $errors->has('birth_month') ? ' is-invalid' : '' }}" id="birth_month" name="birth_month" required>
                                        <option hidden selected></option>
                                        @foreach(range(1, 12) as $key => $m)
                                            <option value="{{ date('n', mktime(0, 0, 0, $m, 1)) }}" {{ ($m==old('birth_month') ? 'selected' : '') }}>
                                                {{ date('F', mktime(0, 0, 0, $m, 1)) }}
                                            </option>
                                        @endforeach
                                    </select>

                                    @if ($errors->has('birth_month'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('birth_month') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="form-group col-md-4">
                                    @php
                                        $last = (int)date('Y')-120;
                                        $now = (int)date('Y')-18;
                                    @endphp

                                    <select class="form-control{{ $errors->has('birth_year') ? ' is-invalid' : '' }}" id="birth_year" name="birth_year" required>
                                        <option hidden selected></option>
                                        @foreach (range($now, $last) as $year)
                                            <option value="{{ $year }}" {{ ($year==old('birth_year') ? 'selected' : '') }}>{{ $year }}</option>
                                        @endforeach

                                    </select>

                                    @if ($errors->has('birth_year'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('birth_year') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-4">
                                    <label for="province">İl</label>
                                    <input type="text" id="province" class="form-control{{ $errors->has('province') ? ' is-invalid' : '' }}" name="province" value="{{ old('province') }}" required>

                                    @if ($errors->has('province'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('province') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-4">
                                    <label for="town">İlçe</label>
                                    <input type="text" id="town" class="form-control{{ $errors->has('town') ? ' is-invalid' : '' }}" name="town" value="{{ old('town') }}" required>

                                    @if ($errors->has('town'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('town') }}</strong>
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group col-4">
                                    <label for="district">Semt / Mahalle / Köy / Mevki</label>
                                    <input type="text" id="district" class="form-control{{ $errors->has('district') ? ' is-invalid' : '' }}" name="district" value="{{ old('district') }}" required>

                                    @if ($errors->has('district'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('district') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="address">Adres</label>
                                    <textarea id="address" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" required>{{ old('address') }}</textarea>

                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <input type="submit" class="btn btn-primary sub-nav-active m-auto" value="Kaydet">
                            </div>
                        </form>
                    </div>
                </div>
    </div>
@endsection
