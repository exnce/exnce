
@extends('layouts.personal')
@section("title",_i("SMS Onaylama"))
@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                @include("personal.verification.security-image-register")
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class=" card rounded border-0 box-shadow h-100">
                    <div class="card-header">
                        <h4 class="mb-0">{{_i("SMS Onaylama")}}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h4>
                    </div>
                        <div class="card-body pb-0">
                            <div class="form-group">
                                <p>{{_i("Sayın")}} <b class="text-danger">{{ $userName }}</b>,</p>
                                <p>{{_i("Lütfen cep telefonunuza gelen 6 haneli kodu giriniz")}}</p>
                            </div>

                            <div class="form-group">
                                <form method="post" action="{{ route("verify-sms") }}">
                                    @csrf
                                    <div class="form-group mt-1">
                                        <div class="form-group">
                                            <div class="text-primary text-left">
                                                <span class="font-weight-bold">{{ _i("Telefon Numaranız") }}</span>
                                            </div>
                                            <input class="form-control bordered-input btn-block" type="text" readonly="true" value="+{{ $userPhone }}">
                                        </div>
                                        {{--
                                        <div class="form-group">
                                            <span>Eğer numara yanlış ise güncellemek için <a href="{{ route("verify-mobile") }}">buraya</a> tıklayınız.</span>
                                        </div>
                                        --}}
                                        <div class="form-group" id="smsCodeContainer">
                                            <div class="text-primary text-left">
                                                <span class="font-weight-bold">{{ _i("SMS Kodu") }}</span>
                                            </div>
                                            <input class="form-control {{ $errors->has('sms_code') ? ' is-invalid' : '' }} bordered-input btn-block" id="sms_code"
                                                   name="sms_code" type="text">

                                            @if ($errors->has('sms_code'))
                                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('sms_code') }}</strong>
                                             </span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <span id="smsTimer" class="hasCountdown"></span>
                                        </div>

                                        <div class="form-group mt-1">
                                            <input type="submit" class="btn btn-block btn-gradient-blue font-weight-bold" id="smsValidation" value="{{_i("Onayla")}}">
                                            <a class="btn btn-block btn-gradient-blue font-weight-bold text-white" id="sendAgain" style="display: none;"
                                               href="{{route("verify-sms")}}">{{_i("Tekrar Gönder")}}</a>

                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section("script")
    <script src="{{asset("js/jquery.plugin.min.js")}}?version={{env("JS_VERSION")}}"></script>
    <script src="{{asset("js/jquery.countdown.min.js")}}?version={{env("JS_VERSION")}}"></script>

    <script type="text/javascript">
        var min = {{ $minutes }};
        var sec = {{ $seconds }};
        var countdownLayout = '{{ _i("Yeniden sms gönderebilmek için <strong>{mnn}{sep}{snn}</strong> saniye beklemelisiniz.") }}';
        var countdownTimeOutMsg = '{{ _i("Size ayrılan süre dolmuştur. Lütfen tekrar deneyiniz.") }}';

        $('#smsTimer').countdown({
            until: min + "m" + sec + "s",
            compact: true,
            layout: countdownLayout,
            onExpiry: countdownExpire
        });

        function countdownExpire() {

            $("#sendAgain").css("display", "");
            $("#smsValidation").css("display", "none");
            $("#smsCodeContainer").css("display", "none");

            var val = countdownTimeOutMsg;
            $("#smsTimer").text(val);
            $('#smsTimer').removeClass("hasCountdown");

        };

    </script>
@endsection
