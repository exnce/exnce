
@extends('layouts.personal')
@section("title",_i("Phone Number Verification"))
@section('content')
    <div class="container pt-5">
        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                @include("personal.verification.security-image-register")
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class=" card rounded border-0 box-shadow">
                    <div class="card-header">
                        <a data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
                           aria-controls="collapseTwo">
                            <h4 class="mb-0">{{_i("Mobile Identification")}}<span class="pull-right"><i
                                            class="fa fa-angle-double-down"></i></span></h4>
                        </a>
                    </div>
                    <div id="collapseTwo" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body pb-0">
                        <div class="form-group">
                            <p>{{_i("Dear")}} <b class="text-danger">{{ $user->name }}</b>,</p>
                            <p>{{_i("Please enter your mobile phone number for the security of your account.")}}</p>
                        </div>

                        <div class="form-group">
                            <form method="post" action="{{ route("verify-mobile-confirm") }}">
                                @csrf
                                <div class="form-group mt-1">
                                    <div class="form-group">
                                        <div class="text-primary text-left">
                                            <span class="font-weight-bold">{{ _i("Please Select Country") }}</span>
                                        </div>
                                        <select id="calling_code" name="calling_code"
                                                class="form-control selectCallingCode bordered-input btn-block"
                                                placeholder="{{_i("Choose anything")}}" data-allow_clear="1">
                                            @foreach($countries as $country)
                                                <option {{$country["calling_code"] == $user->calling_code ? "selected='true'" : "" }} value="{{ $country["calling_code"] }}">{{ $country["name"] }}
                                                    (+{{ $country["calling_code"] }})
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class=form-group">
                                        <div class="text-primary text-left">
                                            <span class="font-weight-bold">{{ _i("Phone Number") }}</span>
                                        </div>
                                        <input class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }} bordered-input btn-block" id="mobilenumber"
                                               name="phone_number" type="number">

                                        @if ($errors->has('phone_number'))
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $errors->first('phone_number') }}</strong>
                                             </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group mt-1">
                                    <input type="submit" class="btn btn-primary sub-nav-active btn-block"
                                           value="{{ _i("Sign In") }}">
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>

@endsection
