
<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header">
        <h4 class="mb-0">{{_i("Security Image")}}
            <span class="pull-right"><i class="fa fa-angle-double-down"></i></span>
        </h4>
    </div>

    <div class="card-body pb-0">
        <div class="form-group text-center">
            <img src="data:image/jpg;base64, {{ $image }}" class="img-responsive">
        </div>
        <div class="form-group">
            <p class="text-justify">{{_i("This security image is defined in your account and will be displayed to you every time you log in to your account.")}}</p>
            <p class="text-justify">{{_i("Note that the address that appears in your browser's address bar is exactly the following.")}}</p>
        </div>
        <div class="form-group text-center">
            <img src="{{ asset("/images/secure-exnce.png") }}" height="30px">
        </div>
    </div>
</div>
