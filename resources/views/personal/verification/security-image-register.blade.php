
<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header">
        <h4 class="mb-0">{{_i("Security Image")}}
            <span class="pull-right"><i class="fa fa-angle-double-down"></i></span>
        </h4>
    </div>

    <div class="card-body pb-0">
        <div class="form-group text-center">
            <img src="data:image/jpg;base64, {{ $image }}" class="img-responsive">
        </div>
        <div class="form-group">
            <p class="text-justify">{{ _i("This security image has been specially created for you and defined in your account.") }}</p>
            <p> {{_i("If you think you have seen a different picture in your next entries, please check the address of your browser in the address bar.")}}</p>
        </div>
        <div class="form-group text-center">
            <img src="{{ asset("/images/secure-exnce.png") }}" height="30px">
        </div>
    </div>
</div>
