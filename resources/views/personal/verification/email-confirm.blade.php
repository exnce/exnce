
@extends('layouts.site')
@section("title",_i("Email Verification"))
@section('content')
    <section id="panel" class="mt-3">
        <div class="container pt-3">
            <div class="row">

                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="pb-4" id="accordion">
                        <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="headingOne">
                                <a data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <h5 class="mb-0">{{ $title }}<span class="pull-right"><i class="fa fa-angle-double-down"></i></span></h5>
                                    <p>{{$message}}</p>
                                </a>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body">

                                    <h5 class="font-weight text-dark">{{ _i("Attention please!") }}</h5>
                                    <ul class="mt-3">
                                        <li>{{ _i("Our authorities never ask you for your password by e-mail.") }}</li>
                                        <li>{{ _i("Your wallet addresses are not forwarded via e-mail.") }}</li>
                                        <li>{{ _i("Do not send money to the specified wallet addresses by e-mail.") }}</li>
                                        <li>{{ _i("Please do not forget to share with us such exceptions.") }}</li>
                                    </ul>
                                    <a href="{{route("login")}}" class="btn btn-primary sub-nav-active col-12 mt-3">{{ _i("Sign In") }}</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
