@extends('layouts.personal')
@section("title",_i("Support Requests"))


@section("content")
    <div class="container pt-3">
        <div class="row pb-4">
            <div class="card card rounded box-shadow pt-3 px-3 py-3 w-100">
                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                       aria-controls="collapseCrypto">
                        <h5 class="mb-0">{{ _i("Support Requests") }}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h5>
                        <span class="d-none d-lg-block d-md-block">{{ _i("You can request a support request and see your support requests in detail.") }}</span>
                    </a>
                </div>
                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="row">

                            <div class="col-12 mb-3">

                                <nav class="nav nav-pills flex-column flex-sm-row">
                                    <a class="text-sm-center nav-link {{!request()->segment(3) ? "active" : ""}}"
                                       href="{{route("support")}}">{{_i("Active Tickets")}}
                                        <span class="badge badge-pill badge-warning ">{{$unsolved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "completed" ? "active" : ""}}"
                                       href="{{route("support-completed")}}">{{_i("Ticket History")}}
                                        <span class="badge badge-pill badge-success">{{$solved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "create" ? "active" : ""}}"
                                       href="{{route("support-create")}}">{{_i("Create New Ticket")}}
                                    </a>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 w-100 mb-5">

                @if(session()->has('message'))
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session()->get('message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif



                    <div class="row">
                        <div class="col-12">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="content">
                                        <h2 class="header text-uppercase">
                                            <i class="fas fa-file-signature text-danger"></i> {{$ticket->subject}}
                                            @if($ticket->status_id != 1)
                                                <span class="pull-right">

                                                <a href="{{route("support-reopen",$ticket->id)}}"
                                                   class="btn btn-success">{{ _i("Reopen") }}</a>
                                                </span>
                                            @endif
                                        </h2>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <div class="col-lg-2 col-sm-12">
                                     <div class="row p-1">
                                        <div class="col-md-12">
                                            <p>
                                                <strong>{{_i("Status")}}</strong>: </p>
                                            <p>
                                                <span style="color: {{$ticket->status->color}}">{{_i($ticket->status->name) }}</span>
                                            </p>

                                            <p>
                                                <strong>{{_i("Priority")}}</strong>:
                                            </p><p>
                                                <span style="color: #e1d200">
                                                            {{ _i($ticket->priority->name)}}
                                                        </span>
                                              </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>
                                                <strong>{{_i("Operator")}}</strong>:</p>
                                            <p> {{ $ticket->agent != null ? $ticket->agent->name : "-" }}
                                            </p>
                                            <p>
                                                <strong>{{_i("Category")}}</strong>:</p>
                                            <p>
                                                <span style="color: #7e0099">
                                                      {{_i($ticket->category->name)}}
                                                    </span>
                                            </p>
                                        </div>

                                        <div class="col-md-12">
                                            <p><strong>{{_i("Created At")}}</strong>:</p><p> {{$ticket->created_at}}</p>
                                            <p><strong>{{_i("Last Update")}}</strong>:</p><p> {{$ticket->updated_at}}</p>
                                        </div>
                                    </div>
                             </div>


                    <div class="col-lg-10 col-sm-12">

                        <div class="chat_window">
                            <ul class="messages">
                                <li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $ticket->content !!}</div></div></li>


                                @foreach($ticket->comments as $comment)
                                    @if($user->id == $comment->user_id)
                                <li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $comment->content !!}</div></div></li>
                                    @else
                                <li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $comment->content !!}</div></div></li>
                                    @endif
                                @endforeach

                            </ul>
                            <div class="bottom_wrapper clearfix">
                                <form method="POST">
                                    @csrf
                                <div class="message_input_wrapper">
                                    <input id="comment" name="comment" class="message_input"
                                                                          placeholder="{{_i("Please enter your message here...")}}"/></div>
                                <button type="submit" class="send_message">
                                    <div class="icon"></div>
                                    <div class="text">{{_i("Send")}}</div>
                                </button>
                                </form>
                            </div>
                        </div>

                    </div>

            </div>
            </div>
        </div>






        </div>







@endsection