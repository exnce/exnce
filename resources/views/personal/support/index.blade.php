
@extends('layouts.personal')
@section("title",_i("Support Requests"))


@section("content")
    <div class="container pt-3">


        <div class="row pb-4">
            <div class="card card rounded box-shadow pt-3 px-3 py-3 w-100">
                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                       aria-controls="collapseCrypto">
                        <h5 class="mb-0">{{ _i("Support Requests") }}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h5>
                        <span class="d-none d-lg-block d-md-block">{{ _i("You can request a support request and see your support requests in detail.") }}</span>
                    </a>
                </div>
                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="row">

                            <div class="col-12 mb-3">

                                <nav class="nav nav-pills flex-column flex-sm-row">
                                    <a class="text-sm-center nav-link {{!request()->segment(3) ? "active" : ""}}"
                                       href="{{route("support")}}">{{_i("Active Tickets")}}
                                        <span class="badge badge-pill badge-warning ">{{$unsolved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "completed" ? "active" : ""}}"
                                       href="{{route("support-completed")}}">{{_i("Ticket History")}}
                                        <span class="badge badge-pill badge-success">{{$solved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "create" ? "active" : ""}}"
                                       href="{{route("support-create")}}">{{_i("Create New Ticket")}}
                                    </a>
                                 </nav>

                            </div>



                            <div class="col-12">

                                <table id="lstTickets" style="100%; margin-top: 10px !important;" cellspacing="0"
                                       class="table display table-striped noborderradius nowrap no-footer collapsed table-hover">
                                    <thead class="table-head font-weight-bold border-0">
                                    <tr>
                                        <th class="border-top-0">{{ _i("Description") }}</th>
                                        <th class="border-top-0">{{ _i("Status") }}</th>
                                        <th class="border-top-0">{{ _i("Last Update") }}</th>
                                        <th class="border-top-0 text-right">{{ _i("Operator") }}</th>
                                        <th class="border-top-0"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@section('script')
    <script>
    var ListCompletedSupportRequests;
    @if(request()->segment(3) == "completed")
        ListCompletedSupportRequests = true;
    @else
        ListCompletedSupportRequests = false;
    @endif
    </script>
@endsection
