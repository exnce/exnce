@extends('layouts.personal')
@section("title",_i("Create Ticket"))


@section("content")
    <div class="container pt-3">
        <div class="row">


        </div>

        <div class="row pb-4">
            <div class="card card rounded  box-shadow pt-3 px-3 py-3 w-100">
                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                       aria-controls="collapseCrypto">
                        <h5 class="mb-0">{{ _i("Create Ticket") }}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h5>
                        <span class="d-none d-lg-block d-md-block">{{ _i("Create a new support request.") }}</span>
                    </a>
                </div>
                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <form method="POST" id="frmCreateSupportRequest" name="frmCreateSupportRequest">
                            @csrf
                        <div class="row">

                            <div class="col-12 mb-3">

                                <nav class="nav nav-pills flex-column flex-sm-row">
                                    <a class="text-sm-center nav-link {{!request()->segment(3) ? "active" : ""}}"
                                       href="{{route("support")}}">{{_i("Active Tickets")}}
                                        <span class="badge badge-pill badge-warning ">{{$unsolved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "completed" ? "active" : ""}}"
                                       href="{{route("support-completed")}}">{{_i("Tickets History")}}
                                        <span class="badge badge-pill badge-success">{{$solved}} </span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "create" ? "active" : ""}}"
                                       href="{{route("support-create")}}">{{_i("Create New Ticket")}}
                                    </a>
                                </nav>

                            </div>
                            <div class="col-12">

                                @if($errors->has('subject'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{_i("Please enter a subject!")}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                @if($errors->has('description'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{_i("Please enter a description!")}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                @if($errors->has('priority'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{_i("Please select priority!")}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif
                                @if($errors->has('category'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{_i("Please select unit!")}}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                @endif

                                    @if(session()->has('message'))
                                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                                            {{ session()->get('message')}}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif

                                <div class="row">
                                    <div class="form-group col-12">
                                        <label for="subject">{{ _i("Subject:") }}</label>
                                        <input type="text" id="subject" class="form-control" name="subject"
                                               value="">
                                    </div>

                                    <div class="form-group col-lg-8 col-sm-12">
                                        <label for="description">{{ _i("Description") }}</label>
                                        <textarea name="description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}" id="description"></textarea>
                                    </div>

                                    <div class="col-lg-4 col-sm-12">
                                        <div class="row">

                                            <div class="form-group col-12">
                                                <label for="prority">{{ _i("Priority:") }}</label>
                                                <select type="text" id="priority" class="form-control {{ $errors->has('priority') ? ' is-invalid' : '' }}" name="priority">
                                                    @foreach($priorities as $priority => $value)
                                                        <option key="{{$value["key"]}}" value="{{$value["key"]}}">{{$value["value"]}}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <label for="category">{{ _i("Unit:") }}</label>
                                                <select type="text" id="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}" name="category">

                                                   @foreach($categories as $category => $value)
                                                       <option key="{{$value["key"]}}" value="{{$value["key"]}}">{{$value["value"]}}</option>
                                                   @endforeach

                                                </select>
                                            </div>

                                            <div class="form-group col-12">
                                                <button id="btnSendSupportRequest" class="btn btn-success form-control">{{_("Submit Ticket")}}</button>
                                            </div>
                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#description').summernote({
                toolbar: [

                    ['style', ['bold', 'italic', 'underline']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert',['picture']]
                ],
                height: 180,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'paper'
                },

            });
        });
    </script>
@endsection