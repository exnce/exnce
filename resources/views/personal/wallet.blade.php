
@extends('layouts.personal')
@section("title",_i("Wallet"))
@section('content')

    <section class="mt-0">
        <div class="container-fluid pt-3">
            <div class="row">

                <div class="col">
                    <div class="pb-4" id="cryptoCurrency">
                        <div class="card card rounded box-shadow pt-3 px-3 py-3 pb-3">
                            <div class="card-header" id="cryptoCurrencyCard">
                                <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                                   aria-controls="collapseCrypto">
                                    <h5 class="mb-0">{{ _i("Wallet") }}<span class="pull-right"><i
                                                    class="fa fa-angle-double-down"></i></span></h5>
                                    <span class="d-none d-lg-block d-md-block">{{ _i("You can see the crypto money wallets and balances that you have defined in your account.") }}</span>
                                </a>
                            </div>
                            <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                                <div class="card-body pl-0 pr-0 pb-0">
                                    <div class="row">


                                        <div class="col-lg-8 col-sm-12">



                                        <table id="walletTable"  name="walletTable" class="table display table-hover noborderradius nowrap" style="width:100%;margin-top:10px !important;">
                                            <thead class="table-head font-weight-bold border-0">
                                            <tr>
                                                <th>{{ _i("Coin") }}</th>
                                                <th>{{ _i("Available Balance") }}</th>
                                                <th>{{ _i("In Order") }}</th>
                                                <th>{{ _i("Total Balance") }}</th>
                                                <th>{{ _i("Action") }}</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                        </div>

                                        <div class="col-lg-4 col-sm-12">
                                            <div id="walletChartPanel">
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





            </div>
        </div>
    </section>


@endsection
