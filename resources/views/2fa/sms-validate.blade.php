

@extends('layouts.site')
@section("title",_i("Two-factor Authentication"))
@section('content')
    <section class="mt-3 container">
        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                @include("personal.verification.security-image-login")
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card rounded border-0 box-shadow h-100">
                    <div class="card-header">
                        <h4 class="mb-0">{{_i("SMS Authentication")}}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h4>
                    </div>

                    <div class="card-body row">
                        <div class="w-75 m-auto">
                            <div class="form-group">
                                <p>{{_i("Mr/Mrs")}} <b class="text-danger">{{ $userName }}</b>,</p>
                                <p>{{_i("Enter the 6-digit code sent to your defined mobile phone.")}}</p>
                                <span id="smsTimer" class="hasCountdown"></span>
                            </div>

                            <form method="POST" action="{{ route('sms-validate') }}">
                                @csrf
                                <div class="form-group">
                                    <input class="form-control bordered-input btn-block" type="text" readonly="true" value="+{{ $userPhone }}">
                                </div>

                                <div class="form-group" id="smsCodeContainer">
                                    <input type="number"
                                           class="form-control{{ $errors->has('sms_code') ? ' is-invalid' : '' }} bordered-input"
                                           name="sms_code" id="sms_code" required autofocus>
                                    @if ($errors->has('sms_code'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('sms_code') }}</strong>
                                </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-block btn-gradient-blue font-weight-bold" id="smsValidation" value="{{_i("Login")}}">
                                    <a class="btn btn-block btn-gradient-blue font-weight-bold text-white" id="sendAgain" style="display: none;"
                                       href="{{route("send-sms")}}">{{_i("Send Again")}}</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
@section("script")
<script src="{{asset("js/jquery.plugin.min.js")}}?version={{env("JS_VERSION")}}"></script>
<script src="{{asset("js/jquery.countdown.min.js")}}?version={{env("JS_VERSION")}}"></script>

<script type="text/javascript">
    var min = {{ $minutes }};
    var sec = {{ $seconds }};
    var countdownLayout = '{{ _i("You must wait <strong>{mnn}{sep}{snn}</strong> seconds to send sms.") }}';
    var countdownTimeOutMsg = '{{ _i("The time allocated to you is expired. Please try again.") }}';

    $('#smsTimer').countdown({
        until: min + "m" + sec + "s",
        compact: true,
        layout: countdownLayout,
        onExpiry: countdownExpire
    });

    function countdownExpire() {

        $("#sendAgain").css("display", "");
        $("#smsValidation").css("display", "none");
        $("#smsCodeContainer").css("display", "none");

        var val = countdownTimeOutMsg;
        $("#smsTimer").text(val);
        $('#smsTimer').removeClass("hasCountdown");

    };

</script>
@endsection
