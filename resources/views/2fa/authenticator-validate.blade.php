
@extends('layouts.site')
@section("title",_i("Two-factor Authentication"))
@section('content')
    <section class="mt-3 container">
        <div class="row">
            <div class="col-md-6 col-sm-12 mb-3">
                @include("personal.verification.security-image-login")
            </div>
            <div class="col-md-6 col-sm-12 mb-3">
                <div class="card rounded border-0 box-shadow h-100">
                    <div class="card-header">
                        <h4 class="mb-0">{{_i("Google Authenticator")}}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h4>
                    </div>

                    <div class="card-body row">
                        <div class="w-75 m-auto">

                        <div class="form-group">
                            <p>{{_i("Mr/Mrs")}} <b class="text-danger">{{ $userName }}</b>,</p>
                            <p>{{_i("Enter the 6-digit pin code generated for this site in Google Authenticator on your mobile phone.")}}</p>
                        </div>

                        <form method="POST" action="{{ route('authenticator-validate') }}">
                            @csrf

                            <div class="form-group">
                                <input type="number"
                                       class="form-control{{ $errors->has('totp') ? ' is-invalid' : '' }} bordered-input"
                                       name="totp" required autofocus>
                                @if ($errors->has('totp'))
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('totp') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn-block btn-gradient-blue font-weight-bold btn-primary" value="{{_i("Giriş")}}">
                            </div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>

@endsection
