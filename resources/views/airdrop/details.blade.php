<div class="row">
    <div class="col-md-4 col-sm-12">
        <div class="row">

            <div class="col-12 p-0">
                <div class="item">
                <a href="#">
                <div class="glassed">
                <img src="{{asset("/images/coins/".strtolower($airdrop->coin->slug).".svg")}}">
                </div>
                </a>
                </div>
            </div>


            <div class="col-12">
                <small class="text-small">{{_i("Token Name")}}</small>
                <input readonly="readonly" type="text"
                       class="form-control bg-white"
                       value="{{$airdrop->coin->name}}">

            </div>

            <div class="col-12">
                <small class="text-small">{{_i("Contrat Address")}}</small>
                <input readonly="readonly" type="text"
                       class="form-control bg-white"
                       value="{{$airdrop->coin->contract}}">

            </div>

            <div class="col-12">
                <small class="text-small">{{_i("Airdrop Token Amount")}}</small>
                <input readonly="readonly" type="text"
                       class="form-control bg-white"
                       value="{{$airdrop->amount}}">
            </div>

            <div class="col-12">
                <small class="text-small">{{_i("Start Date")}}</small>
                <input readonly="readonly" type="text"
                       class="form-control bg-white"
                       value="{{$airdrop->start_date}}">
            </div>

            <div class="col-12">
                <small class="text-small">{{_i("Finish Date")}}</small>
                <input readonly="readonly" type="text"
                       class="form-control bg-white"
                       value="{{$airdrop->finish_date}}">
            </div>

            <div class="col-12">
                <div class='rating-stars text-center pt-3'>
                    <ul id='stars'>
                        <li class='star' title='Poor' data-value='1' data-airdropid="{{$airdrop->id}}">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Fair' data-value='2' data-airdropid="{{$airdrop->id}}">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Good' data-value='3' data-airdropid="{{$airdrop->id}}">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='Excellent' data-value='4' data-airdropid="{{$airdrop->id}}">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                        <li class='star' title='WOW!!!' data-value='5' data-airdropid="{{$airdrop->id}}">
                            <i class='fa fa-star fa-fw'></i>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="col-12">
                @if(auth()->user())
                    @if(!$airdrop->isFollowedBy(auth()->user()))
                        <button data-airdropid="{{$airdrop->id}}" class="btn btn-success btn-block btn-follow-airdrop">{{_i("Join")}}</button>
                    @else
                        <button data-airdropid="{{$airdrop->id}}" class="btn btn-danger btn-block btn-unfollow-airdrop">{{_i("Leave")}}</button>
                    @endif
               @else
                    <button data-airdropid="{{$airdrop->id}}" class="btn btn-success disabled btn-block btn-follow-airdrop">{{_i("Join")}}</button>
                @endif
            </div>

        </div>
    </div>
    <div class="col-md-8 col-sm-12 small-text">
        @php
        $layout = "airdrop.tokens.".$airdrop->coin->slug;
        @endphp
        @include($layout)
    </div>
</div>



