@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("EXNCE Airdrop"))
@section('content')



    <div class="row pl-3 pr-3 mb-5">

        <div class="col-12">




            <div class="card rounded border-0 box-shadow">

                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true" aria-controls="collapseCrypto">
                        <h5 class="text-justify">{{ _i('EXNCE Airdrop') }}</h5>
                        <span class="text-justify d-none d-lg-block d-md-block">{{_i("Are you looking for free crypto airdrops? Try exnce.com! EXNCE airdrop is your free access to new token projects. Uniting airdrop channels and projects worldwide, EXNCE creates a new one-stop airdrop mode with fast, efficient and credible guarantee for all our customers.")}}</span>
                    </a>
                </div>


                <div class="card-body">



                    <div class="row">
                        <div class="col-12 p-0">

                            <table id="listAirdrops" name="listAirdrops" style="100%; margin-top: 10px !important;" cellspacing="0"
                                   class="table display dataTable table-hover noborderradius nowrap responsive">
                                <thead class="table-head text-left font-weight-bold">
                                <tr>
                                    <th>{{ _i("Name") }}</th>
                                    <th>{{ _i("Symbol") }}</th>
                                    <th>{{ _i("Model") }}</th>
                                    <th>{{ _i("Ends In") }}</th>
                                    <th>{{ _i("Token Amount") }}</th>
                                    <th>{{ _i("Followers") }}</th>
                                    <th>{{ _i("Rating") }}</th>
                                    <th class="{{Auth::check() ? '' : 'hidden'}}">{{ _i("Action") }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airdrops as $airdrop)
                                <tr>
                                    <td><img class="img-thumbnail p-1" src="{{asset("/images/coins/".strtolower($airdrop->coin->slug).".svg")}}"
                                              width="48px"/> {{ $airdrop->coin->name }}<br/>
                                    </td>
                                    <td>{{ $airdrop->coin->symbol }}</td>
                                    <td>{{ $airdrop->model }}</td>
                                    <td>{{ $airdrop->finish_date  }}</td>
                                    <td>{{ $airdrop->amount }}</td>
                                    <td>{{ $airdrop->followers()->get()->count() }}</td>
                                    <td>

                                        <div class="progress-outer">
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-info progress-bar-striped active" style="width:{{$airdrop->ratingPercent(5)}}%; box-shadow:-1px 5px 5px rgba(91, 192, 222, 0.7);">
                                                    <div class="progress-value">{{ number_format((float)$airdrop->ratingPercent(5), 2, '.', '')}}%</div>
                                                    </div></div>
                                            </div>

                                        </td>
                                    <td>
                                        @if($airdrop->status == 0)
                                        <button data-id="{{$airdrop->id}}" class="btn btn-success btn-block btn-sm btn-airdrop-details">{{_i("Details")}}</button>
                                        @endif
                                    </td>
                                    <td></td>
                                </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>


                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="frmAirdropDetails" tabindex="-1" role="dialog" aria-labelledby="frmAirdropDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Airdrop Details") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body airdrop-details">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Close") }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
