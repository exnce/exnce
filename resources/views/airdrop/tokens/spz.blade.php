@php

    $telegramURL = $airdrop->coin->detail->telegram;
    $twitterURL = $airdrop->coin->detail->twitter;
    $youtubeVideo = $airdrop->coin->detail->youtube;
    $twitterTweet = "https://twitter.com/swapcoinz/status/1116136204003631105";

@endphp


<div class="row">
    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">About {{$airdrop->coin->name}}
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectDescription" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectDescription_wrapper">
            <div class="row">
                <div class="col-12">
                    <p class="text-justify small-text p-3 card card-body">
                        {{$airdrop->coin->detail->description}}
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Presentation
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="presentation" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="presentation_wrapper">
            <div class="row">
                <div class="col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/DPp0Ggqb01E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Step by Step Guide
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectGuide" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectGuide_wrapper">
            <div class="row">
                <div class="col-12 pt-2">


                    <ol>
                        <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
                        <ul>
                            <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
                        </ul>
                        <li>If you dont have an {{$airdrop->coin->symbol}} wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
                        <ul>
                            <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
                        </ul>
                        <li>Follow EXNCE official reddit account</li>
                        <ul>
                            <li><a href="https://www.reddit.com/user/exnce" target="_blank">Follow</a></li>
                        </ul>


                        <li>Follow EXNCE and  {{$airdrop->coin->symbol}} official twitter account</li>
                        <ul>
                            <li><a href="https://twitter.com/exncecom" target="_blank">Follow</a>  <b>EXNCE</b></li>
                            <li><a href="{{$twitterURL}}" target="_blank">Follow</a> <b>{{$airdrop->coin->name}}</b></li>
                        </ul>
                        <li>Retweet the activity Tweet, tag <b>5</b> friends</li>
                        <ul>
                            <li><a href="{{$twitterTweet}}" target="_blank">Retweet</a></li>
                        </ul>
                        <li>Join the Telegram group of EXNCE and {{$airdrop->coin->symbol}}</li>
                        <ul>
                            <li><a href="https://t.me/exncecom" target="_blank">Join Now</a></li>
                            <li><a href="{{$telegramURL}}" target="_blank">Join Now</a></li>
                        </ul>

                        <li>Subscribe the Youtube channel of EXNCE</li>
                        <ul>
                            <li><a href="https://www.youtube.com/channel/UCMYmuS7xYqQ8BggH1BMKIuw" target="_blank">Subscribe Now</a></li>
                        </ul>

                        <li>Like and write a comment to this video</li>
                        <ul>
                            <li><a href="{{$youtubeVideo}}" target="_blank">Write Now</a></li>
                        </ul>


                        <li>Like the Facebook page of EXNCE</li>
                        <ul>
                            <li><a href="https://www.facebook.com/exncecom" target="_blank">Like Now</a></li>
                        </ul>

                    </ol>

                </div>
            </div>

        </div>

    </div>



</div>



