@php
    $telegramURL = "https://t.me/joinchat/G6U84lLsGUWmR0IkfMiRHw";
    $twitterURL = "https://twitter.com/realCoinStarter";
    $youtubeVideo = "https://www.youtube.com/watch?v=v-AzNyP8krU";
    $twitterTweet = "https://twitter.com/exncecom/status/1134296338970107915";
    $facebook = "https://www.facebook.com/exncecom/posts/1049236818605151";
    $coinchat = "https://www.coinchat.im/candy/u_sRTlgmKpsdCBkrZycI8EQU5QeuS_CI0";
    $exnce_reddit = "https://www.reddit.com/user/exnce/comments/bv2qra/exnce_5000_stac_airdrop_has_launched/";


@endphp

<h2 class="text-warning">About {{$airdrop->coin->name}}</h2>

<p>
    The​ ​next​ ​step​ ​in​ ​the​ ​CoinStarter​ ​journey​ ​is​ ​the​ ​launch​ ​of​ ​the​ ​StarterCoin.​ ​​ ​A​ ​purpose-built​ ​Coin using​ ​the​ ​CoinStarter​ ​network​ ​that​ ​will​ ​allow​ ​members​ ​to​ ​take​ ​an​ ​active​ ​role​ ​in​ ​the​ ​development of​ ​the​ ​company.</br>
    StarterCoins​ ​are​ ​a​ ​cryptocurrency​ ​which​ ​gives​ ​rights​ ​and​ ​privileges​ ​to​ ​their​ ​owners​ ​when​ ​using the​ ​CoinStarter​ ​platform​ ​and​ ​its​ ​family​ ​of​ ​services.StarterCoins​ ​are​ ​available​ ​at​ ​a​ ​discount during​ ​the​ ​ICO.​ ​​ ​Any​ ​unsold​ ​coins​ ​during​ ​the​ ​ICO​ ​burned.​ ​​ ​​ ​Within​ ​a​ ​few​ ​weeks​ ​following​ ​the​ ​ICO, StarterCoins​ ​will​ ​be​ ​available​ ​on​ ​most​ ​exchanges.
</p>

<h2>Step-by-step Guide:</h2>

<ol>
    <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
    <ul>
        <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
    </ul>
    <li>If you dont have an {{$airdrop->coin->symbol}}  wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
    <ul>
        <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
    </ul>
    <li>Follow EXNCE official reddit account</li>
    <ul>
        <li><a href="{{$exnce_reddit}}" target="_blank">Follow</a></li>
    </ul>
    <li>Follow EXNCE official twitter account</li>
    <ul>
        <li><a href="https://twitter.com/exncecom" target="_blank">Follow</a></li>
    </ul>
    <li>Follow {{$airdrop->coin->symbol}} official twitter account</li>
    <ul>
        <li><a href="{{$twitterURL}}" target="_blank">Follow</a></li>
    </ul>
    <li>Retweet the activity Tweet, tag 3 friends</li>
    <ul>
        <li><a href="{{$twitterTweet}}" target="_blank">Retweet</a></li>
    </ul>
    <li>Join the Telegram group of EXNCE</li>
    <ul>
        <li><a href="https://t.me/exncecom" target="_blank">Join Now</a></li>
    </ul>
    <li>Join the Telegram group of  {{$airdrop->coin->symbol}}</li>
    <ul>
        <li><a href="{{$telegramURL}}" target="_blank">Join Now</a></li>
    </ul>

    <li>Subscribe the Youtube channel of EXNCE</li>
    <ul>
        <li><a href="https://www.youtube.com/channel/UCMYmuS7xYqQ8BggH1BMKIuw" target="_blank">Subscribe Now</a></li>
    </ul>

    <li>Like and write a comment to this video</li>
    <ul>
        <li><a href="{{$youtubeVideo}}" target="_blank">Write Now</a></li>
    </ul>

    <li>Like the Facebook page of EXNCE</li>
    <ul>
        <li><a href="https://www.facebook.com/exncecom" target="_blank">Like Now</a></li>
    </ul>

    <li class="text-danger">Join EXNCE on CoinChat</li>
    <ul>
        <li><a href="{{$coinchat}}" target="_blank">Join Now</a></li>
    </ul>

</ol>
