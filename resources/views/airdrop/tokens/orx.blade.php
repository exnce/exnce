@php
    $telegramURL = "https://t.me/oryxtoken";
    $twitterURL = "https://twitter.com/oryx_project";
    $youtubeVideo = "https://www.youtube.com/watch?v=BmbPawmWv0Q";
    $twitterTweet = "https://twitter.com/exncecom/status/1131347023154823168";
    $facebook = "https://www.facebook.com/oryxproject";
    $coinchat = "https://www.coinchat.im/candy/u_sRTlgmKpsdCBkrZycI8EQU5QeuS_CI0";
    $coinchat = "https://www.coinchat.im/candy/u_sRTlgmKpsdCBkrZycI8EQU5QeuS_CI0?fbclid=IwAR2pqV528wbm6X7yc1MM1rnhAjNgPzypIYJOBqKAaLO3GUAV98qHox0S-EQ";



@endphp

<h2 class="text-warning">About {{$airdrop->coin->name}}</h2>

<p>
    Oryx is a blockchain based E-Commerce platform that will combine more than one project within its economy and suitable for customers to buy and sell products, help to verify transactions with high accuracy, and the job will be updated based on the level of project development at each stage.
    <br>
    E-commerce in the Middle East is growing, there are increasing numbers of large players in this business, such as traders, brokers, banks, insurance companies and public institutions both national and on international firms.</p>

<h2>Step-by-step Guide:</h2>

<ol>
    <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
    <ul>
        <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
        <li><span class="text-warning">25 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>If you dont have an {{$airdrop->coin->symbol}}  wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
    <ul>
        <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
        <li><span class="text-warning">25 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow EXNCE official reddit account</li>
    <ul>
        <li><a href="https://www.reddit.com/user/exnce/comments/brwsph/exnce_1000000_orx_airdrop_has_launched/" target="_blank">Follow</a></li>
        <li><span class="text-warning">250 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow EXNCE official twitter account</li>
    <ul>
        <li><a href="https://twitter.com/exncecom" target="_blank">Follow</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow {{$airdrop->coin->symbol}} official twitter account</li>
    <ul>
        <li><a href="{{$twitterURL}}" target="_blank">Follow</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Retweet the activity Tweet, tag 3 friends</li>
    <ul>
        <li><a href="{{$twitterTweet}}" target="_blank">Retweet</a></li>
        <li><span class="text-warning">100 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Join the Telegram group of EXNCE</li>
    <ul>
        <li><a href="https://t.me/exncecom" target="_blank">Join Now</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Join the Telegram group of  {{$airdrop->coin->symbol}}</li>
    <ul>
        <li><a href="{{$telegramURL}}" target="_blank">Join Now</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li>Subscribe the Youtube channel of EXNCE</li>
    <ul>
        <li><a href="https://www.youtube.com/channel/UCMYmuS7xYqQ8BggH1BMKIuw" target="_blank">Subscribe Now</a></li>
        <li><span class="text-warning">200 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li>Like and write a comment to this video</li>
    <ul>
        <li><a href="{{$youtubeVideo}}" target="_blank">Write Now</a></li>
        <li><span class="text-warning">100 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li>Like the Facebook page of EXNCE</li>
    <ul>
        <li><a href="https://www.facebook.com/exncecom" target="_blank">Like Now</a></li>
        <li><span class="text-warning">200 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li class="text-danger">Join EXNCE on CoinChat</li>
    <ul>
        <li><a href="{{$coinchat}}" target="_blank">Join Now</a></li>
        <li><span class="text-warning">500 {{$airdrop->coin->symbol}}</span></li>
    </ul>

</ol>
