@php

    $telegramURL = $airdrop->coin->detail->telegram;
    $twitterURL = $airdrop->coin->detail->twitter;
    $youtubeVideo = $airdrop->coin->detail->youtube;
    $twitterTweet = "https://twitter.com/erenacar81/status/1141425086618116096";

@endphp


<div class="row">
    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">About {{$airdrop->coin->name}}
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectDescription" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectDescription_wrapper">
            <div class="row">
                <div class="col-12">
                    <p class="text-justify small-text p-3 card card-body">
                        {{$airdrop->coin->detail->description}}
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Presentation
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="presentation" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="presentation_wrapper">
            <div class="row">
                <div class="col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/idu4X5-Cd8o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Step by Step Guide
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectGuide" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectGuide_wrapper">
            <div class="row">
                <div class="col-12 pt-2">


                    <ol>
                        <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
                        <ul>
                            <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
                        </ul>
                        <li>If you dont have an {{$airdrop->coin->symbol}} and KZE  wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
                        <ul>
                            <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
                        </ul>

                        <li class="text-danger">Buy <b class="text-success">1000 Almeela(KZE)</b> from EXNCE IEO (10% DISCOUNTED for EXNCE users...)</li>
                        <ul>
                            <li><a href="{{route("ieopair",["KZE","ETH"])}}" target="_blank">Buy 1000 <b>KZE</b> from EXNCE IEO </a></li>
                            <li><span class="text-warning">1000 {{$airdrop->coin->symbol}}</span></li>
                        </ul>
                    </ol>

                </div>
            </div>

        </div>

    </div>



</div>



