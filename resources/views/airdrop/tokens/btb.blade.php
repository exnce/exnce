@php
    $telegramURL = "https://t.me/BitballGroupChat";
    $twitterURL = "https://twitter.com/BitBall_Erc20";
    $youtubeVideo = "https://www.youtube.com/channel/UCshc0oNpahxoulOhe5WwT-Q";
    $twitterTweet = "https://twitter.com/exncecom/status/1152675495663874048";
    $facebook = "https://www.facebook.com/exncecom/posts/1049236818605151";
    $coinchat = "https://www.coinchat.im/candy/u_sRTlgmKpsdCBkrZycI8EQU5QeuS_CI0";
    $exnce_reddit = "https://www.reddit.com/user/Bitball/comments/bvul9g/cryptocurrency_exchange_listing_no_2_we_are/";
    $trustpilot = "https://www.trustpilot.com/review/www.exnce.com";
    $linkedin = "https://www.linkedin.com/company/exnce";
    $medium = "https://medium.com/@exnce";
    $blockfolio = "https://feedback.blockfolio.com/exchange-requests/p/exnce";

@endphp

<div class="row">
    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">About {{$airdrop->coin->name}}
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectDescription" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectDescription_wrapper">
            <div class="row">
                <div class="col-12">
                    <p class="text-justify small-text p-3 card card-body">
                        {{$airdrop->coin->detail->description}}
                    </p>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Presentation
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="presentation" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="presentation_wrapper">
            <div class="row">
                <div class="col-12">
                    <iframe width="100%" height="315" src="https://www.youtube.com/embed/2Igc9xjVxBA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
    </div>


    <div class="col-12">
        <h6 class="card-title text-left" id="orderBookTitle">Step by Step Guide
            <span class="float-right">
                <ul class="orderbook-list-short">
                    <li>
                        <button type="button" data-object="projectGuide" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                    </li>
                </ul>

            </span>
        </h6>
        <div id="projectGuide_wrapper">
            <div class="row">
                <div class="col-12 pt-2">


                    <ol>
                        <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
                        <ul>
                            <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
                        </ul>
                        <li>If you dont have an {{$airdrop->coin->symbol}} wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
                        <ul>
                            <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
                        </ul>



                        <li>Write a review on BlockFolio about EXNCE</li>
                        <ul>
                            <li><a href="{{$blockfolio}}" target="_blank">Write a review <span class="fav-btn filter checked"></span></a></li>
                        </ul>


                        <li>Retweet the activity Tweet, tag <b>5</b> friends</li>
                        <ul>
                            <li><a href="{{$twitterTweet}}" target="_blank">Retweet</a></li>
                        </ul>
                        <li>Join the Telegram group of EXNCE and {{$airdrop->coin->symbol}}</li>
                        <ul>
                            <li><a href="https://t.me/exncecom" target="_blank">Join Now</a></li>
                            <li><a href="{{$telegramURL}}" target="_blank">Join Now</a></li>
                        </ul>



                    </ol>

                </div>
            </div>

        </div>

    </div>



</div>


