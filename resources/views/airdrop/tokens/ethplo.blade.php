@php

    $telegramURL = "https://t.me/ETHplode";
    $twitterURL = "https://twitter.com/ETHplode";
    $youtubeVideo = "https://www.youtube.com/watch?v=x1YREfVTGj4";
    $twitterTweet = "https://twitter.com/exncecom/status/1126496393072300033";

@endphp

<h2 class="text-warning">About {{$airdrop->coin->name}}</h2>

<p>A deflationary decentralized store of value based on the Ethereum blockchain. Whenever $ETHPLO is transferred, 0.5% of the amount will be burned.</p>

<h2>Step-by-step Guide:</h2>

<ol>
    <li>If you dont have an "EXNCE" account, register an "EXNCE" account.</li>
    <ul>
        <li><a href="{{route("register")}}" target="_blank">Register Now</a></li>
        <li><span class="text-warning">25 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>If you dont have an {{$airdrop->coin->symbol}}  wallet on your "EXNCE" account, create {{$airdrop->coin->symbol}} wallet on your account.</li>
    <ul>
        <li><a href="{{route("wallet")}}" target="_blank">Create {{$airdrop->coin->symbol}}  Wallet Now</a></li>
        <li><span class="text-warning">25 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow EXNCE official reddit account</li>
    <ul>
        <li><a href="https://www.reddit.com/user/exnce" target="_blank">Follow</a></li>
        <li><span class="text-warning">250 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow EXNCE official twitter account</li>
    <ul>
        <li><a href="https://twitter.com/exncecom" target="_blank">Follow</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Follow {{$airdrop->coin->symbol}} official twitter account</li>
    <ul>
        <li><a href="{{$twitterURL}}" target="_blank">Follow</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Retweet the activity Tweet, tag 3 friends</li>
    <ul>
        <li><a href="{{$twitterTweet}}" target="_blank">Retweet</a></li>
        <li><span class="text-warning">100 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Join the Telegram group of EXNCE</li>
    <ul>
        <li><a href="https://t.me/exncecom" target="_blank">Join Now</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>
    <li>Join the Telegram group of  {{$airdrop->coin->symbol}}</li>
    <ul>
        <li><a href="{{$telegramURL}}" target="_blank">Join Now</a></li>
        <li><span class="text-warning">150 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li>Subscribe the Youtube channel of EXNCE</li>
    <ul>
        <li><a href="https://www.youtube.com/channel/UCMYmuS7xYqQ8BggH1BMKIuw" target="_blank">Subscribe Now</a></li>
        <li><span class="text-warning">200 {{$airdrop->coin->symbol}}</span></li>
    </ul>

    <li>Like and write a comment to this video</li>
    <ul>
        <li><a href="{{$youtubeVideo}}" target="_blank">Write Now</a></li>
        <li><span class="text-warning">100 {{$airdrop->coin->symbol}}</span></li>
    </ul>


    <li>Like the Facebook page of EXNCE</li>
    <ul>
        <li><a href="https://www.facebook.com/exncecom" target="_blank">Like Now</a></li>
        <li><span class="text-warning">200 {{$airdrop->coin->symbol}}</span></li>
    </ul>


</ol>
