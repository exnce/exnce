@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("Contact"))
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section('content')

    <section class="container-fluid mb-5">
    <div class="row justify-content-center">
        <div class="col-lg-12">
            <div class="card rounded  box-shadow border-0 pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5>{{ _i('Contact') }}</h5></div>

                <div class="card-body">

                    <form name="contactForm" method="post" action="{{route("contact")}}">
                        @csrf
                        <div class="row">
                            <div class="col-12">
                                @if($errors->has("fname") || $errors->has("lname") || $errors->has("email") || $errors->has("message")  || $errors->has("g-recaptcha-response")  )
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="alert-heading">{{_i("Error!")}}</h4>
                                        <hr>
                                        <p class="mb-0">{{_i("Please fill in all required fields...")}}</p>
                                    </div>
                                @endif
                                @if(session("is_success") )
                                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h4 class="alert-heading">{{_i("Congratulations!")}}</h4>
                                        <hr>
                                        <p class="mb-0">{{_i("Your message has been received. You will be given a short turn-around time. We thank you.")}}</p>
                                    </div>
                                @endif
                            </div>
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-12">
                                        <h6 class="card-title text-left" id="orderBookTitle">Address
                                            <span class="float-right">
                                            <ul class="orderbook-list-short">
                                            <li>
                                            <button type="button" data-object="projectDescription" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                                            </li>
                                            </ul>
                                            </span>
                                        </h6>
                                        <div id="projectDescription_wrapper">
                                            <div class="row">
                                                <div class="col-12">


                                                    <table class="table table-striped w-100">
                                                        <tbody>
                                                        <tr>
                                                            <td style="width: 20%">Title:</td>
                                                            <td>SEM MEDIA OÜ</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Address:</td>
                                                            <td>Aadress on Harju maakond, Tallinn, Kesklinna linnaosa, Tina tn 21-5, 10126</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tax Office:</td>
                                                            <td>Estonia</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Tax ID:</td>
                                                            <td>+372 5787 1481</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Email:</td>
                                                            <td>support@exnce.com</td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <h6 class="card-title text-left" id="orderBookTitle">Map
                                            <span class="float-right">
                                            <ul class="orderbook-list-short">
                                            <li>
                                            <button type="button" data-object="map" class="btnExpandTable btn btn-sm btn-outline-light text-warning"><i class="fa fa-angle-double-down"></i></button>
                                            </li>
                                            </ul>
                                            </span>
                                        </h6>
                                        <div id="map_wrapper">
                                            <div class="row">
                                                <div class="col-12">
                                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d4057.6580569627577!2d24.770304!3d59.435926!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469293594f1632dd%3A0xb3c5503ce6d4a830!2sTina+21%2C+10125+Tallinn%2C+Estonia!5e0!3m2!1sen!2shk!4v1561478656411!5m2!1sen!2shk" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="col-md-6">
                                <div class="contact-form">




                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="fname">{{_i("Name")}}</label>
                                        <div class="col-sm-12">
                                            <input type="text" value="{{Auth::check() ? splitName(Auth::user()->name)["firstname"] : ""}}" class="form-control" id="fname" placeholder="{{ _i("Please enter your name")}}" name="fname">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="lname">{{_i("Lastname")}}</label>
                                        <div class="col-sm-12">
                                            <input type="text" class="form-control" id="lname" value="{{ Auth::check() ? splitName(Auth::user()->name)["lastname"] : ""  }}" placeholder="{{_i("Please enter your last name")}}" name="lname">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="email">{{_i("E-Mail")}}</label>
                                        <div class="col-sm-12">
                                            <input type="email" class="form-control" value="{{Auth::check() ? Auth::user()->email : ""}}" id="email" placeholder="{{_i("Please enter your e-mail address")}}" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-sm-2" for="comment">{{_i("Description")}}</label>
                                        <div class="col-sm-12">
                                            <textarea class="form-control" rows="5" id="message" name="message"></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-12">
                                            <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"  data-callback="onSubmit"></div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-12">
                                            <button type="submit" class="btn btn-success">{{_i("Send")}}</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                   </form>
                </div>



            </div>
        </div>
    </div>
    </section>





@endsection


