@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("About"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">

            <div class="col-12">


                <div class="row pb-4">
                    <div class="card card rounded box-shadow pt-3 px-3 py-3 w-100">
                        <div class="card-header" id="cryptoCurrencyCard">
                            <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                               aria-controls="collapseCrypto">
                                <h5 class="mb-0">{{ _i("About") }}<span class="pull-right"><i
                                                class="fa fa-angle-double-down"></i></span></h5>
                                <span class="d-none d-lg-block d-md-block">{{ _i("Important information about exnce.com") }}</span>
                            </a>
                        </div>
                        <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                            <div class="card-body pl-0 pr-0 pb-0">
                                <div class="row">

                                    <div class="col-lg-12 col-sm-12 m-0 mb-4">


                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                    <img width="128px" height="128px" src="{{asset("/images/logo_icon.svg")}}"/>
                                                    <p class="text-justify">
                                                        {{_i("EXNCE, is a semi-decentralized digital asset airdrop distribution service which supports new and established digital currencies on a convenient and seamless platform through which anyone can join, distribute airdrop crypto currencies.")}}</p>
                                                    <p class="text-justify">
                                                        {{_i("EXNCE, provides a streamlined trading process on a simplified and intuitive interface for a more coordinated and rewarding experience while exceeding the limits of what a digital asset airdrop distribution can be.")}}</p>
                                                    <p class="text-justify">
                                                        {{_i("EXNCE, offers a concise and simplified portal for anyone to airdrop distribution of crypto currency, thereby, strategically identifying and bringing together standard blockchain organizations and enthusiastic investors to share ideas and values. Hence, it creates a dynamic community that continuously explores and fuels blockchain innovation.")}}</p>
                                                    <h1>{{_i("Our Advantages")}}</h1>
                                                    <hr/>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-up" class="aos-init aos-animate">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/settings-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("High Performance")}}</p>
                                                            <p class="feature-description">{{_i("High-performance matching engine, capable of fast processing 20,000 orders per second")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-up" class="aos-init aos-animate">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/transfer-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("Safe and Stable Trading Environment")}}</p>
                                                            <p class="feature-description">{{_i("Open and transparent market information with providing safe and stable trading environment and Asset Security Management Solutions.")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-up" class="aos-init aos-animate">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/security-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("Safety Stability")}}</p>
                                                            <p class="feature-description">{{_i("Our technical team have rich experience in the crypto industry. Thanks to their hard work, Exnce has built an industry-leading risk management system based on distributed architecture as well as a powerful anti-DDOS system. ")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-down" class="aos-init">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/mobile-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("Excellent User Experience")}}</p>
                                                            <p class="feature-description">{{_i("To better serve our users, Exnce supports be access from multiple platforms including IOS, Android, H5 and PC Web.")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-down" class="aos-init">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/vendor-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("Multiple-token Support")}}</p>
                                                            <p class="feature-description">{{_i("Support multiple coins trading and high liquid markets with 30+ trading pairs, more premiere tokens coming up")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div data-aos="fade-down" class="aos-init">
                                                        <div class="text-center">
                                                            <img src="{{asset("images/buy-icon.png")}}" class="img-fluid" alt="">
                                                            <p class="feature-title font-weight-bold mb-0">{{_i("Bonus Sharing")}}</p>
                                                            <p class="feature-description">{{_i("We share most of our profits with our partners and users. We believe that helping our users to Blockchain technology, is more important than just profiting ourselves.")}}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    </div>
@endsection
