
@extends('layouts.site')
@php
    $symbol = "{$activeCurrency->symbol}-{$activeCoin->symbol}";
    $selectedPair = "{$activeCurrency->symbol}/{$activeCoin->symbol}";
@endphp
@section("title",_i($selectedPair))
@section('content')

    <div class="container-fluid py-2 bg-gradient-gray">

        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 mb-3 p-1 order-lg-1 order-sm-2 order-xs-2 order-1">

                <div class="card rounded border-0 box-shadow d-none d-sm-block">
                    <div class="card-body pt-1 px-0 pb-0">
                        <div id="chart-section">
                            <iframe id="tradingview_2975a" name="tradingview_2975a"
                                    src="/chart.html?symbol={{ $symbol }}{{ env("APP_ENV") == 'local' ? "&dev=1" : "" }}"
                                    frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen=""
                                    style="display: block; width: 100%; height: 395px;"></iframe>
                        </div>
                    </div>
                </div>

                <div class="card rounded border-0 box-shadow mt-1">
                    <div class="card-body pt-1 px-0 pb-0 text-center">
                        <div class="nonuser">
                            <span>{{_i("Lütfen")}}</span>&nbsp;
                            <a href="{{route("login")}}" class="login">
                                {{_i("Giriş yapın")}}
                            </a>&nbsp;
                            {{_i("veya")}}
                            &nbsp;
                            <a href="{{route("register")}}" class="register">{{_i("Kayıt olun")}}</a>
                         </div>

                        <div class="row mt-2">

                            <div class="col-12 col-sm-12 col-lg-6 pr-lg-0">
                                <div class="market-buy">
                                    <div class="form-group hidden">
                                        <small class="font-weight-bold">Currency:</small>
                                        <input type="text" min="0" id="edtBuyCurrency" name="edtBuyCurrency"
                                               class="form-control" value="{{$activeCoin->symbol}}">
                                        <small class="font-weight-bold">Coin:</small>
                                        <input type="text" min="0" id="edtBuyCoin" name="edtBuyCoin"
                                               class="form-control" value="{{$activeCurrency->symbol}}">
                                    </div>

                                    <div class="mb-2 text-left">
                                        <small class="ml-3">{{_i("Bakiyeniz: ")}}</small>
                                        <span id="coinBalance">0.00</span>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" id="edtPriceBuy" name="edtPriceBuy"
                                               class="ml-3 form-control text-right currency-input border-right-0">

                                        <div class="input-group-addon currency-addon text-white bg-gradient-green border-0 mr-3">
                                            <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                        </div>

                                        <label class="innerLabel">{{ _i("Fiyat") }}</label>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" min="0" id="edtAmountBuyInt" name="edtAmountBuyInt"
                                               class="ml-3 text-right form-control currency-input border-right-0">
                                        <button tabindex="-1" type="button" id="btnIncrementBuyAmount"
                                                name="btnIncrementBuyAmount"
                                                class="bg-transparent pointer border border-left-0 border-right-1 text-dark-bluish">
                                            <i class="fa fa-plus-circle text-dark-bluish" aria-hidden="true"></i>
                                        </button>
                                        <div class="input-group-addon currency-addon text-white bg-gradient-green border-0 mr-3">
                                            <div class="marketSellCoinSymbol mt-2 small-text">{{ $activeCurrency->symbol }}</div>
                                        </div>
                                        <label class="innerLabel">{{ _i("Miktar") }}</label>
                                    </div>

                                    <div class="input-group mb-2">
                                        <div class="ratio-content">
                                            <div class="ratio-item ml-3" data-val=".25">25%</div>
                                            <div class="ratio-item" data-val=".50">50%</div>
                                            <div class="ratio-item" data-val=".75">75%</div>
                                            <div class="ratio-item" data-val="1">100%</div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" id="edtSummaryBuy" name="edtSummaryBuy"
                                               class="ml-3 form-control text-right currency-input border-right-0">
                                        </button>
                                        <div class="input-group-addon currency-addon text-white bg-gradient-green border-0 mr-3">
                                            <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                        </div>
                                        <label class="innerLabel">{{ _i("Toplam") }}</label>
                                    </div>
                                    <div class="hidden">
                                        <div class="text-dark-bluish text-left mt-2 ml-3">
                                            <small class="font-weight-bold">{{_i("İşlem Ucreti")}}</small>
                                        </div>
                                        <div class="input-group mb-2">
                                            <input tabindex="-1" type="text" id="edtFeeBuy" name="edtFeeBuy"
                                                   class="ml-3 form-control text-right currency-input border-right-0"
                                                   readonly="true" aria-label="{{_i("İşlem Ucreti")}}"
                                                   value="0.00"/>
                                            <div class="input-group-addon currency-addon text-white bg-gradient-green border-0 mr-3">
                                                <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group pl-3 pr-3 mt-3">
                                        <button type="button" id="" name="btnMarketBuy"
                                                class="marketBtnBuyNow btn btn-block bg-gradient-green font-weight-bold px-4 text-white">{{ _i("AL") }}</button>
                                    </div>
                                    <div class="pl-3 pr-3 mt-3 text-right">
                                        <small>{{_i("İşlem Ücreti (%s%s): ", "%", ($activeCurrency->processing_fee*100))}}</small>
                                        <small id="divFeeBuy">-</small>
                                        <small id="feeCurrencyLabel">{{ $activeCurrency->symbol }}</small>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-sm-12 col-lg-6 pl-lg-0">
                                <div class="market-sell">
                                    <div class="d-lg-none d-md-none w-100 seperator my-2"></div>


                                    <div class="form-group hidden">
                                        <input type="hidden" id="edtSellCurrency" name="edtSellCurrency"
                                               value="{{$activeCoin->symbol}}">
                                        <input type="hidden" id="edtSellCoin" name="edtSellCoin"
                                               value="{{$activeCurrency->symbol}}">
                                    </div>

                                    <div class="mb-2 text-left">
                                        <small class="ml-3 ml-lg-0">{{_i("Bakiyeniz: ")}}</small>
                                        <span id="currencyBalance">0.00</span>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" id="edtPriceSell" name="edtPriceSell"
                                               class="ml-3 ml-lg-0 form-control text-right currency-input border-right-0">
                                        <div class="input-group-addon currency-addon text-white bg-gradient-red border-0 mr-3">
                                            <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                        </div>
                                        <label class="innerLabel">{{ _i("Fiyat") }}</label>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" min="0" id="edtAmountSellInt" name="edtAmountSellInt"
                                               class="ml-3 ml-lg-0 text-right form-control currency-input border-right-0">
                                        <button type="button" tabindex="-1" id="btnIncrementSellAmount"
                                                name="btnIncrementSellAmount"
                                                class="bg-transparent pointer border border-left-0 border-right-1 text-dark-bluish">
                                            <i class="fa fa-plus-circle text-dark-bluish" aria-hidden="true"></i>
                                        </button>
                                        <div class="input-group-addon currency-addon text-white bg-gradient-red border-0 mr-3">
                                            <div class="marketSellCoinSymbol mt-2 small-text">{{ $activeCurrency->symbol }}</div>
                                        </div>
                                        <label class="innerLabel">{{ _i("Miktar") }}</label>
                                    </div>

                                    <div class="input-group mb-2">
                                        <div class="ratio-content">
                                            <div class="ratio-item ml-3 ml-lg-0" data-val=".25">25%</div>
                                            <div class="ratio-item" data-val=".50">50%</div>
                                            <div class="ratio-item" data-val=".75">75%</div>
                                            <div class="ratio-item" data-val="1">100%</div>
                                            <div class="clear"></div>
                                        </div>
                                    </div>

                                    <div class="input-group mb-2">
                                        <input type="text" id="edtSummarySell" name="edtSummarySell"
                                               class="ml-3 ml-lg-0 form-control text-right currency-input border-right-0">
                                        </button>
                                        <div class="input-group-addon currency-addon text-white bg-gradient-red border-0 mr-3">
                                            <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                        </div>
                                        <label class="innerLabel">{{ _i("Toplam") }}</label>
                                    </div>

                                    <div class="hidden">
                                        <div class="text-dark-bluish text-left mt-2 ml-3">
                                            <small class="font-weight-bold">{{_i("İşlem Ücreti")}}</small>
                                        </div>
                                        <div class="input-group mb-2">
                                            <input tabindex="-1" type="text" id="edtFeeSell" name="edtFeeSell"
                                                   class="ml-3 ml-lg-0 form-control text-right currency-input border-right-0"
                                                   readonly="true" aria-label="{{_i("İşlem Ücreti")}}"
                                                   value="0.00"/>
                                            <div class="input-group-addon currency-addon text-white bg-gradient-red border-0 mr-3">
                                                <div class="mt-2 small-text">{{$activeCoin->symbol}}</div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="form-group pl-3 pl-lg-0 pr-3 mb-3 mt-3">
                                        <button type="button" id="" name="btnMarketSell"
                                                class="marketBtnSellNow btn btn-block bg-gradient-red font-weight-bold px-4 text-white">{{ _i("SAT") }}</button>
                                    </div>
                                    <div class="pl-sm-3 pl-lg-0 pr-3 mb-3 mt-3 text-right">
                                        <small>{{_i("İşlem Ücreti (%s%s): ", "%", ($activeCoin->processing_fee*100))}}</small>
                                        <small id="divFeeSell">-</small>
                                        <small id="feeCurrencyLabel">{{ $activeCoin->symbol }}</small>
                                    </div>

                                </div>
                            </div>
                            <input type="hidden" id="coinFee" value="{{ floatval($activeCoin->processing_fee) }}">
                            <input type="hidden" id="currencyFee"
                                   value="{{ floatval($activeCurrency->processing_fee) }}">
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 mb-3 p-1 order-lg-2 order-sm-1 order-xs-1 order-2">
                <div class="card rounded border-0 box-shadow p-0">
                    <div class="card-body pt-0 pb-0 px-0">
                        <h6 class="card-title text-left" id="orderBookTitle">{{_i("Emir Defteri")}}
                            <span class="pull-right">

                            <ul class="orderbook-list-short">
                                <li class="">
                                    <input type="image" class="orderbook-short" data-filter="0" alt="orderbook-buysell" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAARCAYAAADdRIy+AAABF0lEQVQ4jaXUoUvDQRjG8c+m4MAgGoXhQBAMBpt/gMUoBq2KYBNhIIpZEcE8ZMjqgibLqqJZ25JgEDE5FsSB6Aze4NhUfvv5wHHPexzfe7h7ucxToSjSeZiXpNRgV/2WFvQTcBgLkc+j/h/gMq4jP44S9tIC17GPTIDksHmxO9VAux/gS/CvqAVfwgBuQp3pBzgZ/Ds+gq9gFJ+hTpwws1XtXdzeKU6gcHR4fDndPEnK+h0YKwATJ8yGzW3MRus1nMUHJx1ZjOEAaxGgieeobicdWTRQxorvVhnCPObSJOz04QNusRhOusMM7usjG39dWY/ixq5gNfgyrkLivhS/cg6PIWEerX5h3QlbqEY+lbq/r9O0oI6+AFb5Q71drBHQAAAAAElFTkSuQmCC" selected="">
                                </li>
                                <li class="">
                                    <input type="image" class="orderbook-short" data-filter="1" alt="orderbook-buy" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAPCAYAAADkmO9VAAAAyklEQVQ4jcXSMUoDQRTG8d8sAdMJ6XOAFBZ2HiAHsLROY5tcInfYDWIbwQOk9gKxsxIsLOxCuu02RXbDsGiYLAt+8OC9Yfje/72ZkOd5pUcNEPo0DFV1HrAoCkieYpDauG/DdMLFy/nmEzn/SZjVlyvcRucbvEZ1SI0MIywxiwz2+GkRJkWGHVZ4wBBXmOKuC2Gzwy9scV/X77jB58f145/7+k3xozxHY6/wVhNfpDBfn/Ihvh13MUZ5qVmbsMQ6yjup/Q+fuho1OgDrcC9Xcb+0AgAAAABJRU5ErkJggg==">
                                </li>
                                <li class="">
                                    <input type="image" class="orderbook-short" data-filter="2" alt="orderbook-sell" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAANCAYAAACpUE5eAAAAsUlEQVQ4jaWQMQoCMRBFX9BCsLIVhAXBu1hbeAFrGyux9gwLAfEEVjYeQM9gvY2lW4mFaGxGCEuUmfgh5P8Q3nzGXYoFkXZyT8hUu5HvuaAUsAuMIz8Azv8Ap8Ax8n2gBFa5wBmwBpxAOsB8vxzVQLAAr+JvwEF8CbSAk2RnAQ7FP4Cn+C3QA16STQ3rxPsGKKJsaphSJecjdUMXwu+/3nstC/jesCn9DpUNTDtUT9foDaE9InSwV1aPAAAAAElFTkSuQmCC">
                                </li>
                            </ul>

                        </span>
                        </h6>

                        <table class="table currency-table orderbook mb-0">
                            <thead class="table-head text-center">
                            <tr>
                                <th class="border-0 text-left"><span
                                            class="d-block">{{ _i("Fiyat") }} ({{$activeCoin->symbol}})</span></th>
                                <th class="border-0 text-right pr-1"><span
                                            class="d-block">{{ _i("Miktar") }} ({{$activeCurrency->symbol}})</span></th>
                                <th class="border-0 text-right"><span
                                            class="d-block">{{ _i("Toplam") }} ({{ $activeCoin->symbol }})</span></th>
                            </tr>
                            </thead>
                        </table>

                        <div class="table-wrapper orderbook-panels">
                            <table class="table table-hover currency-table orderbook sell">
                                <tbody class="orderbookSellTable">
                                </tbody>
                            </table>
                        </div>

                        <div class="lastPrice"><span class="price">0.00</span> <i class="fa fa-arrow-up arrow"></i>
                        </div>

                        <div class="table-wrapper orderbook-panels">
                            <table class="table table-hover currency-table orderbook buy">
                                <tbody class="orderbookBuyTable">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-12 col-sm-12 mb-3 p-1 order-3">

                <div class="card rounded border-0 box-shadow p-0">
                    <div class="card-body pt-0 pb-0 px-0">
                        <div class="market-section">
                            <div class="currency-tabs">
                                @foreach ($baseCoins as $coin_id => $baseCoin)
                                    <div class="tab-item {{ ( $activeCoin->id == $coin_id ? "active" : "" ) }}"
                                         data-coinid="{{ $coin_id }}">
                                        <span class="tab-item-text">{{ $baseCoin }}</span>
                                    </div>
                                @endforeach
                            </div>

                            <div>
                                <table class="table currency-table mb-0">
                                    <thead class="table-head text-center">
                                    <tr>
                                        <th class="border-0 text-left"><span class="fav-btn filter"></span></th>
                                        <th class="sorting border-0 text-left pl-1">{{ _i("Sembol") }}</th>
                                        <th class="border-0 text-left pl-1">{{ _i("Fiyat") }}
                                            {{--<svg class="svg-inline--fa fa-sort fa-w-10" aria-hidden="true" data-prefix="fa" data-icon="sort" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512" data-fa-i2svg=""><path fill="currentColor" d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41zm255-105L177 64c-9.4-9.4-24.6-9.4-33.9 0L24 183c-15.1 15.1-4.4 41 17 41h238c21.4 0 32.1-25.9 17-41z"></path></svg>--}}
                                        </th>
                                        <th class="border-0 text-right pr-1">{{ _i("Hacim") }}</th>
                                        <th class="border-0 text-right pr-1">{{ _i("Değişim") }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>

                            <div class="table-wrapper for-coinsTable">
                                <table class="table table-hover currency-table">
                                    <tbody class="coinsTable">
                                    @foreach ($pairList as $pair)
                                        @php
                                            $strPair = $pair["currency"] . "-" . $pair["coin"];
                                            $activeClass = $pair["coin_id"] == $activeCoin->id && $pair["currency_id"] == $activeCurrency->id ? "active" : "";

                                            if($pair["change"] > 0) {
                                                $changeClass = "text-success";
                                            } elseif($pair["change"] < 0) {
                                                $changeClass = "text-danger";
                                            } else {
                                                $changeClass = "";
                                            }

                                        @endphp
                                        <tr class="pair" data-coinid="{{ $pair["coin_id"] }}"
                                            data-currency="{{ $pair["currency_id"] }}"
                                            data-pair="{{ $strPair }}"
                                            style="{{ ( $activeCoin->id != $pair["coin_id"] ? "display:none;" : "" ) }}">

                                            <td class="{{ $activeClass }}"><span
                                                        class="fav-btn {{ in_array($strPair, $favorites) ? "checked" : "" }}"></span>
                                            </td>
                                            <td>
                                                <img src="{{asset("/images/".strtolower($pair["slug"]).".svg")}}"
                                                     width="16px"> {{ $pair["currency"] }}</td>
                                            <td id="lastprice">{{ $pair["lastprice"]  }}</td>
                                            <td id="volume" class="text-right">{{ $pair["volume"]  }}</td>
                                            <td id="change" class="text-right {{ $changeClass }}">{{ $pair["change"]  }}%
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card rounded border-0 box-shadow p-0 mt-1 orderHistoryCard">
                    <div class="card-body pt-0 pb-0 px-0">
                        <h6 class="card-title text-left" id="orderHistoryTitle">{{_i("İşlem Geçmişi")}}</h6>

                        <table class="table currency-table mb-0">
                            <thead class="table-head text-center">
                            <tr>
                                <th class="border-0 text-left">{{ _i("Tarih") }}</th>
                                <th class="border-0  text-left pl-1"><span
                                            class="d-block">{{ _i("Miktar") }} ({{$activeCurrency->symbol}})</span></th>
                                <th class="border-0 text-right px-0 pr-1"><span
                                            class="d-block">{{ _i("Fiyat") }} ({{$activeCoin->symbol}})</span></th>
                            </tr>
                            </thead>
                        </table>

                        <div class="table-wrapper for-marketOrderHistory">
                            <table class="table table-hover currency-table">
                                <tbody class="marketOrderHistory">
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>

            </div>
        </div>

    </div>

@endsection


@section('script')
    <script>
        TSite.CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        TSite.MarketEndPointURI = "/market/load";
        TSite.pairEndPointURI = "/market/";

        var user_hash = "{{ auth()->user() ? md5(md5(auth()->user()->id)) : "" }}";

        var selectedCoin = {{$activeCoin->id}};
        var selectedCurrency = {{$activeCurrency->id}};

        TSite.setCoinID(selectedCoin);
        TSite.setCurrencyID(selectedCurrency);
        TSite.selectedTabID = selectedCoin;

        var EXCHANGE_SOCKET_ADDRESS = "{{ env("EXCHANGE_SOCKET_LOCAL_MODE") ? env("EXCHANGE_SOCKET_SERVER_LOCAL") : env("EXCHANGE_SOCKET_SERVER_REMOTE") }}";
        var socket = io.connect(EXCHANGE_SOCKET_ADDRESS, {secure: true});
        socket.on('connect', function () {
            console.log("connected");
            TSite.loadMarketData();
        });

        socket.on('message', function (message) {
            message = JSON.parse(message);

            if (typeof message == 'undefined') {
                return;
            }

            if ((typeof message.coin.id != 'undefined' && message.coin.id == selectedCoin) &&
                (typeof message.currency.id != 'undefined' && message.currency.id == selectedCurrency)) {
                TSite.fillOrderBookTable(message.buy, "buy", "orderbookBuyTable");
                TSite.fillOrderBookTable(message.sell, "sell", "orderbookSellTable");

                TSite.fillCoinsTable(message.pairsData);
                document.title = "{{ $selectedPair }} " + message.pairsData["{{ $selectedPair }}"].lastprice + " | EXNCE";

                $(".lastPrice .price").text(0);
                $(".lastPrice").removeClass("up down");
                if (typeof message.market_history != 'undefined') {
                    TSite.fillOrderHistoryTable(message.market_history);

                    if (typeof message.market_history[0] != 'undefined') {
                        $(".lastPrice .price").text(message.market_history[0].price);
                        if (message.market_history[0].color == 1) {
                            $(".lastPrice").addClass("up");
                        } else if (message.market_history[0].color == 2) {
                            $(".lastPrice").addClass("down");
                        }
                    }
                }
            }

            TSite.updateVolumes(message.volumes);

        });

    </script>
@endsection
