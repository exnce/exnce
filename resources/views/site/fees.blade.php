@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("Fees"))
@section('content')
    <section id="panel">
        <div class="container-fluid">
            <div class="row">

                <div class="col-12 mb-3">
                    <div class="card card rounded  box-shadow pt-3 px-3 py-3 pb-3">
                        <div class="card-header" id="cryptoCurrencyCard">
                            <a data-toggle="collapse">
                                <h5 class="mb-0">{{ _i("What does Taker and Maker mean?") }}<span class="pull-right"><i
                                                class="fa fa-angle-double-down"></i></span></h5>
                            </a>
                        </div>
                            <div class="card-body pl-0 pr-0 pb-0 pt-2">
                                <p class="text-justify">
                                    {{_i(" Every trade occurs between two parties: the maker, whose order exists on the order book prior to the trade, and the taker, who places the order that matches (or \"takes\") the maker's order. Makers are so named because their orders make the liquidity in a market. Takers are the ones who remove this liquidity by matching makers' orders with their own.")}}
                                </p>

                                <p class="alert alert-success w-100">
                                     {{_i("Deposit of all crypto currencies is free of charge.")}}
                                </p>

                                <p class="alert alert-warning w-100">
{{_i("We have no upper limitations for Deposit & Withdrawals.")}}
                                </p>
                            </div>
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 px-0">

                    <div class="col">
                        <div class="pb-4" id="cryptoCurrency">
                            <div class="card card rounded box-shadow pt-3 px-3 py-3">
                                <div class="card-header" id="cryptoCurrencyCard">
                                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                                       aria-controls="collapseCrypto">
                                        <h5 class="mb-0">{{ _i("Trading fees") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                    </a>
                                </div>
                                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0 pt-0">

                                        <div class="row">


                                            <div class="col-12 scrollableTable">
                                                <table id="lstDepositCoinList" name="lstDepositCoinList"
                                                       class="table display nowrap table-hover dataTable table-responsive no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
                                                    <thead class="table-head border-0">
                                                    <tr>
                                                        <th>{{_i("Market")}}</th>
                                                        <th class="text-right">{{_i("Maker")}}</th>
                                                        <th class="text-right">{{_i("Taker")}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($pairs as $pair)
                                                        <tr>
                                                            <td><b>{{$pair["name"]}}</b></td>
                                                            <td class="vertical-middle text-right">{{$pair["maker_fee"]}}</td>
                                                            <td class="vertical-middle text-right">{{$pair["taker_fee"]}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>



                                        </div>



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 px-0">
                    <div class="col pl-lg-0">
                        <div class="pb-4" id="depositHistory">
                            <div class="card card rounded box-shadow pt-3 px-3 py-3">
                                <div class="card-header" id="depositHistoryCard">
                                    <a data-toggle="collapse" data-target="#collapseDepositHistory" aria-expanded="true"
                                       aria-controls="collapseDepositHistory">
                                        <h5 class="mb-0">{{ _i("Withdrawal fees") }}<span class="pull-right"><i
                                                        class="fa fa-angle-double-down"></i></span></h5>
                                    </a>
                                </div>
                                <div id="collapseDepositHistory" class="collapse show" aria-labelledby="headingOne">
                                    <div class="card-body pl-0 pr-0 pb-0 pt-0">
                                        <div class="row">
                                            <div class="col-12">

                                                <table id="lstDepositCoinList" name="lstDepositCoinList"
                                                       class="table display nowrap table-hover dataTable table-responsive no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
                                                    <thead class="table-head border-0">
                                                    <tr>
                                                        <th>{{_i("Asset")}}</th>
                                                        <th class="text-right">{{_i("Minimum Withdrawal")}}</th>
                                                        <th class="text-right">{{_i("Withdrawal fee")}}</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($coins as $coin)
                                                        <tr>
                                                            <td><b>{{$coin->name}}</b> <small class="text-muted">{{$coin->symbol}}</small></td>
                                                            <td class="vertical-middle text-right">{{$coin->min_withdraw}}</td>
                                                            <td class="vertical-middle text-right">{{$coin->fee}} {{$coin->symbol}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </section>
@endsection
