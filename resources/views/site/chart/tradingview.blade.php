<iframe rel="nofollow" id="tradeChart" name="tradeChart"
        class="tradingViewHome"
        src="/chart.html?lang={{app()->getLocale()}}&symbol={{ $symbol }}{{ env("APP_ENV") == 'local' ? "&dev=1" : "" }}"
        frameborder="0" allowtransparency="true" scrolling="no" allowfullscreen=""></iframe>
