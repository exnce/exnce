@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("System Status"))


@section("content")
    <div class="container pt-3">
        <div class="row mx-auto">

            <div class="col-12">


                <div class="row pb-4">
                    <div class="card card rounded box-shadow pt-3 px-3 py-3 w-100">
                        <div class="card-header" id="cryptoCurrencyCard">
                            <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                               aria-controls="collapseCrypto">
                                <h5 class="mb-0">{{ _i("System Status") }}<span class="pull-right"><i
                                                class="fa fa-angle-double-down"></i></span></h5>
                                <span class="d-none d-lg-block d-md-block">{{ _i("Asset Count : ") }} <b class="text-danger">{{$coins->count()}}</b></span>
                            </a>
                        </div>
                        <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                            <div class="card-body pl-0 pr-0 pb-0">
                                <div class="row">

                                    <div class="col-lg-12 col-sm-12 m-0 mb-4">


                                        <div class="container">
                                            <div class="row">
                                                <div class="col-12 text-center">
                                                     <p class="text-justify">
                                                        {{_i("The System Status page is a monitoring tool designed by our team that provides near real-time information (updates approximately every 5 minutes) about availability and performance, as well as incoming and outgoing transaction status. This is an evolving tool that will include more information on the system performance, aiming at making our system processes as transparent as possible.")}}</p>
                                                    <hr/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12">

                                                    <table id="lstDepositCoinList" name="lstDepositCoinList"
                                                           class="table display nowrap table-hover dataTable table-responsive no-footer" style="100%;margin-top:0px;" cellspacing="0" role="grid">
                                                        <thead class="table-head border-0">
                                                        <tr>
                                                            <th>{{_i("Asset")}}</th>
                                                            <th>{{_i("Symbol")}}</th>
                                                            <th class="text-center">{{_i("Deposits")}}</th>
                                                            <th class="text-center">{{_i("Withdrawals")}}</th>
                                                            <th class="text-center">{{_i("Trading")}}</th>
                                                            <th class="text-center">{{_i("Pending Deposits")}}</th>
                                                            <th class="text-center">{{_i("Pending Withdrawals")}}</th>
                                                            <th class="text-center">{{_i("Maintenance Notes")}}</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                        @foreach($coins as $coin)
                                                            <tr>
                                                                <td><b>{{$coin->name}}</b></td>
                                                                <td>{{$coin->symbol}}</td>
                                                                <td class="{{$coin->can_deposit ? "text-success" : "text-danger"}} text-center">{{$coin->can_deposit ? "Active" : "Offline"}}</td>
                                                                <td class="{{$coin->can_withdraw ? "text-success" : "text-danger"}} text-center">{{$coin->can_withdraw ? "Active" : "Offline"}}</td>
                                                                <td class="{{$coin->can_trade ? "text-success" : "text-danger"}} text-center">{{$coin->can_trade ? "Active" : "Offline"}}</td>
                                                                <td class="text-center" >{{$coin->pending_deposits}}</td>
                                                                <td class="text-center">{{$coin->pending_withdrawals}}</td>
                                                                <td>{{$coin->maintenance_note}}</td>
                                                            </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
