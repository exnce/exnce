@extends('layouts.site')
@php
    $activeCoinID = session("activeCoinID");
    $activeCurrencyID = session("activeCurrencyID");

    if($activeCoinID && $activeCurrencyID) {
        $statsData = getHeaderHomeStatistics($activeCoinID, $activeCurrencyID);
    } else {
        $statsData = getHeaderHomeStatistics();
    }

    if($statsData["change_ratio"] > 0) {
        $szChangeColor = "text-success";
    } elseif($statsData["change_ratio"] < 0) {
        $szChangeColor = "text-danger";
    } else {
        $szChangeColor = "";
    }
    $symbol = "{$activeCurrency->symbol}-{$activeCoin->symbol}";
    $selectedPair = "{$activeCurrency->symbol}/{$activeCoin->symbol}";
@endphp
@section("title",_i($selectedPair))
@section('content')

    <div class="container-fluid">
        <div class="row">



            <div class=exnce_pairs>
                <div class="logo fa fa-angle-right"><span class=blink>_</span></div>
                <div class=handle>
                    <div class="v-bar-a v-bar"></div>
                    <div class="v-bar-b v-bar"></div>
                </div>
            </div>
            <div class=outer-most>
                <div class=side-panel>
                    <div class=side-panel-overlay>
                        <div class=about-me>


                            <div class="card rounded border-0  p-0">
                                <div class="card-body pt-0 pb-0 px-0">
                                    <div class="market-section">
                                        <div class="currency-tabs">
                                            @foreach ($baseCoins as $coin_id => $baseCoin)
                                                <div class="tab-item {{ ( $activeCoin->id == $coin_id ? "active" : "" ) }}"
                                                     data-coinid="{{ $coin_id }}">
                                                    <span class="tab-item-text">{{ $baseCoin }}</span>
                                                </div>
                                            @endforeach
                                        </div>

                                        <div>
                                            <table class="table currency-table fixed-table ">
                                                <thead class="table-head text-center">
                                                <tr>
                                                    <th class="border-0 text-left"><span class="fav-btn filter"></span></th>
                                                    <th class="sorting border-0 text-left pl-1">{{ _i("Pair") }}</th>
                                                    <th class="border-0 text-left pl-1">{{ _i("Price") }}
                                                    </th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Volume") }}</th>
                                                    <th class="border-0 text-right pr-1">{{ _i("Change") }}</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>

                                        <div class="table-wrapper for-coinsTable">
                                            <table class="table table-hover currency-table mb-5">
                                                <tbody class="coinsTable">
                                                @foreach ($pairList as $pair)
                                                    @php
                                                        $strPair = $pair["currency"] . "-" . $pair["coin"];
                                                        $activeClass = $pair["coin_id"] == $activeCoin->id && $pair["currency_id"] == $activeCurrency->id ? "active" : "";

                                                        if($pair["change"] > 0) {
                                                            $changeClass = "text-success";
                                                        } elseif($pair["change"] < 0) {
                                                            $changeClass = "text-danger";
                                                        } else {
                                                            $changeClass = "";
                                                        }

                                                    @endphp
                                                    <tr class="pair" data-coinid="{{ $pair["coin_id"] }}"
                                                        data-currency="{{ $pair["currency_id"] }}"
                                                        data-pair="{{ $strPair }}"
                                                        style="{{ ( $activeCoin->id != $pair["coin_id"] ? "display:none;" : "" ) }}">

                                                        <td class="{{ $activeClass }}"><span
                                                                    class="fav-btn {{ in_array($strPair, $favorites) ? "checked" : "" }}"></span>
                                                        </td>
                                                        <td>{{ $pair["currency"] }}</td>
                                                        <td id="lastprice">{{ $pair["lastprice"]  }}</td>
                                                        <td id="volume" class="text-right">{{ $pair["volume"]  }}</td>
                                                        <td id="change" class="text-right {{ $changeClass }}">{{ $pair["change"]  }}%
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>




                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class=front-area>
                    <div class=content-container>
                        <div class="exnce_chart">
                            <div class="row">


                                <div class="col-12">
                                    <ul class="nav nav-tabs pageination-table-title w-100" id="myTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active border-top-0 border-left-0 border-right-0 bg-transparent"
                                               data-toggle="tab" href="#tabCharts" role="tab" aria-controls="tabCharts"
                                               aria-selected="true">{{ _i("Chart") }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                               data-toggle="tab" href="#tabDepth" role="tab" aria-controls="tabDepth"
                                               aria-selected="true">{{ _i("Depth") }}</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link border-top-0 border-left-0 border-right-0 bg-transparent"
                                               id="islem-tab"
                                               data-toggle="tab" href="#tabInfo" role="tab"
                                               aria-selected="false">{{ _i("Info") }}</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="tabCharts" role="tabpanel">
                                            <div id="chart-section">
                                                @include("site.chart.chart")
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabDepth" role="tabpanel">
                                            <div id="chart-section">
                                                @include("site.chart.depth")
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tabInfo" role="tabpanel">
                                            @include("personal.market.chart.info")
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>
                        <div class=projects>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                        <div class="card rounded border-0  p-0">
                                            <div class="card-body pt-0 pb-0 px-0">
                                                <h6 class="card-title text-left" id="orderBookTitle">{{_i("Orderbook")}}
                                                    <span class="float-right">

                                                        <ul class="orderbook-list-short">
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="0" alt="orderbook-buysell"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAARCAYAAADdRIy+AAABF0lEQVQ4jaXUoUvDQRjG8c+m4MAgGoXhQBAMBpt/gMUoBq2KYBNhIIpZEcE8ZMjqgibLqqJZ25JgEDE5FsSB6Aze4NhUfvv5wHHPexzfe7h7ucxToSjSeZiXpNRgV/2WFvQTcBgLkc+j/h/gMq4jP44S9tIC17GPTIDksHmxO9VAux/gS/CvqAVfwgBuQp3pBzgZ/Ds+gq9gFJ+hTpwws1XtXdzeKU6gcHR4fDndPEnK+h0YKwATJ8yGzW3MRus1nMUHJx1ZjOEAaxGgieeobicdWTRQxorvVhnCPObSJOz04QNusRhOusMM7usjG39dWY/ixq5gNfgyrkLivhS/cg6PIWEerX5h3QlbqEY+lbq/r9O0oI6+AFb5Q71drBHQAAAAAElFTkSuQmCC"
                                                                       selected="">
                                                            </li>
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="1" alt="orderbook-buy"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAPCAYAAADkmO9VAAAAyklEQVQ4jcXSMUoDQRTG8d8sAdMJ6XOAFBZ2HiAHsLROY5tcInfYDWIbwQOk9gKxsxIsLOxCuu02RXbDsGiYLAt+8OC9Yfje/72ZkOd5pUcNEPo0DFV1HrAoCkieYpDauG/DdMLFy/nmEzn/SZjVlyvcRucbvEZ1SI0MIywxiwz2+GkRJkWGHVZ4wBBXmOKuC2Gzwy9scV/X77jB58f145/7+k3xozxHY6/wVhNfpDBfn/Ihvh13MUZ5qVmbsMQ6yjup/Q+fuho1OgDrcC9Xcb+0AgAAAABJRU5ErkJggg==">
                                                            </li>
                                                            <li class="">
                                                                <input type="image" class="orderbook-short" data-filter="2" alt="orderbook-sell"
                                                                       src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAANCAYAAACpUE5eAAAAsUlEQVQ4jaWQMQoCMRBFX9BCsLIVhAXBu1hbeAFrGyux9gwLAfEEVjYeQM9gvY2lW4mFaGxGCEuUmfgh5P8Q3nzGXYoFkXZyT8hUu5HvuaAUsAuMIz8Azv8Ap8Ax8n2gBFa5wBmwBpxAOsB8vxzVQLAAr+JvwEF8CbSAk2RnAQ7FP4Cn+C3QA16STQ3rxPsGKKJsaphSJecjdUMXwu+/3nstC/jesCn9DpUNTDtUT9foDaE9InSwV1aPAAAAAElFTkSuQmCC">
                                                            </li>
                                                        </ul>

                                                    </span>
                                                </h6>

                                                <table class="table currency-table orderbook mb-0">
                                                    <thead class="table-head text-center">
                                                    <tr>
                                                        <th class="border-0 text-left"><span
                                                                    class="d-block">{{ _i("Price") }}
                                                                ({{$activeCoin->symbol}})</span></th>
                                                        <th class="border-0 text-right pr-1"><span
                                                                    class="d-block">{{ _i("Amount") }}
                                                                ({{$activeCurrency->symbol}})</span></th>
                                                        <th class="border-0 text-right"><span
                                                                    class="d-block">{{ _i("Total") }}
                                                                ({{ $activeCoin->symbol }})</span></th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                                <div class="table-wrapper orderbook-panels">
                                                    <table class="table table-hover currency-table orderbook sell">
                                                        <tbody class="orderbookSellTable">
                                                        </tbody>
                                                    </table>
                                                </div>

                                                <div class="lastPrice"><span class="price">0.00</span> <i
                                                            class="fa fa-arrow-up arrow"></i>
                                                </div>

                                                <div class="table-wrapper orderbook-panels">
                                                    <table class="table table-hover currency-table orderbook buy">
                                                        <tbody class="orderbookBuyTable">
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class=skills>
                                <div class=skills-overlay>
                                    <div class=tool-chain>
                                        <div class="card rounded border-0  d-sm-block">
                                            <div class="card-body pt-0 px-0 pb-0">
                                                <h6 class="card-title text-left" id="orderHistoryTitle">{{_i("Price Information")}}</h6>
                                                <div class="row">
                                                    <div id="chart-coin-info" class="col-12">
                                                        <h2 class="coin-chart-head">
                                                            <img src="{{ asset("images/coins/{$statsData["currency"]->slug}.svg") }}"/>
                                                            <div>
                                                                <strong>{{$statsData["currency"]->symbol}}/{{$statsData["coin"]->symbol}}</strong>
                                                                <em lang="en">{{$statsData["currency"]->name}} / {{$statsData["coin"]->name}}</em>
                                                            </div>
                                                        </h2>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="row pl-2">
                                                            <div class="col-6">
                                                                <label>{{$statsData["currency"]->symbol}} {{ _i("Current Price") }}</label>
                                                                <div class="input-group-prepend">
                                                                    <span id="price24h-info">{{$statsData["price"]}}</span>&nbsp;&nbsp
                                                                    <small id="price24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                </div>
                                                            </div>
                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Change") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="change24h-info">{{ $statsData["change_ratio"] }}</span>&nbsp
                                                                        <small id="change24h-symbol">%</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Highest Price") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="priceMax24h-info">{{$statsData["maximum_price"]}}</span>&nbsp
                                                                        <small id="priceMax24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Lowest Price") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="priceMin24h-info">{{$statsData["minimum_price"]}}</span>&nbsp
                                                                        <small id="priceMin24h-symbol">{{$statsData["coin"]->symbol}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-6">
                                                                <label>{{ _i("24h. Volume") }}</label>
                                                                <div class="input-group">
                                                                    <div class="input-group-prepend">
                                                                        <span id="volume24h-info">{{$statsData["volume"]}}</span>&nbsp
                                                                        <small id="volume24h-symbol">{{$statsData["currency"]->symbol}}</small>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="card rounded border-0  p-0">
                                            <div class="card-body pt-0 pb-0 px-0">
                                                <h6 class="card-title text-left" id="orderHistoryTitle">{{_i("Trade History")}}</h6>

                                                <table class="table currency-table">
                                                    <thead class="table-head text-center">
                                                    <tr>
                                                        <th class="border-0 text-left">{{ _i("Time") }}</th>
                                                        <th class="border-0  text-left pl-1"><span
                                                                    class="d-block">{{ _i("Amount") }} ({{$activeCurrency->symbol}})</span></th>
                                                        <th class="border-0 text-right px-0 pr-1"><span
                                                                    class="d-block">{{ _i("Price") }} ({{$activeCoin->symbol}})</span></th>
                                                    </tr>
                                                    </thead>
                                                </table>

                                                <div class="table-wrapper for-marketOrderHistory">
                                                    <table class="table table-hover currency-table">
                                                        <tbody id="marketOrderHistory" class="marketOrderHistory">

                                                        </tbody>
                                                    </table>
                                                </div>

                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </div>




                            <div class=projects-wrapper>

                            </div>
                            <div class=go-back-indicator><span class="back fas fa-long-arrow-alt-left"></span></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
