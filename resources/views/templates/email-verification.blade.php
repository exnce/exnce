@php
     $szConfirmText1 = _i('<a href="%s"> <b>Click here</b></a> to verify your email address');
     $szConfirmText2 = _i("%s will never ask for your password by e-mail. Your wallet addresses will not be forwarded via e-mail. Please do not send e-mail to the wallet address specified by e-mail! Please do not forget to share this situation with us.");
     $szConfirmText1 = sprintf($szConfirmText1, route('verify-email-request', ['token' => $email_token]));
     $szConfirmText2 = sprintf($szConfirmText2, env("APP_NAME"));
     $szConfirmText3 = "%s Team";
     $szConfirmText3 = sprintf($szConfirmText3, env("APP_NAME"));
@endphp
<h1>{{_i("Confirm your email address")}}</h1>
<p>{{_i("Hello;")}}</p>
<p>{!!html_entity_decode($szConfirmText1)!!}</p>
<h2>{{_i("If it doesnt working")}}</h2>
<p><a href="{{route('verify-email-request', ['token' => $email_token])}}" target="_blank">{{route('verify-email-request', ['token' => $email_token])}}</a></p>
<span>{{ $szConfirmText2 }}</span>
<br/>
<p><strong>{{$szConfirmText3}}</strong></p>
<p><a href="{{env("APP_URL")}}" target="_blank">{{env("APP_URL")}}</a></p>
