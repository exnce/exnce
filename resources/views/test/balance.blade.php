
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <script type="text/javascript" src="https://piyolab.github.io/playground/ethereum/web3/v0.20.6/web3.min.js"></script>
</head>
<body>

<h1>Get ERC20 Token Balance</h1>

<h2>Token Address</h2>
<input value="0xe0c6ce3e73029f201e5c0bedb97f67572a93711c" type="text" id="token-address" size="80" oninput="onAddressChange()"></input>
<input onclick="checkBalances()"  class="btn-success" type="button" id="btnCheckBalances" value="Check Balances"/>
<h2>Wallet Address</h2>
@foreach($wallets as $wallet)
<input value="{{$wallet->address}}" type="text" name="wallet-address" size="180"></input>
@endforeach

<script>

    function checkBalances() {
        adresListesi = document.getElementsByName("wallet-address");
        adet = adresListesi.length;
        i = 0;

        for(i = 0; i<adet;i++){
            walletAddress = adresListesi[i].value;
            //

             tokenAddress = "0x2534409daa29b07682020d07eac9ae01c34acec0";
               getERC20TokenBalance(true,tokenAddress, walletAddress,i, (balance) => {
               });



        //  getEtherTokenBalance(walletAddress);
        }


    }


    function getEtherTokenBalance(walletAddress) {

        web3.eth.getBalance(walletAddress, (error1, balance) => {
                console.log(walletAddress + " ====== " + balance.toString());


        });

    }


    function getERC20TokenBalance(isToken, tokenAddress, walletAddress,objectData, callback) {

                let minABI = [
                    // balanceOf
                    {
                        "constant":true,
                        "inputs":[{"name":"_owner","type":"address"}],
                        "name":"balanceOf",
                        "outputs":[{"name":"balance","type":"uint256"}],
                        "type":"function"
                    },
                    // decimals
                    {
                        "constant":true,
                        "inputs":[],
                        "name":"decimals",
                        "outputs":[{"name":"","type":"uint8"}],
                        "type":"function"
                    }
                ];

                let contract = web3.eth.contract(minABI).at(tokenAddress);
                 contract.balanceOf(walletAddress, (error, balance) => {
                    // ERC20トークンの decimals を取得
                    contract.decimals((error, decimals) => {
                        // 残高を計算
                        balance = balance.div(10**decimals);

                        if(balance.toString() != "0"){
                            console.log(walletAddress + " ====== " + balance.toString());
                        }

                        callback(balance);
                    });
                });
    }


    window.onload = function() {
        if (typeof web3 !== 'undefined') {
            web3 = new Web3(web3.currentProvider);
        } else {
            web3 = new Web3(new Web3.providers.HttpProvider("https://mainnet.infura.io/v3/ed0b078730f6405e84d54d1b620c9a41"));
        }
        console.log(web3.version);



    }

</script>

</body>
</html>