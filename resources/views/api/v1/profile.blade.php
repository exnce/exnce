
    class TEXNCECOM {

        displayLog(szMessage){
            console.log(szMessage);

        }

        constructor(instance) {
            let parentObject = "{{$render}}";

            this.libCounter = 0;
            this.parentObj = parentObject;
            this.install();
        }

        installLibrary(check,source){
            if(!check){
                    var lib = document.createElement('script');
                    lib.type = "text/javascript";
                    lib.src = source;
                    lib.async = false;
                    lib.onload = function() {
                        this.libCounter++;
                        this.install();
                    }.bind(this);
                    document.getElementsByTagName('head')[0].appendChild(lib);
            }
        }

        install(){
            switch (this.libCounter) {
                case 0 : this.installLibrary(window.jQuery, "https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"); break;
                case 1 : this.installLibrary(window.jQuery.fn.select2, "https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"); break;
                default :
                    this.prepare();break;
            }
        }

        prepare(){
            this.initUI();
        }

        formatCoinSelect(option) {
            let icon = option.assetLogo;
            let image = '<div class="exncecoinitem"><img height="28px" width="28px" class="exncecoinlogo" src="'+icon+'"/>';
            var ob = image + "<span class='exncecoinname'>"+ option.assetName + " </span><span class='exncecoinsymbol'>"+option.assetSymbol+"</span></div>";
            return ob;
        }


        initUI(){
            var wallets = this.wallets().results;

            $('head').append(this.css());
            $("."+this.parentObj).html(this.html());

            if(wallets != null){
                 $("#exnceAssetList").select2({
                    data:wallets,

                    data:$.map(wallets, function (obj) {
                        return obj;
                    }),
                    theme: 'bootstrap4',
                    placeholder: function(){
                        $(this).data('placeholder');
                    },
                    templateResult: this.formatCoinSelect,
                    templateSelection: this.formatCoinSelect,
                    escapeMarkup: function (m) {
                       return m;
                    },
                 }).bind(this).on('select2:select', function (e) {
                    var data = e.params.data;
                    $("#exnceWalletAddress").val(data.paymentAddress);
                    $("#exncePaymentQR").attr("src", "https://chart.googleapis.com/chart?cht=qr&chl=" + data.paymentAddress + "&chs=160x160&chld=L|0");
                    $("#exncePaymentDetails").fadeOut( "fast" );
                    $("#exncePaymentDetails").fadeIn( "slow" );

                }).bind(this);
            }


        }

        html(){
            return '{!! $html !!}';
        }

        css(){
            return '{!! $css !!}';
        }

        wallets(){
            return JSON.parse('{!!$wallets!!}');
        }

        init() {
         //   alert(this.parentObj);
        }
    }

    let exnce = new TEXNCECOM("EXNCE");
    exnce.init();

