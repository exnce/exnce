@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("API Documentation"))
@section('content')



    <div class="row pl-3 pr-3 mb-5">

        <div class="col-12">




            <div class="card rounded  box-shadow border-0 pt-3 px-3 py-3 pb-3">

                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true" aria-controls="collapseCrypto">
                        <h5>{{ _i('API Documentation') }}</h5>
                        <span>{{_i("Creating an API private key provides access to markets and real-time trading services on Exnce via a third-party site or application.")}}</span>
                    </a>
                </div>


                <div class="card-body">

                    <div class="row">
                        <div class="col-12">
                            <h1 class="text-warning">General Endpoints</h1>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">Test connectivity</h2>
                            <p>Test connectivity to the Rest API.</p>
                            <pre><code>GET /api/v1/ping</code></pre>
                            <pre><code>{{route("pingv1")}}</code></pre>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">Check server time</h2>
                            <p>Test connectivity to the Rest API and get the current server time.</p>
                            <pre><code>GET /api/v1/time</code></pre>
                            <pre><code>{{route("timev1")}}</code></pre>
                        </div>
                    </div>
                    <hr/>

                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">Ticker</h2>
                            <p>24 hour rolling window price change statistics. </p>
                            <pre><code>{{route("tickerv1")}}</code></pre>
                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">Exchange Information</h2>
                            <p>Current exchange trading rules and symbol information</p>
                        </div>
                        <div class="col-6">
                            <p><b>Weight:</b>1</p>
                            <p><b>Parameters : </b> NONE</p>
                            <pre><code>GET /api/v1/exchangeInfo</code></pre>
                            <pre><code>{{route("exchangeInfov1")}}</code></pre>
                        </div>
                        <div class="col-6">
                            <h5 class="text-success">{{_i("Success Response")}}</h5>
                            <pre>
{
    "status": true,
    "success": true,
    "message": "Exchange information has been listed successfuly...",
    "timezone": "UTC",
    "serverTime": 1561469983,
    "rateLimits": [],
    "exchangeFilters": [],
    "symbols": [{
        "symbol": "XRP\/ETH",
        "status": "TRADING",
        "baseAsset": "XRP",
        "baseAssetPrecision": 8,
        "quoteAsset": "ETH",
        "quotePrecision": 8,
        "orderTypes": ["LIMIT"],
        "icebergAllowed": false,
        "filters": []
    }]
}
                            </pre>
                        </div>
                    </div>


                    <hr/>

                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">Get Currencies</h2>
                            <p>This endpoint retrieves a list of all tokens. Not all tokens can be used for trading. Tokens which have or had no representation in ISO 4217 may use a custom code.</p>
                        </div>
                        <div class="col-6">
                            <h5>{{_i("Request Sample")}}</h5>
                            {{route("currenciesv1")}}
                        </div>
                        <div class="col-6">
                            <h5 class="text-success">{{_i("Return Sample")}}</h5>

                            <pre>
[
   {
      "name":"Ethereum",
      "symbol":"ETH",
      "slug":"eth",
      "min_deposit":"0.01000000",
      "min_withdraw":"0.00300000",
      "processing_fee":"0.00100000",
      "can_trade":true,
      "maintenance_note":"-",
      "can_deposit":true,
      "can_withdraw":true
   },
   {
      "name":"Ripple",
      "symbol":"XRP",
      "slug":"xrp",
      "min_deposit":"0.00000000",
      "min_withdraw":"25.00000000",
      "processing_fee":"0.00100000",
      "can_trade":true,
      "maintenance_note":"-",
      "can_deposit":true,
      "can_withdraw":true
   }
]
                            </pre>


                        </div>



                    </div>

                    <div class="row">
                        <div class="col-12">
                            <h1 class="text-warning">Market Data endpoints</h1>
                            <hr/>
                        </div>
                        <div class="col-12">

                            <h3 class="text-warning">Orderbook</h3>
                            <pre><code>GET /api/v1/depth</code></pre>
                            <pre><code>{{route("depthv1")}}?symbol=XLM-ETH&limit=10</code></pre>


                            <div class="row">
                                <div class="col-6">
                                    <p><strong>Weight:</strong>Adjusted based on the limit:</p>
                                    <table class="table-hover table-bordered w-100">
                                        <thead>
                                        <tr>
                                            <th>Limit</th>
                                            <th>Weight</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>5, 10, 20, 50, 100</td>
                                            <td>1</td>
                                        </tr>
                                        <tr>
                                            <td>500</td>
                                            <td>5</td>
                                        </tr>
                                        <tr>
                                            <td>1000</td>
                                            <td>10</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <p><strong>Parameters:</strong></p>
                                    <table class="table-hover table-bordered w-100">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Mandatory</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>symbol</td>
                                            <td>STRING</td>
                                            <td>YES</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>limit</td>
                                            <td>INT</td>
                                            <td>NO</td>
                                            <td>Default 100; max 1000. Valid limits:[5, 10, 20, 50, 100, 500, 1000]</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6">
                                    <h5 class="text-success">{{_i("Success Response")}}</h5>
                                    <pre>
{
  "status": true,
  "success": true,
  "message": "Successfuly",
  "data": {
    "bids": [
      [
        "0.00015200", // PRICE
        "2610.00000000" // QTY
      ],
      [
        "0.00015105",
        "1031.00000000"
      ]
    ],
    "asks": [
      [
        "0.00017000",
        "75.00000000"
      ],
      [
        "0.00021989",
        "2612.88099459"
      ]
    ]
  }
}
                            </pre>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <h2 class="text-warning">Old Trade Lookup (MARKET_DATA)</h2>
                            <p>Get older trades. </p>
                            <pre><code>GET /api/v1/historicalTrades</code></pre>
                            <pre><code>{{route("historicalTradesv1")}}/?symbol=ETHPLO-ETH&limit=10&fromId=5150</code></pre>

                            <div class="row">
                                <div class="col-6">
                                    <p><b>Weight : </b> 5</p>
                                    <p><b>Parameters:</b></p>

                                    <table class="table-hover table-bordered w-100">
                                        <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Type</th>
                                            <th>Mandatory</th>
                                            <th>Description</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td>symbol</td>
                                            <td>STRING</td>
                                            <td>YES</td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>limit</td>
                                            <td>INT</td>
                                            <td>NO</td>
                                            <td>Default 500; max 1000.</td>
                                        </tr>
                                        <tr>
                                            <td>fromId</td>
                                            <td>LONG</td>
                                            <td>NO</td>
                                            <td>TradeId to fetch from. Default gets most recent trades.</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-6">

                                    <pre>
[{
    "id": 5182,
    "amount": "2055.22225000",
    "price": "0.00015105"
}, {
    "id": 5174,
    "amount": "240.50680615",
    "price": "0.00015104"
}, {
    "id": 5170,
    "amount": "100.00000000",
    "price": "0.00015103"
}, {
    "id": 5162,
    "amount": "457.65914655",
    "price": "0.00016698"
}, {
    "id": 5160,
    "amount": "36.00000000",
    "price": "0.00017000"
}, {
    "id": 5156,
    "amount": "11.12500000",
    "price": "0.00017000"
}]
                                    </pre>
                                </div>
                            </div>


                        </div>
                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">openOrders</h2>
                        </div>
                        <div class="col-6">


                            <h5>{{_i("Sample Request")}}</h5>
                            <small>
                    <pre>
curl --location --request POST "https://exnce.com/api/v1/openOrders" \
--header "secretKey: YOUR_SECRET_KEY" \
--header "publicKey: YOUR_PUBLIC_KEY" \
                    </pre>
                            </small>

                        </div>
                        <div class="col-6">
                            <h5 class="text-success">{{_i("Success Response")}}</h5>
                            <pre>
{
    "status": true,
    "success": true,
    "message": "Operation successful!",
    "orders": {
        "bids": [
            {
                "amount": "249.99999981",
                "real_amount": "249.99999981",
                "completed_amount": "0.00000000",
                "remaining_amount": "249.99999981",
                "price": "1.00000000",
                "total": "249.99999981",
                "fee": "0.25000000",
                "processing_fee": "0.00100000",
                "orderId": 23,
                "clientOrderId": "2F8877AC92",
                "symbol": "NPXS/ETH",
                "time": 1554047853,
                "updateTime": 1554047853
            }
        ],
        "asks": [
            {
                "amount": "1.00000000",
                "real_amount": "1.00000000",
                "completed_amount": "0.00000000",
                "remaining_amount": "1.00000000",
                "price": "1.30000000",
                "total": "1.30000000",
                "fee": "0.00130000",
                "processing_fee": "0.00100000",
                "orderId": 27,
                "clientOrderId": "0AFB6B04F2",
                "symbol": "NPXS/TUSD",
                "time": 1554198057,
                "updateTime": 1554198057
            }
        ]
    }
}
</pre>

                            <h5 class="text-danger">{{_i("Failed Response")}}</h5>
                            <small>
                        <pre>
{
  "status": true,
  "success": false,
  "message": "Invalid Secret Key or Public Key!",
  "orders": null
}
                        </pre>
                            </small>
                        </div>
                    </div>
                    <hr/>
                    <div class="row ">
                        <div class="col-12">
                            <h2 class="text-warning">createSell [LIMIT]</h2>
                        </div>
                        <div class="col-6">

                            <table class="table-hover table-bordered w-100">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Data</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td col="3">Method</td>
                                    <td></td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td col="3">Endpoint</td>
                                    <td></td>
                                    <td>https://exnce.com/api/v1/createSell</td>
                                </tr>


                                <tr>
                                    <td col="3">Header</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>secretKey</td>
                                    <td>YOUR_SECRET_KEY</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>publicKey</td>
                                    <td>YOUR_PUBLIC_KEY</td>
                                </tr>
                                <tr>
                                    <td col="3">Body</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>symbol</td>
                                    <td>ETH/BNB</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>side</td>
                                    <td>SELL</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>type</td>
                                    <td>LIMIT</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>quantity</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>price</td>
                                    <td>0.012</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-6">
                            <h5 class="text-success">Response</h5>
                            <pre>
{
    "success": true,
    "type": "success",
    "title": "Information",
    "message": "Your sell order has been successfully created.",
    "data": {
        "clientOrderId": "E6D9C775372853"
    }
}
                            </pre>
                        </div>


                    </div>
                    <hr/>
                    <div class="row">
                        <div class="col-12">
                            <h2 class="text-warning">createBuy [LIMIT]</h2>
                        </div>
                        <div class="col-6">


                            <table class="table-hover table-bordered w-100">
                                <thead>
                                <tr>
                                    <th>Type</th>
                                    <th>Data</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td col="3">Method</td>
                                    <td></td>
                                    <td>POST</td>
                                </tr>
                                <tr>
                                    <td col="3">Endpoint</td>
                                    <td></td>
                                    <td>https://exnce.com/api/v1/createBuy</td>
                                </tr>


                                <tr>
                                    <td col="3">Header</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>secretKey</td>
                                    <td>YOUR_SECRET_KEY</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>publicKey</td>
                                    <td>YOUR_PUBLIC_KEY</td>
                                </tr>
                                <tr>
                                    <td col="3">Body</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>symbol</td>
                                    <td>ETH/BNB</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>side</td>
                                    <td>BUY</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>type</td>
                                    <td>LIMIT</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>quantity</td>
                                    <td>1</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>price</td>
                                    <td>0.012</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="col-6">
                            <h5 class="text-success">Response</h5>
                            <pre>
{
    "success": true,
    "type": "success",
    "title": "Information",
    "message": "Your buy order has been successfully created.",
    "data": {
        "clientOrderId": "E6D9C775372854"
    }
}</pre>
                        </div>
                    </div>

                </div>






        </div>
    </div>
@endsection
