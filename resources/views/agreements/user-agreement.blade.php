@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("Terms Of Service"))
@section('content')


    <div class="row mb-5">
        <div class="col-12">
            <div class="card rounded  border-0 box-shadow pt-3 px-3 py-3 pb-5">

                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true" aria-controls="collapseCrypto">
                        <h5>{{ _i('TERMS AND CONDITIONS OF USE THE SERVICES BY THE EXNCE.COM SITE AND THE PURCHASE OF TOKENS.') }}</h5>
                    </a>
                </div>


                <div class="card-body text-justify">


    <p>
    Please carefully read these Terms and Conditions (the "Terms") before using the website https://exnce.com (the "Website"), as they affect your obligations and rights, including, but not limited to, waiver of rights and limitation of liability. If you want to participate in the pre-sale of EXNCE tokens (hereinafter - "pre-sale"), or in the main distribution of tokens (hereinafter referred to as "ICO"), you must also read these Terms and accept them. If you do not agree to these Terms, you must not use the website exnce.com or buy EXNCE tokens. By agreeing and accepting these Terms as mandatory for all users of exnce.com, you acknowledge that your age is more than 18 years.
  </p>
<ul>
    <li>TERMS.</li>

    1. Account.
    <p>
    The user account on the exnce.com website, which is created and used to purchase tokens. The user is granted access to the Account after successfully creating it on the website https://exnce.com/ and providing all necessary information. Only authorized users have the right to purchase EXNCE tokens on the terms specified in this document.
    </p>
        2. Agreement.
    <p>
    These Terms and all other applicable rules, policies and procedures that may be published from time to time on the Website (including privacy policy, cookie policy, etc.).
    </p>
        3. Ethereum.
    <p>
    A consensual distributed network in which it is possible to use this payment system and all the digital money used in it. This system works on behalf of its users without any central authority or intermediaries.
    </p>
    4. Blockchain.
    <p>
    The type of distributed network, consisting of immutable digital data, called blocks.
    </p>
    5. EXNCE Tokens.
    <p>
    Cryptographic tokens, which are a software product (digital resource) created by the Website Owner as proof of the membership of their owners in the EXNCE ecosystem (the system, not the legal entity). Although EXNCE tokens can be similar to securities, they are not and should not be treated in such terms.
    </p>
        6. User.
    <p>
    Anyone who uses the Website with pre-registration and authorization using an account.
    </p>
        7. The owner of the website, the Company, we.
    <p>
    The first tokenized closed fund, intended for operating with EXNCE assets. In no event (the Сompany) cannot be considered as a partner, employer or agent for any User or provide any financial services.
    </p>
    <li> REGISTRATION AND ACCOUNTING DATA.</li>
    <p>
    Registering on exnce.com, you confirm that you are over 18 years old. Registration is required prior to committing a transaction with exnce.com. Once registered, you may need to provide contact information such as name, address, contact phone number, e-mail address and other personal information. We guarantee the security of the transmitted information and its secure storage on the servers exnce.com and also guarantee that we will not provide it to third parties, except as required by law. You will also need to create a username and password for your account. In doing so, you are solely responsible for maintaining the security and confidentiality of your account information.
    You are also fully responsible for all transactions made on behalf of your account using your login and password. Deex.exchange reserves the right to refuse registration, termination of invoices or cancellation of user’s data at any time and for any reason, including in the event of non-compliance with any terms of the Terms of Use described hereafter.
    You must not use our website in any way that could cause or cause damage or interfere with access to exnce.com. Also, any illegal, fraudulent or malicious activity on the website is caused by any kind of illegal, fraudulent or harmful work or activity harmful to its work.
    </p>
    <li>REGULATION OF USE OF THIRD-PARTY RESOURCES.</li>
    <p>
    The pages of the exnce.com website may contain links to third-party websites and services. Such links are provided for your convenience, but their presence does not mean that they are recommended by EXNCE.COM. In addition, EXNCE.COM does not guarantee their security and compliance with the expectations of users.
    In addition, we are not responsible for the content of any materials referenced by another site, and we do not provide any guarantees for the security of this site in the context of our security policy. EXNCE.COM assumes no responsibility for any damage, loss or any other impact directly or indirectly caused by the use of any content, goods or services available on third-party websites and resources.
    </p>
    <li>COMPENSATION.</li>
    <p>
    To the extent permitted by applicable law, the User agrees to indemnify EXNCE.COM and/or its subsidiaries, affiliates, directors, officers, employees, agents, successors and permitted assignees without any claim, damages, compensate claims, actions, claims, proceedings, expenses and/or liabilities (including, but not limited to, fees paid to lawyers and/or legal representatives) that are necessary for the successful establishment of the rights to compensation filed/incurred by any third party in respect of EXNCE.COM arising as a result of violation of any warranties or rules provided on this website.
    STATEMENT OF WARRANTIES AND DISCLAIMER OF LIABILITY
    THIS WEB SITE AND EXNCE TOKENS ARE PROVIDED ON AN "AS IS" BASIS AND WITHOUT ANY WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED. YOU ACCEPT ALL RESPONSIBILITIES AND RISKS IN RESPECT OF USING THE WEB SITE AND BUYING ANY TOTAL AMOUNT OF TOKENS AND ITS FURTHER USE.
    WHEREAS THIS CONDITION, YOU AGREE C THAT, TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, WEBSITE OWNERS DO NOT ACCEPT ANY LIABILITY THAT MAY OCCUR AS A RESULT OF ANY DAMAGES OR LOSSES, INCLUDING LOSS OF BUSINESS, REVENUE OR PROFITS, LOSS OF OR DAMAGE OF
    </p>
    <p>
    DATA, EQUIPMENT, OR THE SOFTWARE (DIRECT, INDIRECT, PUNISHMENT, ACTUAL, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY OR OTHERWISE), AS A RESULT OF ANY USE OR INABILITY TO USE THIS WEB SITE OR, INFORMATION, SOFTWARE, FACILITIES, SERVICES OR CONTENT ON THIS WEBSITE, FROM THE PURCHASE OR USE OF TOKEN USERS, REGARDLESS OF ADDITION, PROVISION IS THERE SUCH A POSSIBILITY OR NOT.
    YOU UNDERSTAND AND AGREE THAT THE WEBSITE OWNERS WILL NOT BE LIABLE AND MAKE NO RESPONSIBILITY OR LIABILITY FOR ANY CHANGES IN THE VALUE OF ISSUED TOKENS DURING THE REDUCTION OR THE INCREASE OF THEIR VALUE. WEB SITE OWNERS DO NOT GUARANTEE ANYWAY FOR FURTHER SALES OR EXCHANGE OF TOKENS BY COST EXCELLENT FROM INITIAL AND STATUTORY ICO OR PRELIMINARY SALES (PRE-SALE).
    REGULATION OF EXNCE TOKENS SALE.
    </p>
    <p>
    You can purchase EXNCE tokens during the pre-sale period, or during the main placement of tokens (ICO). The offer is indicated here and further on different exchanges. EXNCE tokens are the proof of the ownership of their owners in the EXNCE ecosystem (the system, not the legal entity). EXNCE tokens give their owners the right to profit from the sale, exchange them for other assets. Trade, purchase and exchange of tokens occurs with the use of specially programmed software. Any user who wants to buy tokens must register an account with the Website using the real name and e-mail address and other data required during registration. You can buy EXNCE tokens only through your account.
    PURCHASING THE EXNCE TOKENS, THE USER GUARANTEE THAT HE/SHE FUNDS DO NOT HAVE ANY ILLEGAL OR UNSTABLE SOURCES.
    </p>
    <li>INTELLECTUAL PROPERTY RIGHTS</li>
    <p>
    The Website Owner has valid, unrestricted and exclusive ownership of rights to use the patents, trademarks, trademark registrations, trade names, copyrights, know-how, technology and other intellectual property necessary to the conduct of selling of the EXNCE Tokens and his activities generally.
    In no way shall this Agreement entitle the User for any intellectual property of the Website Owner, including the intellectual property rights for the Website and all text, graphics, user interface, visual interface, photographs, trademarks, logos, artwork, and computer code, design, structure and other content connected to the Website. Arrangement of such content is owned by the EXNCE and is protected by the Intellectual Property Rights and fair competition laws. There are no implied licenses under the Agreement, and any rights not expressly granted to the User hereunder are reserved by EXNCE.COM.
    </p>
    <li>JURISDICTION AND DISPUTE RESOLUTION</li>
    <p>
        To resolve any dispute, controversy or claim arising out of or relating to this Agreement, or the breach thereof, the Parties agree first to negotiate in good faith for a period of not less than sixty (60) days following written notification of such controversy or claim to the other Party.
    If the negotiations do not resolve the dispute, controversy or claim to the reasonable satisfaction of all Parties during such period, then the Parties irrevocably and unconditionally submit to the exclusive jurisdiction under the applicable laws. This Agreement is intended to fully reflect the terms of the original agreement between the Parties. No provision of the Agreement shall be considered waived unless such waiver is in writing and signed by the Party that benefits from the enforcement of such provision.
        US citizens and residents are not allowed to participate in the ICO due to US Securities Law restrictions. We respect all country laws in all locations we operate, and urge you to obtain more information regarding specific financial laws in your jurisdiction.</p>
    <p>
    You can only participate in the ICO if you are neither a U.S. citizen or permanent resident of the United States, nor have a primary residence or domicile in the United States, including Puerto Rico, the U.S. Virgin Islands, and any other territories of the United States.
    No waiver of any provision in the Agreement, however, will be deemed a waiver of a subsequent breach of such provision or a waiver of a similar provision. In addition, a waiver of any breach or a failure to enforce any term or condition of the Agreement will not in any way affect, limit, or waive a Party’s rights hereunder at any time to enforce strict compliance thereafter with every term and condition hereof.
    EXNCE.COM may, at its sole discretion, assign its rights and/or delegate its duties under this Agreement. You may not assign your rights or delegate your duties, and any assignment or delegation without the written consent of EXNCE.COM.
    User may send any questions regarding the use of the Website of the EXNCE Tokens or regarding this Agreement via e-mail to support@exnce.com.
    </p>
</ul>

                </div>
            </div>
        </div>
    </div>
@endsection
