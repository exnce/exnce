@extends(Auth::check() ? 'layouts.personal' : 'layouts.site')
@section("title",_i("Privacy Policy"))
@section('content')

    <div class="row mb-5">
        <div class="col-12">
            <div class="card rounded  border-0 box-shadow pt-3 px-3 py-3 pb-5">

                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true" aria-controls="collapseCrypto">
                        <h5>{{ _i('Privacy Policy') }}</h5>
                    </a>
                </div>


                <div class="card-body text-justify">
<p>
While visiting our website or requesting information from EXNCE you provide us with personal confidential information. EXNCE uses the information you provided only for internal procedures and other relevant records of information, as well as forthe fulfillment of your requests or instructions. We respect the confidentiality of your personal data and do not share such information with third parties unless only it is necessary to fulfill your requests.
</p>
                    <p>
If you provide us with personal information, such as postal address, e-mail address, telephone or fax numbers, demographic or customer data, we will not disclose this information, will not transfer, sell or disclose it to any third parties without prior notice and consent from you, unless we are obliged to do so by virtue of existing legal norms.
</p>
<p>
All personal information will be stored in accordance with our strict safety standards and privacy principles.
</p> </br>
                    <p>
After registration you are responsible for keeping your password confidential. Under no circumstances should you disclose the password to others. Account information and passwords can be stored in cookies. Please refer to your browser's support to receive more information about cookies. Our website uses cookies. You can disable the use of cookies in your browser.
                    </p></br>
                    <p>
You agree to protect and indemnify us for any loss or damage that we may incur as a result of your breach of this clause
                    <p>
                </div>
        </div>
    </div>

@endsection
