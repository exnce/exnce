@extends('layouts.site')
@section("title",_i("Registration"))
@section('content')
    <section class="mt-3 container">
        <div class="col">
            <div class="card rounded border-0 box-shadow h-100">



                <div class="card-body text-center">
                    <img width="200px" height="200px" src="{{asset("images/logo_icon.svg")}}"/>

                    <h2 class="text-center text-warning">{{ _i("EXNCE not available in your country...") }}</h2>
                    <h3 class="text-center">{{ _i("We will be with you again in a very short time...") }}</h3>
                    <br/>
                    <small>
                        Please feel free to contact us at any time by sending an email to <b class="text-danger">support@exnce.com</b>.</small>
                   <br/>
                    <small>
                        We welcome any comments, concerns, or questions that you ...
                    </small>

                </div>
            </div>
        </div>
    </section>
@endsection
