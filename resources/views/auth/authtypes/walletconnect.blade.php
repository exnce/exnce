<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header"><h5>{{ _i('Connect EXNCE via WalletConnect') }}</h5></div>

    <div class="card-body">

        <h2 id="the-metamask-browser-extension">Introduction</h2>
        <p><em>If you already know what WalletConnect is, feel free to skip this section.</em></p>
        <p>
            WalletConnect is an open protocol for connecting desktop Dapps to mobile Wallets using end-to-end encryption by scanning a QR code. Opening up a whole world of Dapps that were once only available to Metamask. The user can interact with any Dapp without comprising their private keys and will be notified to sign any transaction requests on their mobile.

        </p>
    </div>
</div>
