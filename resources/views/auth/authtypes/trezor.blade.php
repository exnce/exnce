<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header"><h5>{{ _i('Connect EXNCE via Trezor') }}</h5></div>

    <div class="card-body">
        <h2 id="the-metamask-browser-extension">Introduction</h2>
        <p><em>If you already know what Trezor is, feel free to skip this section.</em></p>
        <p><b><span class="tw-content-def">Trezor Connect</span></b> is a platform for easy integration of login with <a rel="nofollow" target="_blank" href="https://wiki.trezor.io/Trezor" title="Trezor">Trezor</a> into websites and applications.
        </p>
        <p>Trezor is known as the most secure cryptocurrency hardware wallet. Trezor Connect expands its application to the most secure user authentication device. It allows the user to login without any password. It is immune to keyloggers or phishing and provides a simple fluid interface for users with basic computer skills.
        </p>
        <p>Within the first four months since official release, the device was embraced by users and security experts in over 70 countries.
        </p>
    </div>
</div>
