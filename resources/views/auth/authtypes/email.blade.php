<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header"><h5>{{ _i('Sign In') }}</h5></div>

    <div class="card-body">
        <form method="POST" action="{{ route('login') }}" aria-label="{{ _i('Sign In') }}">
            @csrf

            @if(session("login:error"))
                <div class="row mb-0">
                    <div class="alert alert-danger">{{session("login:error")}}</div>
                </div>
            @endif

            <div class="form-group row">
                <div class="col-12">
                    <label for="email">{{ _i("E-Mail") }}</label>

                    <input id="email" type="email"
                           class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} bordered-input"
                           name="email" value="{{ old('email') }}" required autofocus>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <div class="col-12">
                    <label for="password">{{ _i('Password') }}</label>

                    <input id="password" type="password"
                           class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} bordered-input"
                           name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <div class="col-6">
                    <div class="checkbox d-inline">
                        <label>
                            <input type="checkbox"
                                   name="remember" {{ old('remember') ? 'checked' : '' }}> {{ _i('Remember me') }}
                        </label>
                    </div>
                </div>
                <div class="col-6">
                    <a class="btn btn-link p-0 float-right" href="{{ route('password.request') }}">
                        {{ _i('Forgot your password?') }}
                    </a>
                </div>

            </div>

            <div class="form-group row">
                <div class="col-12">
                    <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"  data-callback="onSubmit"></div>
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                    @endif
                </div>
            </div>

            <div class="form-group row">
                <div class="col-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        {{ _i('Sign In') }}
                    </button>
                </div>
                <div class="col-6">
                    <a class="btn btn-success btn-block" href="{{ route('register') }}">
                        {{ _i('Register') }}
                    </a>
                </div>
            </div>
        </form>
    </div>
</div>
