<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header">
        <h5>{{ _i('Connect EXNCE via Metamask') }}</h5>
    </div>

    <div class="card-body pt-0">


        <div class="row">
            <div class="col-12">
                <h2 id="the-metamask-browser-extension">The MetaMask Browser Extension</h2>
                <p><em>If you already know what MetaMask is, feel free to skip this section.</em></p>
                <p><a href="https://metamask.io" rel="noopener noreferrer">MetaMask</a> is a browser plugin, available as the <a href="https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn" rel="noopener noreferrer">MetaMask Chrome extension</a> or <a href="https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/" rel="noopener noreferrer">Firefox Add-on</a>. At its core, it serves as an Ethereum wallet: By installing it, you will get access to a unique Ethereum public address, with which you can start sending and receiving ether or tokens.</p>
            </div>

            <div class="col-12">
                <div class="input-group mb-3 pr-0 market-sell">
                    <input type="text" id="edtAddress" name="edtAddress" readonly="readonly" class="form-control text-center">
                </div>
                <button onclick="checkMetamask()" id="metamaskLogin" name="metamaskLogin" class="btn btn-success btn-lg btn-block">
                    Connect to Metamask
                </button>
            </div>

        </div>
    </div>

    <div class="card-footer bg-white">
        <div class="row">

            <div class="col-12">
                <div role="alert" aria-live="polite" aria-atomic="true" class="alert fade show alert-danger"><i class="fa fa-bullhorn text-danger"></i><span> Heads up!</span></div>

            </div>
            <div class="col-12">
                <span>We need to verify the ownership of your account by having you sign a message. This will help to ensure your transaction information stays secure and private.</span>
            </div>
        </div>
    </div>
</div>
