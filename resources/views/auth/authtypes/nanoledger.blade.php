<div class="card rounded border-0 box-shadow h-100">
    <div class="card-header"><h5>{{ _i('Connect EXNCE via Nano Ledger') }}</h5></div>

    <div class="card-body">
        <h2 id="the-metamask-browser-extension">Introduction</h2>
        <p><em>If you already know what Nano Ledger is, feel free to skip this section.</em></p>

        <div  role="alert" aria-live="polite" aria-atomic="true" class="alert fade show alert-danger"><!---->Please make sure your device is connected.</div>

        <ol>
            <li>Connect and unlock your Ledger device.</li>
            <li>Open the app of the crypto asset to manage.</li>
            <li>Go to&nbsp;<a href="https://vintage.myetherwallet.com/" target="_self">vintage.MyEtherWallet.com</a>.</li>
            <li>Select the crypto asset network in the top-right corner. Ethereum is selected by default.</li>
            <li>Click the <strong>Send Ether &amp; Tokens</strong>&nbsp;tab.</li>
            <li>Click&nbsp;<strong>Ledger Wallet</strong>.</li>
            <li>Click the&nbsp;<strong>Connect to Ledger Wallet</strong> button.</li>
        </ol>
    </div>
</div>
