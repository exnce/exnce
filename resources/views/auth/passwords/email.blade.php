@extends('layouts.site')
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section("title", _i("Forgot password"))
@section('content')
    <section class="mt-3 container mb-5">
        <div class="row py-3 justify-content-center">
            <div class="col-md-8">
                <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                    <div class="card-header"><h5>{{ _i('Forgot password') }}</h5>
                        <span class="d-none d-lg-block d-md-block text-danger">{{_i("Enter your e-mail address and click send. You will receive an email containing a password renewal link")}}</span>
                    </div>

                    <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <form method="POST" action="{{ route('password.email') }}" aria-label="{{ _i('Forgot password') }}">
                        @csrf

                        <div class="form-group row">

                            <label for="email" class="col-12 col-form-label">{{ _i('E-Mail') }}</label>

                            <div class="col-md-6 mb-2">
                                <input id="email" type="email"
                                       class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} bordered-input"
                                       name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="col-lg-6 col-sm-12 mb-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                    {{ _i('Send') }}
                                </button>
                            </div>


                            <div class="col-md-6 mb-2">
                            <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"  data-callback="onSubmit"></div>
                            @if ($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback" style="display: block;">
                                        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                    </span>
                            @endif
                            </div>
                            <div class="col-lg-6 col-xs-12">
                                <div class="row">


                                    <div class="col-6 mb-2">
                                        <a class="btn btn-success btn-block" href="{{ route('login') }}">
                                            {{ _i('Sign In') }}
                                        </a>
                                    </div>
                                    <div class="col-6">
                                        <a class="btn btn-danger btn-block" href="{{ route('register') }}">
                                            {{ _i('Sign Up') }}
                                        </a>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </form>
                </div>
            </div>
        </div>
        </div>
    </section>
@endsection
