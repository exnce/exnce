@extends('layouts.site')
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section("title",_i("Sign In"))
@section('content')
    <section class="mt-3 container mb-5">

        <div class="row">
            <div class="col-12 col-lg-5 m-md-0 p-0">
                <div class="login-left">
                    <div class="login-left-logo d-flex align-items-center justify-content-between">
                                 <div class="logo-icon">
                                    <img id="logo-loading" name="logo-loading" width="200px" height="200px" src="{{asset("/images/logo_icon.svg")}}" alt="{{_("EXNCE Logo")}}" class="img-fluid">
                                </div>

                    </div>
                    <div class="row no-gutters">
                        <div class="col-12">
                            <div class="login-left-content mt-5">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-7 m-md-0 p-md-0">
                <div class="row p-0 m-0">
                <div class="col-lg-3 col-sm-12 panel list-panel p-0 box-shadow">
                    <ul class="mail-list d-block text-center nav  nav-pills">
                        <li class="label-color-orange">
                            <a class="nav-link active"   data-toggle="pill" href="#v-auth-metamask" role="tab">
                            <div class="name"><img width="100%" height="48px" src="{{asset("/images/metamask.svg")}}"/> </div>
                            <div class="preview">Connect to the MetaMask browser wallet</div>
                            </a>
                        </li>
                        <li class="label-color-green">
                            <a class="nav-link"  data-toggle="pill" href="#v-auth-wallet" role="tab">
                            <div class="name"><img  width="100%" height="40px" src="{{asset("/images/walletConnect.svg")}}"/> </div>
                            <div class="preview">Scan a QR code to link your mobile wallet using WalletConnect</div>
                            </a>
                        </li>
                        <li class="label-color-purple">
                            <a class="nav-link"  data-toggle="pill"  href="#v-auth-trezor" role="tab">
                            <div class="name"><img  width="100%" height="40px" src="{{asset("/images/trezor.svg")}}"/> </div>
                            <div class="preview">Use Trezor hardware wallet to connect.</div>
                            </a>
                        </li>
                        <li class="label-color-purple">
                            <a class="nav-link"  data-toggle="pill"  href="#v-auth-nano" role="tab">
                                <div class="name"><img  width="100%" height="40px" src="{{asset("/images/ledger.svg")}}"/> </div>
                                <div class="preview">Use Nano Ledger hardware wallet to connect.</div>
                            </a>
                        </li>
                        <li class="label-color-blue">
                            <a class="nav-link" data-toggle="pill" href="#v-auth-email">
                            <div class="name"><img width="100%" height="40px" src="{{asset("/images/email.svg")}}"/> </div>
                            <div class="preview">Use your email and password to connect</div>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-9 col-sm-12 p-0 mb-5">
                    <div class="tab-content" id="v-pills-tabContent">
                        <div class="tab-pane fade active show" id="v-auth-metamask" role="tabpanel">
                            @include("auth.authtypes.metamask")
                        </div>
                        <div class="tab-pane fade" id="v-auth-wallet" role="tabpanel">
                            @include("auth.authtypes.walletconnect")
                        </div>
                        <div class="tab-pane fade" id="v-auth-trezor" role="tabpanel">
                            @include("auth.authtypes.trezor")
                        </div>
                        <div class="tab-pane fade" id="v-auth-nano" role="tabpanel">
                            @include("auth.authtypes.nanoledger")
                        </div>
                        <div class="tab-pane fade" id="v-auth-email" role="tabpanel">
                            @include("auth.authtypes.email")
                        </div>
                    </div>
                </div>
                </div>


            </div>
        </div>




    </section>
@endsection


