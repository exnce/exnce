
@extends('layouts.site')
@section('head')
    <script src='https://www.google.com/recaptcha/api.js'></script>
@endsection
@section("title",_i("Sign Up"))
@section('content')
<section class="mt-3 container mb-5">
    <div class="row py-3 justify-content-center">
        <div class="col-md-8">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5>{{ _i('Sign Up') }}</h5></div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" aria-label="{{ _i('Sign Up') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ _i('Name Lastname') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }} bordered-input" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ _i('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} bordered-input" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ _i('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} bordered-input" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ _i('Password (repeat)') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control bordered-input" name="password_confirmation" required>
                            </div>
                        </div>

                        @if($has_ref == true)
                        <div class="form-group row hidden">
                            <label for="reference_name" class="col-md-4 col-form-label text-md-right">{{ _i('Refferer') }}</label>

                            <div class="col-md-6">
                                <input id="reference_name" type="text" class="form-control bordered-input" name="reference_name" readonly="readonly" disabled="disabled" value="{{ get_starred($user->name) }}"/>
                            </div>
                        </div>
                        <div class="form-group row hidden">
                            <label for="reference_code" class="col-md-4 col-form-label text-md-right">{{ _i('Refferer') }}</label>

                            <div class="col-md-6">
                                <input id="reference_code" type="text" class="form-control bordered-input" name="reference_code" value="{{ $user->ref_code }}"/>
                            </div>
                        </div>
                        @endif

                        @if($has_campain == true)
                            <div class="form-group row hidden">
                                <label for="campain_code" class="col-md-4 col-form-label text-md-right">{{ _i('Campain') }}</label>

                                <div class="col-md-6">
                                    <input id="campain_code" type="text" class="form-control bordered-input" name="campain_code" readonly="readonly" value="{{ $campain_code }}"/>
                                </div>
                            </div>
                            <div class="form-group row hidden">
                                <label for="callback_url" class="col-md-4 col-form-label text-md-right">{{ _i('CallBack URL') }}</label>

                                <div class="col-md-6">
                                    <input id="callback_url" type="text" class="form-control bordered-input" readonly="readonly" name="callback_url" value="{{ $callback_url  }}"/>
                                </div>
                            </div>
                        @endif


                        <div class="form-group row">
                            <div class="offset-md-4 col-md-6">
                            <label class="custom-control custom-checkbox">
                                <input id="agreement" name="agreement" type="checkbox" class="form-check-input">
                                <span class="custom-control-indicator"></span>
                                <span class="custom-control-description text-dark d-felx"><a href="{{ route("user-agreement") }}" target="_blank">{{ _i("I have read and agreed to the Terms of Use.") }}</a></span>
                                @if ($errors->has('agreement'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('agreement') }}</strong>
                                </span>
                                @endif

                            </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="g-recaptcha" data-sitekey="{{ env('CAPTCHA_SITE_KEY') }}"  data-callback="onSubmit"></div>
                                @if ($errors->has('g-recaptcha-response'))
                                <span class="invalid-feedback" style="display: block;">
                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-block btn-primary">
                                    {{ _i('Sign Up') }}
                                </button>

                                <a class="btn btn-success btn-block" href="{{ route('login') }}">
                                    {{ _i('Sign In') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
