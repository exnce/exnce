@extends('layouts.site')
@section("title",_i("Under Maintenance"))
@section('content')
    <section class="mt-3 container">
        <div class="col">
            <div class="card rounded border-0 box-shadow h-100">
                <div class="card-header">
                    <h4 class="mb-0 text-center text-danger">{{_i("Under Maintenance")}}<span class="pull-right"><i
                                    class="fa fa-angle-double-down"></i></span></h4>
                </div>

                <div class="card-body text-center">
                    <img width="200px" height="200px" src="{{asset("images/logo_icon.svg")}}"/>

                    <p class="text-center">{{ _i("Right now; In order to serve you better, we make updates on EXNCE.We will be with you again in a very short time...") }}</p>
                    <h2 class="text-center">{{ _i("We will be with you again in a very short time...") }}</h2>

                </div>
            </div>
        </div>
    </section>
@endsection
