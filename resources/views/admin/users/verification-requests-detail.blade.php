<form id="frmApprovalRequestDetailForm" name="frmApprovalRequestDetailForm" method="POST" action="#">
    @csrf

    <input type="hidden" name="approvalId" value="{{ $approval->id }}">
    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Müşteri: ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $approval->user->name }}" disabled>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Açıklama: ")}}</label>
        <textarea name="description" cols="5" rows="5" type="text"
                  class="form-control rounded-s-soft  bordered-input colored-placeholder mb-2"
                  data-allow_clear="1">{{ $approval->description }}</textarea>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Onay : ")}}</label>
        <select name="status" class="form-control rounded-s-soft bordered-input">
            @foreach(getIdentityApprovalStatusCodes() as $status => $val)
                <option value="{{ $status }}" {{ $status==$approval->status ? "selected" : "" }}>{{{ $val }}}</option>
            @endforeach
        </select>
    </div>

</form>
