@extends('layouts.admin')
@section("title",_i("Kullanıcı Listesi"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="/admin/users/confirm">
                @csrf
                <button type="submit">Confirm All</button>
            </form>



            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Kullanıcı Listesi</div>
                <div class="card-body pr-0 pl-0">
                    <table id="userList" name="userList" class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Ad Soyad</td>
                            <td>Email</td>
                            <td>Telefon</td>
                            <td>Yetki</td>
                            <td>Onay</td>
                            <td>Durum</td>
                            <td>İşlem</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
