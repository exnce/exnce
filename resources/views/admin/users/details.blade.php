@extends('layouts.admin')
@section("title",_i("Kullanıcı Listesi"))
@section('content')

    <div class="row">
        <div class="col-12">
            <div class="card rounded border-0 box-shadow pt-3 px-3 py-3 w-100">
                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                       aria-controls="collapseCrypto">
                        <h5 class="mb-0">{{ _i("Hesap Bilgileri") }}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h5>
                        <span class="d-none d-lg-block d-md-block">{{ _i("Kullanicinin hesap bilgilerini ozeti.") }}</span>
                    </a>
                </div>
                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                <div class="card-body">
                    <form method="post" action="/admin/users/detail/{{$user->id}}/updateUser">
                        @csrf
                    <div class="row">



                        <div class="col-lg-3 col-sm-12">
                        <div class="form-group">
                            <label for="name">Name:</label>
                            <input class="form-control" name="name" type="text" value="{{$user->name}}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email:</label>
                            <input class="form-control" name="email" type="text" value="{{$user->email}}">
                        </div>
                            </div>

                            <div class="col-lg-3 col-sm-12">
                        <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input class="form-control" name="phone" type="text" value="{{$user->phone}}">
                        </div>
                            <div class="form-group">
                                <label for="country_id">Yetki:</label>
                                <input class="form-control" name="user_type" type="text" value="{{$user->user_type}}">
                            </div>
                            </div>

                            <div class="col-lg-3 col-sm-12">

                        <div class="form-group">
                            <label for="status">Status:</label>
                            <input class="form-control" name="status" type="text" value="{{$user->status}}">
                        </div>

                        <div class="form-group">
                            <label for="status">Google Authenticator:</label>
                            <input class="form-control" id="two_factor" name="two_factor" type="text" value="{{$user->two_factor}}">
                        </div>


                                <div class="form-group">
                            <label for="verified">Verified:</label>
                            <input class="form-control" name="verified" type="text" value="{{$user->verified}}">
                        </div>

                                <div class="form-group">
                                    <label for="reference_id">ReferenceId:</label>
                                    <input class="form-control" name="reference_id" type="text" value="{{@$user->reference->reference_user->name}}">
                                </div>
                            </div>

                        <div class="col-lg-3 col-sm-12">
                        <div class="form-group">
                            <button class="form-control btn-success" name="btn-save" type="submit">Kaydet</button>
                        </div>

                            <div class="form-group">
                                <a href="{{route("user-login",$user->id)}}" target="_blank" class="form-control btn-success" name="btn-login-as-user">Login As Him</a>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-2">
            <div class="col-12">

                <div class="card rounded border-0 box-shadow pt-3 px-3 py-3 w-100">
                    <div class="card-header">
                        <a data-toggle="collapse" data-target="#walletDetails" aria-expanded="true"
                           aria-controls="walletDetails">
                            <h5 class="mb-0">{{ _i("Cüzdan Detaylari") }}<span class="pull-right"><i
                                            class="fa fa-angle-double-down"></i></span></h5>
                            <span class="d-none d-lg-block d-md-block">{{ _i("Kullanicinin cuzdan ve bakiye bilgileri.") }}</span>
                        </a>
                    </div>
                    <div id="walletDetails" class="collapse show" aria-labelledby="headingOne">

                    <div class="row mt-2">
                        <div class="col-12">
                        <table class="table display table-striped noborderradius nowrap no-footer collapsed table-hover">
                            <thead>
                            <tr>
                                <th width="100px">Coin</th>
                                <th width="200px">Cuzdan Adi</th>
                                <th>Cuzdan Adresi</th>
                                <th width="200px">Kullanilabilir</th>
                                <th width="200px">Islemde</th>
                                <th width="200px">ToplamBakiye</th>
                                <th width="150px">Islem</th>
                            </tr>
                            </thead>

                        <tbody>
                        @foreach($wallets as $wallet)
                                <tr>
                                    <form method="post" action="/admin/users/detail/{{$user->id}}/updateWallet">
                                        @csrf
                                    <td class="form-group">

                                        <div class="kt-coin-card-v2">
                                            <div class="kt-coin-card-v2__pic">
                                                <img src="{{asset("/images/coins/".strtolower($wallet->coin->slug).".svg")}}">
                                            </div>
                                            <div class="kt-coin-card-v2__details"><a class="kt-coin-card-v2__name" href="#">
                                                    {{ $wallet->coin->symbol }}
                                                </a> <span class="kt-coin-card-v2__email">{{$wallet->coin->name}}</span>
                                            </div>
                                        </div>
                                        <input class="form-control hidden" name="id" type="text" value="{{$wallet->id}}">
                                        <input class="form-control hidden " name="type" type="text" value="{{$wallet->type}}">
                                        <input class="form-control hidden" name="coin_id" type="text"
                                               value="{{$wallet->coin_id}}">

                                    </td>
                                    <td class="form-group">
                                        <input class="form-control" name="name" type="text" value="{{$wallet->name}}">
                                    </td>
                                    <td class="form-group">
                                        <input class="form-control" name="address" type="text"
                                               value="{{$wallet->address()->get()->last()->address}}">
                                    </td>

                                    <td class="form-group">
                                        <input class="form-control" type="text"
                                               value="{{$wallet->balance}}">
                                    </td>

                                    <td class="form-group">
                                        <input class="form-control" type="text"
                                               value="{{$wallet->processingBalance}}">
                                    </td>

                                    <td class="form-group">
                                        <input class="form-control" name="balance" type="text"
                                               value="{{$wallet->fixedBalance}}">
                                    </td>

                                    <td class="form-group">
                                        <button class="form-control btn-success" name="btn-save" type="submit">Kaydet
                                        </button>
                                    </td>
                                    </form>
                                </tr>
                        @endforeach
                        </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
