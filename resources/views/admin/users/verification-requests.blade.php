@extends('layouts.admin')
@section("title",_i("Kimlik Onayları"))
@section('content')
    <div class="row">
        <div class="col-md-12">


            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Kimlik Onayları</div>
                <div class="card-body">
                    <table id="identityApprovals" name="identityApprovals"
                           class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Kullanıcı adı</td>
                            <td>Kimlik ön yüz</td>
                            <td>Özçekim</td>
                            <td>Durum</td>
                            <td>Son Güncelleme</td>
                            <td>Detay</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="frmApprovalDetails" tabindex="-1" role="dialog" aria-labelledby="frmApprovalDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Kimlik Onayı Detaylari") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body approval-details">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-identity-approve  btn-success">{{ _i("Güncelle") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
