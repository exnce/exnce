@extends('layouts.admin')
@section("title",_i("Kullanıcı Cüzdanları"))
@section('content')
    <div class="row">
        <div class="col-md-12">


            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Kullanıcı Cüzdanları</div>
                <div class="card-body">
                    <table id="" name=""
                           class="table table-striped table-hover table-sm table-condensed dataTable w-100">
                        <thead>
                        <tr>
                            <th>Üye no</th>
                            <th>Üye adı</th>
                            <th>Cüzdan</th>
                            <th>Cüzdan adresi</th>
                            <th>Balans</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($wallets as $wallet)
                            <tr>
                                <td>{{ $wallet->user_id + 10000 }}</td>
                                <td>{{ $wallet->user->name }}</td>
                                <td>{{ $wallet->coin->symbol }}</td>
                                <td>{{ $wallet->address()->get()->last()->address }}</td>
                                <td>{{ $wallet->balance }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@endsection
