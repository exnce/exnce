@extends('layouts.admin')
@section("title",_i("Iletisim Talepleri"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Iletisim Listesi</div>
                <div class="card-body pr-0 pl-0">
                    <table id="contactList" name="contactList" class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Ad </td>
                            <td>Soyad</td>
                            <td>Email</td>
                            <td>Message</td>
                            <td>Status</td>
                            <td>Tarih</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
