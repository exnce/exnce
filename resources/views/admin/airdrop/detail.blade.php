@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Airdrop Details</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">


                            <table id="listAirdropDetails" name="listAirdrops" style="100%; margin-top: 10px !important;" cellspacing="0"
                                   class="table display dataTable table-striped noborderradius nowrap responsive">
                                <thead class="table-head text-left font-weight-bold">
                                <tr>
                                    <th>{{ _i("Name") }}</th>
                                    <th>{{ _i("Email") }}</th>
                                    <th>{{ _i("Joined") }}</th>
                                    <th>{{ _i("Has Wallet") }}</th>
                                    <th>{{ _i("Action") }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($users as $user)
                                    <tr>
                                        <td>{{$user->name}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                            @php
                                                $wallet=$user->wallets()->where("type", false)->where("coin_id", $airdrop->coin_id)->get()->first();
                                                if($wallet){

                                                    $walletAddress = $wallet->address()->get()->last();
                                                    if($walletAddress){
                                                        $payment_address = $walletAddress->address;
                                                    }else{
                                                        $payment_address = "-";
                                                    }
                                                }else{
                                                    $payment_address = "-";
                                                }
                                            @endphp
                                            {{$payment_address}}</td>
                                        <td>{{$user->pivot->created_at}}</td>
                                        <td>
                                            <form method="POST" action="{{route("admin-airdrop-detail",$airdrop->id)}}">
                                                @csrf
                                                <input type="text" class="form-control" value="{{$user->id}}" name="userId"/>
                                                <input type="text" class="form-control" value="1000" name="amount"/>
                                                 <button type="submit" class="btn btn-sm btn-success">Save</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection
