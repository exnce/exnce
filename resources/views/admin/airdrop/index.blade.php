@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">Airdrops</div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">


                            <table id="listAirdrops" name="listAirdrops" style="100%; margin-top: 10px !important;" cellspacing="0"
                                   class="table display dataTable table-striped noborderradius nowrap responsive">
                                <thead class="table-head text-left font-weight-bold">
                                <tr>
                                    <th>{{_i("Owner")}}</th>
                                    <th>{{ _i("Name") }}</th>
                                    <th>{{ _i("Symbol") }}</th>
                                    <th>{{ _i("Model") }}</th>
                                    <th>{{ _i("Ends In") }}</th>
                                    <th>{{ _i("Token Amount") }}</th>
                                    <th>{{ _i("Followers") }}</th>
                                    <th>{{ _i("Rating") }}</th>
                                    <th>{{ _i("Action") }}</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($airdrops as $airdrop)
                                    <tr>
                                        <td>{{$airdrop->owner->name}}</td>
                                        <td><img src="{{asset("/images/coins/".strtolower($airdrop->coin->slug).".svg")}}"
                                                 width="24px"/>{{ $airdrop->name }}</td>
                                        <td>{{ $airdrop->coin->symbol }}</td>
                                        <td>{{ $airdrop->model }}</td>
                                        <td>{{ $airdrop->finish_date  }}</td>
                                        <td>{{ $airdrop->amount }}</td>
                                        <td>{{ $airdrop->followers()->get()->count() }}</td>
                                        <td>{{ number_format((float)$airdrop->ratingPercent(5), 2, '.', '')}}</td>
                                        <td>
                                            <a href="{{route("admin-airdrop-detail",$airdrop->id)}}" data-id="{{$airdrop->id}}" class="btn btn-success btn-sm btn-airdrop-details">{{_i("Details")}}</a>
                                            <button data-id="{{$airdrop->id}}" class="btn btn-success btn-sm btn-airdrop-details">{{_i("Distribute")}}</button>
                                            <button data-id="{{$airdrop->id}}" class="btn btn-success btn-sm btn-airdrop-details">{{_i("Finish")}}</button>
                                        </td>
                                        <td></td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>

                        </div>
                    </div>



                </div>
            </div>
        </div>
    </div>
@endsection
