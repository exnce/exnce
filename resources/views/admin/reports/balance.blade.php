
@extends('layouts.admin')
@section("title",_i("Reports"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5 class="pull-left pt-2">{{_i("Balance Reports")}}</h5></div>
                <div class="card-body">
                    <div class="row" lang="en">
                         <table class="table display table-striped noborderradius nowrap no-footer collapsed table-hover dataTable">
                               <thead class="table-head font-weight-bold border-0">
                                <tr>
                                    <th>Asset</th>
                                    <th>Symbol</th>
                                    <th>Total Balance</th>
                                    <th>Total Deposit</th>
                                    <th>Total Withdraw</th>
                                    <th>Bid Fee</th>
                                    <th>Ask Fee</th>
                                    <th>Total Fee</th>
                                    <th>Cold Storage</th>
                                </tr>
                               </thead>
                               <tbody>

                                   @foreach($coins as $coin)
                                   <tr>
                                       <td>{{$coin->name}}</td>
                                       <td>{{$coin->symbol}}</td>
                                       <td class="text-right text-danger">{{$coin->total_balance}}</td>
                                       <td class="text-right text-primary">{{$coin->total_deposit}}</td>
                                       <td class="text-right text-secondary">{{$coin->total_withdraw}}</td>
                                       <td class="text-right text-dark-sky">{{$coin->total_buy_fee}}</td>
                                       <td class="text-right text-dark-sky">{{$coin->total_sell_fee}}</td>

                                       <td class="text-right text-warning">{{$coin->total_fee}}</td>
                                       <td class="text-right text-success">{{$coin->cold_storage}}</td>
                                   </tr>
                                   @endforeach

                               </tbody>
                           </table>


                    </div>
                </div>
            </div>
        </div>

@endsection
