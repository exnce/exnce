
@extends('layouts.admin')
@section("title",_i("Reports"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5 class="pull-left pt-2">{{_i("Reports")}}</h5></div>
                <div class="card-body">
                    <div class="row text-center">

                        <div class="col-3">
                            <a href="{{route("balance-reports")}}">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fab fa-bitcoin text-primary"></i>
                                </div>
                                <div class="numbers">
                                    <p class="card-category">{{_i("Balance Report")}}</p>
                                </div>
                            </a>
                        </div>









                    </div>
                </div>
            </div>
        </div>

@endsection
