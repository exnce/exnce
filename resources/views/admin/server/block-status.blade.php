
@extends('layouts.admin')
@section("title",_i("Block Track Islemleri"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">{{ _i("Block Track Islemleri") }}</div>
                <div class="card-body">
                    <table id="blockTracsk" name="blockTracsk" class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>{{ _i("Network Type") }}</td>
                            <td>{{ _i("Block Number") }}</td>
                            <td>{{ _i("Tarih") }}</td>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach($blocks as $block)
                                <tr>
                                    <td>{{$block->network_type}}</td>
                                    <td>{{$block->blockNumber}}</td>
                                    <td>{{$block->created_at}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
