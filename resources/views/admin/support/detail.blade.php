@extends('layouts.admin')
@section("title",_i("Destek Talepleri"))


@section("content")
    <div class="container pt-3">
        <div class="row pb-4">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 w-100">
                <div class="card-header" id="cryptoCurrencyCard">
                    <a data-toggle="collapse" data-target="#collapseCrypto" aria-expanded="true"
                       aria-controls="collapseCrypto">
                        <h5 class="mb-0">{{ _i("Destek Taleplerim") }}<span class="pull-right"><i
                                        class="fa fa-angle-double-down"></i></span></h5>
                        <span class="d-none d-lg-block d-md-block">{{ _i("Yeni bir destek talebi isteyebilir, mevcut destek taleplerinizi detaylı olarak görebilirsiniz.") }}</span>
                    </a>
                </div>
                <div id="collapseCrypto" class="collapse show" aria-labelledby="headingOne">
                    <div class="card-body pl-0 pr-0 pb-0">
                        <div class="row">

                            <div class="col-12 mb-3">

                                <nav class="nav nav-pills flex-column flex-sm-row">
                                    <a class="text-sm-center nav-link {{!request()->segment(3) ? "active" : ""}}"
                                       href="{{route("admin-support-requests")}}">{{_i("Açık Destek Talepleri")}}
                                        <span class="badge badge-pill badge-warning ">{{$unsolved}}</span>
                                    </a>

                                    <a class="text-sm-center nav-link {{request()->segment(3) == "completed" ? "active" : ""}}"
                                       href="{{route("admin-support-completed")}}">{{_i("Çözümlenmiş Talepler")}}
                                        <span class="badge badge-pill badge-success">{{$solved}}</span>
                                    </a>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 w-100 mb-5">

                @if(session()->has('message'))
                    <div class="col-12">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session()->get('message')}}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endif


                    <div class="row">
                        <div class="col-12">

                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div class="content">
                                        <h2 class="header text-uppercase">
                                            <i class="fas fa-file-signature text-danger"></i> {{$ticket->subject}}

                                            <span class="pull-right">

                                                <a href="{{route("admin-support-reopen",$ticket->id)}}"
                                                   class="btn btn-success">{{ _i("Yeniden Aç") }}</a>
                                                </span>

                                        </h2>
                                        <hr/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-2 col-sm-12">
                                     <div class="row p-1">
                                        <div class="col-md-12">
                                            <p>
                                                <strong>{{_i("Durum")}}</strong>: </p>
                                            <p>
                                                <span style="color: {{$ticket->status->color}}">{{_i($ticket->status->name) }}</span>
                                            </p>

                                            <p>
                                                <strong>{{_i("Önemlilik Derecesi")}}</strong>:
                                            </p><p>
                                                <span style="color: #e1d200">
                                                            {{ _i($ticket->priority->name)}}
                                                        </span>
                                              </p>
                                        </div>
                                        <div class="col-md-12">
                                            <p>
                                                <strong>{{_i("Yetkili")}}</strong>:</p>
                                            <p> {{ $ticket->agent != null ? $ticket->agent->name : "-" }}
                                            </p>
                                            <p>
                                                <strong>{{_i("Kategori")}}</strong>:</p>
                                            <p>
                                                <span style="color: #7e0099">
                                                      {{_i($ticket->category->name)}}
                                                    </span>
                                            </p>
                                        </div>

                                        <div class="col-md-12">
                                            <p><strong>{{_i("Oluşturulma")}}</strong>:</p><p> {{$ticket->created_at}}</p>
                                            <p><strong>{{_i("Son Güncelleme")}}</strong>:</p><p> {{$ticket->updated_at}}</p>
                                        </div>
                                    </div>
                             </div>


                    <div class="col-lg-10 col-sm-12">

                        <div class="chat_window">
                            <ul class="messages">
                                <li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $ticket->content !!}</div></div></li>


                                @foreach($ticket->comments as $comment)
                                    @if($user->id == $comment->user_id)
                                <li class="message left appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $comment->content !!}</div></div></li>
                                    @else
                                <li class="message right appeared"><div class="avatar"></div><div class="text_wrapper"><div class="text">{!! $comment->content !!}</div></div></li>
                                    @endif
                                @endforeach

                            </ul>
                            <div class="bottom_wrapper clearfix">
                                <form method="POST">
                                    @csrf


                                    <div class="row">

                                        <div class="form-group col-lg-3 col-sm-12">
                                            <label for="prority">{{ _i("Durum:") }}</label>
                                            <select type="text" id="status" class="form-control" name="status">
                                             <option key="" value=""></option>
                                            @foreach($statuslist as $status => $value)
                                                    <option key="{{$value["key"]}}" value="{{$value["key"]}}">{{$value["value"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-lg-3 col-sm-12">
                                            <label for="operator">{{ _i("Operator:") }}</label>
                                            <select type="text" id="operator" class="form-control" name="operator">

                                            </select>
                                        </div>

                                        <div class="form-group col-lg-3 col-sm-12">
                                            <label for="prority">{{ _i("Aciliyet Durumu:") }}</label>
                                            <select type="text" id="priority" class="form-control {{ $errors->has('priority') ? ' is-invalid' : '' }}" name="priority">
                                                @foreach($priorities as $priority => $value)
                                                    <option key="{{$value["key"]}}" value="{{$value["key"]}}" {{ $ticket->priority->key == $value["key"] ? "selected" : "" }}>{{$value["value"]}}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group col-lg-3 col-sm-12">
                                            <label for="category">{{ _i("İlgili Birim:") }}</label>
                                            <select type="text" id="category" class="form-control {{ $errors->has('category') ? ' is-invalid' : '' }}" name="category">

                                                @foreach($categories as $category => $value)
                                                    <option key="{{$value["key"]}}" value="{{$value["key"]}}" {{ $ticket->category->key == $value["key"] ? "selected" : "" }}>{{$value["value"]}}</option>
                                                @endforeach

                                            </select>
                                        </div>


                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-12">
                                            <textarea name="comment" id="comment"></textarea>
                                        </div>
                                    </div>
                                    <button type="submit" class="send_message">
                                    <div class="icon"></div>
                                    <div class="text">Kaydet</div>
                                </button>
                                </form>
                            </div>
                        </div>

                    </div>

            </div>
            </div>
        </div>






        </div>







@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('#comment').summernote({
                toolbar: [
                    ['style', ['bold', 'italic', 'underline']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert',['picture']]
                ],
                height: 180,   //set editable area's height
                codemirror: { // codemirror options
                    theme: 'paper'
                },

            });
        });
    </script>
@endsection