
<form id="frmDepositRequestDetailForm" name="frmDepositRequestDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Deposit Id: ")}}</label>
        <input id="order_id" name="order_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $depositOrder->id }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Müşteri Adı Soyadı : ")}}</label>
        <input id="username" name="username" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $depositOrder->user->name }}">
    </div>

    <div class="row">
        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Müşteri Mevcut Bakiyesi : ")}}</label>
            <input id="balance" name="balance" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$walletBalance}}">
        </div>

        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Talep Tutarı : ")}}</label>
            <input id="amount" name="amount" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$depositOrder->amount}}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{ _i("Yatıracağı Banka :") }}</label>
        <input id="bank_name" name="bank_name" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{  $depositOrder->bank_account->bank->bank_name }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Yatıracağı IBAN :")}}</label>
        <input id="iban" name="iban" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $depositOrder->bank_account->iban }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Transfer Kodu : ")}}</label>
        <input id="transfer_code" name="transfer_code" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $depositOrder->transfer_code }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Açıklama : ")}}</label>
        <textarea id="description" name="description" cols="5" rows="5" type="text"
                  class="form-control rounded-s-soft  bordered-input colored-placeholder mb-2"
                  data-allow_clear="1">{{ $depositOrder->description }}</textarea>
    </div>
</form>
