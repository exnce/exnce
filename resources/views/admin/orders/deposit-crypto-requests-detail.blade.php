
<form id="frmDepositRequestDetailForm" name="frmDepositRequestDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Deposit Id: ")}}</label>
        <input id="order_id" name="order_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $deposit->id }}">
    </div>


    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Müşteri Adı Soyadı : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $owner->name }}">
    </div>

    <div class="row">
        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Müşteri Mevcut Bakiyesi : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$walletBalance}}">
        </div>

        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Talep Tutarı : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$deposit->amount}}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Kripto Para Birimi : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{  $deposit->coin->name }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Cüzdan Adresi : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $deposit->address  }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Destination Tag : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{$deposit->memo_tag}}">
    </div>


    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Transfer Kodu : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $deposit->txid }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Açıklama : ")}}</label>
        <textarea cols="5" rows="5" type="text"
                  class="form-control rounded-s-soft  bordered-input colored-placeholder mb-2"
                  data-allow_clear="1">-</textarea>
    </div>
</form>
