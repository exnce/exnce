
@extends('layouts.admin')
@section("title",_i("Çekim Talepleri"))
@section('content')
    <div class="row">
        <div class="col-md-12">


            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">{{ _i("Çekim Talepleri") }}</div>
                <div class="card-body">
                    <table id="withdrawOrders" name="withdrawOrders"
                           class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>{{ _i("ID") }}</td>
                            <td>{{ _i("Banka") }}</td>
                            <td>{{ _i("Transfer Kodu") }}</td>
                            <td>{{ _i("Yatıran Kullanıcı") }}</td>
                            <td>{{ _i("Miktar") }}</td>
                            <td>{{ _i("Operator") }}</td>
                            <td>{{ _i("Durum") }}</td>
                            <td>{{ _i("İşlem") }}</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="frmWithdrawDetails" tabindex="-1" role="dialog" aria-labelledby="frmWithdrawDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Çekim Talebi Detaylari") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body withdraw-details">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-approve-withdraw  btn-success">{{ _i("Çekimi Onayla") }}</button>
                    <button type="button" class="btn btn-cancel-withdraw  btn-danger">{{ _i("Çekimi İptal Et") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
