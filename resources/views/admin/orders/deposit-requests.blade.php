
@extends('layouts.admin')
@section("title",_i("Yatırım Talepleri"))
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header">{{ _i("Deposit Requests") }}</div>
                <div class="card-body">

                    <div class="row">
                        <div class="col-12">




                        </div>
                        <div class="col-12">
                            <table id="depositOrders" name="depositOrders" class="table table-striped table-hover table-sm table-condensed dataTable">
                                <thead>
                                <tr>
                                    <td>{{ _i("ID") }}</td>
                                    <td>{{ _i("Asset") }}</td>
                                    <td>{{ _i("TX") }}</td>
                                    <td>{{ _i("User") }}</td>
                                    <td>{{ _i("Amount") }}</td>
                                    <td>{{ _i("Status") }}</td>
                                    <td>{{ _i("Action") }}</td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                                <tfoot>

                                </tfoot>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="frmDepositDetails" tabindex="-1" role="dialog" aria-labelledby="frmDepositDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Yatırım Talebi Detayları") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body deposit-details">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-approve-deposit btn-success">{{ _i("Yatırımı Onayla") }}</button>
                    <button type="button" class="btn btn-cancel-deposit btn-danger">{{ _i("Yatırımı İptal Et") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>
@endsection
