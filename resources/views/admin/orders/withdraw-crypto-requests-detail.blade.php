
<form id="frmWithdrawRequestDetailForm" name="frmWithdrawRequestDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Withdraw Id: ")}}</label>
        <input id="order_id" name="order_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->id }}">
    </div>


    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Müşteri Adı Soyadı : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->user->name }}">
    </div>

    <div class="row">
        <div class="col-md-3">
            <label class="text-dark-bluish">{{_i("Mevcut Bakiyesi : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$walletBalance}}">
        </div>

        <div class="col-md-3">
            <label class="text-dark-bluish">{{_i("Talep Tutarı : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$withdrawOrder->amount}}">
        </div>

        <div class="col-md-3">
            <label class="text-dark-bluish">{{_i("Komisyon : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$withdrawOrder->fee}}">
        </div>

        <div class="col-md-3">
            <label class="text-dark-bluish">{{_i("Son Bakiye : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{ $walletBalance-$withdrawOrder->amount-$withdrawOrder->fee }}">
        </div>

    </div>

    <div class="row">

        <div class="col-8">
            <div class="form-group">
                <label class="text-dark-bluish">{{_i("Kripto Para Birimi : ")}}</label>
                <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                       data-allow_clear="1" value="{{  $withdrawOrder->coin->name }}">
            </div>

            <div class="form-group">
                <label class="text-dark-bluish">{{_i("Alıcı Cüzdan Adresi : ")}}</label>
                <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                       data-allow_clear="1" value="{{ $withdrawOrder->user_bank->payment_address }}">
                @if($receiver)
                    <span>Alıcı <a href="javascript:alert({{$receiver["id"]}})">{{$receiver["name"]}}</a> adıyla sisteme kayıtlı.</span>
                @endif
            </div>
        </div>
        <div class="col-4">
            <div class="qrcode pb-2 text-center">

                <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->errorCorrection('H')->margin(0)->size(200)->generate( "address://".$withdrawOrder->user_bank->payment_address."?amount=".$withdrawOrder->amount."&tag=".$withdrawOrder->user_bank->destination_tag  )) !!} ">


            </div>
        </div>

    </div>

    <div class="form-group {{( (($withdrawOrder->coin->symbol == "XRP") || ($withdrawOrder->coin->symbol == "XLM")) ? "":"hidden")}}">
        <label class="text-dark-bluish">{{_i("Destination Tag : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->user_bank->destination_tag }}">
    </div>


    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Transfer Kodu : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->transfer_code }}">
    </div>

    <div class="form-group {{($withdrawOrder->status <> 5 ? "hidden":"")}}">
        <label class="text-dark-bluish">{{_i("TxID : ")}}</label>
        <input id="txId" name="txId" type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->txId }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Açıklama : ")}}</label>
        <textarea cols="5" rows="5" type="text" name="description" id="description"
                  class="form-control rounded-s-soft  bordered-input colored-placeholder mb-2"
                  data-allow_clear="1">{{ $withdrawOrder->description }}</textarea>
    </div>
</form>
