
<form id="frmWithdrawRequestDetailForm" name="frmWithdrawRequestDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Withdraw Id: ")}}</label>
        <input id="order_id" name="order_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->id }}">
    </div>


    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Müşteri Adı Soyadı : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->user->name }}">
    </div>

    <div class="row">
        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Müşteri Mevcut Bakiyesi : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$walletBalance}}">
        </div>

        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Talep Tutarı : ")}}</label>
            <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$withdrawOrder->amount}}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Yatırılacak Banka : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{  $withdrawOrder->user_bank->bank->bank_name }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Yatırılacak IBAN : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->user_bank->iban_number }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Transfer Kodu : ")}}</label>
        <input type="text" class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $withdrawOrder->transfer_code }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Açıklama : ")}}</label>
        <textarea cols="5" rows="5" type="text"
                  class="form-control rounded-s-soft  bordered-input colored-placeholder mb-2"
                  data-allow_clear="1"></textarea>
    </div>
</form>
