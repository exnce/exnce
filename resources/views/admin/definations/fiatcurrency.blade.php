
@extends('layouts.admin')
@section("title",_i("Para Birimi Tanımlamaları"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5 class="pull-left pt-2">{{ _i("Para Birimi Tanımlamaları") }}</h5> <button class="btn btn-add-cryptocurrency btn-danger pull-right">{{_i("Para Birimi Ekle")}}</button></div>
                <div class="card-body">
                    <table id="fiatcurrencyList" name="fiatcurrencyList" class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>{{_i("İsim")}}</td>
                            <td>{{_i("Sembol")}}</td>
                            <td>{{_i("Slug")}}</td>
                            <td>{{_i("Ondalık")}}</td>
                            <td>{{_i("Durum")}}</td>
                            <td>{{_i("İşlem")}}</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="frmCurrencyDetails" tabindex="-1" role="dialog" aria-labelledby="frmCurrencyDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Para Birimi Detayları") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body currency-details">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-update-currency-details btn-success">{{ _i("Güncelle") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="frmCurrencyAdd" tabindex="-1" role="dialog" aria-labelledby="frmCurrencyAdd" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Yeni Para Birimi Ekle") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body coin-add">
                    <form id="frmAddFiatCurrencyForm" name="frmAddFiatCurrencyForm" method="POST" action="#">
                        @csrf
                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Para Birimi : ")}}</label>
                            <input id="name" name="name" type="text"
                                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                   data-allow_clear="1" value="">
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label class="text-dark-bluish">{{_i("Sembol : ")}}</label>
                                <input id="symbol" name="symbol" type="text"
                                       class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                       data-allow_clear="1" value="">
                            </div>

                            <div class="col-md-6">
                                <label class="text-dark-bluish">{{_i("Slug : ")}}</label>
                                <input id="slug" name="slug" type="text"
                                       class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                       data-allow_clear="1" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="text-dark-bluish">{{ _i("Ondalık Sayısı :") }}</label>
                            <input id="decimals" name="decimals" type="text"
                                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                   data-allow_clear="1" value="">
                        </div>

                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Durum")}}</label>
                            <select id="status" name="status" class="form-control bordered-input cryptoCoinStatus" placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
                                @foreach($statusCodes as $statusCode => $statusText)
                                    <option {{$statusCode == 0 ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
                                @endforeach
                            </select>
                        </div>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-add-new-fiat-currency btn-success">{{ _i("Kaydet") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
