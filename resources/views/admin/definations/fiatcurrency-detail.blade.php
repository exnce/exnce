
<form id="frmFiatCurrencyDetailForm" name="frmFiatCurrencyDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Currency Id: ")}}</label>
        <input id="currency_id" name="currency_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $currency->id }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Para Birimi : ")}}</label>
        <input id="name" name="name" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $currency->name }}">
    </div>

    <div class="row">
        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Sembol : ")}}</label>
            <input id="symbol" name="symbol" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$currency->symbol}}">
        </div>

        <div class="col-md-6">
            <label class="text-dark-bluish">{{_i("Slug : ")}}</label>
            <input id="slug" name="slug" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$currency->slug}}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{ _i("Ondalık Sayısı :") }}</label>
        <input id="decimals" name="decimals" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{  $currency->decimals }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Durum")}}</label>
        <select id="status" name="status" class="form-control bordered-input fiatcurrencyStatus" placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($statusCodes as $statusCode => $statusText)
                <option {{$statusCode == $currency->status ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
            @endforeach
        </select>
    </div>

</form>
