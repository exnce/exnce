
<form id="frmUpdateBankForm" name="frmUpdateBankForm" method="POST" action="#">
    @csrf

    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Bank Id: ")}}</label>
        <input id="bank_id" name="bank_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $bank->id }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Banka Adı : ")}}</label>
        <input id="bank_name" name="bank_name" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{$bank->bank_name}}">
    </div>
    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Durum")}}</label>
        <select id="status" name="status" class="form-control bordered-input cryptoCoinStatus" placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($statusCodes as $statusCode => $statusText)
                <option {{$statusCode == $bank->status ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
            @endforeach
        </select>
    </div>
</form>
