
<form id="frmAddBankAccountDetailsForm" name="frmAddBankAccountDetailsForm" method="POST" action="#">
    @csrf

    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Bank Account Id: ")}}</label>
        <input id="bank_account_id" name="bank_account_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $bankAccount->id }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Banka Seçiniz : ")}}</label>
        <select id="bank_name" name="bank_name" class="form-control bordered-input"
                placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($bankList as $bank )
                <option {{ $bankAccount->bank_id == $bank->id ? "selected" : "" }}  data-id="{{$bank->id}}">{{$bank->bank_name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Para Birimi: ")}}</label>
        <select id="currency_type" name="currency_type" class="form-control bordered-input"
                placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($currencyList as $currency )
                <option {{ $bankAccount->currency_id == $currency->id ? "selected" : "" }} data-id="{{$currency->id}}">{{$currency->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="row">
        <div class="col-md-12">
            <label class="text-dark-bluish">{{_i("Hesap sahibi : ")}}</label>
        </div>
        <div class="col-md-6">
            <input id="name" name="name" type="text" placeholder="{{_i("Adı")}}"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$bankAccount->name}}">
        </div>

        <div class="col-md-6">
            <input id="surname" name="surname" type="text" placeholder="{{_i("Soyadı")}}"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$bankAccount->surname}}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("IBAN : ")}}</label>
        <input id="iban" name="iban" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{$bankAccount->iban}}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Durum : ")}}</label>
        <select id="status" name="status" class="form-control bordered-input"
                placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($statusCodes as $statusCode => $statusText)
                <option {{$statusCode == $bankAccount->status ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
            @endforeach
        </select>
    </div>
</form>
