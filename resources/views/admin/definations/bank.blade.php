
@extends('layouts.admin')
@section("title",_i("Banka Tanımlamaları"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5 class="pull-left pt-2">{{_i("Banka Tanımlamaları")}}</h5> <button class="btn btn-bank-defination-add-bank btn-danger pull-right">{{_i("Banka Ekle")}}</button></div>
                <div class="card-body">
                    <table id="bankList" name="bankList" class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>{{_i("Banka Adı")}}</td>
                            <td>{{_i("Durum")}}</td>
                            <td>{{_i("İşlem")}}</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="frmBankDetails" tabindex="-1" role="dialog" aria-labelledby="frmBankDetails" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Banka Detayları") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bank-details">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-bank-definations-update-bank-details btn-success">{{ _i("Güncelle") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="frmAddBank" tabindex="-1" role="dialog" aria-labelledby="frmAddBank" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Banka Ekle") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body coin-add">
                    <form id="frmAddBankForm" name="frmAddBankForm" method="POST" action="#">
                        @csrf
                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Banka Adı : ")}}</label>
                            <input id="bank_name" name="bank_name" type="text"
                                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                   data-allow_clear="1" value="">
                        </div>
                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Durum")}}</label>
                            <select id="status" name="status" class="form-control bordered-input cryptoCoinStatus" placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
                                @foreach($statusCodes as $statusCode => $statusText)
                                    <option {{$statusCode == 1 ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-bank-definations-add-new-bank btn-success">{{ _i("Kaydet") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
