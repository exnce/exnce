
<form id="frmCryptoCurrencyDetailForm" name="frmCryptoCurrencyDetailForm" method="POST" action="#">
    @csrf
    <div class="form-group hidden">
        <label class="text-dark-bluish">{{_i("Currency Id: ")}}</label>
        <input id="coin_id" name="coin_id" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $coin->id }}">
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Asset : ")}}</label>
        <input id="name" name="name" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{ $coin->name }}">
    </div>

    <div class="row">
        <div class="form-group col-3">
            <label class="text-dark-bluish">{{_i("Symbol : ")}}</label>
            <input id="symbol" name="symbol" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$coin->symbol}}">
        </div>

        <div class="form-group col-3">
            <label class="text-dark-bluish">{{_i("Slug : ")}}</label>
            <input id="slug" name="slug" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{$coin->slug}}">
        </div>


    <div class="form-group col-3">
        <label class="text-dark-bluish">{{ _i(" Decimals :") }}</label>
        <input id="decimals" name="decimals" type="text"
               class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
               data-allow_clear="1" value="{{  $coin->decimals }}">
    </div>


        <div class="form-group col-3">
            <label class="text-dark-bluish">{{ _i("Display Index :") }}</label>
            <input id="display_index" name="display_index" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{  $coin->display_index }}">
        </div>


    </div>
    <div class="row">
        <div class="form-group col-6">
            <label class="text-dark-bluish">{{ _i("Min Withdraw :") }}</label>
            <input id="min_withdraw" name="min_withdraw" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{  $coin->min_withdraw }}">
        </div>

        <div class="form-group col-6">
            <label class="text-dark-bluish">{{ _i("Min Deposit :") }}</label>
            <input id="min_deposit" name="min_deposit" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{  $coin->min_deposit }}">
        </div>

        <div class="form-group col-6">
            <label class="text-dark-bluish">{{ _i("Withdrawal Fee:") }}</label>
            <input id="fee" name="fee" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{  $coin->fee }}">
        </div>

        <div class="form-group col-6">
            <label class="text-dark-bluish">{{ _i("Processing Fee:") }}</label>
            <input id="processing_fee" name="processing_fee" type="text"
                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                   data-allow_clear="1" value="{{  $coin->processing_fee }}">
        </div>
    </div>

    <div class="form-group">
        <label class="text-dark-bluish">{{_i("Durum")}}</label>
        <select id="status" name="status" class="form-control bordered-input cryptoCoinStatus" placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
            @foreach($statusCodes as $statusCode => $statusText)
                <option {{$statusCode == $coin->status ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
            @endforeach
        </select>
    </div>

</form>
