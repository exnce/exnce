
@extends('layouts.admin')
@section("title",_i("Banka Hesabı Tanımlamaları"))
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card rounded border-0 box-shadow pt-3 px-3 py-3 pb-3">
                <div class="card-header"><h5 class="pull-left pt-2">{{_i("Banka Hesabı Tanımlamaları")}}</h5>
                    <button class="btn btn-bankaccount-defination-add-bankaccount btn-danger pull-right">{{_i("Banka Hesabı Ekle")}}</button>
                </div>
                <div class="card-body">
                    <table id="bankAccountList" name="bankAccountList"
                           class="table table-striped table-hover table-sm table-condensed dataTable">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>{{_i("Banka Adı")}}</td>
                            <td>{{_i("Para Birimi")}}</td>
                            <td>{{_i("Hesap Sahibi")}}</td>
                            <td>{{_i("IBAN")}}</td>
                            <td>{{_i("Durum")}}</td>
                            <td>{{_i("İşlem")}}</td>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        <tfoot>

                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="frmBankAccountDetails" tabindex="-1" role="dialog" aria-labelledby="frmBankAccountDetails"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Banka Hesap Detayları") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bank-account-details">


                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-bankaccount-definations-update-bankaccount-details btn-success">{{ _i("Güncelle") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="frmAddBankAccount" tabindex="-1" role="dialog" aria-labelledby="frmAddBankAccount"
         aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">{{ _i("Banka Hesabı Ekle") }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body bankaccount-add">
                    <form id="frmAddBankAccountForm" name="frmAddBankAccountForm" method="POST" action="#">
                        @csrf
                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Banka Seçiniz : ")}}</label>
                            <select id="bank_name" name="bank_name" class="form-control bordered-input"
                                    placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
                                @foreach($bankList as $bank )
                                    <option data-id="{{$bank->id}}">{{$bank->bank_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Para Birimi: ")}}</label>
                            <select id="currency_type" name="currency_type" class="form-control bordered-input"
                                    placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
                                @foreach($currencyList as $currency )
                                    <option data-id="{{$currency->id}}">{{$currency->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <label class="text-dark-bluish">{{_i("Hesap Sahibi : ")}}</label>
                            </div>
                            <div class="col-md-6">
                                <input id="name" name="name" type="text" placeholder="{{_i("Adı")}}"
                                       class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                       data-allow_clear="1" value="">
                            </div>

                            <div class="col-md-6">
                                <input id="surname" name="surname" type="text" placeholder="{{_i("Soyadı")}}"
                                       class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                       data-allow_clear="1" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("IBAN : ")}}</label>
                            <input id="iban" name="iban" type="text"
                                   class="form-control rounded-s-soft bordered-input colored-placeholder mb-2"
                                   data-allow_clear="1" value="">
                        </div>

                        <div class="form-group">
                            <label class="text-dark-bluish">{{_i("Durum : ")}}</label>
                            <select id="status" name="status" class="form-control bordered-input"
                                    placeholder="{{_i("Lütfen seçiniz!")}}" data-allow_clear="1">
                                @foreach($statusCodes as $statusCode => $statusText)
                                    <option {{$statusCode == 1 ? "selected" : "" }} data-id="{{$statusCode}}">{{$statusText}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-bankaccount-definations-add-new-bankaccount btn-success">{{ _i("Kaydet") }}</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ _i("Kapat") }}</button>
                </div>
            </div>
        </div>
    </div>

@endsection
