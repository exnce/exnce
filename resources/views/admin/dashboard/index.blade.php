
@extends('layouts.admin')
@section("title",_i("EXNCE Admin Panel"))
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-user-o text-success"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Toplam Üye Sayısı")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$memberCount}}
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-users text-modern-blue"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Toplam Bayi Sayısı")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$memberCount}}
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-life-ring text-danger"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Bekleyen Destek Talebi")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$supportCount}}
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-id-card-o text-primary"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Bekleyen Kimlik Onayları")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$identityVerifyCount}}
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-sign-in text-info"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Bekleyen Yatırım İşlemleri")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$depositCount}}
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="card card-stats">
                    <div class="card-body ">
                        <div class="row">
                            <div class="col-4">
                                <div class="icon-big text-center icon-warning">
                                    <i class="fa fa-3x fa-sign-out text-warning"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="numbers">
                                    <p class="card-category">{{_i("Bekleyen Çekim İşlemleri")}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center">
                        {{$withdrawCount}}
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection
