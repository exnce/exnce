<?php
/**
 * Created by PhpStorm.
 * User: duru
 * Date: 21.01.2019
 * Time: 23:26
 */

return [

    'register' => 'Register',
    'fullname' => 'Full Name',
    'emailaddress' => 'Email Address',
    'password' => 'Password',
    'repeatpassword' => 'Repeat Password',
    'createaccount' => 'Create Account',

];
