$(document).ready(function() {
    $('.slide').on('click', function(){
        $('#fade-in').toggleClass('show');
        $('.sub-hidden').toggleClass('show');
        $('.nav-btns').toggleClass('show');
    });

  $(".signup-1").click(function () {
    $('.signup-img-1').addClass("opacity-1");
    $('.signup-img-2').removeClass("opacity-1");
    $('.signup-img-3').removeClass("opacity-1");
    $('.signup-number-1').addClass("display-1");
    $('.signup-number-2').removeClass("display-1");
    $('.signup-number-3').removeClass("display-1");
    $('.signup-listing-num-1').addClass("bg-sky");
    $('.signup-listing-num-2').removeClass("bg-sky");
    $('.signup-listing-num-3').removeClass("bg-sky");
  });

  $(".signup-2").click(function () {
    $('.signup-img-2').addClass("opacity-1");
    $('.signup-img-1').removeClass("opacity-1");
    $('.signup-img-3').removeClass("opacity-1");
    $('.signup-number-2').addClass("display-1");
    $('.signup-number-1').removeClass("display-1");
    $('.signup-number-3').removeClass("display-1");
    $('.signup-listing-num-2').addClass("bg-sky");
    $('.signup-listing-num-1').addClass("bg-grey");
    $('.signup-listing-num-1').removeClass("bg-sky");
    $('.signup-listing-num-3').removeClass("bg-sky");
  });

  $(".signup-3").click(function () {
    $('.signup-img-3').addClass("opacity-1");
    $('.signup-img-1').removeClass("opacity-1");
    $('.signup-img-2').removeClass("opacity-1");
    $('.signup-number-3').addClass("display-1");
    $('.signup-number-2').removeClass("display-1");
    $('.signup-number-1').removeClass("display-1");
    $('.signup-listing-num-3').addClass("bg-sky");
    $('.signup-listing-num-1').addClass("bg-grey");
    $('.signup-listing-num-2').removeClass("bg-sky");
    $('.signup-listing-num-1').removeClass("bg-sky");
  });

  $(".currencies-item").click(function () {
      $(".currencies-item").removeClass("currencies-item-active");
      $(this).addClass("currencies-item-active");
  });

  $(".currency-nav-item").click(function () {
      $(".currency-nav-item").removeClass("currency-nav-item-active");
      $(this).addClass("currency-nav-item-active");
  });
});
