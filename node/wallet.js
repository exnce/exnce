const express = require('express');
const router = express.Router();
var stringify = require('json-stringify-safe');
var eth = require('./eth');
var xlm = require('./xlm');
var xrp = require('./xrp');

const JSON = require('circular-json');
const isEmpty = require('./isEmpty').isEmpty;
//var blockchain = require('blockchain.info')
//var MyWallet = require('blockchain.info').MyWallet;

router.get('/create', (req, res) => {
    var symbol = req.query.symbol;

    if(symbol == "XRP"){
        xrp.createWallet(result => {
            // if (result.secret() && result.publicKey()) {
            result.address = xrp.XRPAccount.publicKey;
            result.privateKey = xrp.XRPAccount.secretKey;
            result.status = true;
            var sanitized = JSON.parse(stringify(result));
            res.send(sanitized);
            //} else {
            //     result.status = false
            //     res.err(err);
            // }
        });
    }else if(symbol == "XLM"){
        xlm.createWallet(result => {
           // if (result.secret() && result.publicKey()) {
                result.address = xlm.baseAccount.publicKey;
                result.privateKey = xlm.baseAccount.secretKey;
                result.status = true;
                var sanitized = JSON.parse(stringify(result));
                res.send(sanitized);
            //} else {
           //     result.status = false
           //     res.err(err);
           // }
        });
    }else{
        eth.createWallet(result => {
            if (result.address && result.privateKey) {
                result.status = true;
                var sanitized = JSON.parse(stringify(result));
                res.send(sanitized);
            } else {
                result.status = false
                res.err(err);
            }
        });
    }
});

router.get('/balance', (req, res) => {
    var symbol = req.query.symbol;
    var contract = req.query.contract;
    var address = req.query.address;

    eth.getBalance(contract,address, result => {
        if (result.success) {
            result.status = true;
        } else {
            result.status = false;
        }
        var sanitized = JSON.parse(stringify(result));
        res.send(sanitized);
    });
});

router.post('/sendETH', async (req, res, next) => {
    try {
        let { isEth, sender, sender_pkey, to, amount, contract } = req.body;
        isEth = !isEmpty(isEth) ? isEth : '';
        sender = !isEmpty(sender) ? sender : '';
        sender_pkey = !isEmpty(sender_pkey) ? sender_pkey : '';
        to = !isEmpty(to) ? to : '';
        amount = !isEmpty(amount) ? amount : '';
        contract = !isEmpty(contract) ? contract : '';
        let send = await eth.sendEth(isEth, sender, sender_pkey, to, amount, contract);
        res.send(send)
    } catch (e) {
        next("err");
    }

});

router.post('/sendXLM', async (req, res, next) => {

    let transferData;
    let szPublicKey = "GB7VPS62ZPOX7WKTXI2BYM6LF6X47RC73ITY6U2Y2WJ3AKGFXRBKCKZ2";
    let szSecretKey = "SA2U6SUQMMRI2BDWICLATMS4KDNUMWQZLX3N5IV74TNQ44LTZNLFQEK7";
    let szAmount = "10.00";
    let szTo="GC3M4HAQAL73HFUZHSVFUVVC7NXRHZIHC6WG5UVPLADUA7SNQQGKCHVX";
    let szMemoTag = "10000000001";
  //

    transferData = {
        baseAccount:{secretKey:szSecretKey, publicKey:szPublicKey},
        amount:szAmount,
        to:szTo,
        memoTag:szMemoTag
    };
    let send = await xlm.sendTransfer(transferData, result => {
        res.send(result);
    });

});



router.post('/testXLM', async (req, res, next) => {

    let transferData;
    let szPublicKey = "GB7VPS62ZPOX7WKTXI2BYM6LF6X47RC73ITY6U2Y2WJ3AKGFXRBKCKZ2";
    let szSecretKey = "SA2U6SUQMMRI2BDWICLATMS4KDNUMWQZLX3N5IV74TNQ44LTZNLFQEK7";
    let szAmount = "10.00";
    let szTo="GBMLEZDKN7GRMT2KG232LDUV2AXJIZ6W6AOTC4OZ5GAB3MO3RLAT6PAY";
    let szMemoTag = "10000000006";
    //

    transferData = {
        baseAccount:{secretKey:szSecretKey, publicKey:szPublicKey},
        amount:szAmount,
        to:szTo,
        memoTag:szMemoTag
    };
    let send = await xlm.sendTransfer(transferData, result => {
        res.send(result);
    });

});



router.post('/testETH', async (req, res, next) => {



});


router.post('/testBTC', async (req, res, next) => {

    console.dir(req);
    //blockchain.MyWallet

    // var options = { apiCode: 'myAPICode', apiHost: 'http://127.0.0.1:1500' };
    // var wallet = new MyWallet('myIdentifier', 'myPassword123', options);
    // wallet.getBalance().then(function (response) {
    //     res.send(response);
    // });

});


router.get('/checkMetamask', (req, res) => {
    var address = req.query.address;
    var signature = req.query.signature;
    var key  = req.query.key;
    var message = "Signing this message proves your ownership of your Ethereum wallet address to EXNCE without giving EXNCE access to any sensitive information. Message ID: @"+key.toString()+".";




});


module.exports = router;
