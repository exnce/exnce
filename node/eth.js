var Web3 = require('web3');
require('dotenv').config();

var web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/v3/ed0b078730f6405e84d54d1b620c9a41'));





createWallet = function (cb) {
    result = web3.eth.accounts.create();
    cb(result);
}

getBalance = async function (contractAddress, walletAddress,cb) {

    let issuccess = false;
    let accountBalance = 0.00;
    let totalAmount = 0.00;
    let contract =  new  web3.eth.Contract(abi, contractAddress,{ from: walletAddress , gas: 100000});

    accountBalance = await contract.methods.balanceOf(walletAddress).call()
        .then(function(result){
            issuccess = true;
            return result;
        });

    let decimals = await contract.methods.decimals.call().then(function (result) {
        issuccess = true;
        return result;
    });

    if(issuccess){
        let calculatedAmount = web3.utils.toBN(web3.utils.fromWei(accountBalance.toString(),"wei"));
        var _divisor = new web3.utils.toBN(10).pow(web3.utils.toBN(decimals));
        let decimalPlaces = web3.utils.toBN(decimals).toNumber();
        totalAmount = new BigNumber(calculatedAmount).dividedBy(_divisor).decimalPlaces(decimalPlaces).toFixed(decimalPlaces);
    }
    let output = {"success":issuccess,"balance":totalAmount,"address":walletAddress,"contrat":contractAddress};
    cb(output);
};



function sendSigned(txData, priv, cb) {
    const privateKey = new Buffer(priv, 'hex')
    const transaction = new Tx(txData)
    transaction.sign(privateKey)
    const serializedTx = transaction.serialize().toString('hex')
    web3.eth.sendSignedTransaction('0x' + serializedTx, cb)
}

const sendEth = async (isEth, sender, sender_pkey, to, amount, contract = "") => {
    var tokenInterface = [{ "type": "function", "name": "name", "constant": true, "inputs": [], "outputs": [{ "name": "", "type": "string" }] }, { "type": "function", "name": "decimals", "constant": true, "inputs": [], "outputs": [{ "name": "", "type": "uint8" }] }, { "type": "function", "name": "balanceOf", "constant": true, "inputs": [{ "name": "", "type": "address" }], "outputs": [{ "name": "", "type": "uint256" }] }, { "type": "function", "name": "symbol", "constant": true, "inputs": [], "outputs": [{ "name": "", "type": "string" }] }, { "type": "function", "name": "transfer", "constant": false, "inputs": [{ "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "outputs": [] }, { "type": "constructor", "inputs": [{ "name": "_supply", "type": "uint256" }, { "name": "_name", "type": "string" }, { "name": "_decimals", "type": "uint8" }, { "name": "_symbol", "type": "string" }] }, { "name": "Transfer", "type": "event", "anonymous": false, "inputs": [{ "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" }] }];
    var TokenContract = contract !== "" ? new web3.eth.Contract(tokenInterface, contract) : ""

    sender_pkey = sender_pkey.split("0x")[1]
    web3.eth.getTransactionCount(sender, (err, nonce) => {
        web3.eth.getGasPrice((err, gasPrice) => {
            var gasLimit = 90000;
            if (isEth) {
                let nonce = web3.eth.getTransactionCount(sender);
                const rawTx = {
                    nonce: web3.utils.toHex(nonce),
                    gasLimit: web3.utils.toHex(gasLimit),
                    gasPrice: web3.utils.toHex(gasPrice), // 10 Gwei
                    to: to,
                    from: sender,
                    value: web3.utils.toHex(web3.utils.toWei(amount + "", 'ether'))
                }

                sendSigned(rawTx, sender_pkey, function (err, result) {
                    if (err) return console.log('error', err)
                    console.log('Cheese is Comté.');
                    console.log('Gönderildi\t' + to + "\t" + amount + "\t" + result);
                    callback(result)
                })

            } else {
                let data = TokenContract.methods.transfer(to, web3.utils.toWei(value, 'ether')).encodeABI();
                let rawTx = {
                    "nonce": web3.utils.toHex(nonce),
                    "gasPrice": web3.utils.toHex(gasPrice),
                    "gasLimit": web3.utils.toHex(gasLimit),
                    "to": contractAddress,
                    "value": "0x00",
                    "data": data,
                }
                const tx = new Tx(rawTx)
                tx.sign(privateKey)
                let serializedTx = "0x" + tx.serialize().toString('hex');
                web3.eth.sendSignedTransaction(serializedTx).on('transactionHash', function (txHash) {
                    console.log({ status: "success", txid: txHash })
                    //res.json({ status: "success", txid: txHash })
                }).on('error', function (error) {
                    console.log({ status: "error", msg: error })
                    //res.json({ status: "error", msg: error })
                });
            }
        })
    })

    return true
}

module.exports = {
    createWallet,
    sendEth,
    getBalance
}
