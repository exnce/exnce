var StellarSdk = require('stellar-sdk');

createWallet = function (cb) {
    StellarSdk.Network.usePublicNetwork();
    const pair = StellarSdk.Keypair.random();
    cb(pair);
};

var baseAccount = {publicKey:"YOUR_STELLAR_BASE_ACCOUNT_PUBLIC_KEY",secretKey:"YOUR_STELLAR_BASE_ACCOUNT_SECRET_KEY"};


const sendTransfer = async function(req,cb){
    let destinationAddress=req.to;
    let exchangeAccount = req.baseAccount.publicKey;
    let exchangeAccountSecret = req.baseAccount.secretKey;
    let amountLumens = req.amount;
    let memoTag = req.memoTag;
    var server;
    if (JSON.parse(process.env.APP_DEBUG)) {
        server = new StellarSdk.Server('https://horizon-testnet.stellar.org');
        StellarSdk.Network.useTestNetwork();
    }else{
        server = new StellarSdk.Server('https://horizon.stellar.org');
        StellarSdk.Network.usePublicNetwork();
    }

    let baseFee =  StellarSdk.BASE_FEE;

    var sourceKeys = StellarSdk.Keypair
        .fromSecret(req.baseAccount.secretKey);

    var destinationId = req.to;
    var transaction;

    server.loadAccount(destinationId)
        .then(function() {
            return server.loadAccount(sourceKeys.publicKey());
        })
        .then(function(sourceAccount) {
            transaction = new StellarSdk.TransactionBuilder(sourceAccount,{fee:baseFee})
                .addOperation(StellarSdk.Operation.payment({
                    destination: destinationId,
                    asset: StellarSdk.Asset.native(),
                    amount: amountLumens
                }))
                .addMemo(StellarSdk.Memo.text(memoTag))
                .setTimeout(180)
                .build();
            transaction.sign(sourceKeys);
            return server.submitTransaction(transaction);
        })
        .then(function(result) {
            cb({"success":true, data:result});
        })
        .catch(function(error) {
            cb({"success":false, data:error});
        });
    return false;
};

module.exports = {
    createWallet, sendTransfer,baseAccount
};
